#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "EfficiencyWithPrimitives.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "BeamParameters.hh"
#include "DownstreamTrack.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


EfficiencyWithPrimitives::EfficiencyWithPrimitives(Core::BaseAnalysis *ba) : Analyzer(ba, "EfficiencyWithPrimitives")
{
  
  //--Detector name array
  fTrigDetName[0] = "CHOD";
  fTrigDetName[1] = "RICH";
  fTrigDetName[2] = "LAV";
  fTrigDetName[3] = "MUV3";
  fTrigDetName[4] = "NewCHOD";
  fTrigDetName[5] = "TALK";
  fTrigDetName[6] = "LKr";  

/*MAP FOR TRIGGER SUPERCELLS*/
/****************************************
From cellID to supercell:
each supercell is 4 x 4 cells of LKr.
first supercell contains cells: x[0...3] y[0...3];
second supercell contains cells: x[0...3] y[4...7];
and so on.
The SC energy is the sum of the cells belonging to the SC 
and in time with the SC with maximum energy.
***************************************/
for(int m=0;m<128;m+=4){
    for(int k=0;k<128;k+=4){
      for(int i=0; i<4;i++){
	for (int j=0;j<4;j++){
	  supercells[std::make_pair(i+m, j+k)] = k/4+(m/4)*32;
	  }
      }
    }
  }

 SC = new SCStruct[1024];
}

EfficiencyWithPrimitives::~EfficiencyWithPrimitives(){

}

void EfficiencyWithPrimitives::InitOutput(){

 

}



void EfficiencyWithPrimitives::InitHist(){
 
}
void EfficiencyWithPrimitives::DefineMCSimple(){
}

void EfficiencyWithPrimitives::StartOfRunUser(){
}

void EfficiencyWithPrimitives::StartOfBurstUser(){

/**************************
End of burst: take the number of  
FineTime Bits used to generate the 
address of L0TP ram
**************************/

  EventHeader *Event;
  Event = GetEventHeader();


  SkipEvent=0;
  
  TTree *L0EOBTree = (TTree*)GetCurrentFile()->Get("SpecialTrigger");
  TBranch *BeamBranch=L0EOBTree->GetBranch("Beam");
  TBranch *L0TPBranch=L0EOBTree->GetBranch("L0TP");
  BeamSpecialTrigger* Beam = new BeamSpecialTrigger();
  L0TPSpecialTrigger *L0TPEOB = new L0TPSpecialTrigger();
  L0TPBranch ->SetAddress(&L0TPEOB);
  BeamBranch ->SetAddress(&Beam);
  
  Int_t nL0 = L0EOBTree-> GetEntries();
  std::vector< UInt_t > PrimitiveReceived;
  Double_t Argo=0;
  Bool_t NoBeam=0;

  if(nL0==0){
    SkipEvent=1;//No special triggers
  }

  for (Int_t iL0=0;iL0<nL0;iL0++){//Loop on special triggers
    
    L0EOBTree->GetEvent(iL0);
    SkipEvent=1;
    
    if(L0TPEOB->GetTriggerType()!=0x23) continue; // TriggerType of EOB
    
    SkipEvent=0;//Trovato L'EOB
    
    Argo=Beam->GetCountsARGONION();
    if(Argo<700e5){
      NoBeam=1;
      SkipEvent=1;
    }
    
    BitFineTime=L0TPEOB->GetFineTimeBits();
  }
}

void EfficiencyWithPrimitives::Process(int iEvent){
  if( SkipEvent==1 && GetWithMC()!=1 ) return;
  if( GetEventHeader()->GetEventQualityMask()!=0 ) return;
  
  L0TPData *L0Data = GetL0Data();

  Bool_t Control=0;
  UInt_t TimeStamp=L0Data->GetTimeStamp();
  UInt_t DataType =GetWithMC() ? 1 : L0Data->GetDataType();
 
  if( DataType0x10  ) Control=1; 
 
  if( Control==0 ) return;
  
  //initialize primitive time to -9999999:

  long long time[7][3]; //Timestamp + finetime [Detector][L0TP slot]

  for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){
      time[iTrigDet][iTrigSlot]=-99999;
    }
  }

 

  //Set Primitive time
  for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

      /***********************
       Each positive primitive 
       has bit 14 set at 1
      *************************/     
      
      if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14){

	/*
	  Having only one timestamp, depending on the slot and on the finetime
	  we have to correct for the rollover. This is done in GetSlotTime.
	  Time is with a precision of 100 ps.
	*/

	time[iTrigDet][iTrigSlot] = GetSlotTime((long long)TimeStamp,(Int_t)L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetFineTime(),reffinetime,iTrigSlot,BitFineTime);

      }
    }
  }

 
 //Set Primitive Reference time: CHOD or RICH.
  long long reftime=-999999;
  
  reftime=time[0][0]; //Central slot of the CHOD
  //reftime=time[1][0]; //Central slot of the RICH
   
 /*Now I check which detector is in time with the reference, and I calculate the primitive ID*/

  Double_t LKrPrimE=0;
  Double_t LKrPrimE20ns=0;

  //Set Global Primitive ID
  
  Int_t GlobalPrimitiveID[7]={0,0,0,0,0,0,0};
  
  /*Loop for all L0TP detector and for each slot of the ram around the time of the reference*/

  for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

      if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14)	{	
	
	if(iTrigDet==6){//LKr
	if(TMath::Abs(time[iTrigDet][iTrigSlot]-reftime)<100)LKrPrimE+=56*(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID()&0x1fff); //Energy of the primitive closer than 10 ns
	if(TMath::Abs(time[iTrigDet][iTrigSlot]-reftime)<200)LKrPrimE20ns+=56*(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID()&0x1fff); //Energy of the primitive closer than 20 ns
	}

	//Each detector was required in time closer than 10 ns with respect to the reference time
	if(TMath::Abs(time[iTrigDet][iTrigSlot]-reftime)<100) {//In 10 ns rispetto al trigger
	  GlobalPrimitiveID[iTrigDet] |= L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID(); // The primitive ID is the OR of the primitve ID in time
	}
      }
    }
  }
     
  //Trigger event set
  Bool_t CHOD   =0;
  Bool_t NewCHOD=0;
  Bool_t Qx     =0;
  Bool_t UMTC   =0;
  Bool_t RICH   =0;
  Bool_t MUV3   =0;
  Bool_t LAV    =0;
  Bool_t Calo   =0;


  if(GlobalPrimitiveID[0]!=0)        CHOD=1;
  if(GlobalPrimitiveID[1]!=0)        RICH=1;
  if(GlobalPrimitiveID[2]!=0)        LAV=1;
  if(GlobalPrimitiveID[3]!=0)        MUV3=1; 
  if(GlobalPrimitiveID[4] & 0x1<<11) Qx=1;
  if(GlobalPrimitiveID[4]!=0)        NewCHOD=1;
  if(GlobalPrimitiveID[4] & 0x1<<12) UMTC=1;
  if(GlobalPrimitiveID[6]!=0)        Calo=1; 

  
  
  //Example of pinunu trigger on the control sample
  if( Control ){
    if( RICH && !LAV && !Qx && NewCHOD && !MUV3 && UMTC && LKrPrimE < 20000. ){
      /* Do something that you expect in pinunu */
    }
  }
}




void EfficiencyWithPrimitives::PostProcess(){
    
}

void EfficiencyWithPrimitives::EndOfBurstUser(){


}

void EfficiencyWithPrimitives::EndOfRunUser(){


}
void EfficiencyWithPrimitives::EndOfJobUser(){
 
  SaveAllPlots();

}

void EfficiencyWithPrimitives::DrawPlot(){
}



//********************
int EfficiencyWithPrimitives::FromCellToSuperCell(int CellX, int CellY){
 
  return supercells[std::make_pair(CellX,CellY)]; 
}


long long EfficiencyWithPrimitives::GetSlotTime(long long TimeStamp, Int_t FineTime, Int_t ReferenceFineTime, Int_t iTrigSlot, Int_t BitFineTime)
{
  long long time=0;
  if(iTrigSlot==0){
    time = (long long)(TimeStamp)   * 0x100  + (Int_t)FineTime;
    return time;
  }

  
  if(BitFineTime==0){
    if(iTrigSlot==1){
      time = (long long)(TimeStamp-1) * 0x100  + (Int_t)FineTime;
      return time;
    }
    if(iTrigSlot==2){
      time = (long long)(TimeStamp+1) * 0x100  + (Int_t)FineTime;
      return time;
    }
  }

  Int_t compare = 0;
  for (Int_t i = 0; i < BitFineTime; i++) { 
    compare += (128 >> i); 
  }  
  if(ReferenceFineTime >= compare) {
    time= (long long)(TimeStamp + iTrigSlot - 1) * 0x100  + (Int_t)FineTime;
    return time;
  } else{
    time= (long long)(TimeStamp + iTrigSlot - 2) * 0x100  + (Int_t)FineTime;
    return time;
  }
  return -1;
   
}
