#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "TrackProcessor.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
//#include "utl.hh"
#include "Constants.hh"
#include "Parameters.hh"
#include "AnalysisTools.hh"
#include "BeamParameters.hh"
#include "SpectrometerRICHAssociationOutput.hh"
#include "SpectrometerNewCHODAssociationOutput.hh"
#include "SpectrometerNewCHODAssociation.hh"
#include "SpectrometerRICHAssociationOutputSingleRing.hh"
using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


TrackProcessor::TrackProcessor(Core::BaseAnalysis *ba) : Analyzer(ba, "TrackProcessor")
{
  fTool = AnalysisTools::GetInstance();
  RequestL0Data();
  RequestTree("Spectrometer",new TRecoSpectrometerEvent);
  RequestTree("CHOD",new TRecoCHODEvent);
  RequestTree("LKr",new TRecoLKrEvent);
  RequestTree("MUV1",new TRecoMUV1Event);
  RequestTree("MUV2",new TRecoMUV2Event);
  RequestTree("MUV3",new TRecoMUV3Event);
  RequestTree("RICH",new TRecoRICHEvent);

  // Geometrical instance
  fGeo = GeometricAcceptance::GetInstance();

  fCHODAnal = new CHODAnal(10,ba);
  fLKrAnal = new LKrAnal(10,ba);
  fStrawRapper = new StrawRapper(3,ba,fCHODAnal,fLKrAnal);
  fMUV3Anal = new MUV3Anal(ba);
  fRICHAnal = new RICHAnalysis(0,ba);
  fMUVAnal = new MUVAnal(ba);
  fMu1Cand = new MUVCandidate();
  fMu2Cand = new MUVCandidate();

  fStCand = new StrawCandidate();
  fCHODCand = new CHODCandidate();
  fNewCHODCand = new NewCHODCandidate();
  fLKrClusCand = new LKrCandidate();
  fLKrCellCand = new LKrCandidate();
  fMUV3Cand = new MUV3Candidate();
  fRICHCand = new RICHCandidate();
  fMu3TightCand = new MUV3Candidate();
  //  fMu3MediumCand = new MUV3Candidate();
  fMu3BroadCand = new MUV3Candidate();
  AddParam("Year", &fYear, 2016); // Default is 2015
  AddParam("Run", &fRun, 6610 ); // Default is 2015

  fIsMC = GetWithMC();
}

void TrackProcessor::InitOutput(){

  RegisterOutput("StrawCandidate",fStrawCandidate);
  RegisterOutput("CHODCandidate",fCHODCandidate);
  RegisterOutput("NewCHODCandidate",fNewCHODCandidate);
  RegisterOutput("LKrCandidate",fLKrClusterCandidate);
  RegisterOutput("LKrCellCandidate",fLKrCellCandidate);
  RegisterOutput("RICHCandidate",fRICHCandidate);
  RegisterOutput("MUV1Candidate",fMUV1Candidate);
  RegisterOutput("MUV2Candidate",fMUV2Candidate);
  RegisterOutput("MUV3TightCandidate",fMUV3TightCandidate);
  RegisterOutput("MUV3LooseCandidate",fMUV3BroadCandidate);
  RegisterOutput("IsK3pi",&fIsK3pi);
}

void TrackProcessor::InitHist(){

  BookHisto(new TH1F("Straw_NTracks","",100,0,100));
  if(GetWithMC()){
      BookHisto(new TH1I("pdgID", "Non complete events : pdgID", 0, 0, 0));

      BookHisto(new TH1F("Prod_Pkp" ,"",200,0, 100));
      BookHisto(new TH1F("Prod_Ppip","",200,0, 100));
      BookHisto(new TH1F("Prod_Ppim","",200,0, 100));
      BookHisto(new TH1F("Prod_Ppos","",200,0, 100));
      BookHisto(new TH1F("Prod_Mmiss","",375,-0.1,0.14375));

      BookHisto(new TH1F("Prod_Menu","",500,0, 0.5));
      BookHisto(new TH1F("Prod_M2pi","",500,0, 0.5));
      BookHisto(new TH1F("Prod_costh_pi","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_pim","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_e","",100,-1, 1));
      BookHisto(new TH1F("Prod_costh_nu","",100,-1, 1));

      BookHisto(new TH1F("Prod_costh_2pi","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_pipi","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_pik","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_enu","",100,-1, 1));
      BookHisto(new TH1F("Prod_costh_ek","",100,-1, 1));
      BookHisto(new TH1F("Prod_costh_enus","",100,-1, 1));
      BookHisto(new TH1F("Prod_phi","",126,-3.14, 3.14));
      BookHisto(new TH1F("Prod_zvtx","",300,0, 300000));
      BookHisto(new TH2F("Prod_zvtx_vs_p","",200,0,100,300,0, 300000));
  }

  // Fake track definition
  BookHisto(new TH1F("Straw_momtot","",100,0,100000));
  BookHisto(new TH1F("Straw_momfinal","",100,0,100000));

  // Association with detector candidate
  BookHisto(new TH1F("Track_chodminchi2","",200,0,50));
  BookHisto(new TH2F("Track_chodtrackmatch","",200,-100,100,400,0,2000));
  BookHisto(new TH2F("Track_chodtrackmatch_xy","",200,-500,500,200,-500,500));
  BookHisto(new TH2F("Track_chodprovax","",128,-1280,1280,200,-100,100));
  BookHisto(new TH2F("Track_chodprovay","",128,-1280,1280,200,-100,100));

  // Association with detector candidate
  BookHisto(new TH1F("Track_newchoddiscriminant","",200,0,50));
  BookHisto(new TH2F("Track_newchodtrackmatch","",200,-100,100,400,0,2000));
  BookHisto(new TH2F("Track_newchodtrackmatch_xy","",200,-500,500,200,-500,500));
  BookHisto(new TH2F("Track_newchodprovax","",128,-1280,1280,200,-100,100));
  BookHisto(new TH2F("Track_newchodprovay","",128,-1280,1280,200,-100,100));

  BookHisto(new TH2F("Track_lkr_clus_mindistvst","",200,-100,100,300,0,300));
  BookHisto(new TH2F("Track_lkr_clus_mindistvstchod","",400,-100,100,300,0,300));
  BookHisto(new TH2F("Track_lkr_clus_minxy","",300,-300,300,300,-300,300));
  BookHisto(new TH2F("Track_lkr_clus_energyvsdist","",300,0,300,400,0,100));
  BookHisto(new TH2F("Track_lkr_clus_energyvsdist_zoom","",150,0,150,200,0,10));
  BookHisto(new TH2F("Track_lkr_noclust","",128,-1263.2,1263.2,128,-1263.2,1263.2));
  BookHisto(new TH2F("Track_lkr_cell_mindistvst","",200,-100,100,300,0,300));
  BookHisto(new TH2F("Track_lkr_cell_mindistvstchod","",400,-100,100,300,0,300));
  BookHisto(new TH2F("Track_lkr_cell_minxy","",300,-300,300,300,-300,300));
  BookHisto(new TH2F("Track_lkr_cell_energyvsdist","",300,0,300,400,0,100));
  BookHisto(new TH2F("Track_lkr_cell_energyvsdist_zoom","",150,0,150,200,0,10));
  BookHisto(new TH2F("Track_lkr_nocell","",128,-1263.2,1263.2,128,-1263.2,1263.2));

  BookHisto(new TH1I("Track_rich_singlering_flag","",2,0,2 ));
  BookHisto(new TH2F("Track_rich_singlering_min_xydslope","",250,-0.01,0.01,250,-0.01,0.01));
  //BookHisto(new TH1F("Track_rich_singlering_min_chi2","",300,0,30 ));
  BookHisto(new TH1F("Track_rich_singlering_min_prob","",100,0,1 ));
  BookHisto(new TH2F("Track_rich_singlering_min_radius_vs_p","",100,0,100000, 600, 0, 300 ));
  BookHisto(new TH1I("Track_rich_singlering_min_dtchod","",1600,-100,100 ));
  BookHisto(new TH1F("Track_rich_singlering_min_nobservedhits","",50,0,50 ));
  BookHisto(new TH2F("Track_rich_singlering_final_radius_vs_p","",100,0,100000, 600, 0, 300 ));

  BookHisto(new TH1F("Track_rich_ring_chi2","",300,0,30 ));
  BookHisto(new TH1F("Track_rich_ring_nobservedhits","",50,0,50 ));
  BookHisto(new TH2F("Track_rich_ring_radius_vs_p","",100,0,100000, 600, 0, 300 ));
  BookHisto(new TH1F("Track_rich_hypothesis_all","",6,0,6 ));
  BookHisto(new TH1F("Track_rich_hypothesis_15_to_35_GeV","",6,0,6 ));
  BookHisto(new TH1F("Track_rich_ring_LR","",500,0,100 ));
  BookHisto(new TH2F("Track_rich_ring_LR_vs_status","",5000,0,100,5,0,5.));

  BookHisto(new TH1F("Track_q1associated","",2,0,2));
  BookHisto(new TH1F("Track_newchodassociated","",2,0,2));
  BookHisto(new TH1F("Track_lkrclusterassociated","",2,0,2));
  BookHisto(new TH1F("Track_lkrcellassociated","",2,0,2));
  BookHisto(new TH1F("Track_richmultiassociated","",2,0,2));
  BookHisto(new TH1F("Track_richsingleassociated","",2,0,2));
  BookHisto(new TH1F("Track_richassociated","",2,0,2));
  BookHisto(new TH1F("Track_muv1associated","",2,0,2));
  BookHisto(new TH1F("Track_muv2associated","",2,0,2));
  BookHisto(new TH1F("Track_muv3associated","",2,0,2));

}

void TrackProcessor::DefineMCSimple(){
    if(GetWithMC()){

        // kID = fMCSimple.AddParticle(0, 321);//kaon
        int kID = fMCSimple.AddParticle(0, 321);//kaon
        fMCSimple.AddParticle(kID, 211);//piplus
        fMCSimple.AddParticle(kID, -211);//piminus
        fMCSimple.AddParticle(kID, -11);//eplus
        //fMCSimple.AddParticle(kID, 22);//gamma from kaon
        //pi0ID = fMCSimple.AddParticle(kID, 111);//pi0
        fMCSimple.AddParticle(kID, 22);//g1
        fMCSimple.AddParticle(kID, 22);//g2
    }
}

void TrackProcessor::StartOfRunUser(){
  fHeader =GetEventHeader();

  fCHODAnal->StartBurst(fHeader->GetRunID());
  fLKrAnal->StartBurst(fYear,fHeader->GetRunID());

}

void TrackProcessor::StartOfBurstUser(){
  fIsFilter = GetTree("Reco")->FindBranch("FilterWord") ? true : false;
  fHeader =GetEventHeader();
  fL0Data =GetL0Data();
  fRICHAnal->StartBurst(fYear,fHeader->GetRunID(),fHeader->GetBurstTime());
}

void TrackProcessor::Process(int iEvent){


  fiEvent=iEvent;
  fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
  fCHODEvent         = (TRecoCHODEvent*)GetEvent("CHOD");
  fLKrEvent          = (TRecoLKrEvent*)GetEvent("LKr");
  fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");
  fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
  fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
  fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
  fSpecRICH = *(std::vector<SpectrometerRICHAssociationOutput>*)GetOutput("SpectrometerRICHAssociation.Output");
  fSpecNewCHOD = *(std::vector<SpectrometerNewCHODAssociationOutput>*)GetOutput("SpectrometerNewCHODAssociation.Output");

  if (fLKrEvent->GetErrorMask()) return; //check

  for(int i(0); i<20; i++){

    fStrawCandidate[i].Clear();
    fCHODCandidate[i].Clear();
    fNewCHODCandidate[i].Clear();
    fLKrClusterCandidate[i].Clear();
    fLKrCellCandidate[i].Clear();
    fMUV1Candidate[i].Clear();
    fMUV2Candidate[i].Clear();
    fMUV3Candidate[i].Clear();
    fRICHCandidate[i].Clear();
    fMUV3TightCandidate[i].Clear();
    fMUV3MediumCandidate[i].Clear();
    fMUV3BroadCandidate[i].Clear();
  }

  fStrawRapper->Clear();
  fCHODAnal->Clear();
  fMUV3Anal->Clear();
  fLKrAnal->Clear(0);
  fCHODAnal->SetEvent(fCHODEvent);
  fLKrAnal->SetEvent(fLKrEvent);
  fMUV3Anal->SetEvent(fMUV3Event);
  fMUVAnal->SetMUV1Event(fMUV1Event);
  fMUVAnal->SetMUV2Event(fMUV2Event);


  Int_t  L0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
  Int_t  L0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();

  fControlTrigger = SelectTrigger(0,L0DataType,L0TriggerWord);
  fPinunuTrigger  = SelectTrigger(2,L0DataType,L0TriggerWord);

  // Initialization
  fBadEvent = 0;
  fOneTrackID = -1;
  fTrackCounter = -1;
  fIsGoodTrackEvent = 0;
  fIsK3pi = 0;

  fNTracks = fSpectrometerEvent->GetNCandidates();

  FillHisto("Straw_NTracks", fNTracks);

  // Correct track momentum and time
  for (int iT(0); iT<fSpectrometerEvent->GetNCandidates(); iT++) {
      TRecoSpectrometerCandidate *pT = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(iT);
      if (fYear==2016 && !fIsMC) {
          pT->SetTime(pT->GetTime()-2.1); // mandatory since revision 1421. To be chkeced run by run
      }
      if(fIsMC)
          pT->SetTime(pT->GetTime()+1.85); // offset for the MC
  }

  bool fiducial=true;
  if(GetWithMC()) fiducial = MCPrintouts();
  if(GetWithMC() && !fiducial) return;

  //Preliminary conditions //note

  //Ctrl and Pinunu
  if (!fPinunuTrigger && !fControlTrigger) return; //check
  if(fNTracks == 0 || fNTracks > 10) fBadEvent = 1;//check
  if(fLKrEvent->GetNHits() < 1 || fLKrEvent->GetNHits() > 2000) return;//check

  if(!fBadEvent) UpdateTrackCounter();

  //Debug GR
  // if( GetEventHeader()->GetBurstID()!=1218) return;
  // if( GetEventHeader()->GetEventNumber()!=1766192 ||GetEventHeader()->GetBurstID()!=342) return;
  //if( GetEventHeader()->GetEventNumber()!=1766192 ||GetEventHeader()->GetBurstID()!=342) return;
  //if( GetEventHeader()->GetEventNumber()!=967670 ||GetEventHeader()->GetBurstID()!=55) return;
  //    GetEventHeader()->GetEventNumber()!=1233638 && GetEventHeader()->GetEventNumber()!=910197  &&
  //    GetEventHeader()->GetEventNumber()!=502359  && GetEventHeader()->GetEventNumber()!=37748) return;
  //if( GetEventHeader()->GetBurstID()!=1818 || GetEventHeader()->GetEventNumber()!=618223) return;
//  if( !(GetEventHeader()->GetBurstID()==307358 && GetEventHeader()->GetEventNumber()==2904) &&
//      !(GetEventHeader()->GetBurstID()==313796 && GetEventHeader()->GetEventNumber()==1062) &&
//      !(GetEventHeader()->GetBurstID()==440166 && GetEventHeader()->GetEventNumber()==3276) &&
//      !(GetEventHeader()->GetBurstID()==441814 && GetEventHeader()->GetEventNumber()==1475) &&
//      !(GetEventHeader()->GetBurstID()==469608 && GetEventHeader()->GetEventNumber()==1334) &&
//      !(GetEventHeader()->GetBurstID()==386910 && GetEventHeader()->GetEventNumber()==1880) &&
//      !(GetEventHeader()->GetBurstID()==390323 && GetEventHeader()->GetEventNumber()==798)  &&
//      !(GetEventHeader()->GetBurstID()==390344 && GetEventHeader()->GetEventNumber()==1851) &&
//      !(GetEventHeader()->GetBurstID()==401731 && GetEventHeader()->GetEventNumber()==3646)
// ) return;

 // if( !(GetEventHeader()->GetBurstID()==759 && GetEventHeader()->GetEventNumber()==422158) &&
 //     !(GetEventHeader()->GetBurstID()==496 && GetEventHeader()->GetEventNumber()==1626333) &&
 //     !(GetEventHeader()->GetBurstID()==105 && GetEventHeader()->GetEventNumber()==4617) &&
 //     !(GetEventHeader()->GetBurstID()==115 && GetEventHeader()->GetEventNumber()==1009011) &&
 //     !(GetEventHeader()->GetBurstID()==194 && GetEventHeader()->GetEventNumber()==1125945) &&
 //     !(GetEventHeader()->GetBurstID()==398 && GetEventHeader()->GetEventNumber()==1175899)
 //) return;

  AnalyzeTracks();

  fIsK3pi = PreselectK3pi();

  SetOutputState("StrawCandidate",kOValid);
  SetOutputState("CHODCandidate",kOValid);
  SetOutputState("LKrCandidate",kOValid);
  SetOutputState("IsK3pi",kOValid);

}

Bool_t TrackProcessor::PreselectK3pi() {

    //if(fControlTrigger) return false;
  if(fNTracks < 2 || fNTracks > 3) return false;
  if(fLKrEvent->GetNHits() < 1) return false;
  if(fLKrEvent->GetNCandidates() > 10) return false;

  return true;
}

//--------------- Good Track Selection ------------------//
Bool_t TrackProcessor::AnalyzeTracks(){

  if(fTrackCounter == -1) return 0;

  TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*) fSpectrometerEvent->GetCandidate(fTrackCounter);

  //Track selection
  Bool_t isGoodStraw = fStrawRapper->SelectTrack(fTrackCounter,fSpectrometerEvent);
  fStCand = fStrawRapper->GetStrawCandidate();
  if(isGoodStraw) SetStrawCandidate(fStCand);

  //cout << "counter = " << fTrackCounter << " cand good = " << fStCand->GetGoodTrack() << endl;
  FillHisto("Straw_momtot",track->GetMomentum());
  if(fStCand->GetGoodTrack()){
    FillHisto("Straw_momfinal",track->GetMomentum());
  }

  //CHODSelection
  Int_t goodchod = SelectCHODCandidate(track);
  Bool_t isCHODCandidate = goodchod==-1?0:1;
  fCHODCand->SetIsCHODCandidate(isCHODCandidate);
  FillHisto("Track_q1associated",isCHODCandidate);
  SetCHODCandidate(fCHODCand);

  // NewCHODSelection
  Int_t goodnewchod = SelectNewCHODCandidate(track);
  Bool_t isNewCHODCandidate = goodnewchod==-1?0:1;
  fNewCHODCand->SetIsNewCHODCandidate(isNewCHODCandidate);
  FillHisto("Track_newchodassociated",isNewCHODCandidate);
  SetNewCHODCandidate(fNewCHODCand);

  //LKrSelection
  Int_t goodlkr = SelectLKrCandidate(0,track);
  Bool_t isLKrCandidate = goodlkr==-1?0:1;
  FillHisto("Track_lkrclusterassociated",isLKrCandidate);
  fLKrClusCand->SetIsLKrCandidate(isLKrCandidate);
  SetLKrClusterCandidate(fLKrClusCand);

  Int_t goodlkrcell = SelectLKrCandidate(1,track);
  Bool_t isLKrCellCandidate = goodlkrcell==-1?0:1;
  fLKrCellCand->SetIsLKrCandidate(isLKrCellCandidate);
  FillHisto("Track_lkrcellassociated",isLKrCellCandidate);
  SetLKrCellCandidate(fLKrCellCand);

  //Look for RICH candidates matching the track using Jurgens analyser
  // Int_t goodrich = SelectRICHSingleCandidate(fTrackCounter,track);
  Int_t goodrich = SelectRICHSingleCandidate(track);
  Int_t goodrichmulti = SelectRICHMultiCandidate(fTrackCounter,track);
  Bool_t isRICHSingleCandidate = goodrich==-1?0:1;
  Bool_t isRICHMultiCandidate = goodrichmulti==-1?0:1;

  Bool_t isRICHCandidate = isRICHSingleCandidate || isRICHMultiCandidate;

  fRICHCand->SetIsRICHCandidate(isRICHCandidate);
  FillHisto("Track_richmultiassociated",isRICHMultiCandidate);
  FillHisto("Track_richsingleassociated",isRICHSingleCandidate);
  FillHisto("Track_richassociated",isRICHCandidate);
  fRICHCand->SetSingleID(goodrich);
  fRICHCand->SetMultiID(goodrich);
  SetRICHCandidate(fRICHCand);


  // Look for MUV candidates matching the track
  Int_t goodmuv1 = SelectMUVClusterCandidate(0,track);
  Bool_t isMUV1Candidate = goodmuv1==-1?0:1;
  fMu1Cand->SetIsMUVCandidate(isMUV1Candidate);
  FillHisto("Track_muv1associated",isMUV1Candidate);
  fMu1Cand->SetID(goodmuv1);
  SetMUV1Candidate(fMu1Cand);

  Int_t goodmuv2 = SelectMUVClusterCandidate(1,track);
  Bool_t isMUV2Candidate = goodmuv2==-1?0:1;
  fMu2Cand->SetIsMUVCandidate(isMUV2Candidate);
  FillHisto("Track_muv2associated",isMUV2Candidate);
  fMu2Cand->SetID(goodmuv2);
  SetMUV2Candidate(fMu2Cand);

  // Look for the MUV3 candidate matching to the track
  Bool_t isMUV3Candidate = SelectMUV3Candidate(track);
  FillHisto("Track_muv3associated",isMUV3Candidate);
  SetMUV3TightCandidate(fMu3TightCand);
  //SetMUV3MediumCandidate(fMu3MediumCand);
  SetMUV3BroadCandidate(fMu3BroadCand);

  fMUV3Anal->Clear();
  fMUVAnal->Clear();
  fLKrAnal->Clear(0);
  fCHODAnal->Clear();
  fStrawRapper->Clear();
  UpdateTrackCounter(); //fTrackCounter++

  if(fTrackCounter==fNTracks) return 1;
  AnalyzeTracks();
  return 1;
}

/////////////////////////////////////////////
// Select Q1 candidate matching each track //
/////////////////////////////////////////////
Int_t TrackProcessor::SelectCHODCandidate(TRecoSpectrometerCandidate *thisTrack) {

  Double_t xtrack = thisTrack->xAtAfterMagnet(239009);
  Double_t ytrack = thisTrack->yAtAfterMagnet(239389);

  if(!fCHODAnal->CHODQ1()) return -1;
  double timeoffset = 0;
  double ttime = thisTrack->GetTime() + timeoffset;
  vector<double> chodpar = fCHODAnal->CHODTrackMatching(xtrack,ytrack,ttime,0,GetWithMC());
  fCHODCand = fCHODAnal->GetCHODCandidate();
  Double_t dx = fCHODCand->GetX()-xtrack;
  Double_t dy = fCHODCand->GetY()-ytrack;

  //Correction for mc chod time offset
  //if(fIsMC)
  //fCHODCand->SetDeltaTime(fCHODCand->GetDeltaTime()+0.13);
  FillHisto("Track_chodminchi2",chodpar[0]);
  FillHisto("Track_chodtrackmatch",chodpar[2],sqrt(chodpar[1]));
  FillHisto("Track_chodtrackmatch_xy",dx,dy);
  FillHisto("Track_chodprovax",fCHODCand->GetX(),fCHODCand->GetDeltaTime());
  FillHisto("Track_chodprovay",fCHODCand->GetY(),fCHODCand->GetDeltaTime());


  return 1;
}
/////////////////////////////////////////////
// Select NewCHOD candidate matching each track //
/////////////////////////////////////////////
Int_t TrackProcessor::SelectNewCHODCandidate(TRecoSpectrometerCandidate * thisTrack) {
  Double_t xtrack = thisTrack->xAtAfterMagnet(fGeo->GetZNewCHOD());
  Double_t ytrack = thisTrack->yAtAfterMagnet(fGeo->GetZNewCHOD());

  double timeoffset =0;
  double ttime = thisTrack->GetTime() + timeoffset;

  int imin = 99999;
  double mindr = 99999;
  double mindx = 99999;
  double minx = 99999;
  double miny = 99999;
  double mindy = 99999;
  double mindt = 99999;

  for (int itrack(0); itrack<(int)fSpecNewCHOD.size();itrack++) {
     SpectrometerNewCHODAssociationOutput Match = fSpecNewCHOD[itrack];
     //Match.Print();
     for (int j(0); j<(int)Match.GetNAssociationRecords(); j++) {
       SpectrometerNewCHODAssociationRecord* Record = Match.GetAssociationRecord(j);
       int      id = Record->GetRecoHitID();
       Double_t dx = Record->GetRecoHitX()-xtrack;
       Double_t x  = Record->GetRecoHitX();
       Double_t y  = Record->GetRecoHitY();
       Double_t dy = Record->GetRecoHitY()-ytrack;
       Double_t dt = Record->GetRecoHitTime() - ttime;
       Double_t dr = TMath::Sqrt(dx*dx + dy*dy);
       //Record.Print();

       if(dr < mindr){
           imin=id;
           mindr=dr;
           mindt=dt;
           mindx=dx;
           mindy=dy;
           minx=x+xtrack;
           miny=y+ytrack;
       }
     }

   }

  double chi2newchod = mindt*mindt/(4*7*7)+mindr*mindr/(9*16*16); // Check the weights !

  // if(fIsMC){
  //mindt = mindt - 3.2;
  //}
  fNewCHODCand->SetRecoHitID(imin);
  fNewCHODCand->SetX(mindx+xtrack);
  fNewCHODCand->SetY(mindy+ytrack);
  fNewCHODCand->SetDeltaTime(mindt);
  fNewCHODCand->SetDiscriminant(chi2newchod);

  FillHisto("Track_newchoddiscriminant",chi2newchod);
  FillHisto("Track_newchodtrackmatch",mindt,mindr);
  FillHisto("Track_newchodtrackmatch_xy",mindx,mindy);
  FillHisto("Track_newchodprovax",minx,mindt);
  FillHisto("Track_newchodprovay",miny,mindt);

  return 1;

}


////////////////////////////////////////////////////////////////
// Select RICH candidate matching the track using single ring //
////////////////////////////////////////////////////////////////
// Int_t TrackProcessor::SelectRICHSingleCandidate(Int_t iTrack , TRecoSpectrometerCandidate *thisTrack) {
Int_t TrackProcessor::SelectRICHSingleCandidate(TRecoSpectrometerCandidate *thisTrack) {


  double chodtime = fCHODCand->GetDeltaTime() + thisTrack->GetTime();
  Bool_t singlering = false;
  int minidring = fRICHAnal->TrackSingleRingMatching(chodtime,fRICHEvent,thisTrack);
  fRICHCand = fRICHAnal->GetRICHSingleCandidate();

  if(minidring > -1) singlering = true;

  FillHisto("Track_rich_singlering_flag", singlering );

  std::map<double,int> time;
  std::map<double,int> slopes;
  double   focallength       = 17020;

  if(minidring > 0) {

    //TRecoRICHCandidate* minrichtime = (TRecoRICHCandidate*)fRICHEvent->GetTimeCandidate(minidring);
    Double_t radius     = fRICHCand->GetSingleRingRadius();
    Double_t ringprob   = fRICHCand->GetSingleRingChi2();
    Double_t richtime   = fRICHCand->GetSingleRingDeltaTime() + thisTrack->GetTime();
    double   dtchod     = richtime - chodtime;
    double   richthx    = fRICHCand->GetSingleRingXcenter()/focallength;
    double   richthy    = fRICHCand->GetSingleRingYcenter()/focallength;
    double   nobshits   = fRICHCand->GetSingleRingNHits();
    double   dslopex    = richthx - thisTrack->GetSlopeXAfterMagnet();
    double   dslopey    = richthy - thisTrack->GetSlopeYAfterMagnet();

    FillHisto("Track_rich_singlering_min_xydslope",dslopex,dslopey);
    FillHisto("Track_rich_singlering_min_dtchod",dtchod);
    FillHisto("Track_rich_singlering_min_prob",ringprob);
    FillHisto("Track_rich_singlering_min_radius_vs_p",thisTrack->GetMomentum(), radius);
    FillHisto("Track_rich_singlering_min_nobservedhits",nobshits);


    fRICHCand->SetSingleRingTime (dtchod+chodtime);
    fRICHCand->SetIsRICHSingleRingCandidate(1);
    fRICHCand->SetDiscriminant(fRICHCand->GetDiscriminant());
    fRICHCand->SetSingleRingChi2 (ringprob);
    fRICHCand->SetSingleRingRadius (radius);
    fRICHCand->SetSingleRingXcenter(fRICHCand->GetSingleRingXcenter());
    fRICHCand->SetSingleRingYcenter(fRICHCand->GetSingleRingYcenter());
    fRICHCand->SetSingleRingNHits(nobshits);

    if(fabs(dtchod) < 2 && ringprob > 0.01)
      FillHisto("Track_rich_singlering_final_radius_vs_p",thisTrack->GetMomentum(), radius);
  }

  //if(!fRICHCand->GetIsRICHSingleRingCandidate()) return -1;

  return minidring;


}

Int_t TrackProcessor::SelectRICHMultiCandidate(Int_t iTrack , TRecoSpectrometerCandidate *thisTrack) {

  //Multi ring
  if(!fSpecRICH[iTrack].isValid()) return -1;
  //double chodtime = fCHODCand->GetDeltaTime() + thisTrack->GetTime();

  Double_t hypothesis = fSpecRICH[iTrack].GetMostLikelyHypothesis();
  Double_t elll= fSpecRICH[iTrack].GetLikelihood(1);
  Double_t mull= fSpecRICH[iTrack].GetLikelihood(2);
  Double_t pill= fSpecRICH[iTrack].GetLikelihood(3);
  Double_t maxll= mull >= elll ? mull : elll;
  Double_t radius     = fSpecRICH[iTrack].GetRingRadius();
  Double_t chi2       = fSpecRICH[iTrack].GetRingFitChi2();
  TVector2 ringcenter = fSpecRICH[iTrack].GetRingCentre();
  Int_t    nobshits   = (Int_t)fSpecRICH[iTrack].GetNObservedHits();

  //Int_t    nasshits   = fSpecRICH[iTrack].GetNHitsAssigned();
  Int_t    mirrorid   = fSpecRICH[iTrack].GetTrackMirrorID();
  double   pilr= pill/maxll;
  //double   pilr= pill/(mull+elll);

  int nrings = fRICHEvent->GetNRingCandidates();
  fRICHCand->SetIsRICHMultiRingCandidate(1);
  fRICHCand->SetRICHMultiMuonLikelihood(mull);
  fRICHCand->SetRICHMultiElectronLikelihood(elll);
  fRICHCand->SetRICHMultiPionLikelihood(pill);
  fRICHCand->SetMostLikelyHypothesis(hypothesis);
  fRICHCand->SetRadius(radius);
  fRICHCand->SetXcenter(ringcenter.X());
  fRICHCand->SetYcenter(ringcenter.Y());
  fRICHCand->SetNHits(nobshits);
  fRICHCand->SetNRings(nrings);
  fRICHCand->SetPosNexp(fSpecRICH[iTrack].GetNExpectedSignalHits(1));
  fRICHCand->SetMuNexp(fSpecRICH[iTrack].GetNExpectedSignalHits(2));
  fRICHCand->SetPiNexp(fSpecRICH[iTrack].GetNExpectedSignalHits(3));

  fRICHCand->SetIsPion(pilr);
  fRICHCand->SetChi2(chi2);
  fRICHCand->SetMirrorID(mirrorid);

  FillHisto("Track_rich_ring_chi2",chi2);
  FillHisto("Track_rich_ring_radius_vs_p",thisTrack->GetMomentum(), radius);
  FillHisto("Track_rich_ring_nobservedhits",nobshits);
  FillHisto("Track_rich_hypothesis_all",hypothesis);
  if(thisTrack->GetMomentum() > 15000 && thisTrack->GetMomentum() < 35000 ){
    FillHisto("Track_rich_hypothesis_15_to_35_GeV",hypothesis);
    FillHisto("Track_rich_ring_LR",pill/(mull+elll));
    FillHisto("Track_rich_ring_LR_vs_status",pill/(mull+elll),hypothesis);


  }

  return 1;

}


//////////////////////////
// Select lkr candidate //
//////////////////////////
Int_t TrackProcessor::SelectLKrCandidate(Int_t flag, TRecoSpectrometerCandidate *thisTrack) {

  TVector3 posatlkr = fTool->GetPositionAtZ(thisTrack,241093.);
  Double_t xpart = posatlkr.X();
  Double_t ypart = posatlkr.Y();
  Int_t minid = -1;
  Double_t chodtime = fCHODCand->GetDiscriminant()<20 ? fCHODCand->GetDeltaTime()+thisTrack->GetTime() : 999999.;
  Bool_t isMatchedCand = 0;
  Double_t lkrtime = 99999999.;
  Double_t dist = 99999999.;
  Double_t dtime = 99999999.;
  Double_t dtimechod = 99999999.;
  Double_t energy = 0;
  Double_t dx = -99999.;
  Double_t dy = -99999.;

  switch (flag) {
  case 0:
    minid = fLKrAnal->TrackClusterMatching(chodtime,thisTrack->GetTime(),posatlkr); // cluster correction already called in 2photonanalysis
    fLKrClusCand = fLKrAnal->GetLKrClusterCandidate();
    if (minid>=0 && fLKrClusCand->GetDiscriminant()<150. && fabs(fLKrClusCand->GetDeltaTime())<30.) isMatchedCand = 1;
    if (!isMatchedCand) {
      FillHisto("Track_lkr_noclust",xpart,ypart);
      minid=-1;
      break;
    }
    else {
      lkrtime   = fLKrClusCand->GetDeltaTime()+thisTrack->GetTime();
      dist      = fLKrClusCand->GetDiscriminant();
      dtime     = fLKrClusCand->GetDeltaTime();
      energy    = fLKrClusCand->GetEnergy();
      dx        = fLKrClusCand->GetX()-xpart;
      dy        = fLKrClusCand->GetY()-ypart;
      dtimechod = lkrtime-chodtime;
      FillHisto("Track_lkr_clus_mindistvst",dtime,dist);
      FillHisto("Track_lkr_clus_mindistvstchod",dtimechod,dist);
      FillHisto("Track_lkr_clus_energyvsdist",dist,energy);
      FillHisto("Track_lkr_clus_energyvsdist_zoom",dist,energy);
      FillHisto("Track_lkr_clus_minxy",dx,dy);
      break;
    }
  case 1:
    minid = fLKrAnal->TrackCellMatching(chodtime,thisTrack->GetTime(),posatlkr);
    fLKrCellCand = fLKrAnal->GetLKrCellCandidate();
    if (minid>=0) isMatchedCand = 1;
    if (!isMatchedCand) {
      FillHisto("Track_lkr_nocell",xpart,ypart);
      minid=-1;

      break;
    }
    lkrtime   = fLKrCellCand->GetDeltaTime()+thisTrack->GetTime();
    dist      = fLKrCellCand->GetDiscriminant();
    dtime     = fLKrCellCand->GetDeltaTime();
    energy    = fLKrCellCand->GetEnergy();
    dx        = fLKrCellCand->GetX()-xpart;
    dy        = fLKrCellCand->GetY()-ypart;
    dtimechod = lkrtime-chodtime;
    FillHisto("Track_lkr_cell_mindistvst",dtime,dist);
    FillHisto("Track_lkr_cell_mindistvstchod",dtimechod,dist);
    FillHisto("Track_lkr_cell_energyvsdist",dist,energy);
    FillHisto("Track_lkr_cell_energyvsdist_zoom",dist,energy);
    FillHisto("Track_lkr_cell_minxy",dx,dy);
    break;

  default:
    break;
  }

  return minid;
}

//////////////////////////
// Select MUV candidate //
//////////////////////////
Int_t TrackProcessor::SelectMUVClusterCandidate(Int_t flag, TRecoSpectrometerCandidate *thisTrack) {
  Int_t minid = -1;
  switch (flag){
  case 0:
    minid = fMUVAnal->TrackMUV1Matching(thisTrack);
    fMu1Cand = fMUVAnal->GetMUV1Candidate();
    break;

  case 1:
    minid = fMUVAnal->TrackMUV2Matching(thisTrack);
    fMu2Cand = fMUVAnal->GetMUV2Candidate();
    break;
  default:
    break;
  }

  return minid;
}

///////////////////////////
// Select MUV3 candidate //
///////////////////////////
Bool_t TrackProcessor::SelectMUV3Candidate(TRecoSpectrometerCandidate *thisTrack) {
  Int_t minid1 = fMUV3Anal->TrackTimeMatching(thisTrack);
  fMu3TightCand = fMUV3Anal->GetMUV3TightCandidate();
  //Int_t minid1 = fMUV3Anal->TrackMatching(1,thisTrack);
  //Int_t minid0 = fMUV3Anal->TrackMatching(0,thisTrack);
  //Int_t minid2 = fMUV3Anal->TrackMatching(2,thisTrack);
  //fMu3MediumCand = fMUV3Anal->GetMUV3MediumCandidate();
  //fMu3BroadCand = fMUV3Anal->GetMUV3BroadCandidate();
  // Bool_t isMUV3Candidate = (minid0>-1 || minid1>-1 || minid2>-1) ? 1 : 0;
  // Bool_t isMUV3Candidate = (minid0>-1 || minid2>-1) ? 1 : 0;1;
  Bool_t isMUV3Candidate = (minid1>-1) ? 1 : 0;
  return isMUV3Candidate;
}

Int_t TrackProcessor::SelectTrigger(int triggerType, int type, int mask) {
  int bitPhysics = 0;
  int bitControl = 4;
  int bitMinBias = 0;
  int bitPinunu = 1;


  if (type&0x2) return 0; // skip periodics


  // Select trigger
  if ((triggerType==0) && (type>>bitControl)&1) return 1; // control trigger.
  else if (triggerType==1) { // physics minimum bias trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitMinBias)&1) return 1;
  }
  else if (triggerType==2) { // physics pinunu trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitPinunu)&1) return 1;
  }
  else return 0; // trigger not found.

  return 1;

}

void TrackProcessor::PostProcess(){
  return;

}

void TrackProcessor::EndOfBurstUser(){
  return;
}

void TrackProcessor::EndOfRunUser(){
  return;

}

void TrackProcessor::EndOfJobUser(){
  SaveAllPlots();
  fStrawRapper->GetUserMethods()->SaveAllPlots();
  fCHODAnal->GetUserMethods()->SaveAllPlots();
  fLKrAnal->GetUserMethods()->SaveAllPlots();
  fMUVAnal->GetUserMethods()->SaveAllPlots();
  fMUV3Anal->GetUserMethods()->SaveAllPlots();
  fRICHAnal->GetUserMethods()->SaveAllPlots();


}

void TrackProcessor::DrawPlot(){
  return;
}

TrackProcessor::~TrackProcessor(){
}
Int_t TrackProcessor::MCPrintouts(){
      //bool withMC = true;
      //
      //if(fMCSimple.fStatus == MCSimple::kMissing){
      //    Event* MCTruthEvent = GetMCEvent();
      //    for(int i=0; i<MCTruthEvent->GetNKineParts(); i++){
      //        FillHisto("pdgID", ((KinePart*)MCTruthEvent->GetKineParts()->At(i))->GetParticleName(), 1);
      //    }
      //    withMC = false;
      //}
      //if(fMCSimple.fStatus == MCSimple::kEmpty) withMC = false;

       TIter partit(GetMCEvent()->GetKineParts()) ;
       bool g1=false;
       bool g2=false;
       bool isKS=false;

       TLorentzVector pk  ;
       TLorentzVector pe  ;
       TLorentzVector pnu  ;
       TLorentzVector ppip;
       TLorentzVector ppim;

       //cout << "Event start <<<<<<<<<<<<<<<<<<<" << endl;
       while(partit.Next()){
           //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
           if(((KinePart*)(*partit))->GetParticleName().EqualTo("kaon0S") ){
               //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
               fKplus  = ((KinePart*)(*partit));
               isKS=true;
           }
           if(((KinePart*)(*partit))->GetParticleName().EqualTo("kaon+") ){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
               fKplus  = ((KinePart*)(*partit));
               pk = fKplus->GetInitial4Momentum();
      }
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("pi+") ){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        fPiplus  = ((KinePart*)(*partit));
        ppip = fPiplus->GetInitial4Momentum();
      }
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("nu_e") ){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        fNu  = ((KinePart*)(*partit));
        pnu = fNu->GetInitial4Momentum();
      }
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("pi-")){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        fPiminus = ((KinePart*)(*partit));
        ppim = fPiminus->GetInitial4Momentum();
      }
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("e+")){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        fEplus = ((KinePart*)(*partit));
         pe = fEplus->GetInitial4Momentum();
      }
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("gamma") && !g1){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data() << "1" << std::endl;
        g1=true;
        // fg1     = fMCSimple["gamma"][0]; //for the gamma1
        fg1     = ((KinePart*)(*partit)); //for the gamma1
      }
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("gamma") && g1 && !g2){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data() << "2" << std::endl;
        g2      =true;
        fmck2pig=true;
        fg2     = ((KinePart*)(*partit)); //for the gamma2
      }
    }

       FillHisto("Prod_zvtx",fKplus->GetEndPos().Z());
       FillHisto("Prod_zvtx_vs_p",1e-3*ppip.P(),fKplus->GetEndPos().Z());
       if(isKS && (fKplus->GetEndPos().Z() < 105000 || fKplus->GetEndPos().Z() > 180000) ) return 0;

       TLorentzVector p2pi = ppip + ppim;
       TLorentzVector penu = pe + pnu;

       TVector3 pkvec      = pk.Vect();
       TVector3 ppipvec    = ppip.Vect();
       TVector3 ppimvec    = ppim.Vect();
       TVector3 p2pivec    = p2pi.Vect();
       TVector3 pevec      = pe.Vect();
       TVector3 pnuvec     = pnu.Vect();
       TVector3 penuvec    = penu.Vect();

       pe.Boost(-pk.BoostVector());
       pnu.Boost(-pk.BoostVector());
       ppim.Boost(-pk.BoostVector());
       ppip.Boost(-pk.BoostVector());
       pk.Boost(-pk.BoostVector());


       penu = pe + pnu;
       p2pi = ppip + ppim;

       // std::cout << "P2pi CM= " << p2pi.P() << std::endl;
       // std::cout << "Ppi+ CM= " << ppip.P() << std::endl;
       // std::cout << "Ppi- CM= " << ppim.P() << std::endl;
       // std::cout << "Pe   CM= " << pe.P() << std::endl;
       // std::cout << "Pnu  CM= " << pnu.P() << std::endl;
       // std::cout << "Penu CM= " << penu.P() << std::endl;

       pe.Boost  (-penu.BoostVector());
       pnu.Boost (-penu.BoostVector());
       ppip.Boost(-p2pi.BoostVector());
       ppim.Boost(-p2pi.BoostVector());

       pkvec      = pk.Vect();
       ppipvec    = ppip.Vect();
       ppimvec    = ppim.Vect();
       p2pivec    = p2pi.Vect();
       pevec      = pe.Vect();
       pnuvec     = pnu.Vect();
       penuvec    = penu.Vect();

       // std::cout << "costhpi CM= " << TMath::Cos((ppipvec.Mag()*p2pivec.Mag())/ppipvec.Dot(p2pivec)) << std::endl;
       // std::cout << "costhe  CM= " << TMath::Cos((pevec.Mag()*penuvec.Mag())/pevec.Dot(penuvec)) << std::endl;

       // std::cout << "phi  CM= " << (p2pivec.Mag()*penuvec.Mag())/p2pivec.Dot(penuvec) << std::endl;

       double costhpi   = TMath::Cos(ppipvec.Angle(p2pivec));
       double costhpim  = TMath::Cos(ppimvec.Angle(p2pivec));
       double costhe    = TMath::Cos(pevec.Angle(penuvec));
       double costhnu  = TMath::Cos(pnuvec.Angle(penuvec));

       double costhenu  = TMath::Cos(penuvec.Angle(pkvec));
       double costh2pi  = TMath::Cos(p2pivec.Angle(pkvec));

       double costhpik  = TMath::Cos(ppipvec.Angle(pevec));
       double costhpipi = TMath::Cos(ppipvec.Angle(ppimvec));
       double costhek   = TMath::Cos(pevec.Angle(pkvec));
       double costhenus = TMath::Cos(pevec.Angle(pnuvec));

       double phi = p2pivec.Angle(penuvec);
       // std::cout << "costh2pi CM= " << costh2pi << std::endl;

       // std::cout << "Testing ---" << std::endl;
       // std::cout << "Mk   = " << 1e-6*pk.M2() << std::endl;
       // std::cout << "Pk   = " << pk.P() << std::endl;
       // std::cout << "Menu = " << 1e-6*penu.M2() << std::endl;
       // std::cout << "Penu = " << penu.P() << std::endl;
       // std::cout << "M2pi = " << 1e-6*p2pi.M2() << std::endl;
       // std::cout << "P2pi = " << p2pi.P() << std::endl;
       // std::cout << "Costhpi = " << costhpi << std::endl;
       // std::cout << "Costhe = " << costhe << std::endl;

       //Lab frame
       FillHisto("Prod_Pkp" ,1e-3*pk.P());
       FillHisto("Prod_Ppip",1e-3*ppip.P());
       FillHisto("Prod_Ppim",1e-3*ppim.P());
       FillHisto("Prod_Ppos",1e-3*pe.P());

       FillHisto("Prod_M2pi",1e-6*p2pi.M2());
       FillHisto("Prod_Menu",1e-6*penu.M2());

       FillHisto("Prod_costh_pi",costhpi);
       FillHisto("Prod_costh_pim",costhpim);
       FillHisto("Prod_costh_e",costhe);
       FillHisto("Prod_costh_nu",costhnu);
       FillHisto("Prod_phi",phi);

       FillHisto("Prod_costh_2pi",costh2pi);
       FillHisto("Prod_costh_enu",costhenu);

       FillHisto("Prod_costh_pipi",costhpipi);
       FillHisto("Prod_costh_pik",costhpik);

       FillHisto("Prod_costh_ek",costhek);
       FillHisto("Prod_costh_enus",costhenus);



       if( fKplus->GetEndPos().Z() < 105000 || fKplus->GetEndPos().Z() > 180000 ) return 0;

       return 1;
}
