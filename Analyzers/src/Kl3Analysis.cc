#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "Kl3Analysis.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "BeamParameters.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

Kl3Analysis::Kl3Analysis(Core::BaseAnalysis *ba) : Analyzer(ba, "Kl3Analysis") {

  RequestL0Data();
  RequestTree("Cedar",        new TRecoCedarEvent, "Reco");
  RequestTree("GigaTracker",  new TRecoGigaTrackerEvent, "Reco");
  RequestTree("Spectrometer", new TRecoSpectrometerEvent, "Reco");
  RequestTree("CHANTI",       new TRecoCHANTIEvent, "Reco");
  RequestTree("LAV",          new TRecoLAVEvent, "Reco");
  RequestTree("CHOD",         new TRecoCHODEvent, "Reco");
  RequestTree("NewCHOD",      new TRecoNewCHODEvent, "Reco");
  RequestTree("RICH",         new TRecoRICHEvent, "Reco");
  RequestTree("LKr",          new TRecoLKrEvent, "Reco");
  RequestTree("MUV1",         new TRecoMUV1Event, "Reco");
  RequestTree("MUV2",         new TRecoMUV2Event, "Reco");
  RequestTree("MUV3",         new TRecoMUV3Event, "Reco");
  RequestTree("IRC",          new TRecoIRCEvent, "Reco");
  RequestTree("SAC",          new TRecoSACEvent, "Reco");
  
  controlEventsPerBurst = 0;

  fLAVMatching = new LAVMatching();
  fSAVMatching = new SAVMatching();

  AddParam("MaxNBursts",  &fMaxNBursts,  5000); // max number of bins in histograms

  myGigaTrackerAnalysis = new GigaTrackerAnalysis(ba);
}

Kl3Analysis::~Kl3Analysis() {
  delete fLAVMatching;
  delete fSAVMatching;
}

void Kl3Analysis::InitOutput() {
}

void Kl3Analysis::InitHist() {
  
  BookHisto("ControlEventsPerBurst", new TH1D("ControlEventsPerBurst", "Control events per burst;Burst ID", fMaxNBursts, 0, fMaxNBursts));

  // Preselect tracks
  BookHisto("Tracks_All_pTrack", new TH1D("Tracks_All_pTrack", "All tracks momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_Positive_pTrack", new TH1D("Tracks_Positive_pTrack", "Positive tracks momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_FourChambers_pTrack", new TH1D("Tracks_FourChambers_pTrack", "Four chambers tracks momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_chi2LT20_pTrack", new TH1D("Tracks_chi2LT20_pTrack", "#chi^{2} < 20 tracks momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_SpecAcceptance_pTrack", new TH1D("Tracks_SpecAcceptance_pTrack", "Tracks in STRAW acceptance momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_CHODAcceptance_pTrack", new TH1D("Tracks_CHODAcceptance_pTrack", "Tracks in CHOD acceptance momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_NewCHODAcceptance_pTrack", new TH1D("Tracks_NewCHODAcceptance_pTrack", "Tracks in NewCHOD acceptance momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_LKrAcceptance_pTrack", new TH1D("Tracks_LKrAcceptance_pTrack", "Tracks in LKr acceptance momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_MUV1Acceptance_pTrack", new TH1D("Tracks_MUV1Acceptance_pTrack", "Tracks in MUV1 acceptance momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_MUV2Acceptance_pTrack", new TH1D("Tracks_MUV2Acceptance_pTrack", "Tracks in MUV2 acceptance momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_MUV3Acceptance_pTrack", new TH1D("Tracks_MUV3Acceptance_pTrack", "Tracks in MUV3 acceptance momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_CHODAssociated_pTrack", new TH1D("Tracks_CHODAssociated_pTrack", "CHOD associated tracks momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_NewCHODAssociated_pTrack", new TH1D("Tracks_NewCHODAssociated_pTrack", "NewCHOD associated tracks momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_LKrAssociated_pTrack", new TH1D("Tracks_LKrAssociated_pTrack", "LKr associated tracks momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_CEDARNewCHOD_TimeDifference", new TH1D("Tracks_CEDARNewCHOD_TimeDifference", "Tracks, CEDAR - NewCHOD time;#DeltaT [ns]", 100, -25, 25));
  BookHisto("Tracks_CEDARNewCHODTimeMatched_pTrack", new TH1D("Tracks_CEDARNewCHODTimeMatched_pTrack", "Tracks with, (CEDAR-NewCHOD) time < 10ns, momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("Tracks_Selected_pTrack", new TH1D("Tracks_Selected_pTrack", "Selected tracks momentum; pTrack [GeV/c]", 160, 0, 80));

  BookHisto("test_Tracks_Selected_pTrack", new TH1D("test_Tracks_Selected_pTrack", "test Selected tracks momentum; pTrack [GeV/c]", 160, 0, 80));

  BookHisto("Multiplicity_SelectedTracks", new TH1D("Multiplicity_SelectedTracks", "Selected tracks multiplicity; # tracks", 10, 0, 10));
  
  // GTK matching
  
  BookHisto("Multiplicity_GTKCandidates", new TH1D("Multiplicity_GTKCandidates", "Number of good GTK candidates; # GTK candidates", 10, 0, 10));
  BookHisto("SelectedTrack_GTKMatched_pTrack", new TH1D("SelectedTrack_GTKMatched_pTrack", "Selected tracks, with a min. of 1 GTK matching, momentum; pTrack [GeV/c]", 160, 0, 80));
  BookHisto("BestMatchingGTKCandidate_CEDARGTK_TimeDifference", new TH1D("BestMatchingGTKCandidate_CEDARGTK_TimeDifference", "Best GTK-track match, time difference;#DeltaT [ns]", 100, -1, 1));
  BookHisto("SelectedTrack_CEDARGTKTime_pTrack", new TH1D("SelectedTrack_CEDARGTKTime_pTrack", "Selected tracks, CEDAR-GTK time < 0.5ns, momentum; pTrack [GeV/c]", 160, 0, 80)); 
  BookHisto("BestMatchingGTKCandidate_CEDARGTK_Discriminant", new TH1D("BestMatchingGTKCandidate_CEDARGTK_Discriminant", "BestMatchingGTKCandidate, CEDAR/GTK discriminant;Discriminant", 100, -0.01, 0.1));
  BookHisto("SelectedTrack_GTKDiscriminant_pTrack", new TH1D("SelectedTrack_GTKDiscriminant_pTrack", "Selected tracks, GTK discriminant<0.01, momentum; pTrack [GeV/c]", 160, 0, 80)); 
  BookHisto("BestMatchingGTKCandidate_zVertex", new TH1D("BestMatchingGTKCandidate_zVertex", "BestMatchingGTKCandidate, z vertex;zVertex [m]", 300, 0, 300));
  BookHisto("SelectedTrack_zVertex_pTrack", new TH1D("SelectedTrack_zVertex_pTrack", "Selected tracks, 110m < zVertex < 180m, momentum; pTrack [GeV/c]", 160, 0, 80)); 
  BookHisto("BestMatchingGTKCandidate_CDA", new TH1D("BestMatchingGTKCandidate_CDA", "BestMatchingGTKCandidate, CDA;CDA [mm]", 40, 0, 40));
  BookHisto("SelectedTrack_CDA_pTrack", new TH1D("SelectedTrack_CDA_pTrack", "Selected tracks, CDA < 10mm, momentum; pTrack [GeV/c]", 160, 0, 80)); 
  
  

  // One track kinematics
  BookHisto("OneTrack_beamKchPion_mm2", new TH1D("OneTrack_beamKchPion_mm2", "One track, mm2(beam K^{+} - #pi^{+});mm2(#pi^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKchPion_pVmm2", new TH2D("OneTrack_beamKchPion_pVmm2", "One track, P vs mm2(beam K^{+} - #pi^{+}); pTrack [GeV/c];mm2(#pi^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKchPion_mm2", new TH1D("OneTrack_gtkKchPion_mm2", "One track, mm2(gtk K^{+} - #pi^{+});mm2(#pi^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKchPion_pVmm2", new TH2D("OneTrack_gtkKchPion_pVmm2", "One track, P vs mm2(gtkK^{+} - #pi^{+}); pTrack [GeV/c];mm2(#pi^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));

  BookHisto("OneTrack_beamKMuon_mm2", new TH1D("OneTrack_beamKMuon_mm2", "One track, mm2(beam K^{+} - #mu^{+});mm2(#mu^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKMuon_pVmm2", new TH2D("OneTrack_beamKMuon_pVmm2", "One track, P vs mm2(beam K^{+} - #mu^{+}); pTrack [GeV/c];mm2(#mu^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKMuon_mm2", new TH1D("OneTrack_gtkKMuon_mm2", "One track, mm2(gtk K^{+} - #mu^{+});mm2(#mu^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKMuon_pVmm2", new TH2D("OneTrack_gtkKMuon_pVmm2", "One track, P vs mm2(gtk K^{+} - #mu^{+}); pTrack [GeV/c];mm2(#mu^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));

  BookHisto("OneTrack_beamKPositron_mm2", new TH1D("OneTrack_beamKPositron_mm2", "One track, mm2(beam K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKPositron_pVmm2", new TH2D("OneTrack_beamKPositron_pVmm2", "One track, P vs mm2(beam K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_mm2", new TH1D("OneTrack_gtkKPositron_mm2", "One track, mm2(gtk K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_pVmm2", new TH2D("OneTrack_gtkKPositron_pVmm2", "One track, P vs mm2(gtk K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));

  // One track kinematics, CHANTI veto
  BookHisto("OneTrack_beamKchPion_mm2_CHANTI", new TH1D("OneTrack_beamKchPion_mm2_CHANTI", "One track, CHANTI veto, mm2(beam K^{+} - #pi^{+});mm2(#pi^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKchPion_pVmm2_CHANTI", new TH2D("OneTrack_beamKchPion_pVmm2_CHANTI", "One track, CHANTI veto, P vs mm2(beam K^{+} - #pi^{+}); pTrack [GeV/c];mm2(#pi^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKchPion_mm2_CHANTI", new TH1D("OneTrack_gtkKchPion_mm2_CHANTI", "One track, CHANTI veto, mm2(gtk K^{+} - #pi^{+});mm2(#pi^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKchPion_pVmm2_CHANTI", new TH2D("OneTrack_gtkKchPion_pVmm2_CHANTI", "One track, CHANTI veto, P vs mm2(gtkK^{+} - #pi^{+}); pTrack [GeV/c];mm2(#pi^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));

  BookHisto("OneTrack_beamKMuon_mm2_CHANTI", new TH1D("OneTrack_beamKMuon_mm2_CHANTI", "One track, CHANTI veto, mm2(beam K^{+} - #mu^{+});mm2(#mu^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKMuon_pVmm2_CHANTI", new TH2D("OneTrack_beamKMuon_pVmm2_CHANTI", "One track, CHANTI veto, P vs mm2(beam K^{+} - #mu^{+}); pTrack [GeV/c];mm2(#mu^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKMuon_mm2_CHANTI", new TH1D("OneTrack_gtkKMuon_mm2_CHANTI", "One track, CHANTI veto, mm2(gtk K^{+} - #mu^{+});mm2(#mu^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKMuon_pVmm2_CHANTI", new TH2D("OneTrack_gtkKMuon_pVmm2_CHANTI", "One track, CHANTI veto, P vs mm2(gtk K^{+} - #mu^{+}); pTrack [GeV/c];mm2(#mu^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));

  BookHisto("OneTrack_beamKPositron_mm2_CHANTI", new TH1D("OneTrack_beamKPositron_mm2_CHANTI", "One track, CHANTI veto, mm2(beam K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKPositron_pVmm2_CHANTI", new TH2D("OneTrack_beamKPositron_pVmm2_CHANTI", "One track, CHANTI veto, P vs mm2(beam K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_mm2_CHANTI", new TH1D("OneTrack_gtkKPositron_mm2_CHANTI", "One track, CHANTI veto, mm2(gtk K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_pVmm2_CHANTI", new TH2D("OneTrack_gtkKPositron_pVmm2_CHANTI", "One track, CHANTI veto, P vs mm2(gtk K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  
  BookHisto("OneTrack_Multiplicity_AssociatedMUV3", new TH1D("OneTrack_Multiplicity_AssociatedMUV3", "Number of track associated MUV3 candidates; # MUV3 candidates", 10, 0, 10));

  // Ke3 selection
  BookHisto("OneTrack_beamKPositron_mm2_MUV3", new TH1D("OneTrack_beamKPositron_mm2_MUV3", "One track, MUV3 veto, mm2(beam K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKPositron_pVmm2_MUV3", new TH2D("OneTrack_beamKPositron_pVmm2_MUV3", "One track, MUV3 veto, P vs mm2(beam K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_mm2_MUV3", new TH1D("OneTrack_gtkKPositron_mm2_MUV3", "One track, MUV3 veto, mm2(gtk K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_pVmm2_MUV3", new TH2D("OneTrack_gtkKPositron_pVmm2_MUV3", "One track, MUV3 veto, P vs mm2(gtk K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));

  BookHisto("OneTrack_Ke3_MUV1Energy", new TH1D("OneTrack_Ke3_MUV1Energy", "One track, Ke3 selection, energy in MUV1; Energy [GeV]", 120, 0, 30));
  BookHisto("OneTrack_Ke3_MUV2Energy", new TH1D("OneTrack_Ke3_MUV2Energy", "One track, Ke3 selection, energy in MUV2; Energy [GeV]", 120, 0, 30));

  BookHisto("OneTrack_beamKPositron_mm2_MUV12Energy", new TH1D("OneTrack_beamKPositron_mm2_MUV12Energy", "One track, MUV1+2 energy < 5GeV, mm2(beam K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_beamKPositron_pVmm2_MUV12Energy", new TH2D("OneTrack_beamKPositron_pVmm2_MUV12Energy", "One track, MUV1+2 energy < 5GeV, P vs mm2(beam K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_mm2_MUV12Energy", new TH1D("OneTrack_gtkKPositron_mm2_MUV12Energy", "One track, MUV1+2 energy < 5GeV, mm2(gtk K^{+} - e^{+});mm2(e^{+}) [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto("OneTrack_gtkKPositron_pVmm2_MUV12Energy", new TH2D("OneTrack_gtkKPositron_pVmm2_MUV12Energy", "One track, MUV1+2 energy < 5GeV, P vs mm2(gtk K^{+} - e^{+}); pTrack [GeV/c];mm2(e^{+}) [GeV^{2}/c^{4}]", 160, 0, 80, 100, -0.15, 0.15));
}

void Kl3Analysis::StartOfBurstUser() {
  isFilteredData = GetTree("Reco")->FindBranch("FilterWord") ? true : false;
}

void Kl3Analysis::Process(Int_t) {

  int burstID   = isMC ? 0 : GetEventHeader()->GetBurstID();
  int runNumber = isMC ? 0 : GetEventHeader()->GetRunID();
  //int L0TriggerFlags = isMC ? 0xFF : GetL0Data()->GetTriggerFlags();

  isMC = GetWithMC();
  isControlData = (GetL0Data()->GetDataType() == 0x10);
  isPhysicsData = (GetL0Data()->GetDataType() == 0x1 );

  if (!isMC && !isControlData) return;
  FillHisto("ControlEventsPerBurst", burstID);

  cedarEvent  = (TRecoCedarEvent*) GetEvent("Cedar");
  chantiEvent = (TRecoCHANTIEvent*)GetEvent("CHANTI");
  spectrometerEvent = (TRecoSpectrometerEvent*) GetEvent("Spectrometer");
  chodEvent = (TRecoCHODEvent*)  GetEvent("CHOD");
  newCHODEvent = (TRecoNewCHODEvent*)  GetEvent("NewCHOD");
  lkrEvent = (TRecoLKrEvent*) GetEvent("LKr");
  muv1Event = (TRecoMUV0Event*) GetEvent("MUV1");
  muv2event = (TRecoMUV0Event*) GetEvent("MUV2");
  muv3event = (TRecoMUV0Event*) GetEvent("MUV3");
  TRecoLAVEvent*    LAVEvent    = (TRecoLAVEvent*)   GetEvent("LAV");
  TRecoIRCEvent*    IRCEvent    = (TRecoIRCEvent*)   GetEvent("IRC");
  TRecoSACEvent*    SACEvent    = (TRecoSACEvent*)   GetEvent("SAC");
  
  OutputState gigatrackerEventState;
  if (isFilteredData) {
    gigatrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker"); 
  } else { 
    gigatrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",gigatrackerEventState);
  }

  if (gigatrackerEvent->GetErrorMask()) return;
  
  spectrometerCHODAssociation = *(std::vector<SpectrometerCHODAssociationOutput>*)GetOutput("SpectrometerCHODAssociation.Output");
  spectrometerNewCHODAssociation = *(std::vector<SpectrometerNewCHODAssociationOutput>*)GetOutput("SpectrometerNewCHODAssociation.Output");
  spectrometerLKrAssociation = *(std::vector<SpectrometerLKrAssociationOutput>*)GetOutput("SpectrometerLKrAssociation.Output");
  spectrometerMUV3Association = *(std::vector<SpectrometerMUV3AssociationOutput>*)GetOutput("SpectrometerMUV3Association.Output");
  TClonesArray& spectrometerCalorimetersAssociation = *(TClonesArray*)GetOutput("SpectrometerCalorimetersAssociation.MatchedClusters");
  nTracks = spectrometerEvent->GetNCandidates();
  
  // Perform a basic quality check on tracks
  Kl3Analysis::SelectTracks();
  FillHisto("Multiplicity_SelectedTracks", nSelectedTracks);
  if (nSelectedTracks != 1) return;

  int selectedTrackIndex = selectedTracks.at(0);
  selectedTrack = (TRecoSpectrometerCandidate*) spectrometerEvent->GetCandidate(selectedTrackIndex);

  // Find the best (closest) NewCHOD hit
  SpectrometerNewCHODAssociationOutput spectrometerNewCHODAssociationOutput = spectrometerNewCHODAssociation[selectedTrackIndex];
  int bestNewCHODHitIndex = spectrometerNewCHODAssociationOutput.GetBestMatchingRecordID();
  TRecoNewCHODHit *bestNewCHODHit = (TRecoNewCHODHit*)newCHODEvent->GetHit(bestNewCHODHitIndex);
  double newCHODHitTime = bestNewCHODHit->GetTime();

  // Find the cloest CEDAR candidate to the best NewCHOD hit
  int nCEDARCandidates = cedarEvent->GetNCandidates();
  int nGoodCEDARCandidates = 0;

  double bestCEDARTime=-999.9;
  double lowestNewCHODCEDARdT=999.9;
    
  for (int iCEDARCandidate=0; iCEDARCandidate < nCEDARCandidates; iCEDARCandidate++) {
    TRecoCedarCandidate* cedarCandidate = (TRecoCedarCandidate*) cedarEvent->GetCandidate(iCEDARCandidate);
    if (cedarCandidate->GetNSectors() < 4) return;
    nGoodCEDARCandidates++;
    double cedarTime = cedarCandidate->GetTime();
    double newCHODCEDARdT = cedarTime - newCHODHitTime;
    if (fabs(newCHODCEDARdT) < fabs(lowestNewCHODCEDARdT)) {
      bestCEDARTime = cedarTime;
      lowestNewCHODCEDARdT = newCHODCEDARdT;
    }
  }

  if (!nGoodCEDARCandidates) return;
  FillHisto("Tracks_CEDARNewCHOD_TimeDifference", lowestNewCHODCEDARdT);
    
  // CEDAR NewCHOD timing
  if (fabs(lowestNewCHODCEDARdT) > 10.0) return;
  //FillHisto("Tracks_CEDARNewCHODTimeMatched_pTrack", pTrack*1e-3);

  //if(!Kl3Analysis::MatchGTK()) return;
  //
  double pSelectedTrack = selectedTrack->GetMomentum();
  int nGoodGTKTracks=0;
  bestGTKCandidateIndex=-1;
  double discriminantCEDAR = -1;
  double cda = 9999999.;

  myGigaTrackerAnalysis->Clear();
  myGigaTrackerAnalysis->SetTimeShift(0.);
  myGigaTrackerAnalysis->SetRecoTimingCut(1.1);
  myGigaTrackerAnalysis->SetMatchingTimingCut(-0.4,0.4);

  // Reconstruct GTK candidates
  if (isFilteredData) {
    nGoodGTKTracks = myGigaTrackerAnalysis->ReconstructCandidates(gigatrackerEvent, bestCEDARTime, GetEventHeader()->GetTimeStamp(),1,isMC); // Remove previous candidates !
  } else {
    nGoodGTKTracks = myGigaTrackerAnalysis->ReconstructCandidates(gigatrackerEvent, bestCEDARTime, GetEventHeader()->GetTimeStamp(),1,isMC); // Remove previous candidates !
  }
  FillHisto("Multiplicity_GTKCandidates", nGoodGTKTracks);

  // Find and test the best candidate for one track <--------------------- !!!!!!!!!!
  bestGTKCandidateIndex = myGigaTrackerAnalysis->TrackCandidateMatching(gigatrackerEvent,selectedTrack,bestCEDARTime,0,nGoodGTKTracks);
  cout <<  "best gtk index: " << bestGTKCandidateIndex << endl;
  cout << "local cedar time:  " << bestCEDARTime << endl;
  if (bestGTKCandidateIndex < 0) return ;
  FillHisto("SelectedTrack_GTKMatched_pTrack", pSelectedTrack*1e-3);
    
  TRecoGigaTrackerCandidate* gtkCandidate = (TRecoGigaTrackerCandidate*) gigatrackerEvent->GetCandidate(bestGTKCandidateIndex);
  double gtkTime = gtkCandidate->GetTime();
  double gtkCEDARdT  = gtkTime - bestCEDARTime;
    
  FillHisto("BestMatchingGTKCandidate_CEDARGTK_TimeDifference", gtkCEDARdT);
  if (gtkCEDARdT > 0.5) return;
  FillHisto("SelectedTrack_CEDARGTKTime_pTrack", pSelectedTrack*1e-3);

  TVector3 vertex = myGigaTrackerAnalysis->ComputeVertex(gtkCandidate,selectedTrack,&cda);
  discriminantCEDAR = myGigaTrackerAnalysis->Discriminant(cda,gtkCEDARdT);

  FillHisto("BestMatchingGTKCandidate_CEDARGTK_Discriminant", discriminantCEDAR);    
  if (discriminantCEDAR <= 0.01) return;
  FillHisto("SelectedTrack_GTKDiscriminant_pTrack", pSelectedTrack*1e-3);

  double zVertex = vertex.z();
  FillHisto("BestMatchingGTKCandidate_zVertex", zVertex*1e-3);
  if (zVertex < 110000 || zVertex > 180000) return;
  FillHisto("SelectedTrack_zVertex_pTrack", pSelectedTrack*1e-3);

  FillHisto("BestMatchingGTKCandidate_CDA", cda);
  if (cda > 10) return;
  FillHisto("SelectedTrack_CDA_pTrack", pSelectedTrack*1e-3);
  //
  // GTK kaon
  //TRecoGigaTrackerCandidate* gtkCandidate = (TRecoGigaTrackerCandidate*) gigatrackerEvent->GetCandidate(bestGTKCandidateIndex);
  TVector3 gtkMomentum = gtkCandidate->GetMomentum();
  gtkKaon.SetVectM(gtkMomentum, MKCH);

  // Beam kaon
  TVector3 beamMomentum = BeamParameters::GetInstance()->GetBeamThreeMomentumVsRun(runNumber, !isMC);
  beamKaon.SetVectM(beamMomentum, MKCH);

  // Spectrometer track
  TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*) spectrometerEvent->GetCandidate(selectedTracks.at(0));  
  TVector3 trackMomentum = track->GetThreeMomentumBeforeMagnet();
  pOneTrack = track->GetMomentum();
  
  // pi+
  chargedPion.SetVectM(trackMomentum, MPI);
  mm2BeamKaonChargedPion = (beamKaon - chargedPion).M2();
  mm2GTKKaonChargedPion = (gtkKaon - chargedPion).M2();

  FillHisto("OneTrack_beamKchPion_mm2", mm2BeamKaonChargedPion*1e-6);
  FillHisto("OneTrack_beamKchPion_pVmm2", pOneTrack*1e-3, mm2BeamKaonChargedPion*1e-6);
  FillHisto("OneTrack_gtkKchPion_mm2", mm2GTKKaonChargedPion*1e-6);
  FillHisto("OneTrack_gtkKchPion_pVmm2", pOneTrack*1e-3, mm2GTKKaonChargedPion*1e-6);

  // mu+
  muon.SetVectM(trackMomentum, MMU);
  mm2BeamKaonMuon = (beamKaon - muon).M2();
  mm2GTKKaonMuon = (gtkKaon - muon).M2();

  FillHisto("OneTrack_beamKMuon_mm2", mm2BeamKaonMuon*1e-6);
  FillHisto("OneTrack_beamKMuon_pVmm2", pOneTrack*1e-3, mm2BeamKaonMuon*1e-6);
  FillHisto("OneTrack_gtkKMuon_mm2", mm2GTKKaonMuon*1e-6);
  FillHisto("OneTrack_gtkKMuon_pVmm2", pOneTrack*1e-3, mm2GTKKaonMuon*1e-6);

  //e+
  positron.SetVectM(trackMomentum, MEL);
  mm2BeamKaonPositron = (beamKaon - positron).M2();
  mm2GTKKaonPositron = (gtkKaon - positron).M2();

  FillHisto("OneTrack_beamKPositron_mm2", mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_beamKPositron_pVmm2", pOneTrack*1e-3, mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_mm2", mm2GTKKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_pVmm2", pOneTrack*1e-3, mm2GTKKaonPositron*1e-6);


  // CHANTI veto
  if (chantiEvent->GetNCandidates()) return;

  // pi+
  FillHisto("OneTrack_beamKchPion_mm2_CHANTI", mm2BeamKaonChargedPion*1e-6);
  FillHisto("OneTrack_beamKchPion_pVmm2_CHANTI", pOneTrack*1e-3, mm2BeamKaonChargedPion*1e-6);
  FillHisto("OneTrack_gtkKchPion_mm2_CHANTI", mm2GTKKaonChargedPion*1e-6);
  FillHisto("OneTrack_gtkKchPion_pVmm2_CHANTI", pOneTrack*1e-3, mm2GTKKaonChargedPion*1e-6);

  // mu+
  FillHisto("OneTrack_beamKMuon_mm2_CHANTI", mm2BeamKaonMuon*1e-6);
  FillHisto("OneTrack_beamKMuon_pVmm2_CHANTI", pOneTrack*1e-3, mm2BeamKaonMuon*1e-6);
  FillHisto("OneTrack_gtkKMuon_mm2_CHANTI", mm2GTKKaonMuon*1e-6);
  FillHisto("OneTrack_gtkKMuon_pVmm2_CHANTI", pOneTrack*1e-3, mm2GTKKaonMuon*1e-6);

  //e+
  FillHisto("OneTrack_beamKPositron_mm2_CHANTI", mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_beamKPositron_pVmm2_CHANTI", pOneTrack*1e-3, mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_mm2_CHANTI", mm2GTKKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_pVmm2_CHANTI", pOneTrack*1e-3, mm2GTKKaonPositron*1e-6);
  
  nMUV3Candidates = muv3event->GetNCandidates();
  FillHisto("OneTrack_Multiplicity_AssociatedMUV3", nMUV3Candidates);

  spectrometerMUV3AssociationOutput = spectrometerMUV3Association[selectedTrackIndex]; 
  nAssociatedMUV3Candidates = spectrometerMUV3AssociationOutput.GetNAssociationRecords();

  calorimeterCluster = (CalorimeterCluster*)spectrometerCalorimetersAssociation[selectedTrackIndex];
  muv1Energy = calorimeterCluster->GetMUV1Energy();
  muv2Energy = calorimeterCluster->GetMUV1Energy();

  // is Ke3?
  SelectKe3Candidates();

  // is Km3?

  // is K2pi?

}

void Kl3Analysis::EndOfJobUser() {
  SaveAllPlots();
}

void Kl3Analysis::SelectTracks() {
  
  selectedTracks.clear();
  
  // Keep tracks which meet a basic quality check
  for (int iTrack=0; iTrack < nTracks; iTrack++) {
    TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*) spectrometerEvent->GetCandidate(iTrack);
    double pTrack = track->GetMomentum();
    FillHisto("Tracks_All_pTrack", pTrack*1e-3);

    // Only positive tracks
    if (track->GetCharge() != 1) continue;
    FillHisto("Tracks_Positive_pTrack", pTrack*1e-3);

    // Track in all four STRAW chambers
    if (track->GetNChambers() != 4) continue;
    FillHisto("Tracks_FourChambers_pTrack", pTrack*1e-3);

    // Track quality cut
    double chi2Track = track->GetChi2();
    if (chi2Track > 20.0) continue;
    FillHisto("Tracks_chi2LT20_pTrack", pTrack*1e-3);  

    // Acceptance...
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kSpectrometer, 0)) continue;
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kSpectrometer, 1)) continue;
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kSpectrometer, 2)) continue;
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kSpectrometer, 3)) continue;
    FillHisto("Tracks_SpecAcceptance_pTrack", pTrack*1e-3);
    
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kCHOD)) continue;
    FillHisto("Tracks_CHODAcceptance_pTrack", pTrack*1e-3);
    
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kNewCHOD)) continue;
    FillHisto("Tracks_NewCHODAcceptance_pTrack", pTrack*1e-3);
    
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kLKr)) continue;
    FillHisto("Tracks_LKrAcceptance_pTrack", pTrack*1e-3);
    
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kMUV1)) continue;
    FillHisto("Tracks_MUV1Acceptance_pTrack", pTrack*1e-3);
    
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kMUV2)) continue;
    FillHisto("Tracks_MUV2Acceptance_pTrack", pTrack*1e-3);
    
    if (!GeometricAcceptance::GetInstance()->InAcceptance(track, kMUV3)) continue;
    FillHisto("Tracks_MUV3Acceptance_pTrack", pTrack*1e-3);
  
    // CHOD association
    SpectrometerCHODAssociationOutput spectrometerCHODAssociationOutput = spectrometerCHODAssociation[iTrack];     
    int nTracksCHODAssociated = spectrometerCHODAssociationOutput.GetNAssociationRecords();
    if (!nTracksCHODAssociated) continue;
    FillHisto("Tracks_CHODAssociated_pTrack", pTrack*1e-3);

    // NewCHOD association
    SpectrometerNewCHODAssociationOutput spectrometerNewCHODAssociationOutput = spectrometerNewCHODAssociation[iTrack];     
    int nTracksNewCHODAssociated = spectrometerNewCHODAssociationOutput.GetNAssociationRecords();
    if (!nTracksNewCHODAssociated) continue;
    FillHisto("Tracks_NewCHODAssociated_pTrack", pTrack*1e-3);    

    // LKr association
    SpectrometerLKrAssociationOutput spectrometerLKrAssociationOutput = spectrometerLKrAssociation[iTrack];     
    int nTracksLKrAssociated = spectrometerLKrAssociationOutput.GetNAssociationRecords();
    if (!nTracksLKrAssociated) continue;
    FillHisto("Tracks_LKrAssociated_pTrack", pTrack*1e-3);

    /*
    // Find the best (closest) NewCHOD hit
    int bestNewCHODHitIndex = spectrometerNewCHODAssociationOutput.GetBestMatchingRecordID();
    TRecoNewCHODHit *bestNewCHODHit = (TRecoNewCHODHit*)newCHODEvent->GetHit(bestNewCHODHitIndex);
    double newCHODHitTime = bestNewCHODHit->GetTime();

    // Find the cloest CEDAR candidate to the best NewCHOD hit
    int nCEDARCandidates = cedarEvent->GetNCandidates();
    int nGoodCEDARCandidates = 0;

    bestCEDARTime=-999.9;
    double lowestNewCHODCEDARdT=999.9;
    
    for (int iCEDARCandidate=0; iCEDARCandidate < nCEDARCandidates; iCEDARCandidate++) {
      TRecoCedarCandidate* cedarCandidate = (TRecoCedarCandidate*) cedarEvent->GetCandidate(iCEDARCandidate);
      if (cedarCandidate->GetNSectors() < 4) continue;
      nGoodCEDARCandidates++;
      double cedarTime = cedarCandidate->GetTime();
      double newCHODCEDARdT = cedarTime - newCHODHitTime;
      if (fabs(newCHODCEDARdT) < fabs(lowestNewCHODCEDARdT)) {
        bestCEDARTime = cedarTime;
        lowestNewCHODCEDARdT = newCHODCEDARdT;
      }
    }

    if (!nGoodCEDARCandidates) continue;
    FillHisto("Tracks_CEDARNewCHOD_TimeDifference", lowestNewCHODCEDARdT);
    
    // CEDAR NewCHOD timing
    if (fabs(lowestNewCHODCEDARdT) > 10.0) continue;
    FillHisto("Tracks_CEDARNewCHODTimeMatched_pTrack", pTrack*1e-3);
    */
    selectedTracks.push_back(iTrack);
    FillHisto("Tracks_Selected_pTrack", pTrack*1e-3);
  }
  nSelectedTracks = selectedTracks.size();
}

int Kl3Analysis::MatchGTK(double cedarTime) {
  double bestCEDARTime;
  double pSelectedTrack = selectedTrack->GetMomentum();
  FillHisto("test_Tracks_Selected_pTrack", pSelectedTrack*1e-3);
        
  // GTK matching

  int nGoodGTKTracks=0;
  bestGTKCandidateIndex=-1;
  double discriminantCEDAR = -1;
  double cda = 9999999.;

  myGigaTrackerAnalysis->Clear();
  myGigaTrackerAnalysis->SetTimeShift(0.);
  myGigaTrackerAnalysis->SetRecoTimingCut(1.1);
  myGigaTrackerAnalysis->SetMatchingTimingCut(-0.4,0.4);

  // Reconstruct GTK candidates
  if (isFilteredData) {
    nGoodGTKTracks = myGigaTrackerAnalysis->ReconstructCandidates(gigatrackerEvent, cedarTime, GetEventHeader()->GetTimeStamp(),1,isMC); // Remove previous candidates !
  } else {
    nGoodGTKTracks = myGigaTrackerAnalysis->ReconstructCandidates(gigatrackerEvent, cedarTime, GetEventHeader()->GetTimeStamp(),1,isMC); // Remove previous candidates !
  }
  FillHisto("Multiplicity_GTKCandidates", nGoodGTKTracks);

  // Find and test the best candidate for one track <--------------------- !!!!!!!!!!
  bestGTKCandidateIndex = myGigaTrackerAnalysis->TrackCandidateMatching(gigatrackerEvent,selectedTrack,bestCEDARTime,0,nGoodGTKTracks);
  cout <<  "best gtk index" << bestGTKCandidateIndex << endl;
  if (bestGTKCandidateIndex < 0) return 0;
  FillHisto("SelectedTrack_GTKMatched_pTrack", pSelectedTrack*1e-3);
    
  TRecoGigaTrackerCandidate* gtkCandidate = (TRecoGigaTrackerCandidate*) gigatrackerEvent->GetCandidate(bestGTKCandidateIndex);
  double gtkTime = gtkCandidate->GetTime();
  double gtkCEDARdT  = gtkTime - cedarTime;
    
  FillHisto("BestMatchingGTKCandidate_CEDARGTK_TimeDifference", gtkCEDARdT);
  if (gtkCEDARdT > 0.5) return 0;
  FillHisto("SelectedTrack_CEDARGTKTime_pTrack", pSelectedTrack*1e-3);

  TVector3 vertex = myGigaTrackerAnalysis->ComputeVertex(gtkCandidate,selectedTrack,&cda);
  discriminantCEDAR = myGigaTrackerAnalysis->Discriminant(cda,gtkCEDARdT);

  FillHisto("BestMatchingGTKCandidate_CEDARGTK_Discriminant", discriminantCEDAR);    
  if (discriminantCEDAR <= 0.01) return 0;
  FillHisto("SelectedTrack_GTKDiscriminant_pTrack", pSelectedTrack*1e-3);

  double zVertex = vertex.z();
  FillHisto("BestMatchingGTKCandidate_zVertex", zVertex*1e-3);
  if (zVertex < 110000 || zVertex > 180000) return 0;
  FillHisto("SelectedTrack_zVertex_pTrack", pSelectedTrack*1e-3);

  FillHisto("BestMatchingGTKCandidate_CDA", cda);
  if (cda > 10) return 0;
  FillHisto("SelectedTrack_CDA_pTrack", pSelectedTrack*1e-3);

  return 1;

  //cout << "GTK index:   " << matchedGTKTracks.at(0) << endl;
  //cout << "STRAW index: " << selectedTracks.at(0) << endl;
  // Particles and kinematics
}

int Kl3Analysis::SelectKmu3Candidates() {

  // Kinematics

  // MUV3 associated candidates; exactly one, or just at least one?

  // MUV 1/2 energy

  // EoP

  // Pt

  // SpectrometerCalorimeterAssociation
 
}

int Kl3Analysis::SelectKe3Candidates() {
  double bestCEDARTime;

  // Check that there are no associated MUV3 candidates within a 5ns window
  for (int i=0; i<nAssociatedMUV3Candidates; i++) {
    SpectrometerMUV3AssociationRecord *muv3Record = (SpectrometerMUV3AssociationRecord *)spectrometerMUV3AssociationOutput.GetAssociationRecord(i);
    double muonTime = muv3Record->GetMuonTime();
    if (fabs(bestCEDARTime-muonTime) < 5) return 0;
  }
  FillHisto("OneTrack_beamKPositron_mm2_MUV3", mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_beamKPositron_pVmm2_MUV3", pOneTrack*1e-3, mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_mm2_MUV3", mm2GTKKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_pVmm2_MVU3", pOneTrack*1e-3, mm2GTKKaonPositron*1e-6);

  FillHisto("OneTrack_Ke3_MUV1Energy", muv1Energy*1e-3);
  FillHisto("OneTrack_Ke3_MUV2Energy", muv2Energy*1e-3);

  if ((muv1Energy+muv2Energy) > 5000) return 0;

  FillHisto("OneTrack_beamKPositron_mm2_MUV12Energy", mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_beamKPositron_pVmm2_MUV12Energy", pOneTrack*1e-3, mm2BeamKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_mm2_MUV12Energy", mm2GTKKaonPositron*1e-6);
  FillHisto("OneTrack_gtkKPositron_pVmm2_MUV12Energy", pOneTrack*1e-3, mm2GTKKaonPositron*1e-6);

  return 1;
  
  // MUV3, no candidates at all, or no associated candidates?

  // Kinematics

  // MUV 1/2 energy

  // EoP
 
}

int Kl3Analysis::SelectK2piCandidates() {

  // Kinematics

  // MUV3 associated candidates

  // MUV 1/2 energy

  // EoP

  // Pt

  // SpectrometerCalorimeterAssociation
 
}

/*
void Kl3Analysis::SelectTracks() {
  // CEDAR, GTK matching...

  selectedTracks.clear();
  matchedGTKTracks.clear();

  for (int iPreselectedTrack=0; iPreselectedTrack < nPreselectedTracks; iPreselectedTrack++) {
    int preSelectedTrackIndex = preSelectedTracks.at(iPreselectedTrack);
    TRecoSpectrometerCandidate* preselectedTrack = (TRecoSpectrometerCandidate*) spectrometerEvent->GetCandidate(preSelectedTrackIndex);
    double pPreselectedTrack = preselectedTrack->GetMomentum();
    
    

    
        
    // GTK matching

    int nGoodGTKTracks=0;
    int bestGTKCandidateIndex=-1;
    double discriminantCEDAR = -1;
    double cda = 9999999.;

    myGigaTrackerAnalysis->Clear();
    myGigaTrackerAnalysis->SetTimeShift(0.);
    myGigaTrackerAnalysis->SetRecoTimingCut(1.1);
    myGigaTrackerAnalysis->SetMatchingTimingCut(-0.4,0.4);

    // Reconstruct GTK candidates
    if (isFilteredData) {
      nGoodGTKTracks = myGigaTrackerAnalysis->ReconstructCandidates(gigatrackerEvent, bestCEDARTime, GetEventHeader()->GetTimeStamp(),1,isMC); // Remove previous candidates !
    } else {
      nGoodGTKTracks = myGigaTrackerAnalysis->ReconstructCandidates(gigatrackerEvent, bestCEDARTime, GetEventHeader()->GetTimeStamp(),1,isMC); // Remove previous candidates !
    }
    FillHisto("Multiplicity_GTKCandidates", nGoodGTKTracks);

    // Find and test the best candidate for one track <--------------------- !!!!!!!!!!
    bestGTKCandidateIndex = myGigaTrackerAnalysis->TrackCandidateMatching(gigatrackerEvent,preselectedTrack,bestCEDARTime,0,nGoodGTKTracks);
    if (bestGTKCandidateIndex < 0) continue;
    FillHisto("PreselectedTracks_GTKMatched_pTrack", pPreselectedTrack*1e-3);
    
    TRecoGigaTrackerCandidate* gtkCandidate = (TRecoGigaTrackerCandidate*) gigatrackerEvent->GetCandidate(bestGTKCandidateIndex);
    double gtkTime = gtkCandidate->GetTime();
    double gtkCEDARdT  = gtkTime - bestCEDARTime;
    
    FillHisto("BestMatchingGTKCandidate_CEDARGTK_TimeDifference", gtkCEDARdT);
    if (gtkCEDARdT > 0.5) continue;
    FillHisto("PreselectedTracks_CEDARGTKTime_pTrack", pPreselectedTrack*1e-3);

    TVector3 vertex = myGigaTrackerAnalysis->ComputeVertex(gtkCandidate,preselectedTrack,&cda);
    discriminantCEDAR = myGigaTrackerAnalysis->Discriminant(cda,gtkCEDARdT);

    FillHisto("BestMatchingGTKCandidate_CEDARGTK_Discriminant", discriminantCEDAR);    
    if (discriminantCEDAR <= 0.01) continue;
    FillHisto("PreselectedTracks_GTKDiscriminant_pTrack", pPreselectedTrack*1e-3);

    double zVertex = vertex.z();
    FillHisto("BestMatchingGTKCandidate_zVertex", zVertex*1e-3);
    if (zVertex < 110000 || zVertex > 180000) continue;
    FillHisto("PreselectedTracks_zVertex_pTrack", pPreselectedTrack*1e-3);

    FillHisto("BestMatchingGTKCandidate_CDA", cda);
    if (cda > 10) continue;
    FillHisto("PreselectedTracks_CDA_pTrack", pPreselectedTrack*1e-3);

    selectedTracks.push_back(preSelectedTrackIndex);
    matchedGTKTracks.push_back(bestGTKCandidateIndex);

    //int type = GTK->GetType();
    //double gtkchi2X = GTK->GetChi2X();
    //double gtkchi2Y = GTK->GetChi2Y();
    //double gtkchi2T = GTK->GetChi2Time();
    //double gtkchi2  = GTK->GetChi2();
    

    /*

    TVector3 gtkpos= GTK->GetPosition(2);
    TVector3 gtkpos1= GTK->GetPosition(0);
    TVector3 gtkpos2= GTK->GetPosition(1);
    TVector3 gtkmomentum= GTK->GetMomentum();
    TLorentzVector Pgtk(gtkmomentum, MKCH); //check if the mass is right (its MeV now)

    

    //GigaTrackerAnalysisCHOD->SetReferenceDetector(1);
    //GigaTrackerAnalysisCHOD->DiscriminantNormalization();
    //discr_chod  = GigaTrackerAnalysisCHOD->Discriminant(fcda,gtkchoddt);

    FillHisto("OneTrack_IsGTK_P", gtkmomentum.Mag());
    //FillHisto("OneTrack_IsGTK_richdt",  gtkrichdt);
    //FillHisto("OneTrack_IsGTK_choddt",  gtkchoddt);
    FillHisto("OneTrack_IsGTK_cedardt",  gtkCEDARdT);
    FillHisto("OneTrack_IsGTK_chi2X", gtkchi2X);
    FillHisto("OneTrack_IsGTK_chi2Y", gtkchi2Y);
    FillHisto("OneTrack_IsGTK_chi2T", gtkchi2T);
    FillHisto("OneTrack_IsGTK_chi2", gtkchi2);
    FillHisto("OneTrack_IsGTK_pos", gtkpos.X(),gtkpos.Y());

    double dx12 = gtkpos1.X() - gtkpos2.X();
    double dx23 = gtkpos2.X() - gtkpos.X() ;
    double dx13 = gtkpos1.X() - gtkpos.X() ;
    double dy12 = gtkpos1.Y() - gtkpos2.Y();
    double dy23 = gtkpos2.Y() - gtkpos.Y() ;
    double dy13 = gtkpos1.Y() - gtkpos.Y() ;
    double thetax= dx13/ (gtkpos1.Z() - gtkpos.Z());
    double thetay = dy13/ (gtkpos1.Z() - gtkpos.Z());
    FillHisto("OneTrack_IsGTK_dX_12",dx12 );
    FillHisto("OneTrack_IsGTK_dX_23",dx23 );
    FillHisto("OneTrack_IsGTK_dX_13",dx13 );
    FillHisto("OneTrack_IsGTK_dY_12",dy12 );
    FillHisto("OneTrack_IsGTK_dY_23",dy23 );
    FillHisto("OneTrack_IsGTK_dY_13",dy13 );
    FillHisto("OneTrack_IsGTK_thetax", thetax);
    FillHisto("OneTrack_IsGTK_thetay", thetay);
    //FillHisto("OneTrack_IsGTK_discriminant",discr_best);
    //FillHisto("OneTrack_IsGTK_dt_vs_cda",gtkrichdt, fcda);
    //FillHisto("OneTrack_IsGTK_discriminant_chod",discr_chod);
    //FillHisto("OneTrack_IsGTK_dtchod_vs_cda",gtkchoddt, fcda);
    //FillHisto("OneTrack_IsGTK_discr_rich_vs_cedar",discriminantCEDAR,discr_best);
    FillHisto("OneTrack_IsGTK_discriminant_cedar",discriminantCEDAR);

    FillHisto("OneTrack_IsGTK_dtcedar_vs_cda",gtkCEDARdT, fcda);


    //if(fabs(gtkCEDARdT) > 0.5) return 0;
    
    //if(discr_best <= 0.01) return 0;

    //FillHisto("OneTrack_IsGTK_dtcedar_vs_cda_good",gtkCEDARdT , fcda);
    //FillHisto("OneTrack_IsGTK_dt_vs_cda_good",gtkrichdt, fcda);
    //FillHisto("OneTrack_IsGTK_dtchod_vs_cda_good",gtkchoddt, fcda);
  
    if(discriminantCEDAR <= 0.01) return;

    FillHisto("OneTrack_IsGTK_dtcedar_vs_cda_final",gtkCEDARdT, fcda);
    //FillHisto("OneTrack_IsGTK_dt_vs_cda_final",gtkrichdt, fcda);

   // if(discr_chod <= 0.005) return 0;
    //FillHisto("OneTrack_IsGTK_dtchod_vs_cda_final",gtkchoddt, fcda);
    //if(fcda > 10) return 0;

  /////////////////////////////////////////
  // Zvertex & CDA: track wrt the beam axis

  //Double_t cda  = Tracks[0].GetNominalBeamAxisCDA();
  //Double_t zVertex = Tracks[0].GetNominalBeamAxisVertex().Z();

  FillHisto("hZvtx", 0.001*vertex.Z());
  FillHisto("hCDA", fcda);
  FillHisto("hzVertexCDA", 0.001*vertex.Z(), fcda);

  Bool_t pas_zvtx = (vertex.Z()>110000 && vertex.Z()<180000);
  Bool_t pas_cda  = (fcda<10);

  if (!pas_zvtx) return;
  if (!pas_cda) return;
  

  }
  nSelectedTracks = selectedTracks.size();
}
*/