#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "KaonTrackAnalysis.hh"
#include "EventHeader.hh"
#include "L0TPData.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "AnalysisTools.hh"
//#include "utl.hh"
#include "Constants.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "CalorimeterCluster.hh"
#include "MultiTrackAnal.hh"
#include "LAVMatching.hh"
#include "L0RICHEmulator.hh"
#include "L0NewCHODEmulator.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


//Variables for early decay suppression
Double_t fxPaipD;
Double_t fyPaipD;
Double_t fxKaonD;
Double_t fyKaonD;
Double_t fxprimePaip;
Double_t fyprimePaip;
Double_t fxprimeKaon;
Double_t fyprimeKaon;
Double_t fpPaip;
Double_t fpKaon;

KaonTrackAnalysis::KaonTrackAnalysis(Core::BaseAnalysis *ba) : Analyzer(ba, "KaonTrackAnalysis") {
    fAnalyzerName = "KaonTrackAnalysis";
    RequestL0Data();
    RequestTree("Cedar",new TRecoCedarEvent);
    RequestTree("GigaTracker",new TRecoGigaTrackerEvent);
    RequestTree("CHANTI",new TRecoCHANTIEvent);
    RequestTree("Spectrometer",new TRecoSpectrometerEvent);
    RequestTree("LAV",new TRecoLAVEvent);
    RequestTree("RICH",new TRecoRICHEvent);
    RequestTree("CHOD",new TRecoCHODEvent);
    RequestTree("NewCHOD",new TRecoNewCHODEvent);
    RequestTree("IRC",new TRecoIRCEvent);
    RequestTree("LKr",new TRecoLKrEvent);
    RequestTree("MUV0",new TRecoMUV0Event);
    RequestTree("MUV1",new TRecoMUV1Event);
    RequestTree("MUV2",new TRecoMUV2Event);
    RequestTree("MUV3",new TRecoMUV3Event);
    RequestTree("SAC",new TRecoSACEvent);
    RequestTree("SAV",new TRecoSAVEvent);
    RequestTree("HAC",new TRecoHACEvent);

    // Analysis tools
    fTool = AnalysisTools::GetInstance();

    // Pointer to physics objects
    // fStrawAnalysis = new StrawRapper(1,ba);
    fLKrAnal = new LKrAnal(3,ba);
    fCHODAnal = new CHODAnal(1,ba);
    fStrawAnalysis = new StrawRapper(1,ba,fCHODAnal,fLKrAnal);
    fLAVAnal = new LAVAnal(0,ba);
    fIRCAnal = new IRCAnal(ba);
    fSACAnal = new SACAnal(ba);
    fSAVAnal = new SAVAnal(ba);
    fLAVMatching = new LAVMatching();
    fCaloArray = new TClonesArray("CalorimeterCluster",20);
    //NA62Analysis::Core::HistoHandler::Mkdir(fAnalyzerName,"MultiplicityRejection");
    for(int j(0);j<9;j++)
        fMultAnal[j] = new MultiTrackAnal(j,"MultiplicityRejection",ba);


    // fMultAnalpnn    = new MultiTrackAnal(0,"MultiplicityRejection",ba);
    // fMultAnalmu     = new MultiTrackAnal(1,"MultiplicityRejection",ba);
    // fMultAnalmuctrl = new MultiTrackAnal(2,"MultiplicityRejection",ba);
    // fMultAnalpi     = new MultiTrackAnal(3,"MultiplicityRejection",ba);
    // fMultAnalpictrl = new MultiTrackAnal(4,"MultiplicityRejection",ba);

    // Common variables
    fState = kOValid;
    fFineTimeScale = 24.951059536/256.;
    fYear = 2016;
    xSTRAW_station[0] = 101.2;
    xSTRAW_station[1] = 114.4;
    xSTRAW_station[2] = 92.4;
    xSTRAW_station[3] = 52.8; // [ mm ]

    // Minuit initialization
    fNPars = 1;
    fFitter = new TMinuit(fNPars);
    fFitter->SetFCN(FuncCDA);

}

void KaonTrackAnalysis::InitOutput(){
}

void KaonTrackAnalysis::InitHist(){

    //////////////////////////////////////////////////////
    // Setup RICH emulator for 2017 data.
    //////////////////////////////////////////////////////
    // 1.  coincidence window of RICH firmware
    ReconfigureAnalyzer("L0RICHEmulator", "L0Window", 128*TdcCalib); //12.5 ns
    // 2. FineTimeBit used in the L0TP (RAM segment size)
    ReconfigureAnalyzer("L0RICHEmulator", "FineTimeBit", 1);
    // 3. Multiplicity cut in the PP (clusters are removed if N<PPThreshold)
    ReconfigureAnalyzer("L0RICHEmulator", "PPThreshold", 3);
    // 4. Multiplicity cut in the SL (clusters are removed if N<SLThreshold)
    ReconfigureAnalyzer("L0RICHEmulator", "SLThreshold", 3);

    BookHisto(new TH2F("RandomPhotonVeto",";RunID;PhotonRVeto",2000,6000,8000.,400,0,1));
    BookHisto(new TH2F("TotalRandomVeto",";RunID;FullRVeto",2000,6000,8000.,400,0,1));
    BookHisto(new TH2F("Norm_before_finetiming",";BurstID;RunID",3000,0,3000,2000,6000,8000.));
    BookHisto(new TH2F("Norm_events",";BurstID;RunID",3000,0,3000,2000,6000,8000.));
    BookHisto(new TH2F("Pipi_events",";BurstID;RunID",3000,0,3000,2000,6000,8000.));
    BookHisto(new TH2F("Pipipi_events",";BurstID;RunID",3000,0,3000,2000,6000,8000.));
    BookHisto(new TH2F("Munu_events",";BurstID;RunID",3000,0,3000,2000,6000,8000.));

    BookHisto(new TH2F("P1520_Norm_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P1520_Pipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P1520_Pipipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P1520_Munu_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));

    BookHisto(new TH2F("P2025_Norm_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P2025_Pipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P2025_Pipipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P2025_Munu_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));


    BookHisto(new TH2F("P2530_Norm_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P2530_Pipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P2530_Pipipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P2530_Munu_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));

    BookHisto(new TH2F("P3035_Norm_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P3035_Pipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P3035_Pipipi_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));
    BookHisto(new TH2F("P2025_Munu_events",";BurstID;RunID",2000,0,2000,2000,6000,8000.));

    BookHisto(new TH2F("Munu_kinres_vs_p","",200,0,100,500,-0.05,0.05));


    // Preliminary
    BookHisto(new TH1F("onetrack_dttrack","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtchod","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtlkr","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtktag","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtgtk","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtrichm","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtrichs","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtmuv1","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_dtmuv2","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindttrack","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtchod","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtlkr","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtktag","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtgtk","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtrichm","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtrichs","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtmuv1","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_mindtmuv2","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddttrack","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtchod","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtlkr","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtktag","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtgtk","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtrichm","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtrichs","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtmuv1","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_gooddtmuv2","",100,-25.,25.));
    BookHisto(new TH1F("onetrack_chekid","",10,0,10.));

    BookHisto(new TH2F("Alltrigg_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH1I("Qtrack","",10,-5,5));


    if(GetWithMC()) BookHisto(new TH2F("NRICHHits_vs_Q","",50,0,50,4,-2,2));
    BookHisto(new TH2F("RICH_dt_vs_Q","",400,-10,10,4,-2,2));
    BookHisto(new TH2I("RICH_IsPrim_vs_Q","",3,0,3,4,-2,2));

    BookHisto(new TH1I("RICHIsSingle","",2,0,2));
    BookHisto(new TH1I("RICHIsMulti","",2,0,2));
    BookHisto(new TH2I("IsCHANTI_vs_Charge","",2,0,2,4,-2,2));
    BookHisto(new TH2I("Pimin_IsCHANTI_vs_Charge","",2,0,2,4,-2,2));

    BookHisto(new TH1F("Pinunu_kaon_p","",100,70,80));
    BookHisto(new TH2F("Pinunu_kaon_thx_vs_pk","",100,70,80,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_kaon_thy_vs_pk","",100,70,80,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_kaon_thetay_vs_thetax","",50,-0.001,0.001,50,-0.001,0.001));
    BookHisto(new TH1F("Pinunu_kaon_thetax","",50,-0.001,0.001));
    BookHisto(new TH1F("Pinunu_kaon_thetay","",50,-0.001,0.001));

    BookHisto(new TH2F("Ctrl_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Ctrl_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Ctrl_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Ctrl_xvtx_vs_zvtx","",250,50000,200000,1000,-500,500));
    BookHisto(new TH2F("Ctrl_yvtx_vs_zvtx","",250,50000,200000,1000,-500,500));
    BookHisto(new TH2F("Ctrl_yvtx_vs_xvtx_front","",1000,-500,500,1000,-500,500));
    BookHisto(new TH2F("Ctrl_yvtx_vs_xvtx_end","",1000,-500,500,1000,-500,500));

    BookHisto(new TH2F("Snake_ctrl_r_vs_z","",250,50000,200000,250.,0.,1000.));
    BookHisto(new TH2F("Snake_ctrl_r_vs_z_1535","",250,50000,200000,250.,0.,1000.));
    BookHisto(new TH2F("Snakepos_ctrl_r_vs_z","",250,50000,200000,250.,0.,1000.));
    BookHisto(new TH2F("Snake_ctrl_r_vs_z_aftercut","",250,50000,200000,250.,0.,1000.));

    BookHisto(new TH2F("Ctrl_nobeambg_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_nobeambg_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Ctrl_nobeambg_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_nobeambg_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Ctrl_nobeambg_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Ctrl_notails_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_notails_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Ctrl_notails_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_notails_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Ctrl_notails_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Ctrl_pidrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_pidrej_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Ctrl_pidrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_pidrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Ctrl_pidrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Ctrl_ibvsmmiss_after_particleid","",375,-0.1,0.14375,2000,0,200));
    BookHisto(new TH2F("Ctrl_ibvsmmiss_test_3pi","",375,-0.1,0.14375,2000,0,200));

    BookHisto(new TH2F("Ctrl_richfull_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_richfull_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Ctrl_richfull_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_richfull_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Ctrl_richfull_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Ctrl_richfull_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Ctrl_mult_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));

    BookHisto(new TH2F("Norm_kinematics_mmiss_vs_p_nogtk","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Norm_kinematics_mmiss_vs_phi_nogtk","",320,0,6.4,375,-0.1,0.14375));
    BookHisto(new TH2F("Norm_kinematics_mmiss_vs_p_signal","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Norm_kinematics_mmiss_vs_phi_signal","",320,0,6.4,375,-0.1,0.14375));

    BookHisto(new TH2F("Ctrl_L0RICH","",200,0,100,2,0,2));
    BookHisto(new TH2F("Ctrl_L0MUV3","",200,0,100,2,0,2));
    BookHisto(new TH2F("Ctrl_L0NewCHOD","",200,0,100,2,0,2));
    BookHisto(new TH2F("Ctrl_L0Qx","",200,0,100,2,0,2));
    BookHisto(new TH2F("Ctrl_L0UMTC","",200,0,100,2,0,2));
    BookHisto(new TH2F("//                         Ctrl_L0_nocal_nolav","",200,0,100,2,0,2));

//     BookHisto(new TH2F("Ctrl_LAVTE_gamma1_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_LAVTE_gamma2_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH1F("Ctrl_LAVTE_gamma_distance","",500,0,1000));
//     BookHisto(new TH2F("Ctrl_LAVTE_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_LAVTE_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_L0LAV","",200,0,100,2,0,2));

//     BookHisto(new TH1F("Ctrl_nlavcand","",70,0,70));
//     BookHisto(new TH1F("Ctrl_nlavintime","",70,0,70));

//     BookHisto(new TH1F("Ctrl_1lav_dtlav","",200,-5,5));
//     BookHisto(new TH1F("Ctrl_1lav_LAVID_all","",13,0,13));

//     BookHisto(new TH1F("Ctrl_1lav_LAVID","",13,0,13));

//     BookHisto(new TH2F("Ctrl_1lav_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lav_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lav_ringrad_vs_p","",200,0.,100.,300,0,300));
//     BookHisto(new TH2F("Ctrl_1lav_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Ctrl_1lav_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_1lav_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH1F("Ctrl_1lav_nlavintime","",70,0,70));

//     if(GetWithMC() ){
//         BookHisto(new TH2F("Ctrl_1lav_mcg1_xend_vs_yend","",240,-1200,1200,240,-1200,1200));
//         BookHisto(new TH2F("Ctrl_1lav_mcg1_zend_vs_yend","",250,50000,300000,240,-1200,1200));
//         BookHisto(new TH2F("Ctrl_1lav_mcg1_zend_vs_xend","",250,50000,300000,240,-1200,1200));
//         BookHisto(new TH2F("Ctrl_1lav_mcg2_xend_vs_yend","",240,-1200,1200,240,-1200,1200));
//         BookHisto(new TH2F("Ctrl_1lav_mcg2_zend_vs_xend","",250,50000,300000,240,-1200,1200));
//         BookHisto(new TH2F("Ctrl_1lav_mcg2_zend_vs_yend","",250,50000,300000,240,-1200,1200));
//     }

//     BookHisto(new TH2F("Ctrl_1lavp_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lavp_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));

//     BookHisto(new TH2F("Ctrl_1lavirc_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lavirc_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lavirc_mveto_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lavsac_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lavsac_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_1lavsac_mveto_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));

//     BookHisto(new TH2F("Ctrl_2lavs0_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_2lavs0_mmiss_vs_p_lkr","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_2lavs0_mmiss_vs_p_sav","",200,0,100,375,-0.1,0.14375));

//     BookHisto(new TH2F("Ctrl_2lavs1_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_2lavs1_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_2lavs1_ringrad_vs_p","",200,0.,100.,300,0,300));
//     BookHisto(new TH2F("Ctrl_2lavs1_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Ctrl_2lavs1_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_2lavs1_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_L0Calo","",400,0,100,2,0,2));

//     BookHisto(new TH2F("Ctrl_RPilkr_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_RPilkr_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_RPilkr_pi_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_RPilkr_gamma1_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_RPilkr_gamma2_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH1F("Ctrl_RPilkr_dgamma","",500,0,1000));
//     BookHisto(new TH1F("Ctrl_RPilkr_dgamma1","",500,0,1000));
//     BookHisto(new TH1F("Ctrl_RPilkr_dgamma2","",500,0,1000));

//     BookHisto(new TH2F("Ctrl_RPilkr1_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_RPilkr1_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_RPilkr1_pi_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_RPilkr1_gamma1_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_RPilkr1_gamma2_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH1F("Ctrl_RPilkr1_dgamma","",500,0,1000));
//     BookHisto(new TH1F("Ctrl_RPilkr1_dgamma1","",500,0,1000));
//     BookHisto(new TH1F("Ctrl_RPilkr1_dgamma2","",500,0,1000));

//     BookHisto(new TH2F("Ctrl_mmiss_vs_p_lkr","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_mmiss_vs_p_auxlkr","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_mmiss_vs_p_irc","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_mmiss_vs_p_lav","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_mmiss_vs_p_sac","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_mmiss_vs_p_sav","",200,0,100,375,-0.1,0.14375));

//     BookHisto(new TH2F("Ctrl_phrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_phrej_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_phrej_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Ctrl_phrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_phrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_phrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

//     BookHisto(new TH2F("Ctrl_multirej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_multirej_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_multirej_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Ctrl_multirej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_multirej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_multirej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Ctrl_multirej_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
//     BookHisto(new TH2F("Ctrl_multirej_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

//     BookHisto(new TH2F("Pinunu_start_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_start_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_start_dtchod_vs_cda","",400, -2, 2, 40, 0,10));

//     BookHisto(new TH2F("Pinunu_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Pinunu_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Pinunu_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

//     BookHisto(new TH2F("Pinunu_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

//     BookHisto(new TH2F("Pinunu_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));


//     BookHisto(new TH1F("Pinunu_NGTKCand","",50,0,50));

//     // Multiple gtk rejection
//     BookHisto(new TH2F("MultiGTK_zvtxvsp","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("MultiGTK_zvsz","",250,50000,200000,250,50000,200000));
//     BookHisto(new TH2F("MultiGTK_mvsdz","",250,-10000,10000,375,-0.1,0.14375));
//     BookHisto(new TH2F("MultiGTK_mvsm","",375,-0.1,0.14375,375,-0.1,0.14375));
//     BookHisto(new TH2F("MultiGTKHit_dvsz","",250,50000,200000,800,0.,400.));
//     BookHisto(new TH2F("MultiGTKHit_dvsz2","",250,50000,200000,800,0.,400.));
//     BookHisto(new TH2F("MultiGTKHit_dxdy","",800,-200.,200.,800,-200,200.));
//     BookHisto(new TH2F("MultiGTKHit_xytrack_gtk3","",400,-100.,100.,400,-100,100.));
//     BookHisto(new TH2F("MultiGTKHit_xytrack_fcoe","",400,-100.,100.,400,-100,100.));
//     BookHisto(new TH2F("MultiGTKHit_xytrack_dipo","",400,-100.,100.,400,-100,100.));
//     BookHisto(new TH2F("MultiGTKHit_xy","",400,-100.,100.,400,-100,100.));
//     BookHisto(new TH2F("MultiGTKHit_tot","",250,50000,200000,200,0.,50.));

//     BookHisto(new TH2F("Pinunu_badgtkmatching_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Snake_r_vs_z","",250,50000,200000,250.,0.,1000.));
//     BookHisto(new TH2F("Snake_r_vs_z_1535","",250,50000,200000,250.,0.,1000.));
//     BookHisto(new TH2F("Snakepos_r_vs_z","",250,50000,200000,250.,0.,1000.));
//     BookHisto(new TH2F("Snake_r_vs_z_aftercut","",250,50000,200000,250.,0.,1000.));
//     //BookHisto(new TH2F("NoSnakepos_rvsz","",250,50000,200000,250.,0.,1000.));

//     BookHisto(new TH2F("Pinunu_fidvolout_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_fidvolout_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Pinunu_fidvolout_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Pinunu_fidvolout_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

//     BookHisto(new TH2F("Pinunu_fidvol_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_fidvol_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Pinunu_fidvol_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Pinunu_fidvol_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

//     BookHisto(new TH2F("Pinunu_chanti_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_chanti_dtrich_vs_p","",200,0,100,400,-5,5));
//     BookHisto(new TH2F("Pinunu_chanti_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Pinunu_chanti_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Pinunu_chanti_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

//     BookHisto(new TH2F("Pinunu_nobeambg_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_nobeambg_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Pinunu_nobeambg_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_nobeambg_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Pinunu_nobeambg_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

//     BookHisto(new TH2F("Pinunu_nobeambg_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_nobeambg_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_nobeambg_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

//     BookHisto(new TH2F("Pinunu_nobeambg_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_nobeambg_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
//     BookHisto(new TH2F("Pinunu_nobeambg_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

//     // Kinematic tails
//     BookHisto(new TH2F("Pinunu_tails_spec_nhits","",200,0,100,100,0,100));
//     BookHisto(new TH2F("Pinunu_tails_spec_dthetax","",100,-0.001,0.001,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_tails_spec_dthetay","",100,-0.001,0.001,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_tails_spec_qual1","",100,0,10,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_tails_vertex_xz","",250,50000,200000,200,-200,200));
//     BookHisto(new TH2F("Pinunu_tails_vertex_yz","",250,50000,200000,200,-200,200));
//     BookHisto(new TH2F("Pinunu_tails_kaon_xy","",50,-50,50,50,-50,50));
//     BookHisto(new TH2F("Pinunu_tails_thetavsp","",200,0,100,200,0.,0.02));
//     BookHisto(new TH2F("Pinunu_tails_dktagVSdgtk_chod","",200,-10,10,200,-10,10));
//     BookHisto(new TH2F("Pinunu_tails_dchodVSdktag_gtk","",200,-10,10,200,-10,10));
//     BookHisto(new TH2F("Pinunu_tails_dtktag","",100,-10,10,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_tails_dtchod","",100,-10,10,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_tails_dtlkr","",100,-20,20,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_tails_dtspec","",100,-50,50,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_tails_dktagVSdgtk_chod_check","",200,-10,10,200,-10,10));
//     BookHisto(new TH2F("Pinunu_tails_kaon_xy_check","",200,-50,50,200,-50,50));
//     BookHisto(new TH1F("Pinunu_tails_kaon_p_check","",100,70,80));
//     BookHisto(new TH2F("Pinunu_tails_kaon_thetaxy_check","",50,-0.001,0.001,50,-0.001,0.001));

//     BookHisto(new TH2F("Pinunu_notails_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_notails_p_vs_zvtx","",250,50000,200000,200,0,100));
//     BookHisto(new TH2F("Pinunu_notails_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
//     BookHisto(new TH2F("Pinunu_notails_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
//     BookHisto(new TH2F("Pinunu_notails_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_notails_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_notails_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_notails_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_notails_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_notails_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_notails_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_pid_positron_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pid_positron_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_pid_positron_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_pid_positron_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_pid_nopositron_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pid_nopositron_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_pid_nopositron_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_pid_nopositron_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_pid_nomuon_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pid_nomuon_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_pid_nomuon_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_pid_nomuon_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_pid_muonid_mmiss_vs_eop","",120,0,1.2,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pid_muonid_mmiss_vs_eop_nomuv3","",120,0,1.2,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_pid_muon_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pid_muon_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_pid_muon_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_pid_muon_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_pidrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pidrej_eop_vs_p","",200,0,100,120,0,1.2));
    BookHisto(new TH2F("Pinunu_pidrej_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_pidrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pidrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_pidrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_pidrej_proton_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_pidrej_proton_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_ibvsmmiss_after_particleid","",375,-0.1,0.14375,2000,0,200));
    BookHisto(new TH2F("Pinunu_ibvsmmiss_test_3pi","",375,-0.1,0.14375,2000,0,200));

    BookHisto(new TH2F("Pinunu_segments_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_richsingle_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    // Preliminary
    BookHisto(new TH1F("Pinunu_timing_dtchodrich","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtchodcedar","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtchodgtk","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtcedarrich","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtcedargtk","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtrichgtk","",200,-5.,5.));
    BookHisto(new TH2F("Pinunu_timing_dtchodrich_dtchodcedar","",200,-5.,5.,200,-5.,5.));
    BookHisto(new TH2F("Pinunu_timing_dtrichcedar_dtrichgtk","",200,-5.,5.,200,-5.,5.));
    BookHisto(new TH2F("Pinunu_timing_dtcedarrich_dtcedarchod","",200,-5.,5.,200,-5.,5.));

    BookHisto(new TH1F("Pinunu_timing_discriminant","",400,0,200.));
    BookHisto(new TH1F("Pinunu_timing_dttrack","",200,-20.,20.));
    BookHisto(new TH1F("Pinunu_timing_dtchod","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtlkr","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtktag","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtgtk","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_timing_dtrichs","",200,-5.,5.));

    BookHisto(new TH1F("Pinunu_goodtiming_dtchodrich","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_goodtiming_dtchodcedar","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_goodtiming_dtchodgtk","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_goodtiming_dtcedarrich","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_goodtiming_dtcedargtk","",200,-5.,5.));
    BookHisto(new TH1F("Pinunu_goodtiming_dtrichgtk","",200,-5.,5.));
    BookHisto(new TH2F("Pinunu_goodtiming_dtchodrich_dtchodcedar","",200,-5.,5.,200,-5.,5.));
    BookHisto(new TH2F("Pinunu_goodtiming_dtrichcedar_dtrichgtk","",200,-5.,5.,200,-5.,5.));
    BookHisto(new TH2F("Pinunu_goodtiming_dtcedarrich_dtcedarchod","",200,-5.,5.,200,-5.,5.));

    BookHisto(new TH1F("Pinunu_singlepi_lr","",500,0,100));
    BookHisto(new TH1F("Pinunu_nrichrings","",30,0,30));
    BookHisto(new TH1F("Pinunu_richpim_nrichrings","",30,0,30));
    BookHisto(new TH2F("Pinunu_richpim_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_richpim_sringmass_vs_p","",200,0,100,500,0,1.));
    BookHisto(new TH2F("Pinunu_richpim_mringmass_vs_p","",200,0,100,500,0,1.));
    BookHisto(new TH2F("Pinunu_richpim_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_richpis_eop_vs_p","",200,0,100,120,0.,1.2));
    BookHisto(new TH2F("Pinunu_richpis_ecalo_vs_p","",200,0,100,400,0,100));
    BookHisto(new TH2F("Pinunu_richpis_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_richpis_mmiss_vs_zvtx","",250,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_richpis_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_richpis_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_richpis_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_richpis_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_richpis_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_richpis_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_richpis_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_bph_ecalo_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eouterlkr_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eoutermuv1_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eoutermuv2_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eouterhcal_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eouter_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_ecalotot_vs_p","",400,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_bph_eouterlkr_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eoutermuv1_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eoutermuv2_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eouter_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_eouterhcal_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_ecalo_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_bph_ecalotot_vs_prange","",400,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_aph_ecalo_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eouter_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_etrack_vs_eolkr","",400,0,100,400,0,100));
    BookHisto(new TH2F("Pinunu_aph_nolkr_vs_eolkr","",400,0,100,500,0,500));
    BookHisto(new TH2F("Pinunu_aph_ecalotot_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eouterlkr_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eoutermuv1_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eoutermuv2_vs_p","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eouterhcal_vs_p","",400,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_aph_eouterlkr_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eoutermuv1_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eoutermuv2_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eouterhcal_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_eouter_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_ecalo_vs_prange","",400,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_aph_ecalotot_vs_prange","",400,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_mmiss_vs_p_lkr","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_mmiss_vs_p_auxlkr","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_mmiss_vs_p_irc","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_mmiss_vs_p_lav","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_mmiss_vs_p_sac","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_mmiss_vs_p_sav","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_phrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_phrej_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_phrej_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_phrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_phrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_phrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_phrej_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_phrej_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_phrej_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_phrej_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_phrej_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_phrej_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));


    BookHisto(new TH2F("Pinunu_multirej_nostraw_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_nostraw_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_nostraw_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_multirej_nostraw_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_nostraw_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_multirej_nostraw_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_multirej_nostraw_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_nostraw_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));


    BookHisto(new TH1I("Pinunu_straw_ncand","",10,0,10));
    BookHisto(new TH1I("Pinunu_straw_charge","",4,-2,2));
    BookHisto(new TH1I("Pinunu_straw_isnegative","",2,0,2));
    BookHisto(new TH1F("Pinunu_straw_multicda","",300,0,300));

    BookHisto(new TH2F("Pinunu_ke4ctrl_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_ke4ctrl_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_ke4ctrl_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_ke4ctrl_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_ke4ctrl_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_ke4ctrl_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_ke4ctrl_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_ke4ctrl_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

    for(int j(1);j<6;j++){

        BookHisto(new TH2F(Form("Pinunu_r%d_notchanti_specthx_vs_specthy",j),"",400,-0.02,0.02,400,-0.02,0.02));
        BookHisto(new TH2F(Form("Pinunu_r%d_notchanti_straw1_x_vs_y",j),"",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F(Form("Pinunu_r%d_notchanti_chantiend_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pinunu_r%d_notchanti_fcol_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pinunu_r%d_notchanti_gtk3_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pinunu_r%d_notchanti_trim5_x_vs_y",j),"",1000,-500,500,1000,-500,500));

    }

    if(GetWithMC()){
        BookHisto(new TH2F("Ke4Print_Pim_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pim_x_vs_y_straw2","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pim_x_vs_y_RICHMirror","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pim_x_vs_y_LKr","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pim_x_vs_y_MUV1","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pim_x_vs_y_MUV2","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pim_x_vs_y_MUV3","",240,-1200,1200,240,-1200,1200));
    }

    BookHisto(new TH2F("Pinunu_multirej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_multirej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_multirej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_multirej_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_multirej_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

    BookHisto(new TH2F("Pinunu_multirej_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_multirej_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_multirej_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_multirej_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_multirej_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_multirej_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_track_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_extra_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_dmmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pinunu_beforemamba_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_beforemamba_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_beforemamba_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_beforemamba_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_beforemamba_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_beforemamba_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_beforemamba_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_beforemamba_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

    BookHisto(new TH2F("Pinunu_beforemamba_proton_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_beforemamba_proton_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));


    BookHisto(new TH2F("Pinunu_final_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_final_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_final_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_final_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_final_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_final_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_final_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_final_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));


    BookHisto(new TH2F("Pinunu_final_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_final_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_final_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_final_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_final_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_final_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_r1_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_r1_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_r1_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pinunu_r2_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_r2_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pinunu_r2_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    //KinTails
    BookHisto(new TH2F("Pinunu_kinematics_mmiss_vs_p_nogtk","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_kinematics_mmiss_vs_phi_nogtk","",320,0,6.4,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_kinematics_mmiss_vs_p_signal","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_kinematics_mmiss_vs_phi_signal","",320,0,6.4,375,-0.1,0.14375));
    int momentum = 20;
    TString mode1="Pinunu";
    TString mode2="K2pi";
    TString mode3="Kmu2";
    TString mode4="Norm";
    for(int j(0);j<4;j++){

        momentum = 20 + j*5;

        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_ngtk_1535",mode1.Data(),momentum -5,momentum),"",375,-0.1,0.14375,20,0,20));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535",mode1.Data(),momentum-5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535",mode1.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535_final",mode1.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535_final",mode1.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));


        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_ngtk_1535",mode2.Data(),momentum -5,momentum),"",375,-0.1,0.14375,20,0,20));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535",mode2.Data(),momentum-5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535",mode2.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535_final",mode2.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535_final",mode2.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));


        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_ngtk_1535",mode3.Data(),momentum -5,momentum),"",375,-0.1,0.14375,20,0,20));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535",mode3.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535",mode3.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535_final",mode3.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535_final",mode3.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));


        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_ngtk_1535",mode4.Data(),momentum -5,momentum),"",375,-0.1,0.14375,20,0,20));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535",mode4.Data(),momentum-5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
        BookHisto(new TH2F(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535",mode4.Data(),momentum -5,momentum),"",375,-0.1,0.14375,375,-0.1,0.14375));
    }


    BookHisto(new TH2F("Pimin_start_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_start_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_start_dtchod_vs_cda","",400, -2, 2, 40, 0,10));



    BookHisto(new TH2F("Pimin_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pimin_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));
    BookHisto(new TH2F("Pimin_badgtkmatching_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Snake_pimin_r_vs_z","",250,50000,200000,250.,0.,1000.));
    BookHisto(new TH2F("Snakepos_pimin_r_vs_z","",250,50000,200000,250.,0.,1000.));
    BookHisto(new TH2F("Snake_pimin_r_vs_z_aftercut","",250,50000,200000,250.,0.,1000.));

    BookHisto(new TH2F("Pimin_fidvol_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_fidvol_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_fidvol_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_fidvol_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pimin_nobeambg_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_nobeambg_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_nobeambg_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_nobeambg_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_nobeambg_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pimin_nobeambg_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_nobeambg_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_nobeambg_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pimin_notails_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_notails_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_notails_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_notails_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_notails_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pimin_notails_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_notails_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_notails_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pimin_pidrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_pidrej_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_pidrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_pidrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_pidrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pimin_pidrej_proton_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pimin_pidrej_proton_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));

    BookHisto(new TH2F("Pimin_ibvsmmiss_after_particleid","",375,-0.1,0.14375,2000,0,200));
    BookHisto(new TH2F("Pimin_ibvsmmiss_test_3pi","",375,-0.1,0.14375,2000,0,200));

    BookHisto(new TH2F("Pimin_richpi_eop_vs_p","",200,0,100,120,0.,1.2));
    BookHisto(new TH2F("Pimin_richpi_ecalo_vs_p","",200,0,100,400,0,100));
    BookHisto(new TH2F("Pimin_richpi_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_richpi_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_richpi_mmiss_vs_zvtx","",250,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_richpi_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_richpi_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_richpi_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));

    BookHisto(new TH2F("Pimin_richpi_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_richpi_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_richpi_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pimin_mmiss_vs_p_lkr","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mmiss_vs_p_auxlkr","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mmiss_vs_p_irc","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mmiss_vs_p_sac","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mmiss_vs_p_sav","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mmiss_vs_p_lav","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Pimin_phrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_phrej_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_phrej_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_phrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_phrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_phrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pimin_phrej_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_phrej_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_phrej_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pimin_multirej_nostraw_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_nostraw_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_nostraw_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_multirej_nostraw_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_nostraw_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_multirej_nostraw_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_multirej_nostraw_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_nostraw_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));


    BookHisto(new TH1I("Pimin_straw_ncand","",10,0,10));
    BookHisto(new TH1I("Pimin_straw_charge","",4,-2,2));
    BookHisto(new TH1I("Pimin_straw_ispositive","",2,0,2));
    BookHisto(new TH1F("Pimin_straw_multicda","",300,0,300));

    BookHisto(new TH2F("Pimin_ke4ctrl_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_ke4ctrl_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_ke4ctrl_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_ke4ctrl_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_ke4ctrl_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_ke4ctrl_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_ke4ctrl_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_ke4ctrl_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

    BookHisto(new TH2F("Pimin_rich_ke4ctrl_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_rich_ke4ctrl_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_rich_ke4ctrl_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_rich_ke4ctrl_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_rich_ke4ctrl_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_rich_ke4ctrl_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_rich_ke4ctrl_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_rich_ke4ctrl_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));


    BookHisto(new TH2F("Pimin_ischanti_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_ischanti_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_ischanti_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_ischanti_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_ischanti_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_ischanti_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pimin_isnotchanti_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_isnotchanti_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_isnotchanti_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_isnotchanti_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_isnotchanti_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_isnotchanti_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));


    if(GetWithMC()){
        BookHisto(new TH2F("Ke4Print_Pip_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pip_x_vs_y_straw2","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pip_x_vs_y_RICHMirror","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pip_x_vs_y_LKr","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pip_x_vs_y_MUV1","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pip_x_vs_y_MUV2","",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F("Ke4Print_Pip_x_vs_y_MUV3","",240,-1200,1200,240,-1200,1200));

    }

    BookHisto(new TH2F("Pimin_multirej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_multirej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_multirej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_multirej_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_multirej_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

    BookHisto(new TH2F("Pimin_multirej_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_multirej_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_multirej_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pimin_track_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_extra_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_dmmiss_vs_p","",200,0,100,375,-0.1,0.14375));


    for(int j(1);j<6;j++){

        BookHisto(new TH2F(Form("Pimin_r%d_ischanti_specthx_vs_specthy",j),"",400,-0.02,0.02,400,-0.02,0.02));
        BookHisto(new TH2F(Form("Pimin_r%d_ischanti_straw1_x_vs_y",j),"",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F(Form("Pimin_r%d_ischanti_chantiend_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pimin_r%d_ischanti_fcol_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pimin_r%d_ischanti_gtk3_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pimin_r%d_ischanti_trim5_x_vs_y",j),"",1000,-500,500,1000,-500,500));

    }


    for(int j(1);j<6;j++){

        BookHisto(new TH2F(Form("Pimin_r%d_notchanti_specthx_vs_specthy",j),"",400,-0.02,0.02,400,-0.02,0.02));
        BookHisto(new TH2F(Form("Pimin_r%d_notchanti_straw1_x_vs_y",j),"",240,-1200,1200,240,-1200,1200));
        BookHisto(new TH2F(Form("Pimin_r%d_notchanti_chantiend_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pimin_r%d_notchanti_fcol_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pimin_r%d_notchanti_gtk3_x_vs_y",j),"",1000,-500,500,1000,-500,500));
        BookHisto(new TH2F(Form("Pimin_r%d_notchanti_trim5_x_vs_y",j),"",1000,-500,500,1000,-500,500));

    }
    BookHisto(new TH2F("Pimin_mamba_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mamba_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mamba_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_mamba_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mamba_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_mamba_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_mamba_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_mamba_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

    BookHisto(new TH2F("Pimin_final_rich_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_rich_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_rich_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_final_rich_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_rich_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_final_rich_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_final_rich_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_rich_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));


    BookHisto(new TH2F("Pimin_final_rich_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_final_rich_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_final_rich_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    BookHisto(new TH2F("Pimin_final_rich_ctr_bb_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_final_rich_ctr_bb_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_final_rich_ctr_bb_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));


    BookHisto(new TH2F("Pimin_final_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pimin_final_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_final_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pimin_final_mmiss_vs_fRAtStraw1","",120,0,1200,375,-0.1,0.14375));
    BookHisto(new TH2F("Pimin_final_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));


    BookHisto(new TH2F("Pimin_final_ctr_dtrich_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_final_ctr_dtcedar_vs_cda","",400, -2, 2, 40, 0,10));
    BookHisto(new TH2F("Pimin_final_ctr_dtrich_vs_dtcedar","",400, -2, 2, 400, -2,2));

    //


    BookHisto(new TH2F("Pinunu_testr1_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr1_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_testr1_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr1_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr1_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH2F("Pinunu_testr1_x_vs_y_chod","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr1_x_vs_y_muv1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr1_x_vs_y_muv2","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr1_x_vs_y_muv3","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_testr1_muonprobcal_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Pinunu_testr1_elprobcal_vs_p","",200,0,100,400, 0,1));
    //BookHisto(new TH2F("Pinunu_testr1_muonprobrich_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Pinunu_testr1_lkrshape00","",110,0,1.1,200,0,20));
    BookHisto(new TH1F("Pinunu_testr1_dtime","",200,-10,10));
    BookHisto(new TH2F("Pinunu_testr1_lkrshape","",110,0,1.1,200,0,20));
    BookHisto(new TH2F("Pinunu_testr1_ncellvseseed","",100,0,10,150,0,150));
    BookHisto(new TH2F("Pinunu_testr1_muv1shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Pinunu_testr1_muv2shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Pinunu_testr1_dtlkrmuv1","",200,-10,10,200,-10,10));
    BookHisto(new TH2F("Pinunu_testr1_dtvsr1","",110,0,1.1,200,-10,10));


    BookHisto(new TH2F("Pinunu_testr1_cda_vs_zvtx","",250,50000,200000,40,0,10));
    BookHisto(new TH1F("Pinunu_testr1_ngtk1hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr1_ngtk2hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr1_ngtk3hits","",200,0,200));

    BookHisto(new TH2F("Pinunu_testr1_spec_nhits","",200,0,100,100,0,100));
    BookHisto(new TH2F("Pinunu_testr1_spec_dthetax","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr1_spec_dthetay","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr1_spec_qual1","",100,0,10,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr1_vertex_xz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr1_vertex_yz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr1_kaon_xy","",50,-50,50,50,-50,50));
    BookHisto(new TH2F("Pinunu_testr1_thetavsp","",200,0,100,200,0.,0.02));
    BookHisto(new TH2F("Pinunu_testr1_kaon_xy_check","",200,-50,50,200,-50,50));
    BookHisto(new TH1F("Pinunu_testr1_kaon_p_check","",100,70,80));
    BookHisto(new TH2F("Pinunu_testr1_kaon_thetaxy_check","",50,-0.001,0.001,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_testr1_thx_vs_pk","",100,70,80,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_testr1_thy_vs_pk","",100,70,80,50,-0.001,0.001));

    BookHisto(new TH2F("Pinunu_testr2_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr2_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_testr2_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr2_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr2_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH2F("Pinunu_testr2_x_vs_y_chod","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr2_x_vs_y_muv1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr2_x_vs_y_muv2","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr2_x_vs_y_muv3","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_testr2_muonprobcal_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Pinunu_testr2_elprobcal_vs_p","",200,0,100,400, 0,1));
    //BookHisto(new TH2F("Pinunu_testr2_muonprobrich_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Pinunu_testr2_lkrshape00","",110,0,1.1,200,0,20));
    BookHisto(new TH1F("Pinunu_testr2_dtime","",200,-10,10));
    BookHisto(new TH2F("Pinunu_testr2_lkrshape","",110,0,1.1,200,0,20));
    BookHisto(new TH2F("Pinunu_testr2_ncellvseseed","",100,0,10,150,0,150));
    BookHisto(new TH2F("Pinunu_testr2_muv1shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Pinunu_testr2_muv2shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Pinunu_testr2_dtlkrmuv1","",200,-10,10,200,-10,10));
    BookHisto(new TH2F("Pinunu_testr2_dtvsr1","",110,0,1.1,200,-10,10));


    BookHisto(new TH2F("Pinunu_testr2_cda_vs_zvtx","",250,50000,200000,40,0,10));
    BookHisto(new TH1F("Pinunu_testr2_ngtk1hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr2_ngtk2hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr2_ngtk3hits","",200,0,200));

    BookHisto(new TH2F("Pinunu_testr2_spec_nhits","",200,0,100,100,0,100));
    BookHisto(new TH2F("Pinunu_testr2_spec_dthetax","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr2_spec_dthetay","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr2_spec_qual1","",100,0,10,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr2_vertex_xz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr2_vertex_yz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr2_kaon_xy","",50,-50,50,50,-50,50));
    BookHisto(new TH2F("Pinunu_testr2_thetavsp","",200,0,100,200,0.,0.02));
    BookHisto(new TH2F("Pinunu_testr2_kaon_xy_check","",200,-50,50,200,-50,50));
    BookHisto(new TH1F("Pinunu_testr2_kaon_p_check","",100,70,80));
    BookHisto(new TH2F("Pinunu_testr2_kaon_thetaxy_check","",50,-0.001,0.001,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_testr2_thx_vs_pk","",100,70,80,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_testr2_thy_vs_pk","",100,70,80,50,-0.001,0.001));


    BookHisto(new TH2F("Pinunu_testr3_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr3_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_testr3_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr3_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr3_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH2F("Pinunu_testr3_x_vs_y_chod","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr3_x_vs_y_muv1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr3_x_vs_y_muv2","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr3_x_vs_y_muv3","",240,-1200,1200,240,-1200,1200));


    BookHisto(new TH2F("Pinunu_testr3_cda_vs_zvtx","",250,50000,200000,40,0,10));
    BookHisto(new TH1F("Pinunu_testr3_ngtk1hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr3_ngtk2hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr3_ngtk3hits","",200,0,200));

    BookHisto(new TH2F("Pinunu_testr3_spec_nhits","",200,0,100,100,0,100));
    BookHisto(new TH2F("Pinunu_testr3_spec_dthetax","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr3_spec_dthetay","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr3_spec_qual1","",100,0,10,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr3_vertex_xz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr3_vertex_yz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr3_kaon_xy","",50,-50,50,50,-50,50));
    BookHisto(new TH2F("Pinunu_testr3_thetavsp","",200,0,100,200,0.,0.02));
    BookHisto(new TH2F("Pinunu_testr3_kaon_xy_check","",200,-50,50,200,-50,50));
    BookHisto(new TH1F("Pinunu_testr3_kaon_p_check","",100,70,80));
    BookHisto(new TH2F("Pinunu_testr3_kaon_thetaxy_check","",50,-0.001,0.001,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_testr3_thx_vs_pk","",100,70,80,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_testr3_thy_vs_pk","",100,70,80,50,-0.001,0.001));

    BookHisto(new TH2F("Pinunu_testr4_m miss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr4_p _vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("Pinunu_testr4_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr4_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr4_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH2F("Pinunu_testr4_x_vs_y_chod","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr4_x_vs_y_muv1","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr4_x_vs_y_muv2","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Pinunu_testr4_x_vs_y_muv3","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH2F("Pinunu_testr4_cda_vs_zvtx","",250,50000,200000,40,0,10));
    BookHisto(new TH1F("Pinunu_testr4_ngtk1hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr4_ngtk2hits","",200,0,200));
    BookHisto(new TH1F("Pinunu_testr4_ngtk3hits","",200,0,200));

    BookHisto(new TH2F("Pinunu_testr4_spec_nhits","",200,0,100,100,0,100));
    BookHisto(new TH2F("Pinunu_testr4_spec_dthetax","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr4_spec_dthetay","",100,-0.001,0.001,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr4_spec_qual1","",100,0,10,375,-0.1,0.14375));
    BookHisto(new TH2F("Pinunu_testr4_vertex_xz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr4_vertex_yz","",250,50000,200000,200,-200,200));
    BookHisto(new TH2F("Pinunu_testr4_kaon_xy","",50,-50,50,50,-50,50));
    BookHisto(new TH2F("Pinunu_testr4_thetavsp","",200,0,100,200,0.,0.02));
    BookHisto(new TH2F("Pinunu_testr4_kaon_xy_check","",200,-50,50,200,-50,50));
    BookHisto(new TH1F("Pinunu_testr4_kaon_p_check","",100,70,80));
    BookHisto(new TH2F("Pinunu_testr4_kaon_thetaxy_check","",50,-0.001,0.001,50,-0.001,0.001));

    BookHisto(new TH2F("Pinunu_testr4_thx_vs_pk","",100,70,80,50,-0.001,0.001));
    BookHisto(new TH2F("Pinunu_testr4_thy_vs_pk","",100,70,80,50,-0.001,0.001));


    BookHisto(new TH1F("IsTwoPhoton","",2,0,2));
    BookHisto(new TH1F("IsK2pi","",2,0,2));

    BookHisto(new TH2F("Ke3_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH1F("Ke3_dtimep0","",400,-50.,50.));
    BookHisto(new TH2F("Ke3_mmiss_vs_p_aftertimepi0","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_zvtxpi0_vs_p","",250,5000,20000,375,-0.1,0.15));
    BookHisto(new TH2F("Ke3_mmiss_vs_zvtxpi0","",300,5000,20000,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_mmiss_vs_p_afterzvtxpi0","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Ke3_mmiss_vs_p_beforechanti","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_mmiss_vs_p_nochanti","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_mmiss_vs_p_afterchanti","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_mmiss_vs_p_notails","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_fullmmiss_vs_p_notails","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Ke3_mmiss_vs_p_richm","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_fullmmiss_vs_p_richm","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_ringrad_vs_p","",200,0.,100.,300,0,300));
    BookHisto(new TH2F("Ke3_multrichmass_vs_p","",200,0.,100.,500,0.,1.));
    BookHisto(new TH2F("Ke3_singlerichmass_vs_p","",200,0.,100.,500,0.,1.));
    BookHisto(new TH2F("Ke3_richp_vs_p","",200,0.,100.,200,0.,100.));
    BookHisto(new TH1F("Ke3_nrings","",50,0.,50.));

    BookHisto(new TH2F("Ke3_singlerichmass_vs_p_final","",200,0.,100.,500,0.,1.));
    BookHisto(new TH2F("Ringrad_vs_p","",200,0.,100.,300,0,300));
    BookHisto(new TH2F("Ke3_mmiss_vs_p_richs","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_fullmmiss_vs_p_richs","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Ke3_mmiss_vs_p_phrej","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_fullmmiss_vs_p_phrej","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_eop_vs_p_phrej","",200,0,100,120,0.,1.2));
    BookHisto(new TH2F("Ke3_emuv1_vs_ecal_phrej","",400,0,100,400,0,100));
    BookHisto(new TH2F("Ke3_emuv2_vs_ecal_phrej","",400,0,100,400,0,100));

    BookHisto(new TH2F("Ke3_mmiss_vs_p_murej","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_fullmmiss_vs_p_murej","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_eop_vs_p_murej","",200,0,100,120,0.,1.2));
    BookHisto(new TH2F("Ke3_emuv1_vs_ecal_murej","",400,0,100,400,0,100));
    BookHisto(new TH2F("Ke3_emuv2_vs_ecal_murej","",400,0,100,400,0,100));

    BookHisto(new TH2F("Ke3_mmiss_vs_p_kine","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_fullmmiss_vs_p_kine","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_eop_vs_p_kine","",200,0,100,120,0.,1.2));
    BookHisto(new TH2F("Ke3_emuv1_vs_ecal_kine","",400,0,100,400,0,100));
    BookHisto(new TH2F("Ke3_emuv2_vs_ecal_kine","",400,0,100,400,0,100));

    BookHisto(new TH2F("Ke3_pid_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_pid_eop_vs_p","",200,0,100,120,0,1.2));
    BookHisto(new TH2F("Ke3_pid_mmiss_vs_p_beforePID","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH1F("Ke3_pid_calid_ptrack","",200,0,100));
    BookHisto(new TH2F("Ke3_pid_calid_muonprob_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Ke3_pid_calid_elprob_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Ke3_pid_calid_lkrshape00","",110,0,1.1,200,0,20));
    BookHisto(new TH1F("Ke3_pid_calid_dtime","",200,-10,10));
    BookHisto(new TH2F("Ke3_pid_calid_lkrshape","",110,0,1.1,200,0,20));
    BookHisto(new TH2F("Ke3_pid_calid_ncellvseseed","",100,0,10,150,0,150));
    BookHisto(new TH2F("Ke3_pid_calid_muv1shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Ke3_pid_calid_muv2shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Ke3_pid_calid_dtlkrmuv1","",200,-10,10,200,-10,10));
    BookHisto(new TH2F("Ke3_pid_calid_dtvsr1","",110,0,1.1,200,-10,10));
    BookHisto(new TH2F("Ke3_pid_muonprob_vs_pionprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("Ke3_pid_muonprob_vs_elprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("Ke3_pid_pionprob_vs_elprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("Ke3_pid_erejection_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_pid_eefficiency_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_pid_mmiss_vs_p_ramuon","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Ke3_pid_mmiss_vs_p_ramuon_em","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("K2pi_alltrigg_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH1F("K2pi_dtimep0","",400,-50.,50.));
    BookHisto(new TH2F("K2pi_mmiss_vs_p_aftertimepi0","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_mmiss_vs_zvtxpi0","",300,5000,20000,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_mmiss_vs_p_afterzvtxpi0","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("K2pi_mmiss_vs_p_beforechanti","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_mmiss_vs_p_nochanti","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_mmiss_vs_p_afterchanti","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("K2pi_dcda","",100,0,400,100,0,20));
    BookHisto(new TH2F("K2pi_diffcda","",100,0,100,210,-20,400));
    BookHisto(new TH2F("K2pi_mmiss_vs_p_notails","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_mmiss_vs_p_nomultitrack","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("K2pi_pid_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_pid_eop_vs_p","",200,0,100,120,0,1.2));
    BookHisto(new TH2F("K2pi_pid_mmiss_vs_p_beforePID","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH1F("K2pi_pid_calid_ptrack","",200,0,100));
    BookHisto(new TH2F("K2pi_pid_calid_muonprob_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("K2pi_pid_calid_elprob_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("K2pi_pid_calid_lkrshape00","",110,0,1.1,200,0,20));
    BookHisto(new TH1F("K2pi_pid_calid_dtime","",200,-10,10));
    BookHisto(new TH2F("K2pi_pid_calid_lkrshape","",110,0,1.1,200,0,20));
    BookHisto(new TH2F("K2pi_pid_calid_ncellvseseed","",100,0,10,150,0,150));
    BookHisto(new TH2F("K2pi_pid_calid_muv1shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("K2pi_pid_calid_muv2shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("K2pi_pid_calid_dtlkrmuv1","",200,-10,10,200,-10,10));
    BookHisto(new TH2F("K2pi_pid_calid_dtvsr1","",110,0,1.1,200,-10,10));
    BookHisto(new TH2F("K2pi_pid_muonprob_vs_pionprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("K2pi_pid_muonprob_vs_elprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("K2pi_pid_pionprob_vs_elprob","",400,0,1,400, 0,1));

    BookHisto(new TH2F("K2pi_pid_pionefficiency_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_pid_pionrejection_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_pid_mmiss_vs_p_ramuon","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_pid_mmiss_vs_p_ramuon_em","",200,0,100,375,-0.1,0.14375));

    //RICH pid
    BookHisto(new TH2F("K2pi_mmiss_vs_richlikelihood","",5,0,5,375,-0.1,0.14375));
    BookHisto(new TH1F("K2pi_singlepi_lr","",500,0,100));
    BookHisto(new TH2F("K2pi_mmiss_vs_richlikelihood_1535","",5,0,5,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_kinrange_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_richpionpid_mmiss_vs_richlikelihood_1535","",5,0,5,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_richpionpid_mmiss_vs_richlikelihood_1535_nrings","",5,0,5,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_richpionpid_mringmass_vs_p_asid","",200,0,100,500,0,1.));
    BookHisto(new TH2F("K2pi_richpionpid_sringmass_vs_p_asid","",200,0,100,500,0,1.));
    BookHisto(new TH2F("K2pi_richpionpid_mmiss_vs_p_asid","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("K2pi_richpionpid_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_richpionpid_mmiss_vs_p_full","",200,0,100,375,-0.1,0.14375));

    //KinTails
    BookHisto(new TH2F("K2pi_kinematics_mmiss_vs_p_nogtk","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_kinematics_mmiss_vs_phi_nogtk","",320,0,6.4,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_kinematics_mmiss_vs_p_signal","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("K2pi_kinematics_mmiss_vs_phi_signal","",320,0,6.4,375,-0.1,0.14375));

    BookHisto(new TH2F("K2pi_mmiss_vs_p_full","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Kmu2_alltrigg_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_mmiss_vs_p_beforechanti","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RVeto_mmiss_vs_p_nochanti","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_mmiss_vs_p_afterchanti","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Kmu2_dcda","",100,0,400,100,0,20));
    BookHisto(new TH2F("Kmu2_diffcda","",100,0,100,210,-20,400));
    BookHisto(new TH2F("Kmu2_mmiss_vs_p_notails","",200,0,100,375,-0.1,0.14375));

    //Random Veto plots
    BookHisto(new TH2F("Kmu2_RVeto_mmiss_vs_p_segments_before","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RVeto_mmiss_vs_p_segments_after","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RMultVeto_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RMultVetoFull_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Kmu2_RPhVeto_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RPhVeto_mmiss_vs_p_nogammalkr","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RPhVeto_mmiss_vs_p_nogammalkraux","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RPhVeto_mmiss_vs_p_nogammalav","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RPhVeto_mmiss_vs_p_nogammasav","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_RPhVeto_mmiss_vs_p_nogammafinal","",200,0,100,375,-0.1,0.14375));

    //Random Veto plots
    BookHisto(new TH2F("Kmu2_lkrtest_RPhVeto_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammalkr","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammalkraux","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammalav","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammasav","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammafinal","",200,0,100,375,-0.1,0.14375));

    //Plots used in the muon discriminant building
    BookHisto(new TH2F("Kmu2_mudiscr_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_mudiscr_muv3_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Kmu2_pid_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_pid_eop_vs_p","",200,0,100,120,0,1.2));
    BookHisto(new TH2F("Kmu2_pid_mmiss_vs_p_beforePID","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH1F("Kmu2_pid_calid_ptrack","",200,0,100));
    BookHisto(new TH2F("Kmu2_pid_calid_muonprob_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Kmu2_pid_calid_elprob_vs_p","",200,0,100,400, 0,1));
    BookHisto(new TH2F("Kmu2_pid_calid_lkrshape00","",110,0,1.1,200,0,20));
    BookHisto(new TH1F("Kmu2_pid_calid_dtime","",200,-10,10));
    BookHisto(new TH2F("Kmu2_pid_calid_lkrshape","",110,0,1.1,200,0,20));
    BookHisto(new TH2F("Kmu2_pid_calid_ncellvseseed","",100,0,10,150,0,150));
    BookHisto(new TH2F("Kmu2_pid_calid_muv1shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Kmu2_pid_calid_muv2shape","",110,0,1.1,100,0,200));
    BookHisto(new TH2F("Kmu2_pid_calid_dtlkrmuv1","",200,-10,10,200,-10,10));
    BookHisto(new TH2F("Kmu2_pid_calid_dtvsr1","",110,0,1.1,200,-10,10));
    BookHisto(new TH2F("Kmu2_pid_muonprob_vs_pionprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("Kmu2_pid_muonprob_vs_elprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("Kmu2_pid_pionprob_vs_elprob","",400,0,1,400, 0,1));
    BookHisto(new TH2F("Kmu2_pid_murejection_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_pid_muefficiency_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_pid_mmiss_vs_p_ramuon","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_pid_mmiss_vs_p_ramuon_em","",200,0,100,375,-0.1,0.14375));


    //KinTails
    BookHisto(new TH2F("Kmu2_kinematics_mmiss_vs_p_nogtk","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_kinematics_mmiss_vs_phi_nogtk","",320,0,6.4,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_kinematics_mmiss_vs_p_signal","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_kinematics_mmiss_vs_phi_signal","",320,0,6.4,375,-0.1,0.14375));

    // BookHisto(new TH2F("Kmu2_kinematics_mmiss_vs_ngtk_1535","",375,-0.1,0.14375,20,0,20));
    // BookHisto(new TH2F("Kmu2_kinematics_mmiss_vs_mm2nogtk_1535","",375,-0.1,0.14375,375,-0.1,0.14375));
    // BookHisto(new TH2F("Kmu2_kinematics_mmiss_vs_mm2rich_1535","",375,-0.1,0.14375,375,-0.1,0.14375));

    //RICH Studies
    BookHisto(new TH2F("Kmu2_mmiss_vs_richlikelihood","",5,0,5,375,-0.1,0.14375));
    BookHisto(new TH1F("Kmu2_singlepi_lr","",500,0,100));
    BookHisto(new TH2F("Kmu2_mmiss_vs_richlikelihood_1535","",5,0,5,375,-0.1,0.14375));

    BookHisto(new TH2F("Kmu2_kinrange_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_richpionpid_mmiss_vs_richlikelihood_1535","",5,0,5,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_richpionpid_mmiss_vs_richlikelihood_1535_nrings","",5,0,5,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_richpionpid_mringmass_vs_p_asid","",200,0,100,500,0,1.));
    BookHisto(new TH2F("Kmu2_richpionpid_sringmass_vs_p_asid","",200,0,100,500,0,1.));
    BookHisto(new TH2F("Kmu2_richpionpid_mmiss_vs_p_asid","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("Kmu2_richmuonpid_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("Kmu2_richmuonpid_mmiss_vs_p_full","",200,0,100,375,-0.1,0.14375));


    //BookHisto(new TH1F("Teff_All_dtprim","",2000,-1000,1000));
    //BookHisto(new TH1F("Teff_AllMUV3_NCandidates","",100,0,100));
    //BookHisto(new TH2F("Teff_AllRICH_NHits_vs_NSC","",40,0,40,40,0,40));
    ////BookHisto(new TH1F("Teff_AllRICH_NHits","",50,0,50));
    //BookHisto(new TH2F("Teff_AllRICH_atxy","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH2F("Teff_AllMUV3_atxy","",240,-1200,1200,240,-1200,1200));
    //
    //BookHisto(new TH2F("Teff_MUV3_dist_vs_dtrich","",1000,-100,100,200,0,2000));
    //BookHisto(new TH2F("Teff_MUV3_xy","",24,-1200,1200,24,-1200,1200));
    //BookHisto(new TH2F("Teff_MUV3_atxy","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH2F("Teff_MUV3_min_dist_vs_dtrich","",1000,-100,100,200,0,2000));
    //BookHisto(new TH2F("Teff_MUV3_min_xy","",24,-1200,1200,24,-1200,1200));
    //BookHisto(new TH2F("Teff_MUV3_min_atxy","",240,-1200,1200,240,-1200,1200));
    //
    //BookHisto(new TH2F("Teff_NoMUV3_min_dist_vs_dtrich","",1000,-100,100,200,0,2000));
    //BookHisto(new TH2F("Teff_NoMUV3_min_xy","",24,-1200,1200,24,-1200,1200));
    //BookHisto(new TH2F("Teff_NoMUV3_min_atxy","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH2F("Teff_NoMUV3Cand_xy","",240,-1200,1200,240,-1200,1200));
    //
    //BookHisto(new TH2F("CHODPrimID_vs_DataType","",36000,0,36000,20,0,20));
    //BookHisto(new TH2F("CHODPrimIDSlot0_vs_DataType","",36000,0,36000,20,0,20));
    //BookHisto(new TH2F("CHODPrimIDSlot1_vs_DataType","",36000,0,36000,20,0,20));
    //BookHisto(new TH2F("CHODPrimIDSlot2_vs_DataType","",36000,0,36000,20,0,20));
    //
    //BookHisto(new TH2F("RICHPrimID_vs_DataType","",36000,0,36000,20,0,20));
    //BookHisto(new TH2F("RICHPrimIDSlot0_vs_DataType","",36000,0,36000,20,0,20));
    //BookHisto(new TH2F("RICHPrimIDSlot1_vs_DataType","",36000,0,36000,20,0,20));
    //BookHisto(new TH2F("RICHPrimIDSlot2_vs_DataType","",36000,0,36000,20,0,20));
    //
    //BookHisto(new TH1F("Teff_NoRICH_dtprim","",2000,-1000,1000));
    //BookHisto(new TH2F("Teff_NoRICH_NHits_vs_NSC","",40,0,40,40,0,40));
    //BookHisto(new TH2F("Teff_NoRICH_min_xy","",240,-1200,1200,240,-1200,1200));
    //BookHisto(new TH1F("Teff_RICH_dtprim","",2000,-1000,1000));
    //BookHisto(new TH2F("Teff_RICH_NHits_vs_NSC","",40,0,40,40,0,40));
    //BookHisto(new TH2F("Teff_RICH_min_xy","",240,-1200,1200,240,-1200,1200));

}

/////////////////////////////////////////////////////
// Select the particle closest to the trigger time //
/////////////////////////////////////////////////////


void KaonTrackAnalysis::DefineMCSimple(){
    if(GetWithMC()){

        // kID = fMCSimple.AddParticle(0, 321);//kaon
        int kID = fMCSimple.AddParticle(0, 321);//kaon
        fMCSimple.AddParticle(kID, 211);//piplus
        //fMCSimple.AddParticle(kID, 22);//gamma from kaon
        //pi0ID = fMCSimple.AddParticle(kID, 111);//pi0
        fMCSimple.AddParticle(kID, -211);//piminus
        fMCSimple.AddParticle(kID, -11);//eplus
        fMCSimple.AddParticle(kID, 22);//g1
        fMCSimple.AddParticle(kID, 22);//g2
    }
}

void KaonTrackAnalysis::StartOfRunUser(){
    fRun=GetEventHeader()->GetRunID();
    fSACAnal->StartBurst(fYear,fRun);
    fIRCAnal->StartBurst(fYear,fRun);
    fSAVAnal->StartBurst(fYear,fRun);

    frvnorm=0;
    frvph  =0;
    frvmult=0;
}

void KaonTrackAnalysis::StartOfBurstUser(){

    cout << "Run"<<GetEventHeader()->GetRunID()<<" Burst"<<GetEventHeader()->GetBurstID() << endl;
    fIsFilter = GetTree("Reco")->FindBranch("FilterWord") ? true : false;
    fStrawAnalysis->StartBurst();
    fIsMC = GetWithMC();

    fnnorm_before=0;
    fnnorm=0;
    fnpipi=0;
    fnpipipi=0;
    fnmunu=0;
}

void KaonTrackAnalysis::EndOfBurstUser(){

    std::cout << "----- EOB Analysis INFO ------" << std::endl;
    std::cout << "Run ID***" << "Burst ID***" << "Nnorm_before***" << "Nnorm***" << "Npipi***" << "Npipipi***" << "Nmunu***" << endl;
    std::cout << GetEventHeader()->GetRunID()<< "  " << GetEventHeader()->GetBurstID() << "  "<< fnnorm_before << "  " << fnnorm << "  " <<  fnpipi << "  " << fnpipipi<< "  " <<  fnmunu<< endl;
}

void KaonTrackAnalysis::EndOfRunUser(){

    FillHisto("RandomPhotonVeto", GetEventHeader()->GetRunID(), frvph/frvnorm);
    FillHisto("TotalRandomVeto", GetEventHeader()->GetRunID(), frvmult/frvnorm);
    std::cout << "----- EOR Analysis INFO ------" << std::endl;
    std::cout << "Run ID***" << "Burst ID***" << "RPhV +- Err***" << "RV +- Err***" << endl;
    std::cout << GetEventHeader()->GetRunID()<< "  " << GetEventHeader()->GetBurstID() << "  ";
    std::cout << frvph/frvnorm << " +- " << sqrt(frvph/frvnorm*(1 - frvph/frvnorm)/frvnorm) << " || " <<  frvmult/frvnorm << " +- " << sqrt(frvmult/frvnorm*(1 - frvmult/frvnorm)/frvnorm)<< endl;
}

void KaonTrackAnalysis::EndOfJobUser(){
    //ClearAllPointers();

    SaveAllPlots();
    fStrawAnalysis->GetUserMethods()->SaveAllPlots();
    fSACAnal->GetUserMethods()->SaveAllPlots();
    fIRCAnal->GetUserMethods()->SaveAllPlots();
    fSAVAnal->GetUserMethods()->SaveAllPlots();
    fLKrAnal->GetUserMethods()->SaveAllPlots();
    for(int j(0);j<9;j++)
        fMultAnal[j]->GetUserMethods()->SaveAllPlots();

}

void KaonTrackAnalysis::DrawPlot(){
}

void KaonTrackAnalysis::PostProcess(){

}
KaonTrackAnalysis::~KaonTrackAnalysis(){
}

void KaonTrackAnalysis::Process(int iEvent){

    // if(GetWithMC())
    //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}

    fCedarEvent        = (TRecoCedarEvent*)GetEvent("Cedar");
    fGigaTrackerEvent  = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
    fCHANTIEvent       = (TRecoCHANTIEvent*)GetEvent("CHANTI");
    fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
    fLAVEvent          = (TRecoLAVEvent*)GetEvent("LAV");
    fRICHEvent         = (TRecoRICHEvent*)GetEvent("RICH");
    fCHODEvent         = (TRecoCHODEvent*)GetEvent("CHOD");
    fNewCHODEvent      = (TRecoNewCHODEvent*)GetEvent("NewCHOD");
    fIRCEvent          = (TRecoIRCEvent*)GetEvent("IRC");
    fLKrEvent          = (TRecoLKrEvent*)GetEvent("LKr");
    fMUV0Event         = (TRecoMUV0Event*)GetEvent("MUV0");
    fMUV1Event         = (TRecoMUV1Event*)GetEvent("MUV1");
    fMUV2Event         = (TRecoMUV2Event*)GetEvent("MUV2");
    fMUV3Event         = (TRecoMUV3Event*)GetEvent("MUV3");
    fSACEvent          = (TRecoSACEvent*)GetEvent("SAC");
    fSAVEvent          = (TRecoSAVEvent*)GetEvent("SAV");
    fHACEvent          = (TRecoHACEvent*)GetEvent("HAC");

    if(!fIsFilter){

        if (fLKrEvent->GetErrorMask()) return;
        if (fGigaTrackerEvent->GetErrorMask()) return;
    }

    fStrawAnalysis->Clear();
    // Trigger selection
    // fHeader = GetWithMC() ? 0 : GetEventHeader();
    fHeader =  GetEventHeader();
    fL0Data = GetWithMC() ? 0 : GetL0Data();

    if(!GetWithMC()){

        fFineTriggerTime = fHeader->GetFineTime()*fFineTimeScale;
        // General cut
        Int_t eventqualitymask = fHeader->GetEventQualityMask();
        if(eventqualitymask&(1<<kHAC)) eventqualitymask-=(1<<kHAC);
        if (eventqualitymask>0) return;
    }

    // Flags
    fGTKFlag = *(Int_t *)GetOutput("MainAnalysis.GTKFlag",fState); // 1 no GTK, 0 GTK
    fZVertexCut = !fGTKFlag?105000.:125000.;
    fYear = *(Int_t *)GetOutput("MainAnalysis.Year",fState);

    if (fYear==2016) {
        fTrimTheta = 0.00122;
        fTrimKick = 0.0913;
        fThetaXCenter = 0.00002;
        fThetaYCenter = 0.00002;
    }

    NominalKaon();


    // Check if the event has passed the single track analysis
    Bool_t ots = *(Bool_t *)GetOutput("MainAnalysis.OneTrackEvent",fState);
    //fPinunuL0Trigger= *(Bool_t *)GetOutput("EfficiencyWithPrimitives.PinnTrigger",fState);
    //fPinunuL0Trigger20ns= *(Bool_t *)GetOutput("EfficiencyWithPrimitives.PinnTrigger20ns",fState);
    //fNewCHODL0 = *(Bool_t *)GetOutput("EfficiencyWithPrimitives.NewCHODL0",fState);
    //fRICHL0 = *(Bool_t *)GetOutput("EfficiencyWithPrimitives.RICHL0",fState);
    //fLAVL0  = *(Bool_t *)GetOutput("EfficiencyWithPrimitives.LAVL0",fState);
    //fMUV3L0 = *(Bool_t *)GetOutput("EfficiencyWithPrimitives.MUV3L0",fState);
    //fQxL0   = *(Bool_t *)GetOutput("EfficiencyWithPrimitives.Qx",fState);
    //fUMTC   = *(Bool_t *)GetOutput("EfficiencyWithPrimitives.UMTC",fState);
    //fCaloL0 = *(Bool_t *)GetOutput("EfficiencyWithPrimitives.Calo",fState);







    if (!ots) return ;
    fNTracks = *(Int_t *)GetOutput("MainAnalysis.NKaonEventCandidates",fState);
    if (!fNTracks) return;
    fDownstreamTrack = (DownstreamParticle *)GetOutput("MainAnalysis.KaonEventDownstreamTrack",fState);
    fUpstreamTrack = (UpstreamParticle *)GetOutput("MainAnalysis.KaonEventUpstreamTrack",fState);

    // PiP0 Sample
    fisK2piEvent = *(Bool_t *)GetOutput("TwoPhotonAnalysis.isPiP0Event",fState);
    fisTwoPhoton = *(Bool_t *)GetOutput("TwoPhotonAnalysis.isTwoPhotonEvent",fState);
    // CalorimeterCluster = (CalorimeterCluster*)GetOutput("SpectrometerCalorimeterAssociation.MatchedClusters"fState);
    fCaloArray = (TClonesArray*)GetOutput("SpectrometerCalorimetersAssociation.MatchedClusters",fState);
    fStrawCandidate = (StrawCandidate *)GetOutput("TrackProcessor.StrawCandidate",fState);


    //if(fHeader->GetEventNumber() != 1441359) return;
    fL0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
    fL0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();
    fPhysicsData   = fL0DataType    & 0x1;

    fPinunuTrigger  = SelectTrigger(2,fL0DataType,fL0TriggerWord);
    fControlTrigger = SelectTrigger(0,fL0DataType,fL0TriggerWord);
    if(fPinunuTrigger || fControlTrigger){
        FillHisto("IsTwoPhoton", fisTwoPhoton);
        FillHisto("IsK2pi", fisK2piEvent);
    }
    fLKrAnal->SetEvent(fLKrEvent);
    for(int j(0);j<9;j++){

        fMultAnal[j]->SetMUV0Event(fMUV0Event);
        fMultAnal[j]->SetHACEvent(fHACEvent);

    }
    // Event filtering
    ////  FilterAccept();
    fIdTrack = -1;
    fIdTrack = SelectOneParticle(); //check
    fRAtStraw1 = 0;
    fXAtStraw1 = 0;
    fYAtStraw1 = 0;
    if(fIdTrack==-1) return;

    // Define candidate track
    fTrack = &fDownstreamTrack[fIdTrack];
    fPTrack = fTrack->GetMomentum().P();
    fBeamTrack = &fUpstreamTrack[fIdTrack];
    fPBeamTrack = fBeamTrack->GetMomentum().P();
    fStrawAnalysis->AnalyzeHits(fSpectrometerEvent,fTrack->GetTrackID(),99999999);
    fVertex = fBeamTrack->GetVertex();
    fMMiss = (fBeamTrack->GetMomentum()-fTrack->GetMomentum()).Mag2();
    fMmuMiss = (fBeamTrack->GetMomentum()-fTrack->GetMuonMomentum()).Mag2();
    fMelMiss = (fBeamTrack->GetMomentum()-fTrack->GetElectronMomentum()).Mag2();
    fXAtStraw1 = fTrack->GetPositionAtStraw(0).X();
    fYAtStraw1 = fTrack->GetPositionAtStraw(0).Y();
    fRAtStraw1 = TMath::Sqrt((fXAtStraw1 - xSTRAW_station[0])*(fXAtStraw1 - xSTRAW_station[0])+fYAtStraw1*fYAtStraw1);
    fXAtLKr = fTrack->GetPositionAtLKr().X();
    fYAtLKr = fTrack->GetPositionAtLKr().Y();
    fcda    = fBeamTrack->GetCDA();
    fdtrich = fBeamTrack->GetTime() - fTrack->GetRICHSingleTime();
    fdtchod = fBeamTrack->GetTime() - fTrack->GetCHODTime();
    fdtcedar= fBeamTrack->GetTime() - fBeamTrack->GetKTAGTime();
    fMMissRICH = (fBeamTrack->GetMomentum()-fTrack->GetRICHSingleMomentum()).Mag2();


    // Photons
    fIsLKr = fTrack->GetIsPhotonLKrCandidate();
    fIsLKrActivity = fTrack->GetIsPhotonLKrActivity();
    fIsNewLKr = fTrack->GetIsPhotonNewLKrCandidate();
    fIsLAV = fTrack->GetIsPhotonLAVCandidate();
    fIsIRC = fTrack->GetIsPhotonIRCCandidate();
    fIsSAC = fTrack->GetIsPhotonSACCandidate();
    fIsSAV = fTrack->GetIsPhotonSAVCandidate();

    // Build segments
    fSegmentIB = 99999.;
    fStrawAnalysis->LoadEvent(fLKrEvent);
    fSegmentIB = fStrawAnalysis->ReconstructSegments(&fVertex,fBeamTrack->GetTime(),-1);

    //RICH
    fIsRICHSingleRing = fTrack->GetRICHSingleIsCandidate();
    fIsRICHGoodSingleRing = fTrack->GetIsGoodRICHSR();
    fIsRICHMultiRing = fTrack->GetRICHMultiIsCandidate();
    fRICHStatus = fTrack->GetRICHMultiMostLikelyHypothesis();
    fIsRICHPionLH = fTrack->GetIsRICHPion();
    //fIsRICHPionSumLH = fTrack->GetRICHMultiPionLikelihood()/(fTrack->GetRICHMultiMuonLikelihood() + fTrack->GetRICHMultiElectronLikelihood());
    fpiLH = fTrack->GetRICHMultiPionLikelihood();
    fmuLH = fTrack->GetRICHMultiMuonLikelihood();
    felLH = fTrack->GetRICHMultiElectronLikelihood();

    fRICHp     = fTrack->GetRICHSingleMomentum();
    fMMissRICH = (fBeamTrack->GetMomentum()-fRICHp).Mag2();
    fSRingMass = fTrack->GetRICHSingleMass();
    fMRingMass = fTrack->GetRICHMultiMass();
    fNRings    = fRICHEvent->GetNRingCandidates();

    if(GetWithMC()){

        //Mimicing the L0 RICH Mult trigger condition of at least 4 RICH hits

        int goodNHits = 0;
        Int_t nHits = fRICHEvent->GetNHits();
        TClonesArray& Hits = (*(fRICHEvent->GetHits()));

        for (Int_t jHit=0; jHit<nHits; jHit++) {
            TRecoRICHHit *richhit = (TRecoRICHHit*)Hits[jHit];
            if (richhit->GetOrSuperCellID()==1) continue;

            //FillHisto("RICH_dt_vs_Q",richhit->GetTime() - fTrack->GetCHODTime(),fTrack->GetCharge());

            if(fabs(richhit->GetTime() - fTrack->GetCHODTime()) < 5)
                goodNHits++;
        }

        FillHisto("NRICHHits_vs_Q",goodNHits,fTrack->GetCharge());

        // if(goodNHits < 4) return;
        bool isprim=false;

        ClusVec RICHEmulatedPrimitives =
            *(std::vector<EmulatedL0Primitive>*)GetOutput("L0RICHEmulator.EmulatedL0RICHPrimitives");
        ClusVec::iterator RICHPrim = RICHEmulatedPrimitives.begin();
        for(; RICHPrim != RICHEmulatedPrimitives.end(); ++RICHPrim){
            FillHisto("RICH_dt_vs_Q",RICHPrim->GetAverageTime()-(fTrack->GetCHODTime()*TdcCalib),fTrack->GetCharge());
            if(fabs(RICHPrim->GetAverageTime()-(fTrack->GetCHODTime()*TdcCalib))<10.0 && RICHPrim->GetPrimID("RICH")){

                isprim=true;
                break; // avoid double-counting
            }
        }
        FillHisto("RICH_IsPrim_vs_Q",(int)isprim,(int)fTrack->GetCharge());
        if(!isprim) return;
        fmck2pig=false;
        //Event* MCTruthEvent = GetMCEvent();

        TIter partit(GetMCEvent()->GetKineParts()) ;
        // for(auto i = MCTruthEvent->GetKineParts()){
        bool g1=false;
        bool g2=false;
        while(partit.Next()){
            //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("kaon+") ){
                //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
                fKplus  = ((KinePart*)(*partit));
            }
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("pi+") ){
                //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
                fPiplus  = ((KinePart*)(*partit));
            }
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("pi-")){
                fPiminus = ((KinePart*)(*partit));
            }
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("e+")){
                fEplus = ((KinePart*)(*partit));
            }
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("gamma") && !g1){
                //std::cout << ((KinePart*)(*partit))->GetParticleName().Data() << "1" << std::endl;
                g1=true;
                // fg1     = fMCSimple["gamma"][0]; //for the gamma1
                fg1     = ((KinePart*)(*partit)); //for the gamma1
            }
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("gamma") && g1 && !g2){
                //std::cout << ((KinePart*)(*partit))->GetParticleName().Data() << "2" << std::endl;
                g2      =true;
                fmck2pig=true;
                fg2     = ((KinePart*)(*partit)); //for the gamma2
            }
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("mu+") ){
                //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
                fMuplus  = ((KinePart*)(*partit));
            }
        }

    }


    //Moved multi-track vertex in the beginning to match the code to the one used by Giuseppe
    if (fSpectrometerEvent->GetNCandidates()>1 && fStrawCandidate[fTrack->GetTrackID()].GetMultiCDA()<30.) return;

    // PiP0 Sample
    if(fisK2piEvent && fTrack->GetCharge()==1) K2piAnalysis();
    if(fisTwoPhoton && (fControlTrigger || fIsMC)) Ke3Analysis();

    // Kmu2 sample
    if(fTrack->GetCharge()==1) Kmu2Analysis();
    if(fIsMC) PlotKmu2();
    FillHisto("Alltrigg_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Qtrack",fTrack->GetCharge());
    FillHisto("RICHIsSingle",(Int_t)fIsRICHGoodSingleRing);
    FillHisto("RICHIsMulti",(Int_t)fIsRICHMultiRing);
    FillHisto("IsCHANTI_vs_Charge",(Int_t)fBeamTrack->GetIsCHANTICandidate(), (Int_t)fTrack->GetCharge());
    //cout << "In Kaon Event Analysis" << "\n";
    //std::cout << GetEventHeader()->GetRunID()<< "  " << GetEventHeader()->GetBurstID() << "  " << GetEventHeader()->GetEventNumber() << "  "<< fMMiss<< endl;

    // Pinunu analysis
    if(fTrack->GetCharge()==1){
        if(fPinunuTrigger  || GetWithMC()) PinunuAnalysis();
        if(fControlTrigger || GetWithMC()) PinunuAnalysisCtrl();
    } else {
        if(fPinunuTrigger  || GetWithMC()) PinunuAnalysisNeg();
    }

}

Bool_t KaonTrackAnalysis::Ke3Analysis() {

    TVector3 fPi0Vertex = *(TVector3 *)GetOutput("TwoPhotonAnalysis.Pi0Vertex",fState);
    TLorentzVector fPi0Momentum = *(TLorentzVector *)GetOutput("TwoPhotonAnalysis.Pi0Momentum",fState);
    TLorentzVector fPicMomentum = *(TLorentzVector *)GetOutput("TwoPhotonAnalysis.PicMomentum",fState);
    Double_t fSinglePi0Time = *(Double_t *)GetOutput("TwoPhotonAnalysis.SinglePi0Time",fState);

    FillHisto("Ke3_mmiss_vs_p",fPTrack,fMMiss);
    // Timing pi+ - pi0
    Double_t dtimep0 = fSinglePi0Time-fTrack->GetCHODTime();
    FillHisto("Ke3_dtimep0",dtimep0);
    if (fabs(dtimep0)>5) return 0;

    FillHisto("Ke3_mmiss_vs_p_aftertimepi0",fPTrack,fMMiss);
    FillHisto("Ke3_zvtxpi0_vs_p",fPTrack,fPi0Vertex.Z());
    FillHisto("Ke3_mmiss_vs_zvtxpi0",fPi0Vertex.Z(),fMMiss);
    if (fPi0Vertex.Z()<11500||fPi0Vertex.Z()>18000.) return 0;
    FillHisto("Ke3_mmiss_vs_p_afterzvtxpi0",fPTrack,fMMiss);

    // // Cut against beam background
    if (fVertex.Z()<115000||fVertex.Z()>180000.) return 0;
    FillHisto("Ke3_mmiss_vs_p_beforechanti",fPTrack,fMMiss);

    if (fBeamTrack->GetIsCHANTICandidate()){
        FillHisto("Ke3_mmiss_vs_p_nochanti",fPTrack,fMMiss);

        return 0;
    }

    FillHisto("Ke3_mmiss_vs_p_afterchanti",fPTrack,fMMiss);

    //Kinematic tails suppression
    if(KinematicTails(5)) return 0;

    FillHisto("Ke3_mmiss_vs_p_notails",fPTrack,fMMiss);

    double mm2full = (fBeamTrack->GetMomentum()-fTrack->GetElectronMomentum() - fPi0Momentum).Mag2();;

    FillHisto("Ke3_fullmmiss_vs_p_notails",fPTrack,mm2full);

    //Require electron in the rich using the multi-ring routine
    if(!fIsRICHSingleRing && !fIsRICHMultiRing) return 0;
    if(fRICHStatus != 1) return 0;

    //Segment rejection
    bool isSegments = fSegmentIB<0.5;
    bool isPhoton = fIsIRC || fIsLAV || fIsSAC || fIsSAV;
    Int_t ismuv3 = fTrack->GetMUV3ID()>-1?1:0;
    bool isK2pi = fMMiss < 0.023 && fMMiss > 0.013;

    FillHisto("Ke3_mmiss_vs_p_richm",fPTrack,fMMiss);
    FillHisto("Ke3_fullmmiss_vs_p_richm",fPTrack,mm2full);
    FillHisto("Ke3_ringrad_vs_p",fPTrack,fTrack->GetRICHSingleRadius());
    FillHisto("Ke3_richp_vs_p",fPTrack,fRICHp.P());
    FillHisto("Ke3_multrichmass_vs_p",fPTrack,fMRingMass);
    FillHisto("Ke3_singlerichmass_vs_p",fPTrack,fSRingMass);
    FillHisto("Ke3_nrings",fNRings);

    if(!isPhoton && !ismuv3 && !isK2pi && fabs(mm2full) < 0.01 && !isSegments && fNRings == 1 && fIsRICHSingleRing){
        if(!fTrack->GetIsMIP() && fTrack->GetLKrEovP() > 0.95 && fTrack->GetLKrEovP() < 1.05 && fTrack->GetElectronProb() > 0.99){

            FillHisto("Ke3_singlerichmass_vs_p_final",fPTrack,fSRingMass);
            FillHisto("Ringrad_vs_p",fPTrack,fTrack->GetRICHSingleRadius());
        }
    }
    if(fTrack->GetRICHSingleRadius() < 185) return 0;
    if(fTrack->GetRICHSingleRadius() > 200) return 0;
    if(fSRingMass > 0.08) return 0;

    FillHisto("Ke3_mmiss_vs_p_richs",fPTrack,fMMiss);
    FillHisto("Ke3_fullmmiss_vs_p_richs",fPTrack,mm2full);


    if(isPhoton) return 0;
    FillHisto("Ke3_mmiss_vs_p_phrej",fPTrack,fMMiss);
    FillHisto("Ke3_fullmmiss_vs_p_phrej",fPTrack,mm2full);
    FillHisto("Ke3_eop_vs_p_phrej",fPTrack,fTrack->GetLKrEovP());
    FillHisto("Ke3_emuv1_vs_ecal_phrej",fTrack->GetCalorimetricEnergy(),fTrack->GetMUV1Energy());
    FillHisto("Ke3_emuv2_vs_ecal_phrej",fTrack->GetCalorimetricEnergy(),fTrack->GetMUV2Energy());

    if(ismuv3) return 0;
    FillHisto("Ke3_mmiss_vs_p_murej",fPTrack,fMMiss);
    FillHisto("Ke3_fullmmiss_vs_p_murej",fPTrack,mm2full);
    FillHisto("Ke3_eop_vs_p_murej",fPTrack,fTrack->GetLKrEovP());
    FillHisto("Ke3_emuv1_vs_ecal_murej",fTrack->GetCalorimetricEnergy(),fTrack->GetMUV1Energy());
    FillHisto("Ke3_emuv2_vs_ecal_murej",fTrack->GetCalorimetricEnergy(),fTrack->GetMUV2Energy());


    if(isK2pi) return 0;
    if(isSegments) return 0;
    if(fabs(mm2full) > 0.01) return 0;

    FillHisto("Ke3_mmiss_vs_p_kine",fPTrack,fMMiss);
    FillHisto("Ke3_fullmmiss_vs_p_kine",fPTrack,mm2full);
    FillHisto("Ke3_eop_vs_p_kine",fPTrack,fTrack->GetLKrEovP());
    FillHisto("Ke3_emuv1_vs_ecal_kine",fTrack->GetCalorimetricEnergy(),fTrack->GetMUV1Energy());
    FillHisto("Ke3_emuv2_vs_ecal_kine",fTrack->GetCalorimetricEnergy(),fTrack->GetMUV2Energy());


    PIDStudy(2);

    return 1;
}
Int_t KaonTrackAnalysis::SelectOneParticle() {

    // Define 8 variables according to the numbers of detectors
    // used to define the time of a particle matching a track
    Double_t dTime[9];
    Double_t minTime[9];
    Int_t minTimeID[9];
    for (Int_t jtime=0; jtime<9; jtime++) {
        minTime[jtime] = 9999999.;
        minTimeID[jtime] = -1;
    }

    // Loop on the tracks
    for (Int_t jTrack=0; jTrack<fNTracks; jTrack++) {
        DownstreamParticle *track = &fDownstreamTrack[jTrack];
        UpstreamParticle *beamtrack = &fUpstreamTrack[jTrack];
        Bool_t isRingCandidate = (track->GetRICHMultiIsCandidate() && track->GetRICHSingleIsCandidate()) ? 1 : 0;
        dTime[0] = track->GetTime()-fFineTriggerTime; // Spectrometer
        dTime[1] = track->GetCHODTime()-fFineTriggerTime; // CHOD
        dTime[2] = track->GetLKrTime()-fFineTriggerTime; // LKr
        dTime[3] = beamtrack->GetKTAGTime()-fFineTriggerTime; //KTAG
        dTime[4] = beamtrack->GetTime()-fFineTriggerTime; // GTK
        dTime[5] = isRingCandidate ? track->GetRICHMultiTime()-fFineTriggerTime : -9999999.; // RICH multi
        dTime[6] = isRingCandidate ? track->GetRICHSingleTime()-fFineTriggerTime : -9999999.; // RICH single
        dTime[7] = track->GetMUV1ID()>-1 ? track->GetMUV1Time()-fFineTriggerTime : -9999999.; // MUV1
        dTime[8] = track->GetMUV2ID()>-1 ? track->GetMUV2Time()-fFineTriggerTime : -9999999.; // MUV2
        FillHisto("onetrack_dttrack",dTime[0]);
        FillHisto("onetrack_dtchod" ,dTime[1]);
        FillHisto("onetrack_dtlkr"  ,dTime[2]);
        FillHisto("onetrack_dtktag" ,dTime[3]);
        FillHisto("onetrack_dtgtk"  ,dTime[4]);
        FillHisto("onetrack_dtrichm",dTime[5]);
        FillHisto("onetrack_dtrichs",dTime[6]);
        FillHisto("onetrack_dtmuv1" ,dTime[7]);
        FillHisto("onetrack_dtmuv2" ,dTime[8]);
        for (Int_t jtime=0; jtime<9; jtime++) {
            if (fabs(dTime[jtime])<fabs(minTime[jtime])) {
                minTime[jtime] = dTime[jtime];
                minTimeID[jtime] = jTrack;
            }
        }
    }

    // Test plot
    FillHisto("onetrack_mindttrack",minTime[0]);
    FillHisto("onetrack_mindtchod" ,minTime[1]);
    FillHisto("onetrack_mindtlkr"  ,minTime[2]);
    FillHisto("onetrack_mindtktag" ,minTime[3]);
    FillHisto("onetrack_mindtgtk"  ,minTime[4]);
    FillHisto("onetrack_mindtrichm",minTime[5]);
    FillHisto("onetrack_mindtrichs",minTime[6]);
    FillHisto("onetrack_mindtmuv1" ,minTime[7]);
    FillHisto("onetrack_mindtmuv2" ,minTime[8]);
    Int_t multtrack = 1;
    Int_t trackidmin = minTimeID[1]; // CHOD as a reference
    for (Int_t jtime=2; jtime<9; jtime++) { // straw time not checked (bad resolution)
        if (minTimeID[jtime]>-1 && minTimeID[jtime]!=trackidmin) multtrack++;
    }
    FillHisto("onetrack_chekid",multtrack);
    if (multtrack>1) return -1;
    FillHisto("onetrack_gooddttrack",minTime[0]);
    FillHisto("onetrack_gooddtchod" ,minTime[1]);
    FillHisto("onetrack_gooddtlkr"  ,minTime[2]);
    FillHisto("onetrack_gooddtktag" ,minTime[3]);
    FillHisto("onetrack_gooddtgtk"  ,minTime[4]);
    FillHisto("onetrack_gooddtrichm",minTime[5]);
    FillHisto("onetrack_gooddtrichs",minTime[6]);
    FillHisto("onetrack_gooddtmuv1" ,minTime[7]);
    FillHisto("onetrack_gooddtmuv2" ,minTime[8]);

    return trackidmin;
}

/////////////////////////////////////////////////////////////////////
// PiP0 selection using the 2 photon sample selected independently //
/////////////////////////////////////////////////////////////////////
Bool_t KaonTrackAnalysis::K2piAnalysis() {



    FillHisto("K2pi_alltrigg_mmiss_vs_p",fPTrack,fMMiss);
    if(!fControlTrigger && !fIsMC) return 0;

    TVector3 fPi0Vertex = *(TVector3 *)GetOutput("TwoPhotonAnalysis.Pi0Vertex",fState);
    TLorentzVector fPi0Momentum = *(TLorentzVector *)GetOutput("TwoPhotonAnalysis.Pi0Momentum",fState);
    TLorentzVector fPicMomentum = *(TLorentzVector *)GetOutput("TwoPhotonAnalysis.PicMomentum",fState);
    Double_t fSinglePi0Time = *(Double_t *)GetOutput("TwoPhotonAnalysis.SinglePi0Time",fState);
    Int_t idPhoton1 = *(Int_t *)GetOutput("TwoPhotonAnalysis.P0Gamma1",fState);
    Int_t idPhoton2 = *(Int_t *)GetOutput("TwoPhotonAnalysis.P0Gamma2",fState);

    // Timing pi+ - pi0
    Double_t dtimep0 = fSinglePi0Time-fTrack->GetCHODTime();
    FillHisto("K2pi_dtimep0",dtimep0);
    if (fabs(dtimep0)>5) return 0;

    FillHisto("K2pi_mmiss_vs_p_aftertimepi0",fPTrack,fMMiss);
    FillHisto("K2pi_mmiss_vs_zvtxpi0",fPi0Vertex.Z(),fMMiss);

    if (fPi0Vertex.Z()<11500||fPi0Vertex.Z()>18500.) return 0;
    FillHisto("K2pi_mmiss_vs_p_afterzvtxpi0",fPTrack,fMMiss);

    // // Cut against beam background
    if (fVertex.Z()<115000||fVertex.Z()>180000.) return 0;
    FillHisto("K2pi_mmiss_vs_p_beforechanti",fPTrack,fMMiss);

    if (fBeamTrack->GetIsCHANTICandidate()){
        FillHisto("K2pi_mmiss_vs_p_nochanti",fPTrack,fMMiss);

        return 0;
    }

    FillHisto("K2pi_mmiss_vs_p_afterchanti",fPTrack,fMMiss);

    //Kinematic tails suppression
    if(KinematicTails(2)) return 0;

    FillHisto("K2pi_mmiss_vs_p_notails",fPTrack,fMMiss);

    // bool MultiTrack = MultiTrackSuppression(2);
    bool MultiTrack = false;
    Int_t multflag = fMultAnal[7]->ComputeMultiplicity(fTrack,fBeamTrack);
    MultiTrack = multflag > 0 ? true : false;
    //Remember that the multiplicity is applied for K2pi!!!!
    if(MultiTrack) return 0;
    FillHisto("K2pi_mmiss_vs_p_nomultitrack",fPTrack,fMMiss);

    // // Particle ID
    Int_t positron = PositronDiscriminant();
    Int_t isMuon = MuonDiscriminantRicc(1);
    if (BeamBackgroundCut(0,0)) return 0;
    //Segment rejection
    bool isSegments = fSegmentIB<0.5;

    //Only two photons in time with the track in the LKr
    bool TwoPhotons = fTrack->GetNPhotonLKrCandidates()==2;

    // Add photon rejection for extra cluster in LKr
    Bool_t isPhoton = fIsLAV||fIsIRC||fIsSAC||fIsSAV|| !TwoPhotons ? 1 : 0;

    // Kinematic cut to select
    bool KineCut = fMMiss >= 0.013 && fMMiss < 0.023 && fPTrack >= 9 && fPTrack <=67;

    //RICH identification
    bool richpion = fIsRICHGoodSingleRing && fIsRICHPionLH > 1.2 && fBeamTrack->GetIsGoodGTK() && (fSRingMass > 0.133  && fSRingMass < 0.2); // Full RICH PID

    //Pion sample for calo PID studies
    bool PionForPIDStudy = !isPhoton && !isSegments && KineCut && richpion;

    if(PionForPIDStudy) PIDStudy(1);

    //Pion sample for RICH studies
    bool PionForRICHPID = !positron && !isPhoton && !isMuon && !isSegments && KineCut;
    if(PionForRICHPID) RICHPIDStudy(1);
    //if(PionForRICHPID) RICHSingle(2);

    //Pion sample for kinematic studies
    bool PionForKinematics = !positron && !isPhoton && !isMuon && !isSegments && richpion;
    if(PionForKinematics) KinematicsStudy(2);
    //Pions for multiplicity studies
    //bool PionForMult = !fIsSAC && !fIsIRC && !fIsSAV && !fIsLAV && !isSegments && !isMuon && !positron && TwoPhotons;
    //if(PionForMult) MultiplicityStudy(1);
    //Final K2pi selection
    bool FinalK2piSelection = !isPhoton && !isMuon && !isSegments && richpion && KineCut;
    if(FinalK2piSelection) FillHisto("K2pi_mmiss_vs_p_full",fPTrack,fMMiss);

    // bool ctrlpipi0 = fMMiss > 0.013 && fMMiss < 0.023 && mmissrich > 0.01 && mmissrich < 0.03;
    //bool ctrlpipi0 = fMMiss > 0.013 && fMMiss < 0.023;

    return 1;
}

////////////////////
// Kmu2 selection //
////////////////////
Bool_t KaonTrackAnalysis::Kmu2Analysis() {

    FillHisto("Kmu2_alltrigg_mmiss_vs_p",fPTrack,fMmuMiss);
    if(!fControlTrigger && !fIsMC) return 0;

    // // Clean kinematics
    if (!BeamKinematics(1)) return 0;
    //Cut against beam background
    if (fVertex.Z()<115000||fVertex.Z()>180000.) return 0;
    bool isCHANTI   = (Int_t)fBeamTrack->GetIsCHANTICandidate();
    FillHisto("Kmu2_mmiss_vs_p_beforechanti",fPTrack,fMmuMiss);
    //Segment rejection
    if(isCHANTI)  {
        FillHisto("Kmu2_RVeto_mmiss_vs_p_nochanti",fPTrack,fMmuMiss);
        return 0;
    }
    if (BeamBackgroundCut(0,0)) return 0;

    //Kinematic tails suppression
    if(KinematicTails(1)) return 0;

    FillHisto("Kmu2_mmiss_vs_p_notails",fPTrack,fMmuMiss);

    bool isSegments =  fSegmentIB<0.5;
    // // Particle ID
    Int_t positron = PositronDiscriminant();
    Int_t ismuv3   = fTrack->GetMUV3ID()>-1?1:0;
    Int_t ismuv3pos= fTrack->GetMUV3PosID()>-1?1:0;
    Bool_t isMuon  = fTrack->GetIsMIP() && ismuv3pos;
    Int_t muon_discriminant = MuonDiscriminantRicc(0); // flag for plotting (0 to plot)
    // Photons
    Bool_t isPhoton = fIsLKr||fIsLAV||fIsIRC||fIsSAC||fIsSAV||fIsNewLKr ? 1 : 0;

    FillHisto("Kmu2_mmiss_vs_p_afterchanti",fPTrack,fMmuMiss);

    //Kinematic cuts to select Kmu2
    bool KineCut = fabs(fMmuMiss)<0.005 && fPTrack < 73;

    //RICH positive muon id
    // bool richmuon = fRICHGoodSingleRing && fIsRICHPionLH > 1.2 && fBeamTrack->GetIsGoodGTK();
    bool richmuon = fRICHStatus==2;
    //Muon sample for muon rejection with calo
    bool MuonForPIDStudy = !isPhoton && !isSegments && KineCut && richmuon;
    if(MuonForPIDStudy) PIDStudy(0);

    //Muons for RICH pid studies
    bool MuonForRICHStudies = !positron && !isPhoton && !isSegments && isMuon && fabs(fMmuMiss) < 0.005;
    if(MuonForRICHStudies) RICHPIDStudy(0);
    //Muon sample for random photon rejection studies
    bool MuonForPV = !positron && isMuon && !fTrack->GetIsMulti() && fTrack->GetMuonProb()>0.99 && fIsRICHSingleRing; //no quality on the rich single ring (very loose criteria)

    //Muons for segment rejection random veto
    //bool MuonForSegments = !isPhoton && isMuon;
    if(MuonForPV){
        if(KineCut)
            FillHisto("Kmu2_RVeto_mmiss_vs_p_segments_before",fPTrack,fMmuMiss);

        if(KineCut){
            Bool_t isVetoed = Kmu2PhotonRandomVeto();
            Kmu2NewPhotonRandomVeto();

            if(fPTrack < 35 && fPTrack > 15){
                frvnorm++;
                if(isVetoed)
                    frvph++;
            }
        }

        if(!isPhoton){

            bool MultiTrack = false;
            Int_t multflag = fMultAnal[5]->ComputeMultiplicity(fTrack,fBeamTrack);
            MultiTrack = multflag > 0 ? true : false;

            //Muons for kinematic studies no RICH not to bias the kinematics
            //Full multitrack has to be applied to study the component of
            //kmu3 contamination of the kinematic tails sample
            bool MuonForKinematicStudies = !isPhoton && isMuon && !isSegments && muon_discriminant && !MultiTrack && OneParticleEvent(0);

            if(MuonForKinematicStudies) KinematicsStudy(1);

            if(!MultiTrack && KineCut){

                FillHisto("Kmu2_RMultVeto_mmiss_vs_p",fPTrack,fMmuMiss);
                if(OneParticleEvent(0)){
                    FillHisto("Kmu2_RMultVetoFull_mmiss_vs_p",fPTrack,fMmuMiss);

                    if(!isSegments){
                        FillHisto("Kmu2_RVeto_mmiss_vs_p_segments_after",fPTrack,fMmuMiss);

                        if(fPTrack < 35 && fPTrack > 15)
                            frvmult++;

                    }

                }
            }
        }
    }

    //Muons for multiplicity studies
    //bool MuonForMult = !fIsSAC && !fIsIRC && !fIsSAV && !fIsLAV && !isSegments && isMuon;
    //if(MuonForMult) MultiplicityStudy(0);

    return 1;
}
/////////////////////////////////////
// Pinunu Selection Negative Charge//
/////////////////////////////////////
Bool_t KaonTrackAnalysis::PinunuAnalysisNeg() {
    Bool_t isRingCandidate = fIsRICHMultiRing || fIsRICHSingleRing ? 1 : 0;

    FillHisto("Pimin_start_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_start_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_start_dtchod_vs_cda",fdtchod, fcda);

    // Clean kinematics
    if (!BeamKinematics(3)) return 0;

    //General plots before cuts
    FillHisto("Pimin_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    FillHisto("Pimin_ctr_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_ctr_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);

    if (BeamBackgroundCut(2,0)) return 0;
    if (fisSnakes) return 0;

    FillHisto("Pimin_nobeambg_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_nobeambg_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_nobeambg_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_nobeambg_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_nobeambg_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    FillHisto("Pimin_nobeambg_ctr_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_nobeambg_ctr_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_nobeambg_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);

    if(KinematicTails(1)) return 0;
    FillHisto("Pimin_notails_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_notails_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_notails_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_notails_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_notails_x_vs_y_lkr",fXAtLKr,fYAtLKr);


    FillHisto("Pimin_notails_ctr_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_notails_ctr_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_notails_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);

    // Particle ID with calo
    if (!PionPID(1)) return 0;

    //Multiplicity checks
    FillHisto("Pimin_pidrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_pidrej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_pidrej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_pidrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_pidrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pimin_ibvsmmiss_after_particleid",fMMiss,fSegmentIB);

    // FillHisto("Pimin_pidrej_proton_mmiss_vs_p",fPTrack,fProtonMMiss);
    // FillHisto("Pimin_pidrej_proton_mmiss_vs_zvtx",fVertex.Z(),fProtonMMiss);

    bool NoPhoton = !fIsLKr&&!fIsLAV&&!fIsIRC&&!fIsSAC&&!fIsSAV&&!fIsNewLKr;

    if (NoPhoton) FillHisto("Pimin_ibvsmmiss_test_3pi",fMMiss,fSegmentIB);

    if (fIsRICHMultiRing &&  fabs(fIsRICHPionLH) < 1.2) return 0;
    //if (fIsRICHGoodSingleRing && !fBeamTrack->GetIsGoodGTK()) return 0;

    FillHisto("Pimin_richpi_eop_vs_p", fPTrack,fTrack->GetLKrEovP());
    FillHisto("Pimin_richpi_ecalo_vs_p", fPTrack,fTrack->GetCalorimetricEnergy());
    FillHisto("Pimin_richpi_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_richpi_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_richpi_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_richpi_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_richpi_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pimin_richpi_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    FillHisto("Pimin_richpi_ctr_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_richpi_ctr_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_richpi_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);

    if (fIsLKr) return 0;
    FillHisto("Pimin_mmiss_vs_p_lkr",fPTrack,fMMiss);
    if (fIsNewLKr) return 0;
    FillHisto("Pimin_mmiss_vs_p_auxlkr",fPTrack,fMMiss);
    if (fIsIRC) return 0;
    FillHisto("Pimin_mmiss_vs_p_irc",fPTrack,fMMiss);
    if (fIsSAC) return 0;
    FillHisto("Pimin_mmiss_vs_p_sac",fPTrack,fMMiss);
    if (fIsSAV) return 0;
    FillHisto("Pimin_mmiss_vs_p_sav",fPTrack,fMMiss);
    if (fIsLAV) return 0;
    FillHisto("Pimin_mmiss_vs_p_lav",fPTrack,fMMiss);

    FillHisto("Pimin_phrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_phrej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_phrej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_phrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_phrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pimin_phrej_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    FillHisto("Pimin_phrej_ctr_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_phrej_ctr_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_phrej_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);

    //Multi-track background rejection
    bool isMultiTrack = false;
    //isMultiTrack = MultiTrackSuppression(0);
    Int_t multflag = fMultAnal[1]->ComputeMultiplicity(fTrack,fBeamTrack);
    isMultiTrack = multflag > 0 ? true : false;
    if(isMultiTrack) return 0;
    //double fRAtStraw1 = TMath::Sqrt(fXAtStraw1*fXAtStraw1 + fYAtStraw1*fYAtStraw1) ;


    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pimin_multirej_nostraw_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    FillHisto("Pimin_multirej_nostraw_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_multirej_nostraw_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_multirej_nostraw_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_multirej_nostraw_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_multirej_nostraw_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pimin_multirej_nostraw_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pimin_multirej_nostraw_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

    // Requirement on Momentum
    if (fPTrack<=15 || fPTrack>=35) return 0;

    double region=0;
    if(fMMiss >= 0.068) region = 3;
    if(fMMiss < 0.068 && fMMiss > 0.026) region = 2;
    if(fMMiss <= 0.026 && fMMiss >= 0.01) region = 4;
    if(fMMiss > 0.0   && fMMiss < 0.01) region = 1;
    if(fMMiss <= 0) region = 5;

    bool isStrawMult = OneParticleEvent(2);
    bool isSegments = fSegmentIB<0.5;
    bool isMamba = ExtrapolateBack(region,fBeamTrack->GetIsCHANTICandidate(),1);

    if(!fBeamTrack->GetIsCHANTICandidate() && !isMamba){
        if( !isStrawMult || isSegments){
            if(fPTrack < 35 && fPTrack > 15 )
                FillHisto("Pimin_ke4ctrl_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

            FillHisto("Pimin_ke4ctrl_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pimin_ke4ctrl_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pimin_ke4ctrl_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
            FillHisto("Pimin_ke4ctrl_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pimin_ke4ctrl_x_vs_y_lkr",fXAtLKr,fYAtLKr);
            FillHisto("Pimin_ke4ctrl_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
            FillHisto("Pimin_ke4ctrl_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

        }
    }

    if (isRingCandidate &&  fabs(fIsRICHPionLH) >= 1.2 && fSRingMass < 0.2 && fSRingMass >= 0.133 && fBeamTrack->GetIsGoodGTK()) {
        if(!fBeamTrack->GetIsCHANTICandidate() && !isMamba){
            if( !isStrawMult || isSegments){
                if(fPTrack < 35 && fPTrack > 15 )
                    FillHisto("Pimin_rich_ke4ctrl_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

                FillHisto("Pimin_rich_ke4ctrl_mmiss_vs_p",fPTrack,fMMiss);
                FillHisto("Pimin_rich_ke4ctrl_p_vs_zvtx",fVertex.Z(),fPTrack);
                FillHisto("Pimin_rich_ke4ctrl_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
                FillHisto("Pimin_rich_ke4ctrl_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
                FillHisto("Pimin_rich_ke4ctrl_x_vs_y_lkr",fXAtLKr,fYAtLKr);
                FillHisto("Pimin_rich_ke4ctrl_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
                FillHisto("Pimin_rich_ke4ctrl_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

                if(GetWithMC()) PlotKe4();
            }
        }
    }

    if(!isStrawMult) return 0;
    if(isSegments  ) return 0;

    //Move it up when we understand the jumping mambas!!!!
    if(fBeamTrack->GetIsCHANTICandidate()){
        FillHisto("Pimin_ischanti_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
        FillHisto("Pimin_ischanti_p_vs_zvtx",fVertex.Z(),fPTrack);
        FillHisto("Pimin_ischanti_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Pimin_ischanti_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("Pimin_ischanti_x_vs_y_lkr",fXAtLKr,fYAtLKr);
        if(fPTrack < 35 && fPTrack > 15 )
            FillHisto("Pimin_ischanti_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

        return 0;
    }

    FillHisto("Pimin_isnotchanti_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_isnotchanti_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_isnotchanti_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_isnotchanti_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_isnotchanti_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pimin_isnotchanti_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    if(GetWithMC()) PlotKe4();

    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pimin_multirej_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    FillHisto("Pimin_multirej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_multirej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_multirej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_multirej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_multirej_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pimin_multirej_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pimin_multirej_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

    FillHisto("Pimin_multirej_ctr_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_multirej_ctr_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_multirej_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);


    // Reject pip0 candidates
    bool r1 = fMMiss>0.&&fMMiss<0.01;
    bool r2 = fMMiss>0.026&&fMMiss<0.068;

    for (int jC(0); jC<fBeamTrack->GetNGTKCandidates(); jC++) {
        GigaTrackerCandidate cand = fBeamTrack->GetGTKCandidate(jC);
        Double_t partrack[4] = {cand.GetMomentum()*1000,cand.GetSlopeXZ(),cand.GetSlopeYZ(),Constants::MKCH*0.001};
        TLorentzVector pmom4 = fTool->Get4Momentum(partrack);
        TVector3 pmom = pmom4.Vect();
        TVector3 pini = cand.GetPosition();
        TVector3 vertex = fTool->BlueFieldCorrection(&pmom,pini,1,cand.GetVertex().Z());
        pmom4.SetVectM(pmom,MKCH*0.001);
        Double_t mmiss = (pmom4-fTrack->GetMomentum()).Mag2();
        if(jC==fIdTrack) {

            FillHisto("Pimin_track_mmiss_vs_p",fPTrack,fMMiss);
            continue;
        }
        FillHisto("Pimin_extra_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Pimin_dmmiss_vs_p",fPTrack, mmiss-fMMiss);
    }

    if(isMamba) return 0;

    FillHisto("Pimin_mamba_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
    FillHisto("Pimin_mamba_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_mamba_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_mamba_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_mamba_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_mamba_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pimin_mamba_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pimin_mamba_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);



    // if (isRingCandidate &&  fabs(fIsRICHPionLH) >= 0.7 && fSRingMass < 0.2 && fSRingMass >= 0.133 && fBeamTrack->GetIsGoodGTK()) {
    if (isRingCandidate &&  fabs(fIsRICHPionLH) >= 1.2 && fSRingMass < 0.2 && fSRingMass >= 0.133 && fBeamTrack->GetIsGoodGTK()) {


        FillHisto("Pimin_final_rich_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
        FillHisto("Pimin_final_rich_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Pimin_final_rich_p_vs_zvtx",fVertex.Z(),fPTrack);
        FillHisto("Pimin_final_rich_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
        FillHisto("Pimin_final_rich_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("Pimin_final_rich_x_vs_y_lkr",fXAtLKr,fYAtLKr);
        FillHisto("Pimin_final_rich_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
        FillHisto("Pimin_final_rich_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);


        if(fMMissRICH < -0.04 && fMMiss < -0.04){
            FillHisto("Pimin_final_rich_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
            FillHisto("Pimin_final_rich_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
            FillHisto("Pimin_final_rich_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
        }

        if(fMMissRICH > -0.04 || fMMiss > -0.04){
            FillHisto("Pimin_final_rich_ctr_dtrich_vs_cda",fdtrich,fcda);
            FillHisto("Pimin_final_rich_ctr_dtcedar_vs_cda",fdtcedar,fcda);
            FillHisto("Pimin_final_rich_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
        }

    }

    FillHisto("Pimin_final_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
    FillHisto("Pimin_final_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pimin_final_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pimin_final_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pimin_final_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pimin_final_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pimin_final_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pimin_final_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

    FillHisto("Pimin_final_ctr_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pimin_final_ctr_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pimin_final_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);

    return 1;

}
/////////////////////////////////////
// Pinunu Selection Positive Charge//
/////////////////////////////////////
Bool_t KaonTrackAnalysis::PinunuAnalysis() {

    Bool_t isRingCandidate = fIsRICHMultiRing || fIsRICHSingleRing ? 1 : 0;

    FillHisto("Pinunu_start_dtrich_vs_cda",fdtrich,fcda);
    FillHisto("Pinunu_start_dtcedar_vs_cda",fdtcedar,fcda);
    FillHisto("Pinunu_start_dtchod_vs_cda",fdtchod, fcda);

    if(!isRingCandidate && fTrack->GetCharge()==1) return 0;
    if(!fBeamTrack->GetIsGoodGTK() && fTrack->GetCharge()==1) return 0;

    // Clean kinematics
    if (!BeamKinematics(0)) return 0;

    //General plots before cuts
    FillHisto("Pinunu_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    if(fMMissRICH < -0.04 && fMMiss < -0.04){
        FillHisto("Pinunu_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(fMMissRICH > -0.04 || fMMiss > -0.04){
        FillHisto("Pinunu_ctr_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_ctr_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    // Beam-background rejection (snakes) + CHANTI

    if (BeamBackgroundCut(1,0)) return 0;
    if(fBeamTrack->GetIsCHANTICandidate()) return 0;

    FillHisto("Pinunu_nobeambg_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_nobeambg_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_nobeambg_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_nobeambg_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_nobeambg_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    if(fMMissRICH < -0.04 && fMMiss < -0.04){
        FillHisto("Pinunu_nobeambg_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_nobeambg_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_nobeambg_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(fMMissRICH > -0.04 || fMMiss > -0.04){
        FillHisto("Pinunu_nobeambg_ctr_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_nobeambg_ctr_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_nobeambg_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(KinematicTails(0)) return 0;

    FillHisto("Pinunu_notails_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_notails_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_notails_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_notails_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_notails_x_vs_y_lkr",fXAtLKr,fYAtLKr);


    if(fMMissRICH < -0.04 && fMMiss < -0.04){
        FillHisto("Pinunu_notails_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_notails_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_notails_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(fMMissRICH > -0.04 || fMMiss > -0.04){
        FillHisto("Pinunu_notails_ctr_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_notails_ctr_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_notails_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    // Particle ID with calo
    if (!PionPID(1)) return 0;
    // Asks for RICH
    if (!isRingCandidate) return 0;

    bool isSegments = fSegmentIB<0.5;
    TLorentzVector proton;
    proton.SetVectM(fTrack->GetMomentum().Vect(),0.93827);
    fProtonMMiss = (fBeamTrack->GetMomentum()-proton).Mag2();;

    //Multiplicity checks
    FillHisto("Pinunu_pidrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_pidrej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_pidrej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_pidrej_eop_vs_p",fPTrack,fTrack->GetLKrEovP());
    FillHisto("Pinunu_pidrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_pidrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_ibvsmmiss_after_particleid",fMMiss,fSegmentIB);
    FillHisto("Pinunu_pidrej_proton_mmiss_vs_p",fPTrack,fProtonMMiss);
    FillHisto("Pinunu_pidrej_proton_mmiss_vs_zvtx",fVertex.Z(),fProtonMMiss);

    bool NoPhoton = !fIsLKr&&!fIsLAV&&!fIsIRC&&!fIsSAC&&!fIsSAV&&!fIsNewLKr;

    if (NoPhoton) FillHisto("Pinunu_ibvsmmiss_test_3pi",fMMiss,fSegmentIB);

    FillHisto("Pinunu_segments_mmiss_vs_p",fPTrack,fMMiss);

    if(!fIsRICHSingleRing) return 0;

    FillHisto("Pinunu_richsingle_mmiss_vs_p",fPTrack,fMMiss);

    int time = FineTiming(0);

    FillHisto(Form("Pinunu_singlepi_lr"), fIsRICHPionLH);
    FillHisto(Form("Pinunu_nrichrings"), fNRings);

    if (fabs(fIsRICHPionLH) < 1.2) return 0;
    FillHisto("Pinunu_richpim_nrichrings", fNRings);
    FillHisto("Pinunu_richpim_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_richpim_sringmass_vs_p",fPTrack,fSRingMass);
    FillHisto("Pinunu_richpim_mringmass_vs_p",fPTrack,fMRingMass);
    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pinunu_richpim_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    //Single ring mass cut G.R 09/02/17
    if(fSRingMass < 0.133 || fSRingMass > 0.2) return 0;

    FillHisto("Pinunu_richpis_eop_vs_p", fPTrack,fTrack->GetLKrEovP());
    FillHisto("Pinunu_richpis_ecalo_vs_p", fPTrack,fTrack->GetCalorimetricEnergy());
    FillHisto("Pinunu_richpis_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_richpis_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pinunu_richpis_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    if(fMMissRICH < -0.04 && fMMiss < -0.04){
        FillHisto("Pinunu_richpis_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_richpis_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_richpis_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(fMMissRICH > -0.04 || fMMiss > -0.04){
        FillHisto("Pinunu_richpis_ctr_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_richpis_ctr_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_richpis_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }


    CaloTestsBeforePhotonRejection();
    // Photon rejection
    if (fIsLKr) return 0;
    FillHisto("Pinunu_mmiss_vs_p_lkr",fPTrack,fMMiss);
    if (fIsNewLKr) return 0;
    FillHisto("Pinunu_mmiss_vs_p_auxlkr",fPTrack,fMMiss);
    if (fIsIRC) return 0;
    FillHisto("Pinunu_mmiss_vs_p_irc",fPTrack,fMMiss);
    if (fIsLAV) return 0;
    FillHisto("Pinunu_mmiss_vs_p_lav",fPTrack,fMMiss);
    if (fIsSAC) return 0;
    FillHisto("Pinunu_mmiss_vs_p_sac",fPTrack,fMMiss);
    if (fIsSAV) return 0;
    FillHisto("Pinunu_mmiss_vs_p_sav",fPTrack,fMMiss);


    double region=0;
    if(fMMiss >= 0.068) region = 3;
    if(fMMiss < 0.068 && fMMiss > 0.026) region = 2;
    if(fMMiss <= 0.026 && fMMiss >= 0.01) region = 4;
    if(fMMiss > 0.0   && fMMiss < 0.01) region = 1;
    if(fMMiss <= 0) region = 5;

    bool isMamba = ExtrapolateBack(region,fBeamTrack->GetIsCHANTICandidate(),0);


    FillHisto("Pinunu_beforemamba_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
    FillHisto("Pinunu_beforemamba_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_beforemamba_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_beforemamba_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_beforemamba_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_beforemamba_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_beforemamba_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pinunu_beforemamba_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);
    FillHisto("Pinunu_beforemamba_proton_mmiss_vs_p",fPTrack,fProtonMMiss);
    FillHisto("Pinunu_beforemamba_proton_mmiss_vs_zvtx",fVertex.Z(),fProtonMMiss);

    if(isMamba) return 0;


    FillHisto("Pinunu_phrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_phrej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_phrej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_phrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_phrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pinunu_phrej_mmiss_vs_mmissrich", fMMiss,fMMissRICH);


    if(fMMissRICH < -0.04 && fMMiss < -0.04){
        FillHisto("Pinunu_phrej_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_phrej_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_phrej_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(fMMissRICH > -0.04 || fMMiss > -0.04){
        FillHisto("Pinunu_phrej_ctr_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_phrej_ctr_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_phrej_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    //Multi-track background rejection
    bool isMultiTrack = false;
    Int_t multflag = fMultAnal[0]->ComputeMultiplicity(fTrack,fBeamTrack);
    isMultiTrack = multflag > 0 ? true : false;
    if(isMultiTrack) return 0;


    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pinunu_multirej_nostraw_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    FillHisto("Pinunu_multirej_nostraw_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_multirej_nostraw_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_multirej_nostraw_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_multirej_nostraw_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_multirej_nostraw_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_multirej_nostraw_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pinunu_multirej_nostraw_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

    bool isStrawMult = OneParticleEvent(1);

    if(!isMamba){

        if( !isStrawMult || isSegments){

            if(fPTrack < 35 && fPTrack > 15 )
                FillHisto("Pinunu_ke4ctrl_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

            FillHisto("Pinunu_ke4ctrl_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_ke4ctrl_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_ke4ctrl_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
            FillHisto("Pinunu_ke4ctrl_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_ke4ctrl_x_vs_y_lkr",fXAtLKr,fYAtLKr);
            FillHisto("Pinunu_ke4ctrl_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
            FillHisto("Pinunu_ke4ctrl_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

            if(GetWithMC()) PlotKe4();
        }
    }

    if(!isStrawMult)  return 0;
    if(isSegments) return 0;

    if(fPTrack < 35 && fPTrack > 15 )
        FillHisto("Pinunu_multirej_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    FillHisto("Pinunu_multirej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_multirej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_multirej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_multirej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_multirej_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_multirej_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pinunu_multirej_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

    if(fMMissRICH < -0.04 && fMMiss < -0.04){
        FillHisto("Pinunu_multirej_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_multirej_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_multirej_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(fMMissRICH > -0.04 || fMMiss > -0.04){
        FillHisto("Pinunu_multirej_ctr_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_multirej_ctr_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_multirej_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    for (int jC(0); jC<fBeamTrack->GetNGTKCandidates(); jC++) {
        GigaTrackerCandidate cand = fBeamTrack->GetGTKCandidate(jC);
        Double_t partrack[4] = {cand.GetMomentum()*1000,cand.GetSlopeXZ(),cand.GetSlopeYZ(),Constants::MKCH*0.001};
        TLorentzVector pmom4 = fTool->Get4Momentum(partrack);
        TVector3 pmom = pmom4.Vect();
        TVector3 pini = cand.GetPosition();
        TVector3 vertex = fTool->BlueFieldCorrection(&pmom,pini,1,cand.GetVertex().Z());
        pmom4.SetVectM(pmom,MKCH*0.001);
        Double_t mmiss = (pmom4-fTrack->GetMomentum()).Mag2();
        if(jC==fIdTrack) {

            FillHisto("Pinunu_track_mmiss_vs_p",fPTrack,mmiss);
            continue;
        }
        FillHisto("Pinunu_extra_mmiss_vs_p",fPTrack,mmiss);
        FillHisto("Pinunu_dmmiss_vs_p",fPTrack, mmiss-fMMiss);
    }

    // Requirement on Momentum
    //if (fPTrack<=15 || fPTrack>=35) return 0;

    CaloTestsAfterPhotonRejection();

    FillHisto("Pinunu_final_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
    FillHisto("Pinunu_final_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_final_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_final_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Pinunu_final_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_final_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_final_mmiss_vs_fRAtStraw1",fRAtStraw1,fMMiss);
    FillHisto("Pinunu_final_rstraw1_vs_zvtx",fVertex.Z(),fRAtStraw1);

    if(GetWithMC()) PlotKe4();

    bool rpipi  = fMMiss<0.021 && fMMiss>0.015;
    bool rpipipi= fMMiss>0.068;
    bool rmunu  = fMMiss<0.0;

    if(rpipi){
        fnpipi++;
        FillHisto("Pipi_events",GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
    }

    if(rpipipi){
        fnpipipi++;
        FillHisto("Pipipi_events",GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
    }

    if(rmunu){
        fnmunu++;
        FillHisto("Munu_events",GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
    }

    if(fMMissRICH < -0.04 && fMMiss < -0.04){
        FillHisto("Pinunu_final_ctr_bb_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_final_ctr_bb_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_final_ctr_bb_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(fMMissRICH > -0.04 || fMMiss > -0.04){
        FillHisto("Pinunu_final_ctr_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_final_ctr_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_final_ctr_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    KinematicsStudy(0);
    TestEventsInSignalRegion();

    bool rpipi0= fMMiss>0.01&&fMMiss<0.026&&fMMissRICH>0.01&&fMMissRICH<0.03;

    if(rpipi0||(region==1)||(region ==2 )){
        fLKrAnal->SetTrackPosAtLKr(fXAtLKr,fYAtLKr,Constants::zLKr);
        fLKrAnal->SetTrackClPosAtLKr(fTrack->GetLKrPosition());
        fLKrAnal->SetTrackTime(fTrack->GetDownstreamTime());
        fLKrAnal->SetTrackCHODTime(fTrack->GetCHODTime());
        fLKrAnal->ClustersLeft(0,fMMiss);
        fIRCAnal->Clear();
        fSACAnal->Clear();
        fSAVAnal->Clear();
        fIRCAnal->MakeCandidate(fTrack->GetDownstreamTime(),fIRCEvent);
        fSACAnal->MakeCandidate(fTrack->GetDownstreamTime(),fSACEvent);
        fSAVAnal->MakeCandidate(fTrack->GetDownstreamTime(),fSAVEvent);
    }

    if(region ==1 ){
        FillHisto("Pinunu_r1_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_r1_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_r1_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }

    if(region ==2){
        FillHisto("Pinunu_r2_dtrich_vs_cda",fdtrich,fcda);
        FillHisto("Pinunu_r2_dtcedar_vs_cda",fdtcedar,fcda);
        FillHisto("Pinunu_r2_dtrich_vs_dtcedar",fdtrich,fdtcedar);
    }


    return 1;
}
Bool_t KaonTrackAnalysis::PinunuAnalysisCtrl(){

    FillHisto("Ctrl_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Ctrl_xvtx_vs_zvtx",fVertex.Z(),fVertex.X());
    FillHisto("Ctrl_yvtx_vs_zvtx",fVertex.Z(),fVertex.Y());
    if(fVertex.Z()>103000 && fVertex.Z()<108000)
        FillHisto("Ctrl_yvtx_vs_xvtx_front",fVertex.X(),fVertex.Y());
    if(fVertex.Z()>160000 && fVertex.Z()<165000)
        FillHisto("Ctrl_yvtx_vs_xvtx_end",fVertex.X(),fVertex.Y());

    Bool_t isRingCandidate = fIsRICHMultiRing || fIsRICHSingleRing ? 1 : 0;
    if(!isRingCandidate && fTrack->GetCharge()==1) return 0;
    if(!fBeamTrack->GetIsGoodGTK() && fTrack->GetCharge()==1) return 0;

    if (!BeamKinematics(1)) return 0;

    //1st flag ==1 for plotting ,2nd flag == 1 ctrl sample
    if (BeamBackgroundCut(0,1)) return 0;

    FillHisto("Ctrl_nobeambg_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_nobeambg_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_nobeambg_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_nobeambg_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_nobeambg_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    if(KinematicTails(1)) return 0;

    FillHisto("Ctrl_notails_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_notails_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_notails_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_notails_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_notails_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    // Particle ID with calo
    if (!PionPID(0)) return 0;
    // Asks for RICH

    bool isSegments = fSegmentIB<0.5;

    FillHisto("Ctrl_pidrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_pidrej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_pidrej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_pidrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_pidrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Ctrl_ibvsmmiss_after_particleid",fMMiss,fSegmentIB);

    bool NoPhoton = !fIsLKr&&!fIsLAV&&!fIsIRC&&!fIsSAC&&!fIsSAV&&!fIsNewLKr;

    if (NoPhoton) FillHisto("Ctrl_ibvsmmiss_test_3pi",fMMiss,fSegmentIB);

    //Remove comment after testing
    //if (isSegments) return 0;

    int time = FineTiming(1);

    if(fPTrack < 35 && fPTrack > 15 ){
        if(fabs(fIsRICHPionLH) >= 1.2 && fTrack->GetCharge()==1) {
            if(fSRingMass > 0.133 && fSRingMass < 0.2 ){
                if(fMMiss < 0.021 && fMMiss > 0.015){
                    FillHisto("Norm_before_finetiming",GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
                    fnnorm_before++;
                }
            }
        }
    }


    if(fabs(fIsRICHPionLH) < 1.2 && fTrack->GetCharge()==1) return 0;
    if((fSRingMass < 0.133 || fSRingMass > 0.2) && fTrack->GetCharge()==1) return 0;

    FillHisto("Ctrl_richfull_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_richfull_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_richfull_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_richfull_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_richfull_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    fisRICHRef = L0WithRICH();
    fisNewCHODRef = L0WithNewCHOD();
    fBRPiPi0  = fMMiss > 0.013 && fMMiss < 0.023 && fMMissRICH > 0.01 && fMMissRICH < 0.03;

    KinematicsStudy(3);
    if(fPTrack < 35 && fPTrack > 15 ){

        FillHisto("Ctrl_richfull_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

        //Increment the counter counting the number of normalization
        //events per burst
        if(fMMiss < 0.021 && fMMiss > 0.015){
            FillHisto("Norm_events",GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
            fnnorm++;
            //
            //    std::cout << "GR headache ----" << endl;
            //    std::cout << "Run ID***" << "Burst ID***" << " EventID***" << " MMiss2***"  << endl;
            //    std::cout << GetEventHeader()->GetRunID()<< "  " << GetEventHeader()->GetBurstID() << "  " << GetEventHeader()->GetEventNumber() << "  "<< fMMiss<< endl;
        }
        //if(!fIsMC){
        //
        //std::cout << "---------Event information for PiPi0 Norm ---------" << std::endl;
        //std::cout << "Run ID***" << "Burst ID***" << " EventID***" << " MMiss2***"  << endl;
        //std::cout << GetEventHeader()->GetRunID()<< "  " << GetEventHeader()->GetBurstID() << "  " << GetEventHeader()->GetEventNumber() << "  "<< fMMiss<< endl;
        //std::cout << "------------------------------------ ---------" << std::endl;
        //}


    }
    //Evaluate LAV L0 trigger efficiency using K2pi with 2 photons in the LKr and
    //MUV3/NewCHOD/RICH using k2pi from the background region
    L0TriggerEfficiency();

    SelectK2piForRLKrStudy();

    FillHisto("Ctrl_nlavcand",fLAVEvent->GetNCandidates());

    Int_t k2piformultrv = SelectK2piForMultRV();

    fLAVMatching->SetReferenceTime(fTrack->GetRICHSingleTime());
    bool is2LAVTommaso = fLAVAnal->IsPhotonsInLAV(2,fTrack->GetRICHSingleTime(),fLAVMatching,fLAVEvent);

    if(is2LAVTommaso){

        FillHisto("Ctrl_2lavs0_mmiss_vs_p",fPTrack,fMMiss);


        // Photon rejection random veto
        if (!fIsLKr&&!fIsNewLKr)
            FillHisto("Ctrl_2lavs0_mmiss_vs_p_lkr",fPTrack,fMMiss);
        if (!fIsIRC&&!fIsSAC&&!fIsSAC)
            FillHisto("Ctrl_2lavs0_mmiss_vs_p_sav",fPTrack,fMMiss);


        if(!fIsLKr&&!fIsNewLKr&&!fIsIRC&&!fIsSAC&&!fIsSAV){

            if(fisRICHRef)
                FillHisto("Ctrl_L0Calo", fTrack->GetLKrEovP()*fPTrack, fCaloL0);

            //Multi-track background rejection
            bool isMult2LAVs = false;
            Int_t multflag = fMultAnal[4]->ComputeMultiplicity(fTrack,fBeamTrack);
            isMult2LAVs = multflag > 0 ? true : false;

            FillHisto("Ctrl_2lavs1_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Ctrl_2lavs1_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
            FillHisto("Ctrl_2lavs1_ringrad_vs_p",fPTrack,fTrack->GetRICHSingleRadius());
            FillHisto("Ctrl_2lavs1_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Ctrl_2lavs1_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Ctrl_2lavs1_x_vs_y_lkr",fXAtLKr,fYAtLKr);

        }
    }

    // Photon rejection
    if (fIsLKr) return 0;
    FillHisto("Ctrl_mmiss_vs_p_lkr",fPTrack,fMMiss);
    if (fIsNewLKr) return 0;
    FillHisto("Ctrl_mmiss_vs_p_auxlkr",fPTrack,fMMiss);
    if (fIsIRC) return 0;
    FillHisto("Ctrl_mmiss_vs_p_irc",fPTrack,fMMiss);
    if (fIsLAV) return 0;
    FillHisto("Ctrl_mmiss_vs_p_lav",fPTrack,fMMiss);
    if (fIsSAC) return 0;
    FillHisto("Ctrl_mmiss_vs_p_sac",fPTrack,fMMiss);
    if (fIsSAV) return 0;
    FillHisto("Ctrl_mmiss_vs_p_sav",fPTrack,fMMiss);



    FillHisto("Ctrl_phrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_phrej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_phrej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_phrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_phrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);


    //Multi-track background rejection
    bool isMultiTracknew = false;
    //isMultiTrack = MultiTrackSuppression(0);
    Int_t multflagnew = fMultAnal[8]->ComputeMultiplicity(fTrack,fBeamTrack);
    isMultiTracknew = multflagnew > 0 ? true : false;
    if(isMultiTracknew) return 0;


    FillHisto("Ctrl_multirej_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_multirej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_multirej_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_multirej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_multirej_x_vs_y_lkr",fXAtLKr,fYAtLKr);

    if(fPTrack < 35 && fPTrack > 15)
        FillHisto("Ctrl_multirej_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    return 1;
}

Bool_t KaonTrackAnalysis::KinematicTails(Int_t flag) {

    // Internal track consistency
    TRecoSpectrometerCandidate *spectrack = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fTrack->GetTrackID());
    Double_t dthetax = spectrack->GetSlopeXBeforeFit()-spectrack->GetSlopeXBeforeMagnet();
    Double_t dthetay = spectrack->GetSlopeYBeforeFit()-spectrack->GetSlopeYBeforeMagnet();
    Double_t quality1 = spectrack->GetCombinationTotalQuality();
    if (!flag) {
        FillHisto("Pinunu_tails_spec_dthetax",dthetax,fMMiss);
        FillHisto("Pinunu_tails_spec_dthetay",dthetay,fMMiss);
        FillHisto("Pinunu_tails_spec_qual1",quality1,fMMiss);
        FillHisto("Pinunu_tails_spec_nhits",spectrack->GetMomentum()*0.001,spectrack->GetNHits());
        FillHisto("Pinunu_tails_vertex_xz",fVertex.Z(),fBeamTrack->GetVertex().X());
        FillHisto("Pinunu_tails_vertex_yz",fVertex.Z(),fBeamTrack->GetVertex().Y());
        FillHisto("Pinunu_tails_kaon_xy",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
        Double_t thetapk = fTrack->GetMomentum().Angle(fBeamTrack->GetMomentum().Vect());
        FillHisto("Pinunu_tails_thetavsp",fPTrack,thetapk);
    }

    // Timing
    Double_t tgtk = fBeamTrack->GetTime();
    Double_t dt_gtkktag = tgtk-fBeamTrack->GetKTAGTime();
    Double_t dt_gtkchod = tgtk-fTrack->GetCHODTime();
    Double_t dt_gtklkr  = tgtk-fTrack->GetLKrTime();
    Double_t dt_gtkspec = tgtk-fTrack->GetTime();
    if (!flag) {
        FillHisto("Pinunu_tails_dktagVSdgtk_chod",tgtk-fTrack->GetCHODTime(),fBeamTrack->GetKTAGTime()-fTrack->GetCHODTime());
        FillHisto("Pinunu_tails_dchodVSdktag_gtk",dt_gtkktag,dt_gtkchod);
        FillHisto("Pinunu_tails_dtktag",dt_gtkktag,fMMiss);
        FillHisto("Pinunu_tails_dtchod",dt_gtkchod,fMMiss);
        FillHisto("Pinunu_tails_dtlkr",dt_gtklkr,fMMiss);
        FillHisto("Pinunu_tails_dtspec",dt_gtkspec,fMMiss);
    }

    // Apply cuts
    if (fGTKFlag) return 0; // Do not apply further cuts if the GTK is not in the analysis
    if (dthetax<-0.0003) return 1;//check
    if (dthetax>0.0003) return 1;//check
    if (fabs(dthetay)>0.001) return 1;//check
    if (quality1>4) return 1;//check
    if (fYear==2015) {
        if (fVertex.X()>-85.25+0.0011167*fVertex.Z()) return 1;
        if (fVertex.X()<-139.34+0.001208*fVertex.Z()) return 1;
        if (fabs(dt_gtkktag)>1.4) return 1;
    }
    if (fYear==2016) {
        if (spectrack->GetNHits()>42) return 1;//check
        if (spectrack->GetNHits()<15) return 1;//check
        if (fVertex.X()>-93.9+0.0012*fVertex.Z()) return 1;//check
        if (fVertex.X()<-148.53+0.00122*fVertex.Z()) return 1;//check
        if (fVertex.Y()>3.6+0.000112*fVertex.Z()) return 1;//check
        if (fVertex.Y()<-5.4-0.00009464*fVertex.Z()) return 1;//check
        if(fabs(dt_gtkchod)>1.1) return 1; // Strong cut //check
        if(fabs(dt_gtkktag)>1.1) return 1; // Strong cut //check
    }

    // Final check
    if (!flag) {
        Double_t kaon_thetax = fBeamTrack->GetMomentum().Px()/fBeamTrack->GetMomentum().Pz()-fTrimTheta;
        Double_t kaon_thetay = fBeamTrack->GetMomentum().Py()/fBeamTrack->GetMomentum().Pz();
        FillHisto("Pinunu_tails_dktagVSdgtk_chod_check",tgtk-fTrack->GetCHODTime(),fBeamTrack->GetKTAGTime()-fTrack->GetCHODTime());
        FillHisto("Pinunu_tails_kaon_xy_check",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
        FillHisto("Pinunu_tails_kaon_p_check",fPBeamTrack);
        FillHisto("Pinunu_tails_kaon_thetaxy_check",kaon_thetax,kaon_thetay);
    }

    return 0;
}

//////////////////////////////////
// Minuit minimization function //
//////////////////////////////////
void KaonTrackAnalysis::FuncCDA(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t iflag) {
    Double_t deyd = fyPaipD-fyKaonD;
    Double_t dexd = fxPaipD-fxKaonD;
    Double_t deyp = fyprimePaip-fyprimeKaon;
    Double_t dexp = fxprimePaip-fxprimeKaon;
    Double_t bfield = 1.6678; // T
    Double_t dey = deyd+deyp*(par[0]-99460.)-(0.3*bfield*(par[0]-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(par[0]-99460.)/2.;
    if (par[0]<96960.) {
        Double_t deyend = deyd+deyp*(96960.-99460.)-(0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(96960.-99460.)/2.;
        dey = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(par[0]-96960.);
        if (par[0]<95860.) {
            Double_t deyend2 = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(95860.-96960.);
            dey = deyend2+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(par[0]-95860.)+(0.3*bfield*(par[0]-95860.)*(1./fpPaip-1./fpKaon)*0.001)*(par[0]-95860.)/2.;
        }
    }
    Double_t dex = dexd+dexp*(par[0]-99460.);
    f = dey*dey+dex*dex;
}

Double_t KaonTrackAnalysis::BeamCDA(Double_t zvtx) {
    Double_t zvertex = zvtx;
    Double_t deyd = fyPaipD-fyKaonD;
    Double_t dexd = fxPaipD-fxKaonD;
    Double_t deyp = fyprimePaip-fyprimeKaon;
    Double_t dexp = fxprimePaip-fxprimeKaon;
    Double_t bfield = 1.6678; // T
    Double_t dey = deyd+deyp*(zvertex-99460.)-(0.3*bfield*(zvertex-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(zvertex-99460.)/2.;
    if (zvertex<96960.) {
        Double_t deyend = deyd+deyp*(96960.-99460.)-(0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(96960.-99460.)/2.;
        dey = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(zvertex-96960.);
        if (zvertex<95860.) {
            Double_t deyend2 = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(95860.-96960.);
            dey = deyend2+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaip-1./fpKaon)*0.001)*(zvertex-95860.)+(0.3*bfield*(zvertex-95860.)*(1./fpPaip-1./fpKaon)*0.001)*(zvertex-95860.)/2.;
        }
    }
    Double_t dex = dexd+dexp*(zvertex-99460.);
    return sqrt(dey*dey+dex*dex);
}

Bool_t KaonTrackAnalysis::BeamBackgroundCut(Int_t flag, bool ctrl) {

    //Protection against high multiplicity in the GTK
    if(flag==1) FillHisto("Pinunu_NGTKCand",fBeamTrack->GetNGTKCandidates());
    if(fBeamTrack->GetNGTKCandidates() > 4) return 1; //check
    // Test GTK candidate for bad GTK association
    bool isAGoodVertex = false;
    for (int jG(0); jG<fBeamTrack->GetNGTKCandidates()-1; jG++) {
        GigaTrackerCandidate cand= fBeamTrack->GetGTKCandidate(jG);
        Double_t zvertex = cand.GetVertex().Z();

        if(flag==1){
            FillHisto("MultiGTK_zvtxvsp",zvertex,fPTrack);
            FillHisto("MultiGTK_zvsz",zvertex,fVertex.Z());
            FillHisto("MultiGTK_mvsdz",zvertex-fVertex.Z(),fMMiss);
        }
        Double_t partrack[4] = {cand.GetMomentum()*1000,cand.GetSlopeXZ(),cand.GetSlopeYZ(),0.493667};
        TLorentzVector pmom = fTool->Get4Momentum(partrack);
        Double_t mmiss = (pmom-fTrack->GetMomentum()).Mag2();
        if(flag==1)
            FillHisto("MultiGTK_mvsm",mmiss,fMMiss);

        if (zvertex<105000.&&zvertex>100000.) isAGoodVertex = true;
    }
    if (isAGoodVertex) return 1;

    if(flag==1) FillHisto("Pinunu_badgtkmatching_mmiss_vs_p",fPTrack,fMMiss);
    if(flag==2) FillHisto("Pimin_badgtkmatching_mmiss_vs_p",fPTrack,fMMiss);


    if(flag==1) FillHisto("Snake_r_vs_z",fVertex.Z(),fRAtStraw1);
    if(flag==1&& fPTrack > 15 && fPTrack < 35) FillHisto("Snake_r_vs_z_1535",fVertex.Z(),fRAtStraw1);
    if(ctrl) FillHisto("Snake_ctrl_r_vs_z",fVertex.Z(),fRAtStraw1);
    if(ctrl && fPTrack > 15 && fPTrack < 35) FillHisto("Snake_ctrl_r_vs_z_1535",fVertex.Z(),fRAtStraw1);
    if(flag==2) FillHisto("Snake_pimin_r_vs_z",fVertex.Z(),fRAtStraw1);
    //Anti-snakes zvtx vs fRAtStraw1 cut
    double b1 = -0.004;
    double a1 = 315-b1*105000.;
    double r115 = a1+b1*115000.;
    double b2 = -(900-r115)/10000.;
    double a2 = 900-b2*105000.;
    double b3 = -0.00983333333;
    double a3 = 780-b3*105000.;
    double cut1 = a1+b1*fVertex.Z();
    double cut2 = a2+b2*fVertex.Z();
    double cut3 = a3+b3*fVertex.Z();
    bool isSignal = fRAtStraw1>cut1 && fRAtStraw1>cut2 && fRAtStraw1<cut3 && fVertex.Z()<165000.;
    fisSnakes = !isSignal;
    //cout << "control trigger = " << fControlTrigger << endl;
    //cout << "R In BB = " << fRAtStraw1 << " x = " << fXAtStraw1 << " y = " << fYAtStraw1<< endl;
    if(flag==1 && fisSnakes) FillHisto("Snakepos_r_vs_z",fVertex.Z(),fRAtStraw1);
    if(flag==2 && fisSnakes) FillHisto("Snakepos_pimin_r_vs_z",fVertex.Z(),fRAtStraw1);
    if(ctrl && fisSnakes) FillHisto("Snakepos_ctrl_r_vs_z",fVertex.Z(),fRAtStraw1);

    // Test GTK3 hit
    bool isInteraction= false;
    bool highToT = false;
    TRecoSpectrometerCandidate *strack = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fTrack->GetTrackID());
    TVector3 pmom;
    TVector3 posGTK3 = fTool->GetPositionUpstream(strack,&pmom,102400.); // GTK3
    TVector3 posFCOE = fTool->GetPositionUpstream(strack,&pmom,101255.); // End final collimator
    TVector3 posDIPO = fTool->GetPositionUpstream(strack,&pmom,99460.); // End last achromat dipole
    for (int jH(0); jH<fBeamTrack->GetNGTK3Hits(); jH++) {
        Int_t idh = fBeamTrack->GetGTK3Hit(jH);
        TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*) fGigaTrackerEvent->GetHit(idh);
        Double_t dist = (hit->GetPosition()-posGTK3).XYvector().Mod();
        Double_t dx = (hit->GetPosition()-posGTK3).X();
        Double_t dy = (hit->GetPosition()-posGTK3).Y();
        Double_t dist2 = (hit->GetPosition()-fBeamTrack->GetPosition()).XYvector().Mod();
        if (!fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack<35) FillHisto("MultiGTKHit_dvsz",fVertex.Z(),dist);
        if (dist2>0)
            if(flag==1)
                FillHisto("MultiGTKHit_dvsz2",fVertex.Z(),dist2);
        if(flag==1){
            FillHisto("MultiGTKHit_dxdy",dx,dy);
            FillHisto("MultiGTKHit_xy",hit->GetPosition().X(),hit->GetPosition().Y());
            FillHisto("MultiGTKHit_tot",fVertex.Z(),hit->GetToT());
        }

        if (dist<24.) isInteraction = true; // Against scattering tails in straws
        if (hit->GetToT()>23) highToT = true;
    }
    if (!isInteraction && !fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack<35) {
        if(flag==1){
            FillHisto("MultiGTKHit_xytrack_gtk3",posGTK3.X(),posGTK3.Y());
            FillHisto("MultiGTKHit_xytrack_fcoe",posFCOE.X(),posFCOE.Y());
            FillHisto("MultiGTKHit_xytrack_dipo",posDIPO.X(),posDIPO.Y());
        }
    }
    if (fabs(posGTK3.X())<30.&&fabs(posGTK3.Y())<15.) isInteraction = true; // Against inefficinecy in GTK3

    for (int jH(0); jH<fBeamTrack->GetNGTK1Hits(); jH++) {
        Int_t idh = fBeamTrack->GetGTK1Hit(jH);
        TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*) fGigaTrackerEvent->GetHit(idh);

        if(flag==1)
            FillHisto("MultiGTKHit_tot",fVertex.Z(),hit->GetToT());
        if (hit->GetToT()>23) highToT = true;
    }
    for (int jH(0); jH<fBeamTrack->GetNGTK2Hits(); jH++) {
        Int_t idh = fBeamTrack->GetGTK2Hit(jH);
        TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*) fGigaTrackerEvent->GetHit(idh);
        if(flag==1)
            FillHisto("MultiGTKHit_tot",fVertex.Z(),hit->GetToT());
        if (hit->GetToT()>23) highToT = true;
    }

    //cout << "fpnn trigg " << fPinunuTrigger <<endl;
    //Beam background cuts
    if (highToT) return 1;
    if (isInteraction) return 1;
    ////  if (fBeamTrack->GetNGTK2Hits()<fBeamTrack->GetNGTK3Hits()) return 1;
    if(flag==1 && !fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack < 35) FillHisto("Snake_r_vs_z_aftercut",fVertex.Z(),fRAtStraw1);
    if(flag==2 && !fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack < 35) FillHisto("Snake_pimin_r_vs_z_aftercut",fVertex.Z(),fRAtStraw1);
    //if(flag==1 && fPinunuTrigger && fisSnakes) return 1;
    if(ctrl && !fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack < 35) FillHisto("Snake_ctrl_r_vs_z_aftercut",fVertex.Z(),fRAtStraw1);
    //if(ctrl && fPTrack < 35 && fPTrack > 15 && fisSnakes) return 1;

    FillHisto("Pimin_IsCHANTI_vs_Charge",(Int_t)fBeamTrack->GetIsCHANTICandidate(), (Int_t)fTrack->GetCharge());
    if(flag==2){
        FillHisto("Pimin_fidvol_p_vs_zvtx",fVertex.Z(),fPTrack);
        FillHisto("Pimin_fidvol_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Pimin_fidvol_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("Pimin_fidvol_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }
    //if(flag==2 && (fVertex.Z()<102000||fVertex.Z()>165000.)) return 1;

    // Vertex cut
    if (fVertex.Z()<fZVertexCut||fVertex.Z()>180000.) {
        if(flag==1){
            FillHisto("Pinunu_fidvolout_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_fidvolout_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_fidvolout_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_fidvolout_x_vs_y_lkr",fXAtLKr,fYAtLKr);
        }
        // if(flag==1 || ctrl) return 1;
        if(flag==1 || ctrl || flag==2) return 1;
    }
    if(flag==1){
        FillHisto("Pinunu_fidvol_p_vs_zvtx",fVertex.Z(),fPTrack);
        FillHisto("Pinunu_fidvol_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Pinunu_fidvol_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("Pinunu_fidvol_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }

    // CHANTI cut
    if (fBeamTrack->GetIsCHANTICandidate()) {
        if(flag==2){
            FillHisto("Pinunu_chanti_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_chanti_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_chanti_dtrich_vs_p",fPTrack,fBeamTrack->GetCHANTITime()-fTrack->GetRICHSingleTime());
            FillHisto("Pinunu_chanti_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_chanti_x_vs_y_lkr",fXAtLKr,fYAtLKr);
        }
        if(flag==1 || flag==2) return 0;
        else return 1;
    }

    return 0;
}
Bool_t KaonTrackAnalysis::BeamKinematics(Int_t flag) {
    fTrimTheta = fTrimKick/fPBeamTrack;

    Double_t kaon_thetax = fBeamTrack->GetMomentum().Px()/fBeamTrack->GetMomentum().Pz() - fTrimTheta;
    Double_t kaon_thetay = fBeamTrack->GetMomentum().Py()/fBeamTrack->GetMomentum().Pz();
    if (flag==0) {
        FillHisto("Pinunu_kaon_p",fPBeamTrack);
        FillHisto("Pinunu_kaon_thetay_vs_thetax",kaon_thetax, kaon_thetay);
        FillHisto("Pinunu_kaon_thx_vs_pk",fPBeamTrack,kaon_thetax);
        FillHisto("Pinunu_kaon_thy_vs_pk",fPBeamTrack,kaon_thetay);

    }

    Double_t rth = sqrt((kaon_thetax-fThetaXCenter)*(kaon_thetax-fThetaXCenter)+(kaon_thetay-fThetaYCenter)*(kaon_thetay-fThetaYCenter));

    if (fYear==2016) {
        if (rth>0.00035) return 0; //check
    }
    if (flag==0) {
        FillHisto("Pinunu_kaon_thetax",kaon_thetax);
        FillHisto("Pinunu_kaon_thetay",kaon_thetay);
    }
    if (fPBeamTrack<72.7 || fPBeamTrack>77.2) return 0;//check
    return 1;
}

Int_t KaonTrackAnalysis::PositronDiscriminant() {
    Int_t discriminant = 0;
    if (fTrack->GetLKrEovP()>=0.8) discriminant = 1;
    //if (fTrack->GetCalorimetricEnergy()/fPTrack>=0.8) discriminant = 1;
    return discriminant;
}

Int_t KaonTrackAnalysis::PionDiscriminant() {
    Int_t discriminant = 0;
    if (fTrack->GetCalorimetricEnergy()>2) discriminant = 1;
    return discriminant;
}
//Using Riccardos analyser
Int_t KaonTrackAnalysis::MuonDiscriminantRicc(Int_t flag) {

    Int_t discriminant = 0;
    if(!flag)
        FillHisto("Kmu2_mudiscr_mmiss_vs_p",fPTrack,fMMiss);

    if (fTrack->GetMUV3ID()>-1) {discriminant = 1; fTrack->SetIsRAMuon(1);}
    if(discriminant==1 && !flag)
        FillHisto("Kmu2_mudiscr_muv3_mmiss_vs_p",fPTrack,fMMiss);

    Double_t caloEnergy = fTrack->GetCalorimetricEnergy();
    //Double_t eovp = caloEnergy/fPTrack;
    Double_t e1 = fTrack->GetMUV1Energy();
    Double_t e2 = fTrack->GetMUV2Energy();
    Double_t r1 = e1/caloEnergy;
    Double_t r2 = e2/caloEnergy;
    //Double_t sw1 = fTrack->GetMUV1ShowerWidth();
    //Double_t sw2 = fTrack->GetMUV2ShowerWidth();
    Double_t elkr = fTrack->GetLKrEovP()*fPTrack;
    Double_t seedratio = fTrack->GetLKrSeedEnergy()/elkr;
    Double_t cellratio = fTrack->GetLKrNCells()/elkr;
    //std::cout << " " << discriminant << " " << std::endl;

    if (r1<0.01&&r2<0.01) { // against muon bremm or muon decay before lkr
        if (seedratio>0.2&&cellratio<=3) {discriminant = 1; fTrack->SetIsEM(1);}
        if (seedratio<=0.2&&cellratio<1.8) {discriminant = 1; fTrack->SetIsEM(1);}
        if (seedratio>0.35){discriminant = 1; fTrack->SetIsEM(1);}
        if (seedratio<0.05){discriminant = 1; fTrack->SetIsEM(1);}
    }
    if (seedratio>0. && seedratio<0.8 && cellratio<1.4) {discriminant=1;fTrack->SetIsEM(1);}
    //cout << "sr = " << seedratio << " cr = "<< cellratio << endl;
    //std::cout << " " << discriminant << " " << std::endl;
    if(fTrack->GetIsMIP()){discriminant = 1; fTrack->SetIsRAMuon(1);}
    if(fTrack->GetIsMulti()){discriminant = 1; fTrack->SetIsRAMuon(1);}
    if(fTrack->GetMuonProb() >= 0.01){discriminant = 1; fTrack->SetIsRAMuon(1);}
    if(fTrack->GetPionProb() < 0.96){discriminant = 1; fTrack->SetIsRAMuon(1);}
    //cout << "pprob = " << fTrack->GetPionProb() << " muprob = "<< fTrack->GetMuonProb() << "\n";
    //cout << "mult = " << fTrack->GetIsMulti() << " mip = "<< fTrack->GetIsMIP() << "\n";
    //std::cout << " " << discriminant << " " << std::endl;
    if(fTrack->GetMUV1Energy()>= 1.2*fPTrack ){discriminant = 1;fTrack->SetIsRAMuon(1);}
    if(fTrack->GetMUV2Energy()>= 1.2*fPTrack ){discriminant = 1;fTrack->SetIsRAMuon(1);}
    if(fTrack->GetCalorimetricEnergy()< 0.15*fPTrack - 1.5 ){discriminant = 1;fTrack->SetIsRAMuon(1);}
    if(e1 > 35. || e2 > 35. ){discriminant = 1;fTrack->SetIsRAMuon(1);}
    if(fTrack->GetCalorimetricEnergy()/fPTrack > 1.2 ){discriminant = 1;fTrack->SetIsRAMuon(1);}
    if(fTrack->GetLKrEovP() > 1.05 ){discriminant = 1;fTrack->SetIsRAMuon(1);}
    //cout << "e/p = " << fTrack->GetCalorimetricEnergy()/fPTrack << " p = " << fPTrack << " lkr e/p = "<< fTrack->GetLKrEovP() << "\n";
    //std::cout << " " << discriminant << " " << std::endl;
    return discriminant;
}

Int_t KaonTrackAnalysis::MuonDiscriminant(Int_t flag) {
    Int_t discriminant = 0;
    //if (fTrack->GetMUV3ID()>-1) discriminant = 1;
    Double_t caloEnergy = fTrack->GetCalorimetricEnergy();
    Double_t eovp = caloEnergy/fPTrack;
    Double_t e1 = fTrack->GetMUV1Energy();
    Double_t e2 = fTrack->GetMUV2Energy();
    Double_t r1 = e1/caloEnergy;
    Double_t r2 = e2/caloEnergy;
    Double_t sw1 = fTrack->GetMUV1ShowerWidth();
    Double_t sw2 = fTrack->GetMUV2ShowerWidth();
    if (flag==1) FillHisto("Pinunu_pid_muonid_mmiss_vs_eop_nomuv3",eovp,(fBeamTrack->GetMomentum()-fTrack->GetMomentum()).Mag2());
    if (!discriminant && flag==1) FillHisto("Pinunu_pid_muonid_mmiss_vs_eop_nomuv3",eovp,(fBeamTrack->GetMomentum()-fTrack->GetMomentum()).Mag2());
    if (eovp<0.1) discriminant = 1; // bias in momentum
    if (caloEnergy<=5.) discriminant = 1; // bias in momentum
    if (caloEnergy>=0.825*fPTrack-0.25) discriminant = 1;
    if (caloEnergy<=0.825*fPTrack-27) discriminant = 1;
    if ( (r1<=0.85) && (r2<-1.125*r1+1) && (r2>0.09375*r1+0.025) ) discriminant = 1;
    if ((r2>=-1.125*r1+1) && (r2<-0.94545*r1+0.89818) ) discriminant = 1;
    Double_t elkr = fTrack->GetLKrEovP()*fPTrack;
    Double_t seedratio = fTrack->GetLKrSeedEnergy()/elkr;
    Double_t cellratio = fTrack->GetLKrNCells()/elkr;
    if (elkr<1 &&caloEnergy<9) discriminant = 1;
    if (elkr>2 && elkr<25 &&caloEnergy>30) discriminant = 1;
    if (r1<0.01&&r2<0.01) { // against muon bremm or muon decay before lkr
        if (seedratio>0.2&&cellratio<=3) discriminant = 1;
        if (seedratio<=0.2&&cellratio<1.8) discriminant = 1;
        if (seedratio<0.05) discriminant = 1;
    }
    if (r1>=0.01) {
        if (sw1<1.5*e1+5. && sw1>=5) discriminant = 1;
        if (caloEnergy>33) discriminant = 1;
        if (r1>0.4 && r1<=0.56 && sw1<-88+220*r1) discriminant = 1;
        if (r1>0.56 && sw1<35.5) discriminant = 1;
    }
    if (sw1>250) discriminant = 1;
    if (sw2>5 && sw2<108.33*r2) discriminant = 1;
    if (seedratio>0.4 && seedratio<0.8 && cellratio<1.4) discriminant = 1;
    return discriminant;

}

Int_t KaonTrackAnalysis::PionPIDNew(){

    if (PositronDiscriminant()) return 0;
    if (MuonDiscriminantRicc(1)) return 0;

    return 1;
}
Int_t KaonTrackAnalysis::PionPID(Bool_t flag) {

    // Positron cut
    if (PositronDiscriminant()) {
        if(flag){

            FillHisto("Pinunu_pid_positron_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_pid_positron_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_pid_positron_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_pid_positron_x_vs_y_lkr",fXAtLKr,fYAtLKr);
        }
        return 0;
    }

    if(flag){

        FillHisto("Pinunu_pid_nopositron_p_vs_zvtx",fVertex.Z(),fPTrack);
        FillHisto("Pinunu_pid_nopositron_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Pinunu_pid_nopositron_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("Pinunu_pid_nopositron_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }

    // Muon cut
    //if (MuonDiscriminant(1)) {
    if (MuonDiscriminantRicc(flag)) {

        if(flag){
            FillHisto("Pinunu_pid_muon_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_pid_muon_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_pid_muon_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_pid_muon_x_vs_y_lkr",fXAtLKr,fYAtLKr);
        }

        return 0;
    }

    if(flag){
        FillHisto("Pinunu_pid_nomuon_p_vs_zvtx",fVertex.Z(),fPTrack);
        FillHisto("Pinunu_pid_nomuon_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Pinunu_pid_nomuon_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("Pinunu_pid_nomuon_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }

    // Pion cut


    return 1;

}

Double_t KaonTrackAnalysis::MMissTest(double mmass) {
    double improvedMMiss = fMMiss;
    double deltabestmmiss = fabs(improvedMMiss-mmass);
    for (int jC(0); jC<fBeamTrack->GetNGTKCandidates()-1; jC++) {
        GigaTrackerCandidate cand = fBeamTrack->GetGTKCandidate(jC);
        Double_t partrack[4] = {cand.GetMomentum()*1000,cand.GetSlopeXZ(),cand.GetSlopeYZ(),Constants::MKCH*0.001};
        TLorentzVector pmom4 = fTool->Get4Momentum(partrack);
        TVector3 pmom = pmom4.Vect();
        TVector3 pini = cand.GetPosition();
        TVector3 vertex = fTool->BlueFieldCorrection(&pmom,pini,1,cand.GetVertex().Z());
        pmom4.SetVectM(pmom,MKCH*0.001);
        Double_t mmiss = (pmom4-fTrack->GetMomentum()).Mag2();
        double deltammiss = fabs(mmiss-mmass);
        if (deltammiss<deltabestmmiss) {
            deltabestmmiss = deltammiss;
            improvedMMiss = mmiss;
        }
    }
    return improvedMMiss;
}



Int_t KaonTrackAnalysis::TestEventsInSignalRegion(){

    if(fPTrack > 15 && fPTrack < 35){

        // Event filtering
        //FilterAccept();
        // Internal track consistency
        TRecoSpectrometerCandidate *spectrack = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fTrack->GetTrackID());
        Double_t dthetax = spectrack->GetSlopeXBeforeFit()-spectrack->GetSlopeXBeforeMagnet();
        Double_t dthetay = spectrack->GetSlopeYBeforeFit()-spectrack->GetSlopeYBeforeMagnet();
        Double_t quality1 = spectrack->GetCombinationTotalQuality();

        double dtchodktag = fBeamTrack->GetKTAGTime()-fTrack->GetCHODTime();
        double dtgtkktag = fBeamTrack->GetTime()-fBeamTrack->GetKTAGTime();
        double dtgtkrich = fBeamTrack->GetTime()-fTrack->GetRICHSingleTime();
        double dtchanti = fBeamTrack->GetCHANTITime()-fTrack->GetRICHSingleTime();
        double dtchodchanti = fBeamTrack->GetCHANTITime()-fTrack->GetCHODTime();
        double dtktag = fBeamTrack->GetKTAGTime()-fTrack->GetRICHSingleTime();
        double dtchod = fTrack->GetCHODTime()-fTrack->GetRICHSingleTime();
        double ecal = fTrack->GetCalorimetricEnergy();
        double nchanti = fCHANTIEvent->GetNCandidates();
        double nchantihits = fCHANTIEvent->GetNHits();
        double strawthetax =fTrack->GetMomentum().Px()/fTrack->GetMomentum().Pz() ;
        double strawthetay =fTrack->GetMomentum().Py()/fTrack->GetMomentum().Pz() ;
        double kthetax =fBeamTrack->GetMomentum().Px()/fBeamTrack->GetMomentum().Pz() - fTrimTheta ;
        double kthetay =fBeamTrack->GetMomentum().Py()/fBeamTrack->GetMomentum().Pz() ;

        double richp = fTrack->GetRICHMultiMomentum().P();
        double richs = fTrack->GetRICHSingleMass();
        double richm = fTrack->GetRICHMultiMass();

        double XAtMUV1 = fTrack->GetPositionAtMUV(0).X();
        double YAtMUV1 = fTrack->GetPositionAtMUV(0).Y();
        double XAtMUV2 = fTrack->GetPositionAtMUV(1).X();
        double YAtMUV2 = fTrack->GetPositionAtMUV(1).Y();
        double XAtMUV3 = fTrack->GetPositionAtMUV(2).X();
        double YAtMUV3 = fTrack->GetPositionAtMUV(2).Y();

        Double_t caloEnergy = fTrack->GetCalorimetricEnergy();
        Double_t eovp = caloEnergy/fPTrack;
        Double_t e1 = fTrack->GetMUV1Energy();
        Double_t e2 = fTrack->GetMUV2Energy();
        Double_t r1 = e1/caloEnergy;
        Double_t r2 = e2/caloEnergy;
        Double_t sw1 = fTrack->GetMUV1ShowerWidth();
        Double_t sw2 = fTrack->GetMUV2ShowerWidth();
        Double_t elkr = fTrack->GetLKrEovP()*fPTrack;
        Double_t seedratio = fTrack->GetLKrSeedEnergy()/elkr;
        Double_t cellratio = fTrack->GetLKrNCells()/elkr;

        //Region I
        if(fMMiss < 0.01 && fMMiss > 0){
            //if(!GetWithMC()){

            std::cout << "---------Event information for Region I ---------" << std::endl;
            std::cout << "Run ID***" << "Burst ID***" << " EventID***" << " P***" << " Vtx***" << " MMiss2***" << " PKaon***" << " cda***" << endl;
            std::cout << fHeader->GetRunID()<< "  " << fHeader->GetBurstID() << "  " << fHeader->GetEventNumber() << "  "  << fPTrack<< "  "  << fVertex.Z()<< "  "  << fMMiss<< "  "  << fPBeamTrack<< "  "  << fBeamTrack->GetCDA() << endl;
            std::cout << "ECal***" << "ChantiNCand***" << "ChantiNHits***"<< "dtchanti***" << "xstraw***" << "ystraw***" << "xlkr***" << "ylkr*** "<< std::endl;
            std::cout << ecal << "  " << nchanti<< "  "  << nchantihits<< "  "  << dtchanti<< "  "  << fXAtStraw1<< "  "  << fYAtStraw1<< "  "  << fXAtLKr<< "  "  << fYAtLKr << std::endl;
            std::cout << "dtchodktag***"  << "gtkktag***" << "gtkrich***"<< "dtchantirich***" << "dtchantichod***" << "dtktagrich**" << "dtchodrich***" << std::endl;
            std::cout << dtchodktag << "  "<<dtgtkktag<<"  "<<dtgtkrich<<"  "<<dtchanti<<"  "<<dtchodchanti<<"  "<<dtktag<<"  "<< dtchod<<std::endl;

            std::cout << "kthetax***" << "kthetay***" << "strawthetax***" << "strawthetay***" << "segments***"<< endl;
            std::cout << kthetax     << "  " <<  kthetay << "  "<< strawthetax << "  "<< strawthetay << "  "<< fSegmentIB << std::endl;
            std::cout << "RICHP***" << "mrichmass*** srichmass*** charge" << std::endl;
            std::cout << richp << "  " << richm << "    " << richs << "   " << fTrack->GetCharge()<< std::endl;
            std::cout << "LKr seedratio****" << "LKr cellratio***" << "swMUV1***" << "swMUV2***"<< "E LKr***" <<"EMUV1***" << "EMUV2***" << "E/p***" << std::endl;
            std::cout << seedratio << "  " << cellratio << "  " << sw1 << "  " << sw2 << "  " << elkr << "  " << e1 << "  " << e2 <<  "  "<< eovp << std::endl;

            std::cout << "NLKr Hits = " << fLKrEvent->GetNHits() << "NLKrCells =" << fTrack->GetLKrNCells() << "CHOD mult =" << fTrack->GetCHODMult() << "NCHOD Candidates =" << fCHODEvent->GetNCandidates() << "NCHOD hits =" << fCHODEvent->GetNHits() << "NewCHOD mult = " << fTrack->GetNewCHODMult() << "NNewCHOD hits = " << fNewCHODEvent->GetNHits() << std::endl;

            std::cout << "---------Event END Region I ---------" << std::endl;
            //}

            FillHisto("Pinunu_testr1_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_testr1_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_testr1_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
            FillHisto("Pinunu_testr1_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_testr1_x_vs_y_lkr",fXAtLKr,fYAtLKr);
            FillHisto("Pinunu_testr1_x_vs_y_muv1",XAtMUV1, YAtMUV1);
            FillHisto("Pinunu_testr1_x_vs_y_muv2",XAtMUV2, YAtMUV2);
            FillHisto("Pinunu_testr1_x_vs_y_muv3",XAtMUV3, YAtMUV3);

            FillHisto("Pinunu_testr1_muonprobcal_vs_p",fPTrack,fTrack->GetMuonProb());
            FillHisto("Pinunu_testr1_elprobcal_vs_p",fPTrack,fTrack->GetElectronProb());

            if (fTrack->GetMuonProb()<0.01 && fPTrack>15. && fPTrack<35.) {
                if (r1<0.01 && r2<0.01) FillHisto("Pinunu_testr1_lkrshape00",seedratio,cellratio);
                bool isLKrEM = r1<0.01 && r2<0.01 && ((seedratio>0.2 && cellratio<3.) || seedratio>0.35);
                if (!isLKrEM) {
                    FillHisto("Pinunu_testr1_ncellvseseed",fTrack->GetLKrSeedEnergy(),fTrack->GetLKrNCells());
                    FillHisto("Pinunu_testr1_dtime",fTrack->GetCaloTimeRA()-fBeamTrack->GetKTAGTime());
                    FillHisto("Pinunu_testr1_lkrshape",seedratio,cellratio);
                    //            FillHisto("PiP0_calorimeter_bdtprob",fPTrack,fTrack->GetBDTResponse());
                    if (r1>=0.01) {
                        FillHisto("Pinunu_testr1_muv1shape",r1,sw1);
                        FillHisto("Pinunu_testr1_dtlkrmuv1",fTrack->GetLKrTime()-fBeamTrack->GetKTAGTime(),fTrack->GetMUV1Time()-fBeamTrack->GetKTAGTime());
                        FillHisto("Pinunu_testr1_dtvsr1",r1,fTrack->GetLKrTime()-fTrack->GetMUV1Time());
                    }
                    if (r2>=0.01 && r1<0.01) FillHisto("Pinunu_testr1_muv2shape",r2,sw2);
                }
            }

            FillHisto("Pinunu_testr1_cda_vs_zvtx",fVertex.Z(),fBeamTrack->GetCDA());
            FillHisto("Pinunu_testr1_ngtk1hits",fBeamTrack->GetNGTK1Hits());
            FillHisto("Pinunu_testr1_ngtk2hits",fBeamTrack->GetNGTK2Hits());
            FillHisto("Pinunu_testr1_ngtk3hits",fBeamTrack->GetNGTK3Hits());

            FillHisto("Pinunu_testr1_spec_dthetax",dthetax,fMMiss);
            FillHisto("Pinunu_testr1_spec_dthetay",dthetay,fMMiss);
            FillHisto("Pinunu_testr1_spec_qual1",quality1,fMMiss);
            FillHisto("Pinunu_testr1_spec_nhits",spectrack->GetMomentum()*0.001,spectrack->GetNHits());
            FillHisto("Pinunu_testr1_vertex_xz",fVertex.Z(),fBeamTrack->GetVertex().X());
            FillHisto("Pinunu_testr1_vertex_yz",fVertex.Z(),fBeamTrack->GetVertex().Y());
            FillHisto("Pinunu_testr1_kaon_xy",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            Double_t thetapk = fTrack->GetMomentum().Angle(fBeamTrack->GetMomentum().Vect());
            FillHisto("Pinunu_testr1_thetavsp",fPTrack,thetapk);
            FillHisto("Pinunu_testr1_kaon_xy_check",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            FillHisto("Pinunu_testr1_kaon_p_check",fPBeamTrack);
            FillHisto("Pinunu_testr1_kaon_thetaxy_check",kthetax,kthetay);
            FillHisto("Pinunu_testr1_thx_vs_pk",fPBeamTrack,kthetax);
            FillHisto("Pinunu_testr1_thy_vs_pk",fPBeamTrack,kthetay);

        }


        //Region II
        if(fMMiss < 0.068 && fMMiss > 0.026){
            //if(!GetWithMC()){

            std::cout << "---------Event information for Region II ---------" << std::endl;
            std::cout << "Run ID***" << "Burst ID***" << " EventID***" << " P***" << " Vtx***" << " MMiss2***" << " PKaon***" << " cda***" << endl;
            std::cout << fHeader->GetRunID() << "   " << fHeader->GetBurstID() << "  " << fHeader->GetEventNumber() << "  "  << fPTrack<< "  "  << fVertex.Z()<< "  "  << fMMiss<< "  "  << fPBeamTrack<< "  "  << fBeamTrack->GetCDA() << endl;
            std::cout << "ECal***" << "ChantiNCand***" << "ChantiNHits***"<< "dtchanti***" << "xstraw***" << "ystraw***" << "xlkr***" << "ylkr*** "<< std::endl;
            std::cout <<  "  " << ecal << "  " << nchanti<< "  "  << nchantihits<< "  "  << dtchanti<< "  "  << fXAtStraw1<< "  "  << fYAtStraw1<< "  "  << fXAtLKr<< "  "  << fYAtLKr << std::endl;
            std::cout << "dtchodktag***"  << "gtkktag***" << "gtkrich***"<< "dtchantirich***" << "dtchantichod***" << "dtktagrich**" << "dtchodrich***" << std::endl;
            std::cout << dtchodktag << "  "<<dtgtkktag<<"  "<<dtgtkrich<<"  "<<dtchanti<<"  "<<dtchodchanti<<"  "<<dtktag<<"  "<< dtchod<<std::endl;

            std::cout << "kthetax***" << "kthetay***" << "strawthetax***" << "strawthetay***" << "segments***"<< endl;
            std::cout << kthetax     << "  " <<  kthetay << "  "<< strawthetax << "  "<< strawthetay << "  "<< fSegmentIB << std::endl;
            std::cout << "RICHP***" << "mrichmass*** srichmass*** charge" << std::endl;
            std::cout << richp << "  " << richm << "    " << richs << "   " << fTrack->GetCharge()<< std::endl;
            std::cout << "LKr seedratio****" << "LKr cellratio***" << "swMUV1***" << "swMUV2***"<< "E LKr***" <<"EMUV1***" << "EMUV2***" << "E/p***" << std::endl;
            std::cout << seedratio << "  " << cellratio << "  " << sw1 << "  " << sw2 << "  " << elkr << "  " << e1 << "  " << e2 <<  "  "<< eovp << std::endl;

            std::cout << "NLKr Hits = " << fLKrEvent->GetNHits() << "NLKrCells =" << fTrack->GetLKrNCells() << "CHOD mult =" << fTrack->GetCHODMult() << "NCHOD Candidates =" << fCHODEvent->GetNCandidates() << "NCHOD hits =" << fCHODEvent->GetNHits() << "NewCHOD mult = " << fTrack->GetNewCHODMult() << "NNewCHOD hits = " << fNewCHODEvent->GetNHits() << std::endl;
            std::cout << "---------Event END Region II ---------" << std::endl;
            //}
            //double XAtCHOD = fTrack->GetPositionAtCHOD(0).X() + ;
            FillHisto("Pinunu_testr2_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_testr2_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
            FillHisto("Pinunu_testr2_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_testr2_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_testr2_x_vs_y_lkr",fXAtLKr,fYAtLKr);
            FillHisto("Pinunu_testr2_x_vs_y_muv1",XAtMUV1, YAtMUV1);
            FillHisto("Pinunu_testr2_x_vs_y_muv2",XAtMUV2, YAtMUV2);
            FillHisto("Pinunu_testr2_x_vs_y_muv3",XAtMUV3, YAtMUV3);

            FillHisto("Pinunu_testr2_muonprobcal_vs_p",fPTrack,fTrack->GetMuonProb());
            FillHisto("Pinunu_testr2_elprobcal_vs_p",fPTrack,fTrack->GetElectronProb());
            if (fTrack->GetMuonProb()<0.01 && fPTrack>15. && fPTrack<35.) {
                if (r1<0.01 && r2<0.01) FillHisto("Pinunu_testr2_lkrshape00",seedratio,cellratio);
                bool isLKrEM = r1<0.01 && r2<0.01 && ((seedratio>0.2 && cellratio<3.) || seedratio>0.35);
                if (!isLKrEM) {
                    FillHisto("Pinunu_testr2_ncellvseseed",fTrack->GetLKrSeedEnergy(),fTrack->GetLKrNCells());
                    FillHisto("Pinunu_testr2_dtime",fTrack->GetCaloTimeRA()-fBeamTrack->GetKTAGTime());
                    FillHisto("Pinunu_testr2_lkrshape",seedratio,cellratio);
                    //            FillHisto("PiP0_calorimeter_bdtprob",fPTrack,fTrack->GetBDTResponse());
                    if (r1>=0.01) {
                        FillHisto("Pinunu_testr2_muv1shape",r1,sw1);
                        FillHisto("Pinunu_testr2_dtlkrmuv1",fTrack->GetLKrTime()-fBeamTrack->GetKTAGTime(),fTrack->GetMUV1Time()-fBeamTrack->GetKTAGTime());
                        FillHisto("Pinunu_testr2_dtvsr1",r1,fTrack->GetLKrTime()-fTrack->GetMUV1Time());
                    }
                    if (r2>=0.01 && r1<0.01) FillHisto("Pinunu_testr2_muv2shape",r2,sw2);
                }
            }

            FillHisto("Pinunu_testr2_cda_vs_zvtx",fVertex.Z(),fBeamTrack->GetCDA());
            FillHisto("Pinunu_testr2_ngtk1hits",fBeamTrack->GetNGTK1Hits());
            FillHisto("Pinunu_testr2_ngtk2hits",fBeamTrack->GetNGTK2Hits());
            FillHisto("Pinunu_testr2_ngtk3hits",fBeamTrack->GetNGTK3Hits());

            FillHisto("Pinunu_testr2_spec_dthetax",dthetax,fMMiss);
            FillHisto("Pinunu_testr2_spec_dthetay",dthetay,fMMiss);
            FillHisto("Pinunu_testr2_spec_qual1",quality1,fMMiss);
            FillHisto("Pinunu_testr2_spec_nhits",spectrack->GetMomentum()*0.001,spectrack->GetNHits());
            FillHisto("Pinunu_testr2_vertex_xz",fVertex.Z(),fBeamTrack->GetVertex().X());
            FillHisto("Pinunu_testr2_vertex_yz",fVertex.Z(),fBeamTrack->GetVertex().Y());
            FillHisto("Pinunu_testr2_kaon_xy",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            Double_t thetapk = fTrack->GetMomentum().Angle(fBeamTrack->GetMomentum().Vect());
            FillHisto("Pinunu_testr2_thetavsp",fPTrack,thetapk);
            FillHisto("Pinunu_testr2_kaon_xy_check",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            FillHisto("Pinunu_testr2_kaon_p_check",fPBeamTrack);
            FillHisto("Pinunu_testr2_kaon_thetaxy_check",kthetax,kthetay);
            FillHisto("Pinunu_testr2_thx_vs_pk",fPBeamTrack,kthetax);
            FillHisto("Pinunu_testr2_thy_vs_pk",fPBeamTrack,kthetay);


        }
        //Residual events K3pi region
        if(fMMiss > 0.068){
            //if(!GetWithMC()){
            std::cout << "---------Event information for K3pi Region ---------" << std::endl;
            std::cout << "Run ID***" << "Burst ID***" << " EventID***" << " P***" << " Vtx***" << " MMiss2***" << " PKaon***" << " cda***" << endl;
            std::cout << fHeader->GetRunID() << "   " << fHeader->GetBurstID() << "  " << fHeader->GetEventNumber() << "  "  << fPTrack<< "  "  << fVertex.Z()<< "  "  << fMMiss<< "  "  << fPBeamTrack<< "  "  << fBeamTrack->GetCDA() << endl;

            std::cout << "kthetax***" << "kthetay***" << "strawthetax***" << "strawthetay***" << "segments***"<< endl;
            std::cout << kthetax     << "  " <<  kthetay << "  "<< strawthetax << "  "<< strawthetay << "  "<< fSegmentIB << std::endl;
            std::cout << "RICHP***" << "mrichmass*** srichmass*** charge" << std::endl;
            std::cout << richp << "  " << richm << "    " << richs << "   " << fTrack->GetCharge()<< std::endl;
            std::cout << "LKr seedratio****" << "LKr cellratio***" << "swMUV1***" << "swMUV2***"<< "E LKr***" <<"EMUV1***" << "EMUV2***" << "E/p***" << std::endl;
            std::cout << seedratio << "  " << cellratio << "  " << sw1 << "  " << sw2 << "  " << elkr << "  " << e1 << "  " << e2 <<  "  "<< eovp << std::endl;

            std::cout << "---------Event END K3pi Region ---------" << std::endl;
            //}

            FillHisto("Pinunu_testr3_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_testr3_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
            FillHisto("Pinunu_testr3_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_testr3_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_testr3_x_vs_y_lkr",fXAtLKr,fYAtLKr);
            FillHisto("Pinunu_testr3_x_vs_y_muv1",XAtMUV1, YAtMUV1);
            FillHisto("Pinunu_testr3_x_vs_y_muv2",XAtMUV2, YAtMUV2);
            FillHisto("Pinunu_testr3_x_vs_y_muv3",XAtMUV3, YAtMUV3);


            FillHisto("Pinunu_testr3_cda_vs_zvtx",fVertex.Z(),fBeamTrack->GetCDA());
            FillHisto("Pinunu_testr3_ngtk1hits",fBeamTrack->GetNGTK1Hits());
            FillHisto("Pinunu_testr3_ngtk2hits",fBeamTrack->GetNGTK2Hits());
            FillHisto("Pinunu_testr3_ngtk3hits",fBeamTrack->GetNGTK3Hits());

            FillHisto("Pinunu_testr3_spec_dthetax",dthetax,fMMiss);
            FillHisto("Pinunu_testr3_spec_dthetay",dthetay,fMMiss);
            FillHisto("Pinunu_testr3_spec_qual1",quality1,fMMiss);
            FillHisto("Pinunu_testr3_spec_nhits",spectrack->GetMomentum()*0.001,spectrack->GetNHits());
            FillHisto("Pinunu_testr3_vertex_xz",fVertex.Z(),fBeamTrack->GetVertex().X());
            FillHisto("Pinunu_testr3_vertex_yz",fVertex.Z(),fBeamTrack->GetVertex().Y());
            FillHisto("Pinunu_testr3_kaon_xy",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            Double_t thetapk = fTrack->GetMomentum().Angle(fBeamTrack->GetMomentum().Vect());
            FillHisto("Pinunu_testr3_thetavsp",fPTrack,thetapk);
            FillHisto("Pinunu_testr3_kaon_xy_check",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            FillHisto("Pinunu_testr3_kaon_p_check",fPBeamTrack);
            FillHisto("Pinunu_testr3_kaon_thetaxy_check",kthetax,kthetay);
            FillHisto("Pinunu_testr3_thx_vs_pk",fPBeamTrack,kthetax);
            FillHisto("Pinunu_testr3_thy_vs_pk",fPBeamTrack,kthetay);

        }


        if(fMMiss < 0.0){
            //if(!GetWithMC()){
            std::cout << "---------Event information for Kmu2 Region ---------" << std::endl;
            std::cout << "Run ID***" << "Burst ID***" << " EventID***" << " P***" << " Vtx***" << " MMiss2***" << " PKaon***" << " cda***" << endl;
            std::cout << fHeader->GetRunID() << "   " << fHeader->GetBurstID() << "  " << fHeader->GetEventNumber() << "  "  << fPTrack<< "  "  << fVertex.Z()<< "  "  << fMMiss<< "  "  << fPBeamTrack<< "  "  << fBeamTrack->GetCDA() << endl;

            std::cout << "kthetax***" << "kthetay***" << "strawthetax***" << "strawthetay***" << "segments***"<< endl;
            std::cout << kthetax     << "  " <<  kthetay << "  "<< strawthetax << "  "<< strawthetay << "  "<< fSegmentIB << std::endl;
            std::cout << "RICHP***" << "mrichmass*** srichmass*** charge" << std::endl;
            std::cout << richp << "  " << richm << "    " << richs << "   " << fTrack->GetCharge()<< std::endl;
            std::cout << "LKr seedratio****" << "LKr cellratio***" << "swMUV1***" << "swMUV2***"<< "E LKr***" <<"EMUV1***" << "EMUV2***" << "E/p***" << std::endl;
            std::cout << seedratio << "  " << cellratio << "  " << sw1 << "  " << sw2 << "  " << elkr << "  " << e1 << "  " << e2 <<  "  "<< eovp << std::endl;

            std::cout << "---------Event END Kmu2 Region ---------" << std::endl;
            //}

            FillHisto("Pinunu_testr4_p_vs_zvtx",fVertex.Z(),fPTrack);
            FillHisto("Pinunu_testr4_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
            FillHisto("Pinunu_testr4_mmiss_vs_p",fPTrack,fMMiss);
            FillHisto("Pinunu_testr4_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
            FillHisto("Pinunu_testr4_x_vs_y_lkr",fXAtLKr,fYAtLKr);
            FillHisto("Pinunu_testr4_x_vs_y_muv1",XAtMUV1, YAtMUV1);
            FillHisto("Pinunu_testr4_x_vs_y_muv2",XAtMUV2, YAtMUV2);
            FillHisto("Pinunu_testr4_x_vs_y_muv3",XAtMUV3, YAtMUV3);

            FillHisto("Pinunu_testr4_cda_vs_zvtx",fVertex.Z(),fBeamTrack->GetCDA());
            FillHisto("Pinunu_testr4_ngtk1hits",fBeamTrack->GetNGTK1Hits());
            FillHisto("Pinunu_testr4_ngtk2hits",fBeamTrack->GetNGTK2Hits());
            FillHisto("Pinunu_testr4_ngtk3hits",fBeamTrack->GetNGTK3Hits());

            FillHisto("Pinunu_testr4_spec_dthetax",dthetax,fMMiss);
            FillHisto("Pinunu_testr4_spec_dthetay",dthetay,fMMiss);
            FillHisto("Pinunu_testr4_spec_qual1",quality1,fMMiss);
            FillHisto("Pinunu_testr4_spec_nhits",spectrack->GetMomentum()*0.001,spectrack->GetNHits());
            FillHisto("Pinunu_testr4_vertex_xz",fVertex.Z(),fBeamTrack->GetVertex().X());
            FillHisto("Pinunu_testr4_vertex_yz",fVertex.Z(),fBeamTrack->GetVertex().Y());
            FillHisto("Pinunu_testr4_kaon_xy",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            Double_t thetapk = fTrack->GetMomentum().Angle(fBeamTrack->GetMomentum().Vect());
            FillHisto("Pinunu_testr4_thetavsp",fPTrack,thetapk);
            FillHisto("Pinunu_testr4_kaon_xy_check",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
            FillHisto("Pinunu_testr4_kaon_p_check",fPBeamTrack);
            FillHisto("Pinunu_testr4_kaon_thetaxy_check",kthetax,kthetay);
            FillHisto("Pinunu_testr4_thx_vs_pk",fPBeamTrack,kthetax);
            FillHisto("Pinunu_testr4_thy_vs_pk",fPBeamTrack,kthetay);

        }

    }

    return 1;
}

Int_t KaonTrackAnalysis::SelectTrigger(int triggerType, int type, int mask) {
    int bitPhysics = 0;
    int bitControl = 4;
    int bitMinBias = 0;
    int bitPinunu = 1;


    if (type&0x2) return 0; // skip periodics


    // Select trigger
    if ((triggerType==0) && (type>>bitControl)&1) return 1; // control trigger.
    else if (triggerType==1) { // physics minimum bias trigger.
        if (!((type>>bitPhysics)&1)) return 0;
        if ((mask>>bitMinBias)&1) return 1;
    }
    else if (triggerType==2) { // physics pinunu trigger.
        if (!((type >>bitPhysics)&1)) return 0;
        if ((mask>>bitPinunu)&1) return 1;
    }
    else return 0; // trigger not found.

    return 0;
}

void KaonTrackAnalysis::ClearAllPointers(){

    delete fCedarEvent;
    delete  fGigaTrackerEvent;
    delete  fCHANTIEvent;
    delete  fSpectrometerEvent;
    delete  fLAVEvent;
    delete  fRICHEvent;
    delete  fCHODEvent;
    delete  fIRCEvent ;
    delete  fLKrEvent ;
    delete  fMUV1Event;
    delete  fMUV2Event;
    delete  fMUV3Event;
    delete  fSACEvent;

}

Bool_t KaonTrackAnalysis::Kmu2PhotonRandomVeto(){ // flag 0 muon , flag 1 pion
    Bool_t isPhoton = fIsLKr||fIsLAV||fIsIRC||fIsSAC||fIsSAV||fIsNewLKr ? 1 : 0;
    bool islavtrack = fTrack->GetIsLAVTrack();
    FillHisto("Kmu2_RPhVeto_mmiss_vs_p",fPTrack,fMmuMiss);

    if (!fIsLKr) FillHisto("Kmu2_RPhVeto_mmiss_vs_p_nogammalkr",fPTrack,fMmuMiss);
    if (!fIsLKr&&!fIsNewLKr) FillHisto("Kmu2_RPhVeto_mmiss_vs_p_nogammalkraux",fPTrack,fMmuMiss);
    if (!fIsLAV) FillHisto("Kmu2_RPhVeto_mmiss_vs_p_nogammalav",fPTrack,fMmuMiss);
    if (!fIsSAC&&!fIsIRC&&!fIsSAV) FillHisto("Kmu2_RPhVeto_mmiss_vs_p_nogammasav",fPTrack,fMmuMiss);
    if(!isPhoton)  FillHisto("Kmu2_RPhVeto_mmiss_vs_p_nogammafinal",fPTrack,fMmuMiss);

    return isPhoton;
}

void KaonTrackAnalysis::Kmu2NewPhotonRandomVeto(){ // flag 0 muon , flag 1 pion
    Bool_t isPhoton = fIsLKrActivity||fIsLAV||fIsIRC||fIsSAC||fIsSAV||fIsNewLKr ? 1 : 0;
    bool islavtrack = fTrack->GetIsLAVTrack();
    FillHisto("Kmu2_lkrtest_RPhVeto_mmiss_vs_p",fPTrack,fMmuMiss);
    if (!fIsLKrActivity) FillHisto("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammalkr",fPTrack,fMmuMiss);
    if (!fIsLKrActivity&&!fIsNewLKr) FillHisto("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammalkraux",fPTrack,fMmuMiss);
    if (!fIsLAV) FillHisto("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammalav",fPTrack,fMmuMiss);
    if (!fIsSAC&&!fIsIRC&&!fIsSAV) FillHisto("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammasav",fPTrack,fMmuMiss);
    if(!isPhoton)  FillHisto("Kmu2_lkrtest_RPhVeto_mmiss_vs_p_nogammafinal",fPTrack,fMmuMiss);
}

void KaonTrackAnalysis::KinematicsStudy(Int_t flag){
    TString mode;
    if(flag==0) mode = "Pinunu";
    if(flag==1) mode = "Kmu2";
    if(flag==2) mode = "K2pi";
    if(flag==3) mode = "Norm";


    Double_t mm2nogtk = (fKaonNominalMomentum - fTrack->GetMomentum()).Mag2();
    TVector3 pic_momentum = fTrack->GetMomentum().Vect();
    Double_t phi = pic_momentum.Phi();
    Double_t phisign = phi>=0 ? phi : 2*3.141592653+phi;

    FillHisto(Form("%s_kinematics_mmiss_vs_p_nogtk",mode.Data()),fPTrack,mm2nogtk);
    FillHisto(Form("%s_kinematics_mmiss_vs_phi_nogtk",mode.Data()),phisign,mm2nogtk);
    FillHisto(Form("%s_kinematics_mmiss_vs_p_signal",mode.Data()),fPTrack,fMMiss);
    FillHisto(Form("%s_kinematics_mmiss_vs_phi_signal",mode.Data()),phisign,fMMiss);

    if(fPTrack>15 && fPTrack < 35){

        TLorentzVector richp   = fTrack->GetRICHSingleMomentum();
        double mmissrich = (fBeamTrack->GetMomentum()-richp).Mag2();
        int momentum= 20;
        // FillHisto(Form("%s_kinematics_mmiss_vs_ngtk_1535",mode.Data()),fMMiss,fBeamTrack->GetNGTKCandidates());
        // FillHisto(Form("%s_kinematics_mmiss_vs_mm2nogtk_1535",mode.Data()),fMMiss,mm2nogtk);
        // FillHisto(Form("%s_kinematics_mmiss_vs_mm2rich_1535",mode.Data()),fMMiss,mmissrich);
        for(int j(0);j<4;j++){
            momentum = 20 + j*5;
            if( (double)momentum -5 <= fPTrack  && fPTrack< (double)momentum){

                FillHisto(Form("%s_%d_%d_kinematics_mmiss_vs_ngtk_1535",mode.Data(),(int)(momentum - 5.),(int)momentum),fMMiss,fBeamTrack->GetNGTKCandidates());
                FillHisto(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535",mode.Data(),(int)(momentum - 5.),(int)momentum),fMMiss,mm2nogtk);
                FillHisto(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535",mode.Data(),(int)(momentum - 5),(int)momentum),fMMiss,mmissrich);

                //Signal region definition
                bool r1 = fMMiss>0.0&&fMMiss<0.01;
                bool r2 = fMMiss>0.026&&fMMiss<0.068;
                //bool r1rich = mmissrich>0.0&&mmissrich<0.01;
                //bool r2rich = mmissrich>0.026&&mmissrich<0.068;

                //Region below 25 GeV
                bool lowP_r1nogtk = mm2nogtk>-0.005&&mm2nogtk<0.0135&&fPTrack<=25.;
                bool lowP_r2nogtk = mm2nogtk>0.024&&mm2nogtk<0.068&&fPTrack<=25.;

                //Region above 25 GeV
                bool highP_r1nogtk = mm2nogtk>0.0&&mm2nogtk<0.0135&&fPTrack>25.;
                bool highP_r2nogtk = mm2nogtk>0.024&&mm2nogtk<0.068&&fPTrack>25.;

                bool lowP_signal1 = r1&&lowP_r1nogtk;
                bool lowP_signal2 = r2&&lowP_r2nogtk;

                bool highP_signal1 = r1&&highP_r1nogtk;
                bool highP_signal2 = r2&&highP_r2nogtk;

                //Signal+control region definition
                bool cr1 = fMMiss>0.0&&fMMiss<0.013;
                bool cr2 = fMMiss>0.023&&fMMiss<0.068;
                bool cr1rich = mmissrich>0.0&&mmissrich<0.03;
                bool cr2rich = mmissrich>0.0&&mmissrich<0.068;
                bool cr1nogtk = mm2nogtk>0.0&&mm2nogtk<0.013;
                bool cr2nogtk = mm2nogtk>0.023&&mm2nogtk<0.068;
                bool csignal1 = cr1&&cr1nogtk;
                bool csignal2 = cr2&&cr2nogtk;

                bool rpipi  = fMMiss<0.021 && fMMiss>0.015;
                bool rpipipi= fMMiss>0.068;
                bool rmunu  = fMMiss<0.0;

                if(mode.EqualTo("Norm")){
                    if(rpipi)
                        FillHisto(Form("P%d%d_Norm_events",(int)(momentum - 5.),(int)momentum),GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
                }
                if(mode.EqualTo("Pinunu")){
                    if(rpipi)
                        FillHisto(Form("P%d%d_Pipi_events",(int)(momentum - 5.),(int)momentum),GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
                    if(rpipipi)
                        FillHisto(Form("P%d%d_Pipipi_events",(int)(momentum - 5.),(int)momentum),GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
                    if(rmunu)
                        FillHisto(Form("P%d%d_Munu_events",(int)(momentum - 5.),(int)momentum),GetEventHeader()->GetBurstID(),GetEventHeader()->GetRunID());
                }

                // if (signal1 || signal2) {

                //Include control regions in the final plot, needed for the final analysis
                if(flag!=3){

                    if (lowP_signal1 || lowP_signal2) {
                        FillHisto(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535_final",mode.Data(),momentum - 5,momentum),fMMiss,mm2nogtk);
                        FillHisto(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535_final",mode.Data(),momentum - 5,momentum),fMMiss,mmissrich);
                    }

                    if (highP_signal1 || highP_signal2) {
                        FillHisto(Form("%s_%d_%d_kinematics_mmiss_vs_mm2nogtk_1535_final",mode.Data(),momentum - 5,momentum),fMMiss,mm2nogtk);
                        FillHisto(Form("%s_%d_%d_kinematics_mmiss_vs_mm2rich_1535_final",mode.Data(),momentum - 5,momentum),fMMiss,mmissrich);
                    }
                }

            }

        }
    }

}



void KaonTrackAnalysis::PIDStudy(Int_t flag){

    Double_t mmiss =fMMiss;
    if(flag==0) mmiss =fMmuMiss;
    //if(flag==2) mmiss =fMelMiss;
    TString mode;
    if (flag==0) mode = "Kmu2";
    if (flag==1) mode = "K2pi";
    if (flag==2) mode = "Ke3";


    Double_t caloEnergy = fTrack->GetCalorimetricEnergy();
    //Double_t eovp = caloEnergy/fPTrack;
    Double_t e1 = fTrack->GetMUV1Energy();
    Double_t e2 = fTrack->GetMUV2Energy();
    Double_t r1 = e1/caloEnergy;
    Double_t r2 = e2/caloEnergy;
    Double_t sw1 = fTrack->GetMUV1ShowerWidth();
    Double_t sw2 = fTrack->GetMUV2ShowerWidth();
    Double_t elkr = fTrack->GetLKrEovP()*fPTrack;
    Double_t seedratio = fTrack->GetLKrSeedEnergy()/elkr;
    Double_t cellratio = fTrack->GetLKrNCells()/elkr;

    //bool isKineCut = fabs(fMmuMiss)<0.01 && fPTrack<=73;
    bool isNoPreMuon = fTrack->GetMUV3ID()==-1 && !fTrack->GetIsMIP() && !fTrack->GetIsMulti();
    bool isDiscriminantRA = MuonDiscriminantRicc(1);
    if(flag==2 && fTrack->GetIsMIP() ) return;

    FillHisto(Form("%s_pid_mmiss_vs_p",mode.Data()),fPTrack,mmiss);
    FillHisto(Form("%s_pid_eop_vs_p",mode.Data()),fPTrack,fTrack->GetLKrEovP());
    //if(PositronDiscriminant()) return;

    FillHisto(Form("%s_pid_mmiss_vs_p_beforePID",mode.Data()),fPTrack,mmiss);
    //// PID condition study
    FillHisto(Form("%s_pid_calid_ptrack",mode.Data()),fPTrack);

    if(!fTrack->GetIsRAMuon()){
        FillHisto(Form("%s_pid_mmiss_vs_p_ramuon",mode.Data()),fPTrack,mmiss);
        if(!fTrack->GetIsEM()){
            FillHisto(Form("%s_pid_mmiss_vs_p_ramuon_em",mode.Data()),fPTrack,mmiss);
        }
    }
    if(isNoPreMuon ){
        FillHisto(Form("%s_pid_muonprob_vs_pionprob",mode.Data()),fTrack->GetPionProb(),fTrack->GetMuonProb());
        FillHisto(Form("%s_pid_pionprob_vs_elprob",mode.Data()),fTrack->GetElectronProb(),fTrack->GetPionProb());
        if(fTrack->GetPionProb() > 0.8 && fPTrack < 35 && fPTrack > 15){
            FillHisto(Form("%s_pid_muonprob_vs_elprob",mode.Data()),fTrack->GetElectronProb(),fTrack->GetMuonProb());
        }
    }

    if(!isDiscriminantRA){
        if(flag==0) FillHisto("Kmu2_pid_murejection_mmiss_vs_p",fPTrack,mmiss);
        if(flag==1) FillHisto("K2pi_pid_pionefficiency_mmiss_vs_p",fPTrack,mmiss);
        if(flag==2) FillHisto("Ke3_pid_erejection_mmiss_vs_p",fPTrack,mmiss);

    }

    if(isDiscriminantRA){
        if(flag==0) FillHisto("Kmu2_pid_muefficiency_mmiss_vs_p",fPTrack,mmiss);
        if(flag==1) FillHisto("K2pi_pid_pionrejection_mmiss_vs_p",fPTrack,mmiss);
        if(flag==2) FillHisto("Ke3_pid_eefficiency_mmiss_vs_p",fPTrack,mmiss);
    }


    if (isNoPreMuon) { // RA test
        FillHisto(Form("%s_pid_calid_muonprob_vs_p",mode.Data()),fPTrack,fTrack->GetMuonProb());
        FillHisto(Form("%s_pid_calid_elprob_vs_p",mode.Data()),fPTrack,fTrack->GetElectronProb());
        if (fTrack->GetMuonProb()<0.01 && fPTrack>15. && fPTrack<35.) {
            if (r1<0.01 && r2<0.01) FillHisto(Form("%s_pid_calid_lkrshape00",mode.Data()),seedratio,cellratio);
            bool isLKrEM = r1<0.01 && r2<0.01 && ((seedratio>0.2 && cellratio<3.) || seedratio>0.35);
            if (!isLKrEM) {
                FillHisto(Form("%s_pid_calid_ncellvseseed",mode.Data()),fTrack->GetLKrSeedEnergy(),fTrack->GetLKrNCells());
                FillHisto(Form("%s_pid_calid_dtime",mode.Data()),fTrack->GetCaloTimeRA()-fBeamTrack->GetKTAGTime());
                FillHisto(Form("%s_pid_calid_lkrshape",mode.Data()),seedratio,cellratio);
                if (r1>=0.01) {
                    FillHisto(Form("%s_pid_calid_muv1shape",mode.Data()),r1,sw1);
                    FillHisto(Form("%s_pid_calid_dtlkrmuv1",mode.Data()),fTrack->GetLKrTime()-fBeamTrack->GetKTAGTime(),fTrack->GetMUV1Time()-fBeamTrack->GetKTAGTime());
                    FillHisto(Form("%s_pid_calid_dtvsr1",mode.Data()),r1,fTrack->GetLKrTime()-fTrack->GetMUV1Time());
                }
                if (r2>=0.01 && r1<0.01) FillHisto(Form("%s_pid_calid_muv2shape",mode.Data()),r2,sw2);
            }
        }
    }





    return;

}

void KaonTrackAnalysis::RICHPIDStudy(Int_t flag){

    TString mode;
    if(flag==0) mode = "Kmu2";
    if(flag==1) mode = "K2pi";
    double sringmass = fTrack->GetRICHSingleMass();
    double mringmass = fTrack->GetRICHMultiMass();
    double nrings    = fRICHEvent->GetNRingCandidates();

    FillHisto(Form("%s_mmiss_vs_richlikelihood",mode.Data()),fRICHStatus,fMMiss);
    FillHisto(Form("%s_singlepi_lr",mode.Data()), fIsRICHPionLH);

    if(fIsRICHSingleRing){
        if(flag==1&&nrings==1&&fRICHStatus==3)
            FillHisto("Ringrad_vs_p",fPTrack,fTrack->GetRICHSingleRadius());
        if(flag==0)
            FillHisto("Ringrad_vs_p",fPTrack,fTrack->GetRICHSingleRadius());
    }

    //RICH muon efficiency
    if(fPTrack < 35 && fPTrack > 15){
        FillHisto(Form("%s_mmiss_vs_richlikelihood_1535",mode.Data()),fRICHStatus,fMMiss);
        FillHisto(Form("%s_kinrange_mmiss_vs_p",mode.Data()),fPTrack,fMMiss);

        // if(fIsRICHMultiRing)
        if(nrings < 2)
            FillHisto(Form("%s_richpionpid_mmiss_vs_richlikelihood_1535_nrings",mode.Data()),fRICHStatus,fMMiss);

        // if(fIsRICHMultiRing&&fabs(fIsRICHPionLH) >= 0.7)
        if(fIsRICHMultiRing&&fabs(fIsRICHPionLH) >= 1.2)
            FillHisto(Form("%s_richpionpid_mmiss_vs_richlikelihood_1535",mode.Data()),fRICHStatus,fMMiss);


        //Efficiency G.R algorithm
        if(fIsRICHSingleRing){
            FillHisto(Form("%s_richpionpid_mringmass_vs_p_asid",mode.Data()),fPTrack,mringmass);
            FillHisto(Form("%s_richpionpid_sringmass_vs_p_asid",mode.Data()),fPTrack,sringmass);
            FillHisto(Form("%s_richpionpid_mmiss_vs_p_asid",mode.Data()),fPTrack,fMMiss);


            if(sringmass > 0.133 && sringmass < 0.2 && flag==1){

                FillHisto(Form("%s_richpionpid_mmiss_vs_p",mode.Data()),fPTrack,fMMiss);

                // if(fIsRICHMultiRing && fabs(fIsRICHPionLH) >= 0.7)
                if(fIsRICHMultiRing && fabs(fIsRICHPionLH) >= 1.2)
                    FillHisto(Form("%s_richpionpid_mmiss_vs_p_full",mode.Data()),fPTrack,fMMiss);

            }
            if(sringmass > 0.133 && sringmass < 0.2 && flag==0){
                FillHisto(Form("%s_richmuonpid_mmiss_vs_p",mode.Data()),fPTrack,fMMiss);
                // if(fIsRICHMultiRing&& fabs(fIsRICHPionLH) >= 0.7)
                if(fIsRICHMultiRing&& fabs(fIsRICHPionLH) >= 1.2)
                    FillHisto(Form("%s_richmuonpid_mmiss_vs_p_full",mode.Data()),fPTrack,fMMiss);

            }
        }


    }//mom range
}


void KaonTrackAnalysis::K2piControlTriggerAnalysis(){

    FillHisto("K2pi_ctrltrigg_alltrigg_mmiss_vs_p",fPTrack,fMMiss);
    return;
}

void KaonTrackAnalysis::K2piPinunuTriggerAnalysis(){

    FillHisto("K2pi_pinunutrigg_alltrigg_mmiss_vs_p",fPTrack,fMMiss);
    return;
}
///////////////////////////
// Nominal Kaon Momentum //
///////////////////////////
void KaonTrackAnalysis::NominalKaon() {
    Double_t pkaon = 75.0;
    Double_t tthx = 0.0012;
    Double_t tthy = 0.;
    if (fYear==2015) {
        pkaon = 74.9;
        tthx = 0.00118; // 3809
        tthy = 0.;
    }
    if (fYear==2016) {
        pkaon = 74.9;
        tthx = 0.00122;
        tthy = 0.000026;
    }
    Double_t pz = pkaon/sqrt(1.+tthx*tthx+tthy*tthy);
    Double_t px = pz*tthx;
    Double_t py = pz*tthy;
    fKaonNominalMomentum.SetXYZM(px,py,pz,0.001*Constants::MKCH);
}


void KaonTrackAnalysis::TestMultiplicity(){
    double dtktag = fBeamTrack->GetKTAGTime()-fTrack->GetCHODTime();
    double dtchanti = fBeamTrack->GetCHANTITime()-fTrack->GetCHODTime();
    double ecal = fTrack->GetCalorimetricEnergy();
    double nchanti = fCHANTIEvent->GetNCandidates();
    double nchantihits = fCHANTIEvent->GetNHits();
    double strawthetax =fTrack->GetMomentum().Px()/fTrack->GetMomentum().Pz() ;
    double strawthetay =fTrack->GetMomentum().Py()/fTrack->GetMomentum().Pz() ;
    double kthetax =fBeamTrack->GetMomentum().Px()/fBeamTrack->GetMomentum().Pz() ;
    double kthetay =fBeamTrack->GetMomentum().Py()/fBeamTrack->GetMomentum().Pz() ;

    double richp = fTrack->GetRICHMultiMomentum().P();
    double richm = fTrack->GetRICHMultiMass();

    // double XAtMUV1 = fTrack->GetPositionAtMUV(0).X();
    // double YAtMUV1 = fTrack->GetPositionAtMUV(0).Y();
    // double XAtMUV2 = fTrack->GetPositionAtMUV(1).X();
    // double YAtMUV2 = fTrack->GetPositionAtMUV(1).Y();
    // double XAtMUV3 = fTrack->GetPositionAtMUV(2).X();
    // double YAtMUV3 = fTrack->GetPositionAtMUV(2).Y();
    Double_t caloEnergy = fTrack->GetCalorimetricEnergy();
    Double_t eovp = caloEnergy/fPTrack;
    Double_t e1 = fTrack->GetMUV1Energy();
    Double_t e2 = fTrack->GetMUV2Energy();
    // Double_t r1 = e1/caloEnergy;
    // Double_t r2 = e2/caloEnergy;
    Double_t sw1 = fTrack->GetMUV1ShowerWidth();
    Double_t sw2 = fTrack->GetMUV2ShowerWidth();
    Double_t elkr = fTrack->GetLKrEovP()*fPTrack;
    Double_t seedratio = fTrack->GetLKrSeedEnergy()/elkr;
    Double_t cellratio = fTrack->GetLKrNCells()/elkr;

    std::cout << "---------Event information for mult > 1 ---------" << std::endl;
    std::cout << "Burst ID***" << " EventID***" << " P***" << " Vtx***" << " MMiss2***" << " PKaon***" << " cda***" << endl;
    std::cout << fHeader->GetBurstID() << "  " << fHeader->GetEventNumber() << "  "  << fPTrack<< "  "  << fVertex.Z()<< "  "  << fMMiss<< "  "  << fPBeamTrack<< "  "  << fBeamTrack->GetCDA() << endl;
    std::cout << " Kaondt***" << "ECal***" << "ChantiNCand***" << "ChantiNHits***"<< "dtchanti***" << "xstraw***" << "ystraw***" << "xlkr***" << "ylkr*** "<< std::endl;
    std::cout << dtktag << "  " << ecal << "  " << nchanti<< "  "  << nchantihits<< "  "  << dtchanti<< "  "  << fXAtStraw1<< "  "  << fYAtStraw1<< "  "  << fXAtLKr<< "  "  << fYAtLKr << std::endl;
    std::cout << "kthetax***" << "kthetay***" << "strawthetax***" << "strawthetay***" << "segments***"<< endl;
    std::cout << kthetax     << "  " <<  kthetay << "  "<< strawthetax << "  "<< strawthetay << "  "<< fSegmentIB << std::endl;
    std::cout << "RICHP***" << "richmass***" << std::endl;
    std::cout << richp << "  " << richm << std::endl;
    std::cout << "LKr seedratio****" << "LKr cellratio***" << "swMUV1***" << "swMUV2***"<< "E LKr***" <<"EMUV1***" << "EMUV2***" << "E/p***" << std::endl;
    std::cout << seedratio << "  " << cellratio << "  " << sw1 << "  " << sw2 << "  " << elkr << "  " << e1 << "  " << e2 <<  "  "<< eovp << std::endl;


    cout << "extra lkr = " << fIsLKr << " extra lkr aux = " << fIsNewLKr << endl;
    std::cout << "NLKr Hits = " << fLKrEvent->GetNHits() << "NLKrCells =" << fTrack->GetLKrNCells() << " LKr Mult = " << fTrack->GetLKrMult()  <<  "CHOD mult =" << fTrack->GetCHODMult() << "NCHOD Candidates =" << fCHODEvent->GetNCandidates() << "NCHOD hits =" << fCHODEvent->GetNHits() << "NewCHOD mult = " << fTrack->GetNewCHODMult() << "NNewCHOD hits = " << fNewCHODEvent->GetNHits() << std::endl;

    return;
}

Int_t KaonTrackAnalysis::FineTiming(Int_t flag){

    double trich  = fTrack->GetRICHSingleTime();
    double tchod  = fTrack->GetCHODTime();
    double tcedar = fBeamTrack->GetKTAGTime();
    double tgtk   = fBeamTrack->GetTime();

    double dtchodrich  = tchod - trich;
    double dtchodcedar = tchod - tcedar;
    double dtchodgtk   = tchod - tgtk;
    double dtrichcedar = trich - tcedar;
    double dtrichgtk   = trich - tgtk;
    double dtcedargtk  = tcedar- tgtk;
    // double discriminant= (fabs(dtrichcedar)/(0.18*0.18) + fabs(dtrichgtk)/(0.19*0.19) + fabs(dtchodgtk)/(0.2*0.2) )/(1/(0.19*0.19) + 1/(0.2*0.2) + 1/(0.18*0.18));//Very preliminary
    double discriminant= pow(dtrichcedar/0.16,2) + pow(dtchodcedar/0.23,2) + pow(dtchodrich/0.26,2);

    double dTime[6];
    dTime[0] = fTrack->GetTime()-fFineTriggerTime; // Spectrometer
    dTime[1] = fTrack->GetCHODTime()-fFineTriggerTime; // CHOD
    dTime[2] = fTrack->GetLKrTime()-fFineTriggerTime; // LKr
    dTime[3] = fBeamTrack->GetKTAGTime()-fFineTriggerTime; //KTAG
    dTime[4] = fBeamTrack->GetTime()-fFineTriggerTime; // GTK
    dTime[5] = fTrack->GetRICHSingleTime()-fFineTriggerTime; // RICH single
    if(!flag){

        FillHisto("Pinunu_timing_dtchodrich" ,dtchodrich);
        FillHisto("Pinunu_timing_dtchodcedar",dtchodcedar);
        FillHisto("Pinunu_timing_dtchodgtk"  ,dtchodgtk);
        FillHisto("Pinunu_timing_dtcedarrich",dtrichcedar);
        FillHisto("Pinunu_timing_dtcedargtk" ,dtcedargtk);
        FillHisto("Pinunu_timing_dtrichgtk"  ,dtrichgtk);
        FillHisto("Pinunu_timing_dtchodrich_dtchodcedar",dtchodcedar,dtchodrich);
        FillHisto("Pinunu_timing_dtrichcedar_dtrichgtk",dtrichgtk,dtrichcedar);
        FillHisto("Pinunu_timing_dtcedarrich_dtcedarchod",dtrichcedar,dtchodcedar);
        FillHisto("Pinunu_timing_discriminant",discriminant);
        FillHisto("Pinunu_timing_dttrack",dTime[0]);
        FillHisto("Pinunu_timing_dtchod" ,dTime[1]);
        FillHisto("Pinunu_timing_dtlkr"  ,dTime[2]);
        FillHisto("Pinunu_timing_dtktag" ,dTime[3]);
        FillHisto("Pinunu_timing_dtgtk"  ,dTime[4]);
        FillHisto("Pinunu_timing_dtrichs",dTime[5]);

    }
    if(discriminant > 12) return 0;

    if(!flag){
        FillHisto("Pinunu_goodtiming_dtchodrich" ,dtchodrich);
        FillHisto("Pinunu_goodtiming_dtchodcedar",dtchodcedar);
        FillHisto("Pinunu_goodtiming_dtchodgtk"  ,dtchodgtk);
        FillHisto("Pinunu_goodtiming_dtcedarrich",dtrichcedar);
        FillHisto("Pinunu_goodtiming_dtcedargtk" ,dtcedargtk);
        FillHisto("Pinunu_goodtiming_dtrichgtk"  ,dtrichgtk);
        FillHisto("Pinunu_goodtiming_dtchodrich_dtchodcedar",dtchodcedar,dtchodrich);
        FillHisto("Pinunu_goodtiming_dtrichcedar_dtrichgtk",dtrichgtk,dtrichcedar);
        FillHisto("Pinunu_goodtiming_dtcedarrich_dtcedarchod",dtrichcedar,dtchodcedar);
    }

    // if(fabs(dtchodrich)>1) return 0;
    // if(fabs(dtchodgtk)>1) return 0;
    // if(fabs(dtchodcedar)>1) return 0;
    // if(fabs(dtrichcedar)>0.5) return 0;
    // if(fabs(dtrichgtk)>0.5) return 0;
    // if(fabs(dtcedargtk)>0.5) return 0;

    return 1;
}





// Int_t KaonTrackAnalysis::AnalyzeTrigger(int flag){

//   L0TPData* L0Data = GetL0Data();
//   UInt_t DataType =GetWithMC() ? 1 : L0Data->GetDataType();

//   //Set Primitive Reference time: CHOD.
//   //If bit 14 is set to 1
//   Int_t refchodprimtime=-9999999;
//   if(L0Data->GetPrimitive(0,0).GetPrimitiveID() & 0x1<<14){
//     refchodprimtime=(Int_t)L0Data->GetPrimitive(0,0).GetFineTime();
//   }


//   //Set Primitive Reference time: RICH.
//   Int_t refrichprimtime=-9999999;
//   if(L0Data->GetPrimitive(0,1).GetPrimitiveID() & 0x1<<14){
//     refrichprimtime=(Int_t)L0Data->GetPrimitive(0,1).GetFineTime();
//   }



//   std::map<int,std::pair<double,int>> mapmuv3;
//   TVector2 atMUV3(fTrack->GetPositionAtMUV(2).X(),fTrack->GetPositionAtMUV(2).Y());
//   TVector2 atRICHMirr(fTrack->GetPositionAtRICH(2).X(),fTrack->GetPositionAtRICH(2).Y());


//   TClonesArray &richhits = (*fRICHEvent->GetHits());
//   int nhobs=0;
//   for(int iH=0;iH < fRICHEvent->GetNHits() ; iH++){
//     TRecoRICHHit* richhit = (TRecoRICHHit*)richhits[iH];
//     if (richhit->GetOrSuperCellID()==1)
//       nhobs++;
//     //cout << "scid = " << richhit->GetSuperCellID() << "roch id = " << richhit->GetROChannelID() << endl;
//   }

//   FillHisto("Teff_All_dtprim", refrichprimtime - refchodprimtime);
//   FillHisto("Teff_AllRICH_NHits_vs_NSC",nhobs, fTrack->GetRICHSingleNHits());
//   FillHisto("Teff_AllRICH_atxy",atRICHMirr.X(),atRICHMirr.Y());
//   FillHisto("Teff_AllMUV3_NCandidates", fMUV3Event->GetNCandidates());
//   FillHisto("Teff_AllMUV3_atxy",atMUV3.X(),atMUV3.Y());


//   for (int i(0);i<fMUV3Event->GetNCandidates();i++){

//     TRecoMUV3Candidate *cMUV3 = (TRecoMUV3Candidate *)fMUV3Event->GetCandidate(i);
//     double dtime = cMUV3->GetTime() - fTrack->GetRICHSingleTime();
//     TVector2 pMUV3(cMUV3->GetX(),cMUV3->GetY());
//     double dist = (pMUV3-atMUV3).Mod();

//     FillHisto("Teff_MUV3_dist_vs_dtrich",dtime,dist);
//     FillHisto("Teff_MUV3_xy",pMUV3.X(),pMUV3.Y());
//     FillHisto("Teff_MUV3_atxy",atMUV3.X(),atMUV3.Y());
//     mapmuv3.insert(std::make_pair(dist,std::pair<double,int>(cMUV3->GetTime(),i)));
//   }

//   if(mapmuv3.size() > 0){

//     //Index, distance and dtchod of the closest in space cluster wrt NewCHOD
//     double mindist = mapmuv3.begin()->first;
//     double imin    = mapmuv3.begin()->second.second;
//     double mindt   = fTrack->GetRICHSingleTime() -  mapmuv3.begin()->second.first;
//     TRecoMUV3Candidate *minmuv3 = (TRecoMUV3Candidate *)fMUV3Event->GetCandidate(imin);

//     if(fMUV3L0){

//       FillHisto("Teff_MUV3_min_dist_vs_dtrich",mindt,mindist);
//       FillHisto("Teff_MUV3_min_xy",minmuv3->GetX(),minmuv3->GetY());
//       FillHisto("Teff_MUV3_min_atxy",atMUV3.X(),atMUV3.Y());

//     }
//     if(!fMUV3L0){

//       FillHisto("Teff_NoMUV3_min_dist_vs_dtrich",mindt,mindist);
//       FillHisto("Teff_NoMUV3_min_xy",minmuv3->GetX(),minmuv3->GetY());
//       FillHisto("Teff_NoMUV3_min_atxy",atMUV3.X(),atMUV3.Y());

//     }
//   } else {

//     if(!fMUV3L0)
//       FillHisto("Teff_NoMUV3Cand_xy",atMUV3.X(),atMUV3.Y());
//   }

//   FillHisto("CHODPrimID_vs_DataType",L0Data->GetPrimitive(0,0).GetPrimitiveID(),DataType);
//   FillHisto("RICHPrimID_vs_DataType",L0Data->GetPrimitive(0,1).GetPrimitiveID(),DataType);



//   if(!fRICHL0){

//     //FillHisto("Teff_NoRICH_NSC", nhobs);
//     //FillHisto("Teff_NoRICH_NHits", fTrack->GetRICHSingleNHits());
//     FillHisto("Teff_NoRICH_NHits_vs_NSC", nhobs,fTrack->GetRICHSingleNHits());
//     FillHisto("Teff_NoRICH_min_xy",atRICHMirr.X(),atRICHMirr.Y());
//     FillHisto("Teff_NoRICH_dtprim", refrichprimtime - refchodprimtime);

//     if(L0Data->GetPrimitive(0,0).GetPrimitiveID()==0){

//       FillHisto("RICHPrimIDSlot0_vs_DataType",L0Data->GetPrimitive(0,1).GetPrimitiveID(),DataType);
//       FillHisto("RICHPrimIDSlot1_vs_DataType",L0Data->GetPrimitive(1,1).GetPrimitiveID(),DataType);
//       FillHisto("RICHPrimIDSlot2_vs_DataType",L0Data->GetPrimitive(2,1).GetPrimitiveID(),DataType);
//     }

//     if(L0Data->GetPrimitive(0,1).GetPrimitiveID()==0){

//       //FillHisto("TimeStamp_vs_DataType",L0Data->GetPrimitive(0,0).GetPrimitiveID(),DataType);
//       FillHisto("CHODPrimIDSlot0_vs_DataType",L0Data->GetPrimitive(0,0).GetPrimitiveID(),DataType);
//       FillHisto("CHODPrimIDSlot1_vs_DataType",L0Data->GetPrimitive(1,0).GetPrimitiveID(),DataType);
//       FillHisto("CHODPrimIDSlot2_vs_DataType",L0Data->GetPrimitive(2,0).GetPrimitiveID(),DataType);

//       //cout << "DataType = " << DataType << "TimeStamp = " << L0Data->GetTimeStamp() <<  endl;
//     }

//   }
//   if(fRICHL0){

//     FillHisto("Teff_RICH_dtprim", refrichprimtime - refchodprimtime);
//     FillHisto("Teff_RICH_NHits_vs_NSC", nhobs,fTrack->GetRICHSingleNHits());
//     //FillHisto("Teff_RICH_NSC", nhobs);
//     //FillHisto("Teff_RICH_NHits", fTrack->GetRICHSingleNHits());
//     FillHisto("Teff_RICH_min_xy",atRICHMirr.X(),atRICHMirr.Y());

//   }

//   return 1;
// }

Int_t KaonTrackAnalysis::RICHL0Eff(Int_t &L0RICH){
    L0TPData *L0Data = GetL0Data();

    Bool_t Control=0;
    UInt_t TimeStamp=L0Data->GetTimeStamp();
    UInt_t DataType =GetWithMC() ? 1 : L0Data->GetDataType();

    if( DataType & 0x10  ) Control=1;
    if( Control==0 ) return 0;

    //initialize primitive time to -9999999:


    long long time[7][3]; //Timestamp + finetime [Detector][L0TP slot]

    for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
        for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){
            time[iTrigDet][iTrigSlot]=-99999;
        }
    }

    int nprim=0;
    Double_t primarray[3]={0.};
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

        if(L0Data->GetPrimitive(iTrigSlot,4).GetPrimitiveID() & 0x1<<14){
            nprim++;
            primarray[iTrigSlot]+=1;

        }

    }

    if(nprim!=1) return 0;

    //Set Primitive time
    for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
        for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

            /***********************
       Each positive primitive
       has bit 14 set at 1
            *************************/

            if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14){

                /*
                  Having only one timestamp, depending on the slot and on the finetime
                  we have to correct for the rollover. This is done in GetSlotTime.
                  Time is with a precision of 100 ps.
                */

                time[iTrigDet][iTrigSlot] = GetSlotTime((long long)TimeStamp,(Int_t)L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetFineTime(),(Int_t)GetEventHeader()->GetFineTime(),iTrigSlot,1);
                //std::cout << "Det = " << iTrigDet << "Slot = " << iTrigSlot<< std::endl;
                //cout << "orginal ts = " << L0Data->GetTimeStamp() << "Timestamp = " << TimeStamp << "long tstamp = " << (long long)TimeStamp << endl;
                //cout << GetSlotTime((long long)TimeStamp,(Int_t)L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetFineTime(),reffinetime,iTrigSlot,BitFineTime) << "casted ===" << time[iTrigDet][iTrigSlot] << endl;
            }
        }
    }


    //Set Primitive Reference time: CHOD or RICH.
    long long reftime=-999999;

    if(primarray[0]!=0) reftime=time[4][0];


    /*Loop for all L0TP detector and for each slot of the ram around the time of the reference*/
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){


        if(L0Data->GetPrimitive(iTrigSlot,1).GetPrimitiveID() & 0x1<<14)   {
            //Each detector was required in time closer than 10 ns with respect to the reference time
            if(TMath::Abs(time[1][iTrigSlot]-reftime)<100) {//In 10 ns wrt trigger
                L0RICH |= L0Data->GetPrimitive(iTrigSlot,1).GetPrimitiveID(); // The primitive ID is the OR of the primitve ID in time



            }
        }
    }


    return 1;

}

Int_t KaonTrackAnalysis::L0WithRICH(){

    if(GetWithMC()) return 0;
    L0TPData *L0Data = GetL0Data();
    UInt_t TimeStamp=L0Data->GetTimeStamp();

    //initialize primitive time to -9999999:
    long long time[7][3]; //Timestamp + finetime [Detector][L0TP slot]

    for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
        for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){
            time[iTrigDet][iTrigSlot]=-99999;
        }
    }

    int nprim=0;
    Double_t primarray[3]={0.};
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

        if(L0Data->GetPrimitive(iTrigSlot,1).GetPrimitiveID() & 0x1<<14){
            nprim++;
            primarray[iTrigSlot]+=1;

        }

    }

    if(nprim!=1) return 0;
    if(nprim==1 && !primarray[0]) return 0;

    //Set Primitive time
    for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
        for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

            /***********************
       Each positive primitive
       has bit 14 set at 1
            *************************/

            if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14){

                /*
                  Having only one timestamp, depending on the slot and on the finetime
                  we have to correct for the rollover. This is done in GetSlotTime.
                  Time is with a precision of 100 ps.
                */

                time[iTrigDet][iTrigSlot] = GetSlotTime((long long)TimeStamp,(Int_t)L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetFineTime(),(Int_t)GetEventHeader()->GetFineTime(),iTrigSlot,1);
            }
        }
    }


    //Set Primitive Reference time: CHOD or RICH.
    long long reftime=-999999;
    reftime=time[1][0];


    /*Now I check which detector is in time with the reference, and I calculate the primitive ID*/

    //Set Global Primitive ID

    Int_t GlobalPrimitiveID[7]={0,0,0,0,0,0,0};

    /*Loop for all L0TP detector and for each slot of the ram around the time of the reference*/

    for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
        for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){


            if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14)   {

                //Each detector was required in time closer than 10 ns with respect to the reference time
                if(TMath::Abs(time[iTrigDet][iTrigSlot]-reftime)<100) {//In 10 ns wrt trigger
                    GlobalPrimitiveID[iTrigDet] |= L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID(); // The primitive ID is the OR of the primitve ID in time



                }
            }
        }
    }


    //Trigger event set
    fNewCHODL0=0;
    fQxL0   =0;
    fUMTC   =0;
    //fRICHL0 =0;
    fMUV3L0  =0;
    fLAVL0    =0;
    fCaloL0   =0;


    //if(GlobalPrimitiveID[0]!=0)        fCHOD=1;
    //if(GlobalPrimitiveID[1]!=0)        fRICH=1;
    if(GlobalPrimitiveID[2]!=0)        fLAVL0=1;
    if(GlobalPrimitiveID[3]!=0)        fMUV3L0=1;
    if(GlobalPrimitiveID[4] & 0x1<<11) fQxL0=1;
    if(GlobalPrimitiveID[4]!=0)        fNewCHODL0=1;
    if(GlobalPrimitiveID[4] & 0x1<<12) fUMTC=1;
    if(GlobalPrimitiveID[6]!=0)        fCaloL0=1;

    return 1;
}

Int_t KaonTrackAnalysis::L0WithNewCHOD(){
    if(GetWithMC()) return 0;
    L0TPData *L0Data = GetL0Data();
    UInt_t TimeStamp=L0Data->GetTimeStamp();

    //initialize primitive time to -9999999:
    long long time[7][3]; //Timestamp + finetime [Detector][L0TP slot]

    for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
        for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){
            time[iTrigDet][iTrigSlot]=-99999;
        }
    }

    int nprim=0;
    Double_t primarray[3]={0.};
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

        if(L0Data->GetPrimitive(iTrigSlot,4).GetPrimitiveID() & 0x1<<14){
            nprim++;
            primarray[iTrigSlot]+=1;

        }

    }

    if(nprim!=1) return 0;
    if(nprim==1 && !primarray[0]) return 0;

    //Set Primitive time
    for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
        for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

            /***********************
       Each positive primitive
       has bit 14 set at 1
            *************************/

            if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14){

                /*
                  Having only one timestamp, depending on the slot and on the finetime
                  we have to correct for the rollover. This is done in GetSlotTime.
                  Time is with a precision of 100 ps.
                */

                time[iTrigDet][iTrigSlot] = GetSlotTime((long long)TimeStamp,(Int_t)L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetFineTime(),(Int_t)GetEventHeader()->GetFineTime(),iTrigSlot,1);
            }
        }
    }


    //Set Primitive Reference time: CHOD or RICH.
    long long reftime=-999999;
    reftime=time[4][0];


    /*Now I check which detector is in time with the reference, and I calculate the primitive ID*/

    //Set Global Primitive ID

    Int_t GlobalPrimitiveID[7]={0,0,0,0,0,0,0};

    /*Loop for all L0TP detector and for each slot of the ram around the time of the reference*/

    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){


        if(L0Data->GetPrimitive(iTrigSlot,1).GetPrimitiveID() & 0x1<<14)   {

            //Each detector was required in time closer than 10 ns with respect to the reference time
            if(TMath::Abs(time[1][iTrigSlot]-reftime)<60) {//In 10 ns wrt trigger
                GlobalPrimitiveID[1] |= L0Data->GetPrimitive(iTrigSlot,1).GetPrimitiveID(); // The primitive ID is the OR of the primitve ID in time



            }
        }
    }


    //Trigger event set
    fRICHL0 =0;

    if(GlobalPrimitiveID[1]!=0)        fRICHL0=1;

    return 1;
}


long long KaonTrackAnalysis::GetSlotTime(long long TimeStamp, Int_t FineTime, Int_t ReferenceFineTime, Int_t iTrigSlot, Int_t BitFineTime)
{
    long long time=0;
    if(iTrigSlot==0){
        time = (long long)(TimeStamp)   * 0x100  + (Int_t)FineTime;

        return time;
    }


    if(BitFineTime==0){
        if(iTrigSlot==1){
            time = (long long)(TimeStamp-1) * 0x100  + (Int_t)FineTime;
            return time;
        }
        if(iTrigSlot==2){
            time = (long long)(TimeStamp+1) * 0x100  + (Int_t)FineTime;
            return time;
        }
    }

    Int_t compare = 0;
    for (Int_t i = 0; i < BitFineTime; i++) {
        compare += (128 >> i);
    }
    if(ReferenceFineTime >= compare) {
        time= (long long)(TimeStamp + iTrigSlot - 1) * 0x100  + (Int_t)FineTime;
        return time;
    } else{
        time= (long long)(TimeStamp + iTrigSlot - 2) * 0x100  + (Int_t)FineTime;
        return time;
    }
    return -1;

}
void KaonTrackAnalysis::L0TriggerEfficiency(){

    if(fPTrack > 35 || fPTrack < 15 ) return;
    //Multi-track background rejection
    bool isMultiTrackForPiPi0 = false;
    Int_t multflag = fMultAnal[6]->ComputeMultiplicity(fTrack,fBeamTrack);
    isMultiTrackForPiPi0 = multflag > 0 ? true : false;

    if(isMultiTrackForPiPi0) return;

    FillHisto("Ctrl_mult_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    if(fBRPiPi0 && fisRICHRef){
        bool pinunutrigger = !fMUV3L0 && fNewCHODL0 && !fQxL0 && fUMTC;
        FillHisto("Ctrl_L0MUV3", fPTrack, fMUV3L0);
        FillHisto("Ctrl_L0NewCHOD", fPTrack, fNewCHODL0);
        FillHisto("Ctrl_L0Qx", fPTrack,fQxL0);
        FillHisto("Ctrl_L0UMTC", fPTrack,fUMTC);
        FillHisto("Ctrl_L0_nocal_nolav", fPTrack, pinunutrigger);
    }

    if(fisNewCHODRef){
        FillHisto("Ctrl_L0RICH", fPTrack, fRICHL0);
        //AnalyzeTrigger(0);
    }


    //LAV trigger efficiency using K2pi with two photons in the LKr
    if(fTrack->GetNPhotonLKrCandidates() != 2) return;
    if(fIsSAC || fIsIRC || fIsLAV) return;

    TVector2 ph1_lkr = fTrack->GetPhotonLKrCandidatePosition(0);
    TVector2 ph2_lkr = fTrack->GetPhotonLKrCandidatePosition(1);
    double gamma_distance = TMath::Sqr
        t(pow(ph1_lkr.X() - ph2_lkr.X(),2) + pow(ph1_lkr.Y() - ph2_lkr.Y(),2) );
    FillHisto("Ctrl_LAVTE_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_LAVTE_gamma1_x_vs_y_lkr",ph1_lkr.X(),ph1_lkr.Y());
    FillHisto("Ctrl_LAVTE_gamma2_x_vs_y_lkr",ph2_lkr.X(),ph2_lkr.Y());
    FillHisto("Ctrl_LAVTE_gamma_distance",gamma_distance);
    FillHisto("Ctrl_LAVTE_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    if(ph1_lkr.Mod() < 1000 && ph1_lkr.Mod() > 250 && ph2_lkr.Mod() < 1000 && ph2_lkr.Mod() > 250 && fBRPiPi0 )
        if(fisRICHRef)
            FillHisto("Ctrl_L0LAV", fPTrack, fLAVL0);



    return;
}


Int_t KaonTrackAnalysis::SelectK2piForRLKrStudy(){
    //LAV trigger efficiency using K2pi with two photons in the LKr
    if(fTrack->GetNPhotonLKrCandidates() != 2) return 0;
    if(fIsSAC || fIsIRC || fIsLAV) return 0;

    TVector2 ph1_lkr = fTrack->GetPhotonLKrCandidatePosition(0);
    TVector2 ph2_lkr = fTrack->GetPhotonLKrCandidatePosition(1);
    TVector2 pi_lkr(fTrack->GetPositionAtLKr().X(),fTrack->GetPositionAtLKr().Y());

    double dgamma1 = TMath::Sqrt(pow(ph1_lkr.X() - pi_lkr.X(),2) + pow(ph1_lkr.Y() - pi_lkr.Y(),2) );
    double dgamma2 = TMath::Sqrt(pow(pi_lkr.X() - ph2_lkr.X(),2) + pow(pi_lkr.Y() - ph2_lkr.Y(),2) );
    double dgamma = TMath::Sqrt(pow(ph1_lkr.X() - ph2_lkr.X(),2) + pow(ph1_lkr.Y() - ph2_lkr.Y(),2) );

    FillHisto("Ctrl_RPilkr_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_RPilkr_pi_x_vs_y_lkr",pi_lkr.X(),pi_lkr.Y());
    FillHisto("Ctrl_RPilkr_gamma1_x_vs_y_lkr",ph1_lkr.X(),ph1_lkr.Y());
    FillHisto("Ctrl_RPilkr_gamma2_x_vs_y_lkr",ph2_lkr.X(),ph2_lkr.Y());
    FillHisto("Ctrl_RPilkr_dgamma",dgamma);
    FillHisto("Ctrl_RPilkr_dgamma1",dgamma1);
    FillHisto("Ctrl_RPilkr_dgamma2",dgamma2);
    FillHisto("Ctrl_RPilkr_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    if(dgamma1 < 400 || dgamma2 < 400 || dgamma < 200 || !fBRPiPi0 ) return 0;

    FillHisto("Ctrl_RPilkr1_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_RPilkr1_pi_x_vs_y_lkr",pi_lkr.X(),pi_lkr.Y());
    FillHisto("Ctrl_RPilkr1_gamma1_x_vs_y_lkr",ph1_lkr.X(),ph1_lkr.Y());
    FillHisto("Ctrl_RPilkr1_gamma2_x_vs_y_lkr",ph2_lkr.X(),ph2_lkr.Y());
    FillHisto("Ctrl_RPilkr1_dgamma",dgamma);
    FillHisto("Ctrl_RPilkr1_dgamma1",dgamma1);
    FillHisto("Ctrl_RPilkr1_dgamma2",dgamma2);
    FillHisto("Ctrl_RPilkr1_mmiss_vs_mmissrich", fMMiss,fMMissRICH);

    fLKrAnal->SetTrackPosAtLKr(fXAtLKr,fYAtLKr,Constants::zLKr);
    fLKrAnal->SetTrackClPosAtLKr(fTrack->GetLKrPosition());
    fLKrAnal->SetTrackTime(fTrack->GetRICHSingleTime());
    fLKrAnal->SetTrackCHODTime(fTrack->GetCHODTime());
    Int_t rlkr = fLKrAnal->PiPlusLKrRadius(fPTrack);

    return 1;

}

Int_t KaonTrackAnalysis::SelectK2piForMultRV(){
    if(fIsLKr || fIsNewLKr || !fBRPiPi0) return 0;

    Int_t nlav = fTrack->GetNPhotonLAVCandidates();
    FillHisto("Ctrl_nlavintime",nlav);

    if(nlav < 1) return 0;

    int lavid=-1;
    int lavid1=-1;
    bool goodlav=true;
    bool good2lav=false;
    for (auto p(0);p<nlav;p++){

        double lavidph = fTrack->GetPhotonLAVCandidateLAVID(p);
        double time = fTrack->GetPhotonLAVCandidateTime(p);
        double dt = fTrack->GetRICHSingleTime()-time;
        if(p==0)lavid=lavidph;

        FillHisto("Ctrl_1lav_dtlav",dt);
        FillHisto("Ctrl_1lav_LAVID_all",lavidph);

        if(lavid!=lavidph) goodlav=false;
        if(lavid==lavidph && p!=0) goodlav=false;

    }

    FillHisto("Ctrl_1lav_LAVID",lavid);

    if(!goodlav) return 0;


    FillHisto("Ctrl_1lav_ringrad_vs_p",fPTrack,fTrack->GetRICHSingleRadius());
    FillHisto("Ctrl_1lav_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
    FillHisto("Ctrl_1lav_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Ctrl_1lav_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_1lav_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Ctrl_1lav_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    FillHisto("Ctrl_1lav_nlavintime",nlav);

    if(GetWithMC() && fmck2pig){

        FillHisto("Ctrl_1lav_mcg1_xend_vs_yend",fg1->GetEndPos().X(),fg1->GetEndPos().Y());
        FillHisto("Ctrl_1lav_mcg1_zend_vs_yend",fg1->GetEndPos().Z(),fg1->GetEndPos().Y());
        FillHisto("Ctrl_1lav_mcg1_zend_vs_xend",fg1->GetEndPos().Z(),fg1->GetEndPos().X());

        FillHisto("Ctrl_1lav_mcg2_xend_vs_yend",fg2->GetEndPos().X(),fg2->GetEndPos().Y());
        FillHisto("Ctrl_1lav_mcg2_zend_vs_xend",fg2->GetEndPos().Z(),fg2->GetEndPos().X());
        FillHisto("Ctrl_1lav_mcg2_zend_vs_yend",fg2->GetEndPos().Z(),fg2->GetEndPos().Y());

    }

    //The photon should come after LAV1 to avoid accidentals from LAV1/2
    // if(lavid > 2 && lavid < 12 && fPTrack < 35 && fPTrack > 15){
    if(lavid== 1 || lavid == 12) return 0;
    if(fPTrack > 35 || fPTrack < 15) return 0;

    FillHisto("Ctrl_1lavp_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Ctrl_1lavp_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
    if(fIsIRC && !fIsSAC){

        //Multi-track background rejection
        bool isMult1LAVIRC = false;
        Int_t mflag = fMultAnal[2]->ComputeMultiplicity(fTrack,fBeamTrack);
        isMult1LAVIRC = mflag > 0 ? true : false;

        FillHisto("Ctrl_1lavirc_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Ctrl_1lavirc_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
        if(!isMult1LAVIRC)
            FillHisto("Ctrl_1lavirc_mveto_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
    }

    if(!fIsIRC&&fIsSAC){

        //Multi-track background rejection
        bool isMult1LAVSAC = false;
        Int_t mflag = fMultAnal[3]->ComputeMultiplicity(fTrack,fBeamTrack);
        isMult1LAVSAC = mflag > 0 ? true : false;

        FillHisto("Ctrl_1lavsac_mmiss_vs_p",fPTrack,fMMiss);
        FillHisto("Ctrl_1lavsac_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
        if(!isMult1LAVSAC)
            FillHisto("Ctrl_1lavsac_mveto_mmiss_vs_mmissrich", fMMiss,fMMissRICH);
    }

    return 1;
}


bool KaonTrackAnalysis::OneParticleEvent(Int_t flag) {

    if(flag==1)    FillHisto("Pinunu_straw_ncand", fSpectrometerEvent->GetNCandidates());
    if(flag==2)    FillHisto("Pimin_straw_ncand", fSpectrometerEvent->GetNCandidates());

    if (fSpectrometerEvent->GetNCandidates()>2) return 0; //check
    bool isNegative,isPositive;
    isNegative= 0;
    isPositive= 0;
    for (int jt(0); jt<fSpectrometerEvent->GetNCandidates(); jt++) {
        TRecoSpectrometerCandidate *tr = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(jt);
        if(flag==1) FillHisto("Pinunu_straw_charge", tr->GetCharge());
        if(flag==2) FillHisto("Pimin_straw_charge", tr->GetCharge());

        if (tr->GetCharge()>0) isPositive = 1;
        if (tr->GetCharge()<0) isNegative = 1;
    }

    if(flag==1) FillHisto("Pinunu_straw_isnegative",isNegative);
    if(flag==2) FillHisto("Pimin_straw_ispositive",isPositive);

    if (isNegative && flag!=2) return 0; //check
    if (isPositive && flag==2) return 0; //check

    if(flag==1) FillHisto("Pinunu_straw_multicda",fStrawCandidate[fTrack->GetTrackID()].GetMultiCDA());
    if(flag==2) FillHisto("Pimin_straw_multicda",fStrawCandidate[fTrack->GetTrackID()].GetMultiCDA());
    return 1;
}

Int_t KaonTrackAnalysis::CaloTestsBeforePhotonRejection(){

    double mev2gev=0.001;
    double ecal = fTrack->GetCalorimetricEnergy();
    double emuv1= fTrack->GetMUV1Energy();
    double emuv2= fTrack->GetMUV2Energy();
    double elkr = fTrack->GetLKrEovP()*fPTrack;

    double etotlkr=0;
    TClonesArray& Hits = (*(fLKrEvent->GetHits()));

    for( int i(0);i<fLKrEvent->GetNHits();i++){

        TRecoLKrHit * lkr_hit = (TRecoLKrHit*)Hits.At(i);
        if(lkr_hit->GetEnergy() < 0) continue; // FIXME
        etotlkr+=lkr_hit->GetEnergy();
    }

    double eout0= mev2gev*etotlkr - elkr;
    double eout1= mev2gev*fTrack->GetMUV1OuterEnergy();
    double eout2= mev2gev*fTrack->GetMUV2OuterEnergy();
    double eout = eout0+eout1+eout2;
    double etot = ecal+eout;

    FillHisto("Pinunu_bph_ecalo_vs_p",ecal,fMMiss);
    FillHisto("Pinunu_bph_eouterlkr_vs_p",eout0,fMMiss);
    FillHisto("Pinunu_bph_eoutermuv1_vs_p",eout1,fMMiss);
    FillHisto("Pinunu_bph_eoutermuv2_vs_p",eout2,fMMiss);
    FillHisto("Pinunu_bph_eouter_vs_p",eout,fMMiss);
    FillHisto("Pinunu_bph_eouterhcal_vs_p",eout1+eout2,fMMiss);
    FillHisto("Pinunu_bph_ecalotot_vs_p",etot,fMMiss);


    if(fPTrack > 35 || fPTrack < 15) return 0;


    FillHisto("Pinunu_bph_eouterlkr_vs_prange",eout0,fMMiss);
    FillHisto("Pinunu_bph_eoutermuv1_vs_prange",eout1,fMMiss);
    FillHisto("Pinunu_bph_eoutermuv2_vs_prange",eout2,fMMiss);

    FillHisto("Pinunu_bph_ecalo_vs_prange",ecal,fMMiss);
    FillHisto("Pinunu_bph_eouter_vs_prange",eout,fMMiss);
    FillHisto("Pinunu_bph_eouterhcal_vs_prange",eout1+eout2,fMMiss);
    FillHisto("Pinunu_bph_ecalotot_vs_prange",etot,fMMiss);


}

Int_t KaonTrackAnalysis::CaloTestsAfterPhotonRejection(){


    double mev2gev=0.001;
    double ecal = fTrack->GetCalorimetricEnergy();
    double emuv1= fTrack->GetMUV1Energy();
    double emuv2= fTrack->GetMUV2Energy();
    double elkr = fTrack->GetLKrEovP()*fPTrack;

    double etotlkr=0;
    double noutlkr=0;
    double ntotlkr=0;
    TClonesArray& Hits = (*(fLKrEvent->GetHits()));

    for( int i(0);i<fLKrEvent->GetNHits();i++){

        TRecoLKrHit * lkr_hit = (TRecoLKrHit*)Hits.At(i);
        if(lkr_hit->GetEnergy() < 0) continue; // FIXME
        ntotlkr+=1;

        TVector3 trackAtLKr = fTrack->GetPositionAtLKr();
        Double_t dxlkr = lkr_hit->GetPosition().X() - trackAtLKr.X();
        Double_t dylkr = lkr_hit->GetPosition().Y() - trackAtLKr.Y();
        Double_t dlkr  = TMath::Sqrt(dxlkr*dxlkr + dylkr*dylkr) ;
        Double_t dt    = lkr_hit->GetTime() - fTrack->GetDownstreamTime();
        if(fabs(dt) < 20) continue;
        if(dlkr < 100) continue;
        if(lkr_hit->GetEnergy() > 50){
            //cout << "Extra hit energy = " << lkr_hit->GetEnergy() << " at distance = " << dlkr<< endl;
            etotlkr+=lkr_hit->GetEnergy();
            noutlkr+=1;
        }
    }

    double eout0= mev2gev*etotlkr;
    double eout1= mev2gev*fTrack->GetMUV1OuterEnergy();
    double eout2= mev2gev*fTrack->GetMUV2OuterEnergy();
    double eout = eout0+eout1+eout2;
    double etot = ecal+eout;

    FillHisto("Pinunu_aph_ecalo_vs_p",ecal,fMMiss);
    FillHisto("Pinunu_aph_etrack_vs_eolkr", elkr, eout0);
    FillHisto("Pinunu_aph_nolkr_vs_eolkr",eout0, noutlkr);
    FillHisto("Pinunu_aph_eouter_vs_p",eout,fMMiss);
    FillHisto("Pinunu_aph_eouterhcal_vs_p",eout1+eout2,fMMiss);
    FillHisto("Pinunu_aph_ecalotot_vs_p",etot,fMMiss);

    FillHisto("Pinunu_aph_eouterlkr_vs_p",eout0,fMMiss);
    FillHisto("Pinunu_aph_eoutermuv1_vs_p",eout1,fMMiss);
    FillHisto("Pinunu_aph_eoutermuv2_vs_p",eout2,fMMiss);

    if(fPTrack > 35 || fPTrack < 15) return 0;

    FillHisto("Pinunu_aph_eouterlkr_vs_prange",eout0,fMMiss);
    FillHisto("Pinunu_aph_eoutermuv1_vs_prange",eout1,fMMiss);
    FillHisto("Pinunu_aph_eoutermuv2_vs_prange",eout2,fMMiss);

    FillHisto("Pinunu_aph_ecalo_vs_prange",ecal,fMMiss);
    FillHisto("Pinunu_aph_eouter_vs_prange",eout,fMMiss);
    FillHisto("Pinunu_aph_eouterhcal_vs_prange",eout1+eout2,fMMiss);
    FillHisto("Pinunu_aph_ecalotot_vs_prange",etot,fMMiss);

}
Int_t KaonTrackAnalysis::PlotKmu2(){

    if(!fMuplus) return 0;

    TVector3 Pink     = fKplus->GetInitialMomentum();
    TVector3 Poutk    = fKplus->GetFinalMomentum();
    TVector3 vtxk     = fKplus->GetEndPos().Vect();

    TVector3 Pinmup   = fMuplus->GetInitialMomentum();
    TVector3 Poutmup  = fMuplus->GetFinalMomentum();
    TVector3 vtxpip   = fMuplus->GetEndPos().Vect();
    TVector3 mupatLKr = fMuplus->GetPosLKrEntry();
    TVector3 mupatRICH= fMuplus->GetPosRICHMirrorEntry();
    TString  procmup  = fMuplus->GetEndProcessName();

    TLorentzVector k4mom;
    k4mom.SetXYZM(Poutk.X()*0.001,Poutk.Y()*0.001,Poutk.Z()*0.001,0.493677);
    TLorentzVector mu4mom;
    mu4mom.SetXYZM(Pinmup.X()*0.001,Pinmup.Y()*0.001,Pinmup.Z()*0.001,0.13957);
    double mm2gen = (k4mom - mu4mom).Mag2();
    //cout << " kbef x = " << Pink.X() <<" kbef Y = " << Pink.Y() << " kbef z = " << Pink.Z() << endl;
    //cout << " kaft x = " << Poutk.X() <<" kaft Y = " << Poutk.Y() << " kaft z = " << Poutk.Z() << endl;
    //cout << " mubef x = " << Pinmup.X() <<" mubef Y = " << Pinmup.Y() << " mubef z = " << Pinmup.Z() << endl;
    //cout << " muaft x = " << Poutmup.X() <<" muaft Y = " << Poutmup.Y() << " muaft z = " << Poutmup.Z() << endl;
    FillHisto("Munu_kinres_vs_p",fPTrack, fMMiss-mm2gen);
}

Int_t KaonTrackAnalysis::PlotKe4(){
    if(!fPiminus) return 0;
    if(!fEplus) return 0;

    if(fTrack->GetCharge()==1)
        std::cout << "PRINTOUT BEGIN Positive <<<<<<<<<< ----------------" << std::endl;
    if(fTrack->GetCharge()==-1)
        std::cout << "PRINTOUT BEGIN Negative <<<<<<<<<< ----------------" << std::endl;
    std::cout << "List of particles in the event ----------------" << std::endl;
    std::cout << "Burst ID***" << " EventID***" << endl;
    std::cout << fHeader->GetBurstID() << "  " << fHeader->GetEventNumber() << "  "  << endl;
    std::cout << "Filename: " << Form("/eos/experiment/na62/data/offline/grid/mc/prod/v0.11.0/Kch2pipienu-7/reco/v0.11.0/reco_c60_dr6610_r%d.root",fHeader->GetBurstID()) << std::endl;
    KinePart* mumin= NULL;

    TVector3 Pinmum   (0.,0.,0.);
    TVector3 Poutmum  (0.,0.,0.);
    TVector3 vtxmum   (0.,0.,0.);
    TVector3 mumatLKr (0.,0.,0.);
    TVector3 mumatRICH(0.,0.,0.);
    TString  procmum="";

    TIter partit(GetMCEvent()->GetKineParts()) ;
    while(partit.Next()){
        std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        if(((KinePart*)(*partit))->GetParticleName().EqualTo("mu-")){
            mumin = ((KinePart*)(*partit));

            Pinmum   = mumin->GetInitialMomentum();
            Poutmum  = mumin->GetFinalMomentum();
            vtxmum   = mumin->GetEndPos().Vect();
            mumatLKr = mumin->GetPosLKrEntry();
            mumatRICH= mumin->GetPosRICHMirrorEntry();
            procmum  = mumin->GetEndProcessName();

        }
    }
    std::cout << "ENDOF List of particles in the event ----------------" << std::endl;

    TVector3 Pink     = fKplus->GetInitialMomentum();
    TVector3 vtxk     = fKplus->GetEndPos().Vect();

    TVector3 Pinpip   = fPiplus->GetInitialMomentum();
    TVector3 Poutpip  = fPiplus->GetFinalMomentum();
    TVector3 vtxpip   = fPiplus->GetEndPos().Vect();
    TVector3 pipatLKr = fPiplus->GetPosLKrEntry();
    TVector3 pipatRICH= fPiplus->GetPosRICHMirrorEntry();
    TString procpip   = fPiplus->GetEndProcessName();

    TVector3 Pinpim   = fPiminus->GetInitialMomentum();
    TVector3 Poutpim  = fPiminus->GetFinalMomentum();
    TVector3 vtxpim   = fPiminus->GetEndPos().Vect();
    TVector3 pimatLKr = fPiminus->GetPosLKrEntry();
    TVector3 pimatRICH= fPiminus->GetPosRICHMirrorEntry();
    TString procpim   = fPiminus->GetEndProcessName();

    TVector3 Pinpos   = fEplus->GetInitialMomentum();
    TVector3 Poutpos  = fEplus->GetFinalMomentum();
    TVector3 vtxpos   = fEplus->GetEndPos().Vect();
    TVector3 posatLKr = fEplus->GetPosLKrEntry();
    TVector3 posatRICH= fEplus->GetPosRICHMirrorEntry();
    TString procpos   = fEplus->GetEndProcessName();


    double slope_pip = 0;

    TVector3 pips1pos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,183508);
    TVector3 pips2pos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,194066);
    TVector3 pips3pos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,204460);
    TVector3 pips4pos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,218885);
    TVector3 piprichpos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,236873);
    TVector3 pipm0pos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,237358);
    TVector3 piplkrpos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,241093);
    TVector3 pipm3pos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,246800);
    TVector3 piphacpos = fTool->Propagate(1,&Pinpip,&vtxk,&slope_pip,247800);

    std::cout << "--------------------Ke4 Pi+ ----------------------------" << std::endl;
    std::cout << "Ke4 Vertex          :x = " << vtxk.X()     << "y = " << vtxk.Y() << "z = " << vtxk.Z() <<std::endl;
    std::cout << "Secondary Vertex    :x = " << vtxpip.X()   << "y = " << vtxpip.Y() << "z = " << vtxpip.Z() <<std::endl;
    std::cout << "Initial Momentum P in  = " << Pinpip.Mag() << ":x = " << Pinpip.X() << "y = " << Pinpip.Y() << "z = " << Pinpip.Z() <<std::endl;
    std::cout << "At STRAW1           :x = " << fPiplus->GetPosAtCheckPoint(4).X()  << "y = " << fPiplus->GetPosAtCheckPoint(4).Y()   << "z = " << fPiplus->GetPosAtCheckPoint(4).Z()   <<std::endl;
    std::cout << "At bMNP33           :x = " << fPiplus->GetPosAtCheckPoint(5).X()  << "y = " << fPiplus->GetPosAtCheckPoint(5).Y()   << "z = " << fPiplus->GetPosAtCheckPoint(5).Z()   <<std::endl;
    std::cout << "At eMNP33           :x = " << fPiplus->GetPosAtCheckPoint(6).X()  << "y = " << fPiplus->GetPosAtCheckPoint(6).Y()   << "z = " << fPiplus->GetPosAtCheckPoint(6).Z()   <<std::endl;
    std::cout << "At RICH             :x = " << fPiplus->GetPosAtCheckPoint(7).X()  << "y = " << fPiplus->GetPosAtCheckPoint(7).Y()   << "z = " << fPiplus->GetPosAtCheckPoint(7).Z()   <<std::endl;
    std::cout << "At LKr              :x = " << fPiplus->GetPosAtCheckPoint(9).X()  << "y = " << fPiplus->GetPosAtCheckPoint(9).Y()   << "z = " << fPiplus->GetPosAtCheckPoint(9).Z()   <<std::endl;
    std::cout << "At MUV3             :x = " << fPiplus->GetPosAtCheckPoint(13).X() << "y = " << fPiplus->GetPosAtCheckPoint(13).Y()  << "z = " << fPiplus->GetPosAtCheckPoint(13).Z()  <<std::endl;
    std::cout << procpip.Data() << std::endl;

    std::cout << "--------------------Ke4 Pi- ----------------------------" << std::endl;
    std::cout << "Muon-Pion Vertex    :x = " << vtxpim.X() << "y = " << vtxpim.Y() << "z = " << vtxpim.Z() <<std::endl;
    std::cout << "Initial Momentum P in  = " << Pinpim.Mag() << ":x = " << Pinpim.X() << "y = " << Pinpim.Y() << "z = " << Pinpim.Z() <<std::endl;
    std::cout << "Final momentum   P out = " << Poutpim.Mag() << ":x = " << Poutpim.X() << "y = " << Poutpim.Y() << "z = " << Poutpim.Z() <<std::endl;
    std::cout << "At STRAW1           :x = " << fPiminus->GetPosAtCheckPoint(4).X()  << "y = " << fPiminus->GetPosAtCheckPoint(4).Y()   << "z = " << fPiminus->GetPosAtCheckPoint(4).Z()   <<std::endl;
    std::cout << "At bMNP33           :x = " << fPiminus->GetPosAtCheckPoint(5).X()  << "y = " << fPiminus->GetPosAtCheckPoint(5).Y()   << "z = " << fPiminus->GetPosAtCheckPoint(5).Z()   <<std::endl;
    std::cout << "At eMNP33           :x = " << fPiminus->GetPosAtCheckPoint(6).X()  << "y = " << fPiminus->GetPosAtCheckPoint(6).Y()   << "z = " << fPiminus->GetPosAtCheckPoint(6).Z()   <<std::endl;
    std::cout << "At RICH             :x = " << fPiminus->GetPosAtCheckPoint(7).X()  << "y = " << fPiminus->GetPosAtCheckPoint(7).Y()   << "z = " << fPiminus->GetPosAtCheckPoint(7).Z()   <<std::endl;
    std::cout << "At LKr   other      :x = " << fPiminus->GetPosLKrEntry().X()  << "y = " << fPiminus->GetPosLKrEntry().Y()   << "z = " << fPiminus->GetPosLKrEntry().Z()   <<std::endl;
    std::cout << "At MUV3             :x = " << fPiminus->GetPosAtCheckPoint(13).X() << "y = " << fPiminus->GetPosAtCheckPoint(13).Y()  << "z = " << fPiminus->GetPosAtCheckPoint(13).Z()  <<std::endl;
    std::cout << procpim.Data() << std::endl;

    double slope_pim = 0;
    //Pim extrapolated position
    TVector3 pims1pos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,183508);
    TVector3 pims2pos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,194066);
    TVector3 pims3pos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,204460);
    TVector3 pims4pos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,218885);
    TVector3 pimrichpos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,236873);
    TVector3 pimm0pos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,237358);
    TVector3 pimlkrpos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,241093);
    TVector3 pimm3pos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,246800);
    TVector3 pimhacpos = fTool->Propagate(-1,&Pinpim,&vtxk,&slope_pim,247800);

    std::cout << "At STRAW1   :x = " << pims1pos.X()  << " y = " << pims1pos.Y()  << std::endl;
    std::cout << "At STRAW2   :x = " << pims2pos.X()  << " y = " << pims2pos.Y()  << std::endl;
    std::cout << "At STRAW3   :x = " << pims3pos.X()  << " y = " << pims3pos.Y()  << std::endl;
    std::cout << "At STRAW4   :x = " << pims4pos.X()  << " y = " << pims4pos.Y()  << std::endl;
    std::cout << "At RICH Mirr:x = " << pimrichpos.X()<< " y = " << pimrichpos.Y()<< std::endl;
    std::cout << "At MUV0     :x = " << pimm0pos.X()  << " y = " << pimm0pos.Y()  << std::endl;
    std::cout << "At LKr      :x = " << pimlkrpos.X() << " y = " << pimlkrpos.Y() << std::endl;
    std::cout << "At MUV3     :x = " << pimm3pos.X()  << " y = " << pimm3pos.Y()  << std::endl;
    std::cout << "At HAC      :x = " << pimhacpos.X() << " y = " << pimhacpos.Y() << std::endl;

    if(mumin){
        std::cout << "-----------------------Mu- from Pi- decay --------" << std::endl;

        std::cout << "Muon-Pion Vertex    :x = " << vtxpim.X() << "y = " << vtxpim.Y() << "z = " << vtxpim.Z() <<std::endl;
        std::cout << "Muon-Electron Vertex    :x = " << vtxmum.X() << "y = " << vtxmum.Y() << "z = " << vtxmum.Z() <<std::endl;
        std::cout << "Initial Momentum P in  = " << Pinmum.Mag() << ":x = " << Pinmum.X() << "y = " << Pinmum.Y() << "z = " << Pinmum.Z() <<std::endl;
        std::cout << "Final momentum   P out = " << Poutmum.Mag() << ":x = " << Poutmum.X() << "y = " << Poutmum.Y() << "z = " << Poutmum.Z() <<std::endl;
        std::cout << "At STRAW1           :x = " << mumin->GetPosAtCheckPoint(4).X()  << "y = " << mumin->GetPosAtCheckPoint(4).Y()   << "z = " << mumin->GetPosAtCheckPoint(4).Z()   <<std::endl;
        std::cout << "At bMNP33           :x = " << mumin->GetPosAtCheckPoint(5).X()  << "y = " << mumin->GetPosAtCheckPoint(5).Y()   << "z = " << mumin->GetPosAtCheckPoint(5).Z()   <<std::endl;
        std::cout << "At eMNP33           :x = " << mumin->GetPosAtCheckPoint(6).X()  << "y = " << mumin->GetPosAtCheckPoint(6).Y()   << "z = " << mumin->GetPosAtCheckPoint(6).Z()   <<std::endl;
        std::cout << "At RICH             :x = " << mumin->GetPosAtCheckPoint(7).X()  << "y = " << mumin->GetPosAtCheckPoint(7).Y()   << "z = " << mumin->GetPosAtCheckPoint(7).Z()   <<std::endl;
        std::cout << "At LKr              :x = " << mumin->GetPosAtCheckPoint(9).X()  << "y = " << mumin->GetPosAtCheckPoint(9).Y()   << "z = " << mumin->GetPosAtCheckPoint(9).Z()   <<std::endl;
        std::cout << "At MUV3             :x = " << mumin->GetPosAtCheckPoint(13).X() << "y = " << mumin->GetPosAtCheckPoint(13).Y()  << "z = " << mumin->GetPosAtCheckPoint(13).Z()  <<std::endl;
        std::cout << procmum.Data() << std::endl;

        double slopeafter=0;

        //Mu extrapolated position
        TVector3 mus1pos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,183508);
        TVector3 mus2pos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,194066);
        TVector3 mus3pos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,204460);
        TVector3 mus4pos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,218885);
        TVector3 murichpos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,236873);
        TVector3 mum0pos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,237358);
        TVector3 mulkrpos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,241093);
        TVector3 mum3pos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,246800);
        TVector3 mumhacpos = fTool->Propagate(-1,&Pinmum,&vtxpim,&slopeafter,247800);
        double mumxhac = mum3pos.X() + ( Pinmum.X()/Pinmum.Z() + 990./Pinmum.Mag())*(247800 - 246800);
        std::cout << "Vertex of the Pi- decay Z = " << vtxpim.Z() <<std::endl;
        std::cout << "At HAC      :x = " << mumhacpos.X()<< " y = " << mumhacpos.Y()<< std::endl;
        mumhacpos.SetX(mumxhac);

        std::cout << "At STRAW1   :x = " << mus1pos.X()  << " y = " << mus1pos.Y()  << std::endl;
        std::cout << "At STRAW2   :x = " << mus2pos.X()  << " y = " << mus2pos.Y()  << std::endl;
        std::cout << "At STRAW3   :x = " << mus3pos.X()  << " y = " << mus3pos.Y()  << std::endl;
        std::cout << "At STRAW4   :x = " << mus4pos.X()  << " y = " << mus4pos.Y()  << std::endl;
        std::cout << "At RICH Mirr:x = " << murichpos.X()<< " y = " << murichpos.Y()<< std::endl;
        std::cout << "At MUV0     :x = " << mum0pos.X()  << " y = " << mum0pos.Y()  << std::endl;
        std::cout << "At LKr      :x = " << mulkrpos.X() << " y = " << mulkrpos.Y() << std::endl;
        std::cout << "At MUV3     :x = " << mum3pos.X()  << " y = " << mum3pos.Y()  << std::endl;
        std::cout << "At HAC      :x = " << mumhacpos.X()<< " y = " << mumhacpos.Y()<< std::endl;
    }

    std::cout << "--------------------Ke4e+ ----------------------------" << std::endl;
    double slope_pos = 0;
    //Pos extrapolated position
    TVector3 poss1pos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,183508);
    TVector3 poss2pos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,194066);
    TVector3 poss3pos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,204460);
    TVector3 poss4pos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,218885);
    TVector3 posrichpos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,236873);
    TVector3 posm0pos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,237358);
    TVector3 poslkrpos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,241093);
    TVector3 posm3pos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,246800);
    TVector3 poshacpos = fTool->Propagate(1,&Pinpos,&vtxk,&slope_pos,247800);

    std::cout << "Ke4 Vertex          :x = " << vtxk.X() << "y = " << vtxk.Y() << "z = " << vtxk.Z() <<std::endl;
    std::cout << "Electron end        :x = " << vtxpos.X() << "y = " << vtxpos.Y() << "z = " << vtxpos.Z() <<std::endl;

    std::cout << "Initial Momentum P in  = " << Pinpos.Mag() << ":x = " << Pinpos.X() << "y = " << Pinpos.Y() << "z = " << Pinpos.Z() <<std::endl;
    std::cout << "Final momentum   P out = " << Poutpos.Mag() << ":x = " << Poutpos.X() << "y = " << Poutpos.Y() << "z = " << Poutpos.Z() <<std::endl;
    std::cout << "At STRAW1           :x = " << fEplus->GetPosAtCheckPoint(4).X()  << "y = " << fEplus->GetPosAtCheckPoint(4).Y()   << "z = " << fEplus->GetPosAtCheckPoint(4).Z()   <<std::endl;
    std::cout << "At bMNP33           :x = " << fEplus->GetPosAtCheckPoint(5).X()  << "y = " << fEplus->GetPosAtCheckPoint(5).Y()   << "z = " << fEplus->GetPosAtCheckPoint(5).Z()   <<std::endl;
    std::cout << "At eMNP33           :x = " << fEplus->GetPosAtCheckPoint(6).X()  << "y = " << fEplus->GetPosAtCheckPoint(6).Y()   << "z = " << fEplus->GetPosAtCheckPoint(6).Z()   <<std::endl;
    std::cout << "At RICH             :x = " << fEplus->GetPosAtCheckPoint(7).X()  << "y = " << fEplus->GetPosAtCheckPoint(7).Y()   << "z = " << fEplus->GetPosAtCheckPoint(7).Z()   <<std::endl;
    std::cout << "At LKr              :x = " << fEplus->GetPosAtCheckPoint(9).X()  << "y = " << fEplus->GetPosAtCheckPoint(9).Y()   << "z = " << fEplus->GetPosAtCheckPoint(9).Z()   <<std::endl;
    std::cout << "At MUV3             :x = " << fEplus->GetPosAtCheckPoint(13).X() << "y = " << fEplus->GetPosAtCheckPoint(13).Y()  << "z = " << fEplus->GetPosAtCheckPoint(13).Z()  <<std::endl;
    std::cout << procpos.Data() << std::endl;

    std::cout << "At STRAW1   :x = " << poss1pos.X()  << " y = " << poss1pos.Y()  << std::endl;
    std::cout << "At STRAW2   :x = " << poss2pos.X()  << " y = " << poss2pos.Y()  << std::endl;
    std::cout << "At STRAW3   :x = " << poss3pos.X()  << " y = " << poss3pos.Y()  << std::endl;
    std::cout << "At STRAW4   :x = " << poss4pos.X()  << " y = " << poss4pos.Y()  << std::endl;
    std::cout << "At RICH Mirr:x = " << posrichpos.X()<< " y = " << posrichpos.Y()<< std::endl;
    std::cout << "At MUV0     :x = " << posm0pos.X()  << " y = " << posm0pos.Y()  << std::endl;
    std::cout << "At LKr      :x = " << poslkrpos.X() << " y = " << poslkrpos.Y() << std::endl;
    std::cout << "At MUV3     :x = " << posm3pos.X()  << " y = " << posm3pos.Y()  << std::endl;
    std::cout << "At HAC      :x = " << poshacpos.X() << " y = " << poshacpos.Y() << std::endl;

    std::cout << "mm2 = " << fMMiss << " p = " << fPTrack << " xlkr = " << fXAtLKr  << " ylkr = " << fYAtLKr<< " xstraw1 = " << fXAtStraw1 << " ystraw1 = " << fYAtStraw1  << std::endl;
    std::cout << "PRINTOUT END <<<<<<<<<< ----------------" << std::endl;
    // FillHisto("Pinunu_testr4_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    // FillHisto("Pinunu_testr4_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    // FillHisto("Pinunu_testr4_x_vs_y_muv1",XAtMUV1, YAtMUV1);
    // FillHisto("Pinunu_testr4_x_vs_y_muv2",XAtMUV2, YAtMUV2);
    // FillHisto("Pinunu_testr4_x_vs_y_muv3",XAtMUV3, YAtMUV3);


    if(fTrack->GetCharge()==1){

        if(fPiminus->GetPosAtCheckPoint(4).X()!=0 && fPiminus->GetPosAtCheckPoint(4).Y()!=0)
            FillHisto("Ke4Print_Pim_x_vs_y_straw1", fPiminus->GetPosAtCheckPoint(4).X(), fPiminus->GetPosAtCheckPoint(4).Y() );
        if(fPiminus->GetPosAtCheckPoint(5).X()!=0 && fPiminus->GetPosAtCheckPoint(5).Y()!=0)
            FillHisto("Ke4Print_Pim_x_vs_y_straw2", fPiminus->GetPosAtCheckPoint(5).X(), fPiminus->GetPosAtCheckPoint(5).Y() );
        if(fPiminus->GetPosAtCheckPoint(7).X()!=0 && fPiminus->GetPosAtCheckPoint(7).Y()!=0)
            FillHisto("Ke4Print_Pim_x_vs_y_RICHMirror", fPiminus->GetPosAtCheckPoint(7).X(), fPiminus->GetPosAtCheckPoint(7).Y() );

        if(fPiminus->GetPosAtCheckPoint(9).X()!=0 && fPiminus->GetPosAtCheckPoint(9).Y()!=0)
            FillHisto("Ke4Print_Pim_x_vs_y_LKr", fPiminus->GetPosAtCheckPoint(9).X(), fPiminus->GetPosAtCheckPoint(9).Y() );

        if(fPiminus->GetPosAtCheckPoint(10).X()!=0 && fPiminus->GetPosAtCheckPoint(10).Y()!=0)
            FillHisto("Ke4Print_Pim_x_vs_y_MUV1", fPiminus->GetPosAtCheckPoint(10).X(), fPiminus->GetPosAtCheckPoint(10).Y() );


        if(fPiminus->GetPosAtCheckPoint(11).X()!=0 && fPiminus->GetPosAtCheckPoint(11).Y()!=0)
            FillHisto("Ke4Print_Pim_x_vs_y_MUV2", fPiminus->GetPosAtCheckPoint(11).X(), fPiminus->GetPosAtCheckPoint(11).Y() );

        if(fPiminus->GetPosAtCheckPoint(13).X()!=0 && fPiminus->GetPosAtCheckPoint(13).Y()!=0)
            FillHisto("Ke4Print_Pim_x_vs_y_MUV3", fPiminus->GetPosAtCheckPoint(13).X(), fPiminus->GetPosAtCheckPoint(13).Y() );

    } else {

        if(fPiplus->GetPosAtCheckPoint(4).X()!=0 && fPiplus->GetPosAtCheckPoint(4).Y()!=0)
            FillHisto("Ke4Print_Pip_x_vs_y_straw1", fPiplus->GetPosAtCheckPoint(4).X(), fPiplus->GetPosAtCheckPoint(4).Y() );
        if(fPiplus->GetPosAtCheckPoint(5).X()!=0 && fPiplus->GetPosAtCheckPoint(5).Y()!=0)
            FillHisto("Ke4Print_Pip_x_vs_y_straw2", fPiplus->GetPosAtCheckPoint(5).X(), fPiplus->GetPosAtCheckPoint(5).Y() );
        if(fPiplus->GetPosAtCheckPoint(7).X()!=0 && fPiplus->GetPosAtCheckPoint(7).Y()!=0)
            FillHisto("Ke4Print_Pip_x_vs_y_RICHMirror", fPiplus->GetPosAtCheckPoint(7).X(), fPiplus->GetPosAtCheckPoint(7).Y() );
        if(fPiplus->GetPosAtCheckPoint(9).X()!=0 && fPiplus->GetPosAtCheckPoint(9).Y()!=0)
            FillHisto("Ke4Print_Pip_x_vs_y_LKr", fPiplus->GetPosAtCheckPoint(9).X(), fPiplus->GetPosAtCheckPoint(9).Y() );

        if(fPiplus->GetPosAtCheckPoint(10).X()!=0 && fPiplus->GetPosAtCheckPoint(10).Y()!=0)
            FillHisto("Ke4Print_Pip_x_vs_y_MUV1", fPiplus->GetPosAtCheckPoint(10).X(), fPiplus->GetPosAtCheckPoint(10).Y() );


        if(fPiplus->GetPosAtCheckPoint(11).X()!=0 && fPiplus->GetPosAtCheckPoint(11).Y()!=0)
            FillHisto("Ke4Print_Pip_x_vs_y_MUV2", fPiplus->GetPosAtCheckPoint(11).X(), fPiplus->GetPosAtCheckPoint(11).Y() );

        if(fPiplus->GetPosAtCheckPoint(13).X()!=0 && fPiplus->GetPosAtCheckPoint(13).Y()!=0)
            FillHisto("Ke4Print_Pip_x_vs_y_MUV3", fPiplus->GetPosAtCheckPoint(13).X(), fPiplus->GetPosAtCheckPoint(13).Y() );

    }


}


bool KaonTrackAnalysis::ExtrapolateBack(Int_t region, bool ischanti, bool flag){
    if(region ==0) return 0;
    TString type;
    if(flag==0) type="Pinunu";
    if(flag==1) type="Pimin";

    TRecoSpectrometerCandidate *spectrack = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fTrack->GetTrackID());
    double zFCol = 100800.; // [ mm ]
    double zTRIM5 = 101800.; // [ mm ]
    double zGTK3 = 102400 ; // [ mm ]
    double zCHANTIFront = 102425 ; // [ mm ]
    double zCHANTIEnd = 104458 ; // [ mm ]
    TVector3 atCHANTIzEnd = fTool->GetPositionAtZ(spectrack,zCHANTIEnd);
    TVector3 atFColz = fTool->GetPositionAtZ(spectrack,zFCol);
    TVector3 atGTK3z = fTool->GetPositionAtZ(spectrack,zGTK3);
    TVector3 atTRIM5z = fTool->GetPositionAtZ(spectrack,zTRIM5);

    if(ischanti){

        FillHisto(Form("%s_r%d_ischanti_specthx_vs_specthy",type.Data(),region),spectrack->GetSlopeXBeforeMagnet(),spectrack->GetSlopeYBeforeMagnet());
        FillHisto(Form("%s_r%d_ischanti_chantiend_x_vs_y",type.Data(),region),atCHANTIzEnd.X(),atCHANTIzEnd.Y());
        FillHisto(Form("%s_r%d_ischanti_fcol_x_vs_y",type.Data(),region),atGTK3z.X(),atGTK3z.Y());
        FillHisto(Form("%s_r%d_ischanti_gtk3_x_vs_y",type.Data(),region),atGTK3z.X(),atGTK3z.Y());
        FillHisto(Form("%s_r%d_ischanti_trim5_x_vs_y",type.Data(),region),atTRIM5z.X(),atTRIM5z.Y());
        FillHisto(Form("%s_r%d_ischanti_straw1_x_vs_y",type.Data(),region),fXAtStraw1,fYAtStraw1);

    } else {

        FillHisto(Form("%s_r%d_notchanti_specthx_vs_specthy",type.Data(),region),spectrack->GetSlopeXBeforeMagnet(),spectrack->GetSlopeYBeforeMagnet());
        FillHisto(Form("%s_r%d_notchanti_chantiend_x_vs_y",type.Data(),region),atCHANTIzEnd.X(),atCHANTIzEnd.Y());
        FillHisto(Form("%s_r%d_notchanti_fcol_x_vs_y",type.Data(),region),atFColz.X(),atFColz.Y());
        FillHisto(Form("%s_r%d_notchanti_gtk3_x_vs_y",type.Data(),region),atGTK3z.X(),atGTK3z.Y());
        FillHisto(Form("%s_r%d_notchanti_trim5_x_vs_y",type.Data(),region),atTRIM5z.X(),atTRIM5z.Y());
        FillHisto(Form("%s_r%d_notchanti_straw1_x_vs_y",type.Data(),region),fXAtStraw1,fYAtStraw1);

    }

    if( fabs(atTRIM5z.X()) < 100 && fabs(atTRIM5z.Y()) < 500) return 1;

    // if(atTRIM5z.Y() > 150 && atTRIM5z.Y() < 300 && fabs(atTRIM5z.X())< 50 ) return 1;
    // if(atFColz.X() > -50 && atFColz.X() < -35 && atFColz.Y() >= 50 && atFColz.Y() <= 150) return 1;


    // if(atTRIM5z.Y() < -150 && atTRIM5z.Y() > -300 && fabs(atTRIM5z.X())< 50 ) return 1;
    // if(atFColz.X() < 50 && atFColz.X() > 35 && atFColz.Y() <= -50 && atFColz.Y() >= -150) return 1;


    return 0;
}
