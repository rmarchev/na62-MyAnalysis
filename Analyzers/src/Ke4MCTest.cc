#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "Ke4MCTest.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


Ke4MCTest::Ke4MCTest(Core::BaseAnalysis *ba) : Analyzer(ba, "Ke4MCTest"){

  //RequestTree("MC",new TMCEvent);


}

void Ke4MCTest::InitOutput(){
}

void Ke4MCTest::InitHist(){
  if(GetWithMC()){

      BookHisto(new TH1F("LABProd_Pkp" ,"",200,0,100));
      BookHisto(new TH1F("LABProd_Ppip","",200,0,100));
      BookHisto(new TH1F("LABProd_Ppim","",200,0,100));
      BookHisto(new TH1F("LABProd_Ppos","",200,0,100));
      BookHisto(new TH1F("LABProd_Mmiss","",375,-0.1,0.14375));
      BookHisto(new TH2F("LABProd_Mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

      BookHisto(new TH1F("LABProd_th_pipi","",500,-0.1,0.1));
      BookHisto(new TH1F("LABProd_th_pik","" ,500,-0.1,0.1));
      BookHisto(new TH1F("LABProd_th_ek",""  ,500,-0.1,0.1));
      BookHisto(new TH1F("LABProd_th_enus","",500,-0.1,0.1));


      BookHisto(new TH1F("CMProd_Pkp" ,"",250,0, 500));
      BookHisto(new TH1F("CMProd_Ppip","",250,0, 500));
      BookHisto(new TH1F("CMProd_Ppim","",250,0, 500));
      BookHisto(new TH1F("CMProd_Ppos","",250,0, 500));
      BookHisto(new TH1F("CMProd_Mmiss","",375,-0.1,0.14375));
      BookHisto(new TH2F("CMProd_Mmiss_vs_p","",250,0,500,375,-0.1,0.14375));

      BookHisto(new TH1F("CMProd_th_pipi","",500,-0.1,0.1));
      BookHisto(new TH1F("CMProd_th_pik","" ,500,-0.1,0.1));
      BookHisto(new TH1F("CMProd_th_ek",""  ,500,-0.1,0.1));
      BookHisto(new TH1F("CMProd_th_enus","",500,-0.1,0.1));

      BookHisto(new TH1F("Prod_Menu","",500,0, 0.5));
      BookHisto(new TH1F("Prod_M2pi","",500,0, 0.5));
      BookHisto(new TH1F("Prod_costh_pi","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_pim","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_e","",100,-1, 1));
      BookHisto(new TH1F("Prod_costh_nu","",100,-1, 1));

      BookHisto(new TH1F("Prod_costh_2pi","",100,-1,1));
      BookHisto(new TH1F("Prod_costh_enu","",100,-1, 1));

      BookHisto(new TH1F("Prod_phi","",126,-3.14, 3.14));
      BookHisto(new TH1F("Prod_phi_1","",126,-3.14, 3.14));
      BookHisto(new TH1F("Prod_zvtx","",300,0, 300000));
  }

}

void Ke4MCTest::DefineMCSimple(){
  if(GetWithMC()){

    // kID = fMCSimple.AddParticle(0, 321);//kaon
    int kID = fMCSimple.AddParticle(0, 321);//kaon
    fMCSimple.AddParticle(kID, 211);//piplus
    fMCSimple.AddParticle(kID, -211);//piminus
    fMCSimple.AddParticle(kID, -11);//eplus
    //fMCSimple.AddParticle(kID, 22);//gamma from kaon
    //pi0ID = fMCSimple.AddParticle(kID, 111);//pi0
    fMCSimple.AddParticle(kID, 22);//g1
    fMCSimple.AddParticle(kID, 22);//g2
  }

}

void Ke4MCTest::StartOfRunUser(){
	/// \MemberDescr
	/// This method is called at the beginning of the processing (corresponding to a start of run in the normal NA62 data taking)\n
	/// Do here your start of run processing if any
	/// \EndMemberDescr
}

void Ke4MCTest::StartOfBurstUser(){
	/// \MemberDescr
	/// This method is called when a new file is opened in the ROOT TChain (corresponding to a start/end of burst in the normal NA62 data taking) + at the beginning of the first file\n
	/// Do here your start/end of burst processing if any
	/// \EndMemberDescr
}

void Ke4MCTest::ProcessSpecialTriggerUser(int iEvent, unsigned int triggerType){
	/// \MemberDescr
	/// \param iEvent : Special event number
	/// \param triggerType : Special trigger type (-1 if not known). For this
	/// variable to be filled, RequestL0SpecialTrigger must be called in the constructor.
	///
	/// Process method for special triggers. Called on each special trigger event after each start of burst.\n
	/// \EndMemberDescr
}

void Ke4MCTest::Process(int iEvent){


  //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}

  //TMCEvent *MCEvent = (TMCEvent*)GetEvent("MC");

  if(GetWithMC()){
      bool withMC = true;
      OutputState state;

      if(fMCSimple.fStatus == MCSimple::kEmpty) withMC = false;

       TIter partit(GetMCEvent()->GetKineParts()) ;
       bool g1=false;
       bool g2=false;

       TLorentzVector pk  ;
       TLorentzVector pe  ;
       TLorentzVector pnu  ;
       TLorentzVector ppip;
       TLorentzVector ppim;

       bool pi01=false;
       cout << "Event start <<<<<<<<<<<<<<<<<<<" << endl;
       while(partit.Next()){
         std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;

      if(((KinePart*)(*partit))->GetParticleName().EqualTo("kaon+") ){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        fKplus  = ((KinePart*)(*partit));
        pk = fKplus->GetInitial4Momentum();
      }

      //Ke400
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("pi0") ){

        if(!pi01){

          //std::cout << "1st " <<  ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
          fPiplus  = ((KinePart*)(*partit));
          ppip = fPiplus->GetInitial4Momentum();
          pi01 = true;
        } else {
          //std::cout << "2nd " <<  ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
          fPiminus = ((KinePart*)(*partit));
          ppim = fPiminus->GetInitial4Momentum();
        }

      }

      //Ke4
      if(((KinePart*)(*partit))->GetParticleName().EqualTo("pi+") ){
        //std::cout << "1st " <<  ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
          fPiplus  = ((KinePart*)(*partit));
          ppip = fPiplus->GetInitial4Momentum();
      }
       if(((KinePart*)(*partit))->GetParticleName().EqualTo("pi-")){
         //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
         fPiminus = ((KinePart*)(*partit));
         ppim = fPiminus->GetInitial4Momentum();
      }

      if(((KinePart*)(*partit))->GetParticleName().EqualTo("nu_e") ){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        fNu  = ((KinePart*)(*partit));
        pnu = fNu->GetInitial4Momentum();
      }
            if(((KinePart*)(*partit))->GetParticleName().EqualTo("e+")){
        //std::cout << ((KinePart*)(*partit))->GetParticleName().Data()<< std::endl;
        fEplus = ((KinePart*)(*partit));

        pe = fEplus->GetInitial4Momentum();
      }
    }

       TLorentzVector p2pi = ppip + ppim;
       TLorentzVector penu = pe + pnu;

       TVector3 pkvec      = pk.Vect();
       TVector3 ppipvec    = ppip.Vect();
       TVector3 ppimvec    = ppim.Vect();
       TVector3 p2pivec    = p2pi.Vect();
       TVector3 pevec      = pe.Vect();
       TVector3 pnuvec     = pnu.Vect();
       TVector3 penuvec    = penu.Vect();

       //Lab frame
       FillHisto("LABProd_Pkp" ,1e-3*pk.P());
       FillHisto("LABProd_Ppip",1e-3*ppip.P());
       FillHisto("LABProd_Ppim",1e-3*ppim.P());
       FillHisto("LABProd_Ppos",1e-3*pe.P());
       FillHisto("LABProd_Mmiss",1e-6*(pk-ppip).M2());
       FillHisto("LABProd_Mmiss_vs_p",1e-3*ppip.P(),1e-6*(pk-ppip).M2());

       double costhpik  = ppipvec.Angle(pkvec);
       double costhek   = pevec.Angle(pkvec);
       double costhpipi = ppipvec.Angle(ppimvec);
       double costhenus = pevec.Angle(pnuvec);

       FillHisto("LABProd_th_pipi",costhpipi);
       FillHisto("LABProd_th_pik",costhpik);
       FillHisto("LABProd_th_ek",costhek);
       FillHisto("LABProd_th_enus",costhenus);

       pe.Boost(-pk.BoostVector());
       pnu.Boost(-pk.BoostVector());
       ppim.Boost(-pk.BoostVector());
       ppip.Boost(-pk.BoostVector());
       pk.Boost(-pk.BoostVector());

       costhpik  = ppipvec.Angle(pkvec);
       costhek   = pevec.Angle(pkvec);
       costhpipi = ppipvec.Angle(ppimvec);
       costhenus = pevec.Angle(pnuvec);

       //Lab frame
       FillHisto("CMProd_Pkp" ,pk.P());
       FillHisto("CMProd_Ppip",ppip.P());
       FillHisto("CMProd_Ppim",ppim.P());
       FillHisto("CMProd_Ppos",pe.P());
       FillHisto("CMProd_Mmiss",1e-6*(pk-ppip).M2());
       FillHisto("CMProd_Mmiss_vs_p",ppip.P(),1e-6*(pk-ppip).M2());


       FillHisto("CMProd_th_pipi",costhpipi);
       FillHisto("CMProd_th_pik",costhpik);
       FillHisto("CMProd_th_ek",costhek);
       FillHisto("CMProd_th_enus",costhenus);

       penu = pe + pnu;
       p2pi = ppip + ppim;

       pe.Boost  (-penu.BoostVector());
       pnu.Boost (-penu.BoostVector());
       ppip.Boost(-p2pi.BoostVector());
       ppim.Boost(-p2pi.BoostVector());

       pkvec      = pk.Vect();
       ppipvec    = ppip.Vect();
       ppimvec    = ppim.Vect();
       p2pivec    = p2pi.Vect();
       pevec      = pe.Vect();
       pnuvec     = pnu.Vect();
       penuvec    = penu.Vect();

       // std::cout << "costhpi CM= " << TMath::Cos((ppipvec.Mag()*p2pivec.Mag())/ppipvec.Dot(p2pivec)) << std::endl;
       // std::cout << "costhe  CM= " << TMath::Cos((pevec.Mag()*penuvec.Mag())/pevec.Dot(penuvec)) << std::endl;

       // std::cout << "phi  CM= " << (p2pivec.Mag()*penuvec.Mag())/p2pivec.Dot(penuvec) << std::endl;

       double costhpi   = TMath::Cos(ppipvec.Angle(p2pivec));
       double costhpim  = TMath::Cos(ppimvec.Angle(p2pivec));
       double costhe    = TMath::Cos(pevec.Angle(penuvec));
       double costhnu  = TMath::Cos(pnuvec.Angle(penuvec));

       double costhenu  = TMath::Cos(penuvec.Angle(pkvec));
       double costh2pi  = TMath::Cos(p2pivec.Angle(pkvec));

       TVector3 norm_2pi = ppipvec.Unit().Cross(ppimvec.Unit());
       TVector3 norm_enu = pnuvec.Unit().Cross(pevec.Unit());
       TVector3 norm_2pi_1 = ppipvec.Orthogonal();
       TVector3 norm_enu_1 = pevec.Orthogonal();

       // double phi = pe.Angle(ppipvec);
       // double phi = norm_2pi.Angle(norm_enu);
       // double phi = (norm_2pi+norm_enu).Phi();
       //double phi = norm_2pi.Phi()-norm_enu.Phi();
       //double phi_1 = norm_2pi_1.Phi()-norm_enu_1.Phi();
       double phi   = norm_2pi.Angle(norm_enu);
       double phi_1 = norm_2pi_1.Angle(norm_enu_1);
       // std::cout << "costh2pi CM= " << costh2pi << std::endl;

       FillHisto("Prod_M2pi",1e-6*p2pi.M2());
       FillHisto("Prod_Menu",1e-6*penu.M2());

       FillHisto("Prod_costh_pi",costhpi);
       FillHisto("Prod_costh_pim",costhpim);
       FillHisto("Prod_costh_e",costhe);
       FillHisto("Prod_costh_nu",costhnu);
       FillHisto("Prod_phi",phi);
       FillHisto("Prod_phi_1",phi_1);

       FillHisto("Prod_costh_2pi",costh2pi);
       FillHisto("Prod_costh_enu",costhenu);


       FillHisto("Prod_zvtx",fKplus->GetEndPos().Z());
       if( fKplus->GetEndPos().Z() < 105000 || fKplus->GetEndPos().Z() > 165000 ) return;



  }

}

void Ke4MCTest::PostProcess(){
	/// \MemberDescr
	/// This function is called after an event has been processed by all analyzers. It could be used to free some memory allocated
	/// during the Process.
	/// \EndMemberDescr

}

void Ke4MCTest::EndOfBurstUser(){
	/// \MemberDescr
	/// This method is called when a new file is opened in the ROOT TChain (corresponding to a start/end of burst in the normal NA62 data taking) + at the end of the last file\n
	/// Do here your start/end of burst processing if any.
	/// Be careful: this is called after the event/file has changed.
	/// \EndMemberDescr
}

void Ke4MCTest::EndOfRunUser(){
	/// \MemberDescr
	/// This method is called at the end of the processing (corresponding to a end of run in the normal NA62 data taking)\n
	/// Do here your end of run processing if any\n
	/// \EndMemberDescr

}

void Ke4MCTest::EndOfJobUser(){
    /// \MemberDescr
    /// Called at the end of job, just before exiting NA62Analysis.\n
    /// Do here all output operation needed (write plots, objects)
    /// \n
	/// If you want to save all plots, just call\n
	/// \code
  SaveAllPlots();
	/// \endcode
	/// Or you can just save the ones you want with\n
	/// \code
	/// 	histogram->Write()\n
	///		fHisto.Get...("histoname")->Write();
	/// \endcode
	/// \n
	/// To run over a set of histograms you can use Iterators (HistoHandler::IteratorTH1,
	/// HistoHandler::IteratorTH2, HistoHandler::IteratorTGraph). You can use it to run over
	/// all the histograms or only a subset of histogram by using one of the two forms of
	/// GetIterator...  (replace ... by TH1, TH2 or TGraph)\n
	/// \code
	/// 	GetIterator...()
	/// \endcode
	/// will get an Iterator running over all histograms of this type while
	/// \code
	/// 	GetIterator...("baseName")
	/// \endcode
	/// will get an Iterator running only over the histograms of this type whose name starts
	/// with baseName.\n
	/// For more details and examples on how to use the Iterator after getting it, please refer
	/// to the HistoHandler::Iterator documentation.\n
	/// Although this is described here, Iterators can be used anywhere after the
	/// histograms have been booked.
    /// \EndMemberDescr
}

void Ke4MCTest::DrawPlot(){
	/// \MemberDescr
	/// This method is called at the end of processing to draw plots when the -g option is used.\n
	/// If you want to draw all the plots, just call\n
	/// \code
	/// 	DrawAllPlots();
	/// \endcode
	/// Or get the pointer to the histogram with\n
	/// \code
	/// 	fHisto.GetTH1("histoName");// for TH1
	/// 	fHisto.GetTH2("histoName");// for TH2
	/// 	fHisto.GetGraph("graphName");// for TGraph and TGraphAsymmErrors
	///     fHisto.GetHisto("histoName");// for TH1 or TH2 (returns a TH1 pointer)
	/// \endcode
	/// and manipulate it as usual (TCanvas, Draw, ...)\n
	/// \EndMemberDescr
}

Ke4MCTest::~Ke4MCTest(){
	/// \MemberDescr
	/// Destructor of the Analyzer. If you allocated any memory for class
	/// members, delete them here.
	/// \EndMemberDescr
}
