#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "KSAnalysis.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "SpectrometerNewCHODAssociation.hh"
#include "SpectrometerMUV3Association.hh"
#include "RICHParameters.hh"
#include "RICHImprovedRing.hh"
#include "Constants.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"
#include "Geometry.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

KSAnalysis::KSAnalysis(Core::BaseAnalysis *ba) : Analyzer(ba, "KSAnalysis")
{

    RequestTree("LKr",new TRecoLKrEvent);
    RequestTree("Spectrometer",new TRecoSpectrometerEvent);
    RequestTree("CHOD",new TRecoCHODEvent);
    RequestTree("NewCHOD",new TRecoNewCHODEvent);
    RequestTree("RICH",new TRecoRICHEvent);
    RequestTree("MUV3",new TRecoMUV3Event);
    RequestTree("MUV1",new TRecoMUV1Event);
    RequestTree("MUV2",new TRecoMUV2Event);
    RequestTree("LAV",new TRecoLAVEvent);
    RequestTree("CHANTI",new TRecoCHANTIEvent);


    // fCHODAnal = new CHODAnal(10,ba);
    // fLKrAnal = new LKrAnal(10,ba);
    // fStrawRapper = new StrawRapper(0,ba,fCHODAnal,fLKrAnal);
    fTool = AnalysisTools::GetInstance();

    fMaxNTracks = 50;
    fMaxNVertices = 50;

    //fRICHAnal = new RICHAnalysis(0,ba);
    AddParam("MaxChi2",    &fMaxChi2,       100.0);
    AddParam("MinZvertex", &fMinZVertex,  50000.0); // [mm]
    AddParam("MaxZvertex", &fMaxZVertex, 180000.0); // [mm]

}

void KSAnalysis::InitOutput(){
}

void KSAnalysis::InitHist(){


    if(GetWithMC()){

        BookHisto(new TH1F("Prod_zvtx","",300,0, 300000));
        BookHisto(new TH1F("Prod_Pks" ,"",200,0, 100));
        BookHisto(new TH2F("Prod_Pi_pl_vs_min_mom" ,"",200,0, 100,200,0,100));
        BookHisto(new TH1F("Prod_M2pi","",500,0, 0.5));

        BookHisto(new TH2F("TrackPlus_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackPlus_pcut_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackPlus_kin_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackPlus_full_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackPlus_full_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackPlus_inacc_full_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackPlus_outacc_full_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

        BookHisto(new TH2F("TrackMinus_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackMinus_pcut_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackMinus_kin_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackMinus_full_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackMinus_full_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackMinus_inacc_full_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

        BookHisto(new TH2F("TrackMinus_outacc_full_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));

        BookHisto(new TH2F("TrackPlus_inacc_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackPlus_inacc_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackMinus_inacc_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackMinus_inacc_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));


        BookHisto(new TH2F("TrackPlus_outacc_final_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackPlus_outacc_final_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackMinus_outacc_final_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackMinus_outacc_final_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));

        BookHisto(new TH2F("TrackPlus_inacc_final_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackPlus_inacc_final_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));
        BookHisto(new TH2F("TrackMinus_inacc_final_rstraw1_vs_zvtx","",250,50000,200000,120,0,1200));
        BookHisto(new TH2F("TrackMinus_inacc_final_x_y_straw1","",2400,-1200,1200,2400,-1200,1200));

    }
    BookHisto(new TH1I("Straw_NCand","",30,0,30));
    BookHisto(new TH2I("Straw_Q_vs_NChambers","",5,0,5,4,-2,2));
    BookHisto(new TH2F("Straw_Q_vs_Chi2","",200,0,50,4,-2,2));


    BookHisto(new TH1F("TrackPlus_mom","",200,0,100));
    BookHisto(new TH2F("TrackPlus_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH1F("TrackMinus_mom","",200,0,100));
    BookHisto(new TH2F("TrackMinus_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH1I("OT_LKr_NCand","",50,0,50));
    BookHisto(new TH1I("TT_LKr_NCand","",50,0,50));

    TString type[2] = {"K3pi2","KS"};

    BookHisto(new TH1I(Form("%s_charge_sum",type[1].Data()),"",6,-3,3));
    BookHisto(new TH1I(Form("%s_diffcharge",type[1].Data()),"",6,-3,3));

    //KSPlots
    for(int i(0);i<2;i++){
        BookHisto(new TH1F(Form("%sTrack%d_chodchi2",type[1].Data(),i),"",200,0,50));
        BookHisto(new TH2F(Form("%sTrack%d_nochod",type[1].Data(),i),"",300,-1500,1500,300,-1500,1500));
        BookHisto(new TH2F(Form("%sTrack%d_chodchi2_rejected",type[1].Data(),i),"",300,-1500,1500,300,-1500,1500));
        BookHisto(new TH2F(Form("%sTrack%d_dist_vs_mintime",type[1].Data(),i),"",200,-100,100,50,0,250));
        BookHisto(new TH2F(Form("%sTrack%d_dy_vs_dx",type[1].Data(),i),"",200,-500,500,200,-500,500));
        BookHisto(new TH1F(Form("%sTrack%d_chod_ptrack",type[1].Data(),i),"",200,0,100));
    }


    BookHisto(new TH1F(Form("%s_IsCHOD_p",type[1].Data()),"",200,0,100));
    BookHisto(new TH1F(Form("%s_IsCHOD_dt",type[1].Data()),"",800,-100,100));
    BookHisto(new TH1F(Form("%sPl_IsCHOD_p",type[1].Data()),"",200,0,100));
    BookHisto(new TH1F(Form("%sMin_IsCHOD_p",type[1].Data()),"",200,0,100));

    BookHisto(new TH2F(Form("%s_IsGoodVtx_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_IsKSMass_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsKSMass_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsKSMass_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsKSMass_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsKSMass_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsKSMass_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));

    BookHisto(new TH2F(Form("%s_IsKSMass_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsKSMass_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH1F(Form("%s_dt12_cedar",type[1].Data()),"",1000,-50,50));
    BookHisto(new TH2F(Form("%s_chanti_dt_nhits_all",type[1].Data()),"",1000,-50, 50,50,0,50));
    BookHisto(new TH2F(Form("%s_chanti_dt_nhits_xy",type[1].Data()),"",1000,-50, 50,50,0,50));
    BookHisto(new TH1F(Form("%s_chanti_dt_closest",type[1].Data()),"",1000,-50, 50));

    BookHisto(new TH2F(Form("%s_final_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_final_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_final_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_final_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_final_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_final_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_final_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_final_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_final_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_final_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_NoCHANTI_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_NoCHANTI_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_NoCHANTI_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoCHANTI_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoCHANTI_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoCHANTI_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_NoCHANTI_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoCHANTI_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_CHANTI_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_CHANTI_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_CHANTI_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_CHANTI_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_CHANTI_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_CHANTI_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_CHANTI_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_CHANTI_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_NoSnakes_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_NoSnakes_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_NoSnakes_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoSnakes_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoSnakes_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoSnakes_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_NoSnakes_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoSnakes_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));

    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


}

void KSAnalysis::DefineMCSimple(){
    if(GetWithMC()){

        // kID = fMCSimple.AddParticle(0, 321);//kaon
        int kID = fMCSimple.AddParticle(0, 310);//k0s
        fMCSimple.AddParticle(kID, 211);//piplus
        //fMCSimple.AddParticle(kID, 22);//gamma from kaon
        //pi0ID = fMCSimple.AddParticle(kID, 111);//pi0
        fMCSimple.AddParticle(kID, -211);//piminus
        //fMCSimple.AddParticle(kID, -11);//eplus
        //fMCSimple.AddParticle(kID, 22);//g1
        //fMCSimple.AddParticle(kID, 22);//g2
    }
}

void KSAnalysis::StartOfRunUser(){
}

void KSAnalysis::StartOfBurstUser(){
}

void KSAnalysis::ProcessSpecialTriggerUser(int iEvent, unsigned int triggerType){
}

void KSAnalysis::Process(int iEvent){
    //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
    //if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}

    fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
    fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
    fCHODEvent = (TRecoCHODEvent*)GetEvent("CHOD");
    fNewCHODEvent = (TRecoNewCHODEvent*)GetEvent("NewCHOD");
    fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");
    fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
    fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
    fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
    fCHANTIEvent      = (TRecoCHANTIEvent*)GetEvent("CHANTI");

    fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
    fStrawCandidate   = (StrawCandidate*)GetOutput("TrackProcessor.StrawCandidate");
    fRICHCandidate    = (RICHCandidate*)GetOutput("TrackProcessor.RICHCandidate");
    fCHODCandidate    = (CHODCandidate*)GetOutput("TrackProcessor.CHODCandidate");
    fNewCHODCandidate = (NewCHODCandidate*)GetOutput("TrackProcessor.NewCHODCandidate");
    fLKrClusCandidate = (LKrCandidate*)GetOutput("TrackProcessor.LKrCandidate");
    fLKrCellCandidate = (LKrCandidate*)GetOutput("TrackProcessor.LKrCellCandidate");
    fMUV1Candidate    = (MUVCandidate*)GetOutput("TrackProcessor.MUV1Candidate");
    fMUV2Candidate    = (MUVCandidate*)GetOutput("TrackProcessor.MUV2Candidate");
    fMUV3Candidate    = *(std::vector<SpectrometerMUV3AssociationOutput>*)GetOutput("SpectrometerMUV3Association.Output");

    fmevtogev=1e-3;
    xSTRAW_station[0] = 101.2;
    xSTRAW_station[1] = 114.4;
    xSTRAW_station[2] = 92.4;
    xSTRAW_station[3] = 52.8; // [ mm ]

    //Using the true information
    if(GetWithMC()){

        Int_t IsPiPi = TrueInfoAnalysis(0);
        if(IsPiPi==0) return;

        fPiPlusXAtStraw1 = fPiplus->GetPosAtCheckPoint(4).X();
        fPiPlusYAtStraw1 = fPiplus->GetPosAtCheckPoint(4).Y();

        fPiMinusXAtStraw1 = fPiminus->GetPosAtCheckPoint(4).X();
        fPiMinusYAtStraw1 = fPiminus->GetPosAtCheckPoint(4).Y();

        double rplus = sqrt( (fPiPlusXAtStraw1 - xSTRAW_station[0])*(fPiPlusXAtStraw1 - xSTRAW_station[0]) + fPiPlusYAtStraw1*fPiPlusYAtStraw1 );
        double rminus = sqrt( (fPiMinusXAtStraw1 - xSTRAW_station[0])*(fPiMinusXAtStraw1 - xSTRAW_station[0]) +  fPiMinusYAtStraw1*fPiMinusYAtStraw1);



        //Anti-snakes zvtx vs fRAtStraw1 cut
        double b1 = -0.004;
        double a1 = 315-b1*105000.;
        double r115 = a1+b1*115000.;
        double b2 = -(900-r115)/10000.;
        double a2 = 900-b2*105000.;
        double b3 = -0.00983333333;
        double a3 = 780-b3*105000.;
        double cut1 = a1+b1*fTrueVtx.Z();
        double cut2 = a2+b2*fTrueVtx.Z();
        double cut3 = a3+b3*fTrueVtx.Z();
        bool isSignalPlus = rplus>cut1 && rplus>cut2 && rplus<cut3 && fTrueVtx.Z()<165000.;
        bool isSignalMinus = rminus>cut1 && rminus>cut2 && rminus<cut3 && fTrueVtx.Z()<165000.;

        FillHisto("TrackPlus_x_y_straw1", fPiPlusXAtStraw1, fPiPlusYAtStraw1);
        FillHisto("TrackMinus_x_y_straw1", fPiMinusXAtStraw1, fPiMinusYAtStraw1);

        //In acceptance
        if(rplus > 90 && rplus < 1100 && rminus > 90 && rminus < 1100){
            FillHisto("TrackPlus_inacc_x_y_straw1", fPiPlusXAtStraw1, fPiPlusYAtStraw1);
            FillHisto("TrackPlus_inacc_rstraw1_vs_zvtx", fTrueVtx.Z(), rplus);
            FillHisto("TrackMinus_inacc_x_y_straw1",fPiMinusXAtStraw1, fPiMinusYAtStraw1);
            FillHisto("TrackMinus_inacc_rstraw1_vs_zvtx", fTrueVtx.Z(), rminus);

            if(!isSignalPlus){
                FillHisto("TrackPlus_outacc_final_x_y_straw1", fPiPlusXAtStraw1, fPiPlusYAtStraw1);
                FillHisto("TrackPlus_outacc_final_rstraw1_vs_zvtx", fTrueVtx.Z(), rplus);

            } else {
                FillHisto("TrackPlus_inacc_final_x_y_straw1", fPiPlusXAtStraw1, fPiPlusYAtStraw1);
                FillHisto("TrackPlus_inacc_final_rstraw1_vs_zvtx", fTrueVtx.Z(), rplus);
            }

            if(!isSignalMinus){
                FillHisto("TrackMinus_outacc_final_x_y_straw1",fPiMinusXAtStraw1, fPiMinusYAtStraw1);
                FillHisto("TrackMinus_outacc_final_rstraw1_vs_zvtx", fTrueVtx.Z(), rminus);
            } else {
                FillHisto("TrackMinus_inacc_final_x_y_straw1",fPiMinusXAtStraw1, fPiMinusYAtStraw1);
                FillHisto("TrackMinus_inacc_final_rstraw1_vs_zvtx", fTrueVtx.Z(), rminus);
            }
        }


        //Pi minus in acceptance
        if(fmevtogev*fTruePiMinus.P() < 35 && fmevtogev*fTruePiMinus.P() > 15 ){
            FillHisto("TrackMinus_pcut_x_y_straw1",fPiMinusXAtStraw1, fPiMinusYAtStraw1);

            if(rminus > 90 && rminus < 1100){
                FillHisto("TrackMinus_kin_x_y_straw1", fPiMinusXAtStraw1, fPiMinusYAtStraw1);
                if(rplus < 90 || rplus > 1100){
                    FillHisto("TrackMinus_full_x_y_straw1", fPiMinusXAtStraw1, fPiMinusYAtStraw1);
                    FillHisto("TrackMinus_full_rstraw1_vs_zvtx", fTrueVtx.Z(), rminus);
                    if(!isSignalMinus){
                        FillHisto("TrackMinus_outacc_full_rstraw1_vs_zvtx", fTrueVtx.Z(), rminus);
                    } else {
                        FillHisto("TrackMinus_inacc_full_rstraw1_vs_zvtx", fTrueVtx.Z(), rminus);
                    }

                }
            }
        }

        //Pi plus in acceptance
        if(fmevtogev*fTruePiPlus.P() < 35 && fmevtogev*fTruePiPlus.P() > 15 ){
            FillHisto("TrackPlus_pcut_x_y_straw1", fPiPlusXAtStraw1, fPiPlusYAtStraw1);
            if(rplus > 90 && rplus < 1100){
                FillHisto("TrackPlus_kin_x_y_straw1", fPiPlusXAtStraw1, fPiPlusYAtStraw1);
                if(rminus < 90 || rminus > 1100){
                    FillHisto("TrackPlus_full_x_y_straw1", fPiPlusXAtStraw1, fPiPlusYAtStraw1);
                    FillHisto("TrackPlus_full_rstraw1_vs_zvtx", fTrueVtx.Z(), rplus);

                    if(!isSignalPlus){
                        FillHisto("TrackPlus_outacc_full_rstraw1_vs_zvtx", fTrueVtx.Z(), rplus);
                    } else {
                        FillHisto("TrackPlus_inacc_full_rstraw1_vs_zvtx", fTrueVtx.Z(), rplus);
                    }
                }
            }
        }

    }

    fNTracks = fSpectrometerEvent->GetNCandidates();

    NominalKaon();
    Bool_t isKS = SelectKS();

    Bool_t isPreselectKS = PreselectTwoTracks(0);
    Bool_t isOneTrack = OneTrackAnalysis(0);
    Bool_t isTwoTrack = TwoTrackAnalysis(0);

}
Bool_t KSAnalysis::SelectKS(){

    if(fNTracks!=2) return 0;

    fKSSpecCand[0] = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(0);
    fKSSpecCand[1] = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(1);
    std::pair<TLorentzVector,TLorentzVector> KSpions = fTool->GetTwoPiPlusMomentum(fKSSpecCand[0],fKSSpecCand[1]);
    int q1 = fKSSpecCand[0]->GetCharge();
    int q2 = fKSSpecCand[1]->GetCharge();
    int ipl= -1;
    int imin= -1;
    FillHisto("KS_charge_sum",q1+q2);
    if(q1==q2) return false;
    FillHisto("KS_diffcharge",q1+q2);

    //Select two tracks with chod matching
    Int_t good1 = GoodCHOD(2,0,2);
    Int_t good2 = GoodCHOD(2,1,2);

    if(good1==1) FillHisto("KSTrack0_chod_ptrack",KSpions.first.P());
    if(good2==1) FillHisto("KSTrack1_chod_ptrack",KSpions.second.P());
    if(good1!=1 || good2!=1 ) return false;

    if(q1==1 && q2==-1){
        ipl=0;
        imin = 1;
        fKSPiPlusMom.SetXYZM(KSpions.first.Px(), KSpions.first.Py(), KSpions.first.Pz(),0.13957);
        fKSPiMinusMom.SetXYZM(KSpions.second.Px(), KSpions.second.Py(), KSpions.second.Pz(),0.13957);
    }
    if(q1==-1 && q2==1){
        ipl=1;
        imin = 0;
        fKSPiMinusMom.SetXYZM(KSpions.first.Px(), KSpions.first.Py(), KSpions.first.Pz(),0.13957);
        fKSPiPlusMom.SetXYZM(KSpions.second.Px(), KSpions.second.Py(), KSpions.second.Pz(),0.13957);
    }

    fXAtStraw1=fYAtStraw1=fRAtStraw1=0;
    fXAtStraw1_Min=fYAtStraw1_Min=fRAtStraw1_Min=0;

    fXAtStraw1 = fKSSpecCand[ipl]->xAt(zSTRAW_station[0]);
    fYAtStraw1 = fKSSpecCand[ipl]->yAt(zSTRAW_station[0]);
    fRAtStraw1 = TMath::Sqrt((fXAtStraw1 - xSTRAW_station[0])*(fXAtStraw1 - xSTRAW_station[0])+fYAtStraw1*fYAtStraw1);

    fXAtStraw1_Min = fKSSpecCand[imin]->xAt(zSTRAW_station[0]);
    fYAtStraw1_Min = fKSSpecCand[imin]->yAt(zSTRAW_station[0]);
    fRAtStraw1_Min = TMath::Sqrt((fXAtStraw1_Min - xSTRAW_station[0])*(fXAtStraw1_Min - xSTRAW_station[0])+fYAtStraw1_Min*fYAtStraw1_Min);

    fKS4Mom = fKSPiMinusMom + fKSPiPlusMom;
    fKSMMiss = (fKaonNominalMomentum - fKS4Mom).M2();
    double min_mm2 = (fKaonNominalMomentum - fKSPiMinusMom).M2();
    double plus_mm2 = (fKaonNominalMomentum - fKSPiPlusMom).M2();
    float choddt = (fCHODCandidate[ipl].GetDeltaTime()+fKSSpecCand[ipl]->GetTime()) - (fCHODCandidate[imin].GetDeltaTime() + fKSSpecCand[imin]->GetTime() );
    FillHisto("KS_IsCHOD_p",fKS4Mom.P());
    FillHisto("KS_IsCHOD_dt",choddt);
    FillHisto("KSMin_IsCHOD_p",fKSPiMinusMom.P());
    FillHisto("KSPl_IsCHOD_p",fKSPiPlusMom.P());

    if(fabs(choddt) > 2) return false;

    TVector3 pos[2];

    pos[0]=fKSSpecCand[0]->GetPositionBeforeMagnet();
    pos[1]=fKSSpecCand[1]->GetPositionBeforeMagnet();

    fKScda= 999999;
    TLorentzVector pforvtx[2];
    pforvtx[0]=KSpions.first;
    pforvtx[1]=KSpions.second;
    fKSTwoTrackVertex = fTool->MultiTrackVertexSimple(2,pforvtx,pos,&fKScda);

    if(fKSPiPlusMom.P() < 10 || fKSPiPlusMom.P() > 90 ) return false;
    if(fKSPiMinusMom.P() < 10 || fKSPiMinusMom.P() > 90 ) return false;
    if(fKSTwoTrackVertex.Z() < 103000 || fKSTwoTrackVertex.Z() > 130000) return false;
    if(fKScda > 10) return false;

    FillHisto("KS_IsGoodVtx_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_IsGoodVtx_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_IsGoodVtx_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_IsGoodVtx_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_IsGoodVtx_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_IsGoodVtx_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_IsGoodVtx_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_IsGoodVtx_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_IsGoodVtx_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsGoodVtx_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsGoodVtx_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_IsGoodVtx_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_IsGoodVtx_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_IsGoodVtx_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    if(fKSMMiss > 0.005) return false;
    if(fKS4Mom.P() > 76 ) return false;
    if( fabs(fKS4Mom.M() - 0.497611) > 0.0105) return false;

    FillHisto("KS_IsKSMass_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_IsKSMass_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_IsKSMass_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_IsKSMass_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_IsKSMass_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_IsKSMass_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_IsKSMass_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_IsKSMass_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_IsKSMass_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsKSMass_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsKSMass_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_IsKSMass_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_IsKSMass_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_IsKSMass_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    if(plus_mm2 > 0.068 || plus_mm2 < -0.01) return false;
    if(min_mm2 > 0.068 || min_mm2 < -0.01) return false;
    if(fKSMMiss < -0.05) return false;

    FillHisto("KS_final_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_final_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_final_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_final_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_final_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_final_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_final_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_final_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_final_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_final_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_final_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_final_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_final_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_final_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    double chodtpl = (fCHODCandidate[ipl].GetDeltaTime()+fKSSpecCand[ipl]->GetTime());
    std::map<double, std::pair<double,int>> dtch;

    for(int i(0); i<fCHANTIEvent->GetNCandidates();i++){
        TRecoCHANTICandidate *cand = (TRecoCHANTICandidate *)fCHANTIEvent->GetCandidate(i);
        Double_t dt_chod = cand->GetTime()-chodtpl;
        FillHisto("KS_chanti_dt_nhits_all",dt_chod,cand->GetNHits());
        if (!cand->GetXYMult()) continue; // At least a XY coincidence
        FillHisto("KS_chanti_dt_nhits_xy",dt_chod,cand->GetNHits());

        dtch.insert(std::make_pair(fabs(dt_chod),std::pair<double,int>(cand->GetTime(),i)));

    }

    bool isCHANTI = false;
    if(dtch.size()>0){

        double iminchod  = dtch.begin()->second.second;
        double mindtchod = dtch.begin()->first;
        if(mindtchod < 3){
            FillHisto("KS_chanti_dt_closest",mindtchod);
            isCHANTI= true;
        }
    }

    if(isCHANTI){

        FillHisto("KS_CHANTI_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
        FillHisto("KS_CHANTI_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
        FillHisto("KS_CHANTI_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
        FillHisto("KS_CHANTI_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
        FillHisto("KS_CHANTI_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
        FillHisto("KS_CHANTI_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
        FillHisto("KS_CHANTI_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
        FillHisto("KS_CHANTI_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
        FillHisto("KS_CHANTI_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
        FillHisto("KS_CHANTI_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
        FillHisto("KS_CHANTI_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("KS_CHANTI_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
        FillHisto("KS_CHANTI_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
        FillHisto("KS_CHANTI_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

        return false;
    }

    FillHisto("KS_NoCHANTI_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_NoCHANTI_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_NoCHANTI_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_NoCHANTI_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_NoCHANTI_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_NoCHANTI_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_NoCHANTI_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_NoCHANTI_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_NoCHANTI_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoCHANTI_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoCHANTI_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_NoCHANTI_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_NoCHANTI_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_NoCHANTI_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    //Anti-snakes zvtx vs fRAtStraw1 cut
    double b1 = -0.004;
    double a1 = 315-b1*105000.;
    double r115 = a1+b1*115000.;
    double b2 = -(900-r115)/10000.;
    double a2 = 900-b2*105000.;
    double b3 = -0.00983333333;
    double a3 = 780-b3*105000.;
    double cut1 = a1+b1*fKSTwoTrackVertex.Z();
    double cut2 = a2+b2*fKSTwoTrackVertex.Z();
    double cut3 = a3+b3*fKSTwoTrackVertex.Z();
    bool isSignal = fRAtStraw1>cut1 && fRAtStraw1>cut2 && fRAtStraw1<cut3 && fKSTwoTrackVertex.Z()<165000.;
    if(!isSignal) return false;

    FillHisto("KS_NoSnakes_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_NoSnakes_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_NoSnakes_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_NoSnakes_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_NoSnakes_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_NoSnakes_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_NoSnakes_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_NoSnakes_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_NoSnakes_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoSnakes_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoSnakes_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_NoSnakes_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_NoSnakes_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_NoSnakes_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    if(fKSPiMinusMom.P() > 15 && fKSPiMinusMom.P() < 35){

        FillHisto("KS_IsGoodPiMinus_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
        FillHisto("KS_IsGoodPiMinus_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
        FillHisto("KS_IsGoodPiMinus_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
        FillHisto("KS_IsGoodPiMinus_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
        FillHisto("KS_IsGoodPiMinus_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
        FillHisto("KS_IsGoodPiMinus_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
        FillHisto("KS_IsGoodPiMinus_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
        FillHisto("KS_IsGoodPiMinus_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
        FillHisto("KS_IsGoodPiMinus_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiMinus_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiMinus_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("KS_IsGoodPiMinus_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
        FillHisto("KS_IsGoodPiMinus_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
        FillHisto("KS_IsGoodPiMinus_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    }

    if(fKSPiPlusMom.P() > 15 && fKSPiPlusMom.P() < 35){

        FillHisto("KS_IsGoodPiPlus_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
        FillHisto("KS_IsGoodPiPlus_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
        FillHisto("KS_IsGoodPiPlus_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
        FillHisto("KS_IsGoodPiPlus_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
        FillHisto("KS_IsGoodPiPlus_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
        FillHisto("KS_IsGoodPiPlus_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
        FillHisto("KS_IsGoodPiPlus_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
        FillHisto("KS_IsGoodPiPlus_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
        FillHisto("KS_IsGoodPiPlus_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiPlus_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiPlus_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("KS_IsGoodPiPlus_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
        FillHisto("KS_IsGoodPiPlus_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
        FillHisto("KS_IsGoodPiPlus_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    }

    return 1;
}

Bool_t KSAnalysis::OneTrackAnalysis(Int_t flag){

    FillHisto("OT_LKr_NCand",fLKrEvent->GetNCandidates());

    return true;
}
Bool_t KSAnalysis::TwoTrackAnalysis(Int_t flag){
    FillHisto("TT_LKr_NCand",fLKrEvent->GetNCandidates());
    return true;
}

Bool_t KSAnalysis::PreselectTwoTracks(Int_t flag){

    fNGoodTracks = 0;

    FillHisto("Straw_NCand",fSpectrometerEvent->GetNCandidates());


    for(int i(0);i < fSpectrometerEvent->GetNCandidates();i++){
        TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(i);
        TLorentzVector FourP(track->GetThreeMomentumBeforeMagnet(),139.);
        int charge = track->GetCharge();
        int nchambers = track->GetNChambers();
        double chi2 = track->GetChi2();

        FillHisto("Straw_Q_vs_NChambers",nchambers,charge);
        FillHisto("Straw_Q_vs_Chi2",chi2,charge);
        if(nchambers != 4) continue;
        if(charge==1){
            FillHisto("TrackPlus_mom", 1e-3*track->GetMomentum());
            FillHisto("TrackPlus_mmiss_vs_p", 1e-3*track->GetMomentum(),1e-6*(fKS->GetInitial4Momentum()- fPiplus->GetInitial4Momentum()).M2());
        }  else {
            FillHisto("TrackMinus_mom", 1e-3*track->GetMomentum());
            FillHisto("TrackMinus_mmiss_vs_p", 1e-3*track->GetMomentum(),1e-6*(fKS->GetInitial4Momentum()- fPiplus->GetInitial4Momentum()).M2());
        }
    }

    return true;
}
Int_t KSAnalysis::TrueInfoAnalysis(Int_t flag){

    if(flag==0){

        //std::cout << "Event starts  " << iEvent << std::endl;

        TClonesArray* particles = GetMCEvent()->GetKineParts();
        //std::cout << particles->GetEntries() << " particles present  " << iEvent << std::endl;

        fg1= NULL;
        fPiplus = NULL;
        fPiminus = NULL;
        for (int i(0);i<particles->GetEntries();i++){

            KinePart* ipart = (KinePart*)particles->At(i);
            //std::cout << "Particle " << i  << " called = " << ipart->GetParticleName().Data() << " with index = " << ipart->GetParentIndex() << std::endl;
            if(i > 0){
                KinePart* mother = (KinePart*)particles->At(ipart->GetParentIndex());
                //std::cout << "And Mother: " << mother->GetParticleName().Data()<< std::endl;
            }

            if(ipart->GetParticleName().EqualTo("kaon0S") ){
                //std::cout << (ipart)->GetParticleName().Data()<< std::endl;
                fKS  = ipart;
            }
            if(ipart->GetParticleName().EqualTo("pi+") ){
                //std::cout << (ipart)->GetParticleName().Data()<< std::endl;
                fPiplus  = ipart;
            }
            if(ipart->GetParticleName().EqualTo("pi-")){
                //std::cout << (ipart)->GetParticleName().Data()<< std::endl;
                fPiminus = ipart;
            }
            if(ipart->GetParticleName().EqualTo("gamma") ){
                //std::cout << (ipart)->GetParticleName().Data()<< std::endl;
                fg1  = ipart;
            }
            if(ipart->GetParticleName().EqualTo("mu-") ){
                //std::cout << (ipart)->GetParticleName().Data()<< std::endl;
                fMuminus  = ipart;
            }
            if(ipart->GetParticleName().EqualTo("mu+") ){
                //std::cout << (ipart)->GetParticleName().Data()<< std::endl;
                fMuplus  = ipart;
            }

        }

        if(fg1) return 0;
        if(!fPiminus || !fPiplus) return 0;

        fTrueVtx = fKS->GetEndPos().Vect();
        fTrueKS      = fKS->GetInitial4Momentum();
        fTruePiPlus  = fPiplus->GetInitial4Momentum();
        fTruePiMinus = fPiminus->GetInitial4Momentum();
        fTwoPions = fTruePiPlus +fTruePiMinus;

        FillHisto("Prod_zvtx", fTrueVtx.Z());
        FillHisto("Prod_Pks", fmevtogev*fTrueKS.P());
        FillHisto("Prod_Pi_pl_vs_min_mom", fmevtogev*fTruePiPlus.P(),fmevtogev*fTruePiMinus.P());
        FillHisto("Prod_M2pi", fmevtogev*fTwoPions.M());

        if( fmevtogev*fTruePiPlus.P() > 90 || fmevtogev*fTruePiPlus.P() < 5) return 0;
        if( fmevtogev*fTruePiMinus.P() > 90 || fmevtogev*fTruePiMinus.P() < 5) return 0;
    }

    return 1;
}

Int_t KSAnalysis::GoodCHOD(Int_t ntracks, Int_t iC, Int_t flag){

    TRecoSpectrometerCandidate* track;

    TString selection;
    if(flag==1) {
        track = fSPosCand[iC];
        selection="K3pi2";

    }
    if(flag==2){
        track = fKSSpecCand[iC];
        selection="KS";
    }


    TVector3 poschod = fTool->GetPositionAtZ(track,Constants::zCHOD);
    if(!fCHODCandidate[iC].GetIsCHODCandidate()){
        if(ntracks==2)
            FillHisto(Form("%sTrack%d_nochod",selection.Data(),iC),poschod.X(),poschod.Y());

        return 0;
    }
    FillHisto(Form("%sTrack%d_chodchi2",selection.Data(),iC),fCHODCandidate[iC].GetDiscriminant());

    if (fCHODCandidate[iC].GetDiscriminant()>15){
        if(ntracks==2)
            FillHisto(Form("%sTrack%d_chodchi2_rejected",selection.Data(),iC),poschod.X(),poschod.Y());
        return 0;
    }
    // TVector3 poschod[2]; for (Int_t jdet=0; jdet<2; jdet++) posAtCHOD[jdet] = tools->GetPositionAtZ(track,fZCHOD[jdet]);
    Double_t dx = fCHODCandidate[iC].GetX()-poschod.X();
    Double_t dy = fCHODCandidate[iC].GetY()-poschod.Y();
    Double_t mindist = sqrt(dx*dx+dy*dy);
    Double_t dtime = fCHODCandidate[iC].GetDeltaTime();
    FillHisto(Form("%sTrack%d_dist_vs_mintime",selection.Data(),iC),dtime,mindist);
    FillHisto(Form("%sTrack%d_dy_vs_dx",selection.Data(),iC),dx,dy);


    return 1;
}

///////////////////////////
// Nominal Kaon Momentum //
///////////////////////////
void KSAnalysis::NominalKaon() {
    Double_t pkaon = 74.9;
    Double_t tthx = 0.00122;
    Double_t tthy = 0.000026;

    Double_t pz = pkaon/sqrt(1.+tthx*tthx+tthy*tthy);
    Double_t px = pz*tthx;
    Double_t py = pz*tthy;
    fKaonNominalMomentum.SetXYZM(px,py,pz,0.001*Constants::MKCH);
}

void KSAnalysis::PostProcess(){
    /// \MemberDescr
    /// This function is called after an event has been processed by all analyzers. It could be used to free some memory allocated
    /// during the Process.
    /// \EndMemberDescr

}

void KSAnalysis::EndOfBurstUser(){
    /// \MemberDescr
    /// This method is called when a new file is opened in the ROOT TChain (corresponding to a start/end of burst in the normal NA62 data taking) + at the end of the last file\n
    /// Do here your start/end of burst processing if any.
    /// Be careful: this is called after the event/file has changed.
    /// \EndMemberDescr
}

void KSAnalysis::EndOfRunUser(){
    /// \MemberDescr
    /// This method is called at the end of the processing (corresponding to a end of run in the normal NA62 data taking)\n
    /// Do here your end of run processing if any\n
    /// \EndMemberDescr

}

void KSAnalysis::EndOfJobUser(){
    SaveAllPlots();
}

void KSAnalysis::DrawPlot(){
    /// \MemberDescr
    /// This method is called at the end of processing to draw plots when the -g option is used.\n
    /// If you want to draw all the plots, just call\n
    /// \code
    ///     DrawAllPlots();
    /// \endcode
    /// Or get the pointer to the histogram with\n
    /// \code
    ///     fHisto.GetTH1("histoName");// for TH1
    ///     fHisto.GetTH2("histoName");// for TH2
    ///     fHisto.GetGraph("graphName");// for TGraph and TGraphAsymmErrors
    ///     fHisto.GetHisto("histoName");// for TH1 or TH2 (returns a TH1 pointer)
    /// \endcode
    /// and manipulate it as usual (TCanvas, Draw, ...)\n
    /// \EndMemberDescr
}

KSAnalysis::~KSAnalysis(){
    /// \MemberDescr
    /// Destructor of the Analyzer. If you allocated any memory for class
    /// members, delete them here.
    /// \EndMemberDescr
}
