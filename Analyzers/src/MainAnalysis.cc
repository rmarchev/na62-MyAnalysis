#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "MainAnalysis.hh"
#include "RICHParameters.hh"
#include "EventHeader.hh"
#include "L0TPData.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
//#include "utl.hh"
#include "Constants.hh"
#include "AnalysisTools.hh"
#include "CalorimeterCluster.hh"
#include "SpectrometerNewCHODAssociation.hh"
#include "SpectrometerMUV3Association.hh"
#include "LAVMatching.hh"
#include "SAVMatching.hh"
using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


MainAnalysis::MainAnalysis(Core::BaseAnalysis *ba) : Analyzer(ba, "MainAnalysis")
{

    fTool = AnalysisTools::GetInstance();
    RequestL0Data();
    RequestTree("Spectrometer",new TRecoSpectrometerEvent);
    RequestTree("Cedar",new TRecoCedarEvent);
    RequestTree("CHOD",new TRecoCHODEvent);
    RequestTree("NewCHOD",new TRecoNewCHODEvent);
    RequestTree("LKr",new TRecoLKrEvent);
    RequestTree("MUV1",new TRecoMUV1Event);
    RequestTree("MUV2",new TRecoMUV2Event);
    RequestTree("MUV3",new TRecoMUV3Event);
    RequestTree("GigaTracker",new TRecoGigaTrackerEvent);
    RequestTree("LAV",new TRecoLAVEvent);
    RequestTree("SAV",new TRecoSAVEvent);
    RequestTree("IRC",new TRecoIRCEvent);
    RequestTree("SAC",new TRecoSACEvent);
    RequestTree("CHANTI",new TRecoCHANTIEvent);

    // Pointer to useful classes

    // fStrawRapper = new StrawRapper(0,ba);
    fCHODAnal = new CHODAnal(1,ba);
    fLKrAnal = new LKrAnal(1,ba);
    fMUV3Anal = new MUV3Anal(ba);
    //fRICHAnal = new RICHAnal(ba);
    fMUVAnal = new MUVAnal(ba);
    fCHANTIAnal = new CHANTIAnal(ba);
    fSACAnal = new SACAnal(ba);
    fSAVAnal = new SAVAnal(ba);
    fIRCAnal = new IRCAnal(ba);
    fLAVAnal = new LAVAnal(0,ba);
    fGigaTrackerAnalysis = new GigaTrackerAnalysis(ba);
    //fGigaTrackerAnalysisCHOD = new GigaTrackerAnalysis(ba);
    fCedarAnal = new CedarAnal(0,ba);
    fStrawRapper = new StrawRapper(0,ba,fCHODAnal,fLKrAnal);
    //fCaloArray = new TClonesArray("CalorimeterCluster",20);
    fYear=2016;

    fLAVMatching = new LAVMatching();
    fSAVMatching = new SAVMatching();

    AddParam("GTKFlag", &fGTKFlag, 0); // default 0 GTK in
    AddParam("Trigger", &fTriggerFlag, 2); // 2: pinunu physics (default), 1: min bias physics, 0: control

    //std::cout << "WTF is happening with the mc === " << fIsMC << std::endl;
    //if(GetWithMC()){
    //    fGTKFlag == 1;
    //} else {
    //    fGTKFlag == 0;
    //
    //}
    fFineTimeScale = 24.951059536/256.;

}

void MainAnalysis::InitOutput(){
    RegisterOutput("NBeamPions",&fNBeamPions);
    //RegisterOutput("NAccidentalCandidates",&fNAccidentalTracks);
    //RegisterOutput("AccidentalDownstreamTrack",fAccidentalDownstreamCandidate);
    //RegisterOutput("AccidentalUpstreamTrack",fAccidentalUpstreamCandidate);
    RegisterOutput("NKaonEventCandidates",&fNGoodKaonTracks);
    RegisterOutput("BeamPionDownstreamTrack",fBeamPionDownstreamCandidate);
    RegisterOutput("BeamPionUpstreamTrack",fBeamPionUpstreamCandidate);
    RegisterOutput("KaonEventDownstreamTrack",fKaonEventDownstreamCandidate);
    RegisterOutput("KaonEventUpstreamTrack",fKaonEventUpstreamCandidate);
    RegisterOutput("OneTrackEvent",&fIsGoodOneTrackEvent);
    RegisterOutput("GTKFlag",&fGTKFlag);
    RegisterOutput("Year",&fYear);
}

void MainAnalysis::InitHist(){

    BookHisto(new TH1F("Tracks_step_0_ptrack","",100,0,100000));
    BookHisto(new TH1F("Tracks_step_GoodStrawTrack_ptrack","",100,0,100000));
    BookHisto(new TH1F("Tracks_step_PosCharge_ptrack","",100,0,100000));
    BookHisto(new TH1F("Tracks_step_NegCharge_ptrack","",100,0,100000));

    //Acceptance plots in GeometricalAcceptance
    BookHisto(new TH2F("Tracks_Acc_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw1_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw2_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw3_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw4_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccCHOD_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccNewCHOD_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccLKr_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccRICH_exit_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccRICH_entrance_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccMUV1_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccMUV2_in","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccMUV3_in","",300,-1500,1500,300,-1500,1500));

    BookHisto(new TH2F("Tracks_Acc_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw1_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw2_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw3_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccStraw4_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccCHOD_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccNewCHOD_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccLKr_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccRICH_exit_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccRICH_entrance_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccMUV1_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccMUV2_out","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_AccMUV3_out","",300,-1500,1500,300,-1500,1500));

    //After Geometrical Acceptance
    BookHisto(new TH1F("Tracks_step_Acc_ptrack","",100,0,100000));

    //Chod plots inside goodchodcandidate
    BookHisto(new TH1F("Tracks_IsCHOD_chodminchi2","",200,0,50));
    BookHisto(new TH2F("Tracks_IsCHOD_rejected","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_CHODChi2_rejected","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_CHODMatch_dist_vs_mintime","",200,-100,100,50,0,250));
    BookHisto(new TH2F("Tracks_CHODMatch_dy_vs_dx","",200,-500,500,200,-500,500));
    BookHisto(new TH1F("Tracks_step_GoodCHOD_ptrack","",100,0,100000));

    //Chod plots inside goodchodcandidate
    BookHisto(new TH1F("Tracks_IsNewCHOD_chodminchi2","",200,0,50));
    BookHisto(new TH2F("Tracks_IsNewCHOD_rejected","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_NewCHODChi2_rejected","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_NewCHODMatch_dist_vs_mintime","",200,-100,100,50,0,250));
    BookHisto(new TH2F("Tracks_NewCHODMatch_dist_vs_mindtchod","",800,-50,50,50,0,250));
    BookHisto(new TH2F("Tracks_NewCHODMatch_dy_vs_dx","",200,-500,500,200,-500,500));
    BookHisto(new TH1F("Tracks_step_GoodNewCHOD_ptrack","",100,0,100000));


    // LKr track association
    //BookHisto(new TH2F("Tracks_IsLKr_mindist_vs_dttrack","",200,-100,100,300,0,300));
    //BookHisto(new TH2F("Tracks_IsLKr_mindist_vs_dtchod","",400,-100,100,300,0,300));
    //BookHisto(new TH2F("Tracks_IsLKr_mindx_vs_mindy","",300,-300,300,300,-300,300));
    //BookHisto(new TH2F("Tracks_IsLKr_energy_vs_dist","",300,0,300,400,0,100));
    //BookHisto(new TH2F("Tracks_IsLKr_energy_vs_dist_zoom","",150,0,150,200,0,10));
    //BookHisto(new TH2F("Tracks_IsLKr_eovp_vs_ptrack","",200,0.,100.,130,0,1.3));

    //  BookHisto(new TH1F("Tracks_step_IsLKr_ptrack","",100,0,100000));


    BookHisto(new TH1I("Tracks_IsRICHSingle_flag","",2,0,2));
    BookHisto(new TH1F("Tracks_IsRICHSingle_dtchod","",1600,-100,100));
    BookHisto(new TH1I("Tracks_IsRICHSingle_nhits","",50,0,50));
    //BookHisto(new TH1F("Tracks_IsRICHSingle_ringchi2","",300,0,30));
    BookHisto(new TH1F("Tracks_IsRICHSingle_prob","",100,0,1));
    //BookHisto(new TH1F("Tracks_IsRICHSingle_dr","",800,0.,800.));
    BookHisto(new TH2F("Tracks_IsRICHSingle_dxdy","",1200,-600.,600.,1200,-600.,600.));
    BookHisto(new TH2F("Tracks_IsRICHSingle_xydslope","",250,-0.01,0.01,250,-0.01,0.01));

    BookHisto(new TH2F("Tracks_IsRICHSingle_ringrad_vs_p","",200,0.,100.,300,0,300));
    BookHisto(new TH2F("Tracks_IsRICHSingle_richmass_vs_p","",200,0.,100.,500,0.,1.));
    BookHisto(new TH2F("Tracks_IsRICHSingle_richp_vs_p","",200,0.,100.,200,0.,100.));
    BookHisto(new TH2F("Tracks_IsRICHSingle_dtimevschi2rich","",200,0,50,1600,-100,100));
    BookHisto(new TH2F("Tracks_IsRICHSingle_richp_vs_p_f","",200,0.,100.,200,0.,100.));
    BookHisto(new TH2F("Tracks_IsRICHSingle_dtimevschi2rich_f","",200,0,50,1600,-100,100));

    BookHisto(new TH1I("Tracks_IsRICH_NRings","",50,0,50));
    BookHisto(new TH1F("Tracks_IsRICH_ringchi2","",300,0,30));
    BookHisto(new TH1F("Tracks_IsRICH_hypothesis_all","",6,0,6 ));
    // BookHisto(new TH1F("Tracks_IsRICH_mu_likelihood","",100,0,1 ));
    // BookHisto(new TH1F("Tracks_IsRICH_pi_likelihood","",100,0,1 ));
    //BookHisto(new TH1F("Tracks_IsRICH_e_likelihood","",100,0,1 ));
    BookHisto(new TH1F("Tracks_IsRICH_hypothesis_15_35_GeV","",6,0,6 ));
    BookHisto(new TH2F("Tracks_IsRICH_xyslope","",250,-0.01,0.01,250,-0.01,0.01));
    BookHisto(new TH2F("Tracks_IsRICH_ringrad_vs_p","",200,0.,100.,300,0,300));
    BookHisto(new TH2F("Tracks_IsRICH_richmass_vs_p","",200,0.,100.,500,0.,1.));
    BookHisto(new TH2F("Tracks_IsRICH_richp_vs_p","",200,0.,100.,200,0.,100.));

    BookHisto(new TH2F("Tracks_lavmatching_nph","",100,0,100,100,0,100));
    BookHisto(new TH2F("Tracks_lavmatching_matched","",2,0,2,2,0,2));
    BookHisto(new TH1F("Tracks_lavmatching_time","",200,-10,10));
    BookHisto(new TH2F("Tracks_ircmatching_nph","",20,0,20,20,0,20));
    BookHisto(new TH1F("Tracks_ircmatching_time","",200,-10,10));
    BookHisto(new TH2F("Tracks_sacmatching_nph","",20,0,20,20,0,20));
    BookHisto(new TH1F("Tracks_sacmatching_time","",200,-10,10));

    BookHisto(new TH2F("Tracks_step_NoRICHMultiMatched_ent","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Tracks_step_NoRICHMultiMatched_ex","",240,-1200,1200,240,-1200,1200));


    BookHisto(new TH2F("Tracks_step_NoRICHSingleMatched_ent","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Tracks_step_NoRICHSingleMatched_ex","",240,-1200,1200,240,-1200,1200));

    BookHisto(new TH1F("Tracks_step_IsRICH_ptrack","",100,0,100000));

    BookHisto(new TH1F("Tracks_Tdown","",800,-50,50));

    //LKr plots inside ....
    BookHisto(new TH2F("Tracks_IsLKrCl_rejected","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_IsLKrCell_rejected","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH2F("Tracks_IsLKrRA_rejected","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH1F("Tracks_NLKrHits","",15000,0,15000));
    // LKr track association
    BookHisto(new TH2F("Tracks_IsLKr_mindist_vs_dttrack","",200,-100,100,300,0,300));
    BookHisto(new TH2F("Tracks_IsLKr_mindist_vs_dtchod","",400,-100,100,300,0,300));
    BookHisto(new TH2F("Tracks_IsLKr_mindist_vs_dtrich","",400,-100,100,300,0,300));
    BookHisto(new TH2F("Tracks_IsLKr_mindx_vs_mindy","",300,-300,300,300,-300,300));
    BookHisto(new TH2F("Tracks_IsLKr_energy_vs_dist","",300,0,300,400,0,100));
    BookHisto(new TH2F("Tracks_IsLKr_energy_vs_dist_zoom","",150,0,150,200,0,10));
    BookHisto(new TH2F("Tracks_IsLKr_eovp_vs_ptrack","",200,0.,100.,130,0,1.3));

    BookHisto(new TH1F("Tracks_step_IsLKr_ptrack","",100,0,100000));


    //MUV1 association plots
    BookHisto(new TH2F("Tracks_step_NoMUV1Matched","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH1F("Tracks_step_HasMUV1_ptrack","",100,0,100000));

    //MUV2 association plots
    BookHisto(new TH2F("Tracks_step_NoMUV2Matched","",300,-1500,1500,300,-1500,1500));
    BookHisto(new TH1F("Tracks_step_HasMUV2_ptrack","",100,0,100000));

    // MUV3 track association
    //BookHisto(new TH2F("Tracks_IsMUV3_tight_dist_vs_dchodtime","",200,-50,50,150,0,600));
    //BookHisto(new TH2F("Tracks_IsMUV3_broad_dist_vs_dchodtime","",200,-50,50,150,0,600));
    // BookHisto(new TH2F("Tracks_IsMUV3_dist_vs_dchodtime","",400,-20,20,150,0,600));
    BookHisto(new TH1F("Tracks_IsMUV3_drichtime","",400,-20,20));
    BookHisto(new TH2F("Tracks_IsMUV3_standard_dist_vs_dchodtime","",400,-20,20,150,0,600));


    BookHisto(new TH2F("Tracks_MUV3Matched","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Tracks_NoMUV3Matched","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH1F("Tracks_step_MUV3_rejection_ptrack","",100,0,100000));

    BookHisto(new TH2F("Tracks_standard_MUV3Matched","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("Tracks_standard_NoMUV3Matched","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH1F("Tracks_step_standard_MUV3_rejection_ptrack","",100,0,100000));

    // Hadronic calorimetric energy
    BookHisto(new TH2F("Track_muv1candidate_dist_vs_eovp","",130,0,1.3,300,0,900.));
    BookHisto(new TH2F("Tracks_IsMUV1Cal_dist_vs_dtime_noelkr","",200,-50,50,300,0,900.));
    BookHisto(new TH2F("Tracks_IsMUV1Cal_dist_vs_dtime_elkr","",200,-50,50,300,0,900.));
    BookHisto(new TH2F("Tracks_IsMUV1Cal_emuv1_vs_elkr","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Tracks_IsMUV1Cal_energy_vs_p","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Tracks_IsMUV1Cal_hadenergy_vs_p","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Tracks_IsMUV1Cal_eovp_vs_p","",200,0,100.,130,0,1.3));
    BookHisto(new TH2F("Track_muv2candidate_dist_vs_eovp","",130,0,1.3,300,0,900.));
    BookHisto(new TH2F("Tracks_IsMUV2Cal_dist_vs_dtime_etot","",200,-50,50,300,0,900.));
    BookHisto(new TH2F("Tracks_IsMUV2Cal_dist_vs_dtime_noetot","",200,-50,50,300,0,900.));
    BookHisto(new TH2F("Tracks_IsMUV2Cal_emuv2_vs_etot","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Track_eovpvsdt10","",200,-50,50,130,0,1.3));
    BookHisto(new TH2F("Track_eovpvsdt11","",200,-50,50,130,0,1.3));
    BookHisto(new TH2F("Track_eovpvsdt01","",200,-50,50,130,0,1.3));

    BookHisto(new TH2F("Track_calocandidate_pion_emuvvselkr","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Track_calocandidate_pion_energyvsp","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Track_calocandidate_pion_hadenergyvsp","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Track_calocandidate_pion_eovpvsp","",200,0,100.,130,0,1.3));
    BookHisto(new TH2F("Track_calocandidate_pion_eratio1vsp","",200,0,100.,110,0,1.1));
    BookHisto(new TH2F("Track_calocandidate_pion_eratio2vsp","",200,0,100.,110,0,1.1));
    BookHisto(new TH2F("Track_calocandidate_pion_eratio2vseratio1","",110,0,1.1,110,0,1.1));
    BookHisto(new TH2F("Track_calocandidate_muon_emuvvselkr","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Track_calocandidate_muon_energyvsp","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Track_calocandidate_muon_hadenergyvsp","",200,0,100.,200.,0.,100.));
    BookHisto(new TH2F("Track_calocandidate_muon_eovpvsp","",200,0,100.,130,0,1.3));
    BookHisto(new TH2F("Track_calocandidate_muon_eratio1vsp","",200,0,100.,110,0,1.1));
    BookHisto(new TH2F("Track_calocandidate_muon_eratio2vsp","",200,0,100.,110,0,1.1));
    BookHisto(new TH2F("Track_calocandidate_muon_eratio2vseratio1","",110,0,1.1,110,0,1.1));


    BookHisto(new TH2F("Tracks_dtimetrigchod","",10,0,10,400,-150,150));
    BookHisto(new TH1F("Tracks_step_final","",100,0,100000));

    //Multi-track
    BookHisto(new TH1F("TwoTracks_nstrawtracks","",10,0,10));
    BookHisto(new TH1F("TwoTracks_mmiss","",375,-0.1,0.14375));
    //BookHisto(new TH1F("TwoTracks_mmiss","",300,0.,0.3));

    BookHisto(new TH1F("ThreeTracks_nstrawtracks","",10,0,10));
    BookHisto(new TH1F("ThreeTracks_mk","",300,0.3,0.6));

    // KTAG association
    BookHisto(new TH2F("OneTrack_stability_dtktag_vs_chodchannel","",2048,0,2048,200,-10,10));
    BookHisto(new TH1F("OneTrack_stability_dtktag","",200,-10,10));
    BookHisto(new TH2F("OneTrack_IsKTAG_dtktag_vs_p","",200,0,100,200,-10,10));
    BookHisto(new TH2F("OneTrack_IsNotKTAG_dtktag_vs_p","",200,0,100,200,-10,10));
    BookHisto(new TH1F("OneTrack_step_Q_vs_IsCedarMatched","",4,-2,2));

    BookHisto(new TH1F("OneTrack_NoGTKCand_ptrack","",100,0,100));
    BookHisto(new TH1F("OneTrack_IsGTK_type","",130,0,130));
    BookHisto(new TH1F("OneTrack_IsGTK_P", " GTK momentum;P_{K} [MeV/c]",100,65000.,85000));
    BookHisto(new TH1F("OneTrack_IsGTK_richdt","T_{chod} - T_{gtk} ; dT_{chod - gtk}",400,-2.,2.));
    BookHisto(new TH1F("OneTrack_IsGTK_choddt","T_{chod} - T_{gtk} ; dT_{chod - gtk}",400,-2.,2.));
    BookHisto(new TH1F("OneTrack_IsGTK_cedardt","T_{cedar} - T_{gtk} ; dT_{cedar - gtk}",400,-5.,5.));
    BookHisto(new TH1F("OneTrack_IsGTK_chi2","#chi^{2} ; #chi^{2}",300,0.,300.));
    BookHisto(new TH1F("OneTrack_IsGTK_chi2X","#chi^{2}X ; #chi^{2}",300,0.,300.));
    BookHisto(new TH1F("OneTrack_IsGTK_chi2Y","#chi^{2}Y ; #chi^{2}",300,0.,300.));
    BookHisto(new TH1F("OneTrack_IsGTK_chi2T","#chi^{2}T ; #chi^{2}",300,0.,300.));
    BookHisto(new TH2F("OneTrack_IsGTK_pos","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));

    BookHisto(new TH1F("OneTrack_IsGTK_dX_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("OneTrack_IsGTK_dY_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("OneTrack_IsGTK_dX_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("OneTrack_IsGTK_dY_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("OneTrack_IsGTK_dX_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("OneTrack_IsGTK_dY_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("OneTrack_IsGTK_thetax", " ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
    BookHisto(new TH1F("OneTrack_IsGTK_thetay", " ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));


    BookHisto(new TH1F("OneTrack_IsGTK_discriminant","GTK Discriminant ;#chi^{2} ",200, 0, 1));
    BookHisto(new TH1F("OneTrack_IsGTK_discriminant_cedar","GTK Discriminant ;#chi^{2} ",200, 0, 1));
    BookHisto(new TH1F("OneTrack_IsGTK_discriminant_chod","GTK Discriminant ;#chi^{2} ",200, 0, 1));
    BookHisto(new TH2F("OneTrack_IsGTK_discr_rich_vs_cedar"," ",200, 0, 1,200, 0, 1));

    BookHisto(new TH2F("OneTrack_IsGTK_dt_vs_cda", "  cda vs T_{rich} - T_{gtk} ; dT_{rich - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));
    BookHisto(new TH2F("OneTrack_IsGTK_dtchod_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));

    BookHisto(new TH2F("OneTrack_IsGTK_dtcedar_vs_cda", "  cda vs T_{cedar} - T_{gtk} ; dT_{cedar - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));
    BookHisto(new TH2F("OneTrack_IsGTK_dtcedar_vs_cda_good", "  cda vs T_{cedar} - T_{gtk} ; dT_{cedar - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));
    BookHisto(new TH2F("OneTrack_IsGTK_dt_vs_cda_good", "  cda vs T_{rich} - T_{gtk} ; dT_{rich - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));
    BookHisto(new TH2F("OneTrack_IsGTK_dtchod_vs_cda_good", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));

    BookHisto(new TH2F("OneTrack_IsGTK_dtcedar_vs_cda_final", "  cda vs T_{cedar} - T_{gtk} ; dT_{cedar - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));
    BookHisto(new TH2F("OneTrack_IsGTK_dt_vs_cda_final", "  cda vs T_{rich} - T_{gtk} ; dT_{rich - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));
    BookHisto(new TH2F("OneTrack_IsGTK_dtchod_vs_cda_final", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -2, 2,100, 0,50));

    BookHisto(new TH1F("OneTrack_step_Q_vs_IsGTKMatched","",4,-2,2));
    BookHisto(new TH2F("OneTrack_IsCHANTI_p_vs_zvtx","",300,0, 300000, 100,0,100));
    BookHisto(new TH1F("OneTrack_step_Q_vs_IsCHANTIMatched","",4,-2,2));
    BookHisto(new TH1F("OneTrack_step_Q_vs_IsRSingle","",4,-2,2));
    BookHisto(new TH1F("OneTrack_step_Q_vs_IsRMulti","",4,-2,2));
    BookHisto(new TH1F("OneTrack_step_Q_vs_AIsRSingle","",4,-2,2));
    BookHisto(new TH1F("OneTrack_step_Q_vs_AIsRMulti","",4,-2,2));



    BookHisto(new TH2F("OneTrack_fgtk_mm2_vs_p","",100,0,100,300,-0.15,0.15));
    BookHisto(new TH2F("OneTrack_sgtk_mm2_vs_p","",100,0,100,300,-0.15,0.15));
    BookHisto(new TH2F("OneTrack_tgtk_mm2_vs_p","",100,0,100,300,-0.15,0.15));
    BookHisto(new TH2F("OneTrack_p_vs_zvtx","",300,0, 300000, 100,0,100));
    BookHisto(new TH2F("OneTrack_mm2_vs_p","",100,0,100,300,-0.15,0.15));
    BookHisto(new TH2F("OneTrack_mm2_vs_p_lavtrack","",100,0,100,300,-0.15,0.15));
    BookHisto(new TH2F("OneTrack_mm2_vs_p_nolavtrack","",100,0,100,300,-0.15,0.15));
    BookHisto(new TH2F("OneTrack_mm2_vs_p_nolavatall","",100,0,100,300,-0.15,0.15));
    BookHisto(new TH2F("OneTrack_kaon_pion_zvtxvsp","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("OneTrack_kaon_pion_mmissvsp","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_pionnoe_mmissvsp","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_muon_xymuv3","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("OneTrack_kaon_pion_xymuv3","",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F("OneTrack_kaon_muon_zvtxvsp","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("OneTrack_kaon_muon_mmissvsp","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_muon_mmissmuvsp","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("OneTrack_kaon_chanti_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("OneTrack_kaon_nochanti_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("OneTrack_kaon_lkrextra_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_lav_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_irc_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_sac_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranorm_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextraaux_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextraboth_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextra_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolav_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoirc_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosac_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosacnochanti_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosav_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectron_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3wrich_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3wrichpi_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3wrichmu_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_pion_nosacnoircnolav_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("OneTrack_kaon_pion_nosacnoircnolav_noelectrons_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

    BookHisto(new TH2F("BeamPion_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("BeamPion_richradius_vs_p","",200,0,100,300,0,300));
    BookHisto(new TH2F("BeamPion_p_vs_zvtx","",250,50000,200000,200,0,100));
    BookHisto(new TH2F("BeamPion_mmiss_vs_p_isgtk","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("BeamPion_mmiss_vs_p_nomuv3","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("BeamPion_mmiss_vs_p_isgtk_nomuv3","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("BeamPion_mmiss_vs_p_isgtk_nomuv3lavircsac","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F("BeamPion_mmiss_vs_p_isgtk_nomuv3norichkaonlavircsac","",200,0,100,375,-0.1,0.14375));


}

void MainAnalysis::DefineMCSimple(){

}

void MainAnalysis::StartOfRunUser(){

    EventHeader* header = GetEventHeader();
    fYear=2016;
    fRun = header->GetRunID();
    fIsMC = GetWithMC();
    if(!fIsMC){

        fTGlobal = 0.100;
        fTCorr_ktag[0] = 0.100;
        fTCorr_ktag[1] = 0.;
        fTCorr_gtk[0] = 0.100;
        fTCorr_gtk[1] = 0.;

        TString TimeCorrFileName = "/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/TimeAlignment.dat";
        ifstream TimeCorrFile(TimeCorrFileName.Data());
        TString line;
        int iscorr = 0;
        while (line.ReadLine(TimeCorrFile)) {
            if (line.BeginsWith("#")) continue;
            if (line.BeginsWith("ApplyCorrection")) {
                TObjArray * l = line.Tokenize(" ");
                iscorr = ((TObjString*)(l->At(1)))->GetString().Atoi();
                delete l;
            } else {
                if (!iscorr) continue;
                TObjArray * l = line.Tokenize(" ");
                int runid = ((TObjString*)(l->At(0)))->GetString().Atoi();
                if (runid==(int)GetEventHeader()->GetRunID()) {
                    fTGlobal = ((TObjString*)(l->At(1)))->GetString().Atof();
                    fTCorr_ktag[0] = ((TObjString*)(l->At(2)))->GetString().Atof();
                    fTCorr_ktag[1] = ((TObjString*)(l->At(3)))->GetString().Atof();
                    fTCorr_gtk[0] = ((TObjString*)(l->At(4)))->GetString().Atof();
                    fTCorr_gtk[1] = ((TObjString*)(l->At(5)))->GetString().Atof();
                }
                delete l;
            }
        }
        cout << "---> File " << TimeCorrFileName.Data() << " read " << fTGlobal << " " << fTCorr_ktag[0] << " " << fTCorr_ktag[1] << " " << fTCorr_gtk[0] << " " << fTCorr_gtk[1] << endl;
        TimeCorrFile.close();
    }

    fCHANTIAnal->StartBurst(fYear,fRun );
    fSACAnal->StartBurst(fYear,fRun);
    fIRCAnal->StartBurst(fYear,fRun);
    fCHODAnal->StartBurst(fRun);
    fSAVAnal->StartBurst(fYear,fRun);
    fLAVAnal->StartBurst(fRun,0);
}

void MainAnalysis::StartOfBurstUser(){
    EventHeader* header = GetEventHeader();
    fYear=2016;
    fNewBurst = 1;
    fRun = header->GetRunID();
    fBurst = header->GetBurstID();

    fIsFilter = GetTree("Reco")->FindBranch("FilterWord") ? true : false;
    fElectronRingRadius = 190.0;
    fElectronRingNhits = 14.0;
    fElectronRingRadius = RICHParameters::GetInstance()->GetElectronRingRadius(fRun,header->GetBurstTime());
    fElectronRingNhits = RICHParameters::GetInstance()->GetElectronRingNHits(fRun,header->GetBurstTime());

    if(!fIsMC){

        // Align in time KTAG and GTK (vs trigger time)
        double gtkoff[25];
        double ktgoff[25];
        for (int k=0; k<25; k++) gtkoff[k] = ktgoff[k] = 0;
        int burstid = GetBurstID();
        int runid = GetRunID();
        //double bursttime= header->GetBurstTime();
        if (!fIsMC) {
            TH2F *hktag2 = new TH2F("hktag2","",25,0,250000000,50,-1.5,1.5);
            TH2F *hktag3 = new TH2F("hktag3","",25,0,250000000,50,-1.5,1.5);
            TString condition = Form("L0TP.fDataType<16&&!GigaTracker.fErrorMask&&!EventHeader.fEventQualityMask&&EventHeader.fBurstID==%d",burstid);
            GetTree("Reco")->GetTree()->Draw(Form("(GigaTracker.fHits.fTime-EventHeader.fFineTime*%f):EventHeader.fTimeStamp>>%s",TdcCalib,"hktag3"),condition.Data(),"goff");
            GetTree("Reco")->GetTree()->Draw(Form("(Cedar.fCandidates.fTime-EventHeader.fFineTime*%f):EventHeader.fTimeStamp>>%s",TdcCalib,"hktag2"),condition.Data(),"goff");
            TH1F *hprof2 = (TH1F *)hktag2->ProjectionY()->Clone("hprof2");
            TH1F *hprof3 = (TH1F *)hktag3->ProjectionY()->Clone("hprof3");
            int mb2 = hprof2->GetMaximumBin();
            int mb3 = hprof3->GetMaximumBin();
            hprof2->GetXaxis()->SetRange(mb2-7,mb2+7);
            hprof3->GetXaxis()->SetRange(mb3-7,mb3+7);
            for (int k=0; k<25; k++) {
                ktgoff[k] = fTGlobal-hprof2->GetMean();
                gtkoff[k] = fTGlobal-hprof3->GetMean();
                cout << burstid << "  --  " << gtkoff[k] << " " << ktgoff[k] << " " << hprof3->GetMean() << " " << hprof2->GetMean() << endl;
            }
            delete hktag3;
            delete hktag2;
        }

        fCedarAnal->StartBurst(fBurst,ktgoff);
        fGigaTrackerAnalysis->StartBurst(fYear,runid,burstid,gtkoff);
    }

}

void MainAnalysis::Process(int iEvent){
    if(GetWithMC())
        if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
    OutputState state;

    if (fNewBurst) {
        GetTree("Reco")->GetEntry(iEvent);
        fNewBurst = 0;
    }
    fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
    fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
    fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
    fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
    if(!fIsFilter) fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",state);
    if(fIsFilter || GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.1") || GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.2") ) fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
    fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
    fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
    fIRCEvent = (TRecoIRCEvent*)GetEvent("IRC");
    fSACEvent = (TRecoSACEvent*)GetEvent("SAC");
    fSAVEvent = (TRecoSAVEvent*)GetEvent("SAV");
    fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");
    fCHANTIEvent = (TRecoCHANTIEvent*)GetEvent("CHANTI");
    fCHODEvent = (TRecoCHODEvent*)GetEvent("CHOD");
    fNewCHODEvent = (TRecoNewCHODEvent*)GetEvent("NewCHOD");
    fCedarEvent = (TRecoCedarEvent*)GetEvent("Cedar");
    fStrawCandidate = (StrawCandidate*)GetOutput("TrackProcessor.StrawCandidate");
    fRICHCandidate = (RICHCandidate*)GetOutput("TrackProcessor.RICHCandidate");
    fCHODCandidate = (CHODCandidate*)GetOutput("TrackProcessor.CHODCandidate");
    fNewCHODCandidate = (NewCHODCandidate*)GetOutput("TrackProcessor.NewCHODCandidate");
    fLKrClusCandidate = (LKrCandidate*)GetOutput("TrackProcessor.LKrCandidate");
    fLKrCellCandidate = (LKrCandidate*)GetOutput("TrackProcessor.LKrCellCandidate");
    fMUV1Candidate = (MUVCandidate*)GetOutput("TrackProcessor.MUV1Candidate");
    fMUV2Candidate = (MUVCandidate*)GetOutput("TrackProcessor.MUV2Candidate");
    fMUV3TightCandidate = (MUV3Candidate*)GetOutput("TrackProcessor.MUV3TightCandidate");
    fMUV3LooseCandidate = (MUV3Candidate*)GetOutput("TrackProcessor.MUV3LooseCandidate");
    fMUV3Candidate = *(std::vector<SpectrometerMUV3AssociationOutput>*)GetOutput("SpectrometerMUV3Association.Output");

    if (fGigaTrackerEvent->GetErrorMask()) return; //check
    fGigaTrackerAnalysis->Clear();
    //fGigaTrackerAnalysisCHOD->Clear();

    //Class performing basic straw analysis
    //StrawRapper straw((TRecoSpectrometerEvent*)GetEvent("Spectrometer"),fBaseAnalyser);
    fNTracks = fSpectrometerEvent->GetNCandidates();

    // Clear event
    ClearEvent();
    fIsGoodOneTrackEvent =0;
    fBadEvent = 0;
    if(fNTracks == 0) return;
    if(fNTracks > 10) return;
    fHeader = GetEventHeader();

    fFineTriggerTime = fHeader->GetFineTime()*TdcCalib;
    fLKrAnal->SetEvent(fLKrEvent);
    fCHODAnal->SetEvent(fCHODEvent);
    fCHODAnal->SetLKrEvent(fLKrEvent);
    fCHODAnal->SetNewCHODEvent(fNewCHODEvent);


    fL0Data = GetWithMC() ? 0 : GetL0Data();
    fL0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
    fL0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();
    fPhysicsData   = fL0DataType    & 0x1;

    fControlTrigger = SelectTrigger(0,fL0DataType,fL0TriggerWord);

    // Quality cut
    Int_t eventqualitymask = fHeader->GetEventQualityMask();
    if(eventqualitymask&(1<<kHAC)) eventqualitymask-=(1<<kHAC);
    if (eventqualitymask>0) return;

    fIsGoodOneTrackEvent = Analyze();

    // Output variables
    //SetOutputState("NAccidentalCandidates",kOValid);
    //SetOutputState("AccidentalDownstreamTrack",kOValid);
    //SetOutputState("AccidentalUpstreamTrack",kOValid);
    SetOutputState("NKaonEventCandidates",kOValid);
    SetOutputState("KaonEventDownstreamTrack",kOValid);
    SetOutputState("KaonEventUpstreamTrack",kOValid);
    SetOutputState("OneTrackEvent",kOValid);

}

void MainAnalysis::ClearEvent() {
    for (Int_t j=0; j<10; j++) {
        fDownstreamCandidate[j].Clear();
        //fAccidentalDownstreamCandidate[j].Clear();
        //fAccidentalUpstreamCandidate[j].Clear();
        fBeamPionDownstreamCandidate[j].Clear();
        fBeamPionUpstreamCandidate[j].Clear();
        fKaonEventDownstreamCandidate[j].Clear();
        fKaonEventUpstreamCandidate[j].Clear();
    }
    fNGoodTracks = 0;
    fNK3piTracks = 0;
    fNGoodKaonTracks = 0;
    fNBeamPions = 0;
    //fNAccidentalTracks = 0;
}

void MainAnalysis::ClearUpstreamTrack() {
    fUpstreamTrack.Clear();
    fVertex.SetXYZ(999999.,999999.,0.);
    fcda = 999999.;
}


void MainAnalysis::PostProcess(){

}

void MainAnalysis::EndOfBurstUser(){
}

void MainAnalysis::EndOfRunUser(){

}

void MainAnalysis::EndOfJobUser(){
    SaveAllPlots();
    fCedarAnal->GetUserMethods()->SaveAllPlots();
    fCHODAnal->GetUserMethods()->SaveAllPlots();
    fSACAnal->GetUserMethods()->SaveAllPlots();
    fIRCAnal->GetUserMethods()->SaveAllPlots();
    fSAVAnal->GetUserMethods()->SaveAllPlots();
    fLAVAnal->GetUserMethods()->SaveAllPlots();
    fLKrAnal->GetUserMethods()->SaveAllPlots();
    fCHANTIAnal->GetUserMethods()->SaveAllPlots();
    fGigaTrackerAnalysis->GetUserMethods()->SaveAllPlots();
}

void MainAnalysis::DrawPlot(){
}

MainAnalysis::~MainAnalysis(){
}
Bool_t MainAnalysis::Analyze(){
    //cout << "Ngood = " << fNGoodTracks << endl;
    if(!PreSelectOneTrackEvent()) return false;
    if(!SelectOneTrackEvent())    return false;

    return true;
}

Bool_t MainAnalysis::PreSelectOneTrackEvent(){
    OutputState state;

    TClonesArray& CaloArray = *(TClonesArray*)GetOutput("SpectrometerCalorimetersAssociation.MatchedClusters",state);
    //fCaloArray = *(TClonesArray*)GetOutput("SpectrometerCalorimetersAssociation.MatchedClusters",state);
    //fCaloArray = CaloArray;
    for(int iC(0);iC<fNTracks;iC++){
        fDownstreamTrack.Clear();

        TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(iC);
        FillHisto("Tracks_step_0_ptrack",track->GetMomentum());

        if(!fStrawCandidate[iC].GetGoodTrack()) continue;


        //cout << "In Main Analysis" << "\n";
        //std::cout << GetEventHeader()->GetRunID()<< "  " << GetEventHeader()->GetBurstID() << "  " << GetEventHeader()->GetEventNumber() << endl;

        //CHODGoodCandidate build
        FillHisto("Tracks_step_GoodStrawTrack_ptrack", track->GetMomentum());

        if (fStrawCandidate[iC].GetCharge()==1)
            FillHisto("Tracks_step_PosCharge_ptrack",track->GetMomentum());
        else{

            FillHisto("Tracks_step_NegCharge_ptrack",track->GetMomentum());
        }
        //Geometrical acceptance in all detectors
        if(!GeometricalAcceptance(iC,track)) continue;

        TVector3 posstraw1 = fTool->GetPositionAtZ(track,Constants::zSTRAW_station[0]);
        FillHisto("Tracks_step_Acc_ptrack",track->GetMomentum());

        // Set dowstream particle candidate kinematics
        fDownstreamTrack.SetTime(track->GetTime());
        fDownstreamTrack.SetCharge(track->GetCharge());
        Double_t pmom = track->GetMomentum();
        Double_t dxdz = track->GetSlopeXBeforeMagnet();
        Double_t dydz = track->GetSlopeYBeforeMagnet();
        fDownstreamTrack.SetMomentum(fTool->Get4Momentum(pmom,dxdz,dydz,0.13957018));
        fDownstreamTrack.SetMuonMomentum(fTool->Get4Momentum(pmom,dxdz,dydz,0.1056583715));
        fDownstreamTrack.SetElectronMomentum(fTool->Get4Momentum(pmom,dxdz,dydz,0.000511));

        TVector3 poschod = fTool->GetPositionAtZ(track,Constants::zCHOD);

        if(!GoodCHODCandidate(iC,track)) continue;

        FillHisto("Tracks_step_GoodCHOD_ptrack",track->GetMomentum());

        if(!GoodNewCHODCandidate(iC,track)) continue;
        FillHisto("Tracks_step_GoodNewCHOD_ptrack",track->GetMomentum());

        bool isrich = true;

        if(!GoodRICHSingleCandidate(iC,track)){
            TVector3 posrichent = fTool->GetPositionAtZ(track,Constants::zRICHEntWindow);
            TVector3 posrichex  = fTool->GetPositionAtZ(track,Constants::zRICHExtWindow);
            FillHisto("Tracks_step_NoRICHSingleMatched_ent",posrichent.X(),posrichent.Y());
            FillHisto("Tracks_step_NoRICHSingleMatched_ex",posrichex.X(),posrichex.Y());
            isrich = false;
        }

        if(!GoodRICHMultiCandidate(iC,track)){
            TVector3 posrichent = fTool->GetPositionAtZ(track,Constants::zRICHEntWindow);
            TVector3 posrichex  = fTool->GetPositionAtZ(track,Constants::zRICHExtWindow);
            FillHisto("Tracks_step_NoRICHMultiMatched_ent",posrichent.X(),posrichent.Y());
            FillHisto("Tracks_step_NoRICHMultiMatched_ex",posrichex.X(),posrichex.Y());
        }

        if(!isrich)
            FillHisto("Tracks_step_IsRICH_ptrack",track->GetMomentum());

        // if(!GoodLKrCandidate(iC,track)) continue;
        if(!GoodLKrCandidate(iC,track,(CalorimeterCluster*)CaloArray[iC])) continue;

        FillHisto("Tracks_step_IsLKr_ptrack",track->GetMomentum());
        FillHisto("OneTrack_step_Q_vs_IsRSingle",(Double_t)fDownstreamTrack.GetRICHSingleIsCandidate(),fDownstreamCandidate[iC].GetCharge());
        FillHisto("OneTrack_step_Q_vs_IsRMulti",(Double_t)fDownstreamTrack.GetRICHMultiIsCandidate(),fDownstreamCandidate[iC].GetCharge());
        //Build DownstreamTime
        MakeDownstreamTime(iC,track,isrich);

        if(!fMUV1Candidate[iC].GetIsMUVCandidate()){

            TVector3 posmuv1 = fTool->GetPositionAtZ(track,Constants::zMUV1);
            FillHisto("Tracks_step_NoMUV1Matched",posmuv1.X(),posmuv1.Y());

        }else {

            FillHisto("Tracks_step_HasMUV1_ptrack",track->GetMomentum());
        }

        if(!fMUV2Candidate[iC].GetIsMUVCandidate()){
            //if(!CaloArray[iC]->IsMUV2Associated()){
            TVector3 posmuv2 = fTool->GetPositionAtZ(track,Constants::zMUV2);
            FillHisto("Tracks_step_NoMUV2Matched",posmuv2.X(),posmuv2.Y());
            //continue;
        }else {
            FillHisto("Tracks_step_HasMUV2_ptrack",track->GetMomentum());
        }


        TVector3 posmuv3 = fTool->GetPositionAtZ(track,Constants::zMUV3);
        if(GoodMUV3Candidate(iC,track)) {
            FillHisto("Tracks_MUV3Matched",posmuv3.X(),posmuv3.Y());
            fDownstreamTrack.SetMUV3ID(0);
        }else{
            FillHisto("Tracks_NoMUV3Matched",posmuv3.X(),posmuv3.Y());
            FillHisto("Tracks_step_MUV3_rejection_ptrack",track->GetMomentum());
        }

        if(GoodStandardMUV3Candidate(iC,track)) {
            FillHisto("Tracks_standard_MUV3Matched",posmuv3.X(),posmuv3.Y());
            fDownstreamTrack.SetMUV3PosID(0);
        }else {
            FillHisto("Tracks_standard_NoMUV3Matched",posmuv3.X(),posmuv3.Y());
            FillHisto("Tracks_step_standard_MUV3_rejection_ptrack",track->GetMomentum());
        }

        BuildCalorimetricEnergy(iC, (CalorimeterCluster*)CaloArray[iC]);

        // Trigger time condition: consistency between track selected and trigger
        Double_t dtimetrigchod = fFineTriggerTime-fDownstreamTrack.GetCHODTime();
        //Double_t dtimetrigchod = fFineTriggerTime-fDownstreamTrack.GetRICHSingleTime();
        FillHisto("Tracks_dtimetrigchod",fSpectrometerEvent->GetNCandidates(),dtimetrigchod);
        if (fabs(dtimetrigchod)>25) continue;
        FillHisto("Tracks_step_final",track->GetMomentum());

        // Store good track
        if (fNGoodTracks>9) continue; // protection against crowded events
        fDownstreamTrack.SetTrackID(iC);
        fDownstreamCandidate[fNGoodTracks] = fDownstreamTrack;
        fDownstreamTrack.Clear();
        fNGoodTracks++;
    }//end iC track loop

    if (!fNGoodTracks) return 0;
    if (fNGoodTracks>=9) return 0;


    return 1;
}

Bool_t MainAnalysis::SelectOneTrackEvent(){


    Bool_t goodEvent = 0;
    for (Int_t iC=0; iC<fNGoodTracks; iC++) {

        ClearUpstreamTrack();

        // KTAG candidate
        Bool_t isCedarCandidate = GoodCedarCandidate(fGTKFlag,iC); // GoodCedarCandidate(fGTKFlag,jTrack) if called after GTK reco
        FillHisto("OneTrack_step_Q_vs_IsCedarMatched",(Double_t)isCedarCandidate,fDownstreamCandidate[iC].GetCharge());
        // GTK matching candidate

        //For now always match wrt CHOD
        Int_t isGTKCandidate = MatchUpstreamKaon(iC); // Match kaon using gtk or nominal kaon momentum
        FillHisto("OneTrack_step_Q_vs_IsGTKMatched",(Double_t)isGTKCandidate,fDownstreamCandidate[iC].GetCharge());

        // CHANTI candidates
        Bool_t isCHANTICandidate = GoodCHANTICandidate(isGTKCandidate,iC); // SelectCHANTICandidate(fGTKFlag,iC) if called after GTK reco
        //
        FillHisto("OneTrack_step_Q_vs_IsCHANTIMatched",(Double_t)isCHANTICandidate,fDownstreamCandidate[iC].GetCharge());
        FillHisto("OneTrack_step_Q_vs_AIsRSingle",(Double_t)fDownstreamCandidate[iC].GetRICHSingleIsCandidate(),fDownstreamCandidate[iC].GetCharge());
        FillHisto("OneTrack_step_Q_vs_AIsRMulti",(Double_t)fDownstreamCandidate[iC].GetRICHMultiIsCandidate(),fDownstreamCandidate[iC].GetCharge());
        //// Extra LKr, LAV, IRC, SAC
        Bool_t isLKrExtraCandidate = GoodLKrExtraCandidate(iC);
        Bool_t isLAVCandidate = GoodLAVCandidate(iC);
        Bool_t isIRCCandidate = GoodIRCCandidate(iC);
        Bool_t isSACCandidate = GoodSACCandidate(iC);
        Bool_t isSAVCandidate = GoodSAVCandidate(iC);

        Bool_t isLKrExtraNormal = fDownstreamCandidate[iC].GetIsPhotonLKrCandidate();
        Bool_t isLKrExtraAux    = fDownstreamCandidate[iC].GetIsPhotonNewLKrCandidate();

        // if(fDownstreamCandidate[iC].GetMomentum().P() > 71 && fNBeamPions < 9){
        if(!isCedarCandidate){
            if(fNBeamPions > 9) continue;
            //if(!isGTKCandidate) continue;

            FillHisto("BeamPion_p_vs_zvtx",fVertex.Z(),fDownstreamCandidate[iC].GetMomentum().P());
            FillHisto("BeamPion_richradius_vs_p",fDownstreamCandidate[iC].GetMomentum().P(), fDownstreamCandidate[iC].GetRICHSingleRadius());

            if(fVertex.Z() < 102800 && fVertex.Z() > 102000 ){
                FillHisto("BeamPion_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);

                if (fDownstreamCandidate[iC].GetMUV3ID()==-1)
                    FillHisto("BeamPion_mmiss_vs_p_nomuv3",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                if(isGTKCandidate)
                    FillHisto("BeamPion_mmiss_vs_p_isgtk",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                if (isGTKCandidate && fDownstreamCandidate[iC].GetMUV3ID()==-1)
                    FillHisto("BeamPion_mmiss_vs_p_isgtk_nomuv3",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);

                if (isGTKCandidate && fDownstreamCandidate[iC].GetMUV3ID()==-1 && !isSACCandidate && !isIRCCandidate && !isSAVCandidate && !isLAVCandidate)
                    FillHisto("BeamPion_mmiss_vs_p_isgtk_nomuv3lavircsac",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);

                if (isGTKCandidate && fDownstreamCandidate[iC].GetMUV3ID()==-1 && !isSACCandidate && !isIRCCandidate && !isSAVCandidate && !isLAVCandidate &&fDownstreamCandidate[iC].GetRICHMultiRadius() > 160){
                    FillHisto("BeamPion_mmiss_vs_p_isgtk_nomuv3norichkaonlavircsac",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);


                }



            }

            fBeamPionDownstreamCandidate[fNBeamPions] = fDownstreamCandidate[iC];
            fBeamPionUpstreamCandidate[fNBeamPions] = fUpstreamTrack;
            fNBeamPions++;
            goodEvent = 1;
            continue; // skip event for the main Pinunu selection if Cedar is not present
        }


        // if(!GetWithMC() && !isGTKCandidate) continue;
        if( !isGTKCandidate) continue;
        FillHisto("OneTrack_fgtk_mm2_vs_p",fDownstreamCandidate[iC].GetMomentum().P(), fM2Miss);
        if(fUpstreamTrack.GetRICHDiscriminant()>=0.01)
            FillHisto("OneTrack_sgtk_mm2_vs_p",fDownstreamCandidate[iC].GetMomentum().P(), fM2Miss);
        if(fUpstreamTrack.GetCHODDiscriminant()>=0.005)
            FillHisto("OneTrack_tgtk_mm2_vs_p",fDownstreamCandidate[iC].GetMomentum().P(), fM2Miss);

        // Correct pion and kaon momentum for blue field (vertex already corrected for in the GTK routine)
        BlueFieldMomentumCorrection(iC);


        if(fNGoodKaonTracks> 9) continue;

        goodEvent = 1;
        fKaonEventDownstreamCandidate[fNGoodKaonTracks] = fDownstreamCandidate[iC];
        fKaonEventUpstreamCandidate[fNGoodKaonTracks] = fUpstreamTrack;
        fNGoodKaonTracks++;

        //Plot only for control trigger
        if(fControlTrigger || GetWithMC()) {



            FillHisto("OneTrack_mm2_vs_p",fDownstreamCandidate[iC].GetMomentum().P(), fM2Miss);
            FillHisto("OneTrack_p_vs_zvtx", fVertex.Z(), fDownstreamCandidate[iC].GetMomentum().P());

            if(fDownstreamCandidate[iC].GetIsLAVTrack())
                FillHisto("OneTrack_mm2_vs_p_lavtrack",fDownstreamCandidate[iC].GetMomentum().P(), fM2Miss);
            if(!fDownstreamCandidate[iC].GetIsLAVTrack())
                FillHisto("OneTrack_mm2_vs_p_nolavtrack",fDownstreamCandidate[iC].GetMomentum().P(), fM2Miss);
            if(!isLAVCandidate)
                FillHisto("OneTrack_mm2_vs_p_nolavatall",fDownstreamCandidate[iC].GetMomentum().P(), fM2Miss);


            // Some test
            //TRecoSpectrometerCandidate *track = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fDownstreamCandidate[iC].GetTrackID());
            Double_t xmuv3 = fDownstreamCandidate[iC].GetPositionAtMUV(2).X();
            Double_t ymuv3 = fDownstreamCandidate[iC].GetPositionAtMUV(2).Y();
            if (fDownstreamCandidate[iC].GetMUV3ID()==-1) { // pion
                FillHisto("OneTrack_kaon_pion_zvtxvsp",fVertex.Z(),fDownstreamCandidate[iC].GetMomentum().P());
                FillHisto("OneTrack_kaon_pion_mmissvsp",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                FillHisto("OneTrack_kaon_pion_xymuv3",xmuv3,ymuv3);
                if (fDownstreamCandidate[iC].GetLKrEovP()<0.8) FillHisto("OneTrack_kaon_pionnoe_mmissvsp",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            }
            if (fDownstreamCandidate[iC].GetMUV3ID()==0) { // muon
                FillHisto("OneTrack_kaon_muon_zvtxvsp",fVertex.Z(),fDownstreamCandidate[iC].GetMomentum().P());
                FillHisto("OneTrack_kaon_muon_mmissvsp",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                FillHisto("OneTrack_kaon_muon_xymuv3",xmuv3,ymuv3);
                Double_t m2nu = (fUpstreamTrack.GetMomentum()-fDownstreamCandidate[iC].GetMuonMomentum()).Mag2();
                FillHisto("OneTrack_kaon_muon_mmissmuvsp",fDownstreamCandidate[iC].GetMomentum().P(),m2nu);
            }

            if (fUpstreamTrack.GetIsCHANTICandidate()) FillHisto("OneTrack_kaon_chanti_p_vs_zvtx",fVertex.Z(),fDownstreamCandidate[iC].GetMomentum().P());
            else FillHisto("OneTrack_kaon_nochanti_p_vs_zvtx",fVertex.Z(),fDownstreamCandidate[iC].GetMomentum().P());
            if (isLKrExtraCandidate) FillHisto("OneTrack_kaon_lkrextra_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            else FillHisto("OneTrack_kaon_nolkrextra_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (!isLKrExtraNormal) FillHisto("OneTrack_kaon_nolkrextranorm_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (!isLKrExtraNormal && isLKrExtraAux) FillHisto("OneTrack_kaon_nolkrextraaux_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (!isLKrExtraAux && !isLKrExtraNormal) FillHisto("OneTrack_kaon_nolkrextraboth_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (isLAVCandidate) FillHisto("OneTrack_kaon_lav_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (!isLKrExtraCandidate && !isLAVCandidate) FillHisto("OneTrack_kaon_nolkrextranolav_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (isIRCCandidate) FillHisto("OneTrack_kaon_irc_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (!isLKrExtraCandidate && !isLAVCandidate && !isIRCCandidate) FillHisto("OneTrack_kaon_nolkrextranolavnoirc_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (isSACCandidate) FillHisto("OneTrack_kaon_sac_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
            if (!isLKrExtraCandidate && !isLAVCandidate && !isIRCCandidate && !isSACCandidate) FillHisto("OneTrack_kaon_nolkrextranolavnoircnosac_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);

            if (!isLKrExtraCandidate && !isLAVCandidate && !isIRCCandidate && !isSACCandidate && !isCHANTICandidate){
                FillHisto("OneTrack_kaon_nolkrextranolavnoircnosacnochanti_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                if(!isSAVCandidate){
                    FillHisto("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosav_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                    if( fDownstreamCandidate[iC].GetLKrEovP() < 0.8){
                        FillHisto("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectron_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                        if(fDownstreamCandidate[iC].GetMUV3ID()== -1){
                            FillHisto("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                            if(fDownstreamCandidate[iC].GetRICHMultiIsCandidate()){

                                FillHisto("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3wrich_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                                //if(fDownstreamCandidate[iC].GetRICHMultiIsCandidateMomRange()){

                                if(fDownstreamCandidate[iC].GetRICHMultiMostLikelyHypothesis()==3)
                                    FillHisto("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3wrichpi_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                                if(fDownstreamCandidate[iC].GetRICHMultiMostLikelyHypothesis()==2)
                                    FillHisto("OneTrack_kaon_nolkrextranolavnoircnosacnochantinosavnoelectronnomuv3wrichmu_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                            }

                        }
                    }
                }
            }

            if (!isSACCandidate && !isIRCCandidate && !isLAVCandidate && !isCHANTICandidate && fDownstreamCandidate[iC].GetMUV3ID()== -1 ){

                FillHisto("OneTrack_kaon_pion_nosacnoircnolav_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);
                if( fDownstreamCandidate[iC].GetLKrEovP() < 0.8){
                    FillHisto("OneTrack_kaon_pion_nosacnoircnolav_noelectrons_mmiss_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fM2Miss);

                }
            }


        }//control trigger
    }//NGoodTracks loop

    return goodEvent;
}

//Bool_t MainAnalysis::SelectThreeTrackEvent(){
//
//  if(fNK3piTracks < 2 || fNK3piTracks > 3) return false;
//
//  TLorentzVector p1;
//  TLorentzVector p2;
//  TLorentzVector p3;
//  TLorentzVector pk;
//
//  TLorentzVector k4momgev;
//  Double_t getvpk = 74.9;
//  Double_t getvnorm = 1 / (1 + 0.00122*0.00122 + 0.000026*0.000026);
//  Double_t getvpx = getvpk*0.00122*getvnorm;
//  Double_t getvpy = getvpk*0.000026*getvnorm;
//  Double_t getvpz = getvpk*getvnorm;
//
//  k4momgev.SetXYZM(getvpx,getvpy,getvpz,0.493667);
//
//  if(fNK3piTracks==2){
//
//   p1 = fDownstreamCandidate[0].GetMomentum();
//   p2 = fDownstreamCandidate[1].GetMomentum();
//   p3 = k4momgev - p1 - p2;
//
//   FillHisto("TwoTracks_nstrawtracks",fNTracks);
//   FillHisto("TwoTracks_mmiss",p3.M2());
//
//  }
//
//  if(fNK3piTracks==3){
//
//    p1 = fDownstreamCandidate[0].GetMomentum();
//    p2 = fDownstreamCandidate[1].GetMomentum();
//    p3 = fDownstreamCandidate[2].GetMomentum();
//    pk = p1 + p2 + p3;
//
//    FillHisto("ThreeTracks_nstrawtracks",fNTracks);
//    FillHisto("ThreeTracks_mk", pk.M());
//
//  }
//
//
//
//
//  return true;
//
//}
Int_t MainAnalysis::GoodCedarCandidate(Int_t NoGTKflag,Int_t iC){
    fCedarAnal->Clear();
    fCedarAnal->SetEvent(fCedarEvent);
    Double_t timeref = fDownstreamCandidate[iC].GetDownstreamTime();
    //Double_t timeref = fDownstreamCandidate[iC].GetRICHSingleTime();
    //Double_t timeref = fDownstreamTrack.GetCHODTime();
    //int cV = fDownstreamCandidate[jTrack].GetCHODV();
    //int cH = fDownstreamCandidate[jTrack].GetCHODH();
    //int chid = 16*cV+((cH-64)%16);
    //cout << "chodtime = " <<fDownstreamCandidate[iC].GetCHODTime() << "richtime = " << fDownstreamCandidate[iC].GetRICHSingleTime() << endl;
    Bool_t isKTAGCandidate = (Bool_t)fCedarAnal->TrackMatching(NoGTKflag,timeref,fUpstreamTrack.GetTime(),GetEventHeader()->GetTimeStamp(),fIsMC);
    KTAGCandidate *kaon = fCedarAnal->GetKTAGCandidate();

    fCHODAnal->FindMultiplicity(fDownstreamCandidate[iC]);

    fUpstreamTrack.SetIsKTAGCandidate(isKTAGCandidate);

    if (!isKTAGCandidate) {

        FillHisto("OneTrack_IsNotKTAG_dtktag_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),kaon->GetTime()-fDownstreamCandidate[iC].GetDownstreamTime());
        //return 0;
    }

    fUpstreamTrack.SetIsKTAGCandidate(kaon->GetIsKTAGCandidate());
    fUpstreamTrack.SetKTAGID(kaon->GetID());
    fUpstreamTrack.SetKTAGTime(kaon->GetTime());

    if(!fIsMC){

        //Fill cedar time in case wout of time events are needed
        // if (fabs(kaon->GetTime()-fDownstreamCandidate[iC].GetRICHSingleTime()) > 0.4) return 0;  // New cut instead of -2,4 ns (11/10/2016)
        if (fabs(kaon->GetTime()-fDownstreamCandidate[iC].GetDownstreamTime()) <= 1.0)
            fUpstreamTrack.SetIsGoodKTAG(1);
        else
            return 0;  // New cut instead of -2,4 ns (11/10/2016)

    } else {
        // if (fabs(kaon->GetTime()-fDownstreamCandidate[iC].GetRICHSingleTime())>2) return 0;  // New cut instead of -2,4 ns (11/10/2016)
        if (fabs(kaon->GetTime()-fDownstreamCandidate[iC].GetDownstreamTime())<=2)
            fUpstreamTrack.SetIsGoodKTAG(1);
        else
            return 0;  // New cut instead of -2,4 ns (11/10/2016)

    }

    int cV=fDownstreamCandidate[iC].GetCounterV();
    int cH=fDownstreamCandidate[iC].GetCounterH();
    double chodch1=16*cV+((int)(cH-64)%16);
    double chodch2=16*cH+(int)cV%16;
    double chodVdt=fUpstreamTrack.GetKTAGTime()-fDownstreamCandidate[iC].GetTimeV();
    double chodHdt=fUpstreamTrack.GetKTAGTime()-fDownstreamCandidate[iC].GetTimeH();
    double choddt =fUpstreamTrack.GetKTAGTime()-fDownstreamCandidate[iC].GetCHODTime();;
    FillHisto("OneTrack_stability_dtktag_vs_chodchannel",chodch1,chodVdt);
    FillHisto("OneTrack_stability_dtktag_vs_chodchannel",chodch2,chodHdt);
    FillHisto("OneTrack_stability_dtktag",choddt);
    FillHisto("OneTrack_IsKTAG_dtktag_vs_p",fDownstreamCandidate[iC].GetMomentum().P(),fUpstreamTrack.GetKTAGTime()-fDownstreamCandidate[iC].GetDownstreamTime());
    //FillHisto("OneTrack_dtkcvsdtgc",fUpstreamTrack.GetTime()-fDownstreamCandidate[iC].GetCHODTime(),fUpstreamTrack.GetKTAGTime()-fDownstreamCandidate[iC].GetDownstreamTime());
    return 1;

}

Int_t MainAnalysis::GoodCHANTICandidate(Int_t IsGTKflag,Int_t iC){
    fCHANTIAnal->SetEvent(fCHANTIEvent);
    fCHANTIAnal->Clear();
    Double_t reftime = fDownstreamCandidate[iC].GetDownstreamTime();
    // Double_t reftime = fDownstreamCandidate[iC].GetCHODTime();
    // Double_t reftime = fDownstreamCandidate[iC].GetRICHSingleTime();
    Double_t timegtk = IsGTKflag ? fUpstreamTrack.GetTime() : -999999;
    Double_t timektg = fUpstreamTrack.GetIsKTAGCandidate()>=0 ? fUpstreamTrack.GetKTAGTime() : -999999.;
    //Double_t reftime = NoGTKflag ? fDownstreamCandidate[iC].GetCHODTime() : fUpstreamTrack.GetKTAGTime();
    Bool_t isCHANTICandidate = fCHANTIAnal->TrackMatching(IsGTKflag,reftime,timegtk,timektg);
    if (!isCHANTICandidate) return 0;
    CHANTICandidate *chanti = fCHANTIAnal->GetCHANTICandidate();
    fUpstreamTrack.SetIsCHANTICandidate(chanti->GetIsCHANTICandidate());
    fUpstreamTrack.SetCHANTIID(chanti->GetID());
    fUpstreamTrack.SetCHANTITime(chanti->GetTime());
    FillHisto("OneTrack_IsCHANTI_p_vs_zvtx",fVertex.Z(),fDownstreamCandidate[iC].GetMomentum().P());
    return 1;

}
Int_t MainAnalysis::MatchUpstreamKaon(Int_t iC){

    Int_t goodgtk = 0;
    Int_t refdetector = 0; // -- 1 for chod , 0 for cedar

    goodgtk = GoodGTKCandidate(refdetector,iC);



    return goodgtk;
}

Int_t MainAnalysis::GoodGTKCandidate(Int_t chodorcedar, Int_t iC){


    //double bestdt= -999;
    // double reftime = chodorcedar ? fDownstreamCandidate[iC].GetCHODTime() : fUpstreamTrack.GetKTAGTime();
    double reftime = chodorcedar ? fDownstreamCandidate[iC].GetRICHSingleTime() : fUpstreamTrack.GetKTAGTime();
    //KTAGCandidate *kaon = fCedarAnal->GetKTAGCandidate();
    TRecoSpectrometerCandidate* track =(TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(fDownstreamCandidate[iC].GetTrackID());
    int nGoodTrack = - 1;
    int bestgtk    = - 1;

    fdiscr_2 = 0;
    fdiscr_3 = 0;
    //cout << "NHits = " << fCedarEvent->GetNHits() << " NCandidates = " << fCedarEvent->GetNCandidates() << endl;

    if(!fGTKFlag){

        fGigaTrackerAnalysis->SetTimeShift(0.);
        // fGigaTrackerAnalysis->SetRecoTimingCut(1.5);//chod
        fGigaTrackerAnalysis->SetRecoTimingCut(1.2);
        //fGigaTrackerAnalysis->SetMatchingTimingCut(-0.7,0.7);//chod
        fGigaTrackerAnalysis->SetMatchingTimingCut(-0.6,0.6);

        if(!fIsMC){

            if(fIsFilter)
                nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,reftime,GetEventHeader()->GetTimeStamp(),1); // Remove previous candidates !
            // nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,fDownstreamCandidate[iC].GetCHODTime(),1); // Remove previous candidates !
            if(!fIsFilter)
                nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,reftime,GetEventHeader()->GetTimeStamp(),1); // Remove previous candidates !
        } else {
            //cout <<GetStreamInfo()->GetMCInfo().GetRevision() << endl;
            if(GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.1") ||GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.2") ){
                //nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,reftime,GetEventHeader()->GetTimeStamp(),0); // Do not remove previous candidates !
                nGoodTrack= fGigaTrackerEvent->GetNCandidates();
            }else {
                nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,reftime,GetEventHeader()->GetTimeStamp(),1); // Remove previous candidates !
            }

        }
        // nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,fDownstreamCandidate[iC].GetCHODTime(),1); // Remove previous candidates !
        //nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,fUpstreamTrack.GetKTAGTime(),1); // Remove previous candidates !

        // Find and test the best candidate for one track <--------------------- !!!!!!!!!!
        // bestgtk = fGigaTrackerAnalysis->TrackCandidateMatching(fGigaTrackerEvent,track,fDownstreamCandidate[iC].GetCHODTime(),0,nGoodTrack);
        bestgtk = fGigaTrackerAnalysis->TrackCandidateMatching(fGigaTrackerEvent,track,reftime,0,nGoodTrack);
        //cout << "before bestgtk = " << bestgtk << "ngtkcand = " << fGigaTrackerEvent->GetNCandidates() << endl;
        //bestgtk = fGigaTrackerAnalysis->TrackCandidateMatching(fGigaTrackerEvent,track,fUpstreamTrack.GetKTAGTime(),0,nGoodTrack);
    } else {

        bestgtk = fGigaTrackerAnalysis->ReconstructCandidateNoGTK(fGigaTrackerEvent, track); // Remove previous candidates !

        GigaTrackerCandidate *gtkcand = fGigaTrackerAnalysis->GetGigaTrackerCandidate(0);
        TVector3 vtx = gtkcand->GetVertex();
        //Double_t cda = gtkcand->GetCDA();
        //Double_t dt = gtkcand->GetTime()-reftime;
        Double_t discr = gtkcand->GetDiscriminant();

        //cout << f
        TLorentzVector k4mom;
        Double_t pk = 74.9*1000;
        Double_t norm = 1 / (1 + 0.00122*0.00122 + 0.000026*0.000026);
        Double_t px = pk*0.00122*norm;
        Double_t py = pk*0.000026*norm;
        Double_t pz = pk*norm;
        k4mom.SetXYZM(px,py,pz,0.493667);

        TLorentzVector k4momgev;
        Double_t getvpk = 74.9;
        Double_t getvnorm = 1 / (1 + 0.00122*0.00122 + 0.000026*0.000026);
        Double_t getvpx = getvpk*0.00122*getvnorm;
        Double_t getvpy = getvpk*0.000026*getvnorm;
        Double_t getvpz = getvpk*getvnorm;
        k4momgev.SetXYZM(getvpx,getvpy,getvpz,0.493667);

        TVector3 p3pion = track->GetThreeMomentumBeforeMagnet();
        TVector3 pospion = track->GetPositionBeforeMagnet();

        fcda = 9999999.;
        //// Vertex: first iteration without momentum correction for blue field
        fVertex = fTool->SingleTrackVertex(k4mom.Vect(),p3pion,gtkcand->GetPosition(),pospion,fcda); // First interation: no blue field

        fUpstreamTrack.SetMomentum(k4momgev); // Automatically co
        fUpstreamTrack.SetTime(gtkcand->GetTime());
        fUpstreamTrack.SetPosition(gtkcand->GetPosition());
        fUpstreamTrack.SetPosGTK1(gtkcand->GetPosGTK1());
        fUpstreamTrack.SetPosGTK2(gtkcand->GetPosGTK2());
        fUpstreamTrack.SetQuality(discr);
        fUpstreamTrack.SetType(gtkcand->GetType());
        fUpstreamTrack.SetVertex(fVertex);
        fUpstreamTrack.SetCDA(fcda);
        //fUpstreamTrack.SetChi2X(gtkcand->GetChi2));
        fUpstreamTrack.SetNGTKCandidates(fGigaTrackerAnalysis->GetNGigaTrackerCandidates());
        fM2Miss = (fUpstreamTrack.GetMomentum()-fDownstreamCandidate[iC].GetMomentum()).Mag2();

    }
    //cout << bestgtk << endl;
    //if(fGigaTrackerEvent->GetNCandidates() == 0) return 0;

    FillHisto("OneTrack_NoGTKCand_ptrack", fDownstreamCandidate[iC].GetMomentum().P());
    // if(!GetWithMC()){
    if(!fGTKFlag){
        //cout << "after bestgtk = " << bestgtk << "ngtkcand = " << fGigaTrackerEvent->GetNCandidates()  << endl;
        if(bestgtk<0) return 0;
        TRecoGigaTrackerCandidate* GTK = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(bestgtk);

        int type = GTK->GetType();
        double gtktime = GTK->GetTime();
        double gtkchi2X = GTK->GetChi2X();
        double gtkchi2Y = GTK->GetChi2Y();
        double gtkchi2T = GTK->GetChi2Time();
        double gtkchi2  = GTK->GetChi2();
        // double gtkchoddt=gtktime - fDownstreamCandidate[iC].GetCHODTime();
        double gtkrichdt ;
        double gtkcedardt;
        if(chodorcedar == 1){
            gtkrichdt  =gtktime - reftime;
            gtkcedardt =gtktime - fUpstreamTrack.GetKTAGTime();

        } else {
            gtkrichdt  =gtktime - reftime;
            gtkcedardt =gtktime - fDownstreamCandidate[iC].GetRICHSingleTime();

        }

        double gtkchoddt  =gtktime - fDownstreamCandidate[iC].GetCHODTime();
        TVector3 vertex;
        TVector3 gtkpos= GTK->GetPosition(2);
        TVector3 gtkpos1= GTK->GetPosition(0);
        TVector3 gtkpos2= GTK->GetPosition(1);
        TVector3 gtkmomentum= GTK->GetMomentum();
        TLorentzVector Pgtk(gtkmomentum, Constants::MKCH); //check if the mass is right (its MeV now)

        FillHisto("OneTrack_IsGTK_type", type);
        if(type < 100) return false;

        TVector3 p3pion = track->GetThreeMomentumBeforeMagnet();
        TVector3 pospion = track->GetPositionBeforeMagnet();

        Double_t discr_best = -1;
        Double_t discr_cedar = -1;
        Double_t discr_chod = -1;

        fcda = 9999999.;
        //// Vertex: first iteration without momentum correction for blue field
        // fVertex = fTool->SingleTrackVertex(gtkmomentum,p3pion,gtkpos,pospion,fcda); // First interation: no blue field
        fVertex = fGigaTrackerAnalysis->ComputeVertex(GTK,track,&fcda);

        //right now chodorcedar == 0 (Cedar if ref for reco)
        //RICH if chodorcedar == 0 and rich if == 1
        fGigaTrackerAnalysis->SetReferenceDetector(0);
        discr_cedar = fGigaTrackerAnalysis->Discriminant(fcda,gtkcedardt);

        //CHOD if chodorcedar == 0 and rich if == 1
        fGigaTrackerAnalysis->SetReferenceDetector(1);
        discr_chod  = fGigaTrackerAnalysis->Discriminant(fcda,gtkchoddt);

        //Cedar if chodorcedar == 0 and rich if == 1
        fGigaTrackerAnalysis->SetReferenceDetector(2);
        discr_best = fGigaTrackerAnalysis->Discriminant(fcda,gtkrichdt);

        //fGigaTrackerAnalysisCHOD->SetReferenceDetector(1);
        //fGigaTrackerAnalysisCHOD->DiscriminantNormalization();
        // discr_chod  = fGigaTrackerAnalysisCHOD->Discriminant(fcda,gtkchoddt);


        FillHisto("OneTrack_IsGTK_P", gtkmomentum.Mag());
        FillHisto("OneTrack_IsGTK_richdt",  gtkrichdt);
        FillHisto("OneTrack_IsGTK_choddt",  gtkchoddt);
        FillHisto("OneTrack_IsGTK_cedardt",  gtkcedardt);
        FillHisto("OneTrack_IsGTK_chi2X", gtkchi2X);
        FillHisto("OneTrack_IsGTK_chi2Y", gtkchi2Y);
        FillHisto("OneTrack_IsGTK_chi2T", gtkchi2T);
        FillHisto("OneTrack_IsGTK_chi2", gtkchi2);
        FillHisto("OneTrack_IsGTK_pos", gtkpos.X(),gtkpos.Y());

        double dx12 = gtkpos1.X() - gtkpos2.X();
        double dx23 = gtkpos2.X() - gtkpos.X() ;
        double dx13 = gtkpos1.X() - gtkpos.X() ;
        double dy12 = gtkpos1.Y() - gtkpos2.Y();
        double dy23 = gtkpos2.Y() - gtkpos.Y() ;
        double dy13 = gtkpos1.Y() - gtkpos.Y() ;
        double thetax= dx13/ (gtkpos1.Z() - gtkpos.Z());
        double thetay = dy13/ (gtkpos1.Z() - gtkpos.Z());
        FillHisto("OneTrack_IsGTK_dX_12",dx12 );
        FillHisto("OneTrack_IsGTK_dX_23",dx23 );
        FillHisto("OneTrack_IsGTK_dX_13",dx13 );
        FillHisto("OneTrack_IsGTK_dY_12",dy12 );
        FillHisto("OneTrack_IsGTK_dY_23",dy23 );
        FillHisto("OneTrack_IsGTK_dY_13",dy13 );
        FillHisto("OneTrack_IsGTK_thetax", thetax);
        FillHisto("OneTrack_IsGTK_thetay", thetay);
        FillHisto("OneTrack_IsGTK_discriminant",discr_best);
        FillHisto("OneTrack_IsGTK_dt_vs_cda",gtkrichdt, fcda);
        FillHisto("OneTrack_IsGTK_discriminant_chod",discr_chod);
        FillHisto("OneTrack_IsGTK_dtchod_vs_cda",gtkchoddt, fcda);
        FillHisto("OneTrack_IsGTK_discr_rich_vs_cedar",discr_cedar,discr_best);
        FillHisto("OneTrack_IsGTK_discriminant_cedar",discr_cedar);

        FillHisto("OneTrack_IsGTK_dtcedar_vs_cda",gtkcedardt, fcda);

        if(discr_best> 0.01 && discr_cedar>0.005 && fcda <= 7)
            fUpstreamTrack.SetIsGoodGTK(1);
        else
            fUpstreamTrack.SetIsGoodGTK(0);
        if(discr_best <= 0.01) return 0;

        FillHisto("OneTrack_IsGTK_dtcedar_vs_cda_good",gtkcedardt , fcda);
        FillHisto("OneTrack_IsGTK_dt_vs_cda_good",gtkrichdt, fcda);
        FillHisto("OneTrack_IsGTK_dtchod_vs_cda_good",gtkchoddt, fcda);

        fdiscr_2 = discr_cedar;
        fdiscr_3 = discr_chod;
        if(discr_cedar > 0.01){

            FillHisto("OneTrack_IsGTK_dtcedar_vs_cda_final",gtkcedardt, fcda);
            FillHisto("OneTrack_IsGTK_dt_vs_cda_final",gtkrichdt, fcda);

            if(discr_chod > 0.005)
                FillHisto("OneTrack_IsGTK_dtchod_vs_cda_final",gtkchoddt, fcda);

        }


        if(fcda > 7) return 0;
        StoreUpstreamParameters(iC, GTK, discr_best);
    }

    return 1;
}

void MainAnalysis::StoreUpstreamParameters(Int_t iTr, TRecoGigaTrackerCandidate* gtkcand, Double_t discr){

    TLorentzVector Pgtk;
    Pgtk.SetXYZM(gtkcand->GetMomentum().X()*0.001, gtkcand->GetMomentum().Y()*0.001, gtkcand->GetMomentum().Z()*0.001,Constants::MKCH*0.001); //check if the mass is right (its MeV now)
    fUpstreamTrack.SetMomentum(Pgtk); // Automatically co
    fUpstreamTrack.SetTime(gtkcand->GetTime());
    fUpstreamTrack.SetPosition(gtkcand->GetPosition(2));
    fUpstreamTrack.SetPosGTK1(gtkcand->GetPosition(0));
    fUpstreamTrack.SetPosGTK2(gtkcand->GetPosition(1));
    fUpstreamTrack.SetQuality(discr);
    fUpstreamTrack.SetType(gtkcand->GetType());
    fUpstreamTrack.SetVertex(fVertex);
    fUpstreamTrack.SetCDA(fcda);
    fUpstreamTrack.SetChi2X(gtkcand->GetChi2X());
    fUpstreamTrack.SetRICHDiscriminant(fdiscr_2);
    fUpstreamTrack.SetCHODDiscriminant(fdiscr_3);
    fUpstreamTrack.SetNGTKCandidates(fGigaTrackerAnalysis->GetNGigaTrackerCandidates());

    for (int jG(0); jG<fGigaTrackerAnalysis->GetNGigaTrackerCandidates(); jG++) { // last one is the best, not added here because already stored
        GigaTrackerCandidate *cand = fGigaTrackerAnalysis->GetGigaTrackerCandidate(jG);
        //cout << " cand = " << jG << " cda = " << cand->GetCDA() << " t = " << cand->GetTime() << " tktag = " << fUpstreamTrack.GetKTAGTime() << " d  = " << cand->GetDiscriminant()<< endl;
    }

    for (int jG(0); jG<fGigaTrackerAnalysis->GetNGigaTrackerCandidates()-1; jG++) { // last one is the best, not added here because already stored
        GigaTrackerCandidate *cand = fGigaTrackerAnalysis->GetGigaTrackerCandidate(jG);
        fUpstreamTrack.SetGTKCandidate(jG,*cand);


    }
    fUpstreamTrack.SetNGTK1Hits(fGigaTrackerAnalysis->GetNGigaTrackerGTK1Hits());
    int *gtk1hits = fGigaTrackerAnalysis->GetGigaTrackerGTK1Hit();
    for (int jH(0); jH<fGigaTrackerAnalysis->GetNGigaTrackerGTK1Hits(); jH++) fUpstreamTrack.SetGTK1Hit(jH,gtk1hits[jH]);
    fUpstreamTrack.SetNGTK2Hits(fGigaTrackerAnalysis->GetNGigaTrackerGTK2Hits());
    int *gtk2hits = fGigaTrackerAnalysis->GetGigaTrackerGTK2Hit();
    for (int jH(0); jH<fGigaTrackerAnalysis->GetNGigaTrackerGTK2Hits(); jH++) fUpstreamTrack.SetGTK2Hit(jH,gtk2hits[jH]);
    fUpstreamTrack.SetNGTK3Hits(fGigaTrackerAnalysis->GetNGigaTrackerGTK3Hits());
    int *gtk3hits = fGigaTrackerAnalysis->GetGigaTrackerGTK3Hit();
    for (int jH(0); jH<fGigaTrackerAnalysis->GetNGigaTrackerGTK3Hits(); jH++) fUpstreamTrack.SetGTK3Hit(jH,gtk3hits[jH]);

    // Common variables
    //fTrackVertex = *vertex;
    //fTrackCDA = cda;
    fM2Miss = (fUpstreamTrack.GetMomentum()-fDownstreamCandidate[iTr].GetMomentum()).Mag2();
}


void MainAnalysis::BlueFieldMomentumCorrection(int iC) {

    TRecoSpectrometerCandidate *track = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fDownstreamCandidate[iC].GetTrackID());

    // Pion
    TVector3 pmom = fDownstreamCandidate[iC].GetMomentum().Vect();
    TVector3 pini = fDownstreamCandidate[iC].GetPosition();
    TVector3 vertexcorr = fTool->BlueFieldCorrection(&pmom,pini,track->GetCharge(),fUpstreamTrack.GetVertex().Z());
    TLorentzVector ppion;
    ppion.SetVectM(pmom,0.001*MPI);
    fDownstreamCandidate[iC].SetMomentum(ppion);

    // Kaon
    pmom = fUpstreamTrack.GetMomentum().Vect();
    pini = fUpstreamTrack.GetPosition();
    vertexcorr = fTool->BlueFieldCorrection(&pmom,pini,1,fUpstreamTrack.GetVertex().Z());
    TLorentzVector pkaon;
    pkaon.SetVectM(pmom,0.001*MKCH);
    fUpstreamTrack.SetMomentum(pkaon);

}

Int_t MainAnalysis::GoodCHODCandidate(Int_t iC,TRecoSpectrometerCandidate* track){

    TVector3 poschod = fTool->GetPositionAtZ(track,Constants::zCHOD);
    if(!fCHODCandidate[iC].GetIsCHODCandidate()){
        FillHisto("Tracks_IsCHOD_rejected",poschod.X(),poschod.Y());
        return 0;
    }
    FillHisto("Tracks_IsCHOD_chodminchi2",fCHODCandidate[iC].GetDiscriminant());
    if (fCHODCandidate[iC].GetDiscriminant()>15){
        FillHisto("Tracks_CHODChi2_rejected",poschod.X(),poschod.Y());
        return 0;
    }
    // TVector3 poschod[2]; for (Int_t jdet=0; jdet<2; jdet++) posAtCHOD[jdet] = tools->GetPositionAtZ(track,fZCHOD[jdet]);
    Double_t dx = fCHODCandidate[iC].GetX()-poschod.X();
    Double_t dy = fCHODCandidate[iC].GetY()-poschod.Y();
    Double_t mindist = sqrt(dx*dx+dy*dy);
    Double_t dtime = fCHODCandidate[iC].GetDeltaTime();
    FillHisto("Tracks_CHODMatch_dist_vs_mintime",dtime,mindist);
    FillHisto("Tracks_CHODMatch_dy_vs_dx",dx,dy);

    //Complicated time shift
    fDownstreamTrack.SetCHODTime(fCHODCandidate[iC].GetDeltaTime()+track->GetTime());
    fDownstreamTrack.SetCHODPosition(fCHODCandidate[iC].GetX(),fCHODCandidate[iC].GetY(),Constants::zCHOD);
    fDownstreamTrack.SetCounterV(fCHODCandidate[iC].GetCounterV());
    fDownstreamTrack.SetCounterH(fCHODCandidate[iC].GetCounterH());
    fDownstreamTrack.SetTimeV(fCHODCandidate[iC].GetTimeV());
    fDownstreamTrack.SetTimeH(fCHODCandidate[iC].GetTimeH());
    fDownstreamTrack.SetCHODRecoHitVID(fCHODCandidate[iC].GetRecoHitVID());
    fDownstreamTrack.SetCHODRecoHitHID(fCHODCandidate[iC].GetRecoHitHID());
    fDownstreamTrack.SetIsGoodCHOD(1);


    return 1;
}

Int_t MainAnalysis::GoodNewCHODCandidate(Int_t iC,TRecoSpectrometerCandidate* track){

    TVector3 poschod = fTool->GetPositionAtZ(track,Constants::zNewCHOD);

    if(!fNewCHODCandidate[iC].GetIsNewCHODCandidate()){
        FillHisto("Tracks_IsNewCHOD_rejected",poschod.X(),poschod.Y());
        return 0;
    }
    FillHisto("Tracks_IsNewCHOD_chodminchi2",fNewCHODCandidate[iC].GetDiscriminant());
    if (fNewCHODCandidate[iC].GetDiscriminant()>10){
        FillHisto("Tracks_NewCHODChi2_rejected",poschod.X(),poschod.Y());
        return 0;
    }
    // TVector3 poschod[2]; for (Int_t jdet=0; jdet<2; jdet++) posAtNewCHOD[jdet] = tools->GetPositionAtZ(track,fZNewCHOD[jdet]);
    Double_t imin = fNewCHODCandidate[iC].GetRecoHitID();
    Double_t dx = fNewCHODCandidate[iC].GetX()-poschod.X();
    Double_t dy = fNewCHODCandidate[iC].GetY()-poschod.Y();
    Double_t mindist = sqrt(dx*dx+dy*dy);
    Double_t dtime = fNewCHODCandidate[iC].GetDeltaTime();
    Double_t tnch  = fNewCHODCandidate[iC].GetDeltaTime() + track->GetTime();
    Double_t dtchod = fDownstreamTrack.GetCHODTime() - tnch;

    FillHisto("Tracks_NewCHODMatch_dist_vs_mintime",dtime,mindist);
    FillHisto("Tracks_NewCHODMatch_dist_vs_mindtchod",dtchod,mindist);
    FillHisto("Tracks_NewCHODMatch_dy_vs_dx",dx,dy);

    if(fabs(dtchod) > 5 ) return 0;

    fDownstreamTrack.SetNewCHODTime(fNewCHODCandidate[iC].GetDeltaTime()+track->GetTime());
    fDownstreamTrack.SetNewCHODPosition(fNewCHODCandidate[iC].GetX(),fNewCHODCandidate[iC].GetY(),Constants::zNewCHOD);
    fDownstreamTrack.SetNewCHODRecoHitID(imin);
    fDownstreamTrack.SetIsGoodNewCHOD(1);
    return 1;
}

Int_t MainAnalysis::GoodLKrCandidate(Int_t iC,TRecoSpectrometerCandidate* track,CalorimeterCluster* clus){

    if(!fLKrClusCandidate[iC].GetIsLKrCandidate()){
        TVector3 poslkr = fTool->GetPositionAtZ(track,Constants::zLKr);
        FillHisto("Tracks_IsLKrCl_rejected",poslkr.X(),poslkr.Y());
        //continue;
    }

    if(!fLKrCellCandidate[iC].GetIsLKrCandidate()){
        TVector3 poslkr = fTool->GetPositionAtZ(track,Constants::zLKr);
        FillHisto("Tracks_IsLKrCell_rejected",poslkr.X(),poslkr.Y());
        fDownstreamTrack.SetIsGoodLKr(0);
    }

    //CalorimeterCluster* clus = (CalorimeterCluster*)fCaloArray[iC];
    if(!clus->IsLKrAssociated()){
        TVector3 poslkr = fTool->GetPositionAtZ(track,Constants::zLKr);
        FillHisto("Tracks_IsLKrRA_rejected",poslkr.X(),poslkr.Y());
    }


    if(!fLKrCellCandidate[iC].GetIsLKrCandidate() && !fLKrClusCandidate[iC].GetIsLKrCandidate()) {
        return 0;
        fDownstreamTrack.SetIsGoodLKr(0);
    }

    if(!fLKrCellCandidate[iC].GetIsLKrCandidate()) return 0;
    if(!clus->IsLKrAssociated())  return 0;

    FillHisto("Tracks_NLKrHits", fLKrEvent->GetNHits());

    Bool_t isLKrMatched = fLKrCellCandidate[iC].GetIsLKrCandidate() ? 1 : 0;
    Double_t lkrtime = 99999999.;
    Double_t dist = 99999999.;
    Double_t dtime = 99999999.;
    Double_t dtimechod = 99999999.;
    Double_t dtimerich = 99999999.;
    Double_t energy = 0;
    Double_t seedenergy = 0;
    Int_t ncells = 0;
    Double_t dx = -99999.;
    Double_t dy = -99999.;
    TVector3 posAtLKr = fTool->GetPositionAtZ(track,Constants::zLKr);
    Double_t chodtime  = fCHODCandidate[iC].GetDeltaTime()+track->GetTime();
    Double_t richtime  = fRICHCandidate[iC].GetSingleRingTime();

    TRecoLKrCandidate* lkr = (TRecoLKrCandidate*)clus->GetLKrCandidate();
    if(fIsMC)
        lkrtime    = clus->GetLKrTime()-1.1;
    else
        lkrtime    = clus->GetLKrTime();
    dtime      = lkrtime - track->GetTime();
    energy     = clus->GetLKrEnergy()*0.001;
    seedenergy = lkr->GetClusterSeedEnergy()*0.001;
    dx         = lkr->GetClusterX()-posAtLKr.X();
    dy         = lkr->GetClusterY()-posAtLKr.Y();
    dist       = TMath::Sqrt(dx*dx + dy*dy);
    ncells     = lkr->GetNCells();

    //if(fIsMC){
    //    dtimechod  = lkrtime-chodtime-1.1;//1.1ns offset in MC
    //    dtimerich  = lkrtime-richtime-1.1;
    //} else {
    dtimechod  = lkrtime-chodtime;
    dtimerich  = lkrtime-richtime;
    //}

    FillHisto("Tracks_IsLKr_mindist_vs_dttrack",dtime,dist);
    if (dist>=100.) isLKrMatched = 0; // protection against possible bugs in LKrAnalysis
    if (fabs(dtime)>20.) isLKrMatched = 0;

    if (!isLKrMatched) return 0;
    FillHisto("Tracks_IsLKr_mindist_vs_dtchod",dtimechod,dist);
    FillHisto("Tracks_IsLKr_mindist_vs_dtrich",dtimerich,dist);

    if (fYear==2016 && fabs(dtimechod) > 6) return 0;
    if (fYear==2016 && energy<0)  return 0;

    FillHisto("Tracks_IsLKr_energy_vs_dist",dist,energy);
    FillHisto("Tracks_IsLKr_energy_vs_dist_zoom",dist,energy);
    FillHisto("Tracks_IsLKr_mindx_vs_mindy",dx,dy);

    fDownstreamTrack.SetLKrTime(lkrtime);

    Double_t pmag = track->GetMomentum()*Constants::fmevtogev;
    //Double_t pmag = fDownstreamTrack.GetMomentum().P();
    FillHisto("Tracks_IsLKr_eovp_vs_ptrack",pmag,energy/pmag);
    if(fLKrClusCandidate[iC].GetIsLKrCandidate())
        fDownstreamTrack.SetLKrID(fLKrClusCandidate[iC].GetClusterID());
    fDownstreamTrack.SetLKrEovP(energy/pmag);
    fDownstreamTrack.SetLKrPosition(dx+posAtLKr.X(),dy+posAtLKr.Y());
    fDownstreamTrack.SetLKrNCells(ncells);
    fDownstreamTrack.SetLKrSeedEnergy(seedenergy);
    fDownstreamTrack.SetIsGoodLKr(1);
    return 1;

}

Bool_t MainAnalysis::GoodLKrExtraCandidate(Int_t iC) {
    fLKrAnal->Clear(1); // flag for extra cluster search
    Double_t timeref = 0.;

    //if (fYear==2015) timeref = fDownstreamCandidate[iC].GetCHODTime();
    //if (fYear==2016) timeref = DownstreamReferenceTime(iC);

    timeref = fDownstreamCandidate[iC].GetDownstreamTime();
    // timeref = fDownstreamCandidate[iC].GetCHODTime();
    //timeref = fDownstreamCandidate[iC].GetRICHSingleTime();

    TVector2 poslkrclus = fDownstreamCandidate[iC].GetLKrPosition();
    TVector3 posatlkr = fDownstreamCandidate[iC].GetPositionAtLKr();
    Int_t nlkrextra = fLKrAnal->ExtraClusters(timeref,poslkrclus,posatlkr,fHeader->GetEventNumber());
    Int_t nlkrextraactivity = fLKrAnal->ExtraActivity(timeref,posatlkr);

    if(nlkrextraactivity> 0){
        fDownstreamCandidate[iC].SetIsPhotonLKrActivity(true);
    }
    // Extra cluster found in LKr: store it and return always
    if (nlkrextra) {
        fDownstreamCandidate[iC].SetIsPhotonLKrCandidate(true);
        fDownstreamCandidate[iC].SetNPhotonLKrCandidates(nlkrextra);
        for (Int_t jph=0; jph<nlkrextra; jph++) {
            if (jph>=10) continue;
            LKrCandidate *photon = (LKrCandidate *)fLKrAnal->GetPhotonCandidate(jph);
            fDownstreamCandidate[iC].SetPhotonLKrCandidateTime(jph,photon->GetTime());
            fDownstreamCandidate[iC].SetPhotonLKrCandidatePosition(jph,photon->GetX(),photon->GetY());
            fDownstreamCandidate[iC].SetPhotonLKrCandidateEnergy(jph,photon->GetEnergy());
            fDownstreamCandidate[iC].SetPhotonLKrCandidateNCells(jph,photon->GetNCells());
        }
        return true;
    }

    // Extra cluster found in LKr auxiliary reco: store it and return always
    Int_t nlkrextraaux = fLKrAnal->ExtraAuxClusters(timeref,poslkrclus,posatlkr);
    if (nlkrextraaux) {
        fDownstreamCandidate[iC].SetIsPhotonNewLKrCandidate(true);
        fDownstreamCandidate[iC].SetNPhotonNewLKrCandidates(nlkrextraaux);
        return true;

    }


    // Return no cluster if no extra & no new
    return false;
}

Bool_t MainAnalysis::GoodLAVCandidate(Int_t iC) {
    fLAVAnal->Clear();
    Double_t timeref = fDownstreamCandidate[iC].GetDownstreamTime();
    // Double_t timeref = fDownstreamCandidate[iC].GetCHODTime();
    //Double_t timeref = fDownstreamCandidate[iC].GetRICHSingleTime();

    Int_t nlav = fLAVAnal->MakeCandidate(timeref,fLAVEvent);
    int isnlav = nlav > 0 ? 1 : 0;

    // Standard
    fLAVMatching->SetReferenceTime(timeref);
    fLAVMatching->SetTimeCuts(3,3); //setting the timing cuts to 3 ns
    bool matched = fLAVMatching->LAVHasTimeMatching(fLAVEvent);
    int nnph = fLAVMatching->GetNumberOfMatchedBlocks();
    int* photonIDs=fLAVMatching->GetIndexOfMatchedBlocks();
    int ismatched = matched ? 1 : 0;

    // Test
    FillHisto("Tracks_lavmatching_nph",nlav,fLAVMatching->GetNumberOfMatchedBlocks());
    FillHisto("Tracks_lavmatching_matched",isnlav,ismatched);
    if (matched) FillHisto("Tracks_lavmatching_time",fLAVMatching->GetBestTimeOfMatchedBlocks());

    if(nlav!=0 || nnph!=0 || matched){

        fDownstreamCandidate[iC].SetNPhotonLAVCandidates(nnph);

        fDownstreamCandidate[iC].SetIsPhotonLAVCandidate(true);

        Int_t istrack = fLAVAnal->TrackCompatibility(timeref,fLAVEvent);
        if(istrack > 0)fDownstreamCandidate[iC].SetIsLAVTrack(true);

        TClonesArray& LAVHits = (*(fLAVEvent->GetHits()));

        for (Int_t jph=0; jph<nnph; jph++) {
            if (jph>=80) continue;

            TRecoLAVHit *hit = (TRecoLAVHit*)LAVHits[photonIDs[jph]];
            //PhotonVetoCandidate *photon = (PhotonVetoCandidate *)fLAVAnal->GetPhotonCandidate(jph);
            fDownstreamCandidate[iC].SetPhotonLAVCandidateTime(jph,hit->GetTime());
            fDownstreamCandidate[iC].SetPhotonLAVCandidateID(jph,photonIDs[jph]);
            fDownstreamCandidate[iC].SetPhotonLAVCandidateLAVID(jph,hit->GetLAVID());
        }
        return true;
    }
    return false;
}

Bool_t MainAnalysis::GoodIRCCandidate(Int_t iC) {
    fIRCAnal->Clear();
    Double_t timeref = fDownstreamCandidate[iC].GetDownstreamTime();
    //Double_t timeref = fDownstreamCandidate[iC].GetRICHSingleTime();
    Int_t nirc = fIRCAnal->MakeCandidate(timeref,fIRCEvent);
    fSAVMatching->SetReferenceTime(timeref);
    //if(GetWithMC())

    fSAVMatching->SetSACTimeCuts(7,7);
    fSAVMatching->SetIRCTimeCuts(7,7);

    bool matched = fSAVMatching->SAVHasTimeMatching(fIRCEvent,fSACEvent);
    FillHisto("Tracks_ircmatching_nph",nirc,fSAVMatching->GetNumberOfIRCMatchedBlocks());
    if (fSAVMatching->GetNumberOfIRCMatchedBlocks()) FillHisto("Tracks_ircmatching_time",fSAVMatching->GetBestTimeOfIRCMatchedBlocks());

    int nnph = fSAVMatching->GetNumberOfIRCMatchedBlocks();

    if(nirc!=0 || nnph!=0 || matched){

        fDownstreamCandidate[iC].SetIsPhotonIRCCandidate(true);
        fDownstreamCandidate[iC].SetNPhotonIRCCandidates(nirc);



        for (Int_t jph=0; jph<nirc; jph++) {
            if (jph>=20) continue;
            PhotonVetoCandidate *photon = (PhotonVetoCandidate *)fIRCAnal->GetPhotonCandidate(jph);
            fDownstreamCandidate[iC].SetPhotonIRCCandidateTime(jph,photon->GetTime());
            fDownstreamCandidate[iC].SetPhotonIRCCandidateID(jph,photon->GetID());
        }
        return true;
    }
    return false;
}

Bool_t MainAnalysis::GoodSACCandidate(Int_t iC) {
    fSACAnal->Clear();
    Double_t timeref = fDownstreamCandidate[iC].GetDownstreamTime();
    // Double_t timeref = fDownstreamCandidate[iC].GetCHODTime();
    //Double_t timeref = fDownstreamCandidate[iC].GetRICHSingleTime();
    Int_t nsac = fSACAnal->MakeCandidate(timeref,fSACEvent);

    FillHisto("Tracks_sacmatching_nph",nsac,fSAVMatching->GetNumberOfSACMatchedBlocks());
    if (fSAVMatching->GetNumberOfSACMatchedBlocks()) FillHisto("Tracks_sacmatching_time",fSAVMatching->GetBestTimeOfSACMatchedBlocks());

    int nnph = fSAVMatching->GetNumberOfSACMatchedBlocks();

    if(nsac!=0 || nnph != 0 ){

        fDownstreamCandidate[iC].SetIsPhotonSACCandidate(true);
        fDownstreamCandidate[iC].SetNPhotonSACCandidates(nsac);
        for (Int_t jph=0; jph<nsac; jph++) {
            if (jph>=20) continue;
            PhotonVetoCandidate *photon = (PhotonVetoCandidate *)fSACAnal->GetPhotonCandidate(jph);
            fDownstreamCandidate[iC].SetPhotonSACCandidateTime(jph,photon->GetTime());
            fDownstreamCandidate[iC].SetPhotonSACCandidateID(jph,photon->GetID());
        }
        return true;
    }

    return false;
}

Bool_t MainAnalysis::GoodSAVCandidate(Int_t iC) {
    fSAVAnal->Clear();
    Double_t timeref = fDownstreamCandidate[iC].GetDownstreamTime();
    // Double_t timeref = fDownstreamCandidate[iC].GetCHODTime();
    //Double_t timeref = fDownstreamCandidate[iC].GetRICHSingleTime();
    Int_t nsav = fSAVAnal->MakeCandidate(timeref,fSAVEvent);
    if (!nsav) return false;
    fDownstreamCandidate[iC].SetIsPhotonSAVCandidate(true);
    fDownstreamCandidate[iC].SetNPhotonSAVCandidates(nsav);
    return true;
}



Int_t MainAnalysis::GoodRICHSingleCandidate(Int_t iC,TRecoSpectrometerCandidate* track){

    Double_t singlering        = fRICHCandidate[iC].GetIsRICHSingleRingCandidate();

    FillHisto("Tracks_IsRICHSingle_flag", singlering);

    //if(!fRICHCandidate[iC].GetIsRICHCandidate()) return 0;

    if(!singlering )  return 0;

    Double_t pmag = fDownstreamTrack.GetMomentum().P();

    Double_t focallength       = 17020;
    Double_t single_radius     = fRICHCandidate[iC].GetSingleRingRadius();
    Double_t single_prob       = fRICHCandidate[iC].GetSingleRingChi2();
    Double_t single_time       = fRICHCandidate[iC].GetSingleRingTime();
    //Double_t single_dr         = fRICHCandidate[iC].GetSingleRingDR();
    Double_t single_dx         = fRICHCandidate[iC].GetSingleRingDX();
    Double_t single_dy         = fRICHCandidate[iC].GetSingleRingDY();
    TVector2 single_ringcenter(fRICHCandidate[iC].GetSingleRingXcenter(),fRICHCandidate[iC].GetSingleRingYcenter());
    Int_t    single_nobshits   = fRICHCandidate[iC].GetSingleRingNHits();
    Double_t chodtime = fCHODCandidate[iC].GetDeltaTime()+track->GetTime();
    Double_t dtchod   = single_time-chodtime;
    Double_t discriminant   = fRICHCandidate[iC].GetDiscriminant();
    double   min_richthx    = single_ringcenter.X()/focallength;
    double   min_richthy    = single_ringcenter.Y()/focallength;
    double   min_dslopex    = min_richthx - track->GetSlopeXAfterMagnet();
    double   min_dslopey    = min_richthy - track->GetSlopeYAfterMagnet();
    double   min_dslope     = TMath::Sqrt(min_richthy*min_richthy + min_richthx*min_richthx);
    double   changle        = atan(single_radius/focallength);
    double   changle_el     = atan(fElectronRingRadius/focallength);//ring radius 180mm for electrons
    //Ref index assuming electron is passing giving beta=1
    double   refindex       = 1.0/cos(changle_el);
    Double_t single_mass    = refindex*refindex*cos(changle)*cos(changle) - 1 > 0 ? pmag*sqrt(refindex*refindex*cos(changle)*cos(changle) - 1): 99999 ;
    double   d2ring         = sqrt(fabs(fElectronRingRadius*fElectronRingRadius - single_radius*single_radius));
    double   prich          = 0.001*Constants::MPI*focallength/d2ring;
    TLorentzVector pwithrich = fTool->Get4Momentum(prich*1000,track->GetSlopeXBeforeMagnet(),track->GetSlopeYBeforeMagnet(),0.001*Constants::MPI);

    FillHisto("Tracks_IsRICHSingle_prob",single_prob);
    FillHisto("Tracks_IsRICHSingle_xydslope",min_dslopex,min_dslopey);
    //FillHisto("Tracks_IsRICHSingle_ringchi2",single_chi2);
    //FillHisto("Tracks_IsRICHSingle_dr",single_dr);
    FillHisto("Tracks_IsRICHSingle_dxdy",single_dx,single_dy);
    FillHisto("Tracks_IsRICHSingle_ringrad_vs_p",pmag, single_radius);
    FillHisto("Tracks_IsRICHSingle_nhits",single_nobshits);
    FillHisto("Tracks_IsRICHSingle_dtchod",dtchod);
    FillHisto("Tracks_IsRICHSingle_richmass_vs_p",pmag,single_mass);
    FillHisto("Tracks_IsRICHSingle_richp_vs_p",pmag,prich);

    //Get CHOD multiplicity using Cedar time as reference
    // if( fabs(dtchod) < 2 && single_prob > 0.01 && fRICHCandidate[iC].GetDiscriminant() < 20 ){

    FillHisto("Tracks_IsRICHSingle_dtimevschi2rich",discriminant,dtchod);
    fDownstreamTrack.SetRICHSingleIsCandidate(singlering);
    if( fabs(dtchod) < 2 && single_prob > 0.01 && discriminant <= 50 ){

        FillHisto("Tracks_IsRICHSingle_richp_vs_p_f",pmag,prich);
        FillHisto("Tracks_IsRICHSingle_dtimevschi2rich_f",discriminant,dtchod);
        fDownstreamTrack.SetRICHSingleTime(single_time);
        fDownstreamTrack.SetRICHSingleDiscriminant(discriminant);
        fDownstreamTrack.SetRICHSingleMass(single_mass);
        fDownstreamTrack.SetRICHSingleNHits(single_nobshits);
        fDownstreamTrack.SetRICHSingleMomentum(pwithrich);
        fDownstreamTrack.SetRICHSingleChi2(single_prob);
        fDownstreamTrack.SetRICHSingleRadius(single_radius);
        fDownstreamTrack.SetIsGoodRICHSR(1);
        return 1;

    } else return 0;



    return 0;
}

Int_t MainAnalysis::GoodRICHMultiCandidate(Int_t iC,TRecoSpectrometerCandidate* track){

    //if(!fRICHCandidate[iC].GetIsRICHCandidate()) return 0;
    if(!fRICHCandidate[iC].GetIsRICHMultiRingCandidate()) return 0;
    FillHisto("Tracks_IsRICH_NRings",fRICHCandidate[iC].GetNRings());
    Double_t elll= fRICHCandidate[iC].GetRICHMultiElectronLikelihood();
    Double_t mull= fRICHCandidate[iC].GetRICHMultiMuonLikelihood();
    Double_t pill= fRICHCandidate[iC].GetRICHMultiPionLikelihood();
    Double_t pmag = fDownstreamTrack.GetMomentum().P();

    //if(fRICHEvent->GetNRingCandidates() > 1) return 0;
    Double_t fFocalLength = 17020.;
    //Using multicandidate to store the information about the rich matching candidate

    Double_t hypothesis = fRICHCandidate[iC].GetMostLikelyHypothesis();
    FillHisto("Tracks_IsRICH_ringchi2",fRICHCandidate[iC].GetChi2());
    FillHisto("Tracks_IsRICH_hypothesis_all",hypothesis);

    if(track->GetMomentum() > 15000 && track->GetMomentum() < 35000 ){
        FillHisto("Tracks_IsRICH_hypothesis_15_35_GeV",hypothesis);
        fDownstreamTrack.SetRICHMultiIsCandidateMomRange(1);
    }

    TVector3 posTrackAtMirror = fTool->GetPositionAtZ(track,236873.);
    Int_t mirrorid = fRICHCandidate[iC].GetMirrorID();
    Double_t thetax = track->GetSlopeXAfterMagnet();
    Double_t thetay = track->GetSlopeYAfterMagnet();
    Double_t norm   = 1./sqrt(1 + pow(track->GetSlopeXAfterMagnet(),2) + pow(track->GetSlopeYAfterMagnet(),2));
    Double_t slopex = (fRICHCandidate[iC].GetXcenter())/fFocalLength;
    Double_t slopey = (fRICHCandidate[iC].GetYcenter())/fFocalLength;
    Double_t dslopex = slopex-track->GetSlopeXAfterMagnet();
    Double_t dslopey = slopey-track->GetSlopeYAfterMagnet();
    Double_t radius = fRICHCandidate[iC].GetRadius();
    Double_t changle = atan(radius/fFocalLength);
    Double_t richmass = 1.000062*1.000062*cos(changle)*cos(changle)-1>0 ? pmag*sqrt(1.000062*1.000062*cos(changle)*cos(changle)-1) : 9999.;
    Double_t d2ring = sqrt(fabs(fElectronRingRadius*fElectronRingRadius-radius*radius));
    Double_t prich = 0.13957018*fFocalLength/d2ring;
    TLorentzVector richmomentum;
    Double_t prichx=prich*thetax*norm;
    Double_t prichy=prich*thetay*norm;
    Double_t prichz=prich*norm;
    richmomentum.SetXYZM(prichx,prichy,prichz,0.13957018);

    FillHisto("Tracks_IsRICH_richmass_vs_p",pmag,richmass);
    FillHisto("Tracks_IsRICH_xyslope",dslopex,dslopey);
    FillHisto("Tracks_IsRICH_ringrad_vs_p",pmag,radius);
    FillHisto("Tracks_IsRICH_richp_vs_p",pmag,prich);

    fDownstreamTrack.SetRICHMultiIsCandidate(1);
    fDownstreamTrack.SetIsRICHSingleRingCandidateSlava(fRICHCandidate[iC].GetIsRICHSingleRingCandidateSlava());
    fDownstreamTrack.SetRICHMultiChi2(fRICHCandidate[iC].GetChi2());
    fDownstreamTrack.SetRICHMultiRadius(radius);
    fDownstreamTrack.SetRICHMultiMass(richmass);
    fDownstreamTrack.SetRICHMultiElectronLikelihood(elll);
    fDownstreamTrack.SetRICHMultiMuonLikelihood(mull);
    fDownstreamTrack.SetRICHMultiPionLikelihood(pill);
    fDownstreamTrack.SetIsRICHPion(fRICHCandidate[iC].GetIsPion());
    fDownstreamTrack.SetRICHMultiMomentum(richmomentum);
    fDownstreamTrack.SetRICHMultiMostLikelyHypothesis(hypothesis);
    fDownstreamTrack.SetPosNexp(fRICHCandidate[iC].GetPosNexp());
    fDownstreamTrack.SetMuNexp (fRICHCandidate[iC].GetPiNexp());
    fDownstreamTrack.SetPiNexp (fRICHCandidate[iC].GetMuNexp());
    fDownstreamTrack.SetIsGoodRICHMR(1);
    return 1;
}

Int_t MainAnalysis::GoodMUV3Candidate(Int_t iC,TRecoSpectrometerCandidate* track){

    TVector3 posmuv3 = fTool->GetPositionAtZ(track,Constants::zMUV3);
    // if(!fMUV3TightCandidate[iC].GetIsMUV3Candidate() && !fMUV3LooseCandidate[iC].GetIsMUV3Candidate()) return 0;
    if(!fMUV3TightCandidate[iC].GetIsMUV3Candidate()) return 0;

    Double_t reftime  = fCHODCandidate[iC].GetDeltaTime()+track->GetTime();
    //Double_t reftime  = fRICHCandidate[iC].GetSingleRingTime();

    double muv3time  =fMUV3TightCandidate[iC].GetDeltaTime()+track->GetTime();
    FillHisto("Tracks_IsMUV3_drichtime",muv3time - reftime);

    if( (muv3time-reftime) < 5 && (muv3time-reftime) > -10) {
        fDownstreamTrack.SetMUV3Time(fMUV3TightCandidate[iC].GetDeltaTime()+track->GetTime());
        fDownstreamTrack.SetIsGoodMUV3(1);
        return 1;
    }

    return 0;
}

Int_t MainAnalysis::GoodStandardMUV3Candidate(Int_t iC,TRecoSpectrometerCandidate* track){

    if (!fMUV3Candidate[iC].GetNAssociationRecords()) return false;

    Bool_t inMUV3 = false;
    TVector3 pos = fTool->GetPositionAtZ(track,Constants::zMUV3);
    TVector2 pTrack(pos.X(),pos.Y());

    double mintime = 9999999999;
    double mindt = 9999999999;
    int minid = -1;
    for (int k(0); k<fMUV3Candidate[iC].GetNAssociationRecords(); k++) {
        int iMUV3 = fMUV3Candidate[iC].GetAssociationRecord(k)->GetMuonID();
        TRecoMUV3Candidate *cMUV3 = (TRecoMUV3Candidate *)fMUV3Event->GetCandidate(iMUV3);
        double dtime = cMUV3->GetTime()-fDownstreamTrack.GetCHODTime();
        TVector2 pMUV3(cMUV3->GetX(),cMUV3->GetY());
        Double_t dist = (pMUV3-pTrack).Mod();
        FillHisto("Tracks_IsMUV3_standard_dist_vs_dchodtime",dtime,dist);

        if(fabs(dtime) < fabs(mindt)){
            minid   = k;
            mintime = cMUV3->GetTime();
            mindt   = dtime;
        }
    }

    if (fabs(mindt)<5.) inMUV3 = true;

    if(inMUV3){
        fDownstreamTrack.SetMUV3Time(mintime);
        fDownstreamTrack.SetIsGoodMUV3(1);
    }

    return inMUV3;
}
Int_t MainAnalysis::MakeDownstreamTime(Int_t iC,TRecoSpectrometerCandidate* track,bool isrich){

    double tdown    = -999999;
    double wtot     = -999999;
    double avtot    = -999999;

    //CHOD
    double chodw    = 0.5;
    double chodfrac = fDownstreamTrack.GetCHODTime()/chodw;

    //LKR
    double lkrw     = 1.0;
    double lkrfrac  = fDownstreamTrack.GetLKrTime()/lkrw;

    //STRAW
    double straww   = 10.0;
    double strawfrac= track->GetTime()/straww;

    wtot  = 1./chodw + 1./lkrw + 1./straww;
    avtot = chodfrac + lkrfrac + strawfrac;

    //Decide to use the rich to build the time or not
    if(isrich){
        //RICH
        double richw     = 0.2;
        double richfrac  = fDownstreamTrack.GetRICHSingleTime()/richw;

        wtot += 1./richw;
        avtot+= richfrac;
        tdown = avtot/wtot;

    } else {
        tdown = avtot/wtot;
    }

    FillHisto("Tracks_Tdown",tdown);

    fDownstreamTrack.SetDownstreamTime(tdown);

    return 1;
}
void MainAnalysis::BuildCalorimetricEnergy(Int_t iC, CalorimeterCluster* clus) {
    TRecoSpectrometerCandidate *track = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(iC);
    Double_t elkr = fDownstreamTrack.GetLKrEovP()*fDownstreamTrack.GetMomentum().P();
    Double_t etot = elkr;
    Double_t emuv1 = 0.;
    Double_t dtmuv1chod = -99999.;
    Double_t dtmuv2chod = -99999.;
    //bool ismuv1 =  clus->IsMUV1Associated();
    //bool ismuv2 =  clus->IsMUV2Associated();

    // Add MUV1 energy to LKr energy
    if (clus->GetMUV1Energy() > 0) {
        TRecoMUV1Candidate* muv1 = (TRecoMUV1Candidate*) clus->GetMUV1Candidate();

        TVector3 trackAtMUV1 = fTool->GetPositionAtZ(track,Constants::zMUV1) ;
        Double_t dxmuv1   = muv1->GetX() - trackAtMUV1.X();
        Double_t dymuv1   = muv1->GetY() - trackAtMUV1.Y();
        Double_t distmuv1 = TMath::Sqrt(dxmuv1*dxmuv1 + dymuv1*dymuv1) ;
        Double_t muv1time = clus->GetMUV1Time();
        Double_t dtime = muv1time-fDownstreamTrack.GetLKrTime();
        emuv1 = clus->GetMUV1Energy()*0.001;
        FillHisto("Track_muv1candidate_dist_vs_eovp",fDownstreamTrack.GetLKrEovP(),distmuv1);
        if (fDownstreamTrack.GetLKrEovP()<0.1) {
            FillHisto("Tracks_IsMUV1Cal_dist_vs_dtime_noelkr",dtime,distmuv1);

        } else {
            FillHisto("Tracks_IsMUV1Cal_dist_vs_dtime_elkr",dtime,distmuv1);

        }

        dtmuv1chod = muv1time - fDownstreamTrack.GetCHODTime();
        //dtmuv1chod = muv1time - fDownstreamTrack.GetRICHSingleTime();
    }
    FillHisto("Tracks_IsMUV1Cal_emuv1_vs_elkr",elkr,emuv1);
    etot = elkr+emuv1;

    Double_t etotovp = etot/fDownstreamTrack.GetMomentum().P();
    FillHisto("Tracks_IsMUV1Cal_energy_vs_p",fDownstreamTrack.GetMomentum().P(),etot);
    FillHisto("Tracks_IsMUV1Cal_hadenergy_vs_p",fDownstreamTrack.GetMomentum().P(),etot-elkr);
    FillHisto("Tracks_IsMUV1Cal_eovp_vs_p",fDownstreamTrack.GetMomentum().P(),etotovp);
    //
    //// Add MUV2 energy to LKr+MUV1 energy
    Double_t emuv2 = 0;

    if (clus->GetMUV2Energy() > 0) {
        TRecoMUV2Candidate* muv2 = (TRecoMUV2Candidate*) clus->GetMUV2Candidate();

        TVector3 trackAtMUV2 = fTool->GetPositionAtZ(track,Constants::zMUV2) ;
        Double_t dxmuv2   = muv2->GetX() - trackAtMUV2.X();
        Double_t dymuv2   = muv2->GetY() - trackAtMUV2.Y();
        Double_t distmuv2 = TMath::Sqrt(dxmuv2*dxmuv2 + dymuv2*dymuv2) ;
        Double_t muv2time = clus->GetMUV2Time();
        Double_t dtime2 = muv2time-fDownstreamTrack.GetLKrTime();
        emuv2 = clus->GetMUV2Energy()*0.001;
        FillHisto("Track_muv2candidate_dist_vs_eovp",etotovp,distmuv2);
        if (etotovp<0.15) {
            FillHisto("Tracks_IsMUV2Cal_dist_vs_dtime_noetot",dtime2,distmuv2);
        } else {
            FillHisto("Tracks_IsMUV2Cal_dist_vs_dtime_etot",dtime2,distmuv2);
        }
        dtmuv2chod = muv2time - fDownstreamTrack.GetCHODTime();
        //dtmuv2chod = muv2time - fDownstreamTrack.GetRICHSingleTime();
    }
    FillHisto("Tracks_IsMUV2Cal_emuv2_vs_etot",etot,emuv2);

    // Final histos
    etot += emuv2;
    etotovp = etot/fDownstreamTrack.GetMomentum().P();

    if (emuv1>0&&emuv2==0) FillHisto("Track_eovpvsdt10",dtmuv1chod,etotovp);
    if (emuv1>0&&emuv2>0)  FillHisto("Track_eovpvsdt11",0.5*(dtmuv1chod+dtmuv2chod),etotovp);
    if (emuv2>0&&emuv1==0) FillHisto("Track_eovpvsdt01",dtmuv2chod,etotovp);

    if (fDownstreamTrack.GetMUV3ID()==-1) {
        FillHisto("Track_calocandidate_pion_emuvvselkr",elkr,emuv1+emuv2);
        FillHisto("Track_calocandidate_pion_energyvsp",fDownstreamTrack.GetMomentum().P(),etot);
        FillHisto("Track_calocandidate_pion_eovpvsp",fDownstreamTrack.GetMomentum().P(),etotovp);
        FillHisto("Track_calocandidate_pion_hadenergyvsp",fDownstreamTrack.GetMomentum().P(),etot-elkr);
        FillHisto("Track_calocandidate_pion_eratio1vsp",fDownstreamTrack.GetMomentum().P(),emuv1/etot);
        FillHisto("Track_calocandidate_pion_eratio2vsp",fDownstreamTrack.GetMomentum().P(),emuv2/etot);
        FillHisto("Track_calocandidate_pion_eratio2vseratio1",emuv1/etot,emuv2/etot);
    }
    if (fDownstreamTrack.GetMUV3ID()==0) {
        FillHisto("Track_calocandidate_muon_emuvvselkr",elkr,emuv1+emuv2);
        FillHisto("Track_calocandidate_muon_energyvsp",fDownstreamTrack.GetMomentum().P(),etot);
        FillHisto("Track_calocandidate_muon_eovpvsp",fDownstreamTrack.GetMomentum().P(),etotovp);
        FillHisto("Track_calocandidate_muon_hadenergyvsp",fDownstreamTrack.GetMomentum().P(),etot-elkr);
        FillHisto("Track_calocandidate_muon_eratio1vsp",fDownstreamTrack.GetMomentum().P(),emuv1/etot);
        FillHisto("Track_calocandidate_muon_eratio2vsp",fDownstreamTrack.GetMomentum().P(),emuv2/etot);
        FillHisto("Track_calocandidate_muon_eratio2vseratio1",emuv1/etot,emuv2/etot);
    }

    // Store variables
    fDownstreamTrack.SetCalorimetricEnergy(etot);


    if (clus->GetMUV1Energy() > 0) {
        fDownstreamTrack.SetMUV1Energy(emuv1);
        fDownstreamTrack.SetMUV1Time(clus->GetMUV1Time());
        fDownstreamTrack.SetMUV1OuterEnergy(clus->GetMUV1OuterEnergy());

        TRecoMUV1Candidate *cand = (TRecoMUV1Candidate *)clus->GetMUV1Candidate();
        fDownstreamTrack.SetMUV1ShowerWidth(cand->GetShowerWidth());
        Double_t seedRatio = (cand->GetEnergyHorizontal()+cand->GetEnergyVertical())>0 ? (cand->GetSeedEnergyHorizontal()+cand->GetSeedEnergyVertical())/(cand->GetEnergyHorizontal()+cand->GetEnergyVertical()) : 0;
        fDownstreamTrack.SetMUV1SeedRatio(seedRatio);
        fDownstreamTrack.SetIsGoodMUV1(1);

    }

    if (clus->GetMUV2Energy() > 0) {
        fDownstreamTrack.SetMUV2Energy(emuv2);
        fDownstreamTrack.SetMUV2Time(clus->GetMUV2Energy());
        fDownstreamTrack.SetMUV2OuterEnergy(clus->GetMUV2OuterEnergy());

        TRecoMUV2Candidate *cand = (TRecoMUV2Candidate *)clus->GetMUV2Candidate();
        fDownstreamTrack.SetMUV2ShowerWidth(cand->GetShowerWidth());
        Double_t seedRatio = (cand->GetEnergyHorizontal()+cand->GetEnergyVertical())>0 ? (cand->GetSeedEnergyHorizontal()+cand->GetSeedEnergyVertical())/(cand->GetEnergyHorizontal()+cand->GetEnergyVertical()) : 0;
        fDownstreamTrack.SetMUV2SeedRatio(seedRatio);
        fDownstreamTrack.SetIsGoodMUV2(1);

    }

    // PID R. Aliberti
    fDownstreamTrack.SetCaloTimeRA(clus->GetTime());
    fDownstreamTrack.SetMuonProb(clus->GetIsMuonProbability());
    fDownstreamTrack.SetElectronProb(clus->GetIsElectronProbability());
    fDownstreamTrack.SetPionProb(clus->GetIsPionProbability());
    //cout << "Pmu = " <<clus->GetIsMuonProbability() <<"Pe = " <<clus->GetIsElectronProbability() << "Ppi = " << clus->GetIsPionProbability() << endl;
    double lkrMipDiscr = (clus->GetLKrEnergy()-561.3)/(1.5*54.9);
    double muv1MipDiscr = (clus->GetMUV1Energy()-1183.8)/(1.5*165.8);
    double muv2MipDiscr = (clus->GetMUV2Energy()-1042.5)/(1.5*141.1);
    double mipDiscr = sqrt(lkrMipDiscr*lkrMipDiscr+muv1MipDiscr*muv1MipDiscr+muv2MipDiscr*muv2MipDiscr)/3.;
    bool isMip = mipDiscr<1.;
    fDownstreamTrack.SetMIPDiscr(mipDiscr);
    //fDownstreamTrack.SetIsMuonDiscriminant(isMuon);
    fDownstreamTrack.SetIsMIP(isMip);
    bool isMulti = (clus->GetMUV1OuterEnergy()+clus->GetMUV2OuterEnergy())>=5.e+3;
    fDownstreamTrack.SetIsMulti(isMulti);
    fDownstreamTrack.SetMUV1OuterNhits(clus->GetMUV1OuterNhits());
    fDownstreamTrack.SetMUV2OuterNhits(clus->GetMUV2OuterNhits());

}


Bool_t MainAnalysis::GeometricalAcceptance(Int_t iC, TRecoSpectrometerCandidate* track){
    TVector3 posstraw1 = fTool->GetPositionAtZ(track,Constants::zSTRAW_station[0]);
    TVector3 posstraw2 = fTool->GetPositionAtZ(track,Constants::zSTRAW_station[1]);
    TVector3 posstraw3 = fTool->GetPositionAtZ(track,Constants::zSTRAW_station[2]);
    TVector3 posstraw4 = fTool->GetPositionAtZ(track,Constants::zSTRAW_station[3]);
    TVector3 posnewchodf = fTool->GetPositionAtZ(track,Constants::zNewCHODFront);
    TVector3 posnewchodb = fTool->GetPositionAtZ(track,Constants::zNewCHODBack);
    TVector3 posnewchod = fTool->GetPositionAtZ(track,Constants::zNewCHOD);
    // TVector3 poschod = fTool->GetPositionAtZ(track,Constants::zCHOD);
    TVector3 poschodh = fTool->GetPositionAtZ(track,Constants::zCHODHL);
    TVector3 poschodv = fTool->GetPositionAtZ(track,Constants::zCHODVL);
    TVector3 poschod(poschodv.X(),poschodh.Y(),Constants::zCHOD);
    TVector3 poslkr = fTool->GetPositionAtZ(track,Constants::zLKr);
    TVector3 posmuv1 = fTool->GetPositionAtZ(track,Constants::zMUV1);
    TVector3 posmuv2 = fTool->GetPositionAtZ(track,Constants::zMUV2);
    TVector3 posmuv3 = fTool->GetPositionAtZ(track,Constants::zMUV3);
    TVector3 posrichmirror = fTool->GetPositionAtZ(track,Constants::zRICHMI);
    TVector3 posrichex = fTool->GetPositionAtZ(track,Constants::zRICHExtWindow);
    TVector3 posrichent = fTool->GetPositionAtZ(track,Constants::zRICHEntWindow);
    Bool_t flag = true;

    if (fStrawCandidate[iC].GetAcceptance()<1){
        FillHisto("Tracks_Acc_out",posstraw1.X(),posstraw1.Y());
        flag = false;
    }
    if (fStrawCandidate[iC].GetAcceptanceStraw()<1){
        FillHisto("Tracks_AccStraw1_out",posstraw1.X(),posstraw1.Y());
        FillHisto("Tracks_AccStraw2_out",posstraw2.X(),posstraw2.Y());
        FillHisto("Tracks_AccStraw3_out",posstraw3.X(),posstraw3.Y());
        FillHisto("Tracks_AccStraw4_out",posstraw4.X(),posstraw4.Y());
        flag = false;
    }
    if(fStrawCandidate[iC].GetAcceptanceCHOD() < 1) {
        FillHisto("Tracks_AccCHOD_out",poschod.X(),poschod.Y());
        flag = false;
    }
    if(fStrawCandidate[iC].GetAcceptanceNewCHOD() < 1) {
        FillHisto("Tracks_AccNewCHOD_out",posnewchod.X(),posnewchod.Y());
        flag = false;
    }
    if(fStrawCandidate[iC].GetAcceptanceLKr() < 1) {
        FillHisto("Tracks_AccLKr_out",poslkr.X(),poslkr.Y());
        flag = false;
    }
    if(fStrawCandidate[iC].GetAcceptanceRICH() < 1) {
        FillHisto("Tracks_AccRICH_exit_out",posrichex.X(),posrichex.Y());
        FillHisto("Tracks_AccRICH_entrance_out",posrichent.X(),posrichent.Y());
        flag = false;
    }
    if(fStrawCandidate[iC].GetAcceptanceMUV1() < 1) {
        FillHisto("Tracks_AccMUV1_out",posmuv1.X(),posmuv1.Y());
        flag = false;
    }
    if(fStrawCandidate[iC].GetAcceptanceMUV2() < 1) {
        FillHisto("Tracks_AccMUV2_out",posmuv2.X(),posmuv2.Y());
        flag = false;
    }
    if(fStrawCandidate[iC].GetAcceptanceMUV3() < 1) {
        FillHisto("Tracks_AccMUV3_out",posmuv3.X(),posmuv3.Y());
        flag = false;
    }

    if(flag){

        fDownstreamTrack.SetPositionAtStraw(0,posstraw1);
        fDownstreamTrack.SetPositionAtStraw(1,posstraw2);
        fDownstreamTrack.SetPositionAtStraw(2,posstraw3);
        fDownstreamTrack.SetPositionAtStraw(3,posstraw4);
        fDownstreamTrack.SetPositionAtRICH(0,posrichent);
        fDownstreamTrack.SetPositionAtRICH(1,posrichex);
        fDownstreamTrack.SetPositionAtRICH(2,posrichmirror);
        fDownstreamTrack.SetPositionAtNewCHOD(0, posnewchodf);
        fDownstreamTrack.SetPositionAtNewCHOD(1, posnewchodb);
        fDownstreamTrack.SetPositionAtNewCHOD(2, posnewchod);
        fDownstreamTrack.SetPositionAtCHOD(0, poschodv);
        fDownstreamTrack.SetPositionAtCHOD(1, poschodh);
        fDownstreamTrack.SetPositionAtCHOD(2, poschod);
        fDownstreamTrack.SetPositionAtLKr(poslkr);
        fDownstreamTrack.SetPositionAtMUV(0,posmuv1);
        fDownstreamTrack.SetPositionAtMUV(1,posmuv2);
        fDownstreamTrack.SetPositionAtMUV(2,posmuv3);
        fDownstreamTrack.SetPosition(track->GetPositionBeforeMagnet());

        FillHisto("Tracks_Acc_in",posstraw1.X(),posstraw1.Y());
        FillHisto("Tracks_AccStraw1_in",posstraw1.X(),posstraw1.Y());
        FillHisto("Tracks_AccStraw2_in",posstraw2.X(),posstraw2.Y());
        FillHisto("Tracks_AccStraw3_in",posstraw3.X(),posstraw3.Y());
        FillHisto("Tracks_AccStraw4_in",posstraw4.X(),posstraw4.Y());
        FillHisto("Tracks_AccCHOD_in",poschod.X(),poschod.Y());
        FillHisto("Tracks_AccNewCHOD_in",posnewchod.X(),posnewchod.Y());
        FillHisto("Tracks_AccLKr_in",poslkr.X(),poslkr.Y());
        FillHisto("Tracks_AccRICH_exit_in",posrichex.X(),posrichex.Y());
        FillHisto("Tracks_AccRICH_entrance_in",posrichent.X(),posrichent.Y());
        FillHisto("Tracks_AccMUV1_in",posmuv1.X(),posmuv1.Y());
        FillHisto("Tracks_AccMUV2_in",posmuv2.X(),posmuv2.Y());
        FillHisto("Tracks_AccMUV3_in",posmuv3.X(),posmuv3.Y());

    }
    return flag;

}
Int_t MainAnalysis::SelectTrigger(int triggerType, int type, int mask) {
    int bitPhysics = 0;
    int bitControl = 4;
    int bitMinBias = 0;
    int bitPinunu = 1;


    if (type&0x2) return 0; // skip periodics


    // Select trigger
    if ((triggerType==0) && (type>>bitControl)&1) return 1; // control trigger.
    else if (triggerType==1) { // physics minimum bias trigger.
        if (!((type>>bitPhysics)&1)) return 0;
        if ((mask>>bitMinBias)&1) return 1;
    }
    else if (triggerType==2) { // physics pinunu trigger.
        if (!((type>>bitPhysics)&1)) return 0;
        if ((mask>>bitPinunu)&1) return 1;
    }
    else return 0; // trigger not found.

    return 1;

}
