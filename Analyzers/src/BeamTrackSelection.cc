#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "BeamTrackSelection.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"

#include "EventHeader.hh"
#include "L0TPData.hh"
#include "AnalysisTools.hh"
//#include "utl.hh"
#include "Constants.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


//Variables for early decay suppression
Double_t fxPaipDAcc;
Double_t fyPaipDAcc;
Double_t fxKaonDAcc;
Double_t fyKaonDAcc;
Double_t fxprimePaipAcc;
Double_t fyprimePaipAcc;
Double_t fxprimeKaonAcc;
Double_t fyprimeKaonAcc;
Double_t fpPaipAcc;
Double_t fpKaonAcc;


BeamTrackSelection::BeamTrackSelection(Core::BaseAnalysis *ba) : Analyzer(ba, "BeamTrackSelection")
{

  RequestTree("Spectrometer",new TRecoSpectrometerEvent);
  fTool = AnalysisTools::GetInstance();
  RequestL0Data();
  RequestTree("Cedar",new TRecoCedarEvent);
  RequestTree("CHOD",new TRecoCHODEvent);
  RequestTree("NewCHOD",new TRecoNewCHODEvent);
  RequestTree("LKr",new TRecoLKrEvent);
  RequestTree("MUV0",new TRecoMUV0Event);
  RequestTree("HAC",new TRecoHACEvent);
  RequestTree("MUV1",new TRecoMUV1Event);
  RequestTree("MUV2",new TRecoMUV2Event);
  RequestTree("MUV3",new TRecoMUV3Event);
  RequestTree("LAV",new TRecoLAVEvent);
  RequestTree("SAV",new TRecoSAVEvent);
  RequestTree("IRC",new TRecoIRCEvent);
  RequestTree("SAC",new TRecoSACEvent);
  RequestTree("CHANTI",new TRecoCHANTIEvent);

  xSTRAW_station[0] = 101.2;
  xSTRAW_station[1] = 114.4;
  xSTRAW_station[2] = 92.4;
  xSTRAW_station[3] = 52.8; // [ mm ]

  // Pointer to useful classes

  fCHODAnal = new CHODAnal(0,ba);
  fLKrAnal = new LKrAnal(1,ba);
  fStrawRapper = new StrawRapper(1,ba,fCHODAnal,fLKrAnal);
  //fMUV3Anal = new MUV3Anal(ba);
  ////fRICHAnal = new RICHAnal(ba);
  //fMUVAnal = new MUVAnal(ba);
  //fCHANTIAnal = new CHANTIAnal(ba);
  //fSACAnal = new SACAnal(ba);
  //fSAVAnal = new SAVAnal(ba);
  //fIRCAnal = new IRCAnal(ba);
  //fLAVAnal = new LAVAnal(0,ba);
  //fGigaTrackerAnalysis = new GigaTrackerAnalysis(ba);
  //fCedarAnal = new CedarAnal(ba);
  //fCaloArray = new TClonesArray("CalorimeterCluster",20);
  fYear=2016;


  // Minuit initialization
  fNPars = 1;
  fFitter = new TMinuit(fNPars);
  fFitter->SetFCN(FuncCDA);

}

void BeamTrackSelection::InitOutput(){


  BookHisto(new TH1F("onetrack_gooddtchod","",100,-25.,25.));


  BookHisto(new TH1F("Pinunu_kaon_p","",100,70,80));
  BookHisto(new TH2F("Pinunu_kaon_thetay_vs_thetax","",50,-0.001,0.001,50,-0.001,0.001));
  BookHisto(new TH1F("Pinunu_kaon_thetax","",50,-0.001,0.001));
  BookHisto(new TH1F("Pinunu_kaon_thetay","",50,-0.001,0.001));

  BookHisto(new TH2F("Pinunu_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));


  BookHisto(new TH2F("Pinunu_pid_positron_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_pid_positron_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_pid_positron_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_pid_positron_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  BookHisto(new TH2F("Pinunu_pid_nopositron_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_pid_nopositron_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_pid_nopositron_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_pid_nopositron_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  BookHisto(new TH2F("Pinunu_pid_nomuon_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_pid_nomuon_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_pid_nomuon_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_pid_nomuon_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  BookHisto(new TH2F("Pinunu_pid_muonid_mmiss_vs_eop","",120,0,1.2,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_pid_muonid_mmiss_vs_eop_nomuv3","",120,0,1.2,375,-0.1,0.14375));

  BookHisto(new TH2F("Pinunu_pid_muon_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_pid_muon_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_pid_muon_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_pid_muon_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  //BookHisto(new TH2F("Pinunu_ibvsmmiss_after_particleid","",375,-0.1,0.14375,2000,0,200));
  //BookHisto(new TH2F("Pinunu_ibvsmmiss_test_3pi","",375,-0.1,0.14375,2000,0,200));

  // Multiple gtk rejection
  BookHisto(new TH2F("MultiGTK_zvtxvsp","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("MultiGTK_zvsz","",250,50000,200000,250,50000,200000));
  BookHisto(new TH2F("MultiGTK_mvsdz","",250,-10000,10000,375,-0.1,0.14375));
  BookHisto(new TH2F("MultiGTK_mvsm","",375,-0.1,0.14375,375,-0.1,0.14375));
  BookHisto(new TH2F("MultiGTKHit_dvsz","",250,50000,200000,800,0.,400.));
  BookHisto(new TH2F("MultiGTKHit_dvsz2","",250,50000,200000,800,0.,400.));
  BookHisto(new TH2F("MultiGTKHit_dxdy","",800,-200.,200.,800,-200,200.));
  BookHisto(new TH2F("MultiGTKHit_xytrack_gtk3","",400,-100.,100.,400,-100,100.));
  BookHisto(new TH2F("MultiGTKHit_xytrack_fcoe","",400,-100.,100.,400,-100,100.));
  BookHisto(new TH2F("MultiGTKHit_xytrack_dipo","",400,-100.,100.,400,-100,100.));
  BookHisto(new TH2F("MultiGTKHit_xy","",400,-100.,100.,400,-100,100.));
  BookHisto(new TH2F("MultiGTKHit_tot","",250,50000,200000,200,0.,50.));

  BookHisto(new TH2F("Pinunu_badgtkmatching_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Snake_r_vs_z","",250,50000,200000,250.,0.,1000.));
  BookHisto(new TH2F("Snakepos_r_vs_z","",250,50000,200000,250.,0.,1000.));
  BookHisto(new TH2F("Snake_r_vs_z_aftercut","",250,50000,200000,250.,0.,1000.));
  //BookHisto(new TH2F("NoSnakepos_rvsz","",250,50000,200000,250.,0.,1000.));

  BookHisto(new TH2F("Pinunu_fidvolout_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_fidvolout_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_fidvolout_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_fidvolout_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  BookHisto(new TH2F("Pinunu_fidvol_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_fidvol_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_fidvol_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_fidvol_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  BookHisto(new TH2F("Pinunu_chanti_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_chanti_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_chanti_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_chanti_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  BookHisto(new TH2F("Pinunu_nobeambg_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_nobeambg_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_nobeambg_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_nobeambg_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_nobeambg_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));


  //EarlyDecay suppression
  BookHisto(new TH2F("Pinunu_earlydecay_mmiss_vs_p_single","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_earlydecay_mmiss_vs_mmiss_single","",425,-0.40875,0.14375,425,-0.40875,0.14375));
  BookHisto(new TH2F("Pinunu_earlydecay_xych1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_xylkr","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_zp","",250,93000,100000,200,0,100));
  BookHisto(new TH2F("Pinunu_earlydecay_zm","",250,93000,100000,425,-0.40875,0.14375));
  BookHisto(new TH2F("Pinunu_earlydecay_dcda","",100,0,400,100,0,20));
  BookHisto(new TH2F("Pinunu_earlydecay_diffcda","",100,0,100,210,-20,400));
  BookHisto(new TH2F("Pinunu_earlydecay_xych1_earlydec0","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_xylkr_earlydec0","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_zp_earlydec0","",250,93000,100000,200,0,100));
  BookHisto(new TH2F("Pinunu_earlydecay_zm_earlydec0","",250,93000,100000,425,-0.40875,0.14375));
  BookHisto(new TH2F("Pinunu_earlydecay_dcda_earlydec0","",100,0,400,100,0,20));
  BookHisto(new TH2F("Pinunu_earlydecay_diffcda_earlydec0","",100,0,100,210,-20,400));
  BookHisto(new TH2F("Pinunu_earlydecay_xych1_earlydec1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_xylkr_earlydec1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_dcda_earlydec1","",100,0,400,100,0,400));
  BookHisto(new TH2F("Pinunu_earlydecay_xych1_kmu2","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_xylkr_kmu2","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_dcda_kmu2","",100,0,400,100,0,20));
  BookHisto(new TH2F("Pinunu_earlydecay_xych1_3pion","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_xylkr_3pion","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_earlydecay_dcda_3pion","",100,0,400,100,0,20));


  BookHisto(new TH2F("Pinunu_noearlydecay_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_noearlydecay_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_noearlydecay_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_noearlydecay_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_noearlydecay_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  // Kinematic tails
  BookHisto(new TH2F("Pinunu_tails_spec_nhits","",200,0,100,100,0,100));
  BookHisto(new TH2F("Pinunu_tails_spec_dthetax","",100,-0.001,0.001,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_tails_spec_dthetay","",100,-0.001,0.001,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_tails_spec_qual1","",100,0,10,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_tails_vertex_xz","",250,50000,200000,200,-200,200));
  BookHisto(new TH2F("Pinunu_tails_vertex_yz","",250,50000,200000,200,-200,200));
  BookHisto(new TH2F("Pinunu_tails_kaon_xy","",50,-50,50,50,-50,50));
  BookHisto(new TH2F("Pinunu_tails_thetavsp","",200,0,100,200,0.,0.02));
  BookHisto(new TH2F("Pinunu_tails_dktagVSdgtk_chod","",200,-10,10,200,-10,10));
  BookHisto(new TH2F("Pinunu_tails_dchodVSdktag_gtk","",200,-10,10,200,-10,10));
  BookHisto(new TH2F("Pinunu_tails_dtktag","",100,-10,10,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_tails_dtchod","",100,-10,10,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_tails_dtlkr","",100,-20,20,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_tails_dtspec","",100,-50,50,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_tails_dktagVSdgtk_chod_check","",200,-10,10,200,-10,10));
  BookHisto(new TH2F("Pinunu_tails_kaon_xy_check","",200,-50,50,200,-50,50));
  BookHisto(new TH1F("Pinunu_tails_kaon_p_check","",100,70,80));
  BookHisto(new TH2F("Pinunu_tails_kaon_thetaxy_check","",50,-0.001,0.001,50,-0.001,0.001));

  BookHisto(new TH2F("Pinunu_notails_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_notails_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_notails_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_notails_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_notails_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));

  BookHisto(new TH2F("Pinunu_pidrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_pidrej_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_pidrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_pidrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_pidrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));


  BookHisto(new TH2F("Pinunu_mmiss_vs_p_lkr","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_mmiss_vs_p_lav","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_mmiss_vs_p_irc","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_mmiss_vs_p_sac","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_mmiss_vs_p_sav","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_mmiss_vs_p_auxlkr","",200,0,100,375,-0.1,0.14375));


  BookHisto(new TH2F("Pinunu_phrej_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_phrej_mmiss_vs_mmissrich","",375,-0.1,0.14375,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_phrej_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("Pinunu_phrej_mmiss_vs_zvtx","",500,50000,200000,375,-0.1,0.14375));
  BookHisto(new TH2F("Pinunu_phrej_x_vs_y_straw1","",240,-1200,1200,240,-1200,1200));
  BookHisto(new TH2F("Pinunu_phrej_x_vs_y_lkr","",240,-1200,1200,240,-1200,1200));



  BookHisto(new TH2F("BeamPion_Mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("BeamPion_richradius_vs_p","",200,0,100,300,0,300));
  BookHisto(new TH2F("BeamPion_after_pid_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("BeamPion_mmiss_vs_p_fidvol","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_mmiss_vs_p_nomuv3","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nobeamkine_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("BeamPion_nobeamkine_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_notails_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("BeamPion_notails_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_richmult_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("BeamPion_richmult_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nochanti_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("BeamPion_nochanti_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_final_p_vs_zvtx","",250,50000,200000,200,0,100));
  BookHisto(new TH2F("BeamPion_final_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));


  //BookHisto(new TH2F("BeamPion_mmiss_vs_p_isgtk_nomuv3","",200,0,100,375,-0.1,0.14375));
  //BookHisto(new TH2F("BeamPion_mmiss_vs_p_isgtk_norichkaon","",200,0,100,375,-0.1,0.14375));

  //BookHisto(new TH2F("BeamPion_cedar_Mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  //BookHisto(new TH2F("BeamPion_nocedar_Mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  //BookHisto(new TH2F("BeamPion_Mmiss_vs_p_nomip","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_RPhVeto_mmiss_vs_p_nolavtrack","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_RPhVeto_mmiss_vs_p_nolavaftertrack","",200,0,100,375,-0.1,0.14375));

  BookHisto(new TH2F("BeamPion_RPhVeto_mmiss_vs_p_nolkr","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_RPhVeto_mmiss_vs_p_nolkraux","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_RPhVeto_mmiss_vs_p_nolav","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_RPhVeto_mmiss_vs_p_nosav","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_RPhVeto_mmiss_vs_p_nogamma","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nogamma_mmiss_vs_chodmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nogamma_mmiss_vs_newchodmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nogamma_mmiss_vs_chodnewchodmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nogamma_mmiss_vs_chodslabsintime","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nogamma_mmiss_vs_lkrmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nogamma_mmiss_vs_lkrhadrmult","",100,0,100,375,-0.1,0.14375));

  BookHisto(new TH2F("BeamPion_nogamma_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_mmiss_vs_chodmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_mmiss_vs_newchodmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_mmiss_vs_chodnewchodmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_mmiss_vs_lkrmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_mmiss_vs_lkrhadrmult","",100,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_mmiss_vs_chodslabsintime","",100,0,100,375,-0.1,0.14375));


  BookHisto(new TH1F("BeamPion_muv0_dt","",100,-100,100));
  BookHisto(new TH1F("BeamPion_muv0_dtmin","",100,-100,100));
  BookHisto(new TH2F("BeamPion_muv0_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH1F("BeamPion_hac_dt","",800,-100,100));
  BookHisto(new TH1F("BeamPion_hac_dtmin","",800,-100,100));
  BookHisto(new TH2F("BeamPion_hac_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

  BookHisto(new TH2F("BeamPion_mult_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nomult_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));

  BookHisto(new TH2F("BeamPion_nomultmuv0_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nomulthac_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_nomultmuv0hac_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
  BookHisto(new TH2F("BeamPion_Mmiss_vs_p_nosegments","",200,0,100,375,-0.1,0.14375));
}

void BeamTrackSelection::InitHist(){
}

void BeamTrackSelection::DefineMCSimple(){
}

void BeamTrackSelection::StartOfRunUser(){
}

void BeamTrackSelection::StartOfBurstUser(){
}

void BeamTrackSelection::Process(int iEvent){

  if(GetWithMC()){

    //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}

  }
  fCedarEvent        = (TRecoCedarEvent*)GetEvent("Cedar");
  fGigaTrackerEvent  = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
  fCHANTIEvent       = (TRecoCHANTIEvent*)GetEvent("CHANTI");
  fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
  fLAVEvent          = (TRecoLAVEvent*)GetEvent("LAV");
  fRICHEvent         = (TRecoRICHEvent*)GetEvent("RICH");
  fCHODEvent         = (TRecoCHODEvent*)GetEvent("CHOD");
  fNewCHODEvent      = (TRecoNewCHODEvent*)GetEvent("NewCHOD");
  fIRCEvent          = (TRecoIRCEvent*)GetEvent("IRC");
  fLKrEvent          = (TRecoLKrEvent*)GetEvent("LKr");
  fMUV0Event         = (TRecoMUV0Event*)GetEvent("MUV0");
  fMUV1Event         = (TRecoMUV1Event*)GetEvent("MUV1");
  fMUV2Event         = (TRecoMUV2Event*)GetEvent("MUV2");
  fMUV3Event         = (TRecoMUV3Event*)GetEvent("MUV3");
  fSACEvent          = (TRecoSACEvent*)GetEvent("SAC");
  fHACEvent          = (TRecoHACEvent*)GetEvent("HAC");

  // Clear physics objects
  fStrawRapper->Clear();

  // Trigger selection
  fHeader = GetWithMC() ? 0 : GetEventHeader();
  fL0Data = GetWithMC() ? 0 : GetL0Data();

  if(!GetWithMC()){

    fFineTriggerTime = fHeader->GetFineTime()*fFineTimeScale;
  // General cut
  Int_t eventqualitymask = fHeader->GetEventQualityMask();
  if (eventqualitymask>0) return;
  }

  // Flags
  fGTKFlag = *(Int_t *)GetOutput("MainAnalysis.GTKFlag",fState); // 1 no GTK, 0 GTK
  fZVertexCut = !fGTKFlag?105000.:125000.;
  fYear = *(Int_t *)GetOutput("MainAnalysis.Year",fState);

  if (fYear==2016) {
    fTrimTheta = 0.00122;
    fTrimKick = 0.0913;
    fThetaXCenter = 0.00002;
    fThetaYCenter = 0.00002;
  }

  //EventHeader* header = GetEventHeader();
  //if(header->GetEventNumber() != 1441359) return;

  if(!IsOTSGoodEvent()) return;
  //for(int iC = 0; iC < fNBeamPions; iC++){

  // Define candidate track
  fTrack = &fDownstreamTrack[fIdTrack];
  fPTrack = fTrack->GetMomentum().P();
  fBeamTrack = &fUpstreamTrack[fIdTrack];
  fPBeamTrack = fBeamTrack->GetMomentum().P();
  fStrawRapper->AnalyzeHits(fSpectrometerEvent,fTrack->GetTrackID(),9999999);
  //fVertex = NominalKaon();
  fVertex = fBeamTrack->GetVertex();
  fMMiss = (fBeamTrack->GetMomentum()-fTrack->GetMomentum()).Mag2();
  //fMMiss = (fKaonNominalMomentum-fTrack->GetMomentum()).Mag2();
  fMmuMiss = (fBeamTrack->GetMomentum()-fTrack->GetMuonMomentum()).Mag2();
  fXAtStraw1 = fTrack->GetPositionAtStraw(0).X() - xSTRAW_station[0];
  fYAtStraw1 = fTrack->GetPositionAtStraw(0).Y();
  fXAtLKr = fTrack->GetPositionAtLKr().X();
  fYAtLKr = fTrack->GetPositionAtLKr().Y();

  // Photons
  fIsLKr = fTrack->GetIsPhotonLKrCandidate();
  fIsNewLKr = fTrack->GetIsPhotonNewLKrCandidate();
  fIsLAV = fTrack->GetIsPhotonLAVCandidate();
  fIsIRC = fTrack->GetIsPhotonIRCCandidate();
  fIsSAC = fTrack->GetIsPhotonSACCandidate();
  fIsSAV = fTrack->GetIsPhotonSAVCandidate();
  //std::cout << "it`s not fine" << std::endl;

  // fSegmentIB = fStrawRapper->ReconstructSegments(&fVertex);
  fSegmentIB = fStrawRapper->ReconstructSegments(&fVertex,fBeamTrack->GetTime(),-1);
  PinunuLikeAnalysis();
  ScatteredPionAnalysis();
  //}


}


////////////////////
// Good OTS event //
////////////////////
Bool_t BeamTrackSelection::IsOTSGoodEvent() {

  // Check if the event has passed the single track analysis
  Bool_t ots = *(Bool_t *)GetOutput("MainAnalysis.OneTrackEvent",fState);
  if (!ots) return false;
  fNBeamPions = *(Int_t *)GetOutput("MainAnalysis.NBeamPions",fState);
  if (!fNBeamPions) return false;

  // Define downstream and upstream array
  fDownstreamTrack = (DownstreamParticle *)GetOutput("MainAnalysis.BeamPionDownstreamTrack",fState);
  fUpstreamTrack = (UpstreamParticle *)GetOutput("MainAnalysis.BeamPionUpstreamTrack",fState);

  // Select only event matching the trigger in time
  fIdTrack = fNTracks>1 ? SelectOneParticle() : 0;
  if (fIdTrack==-1) return false;

  return true;
}

Int_t BeamTrackSelection::PinunuLikeAnalysis(){

  if(!BeamKinematics(0)) return 0;

  FillHisto("Pinunu_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
  FillHisto("Pinunu_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("Pinunu_mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("Pinunu_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
  FillHisto("Pinunu_x_vs_y_lkr",fXAtLKr,fYAtLKr);

  if(BeamBackgroundCut(1)) return 0;

  FillHisto("Pinunu_nobeambg_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
  FillHisto("Pinunu_nobeambg_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("Pinunu_nobeambg_mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("Pinunu_nobeambg_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
  FillHisto("Pinunu_nobeambg_x_vs_y_lkr",fXAtLKr,fYAtLKr);

  //Early decay suppression
  if(EarlyDecay(0)) return 0;

  FillHisto("Pinunu_noearlydecay_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
  FillHisto("Pinunu_noearlydecay_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("Pinunu_noearlydecay_mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("Pinunu_noearlydecay_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
  FillHisto("Pinunu_noearlydecay_x_vs_y_lkr",fXAtLKr,fYAtLKr);

  if(KinematicTails(0)) return 0;

  FillHisto("Pinunu_notails_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
  FillHisto("Pinunu_notails_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("Pinunu_notails_mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("Pinunu_notails_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
  FillHisto("Pinunu_notails_x_vs_y_lkr",fXAtLKr,fYAtLKr);

  if(!PionPID(0)) return 0;
  if(!fTrack->GetRICHSingleIsCandidate()) return 0;

  TLorentzVector richp   = fTrack->GetRICHSingleMomentum();
  double mmissrich = (fBeamTrack->GetMomentum()-richp).Mag2();
  double sringmass = fTrack->GetRICHSingleMass();

  FillHisto("Pinunu_pidrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
  FillHisto("Pinunu_pidrej_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("Pinunu_pidrej_mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("Pinunu_pidrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
  FillHisto("Pinunu_pidrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);

  // Photon rejection
  if (fIsLKr) return 0;
  FillHisto("Pinunu_mmiss_vs_p_lkr",fPTrack,fMMiss);
  if (fIsLAV) return 0;
  FillHisto("Pinunu_mmiss_vs_p_lav",fPTrack,fMMiss);
  if (fIsIRC) return 0;
  FillHisto("Pinunu_mmiss_vs_p_irc",fPTrack,fMMiss);
  if (fIsSAC) return 0;
  FillHisto("Pinunu_mmiss_vs_p_sac",fPTrack,fMMiss);
  if (fIsSAV) return 0;
  FillHisto("Pinunu_mmiss_vs_p_sav",fPTrack,fMMiss);
  if (fIsNewLKr) return 0;
  FillHisto("Pinunu_mmiss_vs_p_auxlkr",fPTrack,fMMiss);

  if(fPTrack < 35 && fPTrack > 15 )
    FillHisto("Pinunu_phrej_mmiss_vs_mmissrich", fMMiss,mmissrich);

  FillHisto("Pinunu_phrej_mmiss_vs_zvtx",fVertex.Z(),fMMiss);
  FillHisto("Pinunu_phrej_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("Pinunu_phrej_mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("Pinunu_phrej_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
  FillHisto("Pinunu_phrej_x_vs_y_lkr",fXAtLKr,fYAtLKr);

}

//Scattered 75 GeV Pions
Int_t BeamTrackSelection::ScatteredPionAnalysis(){

  Bool_t isRingCandidate = (fTrack->GetRICHMultiIsCandidate()<2 && fTrack->GetRICHSingleIsCandidate()) ? 1 : 0;


  FillHisto("BeamPion_Mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("BeamPion_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("BeamPion_richradius_vs_p",fPTrack, fTrack->GetRICHSingleRadius());

    if(!PionPID(1)) return 0;

  FillHisto("BeamPion_after_pid_p_vs_zvtx",fVertex.Z(),fPTrack);

  if(fVertex.Z() > 104500 || fVertex.Z() < 101000 ) return 0;
  FillHisto("BeamPion_mmiss_vs_p_fidvol",fPTrack,fMMiss);

  if (fTrack->GetMUV3ID() > -1) return 0;
  FillHisto("BeamPion_mmiss_vs_p_nomuv3",fPTrack,fMMiss);

  if(!BeamKinematics(1)) return 0;
  FillHisto("BeamPion_nobeamkine_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("BeamPion_nobeamkine_mmiss_vs_p",fPTrack,fMMiss);

  if(KinematicTails(1)) return 0;
  FillHisto("BeamPion_notails_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("BeamPion_notails_mmiss_vs_p",fPTrack,fMMiss);

  bool isRICHMultiRing = fTrack->GetRICHMultiIsCandidate();

  if(!isRICHMultiRing) return 0;
  FillHisto("BeamPion_richmult_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("BeamPion_richmult_mmiss_vs_p",fPTrack,fMMiss);


  // Select Elastic component
  if (fBeamTrack->GetIsCHANTICandidate()) return 0;
  FillHisto("BeamPion_nochanti_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("BeamPion_nochanti_mmiss_vs_p",fPTrack,fMMiss);

  if (fPTrack<72.0||fPTrack>77.5) return 0;
  if (!fTrack->GetRICHMultiIsCandidate()) return 0;
  if (fTrack->GetRICHMultiMostLikelyHypothesis()==4) return 0;
  FillHisto("BeamPion_final_p_vs_zvtx",fVertex.Z(),fPTrack);
  FillHisto("BeamPion_final_mmiss_vs_p",fPTrack,fMMiss);

  bool multiplicity   = false;
  Int_t muv1mult      = fTrack->GetMUV1OuterNhits();
  Int_t muv2mult      = fTrack->GetMUV2OuterNhits();
  double chodmult     = fTrack->GetCHODMult();
  double chodslabsintime = fTrack->GetCHODSlabsInTime();
  double chodnewchodmult = fTrack->GetCHODNewCHODMult();
  double newchodmult  = fTrack->GetNewCHODMult();
  double lkrmult      = fTrack->GetLKrMult();
  double lkrhadrmult  = fTrack->GetLKrHadrMult();

  bool lavtrack       = fTrack->GetIsLAVTrack();
  if(!lavtrack){

    FillHisto("BeamPion_RPhVeto_mmiss_vs_p_nolavtrack",fPTrack,fMMiss);
    if(!fIsLAV)
      FillHisto("BeamPion_RPhVeto_mmiss_vs_p_nolavaftertrack",fPTrack,fMMiss);

  }

  bool isPhoton = fIsLKr || fIsNewLKr || fIsLAV || fIsSAV || fIsSAC || fIsIRC;

  if(chodmult > 0) multiplicity = true;
  if(newchodmult > 0) multiplicity = true;
  if(chodnewchodmult > 0) multiplicity = true;
  if(chodslabsintime > 5) multiplicity = true;

  if(!fIsLKr)
    FillHisto("BeamPion_RPhVeto_mmiss_vs_p_nolkr",fPTrack,fMMiss);
  if(!fIsLKr && !fIsNewLKr)
    FillHisto("BeamPion_RPhVeto_mmiss_vs_p_nolkraux",fPTrack,fMMiss);
  if(!fIsLAV)
    FillHisto("BeamPion_RPhVeto_mmiss_vs_p_nolav",fPTrack,fMMiss);
  if(!fIsSAV && !fIsIRC && !fIsSAC)
    FillHisto("BeamPion_RPhVeto_mmiss_vs_p_nosav",fPTrack,fMMiss);
  if(!isPhoton){

    FillHisto("BeamPion_RPhVeto_mmiss_vs_p_nogamma",fPTrack,fMMiss);
    FillHisto("BeamPion_nogamma_mmiss_vs_chodmult",chodmult,fMMiss);
    FillHisto("BeamPion_nogamma_mmiss_vs_newchodmult",newchodmult,fMMiss);
    FillHisto("BeamPion_nogamma_mmiss_vs_chodnewchodmult",chodnewchodmult,fMMiss);
    FillHisto("BeamPion_nogamma_mmiss_vs_chodslabsintime",chodslabsintime,fMMiss);
    FillHisto("BeamPion_nogamma_mmiss_vs_lkrmult", lkrmult,fMMiss);
    FillHisto("BeamPion_nogamma_mmiss_vs_lkrhadrmult", lkrhadrmult,fMMiss);

    if(multiplicity) FillHisto("BeamPion_nogamma_eq_mmiss_vs_chodmult_f",chodmult,fMMiss);

  }

  if(fIsLAV || fIsSAV || fIsSAC || fIsIRC) return 0;

  FillHisto("BeamPion_nogamma_mmiss_vs_p",fPTrack,fMMiss);
  FillHisto("BeamPion_mmiss_vs_chodmult",chodmult,fMMiss);
  FillHisto("BeamPion_mmiss_vs_chodnewchodmult",chodnewchodmult,fMMiss);
  FillHisto("BeamPion_mmiss_vs_newchodmult",newchodmult,fMMiss);
  FillHisto("BeamPion_mmiss_vs_lkrmult", lkrmult,fMMiss);
  FillHisto("BeamPion_mmiss_vs_lkrhadrmult", lkrhadrmult,fMMiss);
  FillHisto("BeamPion_mmiss_vs_chodslabsintime",chodslabsintime,fMMiss);

  if(multiplicity) {
    FillHisto("BeamPion_mult_mmiss_vs_p",fPTrack,fMMiss);
    return 0;
  }

    FillHisto("BeamPion_nomult_mmiss_vs_p",fPTrack,fMMiss);

//Check the timing in muv0
  std::map<double,int> muv0map;
  TClonesArray& muv0hits = *(fMUV0Event->GetHits());
  for(int iC=0;iC < fMUV0Event->GetNHits();iC++){

    TRecoMUV0Hit* muv0 = (TRecoMUV0Hit*)muv0hits[iC];
    double dtime = fTrack->GetRICHSingleTime() - muv0->GetTime();
    FillHisto(Form("BeamPion_muv0_dt"),dtime);
    muv0map.insert(std::make_pair(fabs(dtime),iC));
  }

  fIsMUV0 = false;

  if(muv0map.size()!=0){
    int imin = muv0map.begin()->second;
    TRecoMUV0Hit* muv0min = (TRecoMUV0Hit*)muv0hits[imin];
    double mintime = fTrack->GetRICHSingleTime() - muv0min->GetTime();
    FillHisto(Form("BeamPion_muv0_dtmin"),mintime);
    FillHisto(Form("BeamPion_muv0_mmiss_vs_p"),fPTrack,fMMiss);

      if(mintime > -10 && mintime <  8) fIsMUV0 = true;

    }

  //Check the timing in hasc
  std::map<double,int> hacmap;
  TClonesArray& hachits = *(fHACEvent->GetHits());
  for(int iC=0;iC < fHACEvent->GetNHits();iC++){

    TRecoHACHit* hac = (TRecoHACHit*)hachits[iC];
    double dtime = fTrack->GetRICHSingleTime() - hac->GetTime();

    FillHisto(Form("BeamPion_hac_dt"),dtime);
    hacmap.insert(std::make_pair(fabs(dtime),iC));
  }

  fIsHAC=false;

  if(hacmap.size()!=0){
    int imin = hacmap.begin()->second;
    TRecoHACHit* hacmin = (TRecoHACHit*)hachits[imin];
    double mintime = fTrack->GetRICHSingleTime() - hacmin->GetTime();

    FillHisto(Form("BeamPion_hac_dtmin"),mintime);
    FillHisto(Form("BeamPion_hac_mmiss_vs_p"),fPTrack,fMMiss);

    if(mintime > -3 && mintime <  3) fIsHAC = true;

  }

  if(!fIsHAC) FillHisto(Form("BeamPion_nomulthac_mmiss_vs_p"),fPTrack,fMMiss);
  if(!fIsMUV0) FillHisto(Form("BeamPion_nomultmuv0_mmiss_vs_p"),fPTrack,fMMiss);
  if(!fIsMUV0&&!fIsMUV0) FillHisto(Form("BeamPion_nomultmuv0hac_mmiss_vs_p"),fPTrack,fMMiss);

  if(fSegmentIB<0.5) return 0;
  FillHisto("BeamPion_Mmiss_vs_p_nosegments",fPTrack,fMMiss);

  return 1;
}

void BeamTrackSelection::PostProcess(){

}

void BeamTrackSelection::EndOfBurstUser(){
}

void BeamTrackSelection::EndOfRunUser(){

}

void BeamTrackSelection::EndOfJobUser(){
  SaveAllPlots();
}

void BeamTrackSelection::DrawPlot(){
}

BeamTrackSelection::~BeamTrackSelection(){
}

TVector3 BeamTrackSelection::NominalKaon() {

  Double_t pkaon = 75.0;
  Double_t tthx = 0.0012;
  Double_t tthy = 0.;
  if (fYear==2015) {
    pkaon = 74.9;
    tthx = 0.00118; // 3809
    tthy = 0.;
  }
  if (fYear==2016) {
    pkaon = 74.9;
    tthx = 0.00122;
    tthy = 0.000026;
  }

  TLorentzVector k4mom;
  Double_t pz = pkaon/sqrt(1.+tthx*tthx+tthy*tthy);
  Double_t px = pz*tthx;
  Double_t py = pz*tthy;
  k4mom.SetXYZM(px,py,pz,0.001*Constants::MKCH);
  fKaonNominalMomentum = k4mom;

  TLorentzVector p4mom = fTrack->GetMomentum();
  //Double_t ptrack = track->GetMomentum()*0.001;
  //Double_t norm   = 1 / (1 + track->GetSlopeXBeforeMagnet()*track->GetSlopeXBeforeMagnet() + track->GetSlopeYBeforeMagnet()*track->GetSlopeYBeforeMagnet());
  //Double_t px = ptrack*norm*track->GetSlopeXBeforeMagnet();
  //Double_t py = ptrack*norm*track->GetSlopeYBeforeMagnet();
  //Double_t pz = ptrack*norm;
  //p4mom.SetXYZM(px,py,pz,0.001*Constants::MPI);

  // TVector3 p3pion = track->GetThreeMomentumBeforeMagnet();
  TVector3 p3pion = p4mom.Vect();
  TVector3 pospion = fTrack->GetPositionAtStraw(0);
  TVector3 kmom = k4mom.Vect();
  TVector3 kpos(0,0,101800);
  TVector3 pmom = p4mom.Vect();
  Double_t cda = -99999.;

  fcda = 9999999.;
  //// Vertex: first iteration without momentum correction for blue field
  TVector3 vertex = fTool->SingleTrackVertex(kmom,p3pion,kpos,pospion,fcda); // First interation: no blue field

  return vertex;


}

Int_t BeamTrackSelection::SelectOneParticle() {

  // Define 8 variables according to the numbers of detectors
  // used to define the time of a particle matching a track
  Double_t dTime[8];
  Double_t minTime[8];
  Int_t minTimeID[8];
  for (Int_t jtime=0; jtime<8; jtime++) {
    minTime[jtime] = 9999999.;
    minTimeID[jtime] = -1;
  }

  // Loop on the tracks
  for (Int_t jTrack=0; jTrack<fNTracks; jTrack++) {
    DownstreamParticle *track = &fDownstreamTrack[jTrack];
    UpstreamParticle *beamtrack = &fUpstreamTrack[jTrack];
    Bool_t isRingCandidate = (track->GetRICHMultiIsCandidate() && track->GetRICHSingleIsCandidate()) ? 1 : 0;
    dTime[0] = track->GetTime()-fFineTriggerTime; // Spectrometer
    dTime[1] = track->GetCHODTime()-fFineTriggerTime; // CHOD
    dTime[2] = track->GetLKrTime()-fFineTriggerTime; // LKr
    dTime[3] = beamtrack->GetTime()-fFineTriggerTime; // GTK
    dTime[4] = isRingCandidate ? track->GetRICHMultiTime()-fFineTriggerTime : -9999999.; // RICH multi
    dTime[5] = isRingCandidate ? track->GetRICHSingleTime()-fFineTriggerTime : -9999999.; // RICH single
    dTime[6] = track->GetMUV1ID()>-1 ? track->GetMUV1Time()-fFineTriggerTime : -9999999.; // MUV1
    dTime[7] = track->GetMUV2ID()>-1 ? track->GetMUV2Time()-fFineTriggerTime : -9999999.; // MUV2
    for (Int_t jtime=0; jtime<8; jtime++) {
      if (fabs(dTime[jtime])<fabs(minTime[jtime])) {
        minTime[jtime] = dTime[jtime];
        minTimeID[jtime] = jTrack;
      }
    }
  }

  // Test plot
  Int_t multtrack = 1;
  Int_t trackidmin = minTimeID[1]; // CHOD as a reference
  for (Int_t jtime=2; jtime<8; jtime++) { // straw time not checked (bad resolution)
    if (minTimeID[jtime]>-1 && minTimeID[jtime]!=trackidmin) multtrack++;
  }
  if (multtrack>1) return -1;
  FillHisto("onetrack_gooddtchod" ,minTime[1]);

  return trackidmin;
}

Bool_t BeamTrackSelection::BeamBackgroundCut(Int_t flag) {
  // Test GTK candidate for bad GTK association
  bool isAGoodVertex = false;
  for (int jG(0); jG<fBeamTrack->GetNGTKCandidates()-1; jG++) {
    GigaTrackerCandidate cand = fBeamTrack->GetGTKCandidate(jG);
    Double_t zvertex = cand.GetVertex().Z();

    if(flag){
      FillHisto("MultiGTK_zvtxvsp",zvertex,fPTrack);
      FillHisto("MultiGTK_zvsz",zvertex,fVertex.Z());
      FillHisto("MultiGTK_mvsdz",zvertex-fVertex.Z(),fMMiss);
    }
    Double_t partrack[4] = {cand.GetMomentum()*1000,cand.GetSlopeXZ(),cand.GetSlopeYZ(),0.493667};
    TLorentzVector pmom = fTool->Get4Momentum(partrack);
    Double_t mmiss = (pmom-fTrack->GetMomentum()).Mag2();
    if(flag)
      FillHisto("MultiGTK_mvsm",mmiss,fMMiss);

    if (zvertex<105000.&&zvertex>100000.) isAGoodVertex = true;
  }
  if (isAGoodVertex) return 1;

  if(flag) FillHisto("Pinunu_badgtkmatching_mmiss_vs_p",fPTrack,fMMiss);

  double rstraw1 = sqrt(fXAtStraw1*fXAtStraw1+fYAtStraw1*fYAtStraw1);
  if(flag) FillHisto("Snake_r_vs_z",fVertex.Z(),rstraw1);

  //Anti-snakes zvtx vs rstraw1 cut
  double b1 = -0.004;
  double a1 = 315-b1*105000.;
  double r115 = a1+b1*115000.;
  double b2 = -(900-r115)/10000.;
  double a2 = 900-b2*105000.;
  double b3 = -0.00983333333;
  double a3 = 780-b3*105000.;
  double cut1 = a1+b1*fVertex.Z();
  double cut2 = a2+b2*fVertex.Z();
  double cut3 = a3+b3*fVertex.Z();
  bool isSignal = rstraw1>cut1 && rstraw1>cut2 && rstraw1<cut3 && fVertex.Z()<165000.;
  bool isSnakes = !isSignal;

  if(flag && isSnakes) FillHisto("Snakepos_r_vs_z",fVertex.Z(),rstraw1);

  // Test GTK3 hit
  bool isInteraction= false;
  bool highToT = false;
  TRecoSpectrometerCandidate *strack = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fTrack->GetTrackID());
  TVector3 pmom;
  TVector3 posGTK3 = fTool->GetPositionUpstream(strack,&pmom,102400.); // GTK3
  TVector3 posFCOE = fTool->GetPositionUpstream(strack,&pmom,101255.); // End final collimator
  TVector3 posDIPO = fTool->GetPositionUpstream(strack,&pmom,99460.); // End last achromat dipole
  for (int jH(0); jH<fBeamTrack->GetNGTK3Hits(); jH++) {
    Int_t idh = fBeamTrack->GetGTK3Hit(jH);
    TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*) fGigaTrackerEvent->GetHit(idh);
    Double_t dist = (hit->GetPosition()-posGTK3).XYvector().Mod();
    Double_t dx = (hit->GetPosition()-posGTK3).X();
    Double_t dy = (hit->GetPosition()-posGTK3).Y();
    Double_t dist2 = (hit->GetPosition()-fBeamTrack->GetPosition()).XYvector().Mod();
    if (!fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack<35) FillHisto("MultiGTKHit_dvsz",fVertex.Z(),dist);
    if (dist2>0)
      if(flag)
        FillHisto("MultiGTKHit_dvsz2",fVertex.Z(),dist2);
    if(flag){
      FillHisto("MultiGTKHit_dxdy",dx,dy);
      FillHisto("MultiGTKHit_xy",hit->GetPosition().X(),hit->GetPosition().Y());
      FillHisto("MultiGTKHit_tot",fVertex.Z(),hit->GetToT());
    }

    if (dist<24.) isInteraction = true; // Against scattering tails in straws
    if (hit->GetToT()>23) highToT = true;
  }
  if (!isInteraction && !fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack<35) {
    if(flag){
      FillHisto("MultiGTKHit_xytrack_gtk3",posGTK3.X(),posGTK3.Y());
      FillHisto("MultiGTKHit_xytrack_fcoe",posFCOE.X(),posFCOE.Y());
      FillHisto("MultiGTKHit_xytrack_dipo",posDIPO.X(),posDIPO.Y());
    }
  }
  if (fabs(posGTK3.X())<30.&&fabs(posGTK3.Y())<15.) isInteraction = true; // Against inefficinecy in GTK3

  for (int jH(0); jH<fBeamTrack->GetNGTK1Hits(); jH++) {
    Int_t idh = fBeamTrack->GetGTK1Hit(jH);
    TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*) fGigaTrackerEvent->GetHit(idh);

    if(flag)
      FillHisto("MultiGTKHit_tot",fVertex.Z(),hit->GetToT());
    if (hit->GetToT()>23) highToT = true;
  }
  for (int jH(0); jH<fBeamTrack->GetNGTK2Hits(); jH++) {
    Int_t idh = fBeamTrack->GetGTK2Hit(jH);
    TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*) fGigaTrackerEvent->GetHit(idh);
    if(flag)
      FillHisto("MultiGTKHit_tot",fVertex.Z(),hit->GetToT());
    if (hit->GetToT()>23) highToT = true;
  }

  //Beam background cuts
  if (highToT) return 1;
  if (isInteraction) return 1;
  ////  if (fBeamTrack->GetNGTK2Hits()<fBeamTrack->GetNGTK3Hits()) return 1;
  if(flag && !fBeamTrack->GetIsCHANTICandidate() && fPTrack>15 && fPTrack < 35) FillHisto("Snake_r_vs_z_aftercut",fVertex.Z(),rstraw1);
  if(flag && isSnakes) return 1;
  // Vertex cut
  if (fVertex.Z()<fZVertexCut||fVertex.Z()>165000.) {
    if(flag){
      FillHisto("Pinunu_fidvolout_p_vs_zvtx",fVertex.Z(),fPTrack);
      FillHisto("Pinunu_fidvolout_mmiss_vs_p",fPTrack,fMMiss);
      FillHisto("Pinunu_fidvolout_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
      FillHisto("Pinunu_fidvolout_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }
    return 1;
  }
  if(flag){
    FillHisto("Pinunu_fidvol_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_fidvol_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_fidvol_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_fidvol_x_vs_y_lkr",fXAtLKr,fYAtLKr);
  }

  // CHANTI cut
  if (fBeamTrack->GetIsCHANTICandidate()) {
    if(flag){
      FillHisto("Pinunu_chanti_p_vs_zvtx",fVertex.Z(),fPTrack);
      FillHisto("Pinunu_chanti_mmiss_vs_p",fPTrack,fMMiss);
      FillHisto("Pinunu_chanti_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
      FillHisto("Pinunu_chanti_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }
    return 1;
  }

  return 0;
}

Bool_t BeamTrackSelection::BeamKinematics(Int_t flag) {
  fTrimTheta = fTrimKick/fPBeamTrack;

  Double_t kaon_thetax = fBeamTrack->GetMomentum().Px()/fBeamTrack->GetMomentum().Pz() - fTrimTheta;
  Double_t kaon_thetay = fBeamTrack->GetMomentum().Py()/fBeamTrack->GetMomentum().Pz();
  if (flag==0) {
    FillHisto("Pinunu_kaon_p",fPBeamTrack);
    FillHisto("Pinunu_kaon_thetay_vs_thetax",kaon_thetax, kaon_thetay);
  }

  Double_t rth = sqrt((kaon_thetax-fThetaXCenter)*(kaon_thetax-fThetaXCenter)+(kaon_thetay-fThetaYCenter)*(kaon_thetay-fThetaYCenter));

  if (fYear==2016) {
    if (rth>0.00035) return 0;
  }
  if (flag==0) {
    FillHisto("Pinunu_kaon_thetax",kaon_thetax);
    FillHisto("Pinunu_kaon_thetay",kaon_thetay);
  }
  if (fPBeamTrack<72.7 || fPBeamTrack>77.2) return 0;
  return 1;
}

Bool_t BeamTrackSelection::EarlyDecay(Int_t flag) {

  // Project the pion back to the trim5 and compute the momentum there taking into account the blue field
  TRecoSpectrometerCandidate *spectrack = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fTrack->GetTrackID());
  Double_t partrack[4] = {fPTrack*1000.,spectrack->GetSlopeXBeforeMagnet(),spectrack->GetSlopeYBeforeMagnet(),0.13957018};
  TLorentzVector ptrack = fTool->Get4Momentum(partrack);
  TVector3 pmom = ptrack.Vect();
  TVector3 pini = spectrack->GetPositionBeforeMagnet();
  TVector3 pend = fTool->BlueFieldCorrection(&pmom,pini,spectrack->GetCharge(),101800.); // position of the pion at trim 5
  ptrack.SetVectM(pmom,0.13957018); // 4-momentum of the pion at trim 5

  // Pion momentum upstream to trim5 and position at the end of the last dipole
  TVector3 dprime_dw(ptrack.Px()/ptrack.Pz()-fTrimKick/ptrack.P(),ptrack.Py()/ptrack.Pz(),1.);
  Double_t partrack_dw[4] = {ptrack.P()*1000.,dprime_dw.X(),dprime_dw.Y(),0.13957018};
  TVector3 pdip_dw = pend+dprime_dw*(99460.-101800.); // position of the pion at the end of the last dipole
  TLorentzVector ptrack_dw = fTool->Get4Momentum(partrack_dw); // 4-momentum of the pion at the end of the last dipole

  // Kaon momentum upstream to trim5 and position at the end of the last dipole
  TVector3 dprime_up(fBeamTrack->GetMomentum().Px()/fBeamTrack->GetMomentum().Pz()-fTrimKick/fPBeamTrack,fBeamTrack->GetMomentum().Py()/fBeamTrack->GetMomentum().Pz(),1.);
  Double_t partrack_up[4] = {fPBeamTrack*1000.,dprime_up.X(),dprime_up.Y(),0.493677};
  TVector3 pdip_up = fBeamTrack->GetPosition()+dprime_up*(99460.-101800.); // position of the kaon ath the end of the last dipole
  TLorentzVector ptrack_up = fTool->Get4Momentum(partrack_up); // 4-momentum of the kaon at the end of the last dipole

  // missing mass assuming the decay between the end of the last dipole and the trim5
  Double_t m2misssingle = (ptrack_up-ptrack_dw).Mag2();
  if (!flag) {
    FillHisto("Pinunu_earlydecay_mmiss_vs_p_single",fPTrack,m2misssingle);
    FillHisto("Pinunu_earlydecay_mmiss_vs_mmiss_single",(fBeamTrack->GetMomentum()-fTrack->GetMomentum()).Mag2(),m2misssingle);
  }

  // Look for a decay vertex upstream to the end of the last dipole
  fxPaipDAcc = pdip_dw.X();
  fyPaipDAcc = pdip_dw.Y();
  fxKaonDAcc = pdip_up.X();
  fyKaonDAcc = pdip_up.Y();
  fxprimePaipAcc = dprime_dw.X();
  fyprimePaipAcc = dprime_dw.Y();
  fxprimeKaonAcc = dprime_up.X();
  fyprimeKaonAcc = dprime_up.Y();
  fpPaipAcc = ptrack.P();
  fpKaonAcc = fPBeamTrack;
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat,ierflag;
  Double_t pars[10],epars[10];
  Double_t arglist[1];
  arglist[0] = -1;
  fFitter->mnexcm("SET PRI", arglist, 1, ierflag); // Set MINUIT print level
  fFitter->mnexcm("SET NOW", arglist, 0, ierflag); // Set MINUIT warnings
  fFitter->mnparm(0, "zorig", 95860., 20., 93360., 99460., ierflag);
  arglist[0] = 0;
  fFitter->mnexcm("MIGRAD", arglist, 1, ierflag); // Calls the minimization
  fFitter->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  for(Int_t iPar = 0; iPar < fNPars; iPar++) fFitter->GetParameter(iPar,pars[iPar],epars[iPar]);
  Double_t zorig = pars[0];
  Double_t newcda = BeamCDA(zorig);

  // missing mass at origin
  Double_t bfield = 1.6678; // T
  Double_t thetaX_dw = dprime_dw.X();
  Double_t dypion = -(zorig-99460.)*(0.3*bfield)*(1./fpPaipAcc)*0.001;
  if (zorig<96960.) {
    dypion = -(96960.-99460.)*(0.3*bfield)*(1./fpPaipAcc)*0.001;
    if (zorig<95860.) {
      dypion = -(96960.-99460.)*(0.3*bfield)*(1./fpPaipAcc)*0.001+(zorig-95860.)*(0.3*bfield)*(1./fpPaipAcc)*0.001;
    }
  }
  Double_t thetaY_dw = dprime_dw.Y()+dypion;
  Double_t thetaX_up = dprime_up.X();
  Double_t dykaon = -(zorig-99460.)*(0.3*bfield)*(1./fpKaonAcc)*0.001;
  if (zorig<96960.) {
    dykaon = -(96960.-99460.)*(0.3*bfield)*(1./fpKaonAcc)*0.001;
    if (zorig<95860.) {
      dykaon = -(96960.-99460.)*(0.3*bfield)*(1./fpKaonAcc)*0.001+(zorig-95860.)*(0.3*bfield)*(1./fpKaonAcc)*0.001;
    }
  }
  Double_t thetaY_up = dprime_up.Y()+dykaon;
  Double_t paramt_dw[4] = {ptrack.P()*1000.,thetaX_dw,thetaY_dw,0.13957018};
  Double_t paramt_up[4] = {fPBeamTrack*1000.,thetaX_up,thetaY_up,0.493677};
  TLorentzVector ppion_orig = fTool->Get4Momentum(paramt_dw);
  TLorentzVector pkaon_orig = fTool->Get4Momentum(paramt_up);
  Double_t m2miss_orig = (pkaon_orig-ppion_orig).Mag2();

  // Categories
  Bool_t isEarlyDecay0 = (fPTrack<60 && fPTrack>35 && fMMiss<0.072 && fMMiss>0.015) ? 1 : 0;
  Bool_t isEarlyDecay1 = (fPTrack<60 && fPTrack>35 && fMMiss<0.014 && fMMiss>0.) ? 1 : 0;
  Bool_t isKmu2 = (fPTrack<60 && fMMiss<0) ? 1 : 0;
  Bool_t is3Pion = (fPTrack<50 && fPTrack>10 && fMMiss>0.077) ? 1 : 0;

  // All residual events
  if (!flag) {
    FillHisto("Pinunu_earlydecay_xych1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_earlydecay_xylkr",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_earlydecay_zp",zorig,fPTrack);
    FillHisto("Pinunu_earlydecay_zm",zorig,m2miss_orig);
    FillHisto("Pinunu_earlydecay_dcda",newcda,fBeamTrack->GetCDA());
    FillHisto("Pinunu_earlydecay_diffcda",fPTrack,newcda-fBeamTrack->GetCDA());
  }
  if (flag==1) {
    FillHisto("Kmu2_diffcda",fPTrack,newcda-fBeamTrack->GetCDA());
    if (fPTrack<35 && fPTrack>15) FillHisto("Kmu2_dcda",newcda,fBeamTrack->GetCDA());
  }
  if (flag==2) {
    FillHisto("K2pi_diffcda",fPTrack,newcda-fBeamTrack->GetCDA());
    if (fPTrack<35 && fPTrack>15) FillHisto("K2pi_dcda",newcda,fBeamTrack->GetCDA());
  }

  if (!flag && isEarlyDecay0) {
    FillHisto("Pinunu_earlydecay_xych1_earlydec0",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_earlydecay_xylkr_earlydec0",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_earlydecay_zp_earlydec0",zorig,fPTrack);
    FillHisto("Pinunu_earlydecay_zm_earlydec0",zorig,m2miss_orig);
    FillHisto("Pinunu_earlydecay_dcda_earlydec0",newcda,fBeamTrack->GetCDA());
    FillHisto("Pinunu_earlydecay_diffcda_earlydec0",fPTrack,newcda-fBeamTrack->GetCDA());
  }
  if (!flag && isEarlyDecay1) {
    FillHisto("Pinunu_earlydecay_xych1_earlydec1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_earlydecay_xylkr_earlydec1",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_earlydecay_dcda_earlydec1",newcda,fBeamTrack->GetCDA());
  }
  if (!flag && isKmu2) {
    FillHisto("Pinunu_earlydecay_xych1_kmu2",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_earlydecay_xylkr_kmu2",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_earlydecay_dcda_kmu2",newcda,fBeamTrack->GetCDA());
  }
  if (!flag && is3Pion) {
    FillHisto("Pinunu_earlydecay_xych1_3pion",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_earlydecay_xylkr_3pion",fXAtLKr,fYAtLKr);
    FillHisto("Pinunu_earlydecay_dcda_3pion",newcda,fBeamTrack->GetCDA());
  }

  // Cut against early decays
  if (newcda-fBeamTrack->GetCDA()<35.) return true;

  return false;
}

Bool_t BeamTrackSelection::KinematicTails(Int_t flag) {

  // Internal track consistency
  TRecoSpectrometerCandidate *spectrack = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(fTrack->GetTrackID());
  Double_t dthetax = spectrack->GetSlopeXBeforeFit()-spectrack->GetSlopeXBeforeMagnet();
  Double_t dthetay = spectrack->GetSlopeYBeforeFit()-spectrack->GetSlopeYBeforeMagnet();
  Double_t quality1 = spectrack->GetCombinationTotalQuality();
  if (!flag) {
    FillHisto("Pinunu_tails_spec_dthetax",dthetax,fMMiss);
    FillHisto("Pinunu_tails_spec_dthetay",dthetay,fMMiss);
    FillHisto("Pinunu_tails_spec_qual1",quality1,fMMiss);
    FillHisto("Pinunu_tails_spec_nhits",spectrack->GetMomentum()*0.001,spectrack->GetNHits());
    FillHisto("Pinunu_tails_vertex_xz",fVertex.Z(),fBeamTrack->GetVertex().X());
    FillHisto("Pinunu_tails_vertex_yz",fVertex.Z(),fBeamTrack->GetVertex().Y());
    FillHisto("Pinunu_tails_kaon_xy",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
    Double_t thetapk = fTrack->GetMomentum().Angle(fBeamTrack->GetMomentum().Vect());
    FillHisto("Pinunu_tails_thetavsp",fPTrack,thetapk);
  }

  // Timing
  Double_t tgtk = fBeamTrack->GetTime();
  Double_t dt_gtkktag = tgtk-fBeamTrack->GetKTAGTime();
  Double_t dt_gtkchod = tgtk-fTrack->GetCHODTime();
  Double_t dt_gtklkr  = tgtk-fTrack->GetLKrTime();
  Double_t dt_gtkspec = tgtk-fTrack->GetTime();
  if (!flag) {
    FillHisto("Pinunu_tails_dktagVSdgtk_chod",tgtk-fTrack->GetCHODTime(),fBeamTrack->GetKTAGTime()-fTrack->GetCHODTime());
    FillHisto("Pinunu_tails_dchodVSdktag_gtk",dt_gtkktag,dt_gtkchod);
    FillHisto("Pinunu_tails_dtktag",dt_gtkktag,fMMiss);
    FillHisto("Pinunu_tails_dtchod",dt_gtkchod,fMMiss);
    FillHisto("Pinunu_tails_dtlkr",dt_gtklkr,fMMiss);
    FillHisto("Pinunu_tails_dtspec",dt_gtkspec,fMMiss);
  }

  // Apply cuts
  if (fGTKFlag) return 0; // Do not apply further cuts if the GTK is not in the analysis
  if (dthetax<-0.0003) return 1;
  if (dthetax>0.0003) return 1;
  if (fabs(dthetay)>0.001) return 1;
  if (quality1>4) return 1;
  if (fYear==2015) {
    if (fVertex.X()>-85.25+0.0011167*fVertex.Z()) return 1;
    if (fVertex.X()<-139.34+0.001208*fVertex.Z()) return 1;
    if (fabs(dt_gtkktag)>1.4) return 1;
  }
  if (fYear==2016) {
    if (spectrack->GetNHits()>42) return 1;
    if (spectrack->GetNHits()<15) return 1;
    if (fVertex.X()>-93.9+0.0012*fVertex.Z()) return 1;
    if (fVertex.X()<-148.53+0.00122*fVertex.Z()) return 1;
    if (fVertex.Y()>3.6+0.000112*fVertex.Z()) return 1;
    if (fVertex.Y()<-5.4-0.00009464*fVertex.Z()) return 1;
    if(fabs(dt_gtkchod)>1.1) return 1; // Strong cut
    //if(fabs(dt_gtkktag)>1.5) return 1; // Strong cut
  }

  // Final check
  if (!flag) {
    Double_t kaon_thetax = fBeamTrack->GetMomentum().Px()/fBeamTrack->GetMomentum().Pz()-fTrimTheta;
    Double_t kaon_thetay = fBeamTrack->GetMomentum().Py()/fBeamTrack->GetMomentum().Pz();
    FillHisto("Pinunu_tails_dktagVSdgtk_chod_check",tgtk-fTrack->GetCHODTime(),fBeamTrack->GetKTAGTime()-fTrack->GetCHODTime());
    FillHisto("Pinunu_tails_kaon_xy_check",fBeamTrack->GetPosition().X(),fBeamTrack->GetPosition().Y());
    FillHisto("Pinunu_tails_kaon_p_check",fPBeamTrack);
    FillHisto("Pinunu_tails_kaon_thetaxy_check",kaon_thetax,kaon_thetay);
  }

  return 0;
}

Int_t BeamTrackSelection::PionPID(Bool_t flag) {

  // Positron cut
  if (fTrack->GetLKrEovP()>=0.8) {
    if(flag){

      FillHisto("Pinunu_pid_positron_p_vs_zvtx",fVertex.Z(),fPTrack);
      FillHisto("Pinunu_pid_positron_mmiss_vs_p",fPTrack,fMMiss);
      FillHisto("Pinunu_pid_positron_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
      FillHisto("Pinunu_pid_positron_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }
    return 0;
  }

  if(flag){

    FillHisto("Pinunu_pid_nopositron_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_pid_nopositron_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_pid_nopositron_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_pid_nopositron_x_vs_y_lkr",fXAtLKr,fYAtLKr);
  }

  // Muon cut
  //if (MuonDiscriminant(1)) {
  if (MuonDiscriminantRicc(flag)) {

    if(flag){
      FillHisto("Pinunu_pid_muon_p_vs_zvtx",fVertex.Z(),fPTrack);
      FillHisto("Pinunu_pid_muon_mmiss_vs_p",fPTrack,fMMiss);
      FillHisto("Pinunu_pid_muon_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
      FillHisto("Pinunu_pid_muon_x_vs_y_lkr",fXAtLKr,fYAtLKr);
    }

    return 0;
  }

  if(flag){
    FillHisto("Pinunu_pid_nomuon_p_vs_zvtx",fVertex.Z(),fPTrack);
    FillHisto("Pinunu_pid_nomuon_mmiss_vs_p",fPTrack,fMMiss);
    FillHisto("Pinunu_pid_nomuon_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("Pinunu_pid_nomuon_x_vs_y_lkr",fXAtLKr,fYAtLKr);
  }

  // Pion cut


  return 1;

}

Int_t BeamTrackSelection::MuonDiscriminantRicc(Int_t flag) {

  Int_t discriminant = 0;
  if (fTrack->GetMUV3ID()>-1) {discriminant = 1;}

  Double_t caloEnergy = fTrack->GetCalorimetricEnergy();
  Double_t eovp = caloEnergy/fPTrack;
  Double_t e1 = fTrack->GetMUV1Energy();
  Double_t e2 = fTrack->GetMUV2Energy();
  Double_t r1 = e1/caloEnergy;
  Double_t r2 = e2/caloEnergy;
  Double_t sw1 = fTrack->GetMUV1ShowerWidth();
  Double_t sw2 = fTrack->GetMUV2ShowerWidth();
  Double_t elkr = fTrack->GetLKrEovP()*fPTrack;
  Double_t seedratio = fTrack->GetLKrSeedEnergy()/elkr;
  Double_t cellratio = fTrack->GetLKrNCells()/elkr;

  if (r1<0.01&&r2<0.01) { // against muon bremm or muon decay before lkr
    if (seedratio>0.2&&cellratio<=3) {discriminant = 1; }
    if (seedratio<=0.2&&cellratio<1.8) {discriminant = 1;}
    if (seedratio>0.35){discriminant = 1;}
    if (seedratio<0.05){discriminant = 1; }
  }

  if (seedratio>0. && seedratio<0.8 && cellratio<1.4) discriminant=1;
  if(fTrack->GetCalorimetricEnergy() < 2){discriminant = 1;}
  if(fTrack->GetIsMIP()){discriminant = 1; }
  if(fTrack->GetIsMulti()){discriminant = 1;}
  if(fTrack->GetMuonProb() >= 0.01){discriminant = 1;}

  return discriminant;
}


//////////////////////////////////
// Minuit minimization function //
//////////////////////////////////
void BeamTrackSelection::FuncCDA(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t iflag) {
  Double_t deyd = fyPaipDAcc-fyKaonDAcc;
  Double_t dexd = fxPaipDAcc-fxKaonDAcc;
  Double_t deyp = fyprimePaipAcc-fyprimeKaonAcc;
  Double_t dexp = fxprimePaipAcc-fxprimeKaonAcc;
  Double_t bfield = 1.6678; // T
  Double_t dey = deyd+deyp*(par[0]-99460.)-(0.3*bfield*(par[0]-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(par[0]-99460.)/2.;
  if (par[0]<96960.) {
    Double_t deyend = deyd+deyp*(96960.-99460.)-(0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(96960.-99460.)/2.;
    dey = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(par[0]-96960.);
    if (par[0]<95860.) {
      Double_t deyend2 = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(95860.-96960.);
      dey = deyend2+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(par[0]-95860.)+(0.3*bfield*(par[0]-95860.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(par[0]-95860.)/2.;
    }
  }
  Double_t dex = dexd+dexp*(par[0]-99460.);
  f = dey*dey+dex*dex;
}


Double_t BeamTrackSelection::BeamCDA(Double_t zvtx) {
  Double_t zvertex = zvtx;
  Double_t deyd = fyPaipDAcc-fyKaonDAcc;
  Double_t dexd = fxPaipDAcc-fxKaonDAcc;
  Double_t deyp = fyprimePaipAcc-fyprimeKaonAcc;
  Double_t dexp = fxprimePaipAcc-fxprimeKaonAcc;
  Double_t bfield = 1.6678; // T
  Double_t dey = deyd+deyp*(zvertex-99460.)-(0.3*bfield*(zvertex-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(zvertex-99460.)/2.;
  if (zvertex<96960.) {
    Double_t deyend = deyd+deyp*(96960.-99460.)-(0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(96960.-99460.)/2.;
    dey = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(zvertex-96960.);
    if (zvertex<95860.) {
      Double_t deyend2 = deyend+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(95860.-96960.);
      dey = deyend2+(deyp-0.3*bfield*(96960.-99460.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(zvertex-95860.)+(0.3*bfield*(zvertex-95860.)*(1./fpPaipAcc-1./fpKaonAcc)*0.001)*(zvertex-95860.)/2.;
    }
  }
  Double_t dex = dexd+dexp*(zvertex-99460.);
  return sqrt(dey*dey+dex*dex);
}
