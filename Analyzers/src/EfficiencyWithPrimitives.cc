#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "EfficiencyWithPrimitives.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "BeamParameters.hh"
#include "DownstreamTrack.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


EfficiencyWithPrimitives::EfficiencyWithPrimitives(Core::BaseAnalysis *ba) : Analyzer(ba, "EfficiencyWithPrimitives")
{

  //--Detector name array
  fTrigDetName[0] = "CHOD";
  fTrigDetName[1] = "RICH";
  fTrigDetName[2] = "LAV";
  fTrigDetName[3] = "MUV3";
  fTrigDetName[4] = "NewCHOD";
  fTrigDetName[5] = "TALK";
  fTrigDetName[6] = "LKr";
}

EfficiencyWithPrimitives::~EfficiencyWithPrimitives(){

}

void EfficiencyWithPrimitives::InitOutput(){

  RegisterOutput("PinnTrigger",&fPinunuTrigger);
  RegisterOutput("PinnTrigger20ns",&fPinunuTrigger20ns);
  RegisterOutput("NewCHODL0",&fNewCHOD);
  RegisterOutput("RICHL0",&fRICH);
  RegisterOutput("LAVL0",&fLAV);
  RegisterOutput("MUV3L0",&fMUV3);
  RegisterOutput("Qx",&fQx);
  RegisterOutput("UMTC",&fUMTC);
  RegisterOutput("Calo",&fCalo);

}



void EfficiencyWithPrimitives::InitHist(){
  BookHisto(new TH2F("CHODPrimID_vs_DataType","",36000,0,36000,20,0,20));
  BookHisto(new TH2F("CHODPrimIDSlot0_vs_DataType","",36000,0,36000,20,0,20));
  BookHisto(new TH2F("CHODPrimIDSlot1_vs_DataType","",36000,0,36000,20,0,20));
  BookHisto(new TH2F("CHODPrimIDSlot2_vs_DataType","",36000,0,36000,20,0,20));

  BookHisto(new TH2F("RICHPrimID_vs_DataType","",36000,0,36000,20,0,20));
  BookHisto(new TH2F("RICHPrimIDSlot0_vs_DataType","",36000,0,36000,20,0,20));
  BookHisto(new TH2F("RICHPrimIDSlot1_vs_DataType","",36000,0,36000,20,0,20));
  BookHisto(new TH2F("RICHPrimIDSlot2_vs_DataType","",36000,0,36000,20,0,20));

}
void EfficiencyWithPrimitives::DefineMCSimple(){
}

void EfficiencyWithPrimitives::StartOfRunUser(){
}

void EfficiencyWithPrimitives::StartOfBurstUser(){

  SkipEvent=0;
  BitFineTime=1;

}

void EfficiencyWithPrimitives::Process(int iEvent){
  if( SkipEvent==1 && GetWithMC()!=1 ) return;
  if( GetEventHeader()->GetEventQualityMask()!=0 ) return;

  L0TPData *L0Data = GetL0Data();

  Bool_t Control=0;
  UInt_t TimeStamp=L0Data->GetTimeStamp();
  UInt_t DataType =GetWithMC() ? 1 : L0Data->GetDataType();

  //Datatype should have bit 5 (number 16) set
  if( DataType & 0x10  ) Control=1;

  if( Control==0 ) return;

  //initialize primitive time to -9999999:


  long long time[7][3]; //Timestamp + finetime [Detector][L0TP slot]

  for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
    for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){
      time[iTrigDet][iTrigSlot]=-99999;
    }
  }

  int nprim=0;
  Double_t primarray[3]={0.};
  for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

      if(L0Data->GetPrimitive(iTrigSlot,1).GetPrimitiveID() & 0x1<<14){
          nprim++;
          primarray[iTrigSlot]+=1;

      }

  }

  if(nprim!=1) return;

      //Set Primitive time
      for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
          for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){

              /***********************
       Each positive primitive
       has bit 14 set at 1
              *************************/

              if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14){

                  /*
                    Having only one timestamp, depending on the slot and on the finetime
                    we have to correct for the rollover. This is done in GetSlotTime.
                    Time is with a precision of 100 ps.
                  */

                  time[iTrigDet][iTrigSlot] = GetSlotTime((long long)TimeStamp,(Int_t)L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetFineTime(),(Int_t)GetEventHeader()->GetFineTime(),iTrigSlot,BitFineTime);
                  //std::cout << "Det = " << iTrigDet << "Slot = " << iTrigSlot<< std::endl;
                  //cout << "orginal ts = " << L0Data->GetTimeStamp() << "Timestamp = " << TimeStamp << "long tstamp = " << (long long)TimeStamp << endl;
                  //cout << GetSlotTime((long long)TimeStamp,(Int_t)L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetFineTime(),reffinetime,iTrigSlot,BitFineTime) << "casted ===" << time[iTrigDet][iTrigSlot] << endl;
              }
          }
      }


      //Set Primitive Reference time: CHOD or RICH.
      long long reftime=-999999;

      // reftime=(long long)TimeStamp*0x100+(Int_t)GetEventHeader()->GetFineTime(); //Central time of the trigger window (RICH if it is present, if not, it takes CHOD)

      if(primarray[0]!=0) reftime=time[1][0];


      /*Now I check which detector is in time with the reference, and I calculate the primitive ID*/

      fLKrPrimE=0;
      fLKrPrimE20ns=0;

      //Set Global Primitive ID

      Int_t GlobalPrimitiveID[7]={0,0,0,0,0,0,0};
      FillHisto("CHODPrimID_vs_DataType",L0Data->GetPrimitive(0,0).GetPrimitiveID(),DataType);
      FillHisto("RICHPrimID_vs_DataType",L0Data->GetPrimitive(0,1).GetPrimitiveID(),DataType);
      if(L0Data->GetPrimitive(0,0).GetPrimitiveID()==0){

          FillHisto("RICHPrimIDSlot0_vs_DataType",L0Data->GetPrimitive(0,1).GetPrimitiveID(),DataType);
          FillHisto("RICHPrimIDSlot1_vs_DataType",L0Data->GetPrimitive(1,1).GetPrimitiveID(),DataType);
          FillHisto("RICHPrimIDSlot2_vs_DataType",L0Data->GetPrimitive(2,1).GetPrimitiveID(),DataType);
      }
      if(L0Data->GetPrimitive(0,1).GetPrimitiveID()==0){

          FillHisto("CHODPrimIDSlot0_vs_DataType",L0Data->GetPrimitive(0,0).GetPrimitiveID(),DataType);
          FillHisto("CHODPrimIDSlot1_vs_DataType",L0Data->GetPrimitive(1,0).GetPrimitiveID(),DataType);
          FillHisto("CHODPrimIDSlot2_vs_DataType",L0Data->GetPrimitive(2,0).GetPrimitiveID(),DataType);
      }

      /*Loop for all L0TP detector and for each slot of the ram around the time of the reference*/

      for(UInt_t iTrigDet = 0; iTrigDet < 7; iTrigDet++){
          for(UInt_t iTrigSlot = 0; iTrigSlot < 3; iTrigSlot++){


              if(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID() & 0x1<<14)   {

                  if(iTrigDet==6){//LKr
                      if(TMath::Abs(time[iTrigDet][iTrigSlot]-reftime)<100.)fLKrPrimE+=56*(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID()&0x1fff); //Energy of the primitive closer than 10 ns
                      if(TMath::Abs(time[iTrigDet][iTrigSlot]-reftime)<200)fLKrPrimE20ns+=56*(L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID()&0x1fff); //Energy of the primitive closer than 20 ns
                  }

                  //Each detector was required in time closer than 10 ns with respect to the reference time
                  if(TMath::Abs(time[iTrigDet][iTrigSlot]-reftime)<100) {//In 10 ns wrt trigger
                      GlobalPrimitiveID[iTrigDet] |= L0Data->GetPrimitive(iTrigSlot,iTrigDet).GetPrimitiveID(); // The primitive ID is the OR of the primitve ID in time



                  }
              }
          }
      }


      //Trigger event set
      fCHOD   =0;
      fNewCHOD=0;
      fQx     =0;
      fUMTC   =0;
      fRICH   =0;
      fMUV3   =0;
      fLAV    =0;
      fCalo   =0;


      if(GlobalPrimitiveID[0]!=0)        fCHOD=1;
      if(GlobalPrimitiveID[1]!=0)        fRICH=1;
      if(GlobalPrimitiveID[2]!=0)        fLAV=1;
      if(GlobalPrimitiveID[3]!=0)        fMUV3=1;
      if(GlobalPrimitiveID[4] & 0x1<<11) fQx=1;
      if(GlobalPrimitiveID[4]!=0)        fNewCHOD=1;
      if(GlobalPrimitiveID[4] & 0x1<<12) fUMTC=1;
      if(GlobalPrimitiveID[6]!=0)        fCalo=1;

      fPinunuTrigger    = fRICH && !fLAV && !fQx && fNewCHOD && !fMUV3 && fUMTC && fLKrPrimE < 20000.;
      fPinunuTrigger20ns= fRICH && !fLAV && !fQx && fNewCHOD && !fMUV3 && fUMTC && fLKrPrimE20ns < 20000.;


      SetOutputState("PinnTrigger",kOValid);
      SetOutputState("PinnTrigger20ns",kOValid);
      SetOutputState("RICHL0",kOValid);
      SetOutputState("NewCHODL0",kOValid);
      SetOutputState("LAVL0" ,kOValid);
      SetOutputState("MUV3L0",kOValid);
      SetOutputState("Qx"    ,kOValid);
      SetOutputState("UMTC"  ,kOValid);
      SetOutputState("Calo"  ,kOValid);



}


void EfficiencyWithPrimitives::PostProcess(){

}

void EfficiencyWithPrimitives::EndOfBurstUser(){


}

void EfficiencyWithPrimitives::EndOfRunUser(){


}
void EfficiencyWithPrimitives::EndOfJobUser(){

  SaveAllPlots();

}

void EfficiencyWithPrimitives::DrawPlot(){
}



//********************
int EfficiencyWithPrimitives::FromCellToSuperCell(int CellX, int CellY){

  return supercells[std::make_pair(CellX,CellY)];
}


long long EfficiencyWithPrimitives::GetSlotTime(long long TimeStamp, Int_t FineTime, Int_t ReferenceFineTime, Int_t iTrigSlot, Int_t BitFineTime)
{
  long long time=0;
  if(iTrigSlot==0){
    time = (long long)(TimeStamp)   * 0x100  + (Int_t)FineTime;

    return time;
  }


  if(BitFineTime==0){
    if(iTrigSlot==1){
      time = (long long)(TimeStamp-1) * 0x100  + (Int_t)FineTime;
      return time;
    }
    if(iTrigSlot==2){
      time = (long long)(TimeStamp+1) * 0x100  + (Int_t)FineTime;
      return time;
    }
  }

  Int_t compare = 0;
  for (Int_t i = 0; i < BitFineTime; i++) {
    compare += (128 >> i);
  }
  if(ReferenceFineTime >= compare) {
    time= (long long)(TimeStamp + iTrigSlot - 1) * 0x100  + (Int_t)FineTime;
    return time;
  } else{
    time= (long long)(TimeStamp + iTrigSlot - 2) * 0x100  + (Int_t)FineTime;
    return time;
  }
  return -1;

}
