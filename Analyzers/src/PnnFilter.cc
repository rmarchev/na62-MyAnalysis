#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "PnnFilter.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "TMath.h"
#include "TVector2.h"
#include "NA62Exceptions.hh"
#include "TSystem.h"

#include "GeometricAcceptance.hh"
#include "SpectrometerMUV3AssociationOutput.hh"
#include "LAVMatching.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

/// \class PnnFilter
/// \Brief
/// Filter events for pinunu or control trigger
/// \EndBrief
/// \Detailed
/// Filtering condition controlled by the parameter FilterControl
/// 0: no trigger condition applied for filtering
/// 1: control trigger events only
/// 2: pnn mask trigger only
/// 3: control trigger and pnn mask
///
/// The filtering uses GigaTrackerEvtReco as preanalyzer, storing the
/// fully corrected hit times and well reconstructed GTK candidates
/// in the GigaTracker tree.
///
/// \author Giuseppe Ruggiero (giuseppe.ruggiero@cern.ch)
/// \EndDetailed

PnnFilter::PnnFilter(Core::BaseAnalysis *ba) : Analyzer(ba, "PnnFilter")
{
  RequestAllRecoTrees();
  RequestL0Data();

  // Filtering Parameters
  AddParam("FilterControl",&fFilterControl,0); // 0 pnn filter without trigger selection, 1 filter control, 2 filter pnn, 3 filter control and pnn
  AddParam("DWControl0TRK",&fCDW,10); // Downscaling for control trigger events without tracks reconstructed

  // Geometrical instance
  fGeo = GeometricAcceptance::GetInstance();

  // Pointers to utility classes
  fLAVMatching = new LAVMatching();
}

void PnnFilter::InitOutput(){
}

void PnnFilter::InitHist(){

  // Histograms
  BookHisto(new TH2F("StrawAnalysis_CDAZVertex","",250,50000,300000,200,0,100));
  BookHisto(new TH1F("Track_chodminchi2","",200,0,50));
  BookHisto(new TH2F("Track_chodtrackmatch","",200,-100,100,400,0,2000));
  BookHisto(new TH2F("CHODAnalysis_dtime","",2048,0,2048,200,-10,10));
  BookHisto(new TH1F("KTAGAnalysis_alltime","",1000,-50,50));
  BookHisto(new TH1F("KTAGAnalysis_time","",1000,-50,50));
  BookHisto(new TH2F("MUV3Analysis_timedist","",400,-20,20,150,0,600));
  BookHisto(new TH2F("LKrAnalysis_photon","",200,0,2000,1200,-150,150));
  BookHisto(new TH1F("LAVAnalysis_time_broad","",300,-15,15));
  BookHisto(new TH1F("LAVAnalysis_time_tight","",300,-15,15));
  BookHisto(new TH1F("Track_eovp","",220,0,1.1));

  // Counters
  BookCounter("NTotal");
  BookCounter("NPnn");
  BookCounter("NControl_Filter");
  BookCounter("NControl_0trk");
  BookCounter("NControl_NoSel");
  NewEventFraction("PnnSelected");
  AddCounterToEventFraction("PnnSelected","NTotal");
  AddCounterToEventFraction("PnnSelected","NPnn");
  AddCounterToEventFraction("PnnSelected","NControl_Filter");
  AddCounterToEventFraction("PnnSelected","NControl_0trk");
  AddCounterToEventFraction("PnnSelected","NControl_NoSel");
  DefineSampleSizeCounter("PnnSelected", "NTotal");
}

void PnnFilter::DefineMCSimple(){
}

void PnnFilter::StartOfRunUser(){
}

void PnnFilter::StartOfBurstUser(){

  // Read CHOD parameters
  int runID = GetEventHeader()->GetRunID();
  TString CHODConfigFile = Form("%s/config/CHOD.2016.conf",getenv("NA62RECOSOURCE"));
//  TString CHODT0File = Form("%s/config/CHOD-T0.dat",getenv("NA62RECOSOURCE"));
  TString CHODT0File = Form("/afs/cern.ch/user/n/na62prod/daemonspace/NA62FWr1421-Run%d/NA62Reconstruction/config/CHOD-T0.dat",runID);
  TString CHODSlewingFile = Form("%s/config/CHOD-SlewCorr.run1520_0076-run1520_2083.dat",getenv("NA62RECOSOURCE"));

  // Read Geometry
  ifstream CHODConfig(CHODConfigFile.Data());
  cout << " --->  Reading " << CHODConfigFile.Data() << endl;
  TString line;
  while (line.ReadLine(CHODConfig)) {
    if (line.BeginsWith("#")) continue;
    TObjArray * l = line.Tokenize(" ");
    if (line.BeginsWith("SC_PositionV")) {
      double pos[16];
      for (int jl(0); jl<16; jl++) pos[jl] = ((TObjString*)(l->At(jl+1)))->GetString().Atof();
      for (int j=0; j<16; j++)  fCHODPosV[j] = -pos[j];
      for (int j=16; j<32; j++) fCHODPosV[j] = -pos[31-j];
      for (int j=32; j<48; j++) fCHODPosV[j] = pos[j-32];
      for (int j=48; j<64; j++) fCHODPosV[j] = pos[63-j];
      for (int j=0; j<16; j++)  fCHODPosH[j] = pos[15-j];
      for (int j=16; j<32; j++) fCHODPosH[j] = -pos[j-16];
      for (int j=32; j<48; j++) fCHODPosH[j] = -pos[47-j];
      for (int j=48; j<64; j++) fCHODPosH[j] = pos[j-48];
    }
  }
  cout << "---> File " << CHODConfigFile.Data() << " read " << endl;
  CHODConfig.close();

  // Standard T0
  int nRL = 0;
  ifstream CHODT0(CHODT0File.Data());
  cout << " --->  Reading " << CHODT0File.Data() << endl;
  while (line.ReadLine(CHODT0)) {
    if (line.BeginsWith("#")) continue;
    TObjArray * l = line.Tokenize(" ");
    double t0 = ((TObjString*)(l->At(2)))->GetString().Atof();
    fCHODAllT0[(int)(nRL/16)][(int)(nRL%16)] = fabs(t0)>=99 ? 0.0 : t0;
    l->Delete();
    nRL++;
  }
  cout << "---> File " << CHODT0File.Data() << " read " << nRL << " lines" << endl;
  CHODT0.close();

  // Slewings
  nRL = 0;
  ifstream CHODSlewing(CHODSlewingFile.Data());
  cout << " --->  Reading " << CHODSlewingFile.Data() << endl;
  while (line.ReadLine(CHODSlewing)) {
    TObjArray * l = line.Tokenize(" ");
    fCHODAllSlewSlope[(int)(nRL/16)][(int)(nRL%16)] = ((TObjString*)(l->At(0)))->GetString().Atof();
    fCHODAllSlewConst[(int)(nRL/16)][(int)(nRL%16)] = ((TObjString*)(l->At(1)))->GetString().Atof();
    l->Delete();
    nRL++;
  }
  cout << "---> File " << CHODSlewingFile.Data() << " read " << nRL << " lines" << endl;
  CHODSlewing.close();

  // Fine T0
////  ifstream CHODFineT0("/afs/cern.ch/work/r/ruggierg/public/na62git/database/run6670/CHODfinet0.dat");
////  nRL = 0;
  for (int jP(0); jP<2048; jP++) {
////  while (line.ReadLine(CHODFineT0)) {
////    TObjArray * l = line.Tokenize(" ");
////    fCHODAllFineT0[(int)(nRL/16)][(int)(nRL%16)] = ((TObjString*)(l->At(1)))->GetString().Atof();
////    l->Delete();
////    nRL++;
    fCHODAllFineT0[(int)(nRL/16)][(int)(nRL%16)] = 0.;
  }
////  CHODFineT0.close();

  // LAV Noisy channels
  vector<int> vNoisyCh;
  if (runID==6291)  vNoisyCh = make_vector<int>() << 63081 << 90083 << 103040 << 113002 << 123083 << 123121;
  if (runID==6342)  vNoisyCh = make_vector<int>() << 63081 << 90083 << 103040 << 113002 << 123083;
  if (runID==6420)  vNoisyCh = make_vector<int>() << 63081 << 90083 << 103040 << 112042 << 113002 << 123083;
  if (runID==6483)  vNoisyCh = make_vector<int>() << 63081 << 90083 << 103040 << 113002 << 123083 << 1123083;
  if (runID==6610)  vNoisyCh = make_vector<int>() << 44013 << 63081 << 90083 << 103040 << 113002 << 123083;
  if (runID==6614)  vNoisyCh = make_vector<int>() << 63081 << 90083 << 103040 << 113002 << 123083;
  if (runID==6625)  vNoisyCh = make_vector<int>() << 34070 << 63081 << 90083 << 103040 << 113002 << 121081 << 123083 << 123121;
  if (runID==6632)  vNoisyCh = make_vector<int>() << 63081 << 90083 << 103040 << 113002 << 123083;
  if (runID==6670)  vNoisyCh = make_vector<int>() << 40070 << 63081 << 90083 << 103040 << 113002 << 121081 << 123083;
  if (runID==6683)  vNoisyCh = make_vector<int>() << 63081 << 90083 << 103040 << 113002 << 121081 << 123083;
  fLAVMatching->ClearNoisyChannels();
  for (auto &j : vNoisyCh) fLAVMatching->SetNoisyChannel(j);
  vNoisyCh.clear();
}

void PnnFilter::ProcessSpecialTriggerUser(int iEvent, unsigned int triggerType){
}

void PnnFilter::Process(int iEvent){
  fCedarEvent = (TRecoCedarEvent*)GetEvent("Cedar");
  fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
  fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
  fCHODEvent = (TRecoCHODEvent*)GetEvent("CHOD");
  fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
  fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
  fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
  fL0Data = GetL0Data();
  fRawHeader = GetEventHeader();

  // Clear
  vector<int> fvCHODQ[8];
  fMUV3Candidate.clear();
  //fLAVMatching->Clear();

  // Load analyzers
  fMUV3Candidate = *(vector<SpectrometerMUV3AssociationOutput>*)GetOutput("SpectrometerMUV3Association.Output");

  // Count events
  IncrementCounter("NTotal");

  // General quality
  if (fRawHeader->GetEventQualityMask()>0)  return;

  // Skip periodics
  if (fL0Data->GetDataType()&0x2) return;

  // Filtering conditions
  if (!FilteringCondition()) return;

  // Preliminary conditions for pinunu
  if (fGigaTrackerEvent->GetErrorMask()) return;
  if (!fSpectrometerEvent->GetNCandidates()) return;
  if (fSpectrometerEvent->GetNCandidates()>10) return;
  if (fCHODEvent->GetNHits()<2) return;
  if (!CHODQ1(fvCHODQ)) return;

  // Loop on tracks
  bool isToFilter = false;
  vector<double> chodPar;
  double ttimeoff = -2.1;
  for (int jT(0); jT<fSpectrometerEvent->GetNCandidates(); jT++) {
    chodPar.clear();
    TRecoSpectrometerCandidate *track = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(jT);

    // Track selection
    if (track->GetNChambers()<4) continue;
    if (track->GetChi2()>30) continue;
    double trackP = 0.9987*(1-track->GetCharge()*0.3e-05*track->GetMomentum()*0.001)*track->GetMomentum();
    if (fabs(fabs(track->GetMomentumBeforeFit())-trackP)>25000.) continue;
    if (!fGeo->InAcceptance(track,kNewCHOD,0,-1,-1)) continue;
    if (!fGeo->InAcceptance(track,kCHOD,0,125.,-1)) continue;
    if (!fGeo->InAcceptance(track,kLKr,0,145.,-1)) continue;
    if (!fGeo->InAcceptance(track,kMUV3,0,-1,-1)) continue;
    if (MultiTrack(jT,trackP)) continue;
    if (trackP>50000.) continue;
    if (trackP<8000.) continue;

    // CHOD matching
    double ttime = track->GetTime()+ttimeoff;
    chodPar = CHODTrackMatching(track->xAtAfterMagnet(fGeo->GetZCHODVPlane()),track->yAtAfterMagnet(fGeo->GetZCHODHPlane()),ttime,fvCHODQ);
    FillHisto("Track_chodminchi2",chodPar[0]);
    if (chodPar[0]>20) continue; // discriminant
    FillHisto("Track_chodtrackmatch",chodPar[2],sqrt(chodPar[1]));
    if (fabs(chodPar[2])>30) continue; // CHOD time - Track time
    double chodTime = chodPar[2]+ttime;

    // KTAG matching
    double ktagTime = 999999.;
    if (!KTAGTrackMatching(chodTime,&ktagTime)) continue;
    FillHisto("CHODAnalysis_dtime",16*chodPar[3]+((int)(chodPar[4]-64)%16),ktagTime-chodPar[5]);
    FillHisto("CHODAnalysis_dtime",16*chodPar[4]+((int)chodPar[3]%16),ktagTime-chodPar[6]);
    double dtime = ktagTime-chodTime;
    FillHisto("KTAGAnalysis_time",dtime);
    if (fabs(dtime)>5) continue;

    // GTK condition
    int nhits[3] = {0,0,0};
    for (int jHit(0);jHit<fGigaTrackerEvent->GetNHits();jHit++) {
      TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*)fGigaTrackerEvent->GetHit(jHit);
      if (fabs(hit->GetTime()-ktagTime)<2.) nhits[hit->GetStationNo()]++;
    }
    if (!nhits[0]||!nhits[1]||!nhits[2]) continue;

    // Muon rejection
    if (MUV3TrackMatching(jT,track->xAtAfterMagnet(fGeo->GetZMUV3()),track->yAtAfterMagnet(fGeo->GetZMUV3()),chodTime)) continue;

    // Photon rejection
    double trackELKr = -1.;
    if (LKrPhotons(track->xAtAfterMagnet(fGeo->GetZLKr()),track->yAtAfterMagnet(fGeo->GetZLKr()),chodTime,&trackELKr)) continue;
    if (LAVPhotons(chodTime)) continue;

    // Reject positrons
    double eovp = trackELKr>=0 ? trackELKr/trackP : -1.;
    FillHisto("Track_eovp",eovp);

    // an event is filtered if at least a track exists, matching CHOD and KTAG and not matching MUV3, LKr and LAV signals
    isToFilter = true;
  }

  // Filter pnn
  if (isToFilter) {
    IncrementCounter("NPnn");
    FilterAccept();
  }
}

void PnnFilter::PostProcess(){
}

void PnnFilter::EndOfBurstUser(){
}

void PnnFilter::EndOfRunUser(){
}

void PnnFilter::EndOfJobUser(){
  SaveAllPlots();
}

void PnnFilter::DrawPlot(){
}

PnnFilter::~PnnFilter(){
}

//##################################
//#######                   ########
//####### Analysis Routines ########
//#######                   ########
//##################################

bool PnnFilter::MultiTrack(int iT, double pTrack) {
  bool isMultiTrack = false;
  if (fSpectrometerEvent->GetNCandidates()<2) return isMultiTrack;

  // Select good candidates to pair with
  vector<int> idTrack(fSpectrometerEvent->GetNCandidates());
  iota(begin(idTrack),end(idTrack),0);
  TrackQualityCondition tq(fSpectrometerEvent,iT);
  int nT = distance(idTrack.begin(),partition(idTrack.begin(),idTrack.end(),tq));
  if (!nT) return isMultiTrack;
  vector<int> vTrack;
  vTrack.assign(idTrack.begin(),idTrack.begin()+nT);

  // Find the multi vertex
  TLorentzVector ptracks[2];
  TVector3 postracks[2];
  TRecoSpectrometerCandidate *piT = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(iT);
  ptracks[0] = Get4Momentum(pTrack,piT->GetSlopeXBeforeMagnet(),piT->GetSlopeYBeforeMagnet(),MPI);
  postracks[0] = piT->GetPositionBeforeMagnet();
  for (auto &jT : vTrack) {
    TRecoSpectrometerCandidate *pjT = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(jT);
    double corrP = 0.9987*(1-pjT->GetCharge()*0.3e-05*pjT->GetMomentum()*0.001)*pjT->GetMomentum();
    ptracks[1] = Get4Momentum(corrP,pjT->GetSlopeXBeforeMagnet(),pjT->GetSlopeYBeforeMagnet(),MPI);
    postracks[1] = pjT->GetPositionBeforeMagnet();
    double cda = 9999.;
    TVector3 vertex = MultiTrackVertex(2,ptracks,postracks,&cda);
    if (iT>jT) FillHisto("StrawAnalysis_CDAZVertex",vertex.Z(),cda);
    double dtime = piT->GetTime()-pjT->GetTime();
    if (cda<10 && vertex.Z()>80000. && vertex.Z()<180000. && fabs(dtime)<30.) isMultiTrack = true;
  }

  return isMultiTrack;
}

TLorentzVector PnnFilter::Get4Momentum(double pmag, double thx, double thy, double mass) {
  TLorentzVector pmom;
  double pmomz = pmag/sqrt(1.+thx*thx+thy*thy);
  double pmomx = pmomz*thx;
  double pmomy = pmomz*thy;
  pmom.SetXYZM(pmomx,pmomy,pmomz,mass);
  return pmom;
}

TVector3 PnnFilter::MultiTrackVertex(int nTracks, TLorentzVector *ptracks, TVector3 *postracks, double *cda) {
  TVector3 avPosition(0,0,0);
  TVector3 avSlope(0,0,0);
  TVector3 avSlope2(0,0,0);
  TVector3 avMixed(0,0,0);

  // Compute Z as the position of minimum apporach between tracks
  Double_t z0 = 0;
  for (Int_t j=0; j<nTracks; j++) {
    TVector3 position = postracks[j];
    TLorentzVector momentum = ptracks[j];
    avPosition += position;
    TVector3 ddz = momentum.Vect()*(1./momentum.Vect().Z());
    avSlope += ddz;
    avSlope2 += TVector3(ddz.X()*ddz.X(),ddz.Y()*ddz.Y(),ddz.Z()*ddz.Z());
    avMixed += TVector3(position.X()*ddz.X(),position.Y()*ddz.Y(),position.Z()*ddz.Z());
    z0 = position.Z();
  }
  avPosition = (1./nTracks)*avPosition;
  avSlope = (1./nTracks)*avSlope;
  avSlope2 = (1./nTracks)*avSlope2;
  avMixed = (1./nTracks)*avMixed;
  Double_t num = nTracks*(avMixed.X()+avMixed.Y())-nTracks*(avPosition.X()*avSlope.X()+avPosition.Y()*avSlope.Y());
  Double_t den = nTracks*(avSlope2.X()+avSlope2.Y())-nTracks*(avSlope.X()*avSlope.X()+avSlope.Y()*avSlope.Y());
  Double_t zvertex = z0-num/den;

  // Compute the trasnverse position and the cda
  TVector3 avPosVtx(0,0,0);
  TVector3 avPosVtx2(0,0,0);
  for (Int_t j=0; j<nTracks; j++) {
    TVector3 position = postracks[j];
    TLorentzVector momentum = ptracks[j];
    TVector3 posvtx = position+momentum.Vect()*(1./momentum.Vect().Z())*(zvertex-position.Z());
    avPosVtx += posvtx;
    avPosVtx2 += TVector3(posvtx.X()*posvtx.X(),posvtx.Y()*posvtx.Y(),posvtx.Z()*posvtx.Z());
  }
  avPosVtx = (1./nTracks)*avPosVtx;
  avPosVtx2 = (1./nTracks)*avPosVtx2;
  *cda = sqrt(avPosVtx2.X()+avPosVtx2.Y()-avPosVtx.X()*avPosVtx.X()-avPosVtx.Y()*avPosVtx.Y());

  return TVector3(avPosVtx.X(),avPosVtx.Y(),zvertex);
}

bool PnnFilter::CHODQ1(vector<int> *fvCHODQ) {
  bool isQ1 = false;

  // Sort hits per increasing channel number
  vector<int> idHit(fCHODEvent->GetNHits());
  iota(begin(idHit),end(idHit),0);
  ChannelOrder co(fCHODEvent);
  sort(idHit.begin(),idHit.end(),co);

  // Sort hits per plane
  PlaneCondition pc(fCHODEvent);
  auto from = idHit.begin();
  int nV = distance(from,partition(from,idHit.end(),pc));
  if (!nV) return isQ1;
  if (!(fCHODEvent->GetNHits()-nV)) return isQ1;
  vector<int> vP[2];
  vP[0].assign(from,from+nV);
  vP[1].assign(from+nV,idHit.end());

  // Sort hits per quadrant
  vector<int>::iterator fromP[2];
  fromP[0] = vP[0].begin();
  fromP[1] = vP[1].begin();
  int jQ = 0;
  while ((fromP[0]!=vP[0].end() || fromP[1]!=vP[1].end()) && jQ<4) {
    for (int jP(0); jP<2; jP++) {
      QuadrantCondition qc(fCHODEvent,jQ,jP);
      int nQ = distance(fromP[jP],partition(fromP[jP],vP[jP].end(),qc));
      if (!nQ) continue;
      fvCHODQ[jQ+4*jP].assign(fromP[jP],fromP[jP]+nQ);
      fromP[jP] += nQ;
    }
    jQ++;
  }

  // Select events with hits in at least a corresponding H-V quadrant
  for (int jq(0); jq<4; jq++) {
    if (fvCHODQ[jq].size()&&fvCHODQ[jq+4].size()) isQ1 = true;
  }

  return isQ1;
}

vector<double> PnnFilter::CHODTrackMatching(double xPos, double yPos, double ttime, vector<int> *fvCHODQ) {
  vector<double> par(7);

  // Loop over the quadrant pairs
  double minchi2chod = 99999999.;
  for (int jQ(0); jQ<4; jQ++) {
    double minpar[7];
    double minchi2 = 99999999.;
    if (!fvCHODQ[jQ].size() || !fvCHODQ[jQ+4].size()) continue;
    for (auto &iV : fvCHODQ[jQ]) {
      TRecoCHODHit *hV = (TRecoCHODHit*)fCHODEvent->GetHit(iV);
      int cV = hV->GetChannelID();
      for (auto &iH : fvCHODQ[jQ+4]) {
        TRecoCHODHit *hH = (TRecoCHODHit*)fCHODEvent->GetHit(iH);
        int cH = hH->GetChannelID();
        double tV = hV->GetTime()-CHODTimeCorrection(cV,(cH-64)%16,hV->GetTimeWidth(),0);
        double tH = hH->GetTime()-CHODTimeCorrection(cH,cV%16,hH->GetTimeWidth(),0);
        double dtime = 0.5*(tV+tH)-ttime;
        double dist2 = (fCHODPosV[cV]-xPos)*(fCHODPosV[cV]-xPos)+(fCHODPosH[cH-64]-yPos)*(fCHODPosH[cH-64]-yPos);
        double chi2chod = (tV-tH)*(tV-tH)/(9*3*3)+dtime*dtime/(9*7*7)+dist2/(4*13*13);
        if (chi2chod<minchi2) {
          minchi2 = chi2chod;
          minpar[0] = minchi2;
          minpar[1] = dist2;
          minpar[2] = dtime;
          minpar[3] = cV;
          minpar[4] = cH;
          minpar[5] = tV;
          minpar[6] = tH;
        }
      }
    }
    if (minchi2<minchi2chod) {
      minchi2chod = minchi2;
      par.clear();
      for (int k(0); k<7; k++) par.push_back(minpar[k]);
    }
  }

  return par;
}

double PnnFilter::CHODTimeCorrection(int iS, int iP, double tot, double enslew) {
  double t0 = fCHODAllT0[iS][iP]<99. ? fCHODAllT0[iS][iP]-fCHODAllFineT0[iS][iP] : 0.;
  double efftot = tot<15. ? tot : 15.;
  double sC = tot>0. ? fCHODAllSlewSlope[iS][iP]*efftot+fCHODAllSlewConst[iS][iP] : 0.;
  return t0+enslew*sC;
}

bool PnnFilter::KTAGTrackMatching(double timeref, double *ktagtime) {
  Double_t mintime = 9999999.;
  bool isCand = false;
  for (Int_t jktag=0; jktag<fCedarEvent->GetNCandidates(); jktag++) {
    TRecoCedarCandidate *ktag = (TRecoCedarCandidate *)fCedarEvent->GetCandidate(jktag);
    if (ktag->GetNSectors()<4) continue;
    Double_t dt = ktag->GetTime()-timeref;
    FillHisto("KTAGAnalysis_alltime",dt);
    if (fabs(dt)<fabs(mintime)) {
      mintime = dt;
      isCand = true;
    }
  }
  if (!isCand) return false;
  *ktagtime = mintime+timeref;
  return true;
}

bool PnnFilter::MUV3TrackMatching(int jTrack, double xPos, double yPos, double timeref) {
  bool isMUV3 = false;
  if (!fMUV3Candidate[jTrack].GetNAssociationRecords()) return isMUV3;
  TVector2 pTrack(xPos,yPos);
  for (int k(0); k<fMUV3Candidate[jTrack].GetNAssociationRecords(); k++) {
    int iMUV3 = fMUV3Candidate[jTrack].GetAssociationRecord(k).GetMuonID();
    TRecoMUV3Candidate *cMUV3 = (TRecoMUV3Candidate *)fMUV3Event->GetCandidate(iMUV3);
    double dtime = cMUV3->GetTime()-timeref;
    TVector2 pMUV3(cMUV3->GetX(),cMUV3->GetY());
    double dist = (pMUV3-pTrack).Mod();
    FillHisto("MUV3Analysis_timedist",dtime,dist);
    if (fabs(dtime)<4.) isMUV3 = true;
  }
  return isMUV3;
}

bool PnnFilter::LKrPhotons(double xPos, double yPos, double timeref, double *energy) {
  bool isPhoton = false;
  double mindist = 9999999.;
  double minenergy = -1.;
  for (int jC(0); jC<fLKrEvent->GetNCandidates(); jC++) {
    TRecoLKrCandidate *pC = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(jC);
    double dist = sqrt(((pC->GetClusterX()+1.3)-xPos)*((pC->GetClusterX()+1.3)-xPos)+((pC->GetClusterY()+0.8)-yPos)*((pC->GetClusterY()+0.8)-yPos));
    double dtime = pC->GetClusterTime()-timeref;
    FillHisto("LKrAnalysis_photon",dist,dtime);
    if (dist<150. && fabs(dtime)<10.) { // track associated if less than 15 cm and +-10 ns in time with reference time
      if (dist<mindist) {
        mindist = dist;
        minenergy = LKrCorrectedEnergy(pC);
      }
    }
    if (dist<=180.) continue; // do not look for clusters around 180 mm from the track impact point
    if (fabs(dtime)<5.) isPhoton = true;
  }
  *energy = minenergy;
  return isPhoton;
}

double PnnFilter::LKrCorrectedEnergy(TRecoLKrCandidate *pC) {
  double ue = 0.001*pC->GetClusterEnergy();
  double ce = ue;
  if (pC->GetNCells()>5) {
    if (ue<22) ce = ue/(0.7666+0.0573489*log(ue));
    if (ue>=22 && ue<65) ce = ue/(0.828962+0.0369797*log(ue));
    if (ue>=65) ce = ue/(0.828962+0.0369797*log(65));
    ce = 0.99*ce/(0.94+0.0037*ce-9.4e-05*ce*ce+8.9e-07*ce*ce*ce);
  }
  ce *= 1.03;
  double radius = 0.1*sqrt(pC->GetClusterX()*pC->GetClusterX()+pC->GetClusterY()*pC->GetClusterY());
  if (radius>=14 && radius<=18.5) ce = ce/(0.97249+0.0014692*radius)*0.9999;
  double rcorr2 = 0;
  if (radius>=14 && radius<18) rcorr2 = 0.0042-3.7e-4*radius;
  if (radius>=18 && radius<20) rcorr2 = -0.00211;
  if (radius>=20 && radius<22) rcorr2 = -0.01694-7.769e-4*radius;
  ce *= (1-rcorr2);
  if (ce<15) ce = (ce+0.015)/(15+0.015)*15*0.9999;
  ce *= 1000.;
  return ce;
}

bool PnnFilter::LAVPhotons(double timeref) {
  fLAVMatching->SetReferenceTime(timeref);
  fLAVMatching->SetTimeCuts(10,10);
  if (fLAVMatching->LAVHasTimeMatching(fLAVEvent)) {
    FillHisto("LAVAnalysis_time_broad",fLAVMatching->GetBestTimeOfMatchedBlocks());
    fLAVMatching->SetTimeCuts(2,2);
    if (fLAVMatching->LAVHasTimeMatching(fLAVEvent)) {
      FillHisto("LAVAnalysis_time_tight",fLAVMatching->GetBestTimeOfMatchedBlocks());
      return true;
    }
  }
  return false;
}

//////////////////////////////////////////////
// Filtering conditions for control trigger //
//////////////////////////////////////////////
bool PnnFilter::FilteringCondition() {
  bool isControl = (fL0Data->GetDataType()>>4)&1;
  bool isPhysics = (fL0Data->GetDataType()>>0)&1; // not exclusive with control
  bool isPnnMask = isPhysics && (fL0Data->GetTriggerFlags()>>1)&1;

  switch (fFilterControl) {
    case 0: // always pass the pnn filter
    return true;
    break;

    case 1: // filter control only
    if (isControl) {
      FilterControl(); // pass through the control filter (also if control & physics)
      return false; // do not pass through the pnn filter
    } else return false; // do not pass neither through the control nor the pnn filter
    break;

    case 2: // filter pnn
    if (!isPnnMask) return false; // do not pass through the pnn filter
    return true; // pass through the pnn filter (also if control & physics)
    break;

    case 3:
    if (isControl) { // filter control and pnn
      FilterControl(); // pass through the control filter (also if control & physics)
      return false; // do not pass through the pnn filter
    } else {
      if (!isPnnMask) return false; // do not pass through the pnn filter
      return true; // pass through the pnn filter (physics only, not physics & control)
    }
    break;

    default:
    return false; // do not pass neither through the control nor the pnn filter
    break;
  }

  return true; // pass through the pnn filter (should never reach here);
}

bool PnnFilter::FilterControl() {

  // Filter 0 track events
  if (!fSpectrometerEvent->GetNCandidates()) {
    IncrementCounter("NControl_0trk");
    if (!(GetCounterValue("NControl_0trk")%(fCDW*10))) {
      IncrementCounter("NControl_Filter");
      FilterAccept();
    }
    return 0;
  }

  // Loose selection for events with at least 1 track
  if (fGigaTrackerEvent->GetErrorMask()) return 1;
  vector<int> fvCHODQ[8];
  vector<double> chodPar;
  bool isGood = false;
  double ttimeoff = -2.1;
  if (!CHODQ1(fvCHODQ)) return 1;
  for (int jT(0); jT<fSpectrometerEvent->GetNCandidates(); jT++) {
    chodPar.clear();
    TRecoSpectrometerCandidate *track = (TRecoSpectrometerCandidate *)fSpectrometerEvent->GetCandidate(jT);
    if (track->GetNChambers()<4) continue;
    if (!fGeo->InAcceptance(track,kCHOD,0,125.,-1)) continue;
    if (!fGeo->InAcceptance(track,kLKr,0,140.,-1)) continue;
    if (!fGeo->InAcceptance(track,kMUV3,0,-1,-1)) continue;
    double ttime = track->GetTime()+ttimeoff;
    chodPar = CHODTrackMatching(track->xAtAfterMagnet(fGeo->GetZCHODVPlane()),track->yAtAfterMagnet(fGeo->GetZCHODHPlane()),ttime,fvCHODQ);
    if (chodPar[0]>50) continue; // discriminant
    double chodTime = chodPar[2]+ttime;

    // KTAG matching
    double ktagTime = 999999.;
    bool isKTAG = !KTAGTrackMatching(chodTime,&ktagTime);

    // Reference time
    double timeref = (isKTAG && fabs(ktagTime-chodTime)<=5) ? ktagTime : chodTime;

    // GTK condition
    int nhits[3] = {0,0,0};
    for (int jHit(0);jHit<fGigaTrackerEvent->GetNHits();jHit++) {
      TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*)fGigaTrackerEvent->GetHit(jHit);
      if (fabs(hit->GetTime()-timeref)<2.) nhits[hit->GetStationNo()]++;
    }
    if (!nhits[0]||!nhits[1]||!nhits[2]) continue;
    isGood = true;
  }

  // Filter 1 track events
  if (!isGood) {
    IncrementCounter("NControl_NoSel");
    if (!(GetCounterValue("NControl_NoSel")%(5*fCDW))) {
      IncrementCounter("NControl_Filter");
      FilterAccept();
    }
    return 1;
  }
  IncrementCounter("NControl_Filter");
  FilterAccept();
  return 1;
}
