#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "TwoPhotonAnalysis.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "Parameters.hh"
using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

#include "AnalysisTools.hh"
TwoPhotonAnalysis::TwoPhotonAnalysis(Core::BaseAnalysis *ba) : Analyzer(ba, "TwoPhotonAnalysis")
{
  RequestL0Data();
  RequestTree("LKr",new TRecoLKrEvent);
  RequestTree("Cedar",new TRecoCedarEvent);
  RequestTree("CHOD",new TRecoCHODEvent);
  RequestTree("LAV",new TRecoLAVEvent);

  // Common variables
  fState = kOValid;

  // Geometry
  fZStation[0] = 18350.8;
  fZStation[1] = 19406.6;
  fZStation[2] = 20445.9;
  fZStation[3] = 21888.5;
  fXCenter[0] = 10.12;
  fXCenter[1] = 11.44;
  fXCenter[2] = 9.24;
  fXCenter[3] = 5.28;

  // Analysis tools
  Parameters *par = Parameters::GetInstance();
  tools = AnalysisTools::GetInstance();
  AddParam("Year", &fYear, 2016); // Default is 2016

  // LKr Analysis
  fLKrAnalysis = new LKrAnal(2,ba);
  fLkClusCand = new LKrCandidate();
  fLkCellCand = new LKrCandidate();

  // CHOD
  // fCHODAnalysis = new CHODAnal(1,ba);
  fCHODAnalysis = new CHODAnal(0,ba);

  // LAV
  fLAVAnalysis = new LAVAnal(1,ba);

}

void TwoPhotonAnalysis::InitOutput(){
  RegisterOutput("isTwoPhotonEvent",&fIsTwoPhotonEvent);
  RegisterOutput("isPiP0Event",&fIsPiP0Event);
  RegisterOutput("P0Gamma1",&fIDPhotons1);
  RegisterOutput("P0Gamma2",&fIDPhotons2);
  RegisterOutput("Pi0Vertex",&fPi0Vertex);
  RegisterOutput("Pi0Momentum",&fPi0Momentum);
  RegisterOutput("PicMomentum",&fPicMomentum);
  RegisterOutput("SinglePi0Time",&fPi0Time);
}

void TwoPhotonAnalysis::InitHist(){
  BookHisto(new TH1F("NEMDoublets","",20,0,20));
  BookHisto(new TH1F("IsTwoPhoton","",2,0,2));
  BookHisto(new TH1F("IsK2pi","",2,0,2));

  BookHisto(new TH1F("TwoPhotonEvents","",2,0,2));
  BookHisto(new TH1F("AllVertex_z","",250,5000,20000));
  BookHisto(new TH1F("Pi0s","",10,0,10));
  BookHisto(new TH1F("Pic_momentum","",100,0,100));
  BookHisto(new TH2F("Pic_posAtCHOD","",120,-120,120,120,-120,120));
  BookHisto(new TH1F("Pi0_mmiss_raw","",375,-0.1,0.15));
  BookHisto(new TH2F("Pi0_mmissVSPhi_pic","",320,0,6.4,375,-0.1,0.15));
  BookHisto(new TH2F("Pi0_mmissVSPhi_pi0","",320,0,6.4,375,-0.1,0.15));
  BookHisto(new TH2F("Pi0_mmiss","",100,0,100,375,-0.1,0.15));
  BookHisto(new TH1F("Pic_isolation","",200,0,200));
  BookHisto(new TH2F("Pi0_mmiss_acceptance","",50,0,100,375,-0.1,0.15));
  BookHisto(new TH2F("Pi0_mmiss_isolation","",50,0,100,375,-0.1,0.15));
  BookHisto(new TH2F("Pi0_mmiss_timestamp","",24,0,240000000,375,-0.1,0.15));
  BookHisto(new TH2F("Pic_lkrclus_mindistvst","",400,-100,100,300,0,300));
  BookHisto(new TH2F("Pic_lkrcell_mindistvst","",400,-100,100,300,0,300));
  BookHisto(new TH2F("Pi0_mmiss_final","",50,0,100,375,-0.1,0.15));
  BookHisto(new TH2F("Pi0_mmiss_final_vertex","",250,5000,20000,375,-0.1,0.15));
}

void TwoPhotonAnalysis::DefineMCSimple(){
}

void TwoPhotonAnalysis::StartOfJobUser(){
}

void TwoPhotonAnalysis::StartOfRunUser(){
}

void TwoPhotonAnalysis::StartOfBurstUser(){
  // EventHeader *rawheader = GetEventHeader();
  EventHeader *rawheader = GetEventHeader();
  if(!GetWithMC()){
    //fCHODAnalysis->StartBurst(rawheader->GetRunID(),rawheader->GetBurstID());
  //fLKrAnalysis->StartBurst(fYear,rawheader->GetRunID());
  //fLAVAnalysis->StartBurst(rawheader->GetRunID(),rawheader->GetBurstID());
  }
}

void TwoPhotonAnalysis::Process(int iEvent){

  if(GetWithMC())
    if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
  fCedarEvent = (TRecoCedarEvent*)GetEvent("Cedar");
  fCHODEvent = (TRecoCHODEvent*)GetEvent("CHOD");
  fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
  fL0Data = GetL0Data();

  // Check if the event has passed the single track analysis
  Bool_t ots = *(Bool_t *)GetOutput("MainAnalysis.OneTrackEvent",fState);
  if (!ots) return ;
  fNTracks = *(Int_t *)GetOutput("MainAnalysis.NKaonEventCandidates",fState);
  if (!fNTracks) return;
  fDownstreamTrack = (DownstreamParticle *)GetOutput("MainAnalysis.KaonEventDownstreamTrack",fState);
  fUpstreamTrack = (UpstreamParticle *)GetOutput("MainAnalysis.KaonEventUpstreamTrack",fState);

  // EventHeader* header = GetEventHeader();
  // if(header->GetEventNumber() != 1441359) return;

  fIsTwoPhotonEvent = 0;
  fIsPiP0Event = 0;
  fIDPhotons1 = -1;
  fIDPhotons2 = -1;
  for (Int_t j=0; j<10; j++) {
    fPi0sMomentum[j].SetXYZM(0,0,0,999999.);
    fPi0sTime[j] = -999999.;
    fPi0sDoubletID[j] = -1;
  }
  fPi0Momentum.SetXYZM(0,0,0,999999.);
  fPicMomentum.SetXYZM(0,0,0,999999.);
  fPi0Vertex.SetXYZ(-999999.,-999999.,0);
  fPi0Time = -999999.;
  // fEventHeader = GetEventHeader();
  fEventHeader = GetEventHeader();

  // General cut
  Int_t eventqualitymask = fEventHeader->GetEventQualityMask();
////  if(eventqualitymask&(1<<kHAC)) eventqualitymask-=(1<<kHAC);
  if (eventqualitymask>0 || GetWithMC()) return;

  // Trigger selection
  if (fYear==2016) {
    if (fL0Data->GetDataType()&0x2) return; // no periodics;
  }
  fL0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
  fL0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();
  Bool_t PhysicsData   = fL0DataType    & 0x1;

  fPinunuTrigger  = SelectTrigger(2,fL0DataType,fL0TriggerWord);
  fControlTrigger = SelectTrigger(0,fL0DataType,fL0TriggerWord);

  if(!fPinunuTrigger && !fControlTrigger && !GetWithMC()) return;
  fLKrAnalysis->SetEvent(fLKrEvent);
  fAnalysisOutput = Analysis();

  SetOutputState("isTwoPhotonEvent",kOValid);
  SetOutputState("isPiP0Event",kOValid);
  SetOutputState("P0Gamma1",kOValid);
  SetOutputState("P0Gamma2",kOValid);
  SetOutputState("Pi0Vertex",kOValid);
  SetOutputState("Pi0Momentum",kOValid);
  SetOutputState("PicMomentum",kOValid);
  SetOutputState("SinglePi0Time",kOValid);
}

void TwoPhotonAnalysis::PostProcess(){
}

void TwoPhotonAnalysis::EndOfBurstUser(){
}

void TwoPhotonAnalysis::EndOfRunUser(){
}

void TwoPhotonAnalysis::EndOfJobUser(){
  fLKrAnalysis->GetUserMethods()->SaveAllPlots();
  //fCHODAnalysis->GetUserMethods()->SaveAllPlots();
  //fLAVAnalysis->GetUserMethods()->SaveAllPlots();
  SaveAllPlots();
}

void TwoPhotonAnalysis::DrawPlot(){
}



//////////////////////////////////////////////////////////////////
// USER METHODS //////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

//////////////
// Steering //
//////////////
Bool_t TwoPhotonAnalysis::Analysis() {
  Bool_t mcflag = (fMCSimple.fStatus==MCSimple::kEmpty) ? 0 : 1;
  fFineTriggerTime = fEventHeader->GetFineTime()*24.951059536/256.;
  fLKrAnalysis->Clear(2);
  // fLKrAnalysis->PhotonAnalysis(mcflag,fEventHeader->GetRunID(),fLKrEvent,fL0Data);
  fLKrAnalysis->PhotonAnalysis();
  fNEMDoublets = fLKrAnalysis->GetNEMDoublets();
  FillHisto("NEMDoublets", fNEMDoublets);
  //std::cout << " NemDoublets before" << fNEMDoublets << std::endl;
  if (!fNEMDoublets) return false;
  fTwoEMClusterID = fLKrAnalysis->GetTwoEMClustersID();
  fDoubletCounter = 0;
  if (!SetKaon()) return 0;
  fNPi0s = 0;
  SelectPi0();
  fIsTwoPhotonEvent = AnalyzeTwoPhotonEvent();
  fIsPiP0Event = SelectPiP0Event();
  FillHisto("IsTwoPhoton", fIsTwoPhotonEvent);
  FillHisto("IsK2pi", fIsPiP0Event);

  //std::cout << " 2 gamma" << fIsTwoPhotonEvent << " k2pi" << fIsPiP0Event  << std::endl;
  return true;
}

////////////////////////////////////
// Definition of the trigger type //
////////////////////////////////////
void TwoPhotonAnalysis::SelectTriggerType() {
}

/////////////////////////
// Routine of analysis //
/////////////////////////
Bool_t TwoPhotonAnalysis::SelectPiP0Event() {
  if (!fIsTwoPhotonEvent) return 0;
  if (!SetPiPlus()) return 0;
  FillHisto("Pi0_mmiss_acceptance",fPi0Momentum.P(),fPicMomentum.M2());
  if (!PiPlusIsolation()) return 0;
  FillHisto("Pi0_mmiss_isolation",fPi0Momentum.P(),fPicMomentum.M2());
  FillHisto("Pi0_mmiss_timestamp",fEventHeader->GetTimeStamp(),fPicMomentum.M2());
  //if (!PiPlusCHODHit()) return 0;
  fPi0Time = fPi0sTime[0];
  FillHisto("Pi0_mmiss",fPi0Momentum.P(),fPicMomentum.M2());
  if (!PiPlusLKrCluster()) return 0;
  FillHisto("Pi0_mmiss_final",fPi0Momentum.P(),fPicMomentum.M2());
  //if (LAVCandidate()) return 0;
  FillHisto("Pi0_mmiss_final_vertex",fPi0Vertex.Z(),fPicMomentum.M2());
  if (fPicMomentum.M2()<0.008 || fPicMomentum.M2()>0.031) return 0;
  if(fPi0Vertex.Z() < 10500 || fPi0Vertex.Z() > 18000) return 0;

  fIDPhotons1 = fTwoEMClusterID[2*fPi0sDoubletID[0]];
  fIDPhotons2 = fTwoEMClusterID[2*fPi0sDoubletID[0]+1];
  return 1;
}

//////////////////////////////
// Select two photon events //
// Only one pi0             //
//////////////////////////////
Bool_t TwoPhotonAnalysis::AnalyzeTwoPhotonEvent() {
  FillHisto("Pi0s",fNPi0s);
  if (fNPi0s!=1) return 0;
  fPi0Momentum = fPi0sMomentum[0];
  fPicMomentum = fKaonMomentum-fPi0Momentum;
  FillHisto("Pi0_mmiss_raw",fPicMomentum.M2());
  Double_t phi_pic = fPicMomentum.Vect().Phi();
  Double_t phisign_pic = phi_pic>=0 ? phi_pic : 2*3.141592653+phi_pic;
  FillHisto("Pi0_mmissVSPhi_pic",phisign_pic,fPicMomentum.M2());
  Double_t phi_pi0 = fPi0Momentum.Vect().Phi();
  Double_t phisign_pi0 = phi_pi0>=0 ? phi_pi0 : 2*3.141592653+phi_pi0;
  FillHisto("Pi0_mmissVSPhi_pi0",phisign_pi0,fPicMomentum.M2());
  return 1;
}

////////////////////////
// Pi0 reconstruction //
////////////////////////
void TwoPhotonAnalysis::SelectPi0() {
  Double_t tgamma[2];
  Double_t egamma[2];
  TVector3 posgamma[2];
  for (Int_t j=0; j<2; j++) {
    TRecoLKrCandidate *thisPhoton = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(fTwoEMClusterID[2*fDoubletCounter+j]);
    tgamma[j] = thisPhoton->GetClusterTime();
    egamma[j] = thisPhoton->GetClusterEnergy()/1000.;
    posgamma[j] = TVector3(thisPhoton->GetClusterX()/10.,thisPhoton->GetClusterY()/10.,24109.3);
  }

  // Compute and cut on Z vertex
  Double_t d2gamma = (posgamma[0].X()-posgamma[1].X())*(posgamma[0].X()-posgamma[1].X())+(posgamma[0].Y()-posgamma[1].Y())*(posgamma[0].Y()-posgamma[1].Y());
  Double_t deltaz = sqrt(egamma[0]*egamma[1]*d2gamma)/0.1349766;
  Double_t zvertex = posgamma[0].Z()-deltaz;
  FillHisto("AllVertex_z",zvertex);
  Double_t thetaxkaon = fKaonMomentum.X()/fKaonMomentum.Z();
  TVector3 vertex = TVector3((zvertex-10180.)*tan(thetaxkaon),0.,zvertex);
  TVector3 dist[2];
  TLorentzVector pgamma[2];
  for (Int_t j=0; j<2; j++) {
    dist[j] = posgamma[j]-vertex;
    pgamma[j].SetVectM(egamma[j]*dist[j]*(1./dist[j].Mag()),0);
  }
  Bool_t isNotPi0 = 0;
  if (zvertex<10500.||zvertex>18000.) isNotPi0= 1;

  // Cut on photon position at RICH
  Bool_t notInAcceptance = 0;
  for (Int_t j=0; j<2; j++) {
    TVector3 posRICHFront = vertex+pgamma[j].Vect()*(1/pgamma[j].Z())*(21960-zvertex);
    TVector3 posRICHEnd = vertex+pgamma[j].Vect()*(1/pgamma[j].Z())*(23725.3-zvertex);
    Double_t r2RICHFront = (posRICHFront.X()-3.4)*(posRICHFront.X()-3.4)+posRICHFront.Y()*posRICHFront.Y();
    Double_t r2RICHEnd = posRICHEnd.X()*posRICHEnd.X()+posRICHEnd.Y()*posRICHEnd.Y();
    if (r2RICHFront<100) notInAcceptance = 1;
    if (r2RICHEnd<144) notInAcceptance = 1;
  }
  if (notInAcceptance) isNotPi0 = 1;

  // Store Pi0 information
  if (fNPi0s>=10) isNotPi0 = 1;
  if (!isNotPi0) {
    fPi0sMomentum[fNPi0s] = pgamma[0]+pgamma[1];
    fPi0sTime[fNPi0s] = 0.5*(tgamma[0]+tgamma[1]);
    fPi0sDoubletID[fNPi0s] = fDoubletCounter;
    fPi0Vertex = vertex;
    UpdatePi0Counter();
  }
  UpdateDoubletCounter();
  if (fDoubletCounter<fNEMDoublets) SelectPi0();
}

///////////////////
// Expected kaon //
///////////////////
Bool_t TwoPhotonAnalysis::SetKaon() {
  Double_t pkaon = 75;
  Double_t tthx = 0.0012;
  Double_t tthy = 0.;

  // run 3809, 3821
  if (fYear==2015) {
    pkaon = 74.9;
    tthx = 0.00118;
    tthy = 0.;
  }

  // run 5362
  if (fYear==2016) {
    pkaon = 74.9;
    tthx = 0.00122;
    tthy = 0.000026;
  }

  Double_t pz = pkaon/sqrt(1.+tthx*tthx+tthy*tthy);
  Double_t px = pz*tthx;
  Double_t py = pz*tthy;
  fKaonMomentum.SetXYZM(px,py,pz,0.493667);
  return 1;
}

///////////////////////////
// Expected charged pion //
///////////////////////////
Bool_t TwoPhotonAnalysis::SetPiPlus() {
  TVector3 pPic = fPicMomentum.Vect();
  FillHisto("Pic_momentum",fPicMomentum.P());
  if (fPicMomentum.P()<5. || fPicMomentum.P()>80.) return 0;
  Double_t slopeafter = 0;

  TVector3 pos = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,23896.);
  FillHisto("Pic_posAtCHOD",pos.X(),pos.Y());

  // Projection in straw acceptance
  Double_t r2 = 99999999.;
  for (Int_t j=0; j<4; j++) {
    pos = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,fZStation[j]);
    r2 = (pos.X()-fXCenter[j])*(pos.X()-fXCenter[j])+pos.Y()*pos.Y();
    if (r2<=7.5*7.5) return 0;
    if (r2>=100.*100.) return 0;
  }
  //
  //// RICH acceptance
  pos = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,21938.5);
  r2 = (pos.X()-3.4)*(pos.X()-3.4)+pos.Y()*pos.Y();
  if (r2<=9.0*9.0) return 0;
  if (r2>=110.*110.) return 0;
  pos = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,23732.6);
  r2 = (pos.X()-0.2)*(pos.X()-0.2)+pos.Y()*pos.Y();
  if (r2<=9.0*9.0) return 0;
  if (r2>=110.*110.) return 0;
  //
  // Projection in CHOD acceptance
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<=13.*13.) return 0;
  if (r2>=110.*110.) return 0;

  // LKr acceptance
  //pos = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,24109.3);
  //if (!tools->LKrAcceptance(pos.X(),pos.Y(),15.,113.)) return 0;

  // MUV2 acceptance
  pos = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,24443.5);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<=13.*13.) return 0;
  if (r2>=110.*110.) return 0;

  // MUV3 acceptance
  pos = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,24680.0);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<=13.*13.) return 0;
  if (r2>=110.*110.) return 0;

  return 1;
}

////////////////////////////////////////////////
// Look for a CHOD matching the expected pion //
////////////////////////////////////////////////
Bool_t TwoPhotonAnalysis::PiPlusCHODHit() {
  //Double_t slopeafter = 0;
  //TVector3 pPic = fPicMomentum.Vect();
  //TVector3 poschodV = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,23896.);
  //TVector3 poschodH = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,23934.);
  //Double_t xtrack = poschodV.X(); // cm
  //Double_t ytrack = poschodH.Y(); // cm
  //Int_t chodMatch = fCHODAnalysis->ExpectedTrackMatching(fCHODEvent,xtrack*10,ytrack*10,fPi0sTime[0]);
  //if (!chodMatch) return 0;
  return 1;
}

///////////////////////////////////////////////
// Look for a LKr matching the expected pion //
///////////////////////////////////////////////
Bool_t TwoPhotonAnalysis::PiPlusLKrCluster() {
  Double_t slopeafter = 0;
  TVector3 pPic = fPicMomentum.Vect();
  TVector3 poschodV = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,23896.);
  TVector3 poschodH = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,23934.);
  Double_t xtrack = poschodV.X(); // cm
  Double_t ytrack = poschodH.Y(); // cm
  Int_t minid = -1;
  TVector3 posatlkr(xtrack*10.,ytrack*10,0.);

  minid = fLKrAnalysis->TrackClusterMatching(fPi0sTime[0],fPi0sTime[0],posatlkr);
  fLkClusCand = fLKrAnalysis->GetLKrClusterCandidate();
  Bool_t isMatch = 0;
  if (minid>=0) FillHisto("Pic_lkrclus_mindistvst",fLkClusCand->GetDeltaTime(),fLkClusCand->GetDiscriminant());
  if (minid>=0 && fabs(fLkClusCand->GetDeltaTime())<5 && fLkClusCand->GetDiscriminant()<200.) isMatch = 1;
  if (isMatch) return 1;

  minid = fLKrAnalysis->TrackCellMatching(fPi0sTime[0],fPi0sTime[0],posatlkr);
  fLkCellCand = fLKrAnalysis->GetLKrCellCandidate();
  if (minid>=0 && fabs(fLkClusCand->GetDeltaTime())<8) isMatch = 1;
  if (minid>=0) FillHisto("Pic_lkrcell_mindistvst",fLkCellCand->GetDeltaTime(),fLkCellCand->GetDiscriminant());
  if (isMatch) return 1;

  return 0;
}

///////////////////////////////
// Look for a Pion Isolation //
///////////////////////////////
Bool_t TwoPhotonAnalysis::PiPlusIsolation() {
  TVector3 pPic = fPicMomentum.Vect();
  Double_t slopeafter = 0;
  TVector3 poslkr = tools->Propagate(1,&pPic,&fPi0Vertex,&slopeafter,24109.3);
  Bool_t isIsolated = 1;
  for (Int_t jdoublet=0; jdoublet<fNEMDoublets; jdoublet++) {
    if (jdoublet!=fPi0sDoubletID[0]) continue;
    Int_t photonID[2] = {-1,-1};
    for (Int_t j=0; j<2; j++) {
      photonID[j] = fTwoEMClusterID[2*fPi0sDoubletID[0]+j];
      TRecoLKrCandidate *photon = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(photonID[j]);
      Double_t dist = (poslkr.X()-0.1*photon->GetClusterX())*(poslkr.X()-0.1*photon->GetClusterX())+(poslkr.Y()-0.1*photon->GetClusterY())*(poslkr.Y()-0.1*photon->GetClusterY());
      FillHisto("Pic_isolation",sqrt(dist));
      if (dist<15.*15.) isIsolated = 0;
    }
  }
  return isIsolated;
}

///////////////////////////
// Look for hits in LAVs //
///////////////////////////
Bool_t TwoPhotonAnalysis::LAVCandidate() {
  fLAVAnalysis->Clear();
  Int_t nlav = fLAVAnalysis->MakeCandidate(fPi0sTime[0],fLAVEvent);
  if (!nlav) return false;
  return true;
}

Int_t TwoPhotonAnalysis::SelectTrigger(int triggerType, int type, int mask) {
  int bitPhysics = 0;
  int bitControl = 4;
  int bitMinBias = 0;
  int bitPinunu = 1;


  if (type&0x2) return 0; // skip periodics


  // Select trigger
  if ((triggerType==0) && (type>>bitControl)&1) return 1; // control trigger.
  else if (triggerType==1) { // physics minimum bias trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitMinBias)&1) return 1;
  }
  else if (triggerType==2) { // physics pinunu trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitPinunu)&1) return 1;
  }
  else return 0; // trigger not found.
}
