#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "K3piTrackAnalysis.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Geometry.hh"
#include "Event.hh"
#include "Constants.hh"
#include "Persistency.hh"
#include "Parameters.hh"
#include "AnalysisTools.hh"
#include "LAVMatching.hh"
#include "SAVMatching.hh"
#include "Parameters.hh"
//#include "KTAGCandidate.cc"
#include "SpectrometerNewCHODAssociation.hh"
#include "SpectrometerMUV3Association.hh"
#include "RICHParameters.hh"
#include "RICHImprovedRing.hh"
#include <numeric>
using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

K3piTrackAnalysis::K3piTrackAnalysis(Core::BaseAnalysis *ba) : Analyzer(ba, "K3piTrackAnalysis")
{
    //RequestAllRecoTrees();
    RequestL0Data();
    RequestL1Data();

    fGeo = GeometricAcceptance::GetInstance();

    RequestTree("Spectrometer",new TRecoSpectrometerEvent);
    RequestTree("Cedar",new TRecoCedarEvent);
    RequestTree("CHOD",new TRecoCHODEvent);
    RequestTree("NewCHOD",new TRecoNewCHODEvent);
    RequestTree("LKr",new TRecoLKrEvent);
    RequestTree("MUV1",new TRecoMUV1Event);
    RequestTree("MUV2",new TRecoMUV2Event);
    RequestTree("MUV3",new TRecoMUV3Event);
    RequestTree("GigaTracker",new TRecoGigaTrackerEvent);
    RequestTree("LAV",new TRecoLAVEvent);
    RequestTree("SAV",new TRecoSAVEvent);
    RequestTree("IRC",new TRecoIRCEvent);
    RequestTree("SAC",new TRecoSACEvent);
    RequestTree("CHANTI",new TRecoCHANTIEvent);

    fMaxNTracks = 50;
    fMaxNVertices = 50;

    fTool = AnalysisTools::GetInstance();
    fLAVMatch = new LAVMatching();
    fSAVMatch = new SAVMatching();

    //Parameters* para = Parameters::GetInstance();
    //GigaTrackerCandidate* ffs = new GigaTrackerCandidate();
    //RICHCandidate* ff1s = new RICHCandidate();
    fRICHCand = new RICHCandidate();

    fGigaTrackerAnalysis = new GigaTrackerAnalysis(ba);
    fCedarAnal = new CedarAnal(1,ba);

    fLKrAnal = new LKrAnal(4,ba);
    fCHODAnal = new CHODAnal(2,ba);
    fStrawAnalysis = new StrawRapper(2,ba,fCHODAnal,fLKrAnal);

    //fRICHAnal = new RICHAnalysis(0,ba);
    AddParam("BlueTubeFieldCorr",&fBlueFieldCorrection, true);
    AddParam("MaxChi2",    &fMaxChi2,       100.0);
    AddParam("MinZvertex", &fMinZVertex,  50000.0); // [mm]
    AddParam("MaxZvertex", &fMaxZVertex, 180000.0); // [mm]

    fXCenter[0] = 101.2;
    fXCenter[1] = 114.4;
    fXCenter[2] = 92.4;
    fXCenter[3] = 52.8;


}

void K3piTrackAnalysis::InitOutput(){
}

void K3piTrackAnalysis::InitHist(){

    BookHisto(new TH1D("Ntracksall", "Number of tracks;Number of tracks", 20, -0.5, 19.5));
    BookHisto(new TH1D("Ntracksflag", "Number of tracks;Number of tracks", 20, -0.5, 19.5));
    BookHisto(new TH1D("Ntracks", "Number of tracks;Number of tracks", 20, -0.5, 19.5));

    //Two tracks selection
    BookHisto(new TH1F("PTracks","",200,0.,100.));
    BookHisto(new TH1F("PGoodTracks","",200,0.,100.));
    BookHisto(new TH1F("PGoodTracksInAcc","",200,0.,100.));
    BookHisto(new TH1F("PGoodPosTracks","",200,0.,100.));
    BookHisto(new TH1F("PGoodNegTracks","",200,0.,100.));

    TString type[2] = {"K3pi2","KS"};

    BookHisto(new TH1I(Form("%s_charge_sum",type[1].Data()),"",6,-3,3));
    BookHisto(new TH1I(Form("%s_diffcharge",type[1].Data()),"",6,-3,3));

    //KSPlots
    for(int i(0);i<2;i++){
        BookHisto(new TH1F(Form("%sTrack%d_chodchi2",type[1].Data(),i),"",200,0,50));
        BookHisto(new TH2F(Form("%sTrack%d_nochod",type[1].Data(),i),"",300,-1500,1500,300,-1500,1500));
        BookHisto(new TH2F(Form("%sTrack%d_chodchi2_rejected",type[1].Data(),i),"",300,-1500,1500,300,-1500,1500));
        BookHisto(new TH2F(Form("%sTrack%d_dist_vs_mintime",type[1].Data(),i),"",200,-100,100,50,0,250));
        BookHisto(new TH2F(Form("%sTrack%d_dy_vs_dx",type[1].Data(),i),"",200,-500,500,200,-500,500));
        BookHisto(new TH1F(Form("%sTrack%d_chod_ptrack",type[1].Data(),i),"",200,0,100));
    }


    BookHisto(new TH1F(Form("%s_IsCHOD_p",type[1].Data()),"",200,0,100));
    BookHisto(new TH1F(Form("%s_IsCHOD_dt",type[1].Data()),"",800,-100,100));
    BookHisto(new TH1F(Form("%sPl_IsCHOD_p",type[1].Data()),"",200,0,100));
    BookHisto(new TH1F(Form("%sMin_IsCHOD_p",type[1].Data()),"",200,0,100));
    BookHisto(new TH1F(Form("%sPl_dtchodcedar",type[1].Data()),"",1000,-50,50));
    BookHisto(new TH1F(Form("%sMin_dtchodcedar",type[1].Data()),"",1000,-50,50));

    BookHisto(new TH2F(Form("%s_IsCedar_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsCedar_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsCedar_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsCedar_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsCedar_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsCedar_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsCedar_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsCedar_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsCedar_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsCedar_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsCedar_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsCedar_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_IsCedar_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsCedar_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_IsGoodVtx_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodVtx_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodVtx_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_PID_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_PID_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_PID_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_PID_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_PID_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_PID_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_PID_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_PID_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_PID_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_PID_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_PID_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_PID_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_PID_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_PID_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));

    BookHisto(new TH2F(Form("%s_IsKSMass_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsKSMass_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsKSMass_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsKSMass_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsKSMass_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsKSMass_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsKSMass_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));

    BookHisto(new TH2F(Form("%s_IsKSMass_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsKSMass_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH1F(Form("%s_dt12_cedar",type[1].Data()),"",1000,-50,50));
    BookHisto(new TH2F(Form("%s_chanti_dt_nhits_all",type[1].Data()),"",1000,-50, 50,50,0,50));
    BookHisto(new TH2F(Form("%s_chanti_dt_nhits_xy",type[1].Data()),"",1000,-50, 50,50,0,50));
    BookHisto(new TH1F(Form("%s_chanti_dt_closest",type[1].Data()),"",1000,-50, 50));

    BookHisto(new TH2F(Form("%s_final_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_final_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_final_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_final_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_final_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_final_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_final_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_final_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_final_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_final_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_final_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_NoCHANTI_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_NoCHANTI_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_NoCHANTI_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoCHANTI_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoCHANTI_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoCHANTI_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoCHANTI_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_NoCHANTI_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoCHANTI_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_CHANTI_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_CHANTI_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_CHANTI_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_CHANTI_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_CHANTI_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_CHANTI_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_CHANTI_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_CHANTI_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_CHANTI_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_NoSnakes_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_NoSnakes_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_NoSnakes_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoSnakes_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_NoSnakes_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_NoSnakes_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoSnakes_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_NoSnakes_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_NoSnakes_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));

    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiPlus_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_pt2_vs_p",type[1].Data()),"",200,0, 100,40,0,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_kaon_mmiss_vs_p",type[1].Data()),"",200,0,100,500,-0.5,0.5));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_piminus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_piplus_mmiss_vs_p",type[1].Data()),"",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_p_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200,0,100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_minv_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 1000,0,2)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_cda_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, 0, 200)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_xvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_yvtx_vs_zvtx",type[1].Data()), "", 100, fMinZVertex, fMaxZVertex, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_yvtx_vs_xvtx",type[1].Data()), "", 200, -100, 100, 200, -100, 100)); // [mm]
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_min_x_vs_y_straw1",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_IsGoodPiMinus_min_rstraw1_vs_zvtx",type[1].Data()),"",250,50000,200000,120,0,1200));


    //Matching KS selection
    BookHisto(new TH2F(Form("%s_pip_IsLKrCl_rejected",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_pip_IsLKrCell_rejected",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_pip_IsLKrCell_mindist_vs_dtchod",type[1].Data()),"",800,-50,50,1000,0,1000));
    BookHisto(new TH2F(Form("%s_pip_IsLKrCell_dx_vs_dy",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_pip_EoP_vs_P",type[1].Data()),"",200,0,100,120,0,1.2));
    BookHisto(new TH1F(Form("%s_pip_IsMUV3_dt",type[1].Data()),"",800,-50,50));

    BookHisto(new TH2F(Form("%s_pim_IsLKrCl_rejected",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_pim_IsLKrCell_rejected",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_pim_IsLKrCell_mindist_vs_dtchod",type[1].Data()),"",800,-50,50,1000,0,1000));
    BookHisto(new TH2F(Form("%s_pim_IsLKrCell_dx_vs_dy",type[1].Data()),"",240,-1200,1200,240,-1200,1200));
    BookHisto(new TH2F(Form("%s_pim_EoP_vs_P",type[1].Data()),"",200,0,100,120,0,1.2));
    BookHisto(new TH1F(Form("%s_pim_IsMUV3_dt",type[1].Data()),"",800,-50,50));

//K3pi plots
    for(int i(0);i<2;i++){
        BookHisto(new TH1F(Form("%sTrack%d_chodchi2",type[0].Data(),i),"",200,0,50));
        BookHisto(new TH2F(Form("%sTrack%d_nochod",type[0].Data(),i),"",300,-1500,1500,300,-1500,1500));
        BookHisto(new TH2F(Form("%sTrack%d_chodchi2_rejected",type[0].Data(),i),"",300,-1500,1500,300,-1500,1500));
        BookHisto(new TH2F(Form("%sTrack%d_dist_vs_mintime",type[0].Data(),i),"",200,-100,100,50,0,250));
        BookHisto(new TH2F(Form("%sTrack%d_dy_vs_dx",type[0].Data(),i),"",200,-500,500,200,-500,500));
        BookHisto(new TH1F(Form("%sTrack%d_chod_ptrack",type[0].Data(),i),"",200,0,100));
    }
    for(int i(0);i<2;i++){

        BookHisto(new TH1F(Form("Track%d_rich_dtchod",i),"",1600,-100,100));
        BookHisto(new TH1I(Form("Track%d_rich_nhits",i),"",50,0,50));
        BookHisto(new TH2F(Form("Track%d_rich_dxdy",i),"",1200,-600.,600.,1200,-600.,600.));
        BookHisto(new TH2F(Form("Track%d_rich_xydslope",i),"",250,-0.01,0.01,250,-0.01,0.01));
        BookHisto(new TH2F(Form("Track%d_rich_r_vs_p",i),"",200,0.,100.,300,0,300));
        BookHisto(new TH2F(Form("Track%d_rich_mass_vs_p",i),"",200,0.,100.,500,0.,1.));
        BookHisto(new TH2F(Form("Track%d_richp_vs_p",i),"",200,0.,100.,200,0.,100.));
        BookHisto(new TH1F(Form("Track%d_rich_ptrack",i),"",200,0.,100.));
        BookHisto(new TH1F(Form("Track%d_dtrichcedar",i),"",800,-50.,50.));
        BookHisto(new TH1F(Form("Track%d_dtchodcedar",i),"",800,-50.,50.));
        BookHisto(new TH1F(Form("Track%d_dtchodrich",i),"",800,-50.,50.));

    }

    BookHisto(new TH1F(Form("Track%d_dtgtkrich", 0),"dT_{RICH - GTK}; dt_{RICH - GTK} [ns]", 400, -5, 5));
    BookHisto(new TH1F(Form("Track%d_dtgtkcedar", 0),"dT_{Cedar - GTK}; dt_{Cedar - GTK} [ns]", 400, -5, 5));

    BookHisto(new TH1F(Form("Track%d_gtk_P",0), " GTK momentum;P_{K} [MeV/c]",100,65.,85));
    BookHisto(new TH1F(Form("Track%d_gtk_chi2", 0),"#chi^{2} ; #chi^{2}",300,0.,300.));
    BookHisto(new TH2F(Form("Track%d_gtk_pos", 0),"GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));

    //
    BookHisto(new TH1F(Form("Track%d_gtk_dX_12", 0),"; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F(Form("Track%d_gtk_dY_12", 0),"; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F(Form("Track%d_gtk_dX_13", 0),"; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F(Form("Track%d_gtk_dY_13", 0),"; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F(Form("Track%d_gtk_dX_23", 0),"; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F(Form("Track%d_gtk_dY_23", 0),"; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F(Form("Track%d_gtk_thetax",0)," ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
    BookHisto(new TH1F(Form("Track%d_gtk_thetay",0)," ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));

    BookHisto(new TH2F(Form("Track%d_gtk_dt_vs_cda",0), "  cda vs T_{RICH} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    BookHisto(new TH2F(Form("Track%d_gtk_dtcedar_vs_cda",0), "  cda vs T_{Cedar} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));

    BookHisto(new TH2F(Form("Track%d_gtk_dt_vs_cda_after",0), "  cda vs T_{RICH} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    BookHisto(new TH2F(Form("Track%d_gtk_dtcedar_vs_cda_after",0), "  cda vs T_{Cedar} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));


    BookHisto(new TH1F("TwoTracks_nstrawtracks","",10,0,10));
    BookHisto(new TH1F("TwoTracks_mmiss","",375,-0.1,0.14375));
    BookHisto(new TH1F("TwoTracks_mmiss_piminus","",375,-0.1,0.14375));
    BookHisto(new TH1F("TwoTracks_p1","",200,0.,100.));
    BookHisto(new TH1F("TwoTracks_p2","",200,0.,100.));
    BookHisto(new TH1F("TwoTracks_p3","",200,0.,100.));
    BookHisto(new TH1D("TwoTracks_zvtx", "Vertex Z coordinate;Z [mm]", 100, fMinZVertex, fMaxZVertex)); // [m]
    BookHisto(new TH1D("TwoTracks_cda", "cda;cda [mm]", 200, 0, 200)); // [m]
    BookHisto(new TH2D("TwoTracks_PiAtstraw1", "Track (x,y) at Straw chamber 1;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));



    BookHisto(new TH2F("TwoTracks_nosegments_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2D("TwoTracks_nosegments_PiAtstraw1", "Track (x,y) at Straw chamber 1;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2F("TwoTracks_ibvsmmiss_test_3pi","",375,-0.1,0.14375,2000,0,200));

    BookHisto(new TH2F("TwoTracks_aftersegments_mmiss_vs_p","",200,0,100,375,-0.1,0.14375));
    BookHisto(new TH2D("TwoTracks_aftersegments_PiAtstraw1", "Track (x,y) at Straw chamber 1;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));


    //BookHisto(new TH1D("vtxchi2", "Vertex #chi^{2};#chi^{2}", 100,0, fMaxChi2));
    //BookHisto(new TH1D("vtxcharge", "Charge of the vertex;Vtx_{Q}", 8, -4, 4));
    //BookHisto(new TH1D("xvtx", "Vertex X coordinate;Z [mm]", 1000, -500, 500)); // [mm]
    //BookHisto(new TH1D("yvtx", "Vertex Y coordinate;Z [mm]", 1000, -500, 500)); // [mm]
    //BookHisto(new TH1D("zvtx", "Vertex Z coordinate;Z [mm]", 100, fMinZVertex, fMaxZVertex)); // [m]
    //BookHisto(new TH1D("ptot_all", "Momentum of the three track system;P_{3track} [GeV/c]", 100, 50, 100)); // [GeV/c]
    //
    //BookHisto(new TH2D("trackXY_atstraw1", "Track (x,y) at Straw chamber 1;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //BookHisto(new TH2D("trackXY_atstraw2", "Track (x,y) at Straw chamber 2;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //BookHisto(new TH2D("trackXY_atstraw3", "Track (x,y) at Straw chamber 3;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //BookHisto(new TH2D("trackXY_atstraw4", "Track (x,y) at Straw chamber 4;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //BookHisto(new TH2D("trackXY_atCHOD", "Track (x,y) at CHOD;x [mm];y [mm]", 60, -1200, 1200, 60, -1200, 1200));
    //BookHisto(new TH2D("trackXY_atLKr", "Track (x,y) at CHOD;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //
    //BookHisto(new TH1D("richmatch", "", 10, 0, 10));
    //
    //BookHisto(new TH2D("trackXY_atMUV1", "Track (x,y) at MUV1;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //BookHisto(new TH2D("trackXY_atMUV2", "Track (x,y) at MUV2;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //BookHisto(new TH2D("trackXY_atMUV3", "Track (x,y) at MUV3;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    //
    //BookHisto(new TH2D("oddtrackXY_atCHOD", "Track (x,y) at CHOD;x [mm];y [mm]", 60, -1200, 1200, 60, -1200, 1200));
    //
    //BookHisto(new TH1F("dhittime","HitTime; t_{hit} [ns]", 800, -50, 50));
    //BookHisto(new TH1F("hit_dist","Distance to hit;dist [mm]",400, 0, 2000));
    //BookHisto(new TH1F("hit_chi2","CHOD Discriminant ;#chi^{2} ",200, 0, 50));
    //BookHisto(new TH2F("hit_dtime_vs_dist","dt_{Hit} vs Distance to hit; dt_{hit} [ns];dist [mm]", 200, -100, 100, 400, 0, 2000));
    //BookHisto(new TH2F("hit_chi2_vs_dtime","HitTime vs dt; #chi^{2} ;di2st [mm]", 200, 0, 50, 200, -100, 100));
    //BookHisto(new TH2F("hit_chi2_vs_distance"," Discriminant vs Distance to hit; #chi^{2};dist [mm]", 200, 0, 50, 400, 0, 2000));
    //
    //BookHisto(new TH1F("lkrdist","Track distance to cluster in LKr ;dist [mm]",400, 0, 2000));
    //BookHisto(new TH1F("dtlkr","dT_{lkr}; dt_{lkr} [ns]", 800, -50, 50));
    //
    //BookHisto(new TH1D("track_lkrenergy", "Energy of the Track released in lkr;E_{lkr} [GeV/c]", 200,0, 100)); // [GeV/c]
    //BookHisto(new TH1D("track_eop", "E/p;E_{lkr}/p", 120,0, 1.2)); // [GeV/c]
    //BookHisto(new TH1F("eventtime","Average CHOD time of the tracks; T_{Event} [ns]", 800, -50, 50));
    //BookHisto(new TH1D("cedar_nsectors", "Number of sectors in cedar;Nsectors", 10, -0.5, 9.5));
    //BookHisto(new TH1F("dtcedar","dT_{cedar}; dt_{Cedar} [ns]", 800, -50, 50));
    //BookHisto(new TH1F("dtmuv1","dT_{muv1}; dt_{muv1} [ns]", 800, -50, 50));
    //BookHisto(new TH1F("dtmuv2","dT_{muv2}; dt_{muv2} [ns]", 800, -50, 50));
    //BookHisto(new TH1F("dtmuv3","dT_{muv3}; dt_{muv3} [ns]", 800, -50, 50));
    //
    //BookHisto(new TH1F("distmuv1","Distance to closest MUV1 cand;dist [mm]",400, 0, 2000));
    //BookHisto(new TH1F("distmuv2","Distance to closest MUV2 cand;dist [mm]",400, 0, 2000));
    //BookHisto(new TH1F("distmuv3","Distance to closest MUV3 cand;dist [mm]",400, 0, 2000));
    //
    //
    //
    //BookHisto(new TH1D("pttot", "Momentum of the three track system;P_{3track} [GeV/c]", 150, -0.5, 1)); // [GeV/c]
    //
    //BookHisto(new TH1D("mtot", "Mass of the three track system;M_{3track} [GeV/c]", 300, 0.3, 0.6)); // [GeV/c]
    //BookHisto(new TH1D("ptot", "Momentum of the three track system;P_{3track} [GeV/c]", 100, 50, 100)); // [GeV/c]
    //
    //BookHisto(new TH1D("dxdz",       "Kaon dx/dz",  100, -0.0025, 0.0025));
    //BookHisto(new TH1D("dydz",       "Kaon dy/dz",  100, -0.0025, 0.0025));
    //BookHisto(new TH2D("ptot_vs_dydz", "PKaon vs Kaon dy/dz",100,65.,85. , 100, -0.0025, 0.0025));
    //BookHisto(new TH2D("ptot_vs_dxdz", "PKaon vs Kaon dy/dz",100,65.,85. , 100, -0.0025, 0.0025));
    //BookHisto(new TH2D("XYtrim5",    "Kaon (x,y) at z=101.8m;x [mm]; y[mm]", 50, -50, 50, 50, -50, 50));
    //BookHisto(new TH1D("Xtrim5", "Kaon x at z=101.8m;x [mm]", 100, -50, 50));
    //BookHisto(new TH1D("Ytrim5", "Kaon y at z=101.8m;y [mm]", 100, -50, 50));
    //
    //
    //BookHisto(new TH1I("Track_rich_singlering_flag","",2,0,2 ));
    //BookHisto(new TH1I("Track_rich_singlering_dtchod","",1600,-100,100 ));
    //BookHisto(new TH2F("Track_rich_singlering_xydslope","",250,-0.01,0.01,250,-0.01,0.01));
    //BookHisto(new TH1F("Track_rich_singlering_chi2","",300,0,30 ));
    //BookHisto(new TH1F("Track_rich_singlering_nobservedhits","",50,0,50 ));
    //BookHisto(new TH2F("Track_rich_singlering_radius_vs_p","",100,0,100000, 600, 0, 300 ));
    //
    //
    //BookHisto(new TH2F("Track_rich_singlering_min_xydslope","",250,-0.01,0.01,250,-0.01,0.01));
    //BookHisto(new TH1F("Track_rich_singlering_min_chi2","",300,0,30 ));
    //BookHisto(new TH1F("Track_rich_singlering_min_nobservedhits","",50,0,50 ));
    //BookHisto(new TH2F("Track_rich_singlering_min_radius_vs_p","",100,0,100000, 600, 0, 300 ));
    //BookHisto(new TH1I("Track_rich_singlering_min_dtchod","",1600,-100,100 ));
    //BookHisto(new TH2F("Track_rich_singlering_final_radius_vs_p","",100,0,100000, 600, 0, 300 ));
    //
    //BookHisto(new TH2F("Track_richnew_singlering_min_xydslope","",250,-0.01,0.01,250,-0.01,0.01));
    //BookHisto(new TH1F("Track_richnew_singlering_min_chi2","",300,0,30 ));
    //BookHisto(new TH2F("Track_richnew_singlering_min_radius_vs_p","",100,0,100000, 600, 0, 300 ));
    //BookHisto(new TH1I("Track_richnew_singlering_min_dtchod","",1600,-100,100 ));
    //BookHisto(new TH1F("Track_richnew_singlering_min_nobservedhits","",50,0,50 ));
    //BookHisto(new TH2F("Track_richnew_singlering_final_radius_vs_p","",100,0,100000, 600, 0, 300 ));
    //
    //
    //BookHisto(new TH1F("chodcedar_testdt","dT_{Cedar - CHOD}; dt_{Cedar - CHOD} [ns]", 800, -50, 50));
    //BookHisto(new TH1I("gtk_type" , ";GTK type" ,200,0,200));
    //
    //BookHisto(new TH1F("dtgtkchod","dT_{CHOD - GTK}; dt_{CHOD - GTK} [ns]", 400, -5, 5));
    //BookHisto(new TH1F("dtgtkcedar","dT_{cedar - GTK}; dt_{Cedar - GTK} [ns]", 400, -5, 5));
    //
    //
    //BookHisto(new TH1F("gtk_P", " GTK momentum;P_{K} [MeV/c]",100,65.,85));
    //BookHisto(new TH1I("gtk_Nhits", "Number of GTK hits;Nhits" ,100,0,100));
    //BookHisto(new TH1F("gtk_chi2","#chi^{2} ; #chi^{2}",300,0.,300.));
    //BookHisto(new TH1F("gtk_chi2X","#chi^{2}X ; #chi^{2}",300,0.,300.));
    //BookHisto(new TH1F("gtk_chi2Y","#chi^{2}Y ; #chi^{2}",300,0.,300.));
    //BookHisto(new TH1F("gtk_chi2T","#chi^{2}T ; #chi^{2}",300,0.,300.));
    //BookHisto(new TH2F("gtk_pos","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));
    //
    //
    //BookHisto(new TH1F("gtk_dX_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    //BookHisto(new TH1F("gtk_dY_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    //BookHisto(new TH1F("gtk_dX_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    //BookHisto(new TH1F("gtk_dY_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    //BookHisto(new TH1F("gtk_dX_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    //BookHisto(new TH1F("gtk_dY_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    //BookHisto(new TH1F("gtk_thetax", " ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
    //BookHisto(new TH1F("gtk_thetay", " ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));
    //
    //BookHisto(new TH1F("gtk_discriminant","GTK Discriminant ;#chi^{2} ",200, 0, 1));
    //BookHisto(new TH1F("gtk_discriminant_cedar","GTK Discriminant ;#chi^{2} ",200, 0, 1));
    //BookHisto(new TH1F("gtk_discriminant_after","GTK Discriminant ;#chi^{2} ",200, 0, 1));
    //BookHisto(new TH2F("gtk_dt_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH2F("gtk_dtcedar_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH1F("gtk_kaondiscriminant","GTK Kaon Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH2F("gtk_dt_vs_cda_after", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH2F("gtk_dtcedar_vs_cda_after", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH1F("gtk_kaondiscriminant_after","GTK Kaon Discriminant ;#chi^{2} ",250, 0, 50));
    //
    //BookHisto(new TH2F("gtk_dt_vs_cda_final", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH2F("gtk_dtcedar_vs_cda_final", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH1F("gtk_kaondiscriminant_final","GTK Kaon Discriminant ;#chi^{2} ",250, 0, 50));
    //
    //BookHisto(new TH1F("Discr_kaon_all","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH1F("Discr_kaon_true","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH1F("RICH_discr_kaon_true","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH1F("RICH_chi2_true","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH2F("RICH_dt_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH2F("RICH_dtcedar_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //
    //BookHisto(new TH1F("Cedar_discr_kaon_true","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH1F("Cedar_chi2_true","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH2F("Cedar_dt_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //BookHisto(new TH2F("Cedar_dtcedar_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -5, 5,200, 0, 200));
    //
    //BookHisto(new TH1F("Both_discr_kaon_true","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //BookHisto(new TH1F("Both_chi2_true","GTK Discriminant ;#chi^{2} ",250, 0, 50));
    //
    //BookHisto(new TH1F("dtgtkcedar_after","dT_{cedar - GTK}; dt_{Cedar - GTK} [ns]", 800, -50, 50));
    //BookHisto(new TH2F("gtk_Nhits_vs_P", "Number of GTK hits vs P;Nhits;P" ,30,0,30,100,65.,85));
    //BookHisto(new TH1F("gtk_Ncand", "Number of GTK candidates;Ncand" ,20,0,20));
    //
    //BookHisto(new TH1F("ptot_aftergtk", " GTK momentum;P_{K} [MeV/c]",100,65.,85));
    //
    //BookHisto(new TH1F("dtpikaon","dT_{RICH #pi 1 - GTK};dT_{RICH #pi 1 - GTK}", 320, -20, 20));
    //BookHisto(new TH1F("discriminant","Discriminant for #pi", 100, 0, 50));
    //BookHisto(new TH1F("discriminant_best","Discriminant for #pi", 100, 0, 50));
    //BookHisto(new TH1F("discriminant_difference","Discriminant for #pi", 100, 0, 20));
    //BookHisto(new TH2F("pk_vs_dpk", "Pk from K3piTrackAnalysis vs Kaon momentum difference using the GTK - same using K3piTrackAnalysis ;dP_{K};P_{K} [Gev/c]",100,65,85,200, -10, 10));
    //BookHisto(new TH2F("pk_vs_dx", "  ;P_{K};dx_{GTK - K3piTrackAnalysis}",100,65,85,120,-20, 20));
    //BookHisto(new TH2F("pk_vs_dy", "  ;P_{K};dy_{GTK - K3piTrackAnalysis}",100,65,85,120,-20, 20));
    //BookHisto(new TH2F("x_vs_kthx", ";x^{K_{3#pi}}_{gtk3};K_{3#pi} #theta^{K}_{x}",240,-40,40,100,0.0005,0.0025));
    //BookHisto(new TH2F("gtk_x_vs_kthx", ";x^{gtk}_{gtk3};gtk #theta^{K}_{x}",240,-40,40,100,0.0005,0.0025));
    //BookHisto(new TH2F("y_vs_kthy", ";y^{K_{3#pi}}_{gtk3};K_{3#pi} #theta^{K}_{y}",120,-20,20,500,-0.01,0.01));
    //BookHisto(new TH2F("gtk_y_vs_kthy", ";y^{gtk}_{gtk3};gtk #theta^{K}_{y}",120,-20,20,500,-0.01,0.01));


}

void K3piTrackAnalysis::DefineMCSimple(){

}
void K3piTrackAnalysis::ClearTracks(){
    for (int j(0);j<10;j++){
        fUpstreamTrack[j].Clear();
        fDownstreamTrack[j].Clear();
    }
    fCedarCandidate[0].Clear();
    fCedarCandidate[1].Clear();

}
void K3piTrackAnalysis::StartOfRunUser(){

}

void K3piTrackAnalysis::StartOfBurstUser(){

    fYear=2016;
    EventHeader* header = GetEventHeader();
    fRun = header->GetRunID();
    fElectronRingRadius = RICHParameters::GetInstance()->GetElectronRingRadius(fRun,header->GetBurstTime());
    fElectronRingNhits = RICHParameters::GetInstance()->GetElectronRingNHits(fRun,header->GetBurstTime());

    fIsFilter = GetTree("Reco")->FindBranch("FilterWord") ? true : false;

    fStrawAnalysis->StartBurst();
}

void K3piTrackAnalysis::Process(int iEvent){

    OutputState state;
    //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}

    fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
    fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
    fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
    fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
    if(!fIsFilter) fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",state);
    if(fIsFilter) fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
    fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
    fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
    fIRCEvent = (TRecoIRCEvent*)GetEvent("IRC");
    fSACEvent = (TRecoSACEvent*)GetEvent("SAC");
    fSAVEvent = (TRecoSAVEvent*)GetEvent("SAV");
    fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");
    fCHANTIEvent      = (TRecoCHANTIEvent*)GetEvent("CHANTI");
    fCHODEvent        = (TRecoCHODEvent*)GetEvent("CHOD");
    fNewCHODEvent     = (TRecoNewCHODEvent*)GetEvent("NewCHOD");
    fCedarEvent       = (TRecoCedarEvent*)GetEvent("Cedar");
    fStrawCandidate   = (StrawCandidate*)GetOutput("TrackProcessor.StrawCandidate");
    fRICHCandidate    = (RICHCandidate*)GetOutput("TrackProcessor.RICHCandidate");
    fCHODCandidate    = (CHODCandidate*)GetOutput("TrackProcessor.CHODCandidate");
    fNewCHODCandidate = (NewCHODCandidate*)GetOutput("TrackProcessor.NewCHODCandidate");
    fLKrClusCandidate = (LKrCandidate*)GetOutput("TrackProcessor.LKrCandidate");
    fLKrCellCandidate = (LKrCandidate*)GetOutput("TrackProcessor.LKrCellCandidate");
    fMUV1Candidate    = (MUVCandidate*)GetOutput("TrackProcessor.MUV1Candidate");
    fMUV2Candidate    = (MUVCandidate*)GetOutput("TrackProcessor.MUV2Candidate");
    //fMUV3Candidate    = *(std::vector<SpectrometerMUV3AssociationOutput>*)GetOutput("SpectrometerMUV3Association.Output");
    fIsK3pi = (Bool_t)GetOutput("TrackProcessor.IsK3pi");
    //fMUV3TightCandidate = (MUV3Candidate*)GetOutput("TrackProcessor.MUV3TightCandidate");
    //fMUV3LooseCandidate = (MUV3Candidate*)GetOutput("TrackProcessor.MUV3LooseCandidate");
    if(!fIsK3pi) return;
    if (fGigaTrackerEvent->GetErrorMask()) return;
    if (fCedarEvent->GetNCandidates() == 0) return;
    if (fCedarEvent->GetNCandidates()> fMaxNTracks) return;
    fGigaTrackerAnalysis->Clear();
    fStrawAnalysis->Clear();

    fIsMC = GetWithMC();
    fVertexContainer.clear();
    fNTracks= fSpectrometerEvent->GetNCandidates();
    FillHisto("Ntracksall", fNTracks);

    //Save nominal kaon 4 momentum
    NominalKaon();

    Int_t  L0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
    Int_t  L0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();
    Bool_t PhysicsData   = L0DataType    & 0x1;
    Bool_t TriggerOK     = L0TriggerWord & 0xFF;

    fControlTrigger = PhysicsData && TriggerOK;
    fBurstID       = GetWithMC() ? 0 : GetEventHeader()->GetBurstID();
    fBurstTime     = GetWithMC() ? 0 : GetEventHeader()->GetBurstTime();

    Int_t ngoodtracks = 0;
    for(int iC(0);iC<fNTracks;iC++){

        TRecoSpectrometerCandidate* Scand = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(iC);
        Double_t ptrack   = Scand->GetMomentum()*fmevtogev;
        //double pattreco = Scand->GetMomentumBeforeFit()*fmevtogev;
        //double chi2 = Scand->GetChi2();
        //double ttime = Scand->GetTime();
        int charge = Scand->GetCharge();
        //int nChambers = Scand->GetNChambers();

        FillHisto("PTracks", ptrack);

        if(!fStrawCandidate[iC].GetGoodTrack()) continue;
        FillHisto("PGoodTracks", ptrack);

        if (!fGeo->InAcceptance(Scand, kSpectrometer, 0)) continue;
        if (!fGeo->InAcceptance(Scand, kSpectrometer, 1)) continue;
        if (!fGeo->InAcceptance(Scand, kSpectrometer, 2)) continue;
        if (!fGeo->InAcceptance(Scand, kSpectrometer, 3)) continue;
        if (!fGeo->InAcceptance(Scand, kNewCHOD))         continue;
        if (!fGeo->InAcceptance(Scand, kCHOD))         continue;
        //if (!fGeo->InAcceptance(Scand, kRICH))         continue;
        if (!fGeo->InAcceptance(Scand, kLKr))         continue;

        FillHisto("PGoodTracksInAcc", ptrack);
        if(charge==1) FillHisto("PGoodPosTracks", ptrack);
        if(charge==-1) FillHisto("PGoodNegTracks", ptrack);

        ngoodtracks++;
    }

    ClearTracks();
    fSegmentIB = 99999.;
    //Check if it is a good condition~!
    if(ngoodtracks < 2) return;
    FillHisto("Ntracks", fSpectrometerEvent->GetNCandidates());

    Bool_t isGoodTwoTrack   = false;
    Bool_t isGoodThreeTrack = false;
    Bool_t isGoodKS = false;

    //Select cedar matching
    fCedarAnal->SetEvent(fCedarEvent);

    if(ngoodtracks==2 && fNTracks==2) isGoodKS  = SelectKS();
    if(ngoodtracks==2 && fNTracks==2) isGoodTwoTrack  = SelectTwoTrackEvent();
    if(ngoodtracks==3 && fNTracks==3) isGoodThreeTrack= SelectThreeTrackEvent();


}


Bool_t K3piTrackAnalysis::SelectKS(){

    if(fNTracks !=2) return false;

    fKSSpecCand[0] = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(0);
    fKSSpecCand[1] = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(1);
    std::pair<TLorentzVector,TLorentzVector> KSpions = fTool->GetTwoPiPlusMomentum(fKSSpecCand[0],fKSSpecCand[1]);
    int q1 = fKSSpecCand[0]->GetCharge();
    int q2 = fKSSpecCand[1]->GetCharge();
    int ipl= -1;
    int imin= -1;
    FillHisto("KS_charge_sum",q1+q2);
    if(q1==q2) return false;
    FillHisto("KS_diffcharge",q1+q2);

    //Select two tracks with chod matching
    Int_t good1 = GoodCHOD(2,0,2);
    Int_t good2 = GoodCHOD(2,1,2);

    if(good1==1) FillHisto("KSTrack0_chod_ptrack",KSpions.first.P());
    if(good2==1) FillHisto("KSTrack1_chod_ptrack",KSpions.second.P());
    if(good1!=1 || good2!=1 ) return false;

    if(q1==1 && q2==-1){
        ipl=0;
        imin = 1;
        fKSPiPlusMom.SetXYZM(KSpions.first.Px(), KSpions.first.Py(), KSpions.first.Pz(),0.13957);
        fKSPiMinusMom.SetXYZM(KSpions.second.Px(), KSpions.second.Py(), KSpions.second.Pz(),0.13957);
    }
    if(q1==-1 && q2==1){
        ipl=1;
        imin = 0;
        fKSPiMinusMom.SetXYZM(KSpions.first.Px(), KSpions.first.Py(), KSpions.first.Pz(),0.13957);
        fKSPiPlusMom.SetXYZM(KSpions.second.Px(), KSpions.second.Py(), KSpions.second.Pz(),0.13957);
    }

    fXAtStraw1=fYAtStraw1=fRAtStraw1=0;
    fXAtStraw1_Min=fYAtStraw1_Min=fRAtStraw1_Min=0;

    fXAtStraw1 = fKSSpecCand[ipl]->xAt(zSTRAW_station[0]);
    fYAtStraw1 = fKSSpecCand[ipl]->yAt(zSTRAW_station[0]);
    fRAtStraw1 = TMath::Sqrt((fXAtStraw1 - xSTRAW_station[0])*(fXAtStraw1 - xSTRAW_station[0])+fYAtStraw1*fYAtStraw1);

    fXAtStraw1_Min = fKSSpecCand[imin]->xAt(zSTRAW_station[0]);
    fYAtStraw1_Min = fKSSpecCand[imin]->yAt(zSTRAW_station[0]);
    fRAtStraw1_Min = TMath::Sqrt((fXAtStraw1_Min - xSTRAW_station[0])*(fXAtStraw1_Min - xSTRAW_station[0])+fYAtStraw1_Min*fYAtStraw1_Min);

    fKS4Mom = fKSPiMinusMom + fKSPiPlusMom;
    fKSMMiss = (fKaonNominalMomentum - fKS4Mom).M2();
    double min_mm2 = (fKaonNominalMomentum - fKSPiMinusMom).M2();
    double plus_mm2 = (fKaonNominalMomentum - fKSPiPlusMom).M2();
    float choddt = (fCHODCandidate[ipl].GetDeltaTime()+fKSSpecCand[ipl]->GetTime()) - (fCHODCandidate[imin].GetDeltaTime() + fKSSpecCand[imin]->GetTime() );
    FillHisto("KS_IsCHOD_p",fKS4Mom.P());
    FillHisto("KS_IsCHOD_dt",choddt);
    FillHisto("KSMin_IsCHOD_p",fKSPiMinusMom.P());
    FillHisto("KSPl_IsCHOD_p",fKSPiPlusMom.P());

    if(fabs(choddt) > 2) return false;

    fCedarAnal->Clear(); // Clear the candidate in the CedarAnal

    Int_t iscedarpl = fCedarAnal->TrackMatchingSimple(fCHODCandidate[ipl].GetDeltaTime()+fKSSpecCand[ipl]->GetTime(),0,fIsMC,2);
    if(iscedarpl > -1) fKSCedarCandidate[ipl] = *fCedarAnal->GetKTAGCandidate();

    fCedarAnal->Clear(); // Clear the candidate in the CedarAnal

    Int_t iscedarmin = fCedarAnal->TrackMatchingSimple(fCHODCandidate[imin].GetDeltaTime()+fKSSpecCand[imin]->GetTime(),1,fIsMC,2);

    if(iscedarmin > -1) fKSCedarCandidate[imin] = *fCedarAnal->GetKTAGCandidate();
    if(iscedarpl < 0 || iscedarmin < 0) return false;

    TVector3 pos[2];

    pos[0]=fKSSpecCand[0]->GetPositionBeforeMagnet();
    pos[1]=fKSSpecCand[1]->GetPositionBeforeMagnet();

    fKScda= 999999;
    TLorentzVector pforvtx[2];
    pforvtx[0]=KSpions.first;
    pforvtx[1]=KSpions.second;
    fKSTwoTrackVertex = fTool->MultiTrackVertexSimple(2,pforvtx,pos,&fKScda);

    // t_chod  - t_cedar
    double dtchcedpl = (fCHODCandidate[ipl].GetDeltaTime()+fKSSpecCand[ipl]->GetTime())-fKSCedarCandidate[ipl].GetTime();
    double dtchcedmin= (fCHODCandidate[imin] .GetDeltaTime()+fKSSpecCand[imin] ->GetTime())-fKSCedarCandidate[imin].GetTime();


    FillHisto("KSPl_dtchodcedar",dtchcedpl);
    FillHisto("KSMin_dtchodcedar",dtchcedmin);
    FillHisto("KS_IsCedar_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_IsCedar_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_IsCedar_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_IsCedar_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_IsCedar_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_IsCedar_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_IsCedar_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_IsCedar_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_IsCedar_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsCedar_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsCedar_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_IsCedar_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_IsCedar_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_IsCedar_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    if(fKSPiPlusMom.P() < 10 || fKSPiPlusMom.P() > 90 ) return false;
    if(fKSPiMinusMom.P() < 10 || fKSPiMinusMom.P() > 90 ) return false;
    if(fKSTwoTrackVertex.Z() < 103000 || fKSTwoTrackVertex.Z() > 130000) return false;
    if(fKScda > 10) return false;

    FillHisto("KS_IsGoodVtx_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_IsGoodVtx_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_IsGoodVtx_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_IsGoodVtx_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_IsGoodVtx_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_IsGoodVtx_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_IsGoodVtx_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_IsGoodVtx_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_IsGoodVtx_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsGoodVtx_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsGoodVtx_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_IsGoodVtx_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_IsGoodVtx_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_IsGoodVtx_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    Int_t PiPlusInLKr   = GoodLKr(2,ipl,"pip",2);
    Int_t PiMinusInLKr  = GoodLKr(2,imin,"pim",2);
    Int_t PiPlusInMUV3  = GoodMUV3("pip",ipl,2);
    Int_t PiMinusInMUV3 = GoodMUV3("pim",imin,2);

    Int_t PiPlusStatus  = ParticleID("pip",ipl);
    Int_t PiMinusStatus = ParticleID("pim",imin);

    //std::cout << " ------------------" << std::endl;
    //std::cout << "pi + : "<< PiPlusStatus << "in lkr =" << PiPlusInLKr << " in MUV3 = " << PiPlusInMUV3<< std::endl;
    //std::cout << "pi - : "<< PiMinusStatus << "in lkr =" << PiMinusInLKr << " in MUV3 = " << PiMinusInMUV3<< std::endl;
    if(PiPlusInMUV3==1 || PiMinusInMUV3==1) return false;
    if(PiPlusInLKr==0 || PiMinusInLKr==0) return false;
    if(PiPlusStatus==0 || PiMinusStatus==0) return false;

    FillHisto("KS_PID_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_PID_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_PID_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_PID_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_PID_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_PID_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_PID_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_PID_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_PID_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_PID_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_PID_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_PID_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_PID_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_PID_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    if(fKSMMiss > 0.005) return false;
    if(fKS4Mom.P() > 76 ) return false;
    if( fabs(fKS4Mom.M() - 0.497611) > 0.0105) return false;

    FillHisto("KS_IsKSMass_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_IsKSMass_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_IsKSMass_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_IsKSMass_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_IsKSMass_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_IsKSMass_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_IsKSMass_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_IsKSMass_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_IsKSMass_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsKSMass_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_IsKSMass_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_IsKSMass_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_IsKSMass_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_IsKSMass_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    FillHisto("KS_dt12_cedar",fKSCedarCandidate[ipl].GetTime() - fKSCedarCandidate[imin].GetTime() );
    double ktagtime = fKSCedarCandidate[ipl].GetTime();

    std::map<double, std::pair<double,int>> dtktag;
    if(plus_mm2 > 0.068 || plus_mm2 < -0.01) return false;
    if(min_mm2 > 0.068 || min_mm2 < -0.01) return false;
    if(fKSMMiss < -0.05) return false;

    FillHisto("KS_final_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_final_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_final_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_final_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_final_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_final_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_final_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_final_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_final_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_final_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_final_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_final_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_final_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_final_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    for (int i(0); i<fCHANTIEvent->GetNCandidates();i++){
        TRecoCHANTICandidate *cand = (TRecoCHANTICandidate *)fCHANTIEvent->GetCandidate(i);
        Double_t dt_ktg = cand->GetTime()-ktagtime;
        FillHisto("KS_chanti_dt_nhits_all",dt_ktg,cand->GetNHits());
        if (!cand->GetXYMult()) continue; // At least a XY coincidence
        FillHisto("KS_chanti_dt_nhits_xy",dt_ktg,cand->GetNHits());

        dtktag.insert(std::make_pair(fabs(dt_ktg),std::pair<double,int>(cand->GetTime(),i)));

    }

    bool isCHANTI = false;
    if(dtktag.size()>0){

        double iminktag  = dtktag.begin()->second.second;
        double mindtktag = dtktag.begin()->first;
        if(mindtktag < 3){
            FillHisto("KS_chanti_dt_closest",mindtktag);
            isCHANTI= true;
        }
    }

    if(isCHANTI){

        FillHisto("KS_CHANTI_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
        FillHisto("KS_CHANTI_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
        FillHisto("KS_CHANTI_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
        FillHisto("KS_CHANTI_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
        FillHisto("KS_CHANTI_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
        FillHisto("KS_CHANTI_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
        FillHisto("KS_CHANTI_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
        FillHisto("KS_CHANTI_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
        FillHisto("KS_CHANTI_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
        FillHisto("KS_CHANTI_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
        FillHisto("KS_CHANTI_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("KS_CHANTI_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
        FillHisto("KS_CHANTI_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
        FillHisto("KS_CHANTI_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

        return false;
    }

    FillHisto("KS_NoCHANTI_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_NoCHANTI_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_NoCHANTI_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_NoCHANTI_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_NoCHANTI_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_NoCHANTI_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_NoCHANTI_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_NoCHANTI_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_NoCHANTI_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoCHANTI_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoCHANTI_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_NoCHANTI_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_NoCHANTI_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_NoCHANTI_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    //Anti-snakes zvtx vs fRAtStraw1 cut
    double b1 = -0.004;
    double a1 = 315-b1*105000.;
    double r115 = a1+b1*115000.;
    double b2 = -(900-r115)/10000.;
    double a2 = 900-b2*105000.;
    double b3 = -0.00983333333;
    double a3 = 780-b3*105000.;
    double cut1 = a1+b1*fKSTwoTrackVertex.Z();
    double cut2 = a2+b2*fKSTwoTrackVertex.Z();
    double cut3 = a3+b3*fKSTwoTrackVertex.Z();
    bool isSignal = fRAtStraw1>cut1 && fRAtStraw1>cut2 && fRAtStraw1<cut3 && fKSTwoTrackVertex.Z()<165000.;
    if(!isSignal) return false;

    FillHisto("KS_NoSnakes_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
    FillHisto("KS_NoSnakes_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
    FillHisto("KS_NoSnakes_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
    FillHisto("KS_NoSnakes_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
    FillHisto("KS_NoSnakes_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
    FillHisto("KS_NoSnakes_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
    FillHisto("KS_NoSnakes_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
    FillHisto("KS_NoSnakes_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
    FillHisto("KS_NoSnakes_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoSnakes_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
    FillHisto("KS_NoSnakes_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
    FillHisto("KS_NoSnakes_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
    FillHisto("KS_NoSnakes_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
    FillHisto("KS_NoSnakes_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    if(fKSPiMinusMom.P() > 15 && fKSPiMinusMom.P() < 35){

        FillHisto("KS_IsGoodPiMinus_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
        FillHisto("KS_IsGoodPiMinus_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
        FillHisto("KS_IsGoodPiMinus_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
        FillHisto("KS_IsGoodPiMinus_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
        FillHisto("KS_IsGoodPiMinus_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
        FillHisto("KS_IsGoodPiMinus_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
        FillHisto("KS_IsGoodPiMinus_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
        FillHisto("KS_IsGoodPiMinus_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
        FillHisto("KS_IsGoodPiMinus_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiMinus_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiMinus_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("KS_IsGoodPiMinus_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
        FillHisto("KS_IsGoodPiMinus_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
        FillHisto("KS_IsGoodPiMinus_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    }

    if(fKSPiPlusMom.P() > 15 && fKSPiPlusMom.P() < 35){

        FillHisto("KS_IsGoodPiPlus_pt2_vs_p",fKS4Mom.P(), fKS4Mom.Perp2());
        FillHisto("KS_IsGoodPiPlus_kaon_mmiss_vs_p",fKS4Mom.P(),fKSMMiss);
        FillHisto("KS_IsGoodPiPlus_piminus_mmiss_vs_p",fKSPiMinusMom.P(),min_mm2);
        FillHisto("KS_IsGoodPiPlus_piplus_mmiss_vs_p",fKSPiPlusMom.P(),plus_mm2);
        FillHisto("KS_IsGoodPiPlus_p_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.P());
        FillHisto("KS_IsGoodPiPlus_minv_vs_zvtx",fKSTwoTrackVertex.Z(), fKS4Mom.M());
        FillHisto("KS_IsGoodPiPlus_cda_vs_zvtx",fKSTwoTrackVertex.Z(), fKScda);
        FillHisto("KS_IsGoodPiPlus_xvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.X());
        FillHisto("KS_IsGoodPiPlus_yvtx_vs_xvtx",fKSTwoTrackVertex.X(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiPlus_yvtx_vs_zvtx",fKSTwoTrackVertex.Z(), fKSTwoTrackVertex.Y());
        FillHisto("KS_IsGoodPiPlus_x_vs_y_straw1",fXAtStraw1,fYAtStraw1);
        FillHisto("KS_IsGoodPiPlus_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1);
        FillHisto("KS_IsGoodPiPlus_min_x_vs_y_straw1",fXAtStraw1_Min,fYAtStraw1_Min);
        FillHisto("KS_IsGoodPiPlus_min_rstraw1_vs_zvtx",fKSTwoTrackVertex.Z(),fRAtStraw1_Min);

    }

    return 1;
}
Bool_t K3piTrackAnalysis::SelectTwoTrackEvent(){

    TLorentzVector k4momgev;
    Double_t getvpk = 74.9;
    Double_t getvnorm = 1 / (1 + 0.00122*0.00122 + 0.000026*0.000026);
    Double_t getvpx = getvpk*0.00122*getvnorm;
    Double_t getvpy = getvpk*0.000026*getvnorm;
    Double_t getvpz = getvpk*getvnorm;

    fSPosCand[0] = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(0);
    fSPosCand[1] = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(1);

    double Ptrack1 = fSPosCand[0]->GetMomentum()*0.001;
    double Ptrack2 = fSPosCand[1]->GetMomentum()*0.001;
    k4momgev.SetXYZM(getvpx,getvpy,getvpz,0.493667);

    //Select two tracks with chod matching
    Int_t good1 = GoodCHOD(2,0,1);
    Int_t good2 = GoodCHOD(2,1,1);
    if(good1==1) FillHisto("K3pi2Track0_chod_ptrack",Ptrack1);
    if(good2==1) FillHisto("K3pi2Track1_chod_ptrack",Ptrack2);
    if(good1!=1 || good2!=1 ) return false;

    //Select two tracks with rich matching
    Int_t rich1 = GoodRICH(2,0);
    Int_t rich2 = GoodRICH(2,1);

    if(rich1==1) FillHisto("Track0_rich_ptrack",Ptrack1);
    if(rich2==1) FillHisto("Track1_rich_ptrack",Ptrack2);
    if(rich1!=1 || rich2!=1 ) return false;


    fCedarAnal->Clear(); // Clear the candidate in the CedarAnal

    Bool_t iscedar1 = (Bool_t)fCedarAnal->TrackMatchingSimple(fRICHCandidate[0].GetSingleRingTime(),0,fIsMC,1);
    if(iscedar1) fCedarCandidate[0] = *fCedarAnal->GetKTAGCandidate();

    fCedarAnal->Clear(); // Clear the candidate in the CedarAnal
    Bool_t iscedar2 = (Bool_t)fCedarAnal->TrackMatchingSimple(fRICHCandidate[1].GetSingleRingTime(),1,fIsMC,1);
    if(iscedar2) fCedarCandidate[1] = *fCedarAnal->GetKTAGCandidate();
    if(!iscedar1 || !iscedar2) return false;


    // t_rich  - t_cedar
    double dtrc1 = fRICHCandidate[0].GetSingleRingTime()-fCedarCandidate[0].GetTime();
    double dtrc2 = fRICHCandidate[1].GetSingleRingTime()-fCedarCandidate[1].GetTime();


    // t_chod  - t_cedar
    double dtchced1 = (fCHODCandidate[0].GetDeltaTime()+fSPosCand[0]->GetTime())-fCedarCandidate[0].GetTime();
    double dtchced2 = (fCHODCandidate[1] .GetDeltaTime()+fSPosCand[1] ->GetTime())-fCedarCandidate[1].GetTime();

    // t_chod  - t_rich
    double dtchr1 = (fCHODCandidate[0].GetDeltaTime()+fSPosCand[0]->GetTime())-fRICHCandidate[0].GetSingleRingTime();
    double dtchr2 = (fCHODCandidate[1] .GetDeltaTime()+fSPosCand[1] ->GetTime())-fRICHCandidate[1].GetSingleRingTime();

    FillHisto("Track0_dtrichcedar",dtrc1);
    FillHisto("Track1_dtrichcedar",dtrc2);

    FillHisto("Track0_dtchodcedar",dtchced1);
    FillHisto("Track1_dtchodcedar",dtchced2);

    FillHisto("Track0_dtchodrich",dtchr1);
    FillHisto("Track1_dtchodrich",dtchr2);

    //Select gtk matching using rich
    Int_t isGTK = GoodGTK(2,0);

    if(isGTK!=1) return false;

    std::pair<TLorentzVector,TLorentzVector> p = fTool->GetTwoPiPlusMomentum(fSPosCand[0],fSPosCand[1]);

    TVector3 pos[2];

    pos[0]=fSPosCand[0]->GetPositionBeforeMagnet();
    pos[1]=fSPosCand[1]->GetPositionBeforeMagnet();

    //Pi minus using the gtk matched to the first pi+
    TLorentzVector p3 = fUpstreamTrack[0].GetMomentum() - p.first - p.second;

    //Vector of the missing mass
    TLorentzVector pmiss = fUpstreamTrack[0].GetMomentum() - p.first;

    TVector3 thpi3(p3.X()/p3.Z(),p3.Y()/p3.Z(),1);

    FillHisto("TwoTracks_nstrawtracks",fNTracks);
    FillHisto("TwoTracks_mmiss",pmiss.M2());
    FillHisto("TwoTracks_mmiss_piminus",p3.M2());

    if(p3.M2() < 0.013 || p3.M2() > 0.023) return false;

    FillHisto("TwoTracks_p1",p.first.P());
    FillHisto("TwoTracks_p2",p.second.P());
    FillHisto("TwoTracks_p3",p3.P());

    Double_t cda= 999999;
    TLorentzVector pforvtx[2];
    pforvtx[0]=p.first;
    pforvtx[1]=p.second;
    fTwoTrackVertex = fTool->MultiTrackVertexSimple(2,pforvtx,pos,&cda);

    FillHisto("TwoTracks_zvtx",fTwoTrackVertex.Z());
    FillHisto("TwoTracks_cda",cda);
    TVector3 pi1_atSTRAW1 = fTool->GetZ(thpi3,fTwoTrackVertex,Constants::zSTRAW_station[0]);

    if(cda > 7) return false;
    if(fTwoTrackVertex.Z() < 110000 || fTwoTrackVertex.Z() > 165000) return false;

    //Reconstruct segments
    fStrawAnalysis->AnalyzeHits(fSpectrometerEvent,0,99999999);
    fSegmentIB = fStrawAnalysis->ReconstructSegments(&fTwoTrackVertex,fUpstreamTrack[0].GetTime(),-1);
    bool isSegments = fSegmentIB<0.5;


    FillHisto("TwoTracks_PiAtstraw1",pi1_atSTRAW1.X(),pi1_atSTRAW1.Y());
    if(!InStraw1Acceptance(pi1_atSTRAW1)) return false;

    FillHisto("TwoTracks_nosegments_PiAtstraw1",pi1_atSTRAW1.X(),pi1_atSTRAW1.Y());

    FillHisto("TwoTracks_nosegments_mmiss_vs_p",p.first.P(), pmiss.M2());

    FillHisto("TwoTracks_ibvsmmiss_test_3pi",pmiss.M2(),fSegmentIB);

    if(isSegments) return false;

    FillHisto("TwoTracks_aftersegments_PiAtstraw1",pi1_atSTRAW1.X(),pi1_atSTRAW1.Y());
    FillHisto("TwoTracks_aftersegments_mmiss_vs_p",p.first.P(),pmiss.M2());

    return true;
}

Bool_t K3piTrackAnalysis::InStraw1Acceptance(TVector3 pos){

    Bool_t accstraw1 = true;
    double r2;

    // Straw acceptance (very rough)
    r2 = (pos.X()-fXCenter[0])*(pos.X()-fXCenter[0])+pos.Y()*pos.Y();
    if (r2<75.*75. ||r2>1000*1000) accstraw1 = false;

    return accstraw1;
}
Int_t K3piTrackAnalysis::GoodCHOD(Int_t ntracks, Int_t iC, Int_t flag){

    TRecoSpectrometerCandidate* track;

    TString selection;
    if(flag==1) {
        track = fSPosCand[iC];
        selection="K3pi2";

    }
    if(flag==2){
        track = fKSSpecCand[iC];
        selection="KS";
    }


    TVector3 poschod = fTool->GetPositionAtZ(track,Constants::zCHOD);
    if(!fCHODCandidate[iC].GetIsCHODCandidate()){
        if(ntracks==2)
            FillHisto(Form("%sTrack%d_nochod",selection.Data(),iC),poschod.X(),poschod.Y());

        return 0;
    }
    FillHisto(Form("%sTrack%d_chodchi2",selection.Data(),iC),fCHODCandidate[iC].GetDiscriminant());

    if (fCHODCandidate[iC].GetDiscriminant()>15){
        if(ntracks==2)
            FillHisto(Form("%sTrack%d_chodchi2_rejected",selection.Data(),iC),poschod.X(),poschod.Y());
        return 0;
    }
    // TVector3 poschod[2]; for (Int_t jdet=0; jdet<2; jdet++) posAtCHOD[jdet] = tools->GetPositionAtZ(track,fZCHOD[jdet]);
    Double_t dx = fCHODCandidate[iC].GetX()-poschod.X();
    Double_t dy = fCHODCandidate[iC].GetY()-poschod.Y();
    Double_t mindist = sqrt(dx*dx+dy*dy);
    Double_t dtime = fCHODCandidate[iC].GetDeltaTime();
    FillHisto(Form("%sTrack%d_dist_vs_mintime",selection.Data(),iC),dtime,mindist);
    FillHisto(Form("%sTrack%d_dy_vs_dx",selection.Data(),iC),dx,dy);


    return 1;
}
Int_t K3piTrackAnalysis::GoodLKr(Int_t ntracks, Int_t iC,TString part,int flag){
    TRecoSpectrometerCandidate* track;
    TString selection;
    if(flag==1) {
        track = fSPosCand[iC];
        selection="K3pi2";

    }
    if(flag==2){
        track = fKSSpecCand[iC];
        selection="KS";
    }

    TVector3 poslkr = fTool->GetPositionAtZ(track,Constants::zLKr);

    if(!fLKrClusCandidate[iC].GetIsLKrCandidate()){
        FillHisto(Form("%s_%s_IsLKrCl_rejected",selection.Data(),part.Data()),poslkr.X(),poslkr.Y());
    }

    if(!fLKrCellCandidate[iC].GetIsLKrCandidate()){
        FillHisto(Form("%s_%s_IsLKrCell_rejected",selection.Data(),part.Data()),poslkr.X(),poslkr.Y());
        fDownstreamTrack[iC].SetIsGoodLKr(0);
    }


    if(!fLKrCellCandidate[iC].GetIsLKrCandidate()) return 0;

    Double_t lkrtime = 99999999.;
    Double_t dr = 99999999.;
    Double_t dist = 99999999.;
    Double_t dtime = 99999999.;
    Double_t chodtime = 99999999.;
    Double_t energy = 0;
    Double_t seedenergy = 0;
    Int_t ncells = 0;
    Double_t dx = -99999.;
    Double_t dy = -99999.;
    Double_t pmag = track->GetMomentum()*Constants::fmevtogev;

    ncells      = fLKrCellCandidate[iC].GetNCells();
    energy      = fLKrCellCandidate[iC].GetEnergy();
    chodtime    = fCHODCandidate[iC].GetDeltaTime()+track->GetTime();
    dtime       = chodtime - (fLKrCellCandidate[iC].GetDeltaTime()+track->GetTime());
    dist        = fLKrCellCandidate[iC].GetDiscriminant();
    dx          = fLKrCellCandidate[iC].GetX()-poslkr.X();
    dy          = fLKrCellCandidate[iC].GetY()-poslkr.Y();
    dr          = sqrt(dx*dx+dy*dy);

    FillHisto(Form("%s_%s_IsLKrCell_mindist_vs_dtchod",selection.Data(),part.Data()),dtime,dist);
    FillHisto(Form("%s_%s_IsLKrCell_dx_vs_dy",selection.Data(),part.Data()),dx,dy);

    if(dr >= 100) return 0;
    if(dtime >= 6) return 0;

    FillHisto(Form("%s_%s_EoP_vs_P",selection.Data(),part.Data()),pmag, energy/pmag);

    fDownstreamTrack[iC].SetLKrEovP(energy/pmag);
    fDownstreamTrack[iC].SetLKrPosition(dx+poslkr.X(),dy+poslkr.Y());
    fDownstreamTrack[iC].SetLKrNCells(ncells);
    fDownstreamTrack[iC].SetLKrSeedEnergy(seedenergy);
    fDownstreamTrack[iC].SetIsGoodLKr(1);

    return 1;
}

Int_t K3piTrackAnalysis::GoodRICH(Int_t ntracks, Int_t iC){

    TRecoSpectrometerCandidate* track = fSPosCand[iC];

    if(!fRICHCandidate[iC].GetIsRICHCandidate()) return 0;
    Double_t pmag    = track->GetMomentum()*0.001;
    Double_t focal   = 17020;
    Double_t r       = fRICHCandidate[iC].GetSingleRingRadius();
    Double_t prob    = fRICHCandidate[iC].GetSingleRingChi2();
    Double_t t       = fRICHCandidate[iC].GetSingleRingTime();
    Double_t dx      = fRICHCandidate[iC].GetSingleRingDX();
    Double_t dy      = fRICHCandidate[iC].GetSingleRingDY();
    TVector2 rc(fRICHCandidate[iC].GetSingleRingXcenter(),fRICHCandidate[iC].GetSingleRingYcenter());
    Int_t    nobshits= fRICHCandidate[iC].GetSingleRingNHits();
    Double_t tchod   = fCHODCandidate[iC].GetDeltaTime()+track->GetTime();
    Double_t dtchod  = t-tchod;

    double   changle        = atan(r/focal);
    double   changle_el     = atan(fElectronRingRadius/focal);//ring radius 180mm for electrons
    double   richthx = rc.X()/focal;
    double   richthy = rc.Y()/focal;
    double   dslopex = richthx - track->GetSlopeXAfterMagnet();
    double   dslopey = richthy - track->GetSlopeYAfterMagnet();
    double   refindex= 1.0/cos(changle_el);
    Double_t mass    = refindex*refindex*cos(changle)*cos(changle) - 1 > 0 ? pmag*sqrt(refindex*refindex*cos(changle)*cos(changle) - 1): 99999 ;
    double   d2ring  = sqrt(fabs(fElectronRingRadius*fElectronRingRadius - r*r));
    double   prich   = 0.001*Constants::MPI*focal/d2ring;
    TLorentzVector pwithrich = fTool->Get4Momentum(prich*1000,track->GetSlopeXBeforeMagnet(),track->GetSlopeYBeforeMagnet(),0.001*Constants::MPI);

    FillHisto(Form("Track%d_rich_xydslope",iC),dslopex,dslopey);
    FillHisto(Form("Track%d_rich_dxdy",iC),dx,dy);
    FillHisto(Form("Track%d_rich_r_vs_p",iC),pmag, r);
    FillHisto(Form("Track%d_rich_nhits",iC),nobshits);
    FillHisto(Form("Track%d_rich_dtchod",iC),dtchod);
    FillHisto(Form("Track%d_rich_mass_vs_p",iC),pmag,mass);
    FillHisto(Form("Track%d_richp_vs_p",iC) ,pmag,prich);

    if(fabs(dtchod) > 2) return 0;

    return 1;

}

Int_t K3piTrackAnalysis::GoodGTK(Int_t ntracks, Int_t iC){



    fGigaTrackerAnalysis->SetTimeShift(0);
    fGigaTrackerAnalysis->SetRecoTimingCut(1.2);
    fGigaTrackerAnalysis->SetMatchingTimingCut(-0.5,0.5);

    double time     = fRICHCandidate[iC].GetSingleRingTime();
    double cedartime= fCedarCandidate[iC].GetTime();
    //double ktagtime = fRICHCandidate[iC].GetSingleRingTime();
    int nGoodTrack  = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,time,GetEventHeader()->GetTimeStamp(),1); // Remove previous candidates !
    int matchedgtk = fGigaTrackerAnalysis->TrackCandidateMatching(fGigaTrackerEvent,fSPosCand[0],time,0,nGoodTrack);

    Double_t discr_best = -1;
    Double_t discr_cedar = -1;
    fcda = 9999999.;

    if(fGigaTrackerEvent->GetNCandidates() == 0) return 0;
    if(matchedgtk < 0) return 0;
    TRecoGigaTrackerCandidate* bestgtk = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(matchedgtk);

    double gtktime  = bestgtk->GetTime();
    double gtkchi2  = bestgtk->GetChi2();
    int gtknhits    = bestgtk->GetNHits();
    TVector3 gtkpos = bestgtk->GetPosition(2);
    TVector3 gtkpos1= bestgtk->GetPosition(0);
    TVector3 gtkpos2= bestgtk->GetPosition(1);
    TVector3 gtkmomentum= bestgtk->GetMomentum();

    if(ntracks==2){

        FillHisto(Form("Track%d_dtgtkrich", iC),gtktime - time);
        FillHisto(Form("Track%d_dtgtkcedar", iC),gtktime - cedartime);

        FillHisto(Form("Track%d_gtk_P"    , iC), gtkmomentum.Mag()*fmevtogev);
        FillHisto(Form("Track%d_gtk_chi2" , iC), gtkchi2);
        FillHisto(Form("Track%d_gtk_pos"  , iC), gtkpos.X(),gtkpos.Y());
    }

    if(gtkchi2 > 50) return 0;

    double dx12   = gtkpos1.X() - gtkpos2.X();
    double dx23   = gtkpos2.X() - gtkpos.X() ;
    double dx13   = gtkpos1.X() - gtkpos.X() ;
    double dy12   = gtkpos1.Y() - gtkpos2.Y();
    double dy23   = gtkpos2.Y() - gtkpos.Y() ;
    double dy13   = gtkpos1.Y() - gtkpos.Y() ;
    double thetax = dx13/ (gtkpos1.Z() - gtkpos.Z());
    double thetay = dy13/ (gtkpos1.Z() - gtkpos.Z());

    if(ntracks==2){
        FillHisto(Form("Track%d_gtk_dX_12" , iC),dx12 );
        FillHisto(Form("Track%d_gtk_dX_23" , iC),dx23 );
        FillHisto(Form("Track%d_gtk_dX_13" , iC),dx13 );
        FillHisto(Form("Track%d_gtk_dY_12" , iC),dy12 );
        FillHisto(Form("Track%d_gtk_dY_23" , iC),dy23 );
        FillHisto(Form("Track%d_gtk_dY_13" , iC),dy13 );
        FillHisto(Form("Track%d_gtk_thetax", iC),thetax);
        FillHisto(Form("Track%d_gtk_thetay", iC),thetay);
    }


    // Vertex: first iteration without momentum correction for blue field
    TVector3 vertex = fGigaTrackerAnalysis->ComputeVertex(bestgtk,fSPosCand[0],&fcda);

    discr_best  = fGigaTrackerAnalysis->Discriminant(fcda,gtktime - time);
    discr_cedar = fGigaTrackerAnalysis->Discriminant(fcda,gtktime - cedartime);

    if(ntracks==2){
        FillHisto(Form("Track%d_gtk_dt_vs_cda",iC),gtktime - time, fcda);
        FillHisto(Form("Track%d_gtk_dtcedar_vs_cda",iC),gtktime - cedartime , fcda);
    }

    if(discr_best<0.01) return 0;
    if(discr_cedar<0.01) return 0;

    if(ntracks==2){
        FillHisto(Form("Track%d_gtk_dt_vs_cda_after",iC),gtktime - time, fcda);
        FillHisto(Form("Track%d_gtk_dtcedar_vs_cda_after",iC),gtktime - cedartime , fcda);
    }

    FillUpstream(iC,bestgtk,discr_best);
    return 1;
}
void K3piTrackAnalysis::FillUpstream(Int_t iC,TRecoGigaTrackerCandidate* gtkcand, Double_t discr){

    TLorentzVector Pgtk;
    Pgtk.SetXYZM(gtkcand->GetMomentum().X()*0.001, gtkcand->GetMomentum().Y()*0.001, gtkcand->GetMomentum().Z()*0.001,Constants::MKCH*0.001); //check if the mass is right (its MeV now)

    fUpstreamTrack[iC].SetMomentum(Pgtk); // Automatically co
    fUpstreamTrack[iC].SetTime(gtkcand->GetTime());
    fUpstreamTrack[iC].SetPosition(gtkcand->GetPosition(2));
    fUpstreamTrack[iC].SetPosGTK1(gtkcand->GetPosition(0));
    fUpstreamTrack[iC].SetPosGTK2(gtkcand->GetPosition(1));
    fUpstreamTrack[iC].SetQuality(discr);
    fUpstreamTrack[iC].SetVertex(fTwoTrackVertex);
    fUpstreamTrack[iC].SetCDA(fcda);
    return ;
}

Bool_t K3piTrackAnalysis::SelectThreeTrackEvent(){
    // // Three-track vertices
    // int ind[3];
    // for (ind[0]=0; ind[0]<fSpectrometerEvent->GetNCandidates(); ind[0]++) {
    //   for (ind[1]=ind[0]+1; ind[1]<fSpectrometerEvent->GetNCandidates(); ind[1]++) {
    //     for (ind[2]=ind[1]+1; ind[2]<fSpectrometerEvent->GetNCandidates(); ind[2]++) {
    //       BuildVertex(ind, 3);
    //     }
    //   }
    // }

    // std::vector<SpectrometerTrackVertex>::iterator vtxit;

    //if(fVertexContainer.size() != 1) return;

    //for(vtxit = fVertexContainer.begin(); vtxit != fVertexContainer.end();vtxit++){

    // TVector3 VtxPos = (*vtxit).GetPosition();
    // Double_t Xvertex = VtxPos.x();
    // Double_t Yvertex = VtxPos.y();
    // Double_t Zvertex = VtxPos.z();
    // Double_t VtxCharge = (*vtxit).GetCharge();
    // Double_t VtxChi2 = (*vtxit).GetChi2();
    // TVector3 Ptot = (*vtxit).GetTotalThreeMomentum();

    // if( VtxPos.x() < -20  || VtxPos.x() > 100 ) continue;
    // if(fabs(VtxPos.y()) > 20 ) continue;
    // if(VtxPos.z() > 165000 || VtxPos.z() < 110000  ) continue;
    // if(VtxChi2 > 10) continue;
    // if(fabs(VtxCharge) > 1) continue;

    // TLorentzVector trkp[3];

    // Int_t ngoodtracks=0;
    // Int_t ngoodrich=0;
    // int npos=0; // number of positively charged tracks
    // fSPosCand = nullptr;
    // fSPosCandID = -1;

    // vector<double> chodPar;

    // std::cout << "-----" << std::endl;
    //for(int i=0;i<3;i++){
    //
    //  Int_t iTrack = (*vtxit).GetTrackIndex(i);
    //  TRecoSpectrometerCandidate* Scand = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(iTrack);
    //  Double_t ptrack   = Scand->GetMomentum()*fmevtogev;
    //  double pattreco = Scand->GetMomentumBeforeFit()*fmevtogev;
    //  double chi2 = Scand->GetChi2();
    //  if(GetWithMC())
    //    Scand->SetTime(Scand->GetTime()-8.6);
    //  double ttime = Scand->GetTime();
    //  int charge = Scand->GetCharge();
    //  int nChambers = Scand->GetNChambers();
    //
    //
    //  fCHODTime[i] = -99999;
    //  //Z is the average between horizontal and vertical channels
    //  fCHODPos[i].SetXYZ(-9999,-9999,-9999);
    //
    //  if (fabs(ptrack - pattreco) > 20) continue;
    //  if (chi2 > 20) continue;
    //  if (nChambers!=4) continue;
    //  if (!fGeo->InAcceptance(Scand, kSpectrometer, 0)) continue;
    //  if (!fGeo->InAcceptance(Scand, kSpectrometer, 1)) continue;
    //  if (!fGeo->InAcceptance(Scand, kSpectrometer, 2)) continue;
    //  if (!fGeo->InAcceptance(Scand, kSpectrometer, 3)) continue;
    //  if (!fGeo->InAcceptance(Scand, kNewCHOD))         continue;
    //  if (!fGeo->InAcceptance(Scand, kCHOD))         continue;
    //  //if (!fGeo->InAcceptance(Scand, kRICH))         continue;
    //  if (!fGeo->InAcceptance(Scand, kLKr))         continue;
    //
    //  trkp[i].SetVectM((*vtxit).GetTrackThreeMomentum(i),PiMass*fgevtomev);
    //
    //
    //  Double_t zStraw[4];
    //  TVector3 pos_atstraw[4];
    //  TVector3 pos_atlkr  = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zLKr);
    //  TVector3 pos_atmuv1 = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zMUV1);
    //  TVector3 pos_atmuv2 = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zMUV2);
    //  TVector3 pos_atmuv3 = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zMUV3);
    //  //cout << "zLKr = " << zLKr << endl;
    //
    //
    //  zStraw[0] = 183508.0;
    //  zStraw[1] = 194066.0;
    //  zStraw[2] = 204459.0;
    //  zStraw[3] = 218885.0;

    //  for (int j=0; j < 4; j++) {
    //    // pos_atstraw[j] = fTools->GetPositionAtZ(track,zStraw[j]);
    //    pos_atstraw[j] = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zStraw[j]);
    //    FillHisto(Form("trackXY_atstraw%d",j+1),pos_atstraw[j].X(),pos_atstraw[j].Y());
    //  }
    //
    //  //std::cout << "chod hits = " << fCHODEvent->GetNHits()<< std::endl;
    //  // Int_t ichod = MatchCHOD(i,Scand);
    //  chodPar = CHODTrackMatching(Scand->xAtAfterMagnet(fGeo->GetZCHODVPlane()),Scand->yAtAfterMagnet(fGeo->GetZCHODHPlane()),ttime);
    //  fCHODPos[i].SetXYZ(chodPar[7],chodPar[8],(fGeo->GetZCHODHPlane()+fGeo->GetZCHODVPlane())/2);
    //  fCHODTime[i] = chodPar[2]+ Scand->GetTime();
    //  //fCHODPos[i].SetXYZ(dx,d,239150);
    //
    //  //AnalyzeLKr(i,fCHODTime[i]);
    //
    //  Int_t igoodlkr = MatchLKr(iTrack,fCHODTime[i],pos_atlkr);
    //
    //  //cout << "-------End with ilk = " << igoodlkr << endl;
    //  if(igoodlkr >= 0) {
    //    //if(igoodlkr < 0)continue;
    //    TRecoLKrCandidate* closestlkrcand = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(igoodlkr);
    //    Double_t energy = closestlkrcand->GetClusterEnergy()*fmevtogev;
    //    Double_t eop = closestlkrcand->GetClusterEnergy()/Scand->GetMomentum();
    //    FillHisto("track_lkrenergy",energy);
    //    FillHisto("track_eop",eop);
    //
    //    if(eop > 0.8) continue;
    //    if(eop < 0.15) continue;
    //  }
    //
    //  // std::cout << "i = " << i << std::endl;
    //  // std::cout << "ichod = " << ichod << std::endl;
    //  // std::cout << "ilkr = " << igoodlkr << std::endl;
    //  if(ptrack < 10) continue;
    //
    //  FillHisto("trackXY_atCHOD",fCHODPos[i].X(),fCHODPos[i].Y());
    //  FillHisto("trackXY_atLKr",pos_atlkr.X(),pos_atlkr.Y());
    //
    //  Int_t igoodmuv1= MUV1Match(fCHODTime[i],pos_atmuv1);
    //  Int_t igoodmuv2= MUV2Match(fCHODTime[i],pos_atmuv2);
    //  Int_t igoodmuv3= MUV3Match(fCHODTime[i],pos_atmuv3);
    //
    //
    //  FillHisto("trackXY_atMUV3",pos_atmuv3.X(),pos_atmuv3.Y());
    //  if(igoodmuv3 >= 0) continue;
    //
    //  FillHisto("trackXY_atMUV1",pos_atmuv1.X(),pos_atmuv1.Y());
    //  //if(igoodmuv1 < 0) continue;
    //
    //  FillHisto("trackXY_atMUV2",pos_atmuv2.X(),pos_atmuv2.Y());
    //  //if(igoodmuv2 < 0) continue;
    //
    //  if(Scand->GetCharge()*VtxCharge == -1)
    //    FillHisto("oddtrackXY_atCHOD",fCHODPos[i].X(),fCHODPos[i].Y());
    //  //std::cout << "Time of track " << i << "in chod = " << fCHODTime[i] << std::endl;
    //
    //
    //  if(npos == 0 && Scand->GetCharge() == 1){
    //    npos++;
    //    fSPosCand     = Scand;
    //    fSPosCandID   = iTrack;
    //    fSPosCandTime = fCHODTime[i];
    //  }
    //
    //  ngoodtracks++;
    //}
    //
    //if(ngoodtracks!=3) continue;
    ////if(ngoodrich < 1) continue;
    //
    //
    //
    ////cout << "Stage 0 +" << endl;
    //
    //fKaonTime = (fCHODTime[0] + fCHODTime[1] + fCHODTime[2])/3  ;
    //fEventTime = MatchRICHSingle(fSPosCandID,fSPosCandTime, fSPosCand);
    //
    //FillHisto("eventtime",fEventTime);
    ////Int_t cedarmatch = CedarInTime(fEventTime);
    //
    ////if(cedarmatch < 0) continue;
    ////TRecoCedarCandidate* cedar = (TRecoCedarCandidate*) fCedarEvent->GetCandidate(cedarmatch);
    //
    ////FillHisto("dtcedar",fEventTime - cedar->GetTime());
    //
    //double minrichtime = -999;
    //double mindtrich = 989898798;
    //
    ////if(fEventTime - cedar->GetTime() > 0.65 || fEventTime - cedar->GetTime() < -0.55 ) continue;
    //
    //fK4Momentum = trkp[0] + trkp[1] +trkp[2];
    //fK3Momentum = fK4Momentum.Vect();
    //Double_t       M3pi               = fK4Momentum.M()*fmevtogev;  //[GeV/c^2]
    //Double_t       dxdz               = fK3Momentum.X()/fK3Momentum.Z();
    //Double_t       dydz               = fK3Momentum.Y()/fK3Momentum.Z();
    //
    //
    //// The middle of the TRIM5 magnet: z = 101.8 m
    //Double_t Xstart = Xvertex + dxdz*(101800.0 - Zvertex);
    //Double_t Ystart = Yvertex + dydz*(101800.0 - Zvertex);
    ////Transverse momentum of the kaon wrt to the nominal kaon axis
    //Double_t shifted_pt = (fK4Momentum.Pt() - 0.0012*fK4Momentum.P())*fmevtogev;
    //FillHisto("pttot", shifted_pt);
    //
    //if(fabs(shifted_pt) > 0.03) continue;
    //
    //
    //fLAVMatch->SetReferenceTime(fEventTime);
    //fSAVMatch->SetReferenceTime(fEventTime);
    //
    //Bool_t islav   = fLAVMatch->LAVHasTimeMatching(fLAVEvent);
    //Int_t  issav   = fSAVMatch->SAVHasTimeMatching(fIRCEvent, fSACEvent);
    //
    //if( issav !=0) continue;
    //if( islav ) continue;
    //
    //FillHisto("mtot",M3pi);
    //
    //if(M3pi > 0.497 || M3pi < 0.490) continue;
    //
    //
    //FillHisto("ptot",Ptot.Mag() *fmevtogev);
    //
    //if(Ptot.Mag()*fmevtogev > 78 || Ptot.Mag()*fmevtogev < 72) continue;
    //
    //FillHisto("dxdz",dxdz);
    //FillHisto("ptot_vs_dxdz",Ptot.Mag()*fmevtogev,dxdz);
    //FillHisto("dydz",dydz);
    //FillHisto("ptot_vs_dydz",Ptot.Mag()*fmevtogev,dydz);
    //FillHisto("XYtrim5",Xstart,Ystart);
    //FillHisto("Xtrim5" ,Xstart);
    //FillHisto("Ytrim5" ,Ystart);

    //Int_t goodgtk  = GTKMatch(fEventTime,cedar->GetTime());

    //Int_t goodgtk  = GTKMatch(minrichtime,cedar->GetTime());
    //std::cout << " WTFFFFFFFFFFFFFFFFFFFFFF"<< fGigaTrackerEvent->GetNCandidates() << std::endl;

    //if(goodgtk < 0) continue;
    //cout << trkp[0].P() << "Part 2 = " <<  trkp[1].P() << "Part 3 =" << trkp[2].P() << endl;
    //cout << "muv3 cand = " << fMUV3Event->GetNCandidates() << endl;
    //FillHisto("ptot_aftergtk",Ptot.Mag() *fmevtogev);
    //GTKAnalysis(VtxPos);

    //}

    return true;
}

void K3piTrackAnalysis::PostProcess(){
    /// \MemberDescr
    /// This function is called after an event has been processed by all analyzers. It could be used to free some memory allocated
    /// during the Process.
    /// \EndMemberDescr

}

void K3piTrackAnalysis::EndOfBurstUser(){
    /// \MemberDescr
    /// This method is called when a new file is opened in the ROOT TChain (corresponding to a start/end of burst in the normal NA62 data taking) + at the end of the last file\n
    /// Do here your start/end of burst processing if any
    /// \EndMemberDescr
}

void K3piTrackAnalysis::BuildVertex(Int_t ind[], Int_t NTracks) {

    if (fVertexContainer.size()==fMaxNVertices) return;
    if (NTracks >= fMaxNTracks) return;

    fVertexLSF.Reset();

    Int_t Charge = 0;
    for (Int_t iTrack=0; iTrack<NTracks; iTrack++) {
        TRecoSpectrometerCandidate *cand =
            (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(ind[iTrack]);
        Charge += cand->GetCharge();
        fVertexLSF.AddTrack(cand);
    }

    if (fBlueFieldCorrection) fVertexLSF.FitVertex(kTRUE); // BF correction included


    // Loose seleciton of vertices returned by VertexLSF
    if (fVertexLSF.GetChi2()>fMaxChi2) return;
    if (fVertexLSF.GetVertexPosition().z()<fMinZVertex) return;
    if (fVertexLSF.GetVertexPosition().z()>fMaxZVertex) return;


    // Save the vertex into a common structure
    SpectrometerTrackVertex Vertex;
    Vertex.SetNTracks (fVertexLSF.GetNTracks());
    Vertex.SetCharge  (Charge);
    Vertex.SetPosition(fVertexLSF.GetVertexPosition());
    Vertex.SetChi2    (fVertexLSF.GetChi2());

    TVector3 TotalThreeMomentum = TVector3(0.0, 0.0, 0.0);
    for (int iTrack=0; iTrack<fVertexLSF.GetNTracks(); iTrack++) {
        TRecoSpectrometerCandidate *Scand = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(ind[iTrack]);
        TVector3 Momentum = fVertexLSF.GetTrackThreeMomentum(iTrack);

        Vertex.AddTrack(Scand, ind[iTrack], Scand->GetCharge(), Momentum, Scand->GetThreeMomentumBeforeMagnet());

        TotalThreeMomentum += Momentum;
    }
    Vertex.SetTotalThreeMomentum(TotalThreeMomentum);

    // Monitoring of the vertices in the output
    FillHisto("vtxchi2", fVertexLSF.GetChi2());
    FillHisto("zvtx", fVertexLSF.GetVertexPosition().z()); // [mm]
    FillHisto("yvtx", fVertexLSF.GetVertexPosition().y()); // [mm]
    FillHisto("xvtx", fVertexLSF.GetVertexPosition().x()); // [mm]
    FillHisto("vtxcharge", Vertex.GetCharge());
    FillHisto("ptot_all",Vertex.GetTotalMomentum()*fmevtogev); // [GeV/c]

    fVertexContainer.push_back(Vertex);
}


Int_t K3piTrackAnalysis::GetQuadrant(Int_t channelid) {
    if (channelid<=63) {
        if (channelid>=0  && channelid <=15) return 0;
        if (channelid>=16 && channelid <=31) return 1;
        if (channelid>=32 && channelid <=47) return 2;
        if (channelid>=48 && channelid <=63) return 3;
    } else {
        if (channelid>=64  && channelid<=79)  return 0;
        if (channelid>=80  && channelid<=95)  return 1;
        if (channelid>=96  && channelid<=111) return 2;
        if (channelid>=112 && channelid<=127) return 3;
    }
    return -1;
}


Double_t K3piTrackAnalysis::MatchRICHSingle(Int_t itrack,double reftime, TRecoSpectrometerCandidate *thisTrack){

    //Bool_t singlering = false;
    //int minidring = fRICHAnal->TrackSingleRingMatching(reftime,fRICHEvent,thisTrack);
    //fRICHCand = fRICHAnal->GetRICHSingleCandidate();
    //
    //if(minidring > -1) singlering = true;
    //
    //FillHisto("Track_rich_singlering_flag", singlering );
    //
    //
    std::map<double,int> time;
    std::map<double,int> slopes;
    double   focallength       = 17020;
    //std::cout << "RICH b " <<  fRICHEvent->GetNPMTimeCandidates() << std::endl;
    for(int i = 0;i<fRICHEvent->GetNPMTimeCandidates();i++){
        TRecoRICHCandidate* richpm = (TRecoRICHCandidate*)fRICHEvent->GetPMTimeCandidate(i);

        Double_t single_radius     = richpm->GetRingRadiusSingleRing();
        Double_t single_radius_err = richpm->GetRingRadiusErrorSingleRing();
        Double_t single_chi2       = richpm->GetRingChi2SingleRing();
        Double_t single_time       = richpm->GetRingTimeSingleRing();
        TVector2 single_ringcenter = richpm->GetRingCenterSingleRing();
        TVector2 single_ringc_err  = richpm->GetRingCenterErrorSingleRing();
        Int_t    single_nobshits   = richpm->GetNHitsSingleRing();
        Double_t single_niter      = richpm->GetNIterationsSingleRing();
        // double   dtchod            = reftime - single_time;
        double   richthx           = single_ringcenter.X()/focallength;
        double   richthy           = single_ringcenter.Y()/focallength;
        double   dslopex           = richthx - thisTrack->GetSlopeXAfterMagnet();
        double   dslopey           = richthy - thisTrack->GetSlopeYAfterMagnet();
        double   dslope            = TMath::Sqrt(dslopex*dslopex + dslopey*dslopey);
        FillHisto("Track_rich_singlering_xydslope",dslopex,dslopey);
        FillHisto("Track_rich_singlering_chi2",single_chi2);
        FillHisto("Track_rich_singlering_radius_vs_p",thisTrack->GetMomentum(), single_radius);
        FillHisto("Track_rich_singlering_nobservedhits",single_nobshits);

        slopes.insert(std::make_pair(fabs(dslope),i));

    }


    for(int i = 0;i<fRICHEvent->GetNTimeCandidates();i++){
        TRecoRICHCandidate* ring = (TRecoRICHCandidate*)fRICHEvent->GetTimeCandidate(i);

        Double_t richtime        = ring->GetTime();
        double   dtchod            = reftime - richtime;
        FillHisto("Track_rich_singlering_dtchod",dtchod);

        time.insert(std::make_pair(fabs(dtchod),i));

    }
    //std::cout << "RICH a "  << fRICHEvent->GetNTimeCandidates() << std::endl;
    double   min_dtchod = -9999;
    double   min_time = -9999;
    if(time.size()!= 0){
        if(slopes.size()!= 0){

            int imin  = slopes.begin()->second;
            int itmin = time.begin()->second;
            TRecoRICHCandidate* minrich = (TRecoRICHCandidate*)fRICHEvent->GetPMTimeCandidate(imin);
            TRecoRICHCandidate* minrichtime = (TRecoRICHCandidate*)fRICHEvent->GetTimeCandidate(itmin);

            Double_t min_radius     = minrich->GetRingRadiusSingleRing();
            Double_t min_chi2       = minrich->GetRingChi2SingleRing();
            min_time       = minrich->GetRingTimeSingleRing();
            TVector2 min_ringcenter = minrich->GetRingCenterSingleRing();
            Int_t    min_nobshits   = minrich->GetNHitsSingleRing();
            min_dtchod     = reftime - min_time;
            double   min_richthx    = min_ringcenter.X()/focallength;
            double   min_richthy    = min_ringcenter.Y()/focallength;
            double   min_dslopex    = min_richthx - thisTrack->GetSlopeXAfterMagnet();
            double   min_dslopey    = min_richthy - thisTrack->GetSlopeYAfterMagnet();

            FillHisto("Track_rich_singlering_min_xydslope",min_dslopex,min_dslopey);
            FillHisto("Track_rich_singlering_min_dtchod",min_dtchod);
            FillHisto("Track_rich_singlering_min_chi2",min_chi2);
            FillHisto("Track_rich_singlering_min_radius_vs_p",thisTrack->GetMomentum(), min_radius);
            FillHisto("Track_rich_singlering_min_nobservedhits",min_nobshits);

            if(fabs(min_dtchod) < 2){

                fRICHCand->SetIsRICHSingleRingCandidateSlava(1);
                FillHisto("Track_rich_singlering_final_radius_vs_p",thisTrack->GetMomentum(), min_radius);

            }

        }
    }

    return min_time;
}

Int_t K3piTrackAnalysis::MatchLKr(Int_t itrack,Double_t trktime, TVector3 posatlkr){

    std::map<Double_t,int> closestlkr;
    std::map<Int_t,Double_t> distlkr;
    std::vector<Double_t> tdiff;
    std::vector<Double_t> dtrkcl;
    int ilkr=-1;
    TRecoLKrCandidate* lkr;
    double dtlkr;
    //cout << "start matching ----------" << endl;
    for (int iLKrCand=0; iLKrCand<fLKrEvent->GetNCandidates(); iLKrCand++) {
        lkr = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(iLKrCand);
        //maybe the number 3 should be changed later
        tdiff.push_back(3 - (lkr->GetClusterTime() - trktime));
        dtlkr = fabs(3 -( lkr->GetClusterTime() - trktime));
        dtrkcl.push_back(sqrt(pow(lkr->GetClusterX() - posatlkr.X(),2 ) + pow(lkr->GetClusterY() - posatlkr.Y(),2 ) )) ;
        closestlkr[dtlkr] = iLKrCand;
        //distlkr[iLKrCand] = dtrkcl;
        //cout << "i = " << iLKrCand << "dtlkr = " << dtlkr << "dtrkcl = " << dtrkcl[iLKrCand]<< endl;
        //cout << "x cand = " << lkr->GetClusterX() << "x track = " << posatlkr.X() << endl;
        //cout << "y cand = " << lkr->GetClusterY() << "y track = " << posatlkr.Y() << endl;



    }
    //cout << " final i = " << closestlkr.begin()->second << endl;
    if(fLKrEvent->GetNCandidates() == 0) return -1;
    ilkr = closestlkr.begin()->second;

    if(ilkr< 0) return -1;
    //cout << "lkr = " << ilkr << endl;
    FillHisto("lkrdist",dtrkcl[ilkr]);
    FillHisto("dtlkr",tdiff[ilkr]);

    //cout << " final ilkr = " << ilkr << "dt = " << tdiff[ilkr] << "lkrdist = " << dtrkcl[ilkr] << endl;
    if(fabs(tdiff[ilkr]) > 6. ) return -1;
    //if(dtrkcl[ilkr] > 30. ) return -1;

    return ilkr;
}

Int_t K3piTrackAnalysis::CedarInTime(Double_t time){

    std::map<Double_t,int> closestcedar;
    int icedar=-1;
    TRecoCedarCandidate* cedar;
    double dtcedar;

    for (int i=0; i<fCedarEvent->GetNCandidates(); i++) {
        cedar = (TRecoCedarCandidate*) fCedarEvent->GetCandidate(i);
        dtcedar = fabs(time - cedar->GetTime());
        FillHisto("Cedar_nsectors",cedar->GetNSectors());
        if(cedar->GetNSectors() < 4) continue;
        closestcedar[dtcedar] = i;
    }

    if(fCedarEvent->GetNCandidates() == 0) return -1;
    if(closestcedar.size() == 0) return -1;
    if(closestcedar.size() > 0) icedar = closestcedar.begin()->second;
    else icedar = -1;

    if(icedar < 0) return -1;
    return icedar;
}

//Int_t K3piTrackAnalysis::GTKMatch(double chodtime,double cedartime){
//
//  fGigaTrackerAnalysis->SetTimeShift(0);
//  fGigaTrackerAnalysis->SetRecoTimingCut(1.2);
//  fGigaTrackerAnalysis->SetMatchingTimingCut(-0.5,0.5);
//
//  int nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,cedartime,GetEventHeader()->GetTimeStamp(),1,fIsMC); // Remove previous candidates !
//  int matchedgtk = fGigaTrackerAnalysis->TrackCandidateMatching(fGigaTrackerEvent,fSPosCand[0],cedartime,0,nGoodTrack);
//
//  Double_t discr_best = -1;
//  Double_t discr_cedar = -1;
//  Double_t cda = 9999999.;
//
//  if(fGigaTrackerEvent->GetNCandidates() == 0) return -1;
//
//  std::map<double,int> t_Kaondiscriminant;
//
//  for(int i(0);i<nGoodTrack;i++){
//
//    TRecoGigaTrackerCandidate* gtkcand = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(i);
//
//    double gtktime1 = gtkcand->GetTime();
//    TVector3 t_gtkpos= gtkcand->GetPosition(2);
//    TVector3 t_gtkpos1= gtkcand->GetPosition(0);
//    TVector3 t_gtkpos2= gtkcand->GetPosition(1);
//    TVector3 t_gtkmomentum= gtkcand->GetMomentum();
//
//    double cda1 =8888888;
//    TVector3 vtx = fGigaTrackerAnalysis->ComputeVertex(gtkcand,fSPosCand,&cda1);
//    double discr = 999999;
//
//    double t_dp =fK3Momentum.Mag()*fmevtogev - t_gtkmomentum.Mag()*fmevtogev;
//    double t_dpx =fK3Momentum.Px()*fmevtogev - t_gtkmomentum.Px()*fmevtogev;
//    double t_dpy =fK3Momentum.Py()*fmevtogev - t_gtkmomentum.Py()*fmevtogev;
//    double t_pk_k3pi =fK3Momentum.Mag()*fmevtogev;
//    TVector3 pos_at_gtk3 = AnalysisTools::GetInstance()->GetKaonPositionAtZ(fK3Momentum,vtx,t_gtkpos.Z());
//    double t_dx = t_gtkpos.X() - pos_at_gtk3.X();
//    double t_dy = t_gtkpos.Y() - pos_at_gtk3.Y();
//    double kaonchi2= pow(t_dp/0.55,2) + pow(t_dpy/0.005,2) + pow(t_dpx/0.005,2) + pow(t_dx/2.5,2) + pow(t_dy/2.5,2);
//    t_Kaondiscriminant.insert(std::make_pair(kaonchi2,i));
//
//    FillHisto("Discr_kaon_all", kaonchi2);
//  }
//
//
//  double min_kaondiscr = t_Kaondiscriminant.begin()->first;
//  double igtkmin       = t_Kaondiscriminant.begin()->second;
//  FillHisto("Discr_kaon_true", min_kaondiscr);
//
//  if(min_kaondiscr > 40) return -1;
//  FillHisto("chodcedar_testdt",cedartime - chodtime + 0.049);
//  if(matchedgtk < 0 || nGoodTrack == 0) return -1;
//  TRecoGigaTrackerCandidate* bestgtk = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(matchedgtk);
//
//  int type = bestgtk->GetType();
//  double gtktime = bestgtk->GetTime();
//  double gtkchi2X = bestgtk->GetChi2X();
//  double gtkchi2Y = bestgtk->GetChi2Y();
//  double gtkchi2T = bestgtk->GetChi2Time();
//  double gtkchi2  = bestgtk->GetChi2();
//  int gtknhits = bestgtk->GetNHits();
//  TVector3 gtkpos= bestgtk->GetPosition(2);
//  TVector3 gtkpos1= bestgtk->GetPosition(0);
//  TVector3 gtkpos2= bestgtk->GetPosition(1);
//  TVector3 gtkmomentum= bestgtk->GetMomentum();
//
//  FillHisto("dtgtkcedar",gtktime - cedartime - 0.049);
//  FillHisto("dtgtkchod",gtktime - chodtime);
//
//  if(fabs(gtktime - cedartime - 0.049) > 0.5) return -1;
//  FillHisto("gtk_type", type);
//  if(type < 100) return -1;
//
//  // FillHisto("dtgtkchod",tdiffchod[igtk]);
//  // FillHisto("dtgtkcedar",tdiffcedar[igtk]);
//
//  FillHisto("gtk_P", gtkmomentum.Mag()*fmevtogev);
//  FillHisto("gtk_chi2X", gtkchi2X);
//  FillHisto("gtk_chi2Y", gtkchi2Y);
//  FillHisto("gtk_chi2T", gtkchi2T);
//  FillHisto("gtk_chi2", gtkchi2);
//  FillHisto("gtk_Nhits", gtknhits);
//  FillHisto("gtk_pos", gtkpos.X(),gtkpos.Y());
//
//  if(gtkchi2 > 50) return -1;
//
//  double dx12 = gtkpos1.X() - gtkpos2.X();
//  double dx23 = gtkpos2.X() - gtkpos.X() ;
//  double dx13 = gtkpos1.X() - gtkpos.X() ;
//  double dy12 = gtkpos1.Y() - gtkpos2.Y();
//  double dy23 = gtkpos2.Y() - gtkpos.Y() ;
//  double dy13 = gtkpos1.Y() - gtkpos.Y() ;
//  double thetax= dx13/ (gtkpos1.Z() - gtkpos.Z());
//  double thetay = dy13/ (gtkpos1.Z() - gtkpos.Z());
//
//  FillHisto("gtk_dX_12",dx12 );
//  FillHisto("gtk_dX_23",dx23 );
//  FillHisto("gtk_dX_13",dx13 );
//  FillHisto("gtk_dY_12",dy12 );
//  FillHisto("gtk_dY_23",dy23 );
//  FillHisto("gtk_dY_13",dy13 );
//  FillHisto("gtk_thetax", thetax);
//  FillHisto("gtk_thetay", thetay);
//
//
//  // Vertex: first iteration without momentum correction for blue field
//  TVector3 vertex = fGigaTrackerAnalysis->ComputeVertex(bestgtk,fSPosCand[0],&cda);
//
//  //TVector3 vertex = fTool->SingleTrackVertex(gtkmomentum,p3pion,gtkpos,pospion,cda);
//
//  discr_best  = fGigaTrackerAnalysis->Discriminant(cda,gtktime - chodtime);
//  discr_cedar = fGigaTrackerAnalysis->Discriminant(cda,gtktime - cedartime - 0.049);
//
//  FillHisto("gtk_discriminant",discr_best);
//  FillHisto("gtk_discriminant_cedar",discr_cedar);
//  FillHisto("gtk_dt_vs_cda",gtktime - chodtime, cda);
//  FillHisto("gtk_dtcedar_vs_cda",gtktime - cedartime - 0.049 , cda);
//
//  //Selecting True Kaon
//
//  TRecoGigaTrackerCandidate* gtktrue = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(igtkmin);
//  TVector3 true_gtkpos= gtktrue->GetPosition(2);
//  TVector3 true_gtkpos1= gtktrue->GetPosition(0);
//  TVector3 true_gtkpos2= gtktrue->GetPosition(1);
//  TVector3 true_gtkmomentum= gtktrue->GetMomentum();
//
//
//  double dp           =true_gtkmomentum.Mag()*fmevtogev - gtkmomentum.Mag()*fmevtogev;
//  double dpx          =true_gtkmomentum.Px()*fmevtogev - gtkmomentum.Px()*fmevtogev;
//  double dpy          =true_gtkmomentum.Py()*fmevtogev - gtkmomentum.Py()*fmevtogev;
//  //double pk_k3pi      =true_gtkmomentum.Mag()*fmevtogev;
//  double dx           =gtkpos.X() - true_gtkpos.X();
//  double dy           =gtkpos.Y() - true_gtkpos.Y();
//
//  double chi2true = pow(dp/0.55,2) + pow(dpx/0.005,2) + pow(dpy/0.005,2) + pow(dx/2.5,2) + pow(dy/2.5,2);
//
//  if(discr_best > 0.01){
//    FillHisto("RICH_discr_kaon_true", min_kaondiscr);
//
//    if(min_kaondiscr < 40){
//
//      FillHisto("RICH_chi2_true", chi2true);
//      FillHisto("RICH_dt_vs_cda",gtktime - chodtime, cda);
//      FillHisto("RICH_dtcedar_vs_cda",gtktime - cedartime - 0.049 , cda);
//    }
//
//  }
//
//  if(discr_cedar > 0.01){
//    FillHisto("Cedar_discr_kaon_true", min_kaondiscr);
//    if(min_kaondiscr < 40){
//      FillHisto("Cedar_chi2_true", chi2true);
//      FillHisto("Cedar_dt_vs_cda",gtktime - chodtime, cda);
//      FillHisto("Cedar_dtcedar_vs_cda",gtktime - cedartime - 0.049 , cda);
//    }
//  }
//
//  if(discr_best<= 0.01) return -1;
//
//  FillHisto("gtk_dt_vs_cda_after",gtktime - chodtime, cda);
//  FillHisto("gtk_dtcedar_vs_cda_after",gtktime - cedartime - 0.049 , cda);
//  FillHisto("gtk_discriminant_after",discr_best);
//  FillHisto("dtgtkcedar_after",gtktime - cedartime - 0.049);
//
//  if(discr_cedar<= 0.01) return -1;
//  FillHisto("gtk_dt_vs_cda_final",gtktime - chodtime, cda);
//  FillHisto("gtk_dtcedar_vs_cda_final",gtktime - cedartime - 0.049 , cda);
//
//  FillHisto("Both_discr_kaon_true", min_kaondiscr);
//  if(min_kaondiscr < 40){
//    FillHisto("Both_chi2_true", chi2true);
//  }
//
//  FillHisto("gtk_Nhits_vs_P", (Double_t)gtknhits, gtkmomentum.Mag()*fmevtogev);
//  FillHisto("gtk_Ncand", fGigaTrackerEvent->GetNCandidates());
//
//
//  return matchedgtk;
//
//}


Int_t K3piTrackAnalysis::MUV1Match(Double_t trktime, TVector3 posatMUV1){

    std::map<Double_t,int> closestMUV1;
    std::vector<Double_t> tdiff;
    std::vector<Double_t> dtrkcl;
    int iMUV1=-1;
    TRecoMUV1Candidate* MUV1;
    TRecoMUV1Candidate* goodmuv1;
    double dtMUV1;

    for (int i=0; i<fMUV1Event->GetNCandidates(); i++) {
        MUV1 = (TRecoMUV1Candidate*) fMUV1Event->GetCandidate(i);
        //maybe the number 3 should be changed later
        tdiff.push_back(MUV1->GetTime() - trktime);
        dtMUV1 = fabs(MUV1->GetTime() - trktime);
        dtrkcl.push_back(sqrt(pow(MUV1->GetX() - posatMUV1.X(),2 ) + pow(MUV1->GetY() - posatMUV1.Y(),2 ) )) ;
        closestMUV1[dtMUV1] = i;

    }
    if(fMUV1Event->GetNCandidates() == 0) return -1; else iMUV1 = closestMUV1.begin()->second;

    if(iMUV1< 0) return -1;

    goodmuv1 = (TRecoMUV1Candidate*) fMUV1Event->GetCandidate(iMUV1);
    FillHisto("dtmuv1", tdiff[iMUV1]);
    FillHisto("distmuv1", dtrkcl[iMUV1]);
    if(dtrkcl[iMUV1] > 120. ) return -1;
    if(fabs(tdiff[iMUV1]) > 10. ) return -1;

    return iMUV1;
}

Int_t K3piTrackAnalysis::MUV2Match(Double_t trktime, TVector3 posatMUV2){

    std::map<Double_t,int> closestMUV2;
    std::vector<Double_t> tdiff;
    std::vector<Double_t> dtrkcl;
    int iMUV2=-1;
    TRecoMUV2Candidate* MUV2;
    TRecoMUV2Candidate* goodmuv2;
    double dtMUV2;

    for (int i=0; i<fMUV2Event->GetNCandidates(); i++) {
        MUV2 = (TRecoMUV2Candidate*) fMUV2Event->GetCandidate(i);
        //maybe the number 3 should be changed later
        tdiff.push_back(MUV2->GetTime() - trktime);
        dtMUV2 = fabs(MUV2->GetTime() - trktime);
        dtrkcl.push_back(sqrt(pow(MUV2->GetX() - posatMUV2.X(),2 ) + pow(MUV2->GetY() - posatMUV2.Y(),2 ) )) ;
        closestMUV2[dtMUV2] = i;

    }
    if(fMUV2Event->GetNCandidates() == 0) return -1; else iMUV2 = closestMUV2.begin()->second;

    if(iMUV2< 0) return -1;

    goodmuv2 =(TRecoMUV2Candidate*) fMUV2Event->GetCandidate(iMUV2);
    FillHisto("dtmuv2", tdiff[iMUV2]);
    FillHisto("distmuv2", dtrkcl[iMUV2]);

    if(fabs(tdiff[iMUV2]) > 10. ) return -1;
    if(dtrkcl[iMUV2] > 220. ) return -1;


    //std::cout << "sw = " << goodmuv2->GetShowerWidth() << std::endl;
    return iMUV2;
}
Int_t K3piTrackAnalysis::MUV3Match(Double_t trktime, TVector3 posatMUV3){

    std::map<Double_t,int> closestMUV3;
    std::vector<Double_t> tdiff;
    std::vector<Double_t> dtrkcl;
    int iMUV3=-1;
    TRecoMUV3Candidate* MUV3;
    TRecoMUV3Candidate* goodmuv3;
    double dtMUV3;

    for (int i=0; i<fMUV3Event->GetNCandidates(); i++) {
        MUV3 = (TRecoMUV3Candidate*) fMUV3Event->GetCandidate(i);
        //maybe the number 3 should be changed later
        tdiff.push_back(MUV3->GetTime() - trktime);
        dtMUV3 = fabs(MUV3->GetTime() - trktime);
        dtrkcl.push_back(sqrt(pow(MUV3->GetX() - posatMUV3.X(),2 ) + pow(MUV3->GetY() - posatMUV3.Y(),2 ) )) ;
        closestMUV3[dtMUV3] = i;

    }
    if(fMUV3Event->GetNCandidates() == 0) return -1; else iMUV3 = closestMUV3.begin()->second;

    if(iMUV3< 0) return -1;

    goodmuv3 = (TRecoMUV3Candidate*)fMUV3Event->GetCandidate(iMUV3);

    FillHisto("dtmuv3", tdiff[iMUV3]);
    FillHisto("distmuv3", dtrkcl[iMUV3]);

    if(fabs(tdiff[iMUV3]) > 3. ) return -1;
    if(dtrkcl[iMUV3] > 440. ) return -1;

    //std::cout << "MUV3 = " << closestMUV3.begin()->first  << std::endl;
    //std::cout << "MUV3 = " << closestMUV3.begin()->second  << std::endl;
    //std::cout << "MUV3dtrkcl = " << dtrkcl[iMUV3]  << std::endl;


    return iMUV3;
}


Int_t K3piTrackAnalysis::GTKAnalysis(TVector3 vtxposition){


    std::vector<double> discriminant;
    double best_discr= 90000;
    int ibestdiscr=-1;
    for(int i(0);i<fGigaTrackerEvent->GetNCandidates();i++){
        TRecoGigaTrackerCandidate* gtk = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(i);

        //int type = gtk->GetType();
        double gtktime = gtk->GetTime();
        //double gtkchi2X = gtk->GetChi2X();
        //double gtkchi2Y = gtk->GetChi2Y();
        //double gtkchi2T = gtk->GetChi2Time();
        //double gtkchi2  = gtk->GetChi2();
        //int gtknhits = gtk->GetNHits();
        TVector3 gtkpos1= gtk->GetPosition(0);
        TVector3 gtkpos2= gtk->GetPosition(1);
        TVector3 gtkpos3= gtk->GetPosition(2);
        TVector3 gtkmomentum= gtk->GetMomentum();

        double dp =fK3Momentum.Mag()*fmevtogev - gtkmomentum.Mag()*fmevtogev;
        double pk_k3pi =fK3Momentum.Mag()*fmevtogev;
        TVector3 pos_at_gtk3 = AnalysisTools::GetInstance()->GetKaonPositionAtZ(fK3Momentum,vtxposition,gtkpos3.Z());

        double dx = gtkpos3.X() - pos_at_gtk3.X();
        double dy = gtkpos3.Y() - pos_at_gtk3.Y();
        double dt = gtktime-fEventTime;
        FillHisto("dtpikaon",dt);
        FillHisto("pk_vs_dpk",pk_k3pi,dp);
        FillHisto("pk_vs_dx",pk_k3pi,dx);
        FillHisto("pk_vs_dy",pk_k3pi,dy);

        FillHisto("x_vs_kthx",pos_at_gtk3.X(),fK3Momentum.X()/fK3Momentum.Z());
        FillHisto("gtk_x_vs_kthx",gtkpos3.X(),gtkmomentum.X()/gtkmomentum.Z());
        FillHisto("y_vs_kthy",pos_at_gtk3.Y(),fK3Momentum.Y()/fK3Momentum.Z());
        FillHisto("gtk_y_vs_kthy",gtkpos3.Y(),gtkmomentum.Y()/gtkmomentum.Z());

        discriminant.push_back(pow(dx/2.5,2) + pow(dy/2.5,2) + pow(dt/0.16,2));

        FillHisto("discriminant",discriminant[i]);

        if(discriminant[i] < best_discr){
            ibestdiscr = i;
            best_discr = discriminant[i];
        }

    }

    FillHisto("discriminant_best",discriminant[best_discr]);

    for(int i(0);i<fGigaTrackerEvent->GetNCandidates();i++){

        if(i==ibestdiscr) continue;

        FillHisto("discriminant_difference",discriminant[i] - discriminant[ibestdiscr]);

    }
    return 1;


}
void K3piTrackAnalysis::EndOfRunUser(){
    SaveAllPlots();
    //fGigaTrackerAnalysis->GetUserMethods()->SaveAllPlots();
    fCedarAnal->GetUserMethods()->SaveAllPlots();
    fStrawAnalysis->GetUserMethods()->SaveAllPlots();
    //fRICHAnal->GetUserMethods()->SaveAllPlots();
}


void K3piTrackAnalysis::DrawPlot(){
}

K3piTrackAnalysis::~K3piTrackAnalysis(){
}

bool K3piTrackAnalysis::CHODQ1() {
    bool isQ1 = false;

    // Sort the hits per increasing channel number
    vector<int> idHit(fCHODEvent->GetNHits());
    iota(begin(idHit),end(idHit),0);
    ChannelOrder co(fCHODEvent);
    sort(idHit.begin(),idHit.end(),co);

    // Divide hits per plane if any
    PlaneCondition pc(fCHODEvent);
    auto from = idHit.begin();
    int nV = distance(from,partition(from,idHit.end(),pc));
    if (!nV) return isQ1;
    if (!(fCHODEvent->GetNHits()-nV)) return isQ1;
    vector<int> vP[2];
    vP[0].assign(from,from+nV);
    vP[1].assign(from+nV,idHit.end());

    // Separate hits per quadrant
    vector<int>::iterator fromP[2];
    fromP[0] = vP[0].begin();
    fromP[1] = vP[1].begin();
    int jQ = 0;
    vector<int> vQ[2];
    while ((fromP[0]!=vP[0].end() || fromP[1]!=vP[1].end()) && jQ<4) {
        for (int jP(0); jP<2; jP++) {
            QuadrantCondition qc(fCHODEvent,jQ,jP);
            int nQ = distance(fromP[jP],partition(fromP[jP],vP[jP].end(),qc));
            if (!nQ) continue;
            fvCHODQ[jP][jQ].assign(fromP[jP],fromP[jP]+nQ);
            fromP[jP] += nQ;
        }
        jQ++;
    }

    // Select events with hits in corresponding H-V quadrants
    for (int jq(0); jq<4; jq++) {
        if (fvCHODQ[0][jq].size()&&fvCHODQ[1][jq].size()) isQ1 = true;
    }

    return isQ1;
}

vector<double> K3piTrackAnalysis::CHODTrackMatching(double xPos, double yPos, double ttime) {
    vector<double> par(7);
    double minpar[9];

    // Loop over the quadrant pairs
    double minchi2chod = 99999999.;
    for (int jQ(0); jQ<4; jQ++) {
        if (!fvCHODQ[0][jQ].size() || !fvCHODQ[1][jQ].size()) continue;
        double minchi2 = 99999999.;
        for (auto &iV : fvCHODQ[0][jQ]) {
            TRecoCHODHit *HitV = (TRecoCHODHit*)fCHODEvent->GetHit(iV);
            int counterV = HitV->GetChannelID();
            for (auto &iH : fvCHODQ[1][jQ]) {
                TRecoCHODHit *HitH = (TRecoCHODHit*)fCHODEvent->GetHit(iH);
                int counterH = HitH->GetChannelID();
                //double tVcorr = HitV->GetTime()-CHODTimeCorrection(counterV,(counterH-64)%16,HitV->GetTimeWidth(),0);
                //double tHcorr = HitH->GetTime()-CHODTimeCorrection(counterH,counterV%16,HitH->GetTimeWidth(),0);
                double tVcorr = HitV->GetTime()-CHODTimeCorrection(counterV,(counterH-64)%16,HitV->GetTimeWidth(),fCHODPosV[counterV],fCHODPosH[counterH-64],1);
                double tHcorr = HitH->GetTime()-CHODTimeCorrection(counterH,counterV%16,HitH->GetTimeWidth(),fCHODPosV[counterV],fCHODPosH[counterH-64],1);

                double dtime = 0.5*(tVcorr+tHcorr)-ttime;
                double dist2 = (fCHODPosV[counterV]-xPos)*(fCHODPosV[counterV]-xPos)+(fCHODPosH[counterH-64]-yPos)*(fCHODPosH[counterH-64]-yPos);
                double chi2chod = (tVcorr-tHcorr)*(tVcorr-tHcorr)/(9*3*3)+dtime*dtime/(9*7*7)+dist2/(4*13*13); // Check the weights !
                if (chi2chod<minchi2) {
                    minchi2 = chi2chod;
                    minpar[0] = minchi2;
                    minpar[1] = dist2;
                    minpar[2] = dtime;
                    minpar[3] = (double)counterV;
                    minpar[4] = (double)counterH;
                    minpar[5] = tVcorr;
                    minpar[6] = tHcorr;
                    minpar[7] = fCHODPosV[counterV];//x position
                    minpar[8] = fCHODPosH[counterH-(int)64];//y position

                }
            }
        }
        if (minchi2<minchi2chod) {
            minchi2chod = minchi2;
            par.clear();
            for (int k(0); k<9; k++) par.push_back(minpar[k]);
        }
    }


    return par;
}

double K3piTrackAnalysis::CHODTimeCorrection(int iS, int iP, double tot, double xslab, double yslab, double enslew) {
    //  double t0 = fCHODAllT0[iS][iP]<99. ? fCHODAllT0[iS][iP]-(fCHODAllFineT0[iS][iP]-0.1) : 0.;
    if (fIsMC) {
        if (iS<64) return (fCHODT0[iS]-fCHODVelocity[iS]*(fabs(yslab)+(fCHODLengthV[0]-fCHODLengthV[iS]))+9.);
        if (iS>63) return (fCHODT0[iS]-fCHODVelocity[iS]*(fabs(xslab)+(fCHODLengthV[0]-fCHODLengthH[iS-64]))+9.);
    }
    double t0 = fCHODAllT0[iS][iP]<99. ? fCHODAllT0[iS][iP]-(fCHODAllFineT0[iS][iP]) : 0.;
    double efftot = tot<15. ? tot : 15.;
    double sC = tot>0. ? fCHODAllSlewSlope[iS][iP]*efftot+fCHODAllSlewConst[iS][iP] : 0.;
    return t0+enslew*sC;

}

// double K3piTrackAnalysis::CHODTimeCorrection(int iS, int iP, double tot, double enslew) {
//   double t0 = fCHODAllT0[iS][iP]<99. ? fCHODAllT0[iS][iP]-fCHODAllFineT0[iS][iP] : 0.;
//   double efftot = tot<15. ? tot : 15.;
//   double sC = tot>0. ? fCHODAllSlewSlope[iS][iP]*efftot+fCHODAllSlewConst[iS][iP] : 0.;
//   return t0+enslew*sC;
// }
///////////////////////////
// Nominal Kaon Momentum //
///////////////////////////
void K3piTrackAnalysis::NominalKaon() {
    Double_t pkaon = 75.0;
    Double_t tthx = 0.0012;
    Double_t tthy = 0.;
    if (fYear==2015) {
        pkaon = 74.9;
        tthx = 0.00118; // 3809
        tthy = 0.;
    }
    if (fYear==2016) {
        pkaon = 74.9;
        tthx = 0.00122;
        tthy = 0.000026;
    }
    Double_t pz = pkaon/sqrt(1.+tthx*tthx+tthy*tthy);
    Double_t px = pz*tthx;
    Double_t py = pz*tthy;
    fKaonNominalMomentum.SetXYZM(px,py,pz,0.001*Constants::MKCH);
}

Int_t K3piTrackAnalysis::ParticleID(TString part,int iC){

    int type = 0;

    if(part.EqualTo("pip")) type = 1;
    if(part.EqualTo("pim")) type = 2;

    switch(type){
    case 1: //pi+

      //cout << "PID Piplus" << endl;
        if(fDownstreamTrack[iC].GetLKrEovP()>0.8) return 0;
        break;
    case 2:
      //cout << "PID Piplus" << endl;
        if(fDownstreamTrack[iC].GetLKrEovP()>0.8) return 0;
        break;
    }
    return 1;
}

Int_t K3piTrackAnalysis::GoodMUV3(TString part,Int_t iC,int flag){

  TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(iC);

  TString selection;

  if(flag==1) {
    track = fSPosCand[iC];
    selection="K3pi2";

  }
  if(flag==2){
    track = fKSSpecCand[iC];
    selection="KS";
  }

    TVector3 posmuv3 = fTool->GetPositionAtZ(track,Constants::zMUV3);
    //if(!fMUV3TightCandidate[iC].GetIsMUV3Candidate()) return 0;
    bool isMUV3 = false;

    Double_t reftime  = fCHODCandidate[iC].GetDeltaTime()+track->GetTime();

    for(int i(0); i<fMUV3Event->GetNCandidates();i++){

      TRecoMUV3Candidate* muv3 = (TRecoMUV3Candidate*)fMUV3Event->GetCandidate(i);

      double tmuv3 = muv3->GetTime();
      FillHisto(Form("%s_%s_IsMUV3_dt",selection.Data(),part.Data()),reftime - tmuv3);

      if(fabs(reftime-tmuv3) < 5 ) isMUV3 = true;

    }

    //Double_t reftime  = fRICHCandidate[iC].GetSingleRingTime();

    //double muv3time =fMUV3TightCandidate[iC].GetDeltaTime()+track->GetTime();

    //if(fabs(reftime-muv3time) < 5) {
      //fDownstreamTrack.SetMUV3Time(fMUV3TightCandidate[iC].GetDeltaTime()+track->GetTime());
      ///        fDownstreamTrack.SetIsGoodMUV3(1);
    if(isMUV3) return 1;

    return 0;
        //}

        //return 0;
}
