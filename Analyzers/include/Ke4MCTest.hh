#ifndef KE4MCTEST_HH
#define KE4MCTEST_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class TH1I;
class TH2F;
class TGraph;
class TTree;


class Ke4MCTest : public NA62Analysis::Analyzer
{
public:
  Ke4MCTest(NA62Analysis::Core::BaseAnalysis *ba);
  ~Ke4MCTest();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void ProcessSpecialTriggerUser(int iEvent, unsigned int triggerType);
  void Process(int iEvent);
  void PostProcess();
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void DrawPlot();
protected:

  KinePart* fPiplus;
  KinePart* fEplus;
  KinePart* fNu;
  KinePart* fPiminus;
  KinePart *fKplus;
  KinePart *fg1;
  KinePart *fg2;


};
#endif
