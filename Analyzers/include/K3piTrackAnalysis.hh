#ifndef K3PI_HH
#define K3PI_HH

#include <stdlib.h>
#include <vector>
#include "VertexLSF.hh"
#include "BlueTubeTracker.hh"

#include "Analyzer.hh"
#include "MCSimple.hh"
#include "SpectrometerTrackVertex.hh"
#include <TCanvas.h>
#include "GigaTrackerCandidate.hh"
#include "GigaTrackerAnalysis.hh"
#include "GeometricAcceptance.hh"
#include "RICHCandidate.hh"
#include "RICHAnalysis.hh"
#include "CedarAnal.hh"

#include "StrawRapper.hh"
#include "CHODAnal.hh"
#include "LKrAnal.hh"
//#include "RICHAnal.hh"
#include "MUVAnal.hh"
#include "CHANTIAnal.hh"
#include "SACAnal.hh"
#include "SAVAnal.hh"
#include "IRCAnal.hh"
#include "LAVAnal.hh"
#include "CedarAnal.hh"
#include "MUV3Anal.hh"
#include "GigaTrackerAnalysis.hh"

#include "StrawCandidate.hh"
#include "CHODCandidate.hh"
#include "NewCHODCandidate.hh"
#include "RICHCandidate.hh"
#include "LKrCandidate.hh"
#include "MUV3Candidate.hh"
#include "MUVCandidate.hh"
#include "DownstreamParticle.hh"
#include "UpstreamParticle.hh"
#include "SpectrometerMUV3AssociationOutput.hh"

class LAVMatching;
class SAVMatching;
class AnalysisTools;

class K3piTrackAnalysis : public NA62Analysis::Analyzer
{
public:
  K3piTrackAnalysis(NA62Analysis::Core::BaseAnalysis *ba);
  ~K3piTrackAnalysis();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void PostProcess();
  Bool_t SelectThreeTrackEvent();
  Bool_t SelectTwoTrackEvent();
  Bool_t SelectKS();
  void DrawPlot();
  bool CHODQ1() ;
  void ClearTracks();
  Int_t GoodCHOD(Int_t ntracks, Int_t iC, Int_t flag);
  Int_t GoodRICH(Int_t ntracks, Int_t iC);
  Int_t GoodGTK(Int_t ntracks, Int_t iC);
  void FillUpstream(Int_t iC,TRecoGigaTrackerCandidate* gtkcand, Double_t discr);
  Bool_t InStraw1Acceptance(TVector3 pos);
  vector<double> CHODTrackMatching(double xPos, double yPos, double ttime) ;
  //double CHODTimeCorrection(int iS, int iP, double tot, double enslew) ;
  double CHODTimeCorrection(int iS, int iP, double tot, double xslab, double yslab, double enslew) ;
  Double_t MatchRICHSingle(Int_t itrack, double reftime, TRecoSpectrometerCandidate *thisTrack);
  Int_t MatchLKr(Int_t itrack,Double_t trktime, TVector3 posatlkr);
  Int_t GetQuadrant(Int_t channelid) ;
  Int_t CedarInTime(Double_t time);
  // Int_t GTKAnalysis(Int_t igtk,TVector3 vtxposition);
  Int_t GTKAnalysis(TVector3 vtxposition);
  Int_t GTKMatch(double chodtime,double cedartime);
  Int_t MUV1Match(Double_t trktime, TVector3 posatMUV1);
  Int_t MUV2Match(Double_t trktime, TVector3 posatMUV2);
  Int_t MUV3Match(Double_t trktime, TVector3 posatMUV3);
  void NominalKaon();
  Int_t ParticleID(TString part,int iC);
  Int_t GoodLKr(Int_t ntracks, Int_t iC,TString part,int flag);
  Int_t GoodMUV3(TString part,Int_t iC,int flag);
protected:

  Double_t fMaxNTracks;
  Double_t fMaxNVertices;
  Double_t fMinZVertex;   ///< Lower limit of the Z range for vertices to be saved
  Double_t fMaxZVertex;   ///< Upper limit of the Z range for vertices to be saved
  Double_t fMaxChi2;      ///< Upper limit of vertex chi2 for vertices to be saved

  Double_t fElectronRingRadius;
  Int_t fElectronRingNhits;
  Double_t fCHODTime[3];
  Double_t fRICHTime[3];
  TVector3 fCHODPos[3];
  Double_t fEventTime;
  Double_t fKaonTime;
  Double_t  fXCenter[4];

  Double_t fXAtStraw1;
  Double_t fYAtStraw1;
  Double_t fRAtStraw1;

  Double_t fXAtStraw1_Min;
  Double_t fYAtStraw1_Min;
  Double_t fRAtStraw1_Min;

  Int_t fRun;
  Int_t fYear;
  Int_t fSPosCandID;
  Int_t fNTracks;
  Int_t fControlTrigger;
  Int_t fBurstID;
  Double_t fSPosCandTime;
  Double_t fBurstTime;

  Bool_t fBlueFieldCorrection;
  bool fIsMC;
  bool fIsK3pi;
  bool fIsFilter;

  AnalysisTools* fTool;
  TRecoSpectrometerCandidate* fSPosCand[2];
  TRecoSpectrometerCandidate* fKSSpecCand[2];
  TRecoSpectrometerEvent* fSpectrometerEvent;
  TRecoCHODEvent* fCHODEvent;
  TRecoGigaTrackerEvent* fGigaTrackerEvent;
  TRecoCedarEvent* fCedarEvent;
  TRecoLKrEvent* fLKrEvent;
  TRecoCHANTIEvent* fCHANTIEvent;
  TRecoMUV3Event*             fMUV3Event;
  TRecoMUV2Event*             fMUV2Event;
  TRecoNewCHODEvent*          fNewCHODEvent;
  TRecoMUV1Event*             fMUV1Event;
  TRecoRICHEvent*             fRICHEvent;
  TRecoLAVEvent*              fLAVEvent;
  TRecoSACEvent*              fSACEvent;
  TRecoSAVEvent*              fSAVEvent;
  TRecoIRCEvent*              fIRCEvent;

  StrawRapper *fStrawAnalysis;
  RICHAnalysis *fRICHAnal;
  CedarAnal *fCedarAnal;
  LKrAnal *fLKrAnal;
  CHODAnal *fCHODAnal;
  RICHCandidate *fRICHCand;

  std::vector<SpectrometerMUV3AssociationOutput> fMUV3Candidate;
  Double_t fSegmentIB;
  Double_t fcda;
  TVector3 fTwoTrackVertex;
  //Instances of physics Objects
  StrawCandidate *fStrawCandidate;
  CHODCandidate *fCHODCandidate;
  NewCHODCandidate *fNewCHODCandidate;
  LKrCandidate *fLKrClusCandidate;
  LKrCandidate *fLKrCellCandidate;
  //MUV3Candidate *fMUV3Candidate;
  //RICHAnal *fRICHAnal;
  RICHCandidate *fRICHCandidate;
  //RICHCandidate *fRiMultCand;
  //RICHCandidate *fRiSingCand;
  MUVCandidate *fMUV1Candidate;
  MUVCandidate *fMUV2Candidate;
  //MUV3Candidate *fMUV3TightCandidate;
  //MUV3Candidate *fMUV3LooseCandidate;

  KTAGCandidate fCedarCandidate[2];
  KTAGCandidate fKSCedarCandidate[2];
  // DownstreamParticle fDownstreamCandidate[10];
  DownstreamParticle fDownstreamTrack[10];
  UpstreamParticle fUpstreamTrack[10];

  VertexLSF fVertexLSF;
  std::vector<SpectrometerTrackVertex> fVertexContainer;

  void BuildVertex(Int_t ind[], Int_t NTracks);
  TLorentzVector fK4Momentum;
  TVector3 fK3Momentum;
  TLorentzVector fKaonNominalMomentum;
  TLorentzVector fKS4Mom;
  TLorentzVector fKSPiPlusMom;
  TLorentzVector fKSPiMinusMom;
  TVector3 fKSTwoTrackVertex;
  double fKSMMiss;
  double fKScda;

  double fCHODPosV[64];
  double fCHODPosH[64];
  double fCHODAllSlewSlope[128][16];
  double fCHODAllSlewConst[128][16];
  double fCHODAllT0[128][16];
  double fCHODAllFineT0[128][16];
  vector<int> fvCHODQ[2][4];

  double fCHODLengthV[128];
  double fCHODLengthH[128];
  double fCHODT0[128];
  double fCHODVelocity[128];

  LAVMatching* fLAVMatch ;
  SAVMatching* fSAVMatch ;

  GigaTrackerAnalysis *fGigaTrackerAnalysis;

  GeometricAcceptance* fGeo;

protected:
  struct TrackQualityCondition{
    TRecoSpectrometerEvent* fevent;
    int fjT;
    TrackQualityCondition(TRecoSpectrometerEvent* a, int k) : fevent(a),fjT(k) {};
    bool operator() ( int i ){
      TRecoSpectrometerCandidate *pT = (TRecoSpectrometerCandidate *)fevent->GetCandidate(i);
      bool goodT = true;
      if (i==fjT) goodT = false;
      if (pT->GetNChambers()<4) goodT = false;
      if (pT->GetChi2()>30) goodT = false;
      return goodT;
    }
  };
  struct ChannelOrder{
    TRecoCHODEvent* fevent;
    ChannelOrder(TRecoCHODEvent* a) : fevent(a) {};
    bool operator() ( int i, int j ){
      TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
      TRecoCHODHit* hj = (TRecoCHODHit*) fevent->GetHit(j);
      return hi->GetChannelID()<hj->GetChannelID();
    }
  };
  struct PlaneCondition{
    TRecoCHODEvent* fevent;
    PlaneCondition(TRecoCHODEvent* a) : fevent(a) {};
    bool operator() ( int i ){
      TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
      return hi->GetChannelID()<=63;
    }
  };
  struct QuadrantCondition{
    TRecoCHODEvent* fevent;
    int fQ;
    int fP;
    QuadrantCondition(TRecoCHODEvent* a, int q, int p) : fevent(a),fQ(q),fP(p) {};
    bool operator() ( int i ){
      TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
      return ((hi->GetChannelID()>=16*fQ+64*fP)&&(hi->GetChannelID()<16*(fQ+1)+64*fP));
    }
  };

};
#endif
