#ifndef Kl3Analysis_HH
#define Kl3Analysis_HH

#include "Analyzer.hh"
#include "SpectrometerTrackVertex.hh"
#include "GeometricAcceptance.hh"
#include "TwoLinesCDA.hh"
#include "LAVMatching.hh"
#include "SAVMatching.hh"
#include <stdlib.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include "GigaTrackerAnalysis.hh"

#include "SpectrometerNewCHODAssociation.hh"
#include "SpectrometerLKrAssociation.hh"
#include "SpectrometerMUV3Association.hh"
#include "SpectrometerCalorimetersAssociation.hh"

#include "CalorimeterCluster.hh"

class Kl3Analysis : public NA62Analysis::Analyzer {

public:
  Kl3Analysis(NA62Analysis::Core::BaseAnalysis *ba);
  ~Kl3Analysis();
  void InitHist();
  void InitOutput();
  void DefineMCSimple() {}
  void Process(Int_t);
  void PostProcess() {}
  void StartOfBurstUser();
  void EndOfBurstUser() {}
  void StartOfRunUser() {}
  void EndOfRunUser() {}
  void EndOfJobUser();
  void DrawPlot() {}

  void SelectTracks();
  int MatchGTK(double cedarTime);

  int SelectKe3Candidates();
  int SelectKmu3Candidates();
  int SelectK2piCandidates();

protected:
  TRecoCedarEvent*  cedarEvent;
  TRecoGigaTrackerEvent*  gigatrackerEvent;
  TRecoCHANTIEvent* chantiEvent;
  TRecoSpectrometerEvent* spectrometerEvent;
  TRecoCHODEvent* chodEvent;
  TRecoNewCHODEvent* newCHODEvent;
  TRecoLKrEvent* lkrEvent;
  TRecoMUV0Event* muv1Event;
  TRecoMUV0Event* muv2event;
  TRecoMUV0Event* muv3event;

  std::vector<SpectrometerCHODAssociationOutput> spectrometerCHODAssociation;
  std::vector<SpectrometerNewCHODAssociationOutput> spectrometerNewCHODAssociation;
  std::vector<SpectrometerLKrAssociationOutput> spectrometerLKrAssociation;
  std::vector<SpectrometerMUV3AssociationOutput> spectrometerMUV3Association;

  Int_t nTracks;
  vector <int> selectedTracks;
  Int_t nSelectedTracks;

  

  TRecoSpectrometerCandidate* selectedTrack;
  double pOneTrack;

  int bestGTKCandidateIndex;

  // Particles

  TLorentzVector gtkKaon;
  TLorentzVector beamKaon;
  TLorentzVector chargedPion;
  TLorentzVector muon;
  TLorentzVector positron;

  // mm2
  double mm2BeamKaonChargedPion;
  double mm2GTKKaonChargedPion;

  double mm2BeamKaonMuon;
  double mm2GTKKaonMuon;

  double mm2BeamKaonPositron;
  double mm2GTKKaonPositron;

  int nMUV3Candidates;
  int nAssociatedMUV3Candidates;

  SpectrometerMUV3AssociationOutput spectrometerMUV3AssociationOutput;

  CalorimeterCluster *calorimeterCluster;

  double muv1Energy;
  double muv2Energy; 
private:

  Bool_t isControlData;
  Bool_t isPhysicsData;
  Bool_t isMC;
  Bool_t isFilteredData;
//double gosho;
  Int_t controlEventsPerBurst;

  LAVMatching  *fLAVMatching;
  SAVMatching  *fSAVMatching;
  Double_t     fMaxNBursts;  ///< Number of bins in the histograms of counts vs burst ID, default = 5000

  GigaTrackerAnalysis *myGigaTrackerAnalysis;
};
#endif

