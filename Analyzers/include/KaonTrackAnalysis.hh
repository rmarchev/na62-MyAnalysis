#ifndef KAONTRACKANALYSIS_HH
#define KAONTRACKANALYSIS_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
#include "DownstreamParticle.hh"
#include "UpstreamParticle.hh"
#include "TMinuit.h"

#include "StrawRapper.hh"
#include "CHODAnal.hh"
#include "LKrAnal.hh"
//#include "RICHAnal.hh"
#include "MUVAnal.hh"
#include "CHANTIAnal.hh"
#include "SACAnal.hh"
#include "SAVAnal.hh"
#include "IRCAnal.hh"
#include "LAVAnal.hh"
#include "CedarAnal.hh"
#include "MUV3Anal.hh"
#include "GigaTrackerAnalysis.hh"

#include "StrawCandidate.hh"
#include "CHODCandidate.hh"
#include "RICHCandidate.hh"
#include "LKrCandidate.hh"
#include "MUV3Candidate.hh"
#include "MUVCandidate.hh"

class TH1I;

class TH2F;
class TGraph;
class TTree;
class MultiTrackAnal;

class KaonTrackAnalysis : public NA62Analysis::Analyzer
{
public:
  KaonTrackAnalysis(NA62Analysis::Core::BaseAnalysis *ba);
  ~KaonTrackAnalysis();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void PostProcess();
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void DrawPlot();

  bool OneParticleEvent(Int_t flag) ;

  void L0TriggerEfficiency();
  Int_t PositronDiscriminant();
  Int_t MuonDiscriminant(Int_t flag);
  Int_t MuonDiscriminantRicc(Int_t flag) ;
  Int_t PionDiscriminant();
  //Int_t PionPID();
  Int_t PionPID(Bool_t flag);
  Int_t PionPIDNew();
  void PIDStudy(Int_t flag);
  void RICHPIDStudy(Int_t flag);
  //Int_t RICHSingle(Int_t flag);
  Int_t FineTiming(Int_t flag);
  Bool_t Kmu2PhotonRandomVeto();
  void Kmu2NewPhotonRandomVeto();
  Int_t SelectK2piForRLKrStudy();
  Int_t SelectK2piForMultRV();
  //void MultiplicityStudy(Int_t flag);
  void KinematicsStudy(Int_t flag);
  Int_t TestEventsInSignalRegion();
  Int_t CaloTestsBeforePhotonRejection();
  Int_t CaloTestsAfterPhotonRejection();
  void TestMultiplicity();
    bool ExtrapolateBack(Int_t region, bool ischanti,bool flag);
  Int_t PlotKe4();
    Int_t PlotKmu2();
  Int_t AnalyzeTrigger(int flag);
  Int_t L0WithNewCHOD();
  Int_t L0WithRICH();
  Int_t RICHL0Eff(Int_t &L0RICH);
  long long GetSlotTime(long long TimeStamp, Int_t FineTime, Int_t ReferenceFineTime, Int_t iTrigSlot, Int_t BitFineTime);

  //Bool_t MultiTrackSuppression(Int_t flag);
  Double_t MMissTest(double mmass);
  void NominalKaon();

  Int_t SelectTrigger(int triggerType, int type, int mask);


  static void FuncCDA(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t iflag) ;
  Double_t BeamCDA(Double_t zvtx);
  Bool_t BeamKinematics(Int_t flag);
  Bool_t BeamBackgroundCut(Int_t flag, bool ctrl);
  Bool_t KinematicTails(Int_t flag) ;
  Bool_t EarlyDecay(Int_t flag) ;
  void ClearAllPointers();

  Int_t SelectOneParticle();
  Bool_t Kmu2Analysis();
  Bool_t K2piAnalysis();
  Bool_t Ke3Analysis();
  void K2piControlTriggerAnalysis();
  void K2piPinunuTriggerAnalysis();
  Bool_t PinunuAnalysis();
    Bool_t PinunuAnalysisCtrl();
    Bool_t PinunuAnalysisNeg();


protected:
  StrawRapper *fStrawAnalysis;
  LKrAnal* fLKrAnal;
  CHODAnal* fCHODAnal;
protected:
  TRecoSpectrometerEvent *fSpectrometerEvent;
  TRecoCHODEvent *fCHODEvent;
  TRecoNewCHODEvent *fNewCHODEvent;
  TRecoCedarEvent *fCedarEvent;
  TRecoRICHEvent *fRICHEvent;
  TRecoLKrEvent *fLKrEvent;
  TRecoMUV0Event *fMUV0Event;
  TRecoMUV1Event *fMUV1Event;
  TRecoMUV2Event *fMUV2Event;
  TRecoMUV3Event *fMUV3Event;
  TRecoLAVEvent *fLAVEvent;
  TRecoIRCEvent *fIRCEvent;
  TRecoSACEvent *fSACEvent;
  TRecoSAVEvent *fSAVEvent;
  TRecoCHANTIEvent *fCHANTIEvent;
  TRecoHACEvent *fHACEvent;
  TRecoGigaTrackerEvent *fGigaTrackerEvent;
  EventHeader *fHeader;
  L0TPData *fL0Data;
  Int_t fL0DataType;
  Int_t fL0TriggerWord;
  Bool_t fPhysicsData;
  Bool_t fControlTrigger;
  Bool_t fPinunuTrigger;
  AnalysisTools *fTool;
  OutputState fState;
  Int_t fGTKFlag;
  Int_t fYear;
  Int_t fRun;
  Double_t fTrimTheta;
  Double_t fTrimKick;
  Double_t fThetaXCenter;
  Double_t fThetaYCenter;
  Double_t fZVertexCut;
  Double_t fFineTimeScale;
  Double_t fFineTriggerTime;
  //TriggerAnalysis *fTriggerAnalysis;

private:
  bool fIsFilter;
  bool fIsMC;
    bool fisSnakes;
    bool fisEarlyVtx;
    double fProtonMMiss;
  Int_t fNTracks;
  Int_t fIdTrack;
  DownstreamParticle *fDownstreamTrack;
  UpstreamParticle *fUpstreamTrack;
  StrawCandidate *fStrawCandidate;
  TLorentzVector fKaonMomentum;
  TLorentzVector fKaonNominalMomentum;
  DownstreamParticle *fTrack;
  UpstreamParticle *fBeamTrack;
  TClonesArray* fCaloArray;
  TVector3 fVertex;
  Double_t fMMiss;
  Double_t fMMissRICH;
  TLorentzVector fRICHp    ;
  Double_t fSRingMass;
  Double_t fMRingMass;
  Int_t fNRings   ;
  Double_t fMmuMiss;
  Double_t fMelMiss;
  Double_t fPTrack;
  Double_t fPBeamTrack;
  Double_t fRAtStraw1;
  Double_t fXAtStraw1;
  Double_t fYAtStraw1;
  Double_t fXAtLKr;
  Double_t fYAtLKr;
  Double_t fcda;
  Double_t fdtrich;
  Double_t fdtcedar;
    Double_t fdtchod;

  Double_t fpiLH;
  Double_t fmuLH;
  Double_t felLH;
  Double_t fRICHStatus;
  Double_t fSegmentIB;
  Double_t fIsRICHPionLH;
  Double_t fIsRICHPionSumLH;
  Bool_t fIsRICHSingleRing;
    Bool_t fIsRICHGoodSingleRing;
  Bool_t fIsRICHMultiRing;
  Bool_t fIsRICHSingleRingSlava;
  Bool_t fIsLKr;
  Bool_t fIsLKrActivity;
  Bool_t fIsNewLKr;
  Bool_t fIsLAV;
  Bool_t fIsIRC;
  Bool_t fIsSAC;
  Bool_t fIsSAV;
  Bool_t fIsHAC;
  Bool_t fIsHACRV;
  Bool_t fIsMUV0;
  Bool_t fIsMUV0RV;
  Bool_t fisK2piEvent;
  Bool_t fisTwoPhoton;

  Bool_t fBRPiPi0;

  LAVMatching* fLAVMatching;
  LAVAnal* fLAVAnal;
  SAVAnal* fSAVAnal;
  SACAnal* fSACAnal;
  IRCAnal* fIRCAnal;
  Bool_t fmck2pig;
  Bool_t fisRICHRef;
  Bool_t fisNewCHODRef;
  Bool_t fPinunuL0Trigger;
  Bool_t fPinunuL0Trigger20ns;
  Bool_t fRICHL0 ;
  Bool_t fNewCHODL0 ;
  Bool_t fLAVL0  ;
  Bool_t fMUV3L0 ;
  Bool_t fQxL0   ;
  Bool_t fUMTC   ;
  Bool_t fCaloL0 ;
  Double_t xSTRAW_station[4];
  Double_t zSTRAW_station[4];

private:
  TString fAnalyzerName;
  Int_t fNPars;
  TMinuit *fFitter;

  MultiTrackAnal* fMultAnal[9];

    KinePart* fKplus;
    KinePart* fPiplus;
    KinePart* fPiminus;
    KinePart* fEplus;
    KinePart* fg1;
    KinePart* fg2;
    KinePart* fMuplus;
    //Counting events burst-by-burst
    int fnnorm,fnpipi,fnpipipi,fnmunu,fnnorm_before;

    //Counting events run-by-run
    double frvph,frvmult,frvnorm;
//MultiTrackAnal* fMultAnalpnn;
  //MultiTrackAnal* fMultAnalmu;
  //MultiTrackAnal* fMultAnalmuctrl;
  //MultiTrackAnal* fMultAnalpictrl;
  //MultiTrackAnal* fMultAnalpi;
};
#endif
