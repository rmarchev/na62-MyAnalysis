#ifndef BEAMTRACKSELECTION_HH
#define BEAMTRACKSELECTION_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
#include "StrawRapper.hh"
#include "CHODAnal.hh"
#include "LKrAnal.hh"
//#include "RICHAnal.hh"
#include "MUVAnal.hh"
#include "CHANTIAnal.hh"
#include "SACAnal.hh"
#include "SAVAnal.hh"
#include "IRCAnal.hh"
#include "LAVAnal.hh"
#include "CedarAnal.hh"
#include "MUV3Anal.hh"
#include "GigaTrackerAnalysis.hh"

#include "DownstreamParticle.hh"
#include "UpstreamParticle.hh"
#include "StrawCandidate.hh"
#include "CHODCandidate.hh"
#include "RICHCandidate.hh"
#include "LKrCandidate.hh"
#include "MUV3Candidate.hh"
#include "MUVCandidate.hh"

#include "TMinuit.h"

class TH1I;
class TH2F;
class TGraph;
class TTree;


class BeamTrackSelection : public NA62Analysis::Analyzer
{
public:

  BeamTrackSelection(NA62Analysis::Core::BaseAnalysis *ba);
  ~BeamTrackSelection();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void PostProcess();
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void DrawPlot();

  Bool_t BeamBackgroundCut(Int_t flag);
  Int_t  MuonDiscriminantRicc(Int_t flag);
  Int_t  PionPID(Bool_t flag);
  Bool_t EarlyDecay(Int_t flag);
  Bool_t BeamKinematics(Int_t flag);
  Bool_t KinematicTails(Int_t flag);
  static void FuncCDA(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t iflag);
  Double_t BeamCDA(Double_t zvtx);

  Int_t SelectOneParticle();
  Bool_t  IsOTSGoodEvent();
  Int_t PinunuLikeAnalysis();
  Int_t ScatteredPionAnalysis();
  TVector3 NominalKaon();

protected:

  LKrAnal *fLKrAnal;
  CHODAnal *fCHODAnal;
  StrawRapper *fStrawRapper;
  TRecoSpectrometerEvent *fSpectrometerEvent;
  TRecoCHODEvent *fCHODEvent;
  TRecoHACEvent *fHACEvent;
  TRecoNewCHODEvent *fNewCHODEvent;
  TRecoCedarEvent *fCedarEvent;
  TRecoRICHEvent *fRICHEvent;
  TRecoLKrEvent *fLKrEvent;
  TRecoMUV0Event *fMUV0Event;
  TRecoMUV1Event *fMUV1Event;
  TRecoMUV2Event *fMUV2Event;
  TRecoMUV3Event *fMUV3Event;
  TRecoLAVEvent *fLAVEvent;
  TRecoIRCEvent *fIRCEvent;
  TRecoSACEvent *fSACEvent;
  TRecoCHANTIEvent *fCHANTIEvent;
  TRecoGigaTrackerEvent *fGigaTrackerEvent;
  EventHeader *fHeader;
  L0TPData *fL0Data;

  Int_t fL0DataType;
  Int_t fL0TriggerWord;
  Bool_t fPhysicsData;
  Bool_t fControlTrigger;
  Bool_t fPinunuTrigger;
  AnalysisTools *fTool;
  OutputState fState;
  Int_t fGTKFlag;
  Int_t fYear;
  Double_t fTrimTheta;
  Double_t fTrimKick;
  Double_t fThetaXCenter;
  Double_t fThetaYCenter;
  Double_t fZVertexCut;
  Double_t fFineTimeScale;
  Double_t fFineTriggerTime;
  Int_t fNBeamPions;
  //TriggerAnalysis *fTriggerAnalysis;


  // private:
  Int_t fNTracks;
  Int_t fIdTrack;
  DownstreamParticle *fDownstreamTrack;
  UpstreamParticle *fUpstreamTrack;
  TLorentzVector fKaonMomentum;
  DownstreamParticle *fTrack;
  UpstreamParticle *fBeamTrack;
  //   TClonesArray* fCaloArray;
  TVector3 fVertex;
  Double_t fcda;
  TLorentzVector fKaonNominalMomentum;
  Double_t fMMiss;
  Double_t fMmuMiss;
  Double_t fPTrack;
  Double_t fPBeamTrack;
  Double_t fXAtStraw1;
  Double_t fYAtStraw1;
  Double_t fXAtLKr;
  Double_t fYAtLKr;
  Bool_t fIsLKr;
  Bool_t fIsNewLKr;
  Bool_t fIsLAV;
  Bool_t fIsIRC;
  Bool_t fIsSAC;
  Bool_t fIsSAV;
  Bool_t fIsMUV0;
  Bool_t fIsHAC;
  Double_t fSegmentIB;
  //   Bool_t fisK2piEvent;
  //   Bool_t fisTwoPhoton;

  Double_t xSTRAW_station[4];
  Double_t zSTRAW_station[4];


private:
  Int_t fNPars;
  TMinuit *fFitter;


};
#endif
