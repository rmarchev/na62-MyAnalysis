#ifndef MAINANALYSIS_HH
#define MAINANALYSIS_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
#include "GigaTrackerCandidate.hh"
#include "DownstreamParticle.hh"
#include "UpstreamParticle.hh"

#include "AnalysisTools.hh"
#include "StrawRapper.hh"
#include "CHODAnal.hh"
#include "LKrAnal.hh"
//#include "RICHAnal.hh"
#include "MUVAnal.hh"
#include "CHANTIAnal.hh"
#include "SACAnal.hh"
#include "SAVAnal.hh"
#include "IRCAnal.hh"
#include "LAVAnal.hh"
#include "CedarAnal.hh"
#include "MUV3Anal.hh"
#include "GigaTrackerAnalysis.hh"

#include "StrawCandidate.hh"
#include "CHODCandidate.hh"
#include "NewCHODCandidate.hh"
#include "RICHCandidate.hh"
#include "LKrCandidate.hh"
#include "MUV3Candidate.hh"
#include "MUVCandidate.hh"
#include "SpectrometerMUV3AssociationOutput.hh"

class TH1I;
class TH2F;
class TGraph;
class TTree;
class AnalysisTools;
class Particle;
// class RawHeader;
class EventHeader;
class UpstreamParticle;
class DownstreamParticle;
class CalorimeterCluster;
class LAVMatching;
class SAVMatching;

class MainAnalysis : public NA62Analysis::Analyzer
{
public:
  MainAnalysis(NA62Analysis::Core::BaseAnalysis *ba);
  ~MainAnalysis();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void PostProcess();
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void DrawPlot();
  void ClearEvent();
  void ClearUpstreamTrack();

  Bool_t Analyze();
  Bool_t PreSelectOneTrackEvent();
  Bool_t SelectOneTrackEvent();
  Bool_t GeometricalAcceptance(Int_t iC,TRecoSpectrometerCandidate* track);
  Int_t GoodCHODCandidate(Int_t iC,TRecoSpectrometerCandidate* track);
  Int_t GoodNewCHODCandidate(Int_t iC,TRecoSpectrometerCandidate* track);
  // Int_t GoodLKrCandidate(Int_t iC,TRecoSpectrometerCandidate* track);
  Int_t GoodLKrCandidate(Int_t iC,TRecoSpectrometerCandidate* track,CalorimeterCluster* clus);
  Int_t GoodRICHSingleCandidate(Int_t iC,TRecoSpectrometerCandidate* track);
  Int_t GoodRICHMultiCandidate(Int_t iC,TRecoSpectrometerCandidate* track);
  Int_t GoodMUV3Candidate(Int_t iC,TRecoSpectrometerCandidate* track);
  Int_t GoodStandardMUV3Candidate(Int_t iC,TRecoSpectrometerCandidate* track);
  Int_t MakeDownstreamTime(Int_t iC,TRecoSpectrometerCandidate* track,bool isrich);
  Int_t MatchUpstreamKaon(Int_t iC);
  Int_t GoodCedarCandidate(Int_t NoGTKflag,Int_t iC);
  Int_t GoodCHANTICandidate(Int_t IsGTKflag,Int_t iC);
  Int_t GoodGTKCandidate(Int_t chodorcedar,Int_t iC);
  Bool_t GoodLAVCandidate(Int_t iC);
  Bool_t GoodIRCCandidate(Int_t iC);
  Bool_t GoodSACCandidate(Int_t iC);
  Bool_t GoodSAVCandidate(Int_t iC);
  Bool_t GoodLKrExtraCandidate(Int_t iC);

  void BlueFieldMomentumCorrection(int iC);
  void StoreUpstreamParameters( Int_t iTr, TRecoGigaTrackerCandidate* gtkcand, Double_t discr);
  //void BuildCalorimetricEnergy(Int_t iC);
  void BuildCalorimetricEnergy(Int_t iC, CalorimeterCluster* clus);
    Int_t SelectTrigger(int triggerType, int type, int mask) ;
protected:
    L0TPData *fL0Data;
    Int_t fL0DataType;
    Int_t fL0TriggerWord;
    Bool_t fPhysicsData;
    Bool_t fControlTrigger;

  AnalysisTools* fTool;
  // RawHeader* fHeader;
  EventHeader* fHeader;
  TRecoSpectrometerEvent* fSpectrometerEvent;
  TRecoCHODEvent *fCHODEvent;
  TRecoNewCHODEvent *fNewCHODEvent;
  TRecoRICHEvent *fRICHEvent;
  TRecoLKrEvent *fLKrEvent;
  TRecoMUV1Event *fMUV1Event;
  TRecoMUV2Event *fMUV2Event;
  TRecoMUV3Event *fMUV3Event;
  TRecoLAVEvent *fLAVEvent;
  TRecoSAVEvent *fSAVEvent;
  TRecoIRCEvent *fIRCEvent;
  TRecoSACEvent *fSACEvent;
  TRecoCHANTIEvent *fCHANTIEvent;
  TRecoCedarEvent *fCedarEvent;
  TRecoGigaTrackerEvent *fGigaTrackerEvent;
  //TClonesArray fCaloArray;
  //inline void UpdateTrackCounter() {fTrackCounter++;};

  Int_t fTriggerFlag;
  Int_t fFineTimeScale;
  Int_t fFineTriggerTime;
  Int_t fGTKFlag;
  Int_t fNTracks;
  Int_t fNBeamPions;
  Int_t fNGoodTracks;
  Int_t fNK3piTracks;
  Int_t fNGoodKaonTracks;
  Int_t fBadEvent;
  Int_t fIsGoodOneTrackEvent;
  Int_t fIsGoodK3piEvent;
  Int_t fYear;
  Int_t fRun;
  Int_t fBurst;

  double fTCorr_ktag[2];
  double fTCorr_gtk[2];
  double fTGlobal;

  //TClonesArray* fCaloArray;
  StrawRapper* fStrawRapper;
  CHODAnal* fCHODAnal;
  LKrAnal *fLKrAnal;
  SACAnal *fSACAnal;
  SAVAnal *fSAVAnal;
  IRCAnal *fIRCAnal;
  LAVAnal *fLAVAnal;
  MUVAnal *fMUVAnal;
  CHANTIAnal *fCHANTIAnal;
  MUV3Anal *fMUV3Anal;
  CedarAnal *fCedarAnal;
  GigaTrackerAnalysis *fGigaTrackerAnalysis;
  GigaTrackerAnalysis *fGigaTrackerAnalysisCHOD;

  std::vector<SpectrometerMUV3AssociationOutput> fMUV3Candidate;
  //Instances of physics Objects
  StrawCandidate *fStrawCandidate;
  CHODCandidate *fCHODCandidate;
  NewCHODCandidate *fNewCHODCandidate;
  LKrCandidate *fLKrClusCandidate;
  LKrCandidate *fLKrCellCandidate;
  //MUV3Candidate *fMUV3Candidate;
  //RICHAnal *fRICHAnal;
  RICHCandidate *fRICHCandidate;
  //RICHCandidate *fRiMultCand;
  //RICHCandidate *fRiSingCand;
  MUVCandidate *fMUV1Candidate;
  MUVCandidate *fMUV2Candidate;
  MUV3Candidate *fMUV3TightCandidate;
  MUV3Candidate *fMUV3LooseCandidate;

  DownstreamParticle fDownstreamCandidate[10];
  UpstreamParticle fUpstreamCandidate[10];
  DownstreamParticle fKaonEventDownstreamCandidate[10];
  UpstreamParticle fKaonEventUpstreamCandidate[10];
  DownstreamParticle fBeamPionDownstreamCandidate[10];
  UpstreamParticle fBeamPionUpstreamCandidate[10];



private: // Common to each track
  DownstreamParticle fDownstreamTrack;
  UpstreamParticle fUpstreamTrack;
  TVector3 fVertex;
  Double_t fcda;
  Double_t fdiscr_2;
  Double_t fdiscr_3;
  Double_t fM2Miss;
  Double_t fElectronRingRadius;
  Double_t fElectronRingNhits;
  bool fIsFilter;
  bool fIsMC;
  int fNewBurst;
  LAVMatching* fLAVMatching;
  SAVMatching* fSAVMatching;
};
#endif
