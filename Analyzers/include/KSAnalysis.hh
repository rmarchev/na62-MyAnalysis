#ifndef KSANALYSIS_HH
#define KSANALYSIS_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

#include "StrawRapper.hh"
#include "CHODAnal.hh"
#include "LKrAnal.hh"

#include "StrawCandidate.hh"
#include "CHODCandidate.hh"
#include "NewCHODCandidate.hh"
#include "RICHCandidate.hh"
#include "LKrCandidate.hh"
#include "MUV3Candidate.hh"
#include "MUVCandidate.hh"
#include "SpectrometerMUV3AssociationOutput.hh"

class TH1I;
class TH2F;
class TGraph;
class TTree;
class AnalysisTools;

class KSAnalysis : public NA62Analysis::Analyzer
{
public:
    KSAnalysis(NA62Analysis::Core::BaseAnalysis *ba);
    ~KSAnalysis();
    void InitHist();
    void InitOutput();
    void DefineMCSimple();
    void ProcessSpecialTriggerUser(int iEvent, unsigned int triggerType);
    void Process(int iEvent);
    void PostProcess();
    void StartOfBurstUser();
    void EndOfBurstUser();
    void StartOfRunUser();
    void EndOfRunUser();
    void EndOfJobUser();
    void DrawPlot();
    Int_t TrueInfoAnalysis(Int_t flag);
    Bool_t PreselectTwoTracks(Int_t flag);
    Bool_t OneTrackAnalysis(Int_t flag);
    Bool_t TwoTrackAnalysis(Int_t flag);
    Bool_t SelectKS();
    Int_t GoodCHOD(Int_t ntracks, Int_t iC, Int_t flag);
    void NominalKaon();
protected:


  Double_t fMaxNTracks;
  Double_t fMaxNVertices;
  Double_t fMinZVertex;   ///< Lower limit of the Z range for vertices to be saved
  Double_t fMaxZVertex;   ///< Upper limit of the Z range for vertices to be saved
  Double_t fMaxChi2;      ///< Upper limit of vertex chi2 for vertices to be saved

    KinePart* fPiplus;
    KinePart* fEplus;
    KinePart* fNu;
    KinePart* fPiminus;
    KinePart* fMuminus;
    KinePart* fMuplus;
    KinePart *fKS;
    KinePart *fg1;
    KinePart *fg2;

    AnalysisTools* fTool;
    TRecoSpectrometerCandidate* fSPosCand[2];
    TRecoSpectrometerCandidate* fKSSpecCand[2];
    TRecoSpectrometerEvent *fSpectrometerEvent;
    TRecoCHODEvent *fCHODEvent;
    TRecoNewCHODEvent *fNewCHODEvent;
    TRecoRICHEvent *fRICHEvent;
    TRecoLKrEvent *fLKrEvent;
    TRecoMUV0Event *fMUV0Event;
    TRecoMUV1Event *fMUV1Event;
    TRecoMUV2Event *fMUV2Event;
    TRecoMUV3Event *fMUV3Event;
    TRecoLAVEvent *fLAVEvent;
    TRecoIRCEvent *fIRCEvent;
    TRecoSACEvent *fSACEvent;
    TRecoCHANTIEvent *fCHANTIEvent;

    double fmevtogev;
    double xSTRAW_station[4];
    double fPiPlusXAtStraw1;
    double fPiPlusYAtStraw1;
    double fPiMinusXAtStraw1;
    double fPiMinusYAtStraw1;
    TLorentzVector fTruePiPlus;
    TLorentzVector fTruePiMinus;
    TLorentzVector fTrueMuMinus;
    TLorentzVector fTrueMuPlus;
    TLorentzVector fTrueKS;
    TLorentzVector fTwoPions;
    TVector3 fTrueVtx;

    int fNTracks;
    int fNGoodTracks;

    LKrAnal* fLKrAnal;
    CHODAnal* fCHODAnal;
    StrawRapper* fStrawRapper;
    RICHCandidate *fRICHCand;

    std::vector<SpectrometerMUV3AssociationOutput> fMUV3Candidate;
    Double_t fSegmentIB;
    Double_t fcda;
    TVector3 fTwoTrackVertex;
    //Instances of physics Objects
    StrawCandidate *fStrawCandidate;
    CHODCandidate *fCHODCandidate;
    NewCHODCandidate *fNewCHODCandidate;
    LKrCandidate *fLKrClusCandidate;
    LKrCandidate *fLKrCellCandidate;
    RICHCandidate *fRICHCandidate;
    MUVCandidate *fMUV1Candidate;
    MUVCandidate *fMUV2Candidate;


    //void BuildVertex(Int_t ind[], Int_t NTracks);
    TLorentzVector fK4Momentum;
    TVector3 fK3Momentum;
    TLorentzVector fKaonNominalMomentum;
    TLorentzVector fKS4Mom;
    TLorentzVector fKSPiPlusMom;
    TLorentzVector fKSPiMinusMom;
    TVector3 fKSTwoTrackVertex;
    double fKSMMiss;
    double fKScda;


    Double_t fXAtStraw1;
    Double_t fYAtStraw1;
    Double_t fRAtStraw1;

    Double_t fXAtStraw1_Min;
    Double_t fYAtStraw1_Min;
    Double_t fRAtStraw1_Min;

};
#endif
