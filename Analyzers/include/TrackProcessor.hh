#ifndef ONETRACKSELECTION_HH
#define ONETRACKSELECTION_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
#include "AnalysisTools.hh"
#include "StrawRapper.hh"
#include "CHODAnal.hh"
#include "LKrAnal.hh"

#include "RICHAnalysis.hh"
#include "MUVAnal.hh"
#include "MUV3Anal.hh"

#include "StrawCandidate.hh"
#include "CHODCandidate.hh"
#include "NewCHODCandidate.hh"
#include "LKrCandidate.hh"
#include "RICHCandidate.hh"
#include "MUV3Candidate.hh"
#include "MUVCandidate.hh"
#include "SpectrometerNewCHODAssociationOutput.hh"
#include "SpectrometerRICHAssociationOutput.hh"

class TH1I;
class TH2F;
class TGraph;
class TTree;
class GeometricAcceptance;
class AnalysisTools;
//class RICHImprovedRing;
class TRecoSpectrometerCandidate;
class TRecoSpectrometerEvent;
class TRecoCHODEvent;
class TRecoLKrEvent;
//class TRecoRICHEvent;
class TRecoMUV1Event;
class TRecoMUV2Event;
class TRecoMUV3Event;
//class TRecoRICHCandidate;
//class TRecoRICHHit;


class TrackProcessor : public NA62Analysis::Analyzer
{
public:
  TrackProcessor(NA62Analysis::Core::BaseAnalysis *ba);
  ~TrackProcessor();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void PostProcess();
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void DrawPlot();

  Bool_t AnalyzeTracks();
  Bool_t PreselectK3pi();
  Int_t MCPrintouts();
  Int_t SelectTrigger(int triggerType, int type, int mask) ;
  Int_t SelectCHODCandidate(TRecoSpectrometerCandidate *thisTrack);
  Int_t SelectNewCHODCandidate(TRecoSpectrometerCandidate *thisTrack);
  Int_t SelectLKrCandidate(Int_t flag,TRecoSpectrometerCandidate *thisTrack);
  Int_t SelectRICHSingleCandidate(TRecoSpectrometerCandidate *thisTrack);
  Int_t SelectRICHMultiCandidate(Int_t iTrack,TRecoSpectrometerCandidate *thisTrack);
  Bool_t SelectMUV3Candidate(TRecoSpectrometerCandidate *thisTrack);
  Int_t SelectMUVClusterCandidate(Int_t,TRecoSpectrometerCandidate *thisTrack);
protected:

  TRecoSpectrometerEvent* fSpectrometerEvent;
  TRecoCHODEvent *fCHODEvent;
  TRecoRICHEvent *fRICHEvent;
  TRecoLKrEvent *fLKrEvent;
  TRecoMUV1Event *fMUV1Event;
  TRecoMUV2Event *fMUV2Event;
  TRecoMUV3Event *fMUV3Event;

  std::vector<SpectrometerNewCHODAssociationOutput> fSpecNewCHOD;
  std::vector<SpectrometerRICHAssociationOutput> fSpecRICH;
  //std::vector<SpectrometerRICHAssociationOutputSingleRing> fSpecRICHSingleRing;
  inline void SetTrackFakeFlag(Int_t j, Bool_t val) {fTrackFakeFlag[j]=val;};
  inline void UpdateTrackCounter() {fTrackCounter++;};


  GeometricAcceptance* fGeo;
  Int_t fiEvent;
  Int_t fNTracks;
  Int_t fTrackCounter;
  Int_t fBadEvent;
  Int_t fIsGoodTrackEvent;
  Int_t fOneTrackID;
  Bool_t fTrackFakeFlag[20];
  Int_t fYear;
  Int_t fRun;
  bool fIsFilter;
  bool fIsMC;
  bool fIsK3pi;

  bool fmck2pig;
  bool fControlTrigger;
  bool fPinunuTrigger;

  KinePart* fPiplus;
  KinePart* fEplus;
  KinePart* fNu;
  KinePart* fPiminus;
  KinePart *fKplus;
  KinePart *fg1;
  KinePart *fg2;

  AnalysisTools* fTool;
  L0TPData *fL0Data;
  // RawHeader *fHeader;
  EventHeader *fHeader;

  StrawCandidate fStrawCandidate[20];
  inline void SetStrawCandidate(StrawCandidate *cand) {fStrawCandidate[fTrackCounter]=*cand;};

  CHODCandidate fCHODCandidate[20];
  inline void SetCHODCandidate(CHODCandidate *cand) {fCHODCandidate[fTrackCounter]=*cand;};


  NewCHODCandidate fNewCHODCandidate[20];
  inline void SetNewCHODCandidate(NewCHODCandidate *cand) {fNewCHODCandidate[fTrackCounter]=*cand;};

  RICHCandidate fRICHCandidate[20];
  //RICHCandidate fRICHMultiCandidate[20];
  inline void SetRICHCandidate(RICHCandidate *cand) {fRICHCandidate[fTrackCounter]=*cand;};
  //inline void SetRICHMultiCandidate(RICHCandidate *cand) {fRICHMultiCandidate[fTrackCounter]=*cand;};
  //RICHCandidate fRICHSingleCandidate[20];
  //inline void SetRICHSingleCandidate(RICHCandidate *cand) {fRICHSingleCandidate[fTrackCounter]=*cand;};
  LKrCandidate fLKrClusterCandidate[20];
  inline void SetLKrClusterCandidate(LKrCandidate *cand) {fLKrClusterCandidate[fTrackCounter]=*cand;};
  LKrCandidate fLKrCellCandidate[20];
  inline void SetLKrCellCandidate(LKrCandidate *cand) {fLKrCellCandidate[fTrackCounter]=*cand;};

  MUVCandidate fMUV1Candidate[20];
  inline void SetMUV1Candidate(MUVCandidate *cand) {fMUV1Candidate[fTrackCounter]=*cand;};

  MUVCandidate fMUV2Candidate[20];
  inline void SetMUV2Candidate(MUVCandidate *cand) {fMUV2Candidate[fTrackCounter]=*cand;};

  MUV3Candidate fMUV3Candidate[20];
  inline void SetMUV3Candidate(MUV3Candidate *cand) {fMUV3Candidate[fTrackCounter]=*cand;};

  MUV3Candidate fMUV3TightCandidate[20];
  inline void SetMUV3TightCandidate(MUV3Candidate *cand) {fMUV3TightCandidate[fTrackCounter]=*cand;};
  MUV3Candidate fMUV3MediumCandidate[20];
  inline void SetMUV3MediumCandidate(MUV3Candidate *cand) {fMUV3MediumCandidate[fTrackCounter]=*cand;};
  MUV3Candidate fMUV3BroadCandidate[20];
  inline void SetMUV3BroadCandidate(MUV3Candidate *cand) {fMUV3BroadCandidate[fTrackCounter]=*cand;};
  StrawRapper* fStrawRapper;
  CHODAnal* fCHODAnal;
  LKrAnal *fLKrAnal;
  MUVAnal *fMUVAnal;
  MUV3Anal *fMUV3Anal;

  //Instances of physics Objects
  StrawCandidate *fStCand;
  CHODCandidate *fCHODCand;
  NewCHODCandidate *fNewCHODCand;
  LKrCandidate *fLKrClusCand;
  LKrCandidate *fLKrCellCand;
  MUV3Candidate *fMUV3Cand;
  RICHAnalysis *fRICHAnal;
  RICHCandidate *fRICHCand;
  //RICHCandidate *fRiMultCand;
  //RICHCandidate *fRiSingCand;
  MUVCandidate *fMu1Cand;
  MUVCandidate *fMu2Cand;
  MUV3Candidate *fMu3TightCand;
  MUV3Candidate *fMu3MediumCand;
  MUV3Candidate *fMu3BroadCand;
};
#endif
