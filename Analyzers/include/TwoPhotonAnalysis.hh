#ifndef TWOPHOTONEVENT_HH
#define TWOPHOTONEVENT_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "EventHeader.hh"
#include "DetectorAcceptance.hh"
#include "DownstreamParticle.hh"
#include "UpstreamParticle.hh"
#include <TCanvas.h>
#include "LKrAnal.hh"
#include "CHODAnal.hh"
#include "LAVAnal.hh"

//#include "LKrAnalysis.hh"
//#include "CHODAnalysis.hh"
//#include "LAVAnalysis.hh"

class TH1I;
class TH2F;
class TGraph;
class TTree;

class TRecoLKrEvent;
class TRecoCHODEvent;
class TRecoLAVEvent;
class TRecoCedarEvent;
class AnalysisTools;

class TwoPhotonAnalysis : public NA62Analysis::Analyzer
{
public:
  TwoPhotonAnalysis(NA62Analysis::Core::BaseAnalysis *ba);
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void StartOfJobUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot();
  Int_t SelectTrigger(int triggerType,int type,int mask);

protected:
  Int_t fL0DataType;
  Int_t fL0TriggerWord;
  Int_t fPinunuTrigger;
  Int_t fControlTrigger;
  OutputState fState;
  AnalysisTools *tools;
  Bool_t fAnalysisOutput;
  //EventHeader *fEventHeader;
  EventHeader *fEventHeader;
  TRecoLKrEvent *fLKrEvent;
  TRecoCHODEvent *fCHODEvent;
  TRecoLAVEvent *fLAVEvent;
  TRecoCedarEvent *fCedarEvent;
  LKrAnal *fLKrAnalysis;
  LKrCandidate *fLkClusCand;
  LKrCandidate *fLkCellCand;
  CHODAnal *fCHODAnalysis;
  LAVAnal *fLAVAnalysis;
  Double_t fYear;
  Double_t fZStation[4];
  Double_t fXCenter[4];
  L0TPData *fL0Data;

  Bool_t fIsTwoPhotonEvent;
  Bool_t fIsPiP0Event;
  Int_t fIDPhotons1;
  Int_t fIDPhotons2;
  Int_t fDoubletCounter;
  Int_t fNEMDoublets;
  Int_t *fTwoEMClusterID;

  // Pi0
  Int_t fNPi0s;
  TLorentzVector fPi0sMomentum[10];
  Double_t fPi0sTime[10];
  Int_t fPi0sDoubletID[10];
  Double_t fPi0Time;
  TLorentzVector fPi0Momentum;
  TVector3 fPi0Vertex;

  //Output from MainAnalysis
  Int_t fNTracks;
  DownstreamParticle *fDownstreamTrack;
  UpstreamParticle *fUpstreamTrack;

  // Kaon
  TLorentzVector fKaonMomentum;

  // Pi+
  TLorentzVector fPicMomentum;

protected:
  Bool_t Analysis();
  void SelectTriggerType();
  void SelectPi0();
  Bool_t AnalyzeTwoPhotonEvent();
  Bool_t SelectPiP0Event();
  Bool_t SetKaon();
  Bool_t SetPiPlus();
  Bool_t PiPlusCHODHit();
  Bool_t PiPlusLKrCluster();
  Bool_t PiPlusIsolation();
  Bool_t LAVCandidate();
  inline void UpdateDoubletCounter() {fDoubletCounter++;};
  inline void UpdatePi0Counter() {fNPi0s++;};

  // temporary
  Int_t CHODMakeCandidate(Double_t,Double_t,Double_t*);
  Double_t fFineTriggerTime;
  Double_t *fPosV;
  Double_t *fPosH;
  Double_t *fLengthV;
  Double_t *fLengthH;
  Double_t *fT0;
  Double_t *fVelocity;
  Double_t fTimeLAVLKr[12];

};
#endif
