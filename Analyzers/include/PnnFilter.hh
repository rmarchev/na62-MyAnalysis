#ifndef PNNFILTER_HH
#define PNNFILTER_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
#include "EventHeader.hh"

class TH1I;
class TH2F;
class TGraph;
class TTree;

class GeometricAcceptance;
class SpectrometerMUV3AssociationOutput;
class LAVMatching;

class PnnFilter : public NA62Analysis::Analyzer
{
	public:
		PnnFilter(NA62Analysis::Core::BaseAnalysis *ba);
		~PnnFilter();
		void InitHist();
		void InitOutput();
		void DefineMCSimple();
		void ProcessSpecialTriggerUser(int iEvent, unsigned int triggerType);
		void Process(int iEvent);
		void PostProcess();
		void StartOfBurstUser();
		void EndOfBurstUser();
		void StartOfRunUser();
		void EndOfRunUser();
                void EndOfJobUser();
		void DrawPlot();

        protected:
        TRecoCedarEvent *fCedarEvent;
        TRecoGigaTrackerEvent *fGigaTrackerEvent;
        TRecoSpectrometerEvent *fSpectrometerEvent;
        TRecoCHODEvent *fCHODEvent;
        TRecoLKrEvent *fLKrEvent;
        TRecoLAVEvent *fLAVEvent;
        TRecoMUV3Event *fMUV3Event;
        EventHeader *fRawHeader;
        L0TPData *fL0Data;
        int fFilterControl;
        int fCDW;
        GeometricAcceptance *fGeo;
        LAVMatching *fLAVMatching;
        std::vector<SpectrometerMUV3AssociationOutput> fMUV3Candidate; 
        double fCHODPosV[64]; 
        double fCHODPosH[64]; 
        double fCHODAllSlewSlope[128][16];
        double fCHODAllSlewConst[128][16];
        double fCHODAllT0[128][16];
        double fCHODAllFineT0[128][16];

	protected:
        bool MultiTrack(int,double);
        TLorentzVector Get4Momentum(double,double,double,double);
        TVector3 MultiTrackVertex(int,TLorentzVector*,TVector3*,double*); 
        bool CHODQ1(vector<int> *);
        vector<double> CHODTrackMatching(double,double,double,vector<int>*); 
        double CHODTimeCorrection(int,int,double,double);
        bool KTAGTrackMatching(double,double*); 
        bool MUV3TrackMatching(int,double,double,double);
        bool LKrPhotons(double,double,double,double*);
        double LKrCorrectedEnergy(TRecoLKrCandidate*);
        bool LAVPhotons(double);
        bool FilteringCondition();
        bool FilterControl();

        protected:
        struct TrackQualityCondition{
          TRecoSpectrometerEvent* fevent;
          int fjT;
          TrackQualityCondition(TRecoSpectrometerEvent* a, int k) : fevent(a),fjT(k) {};
          bool operator() ( int i ){
            TRecoSpectrometerCandidate *pT = (TRecoSpectrometerCandidate *)fevent->GetCandidate(i);
            bool goodT = true;
            if (i==fjT) goodT = false;
            if (pT->GetNChambers()<4) goodT = false;
            if (pT->GetChi2()>30) goodT = false;
            return goodT;
          }
        };
        struct ChannelOrder{
          TRecoCHODEvent* fevent;
          ChannelOrder(TRecoCHODEvent* a) : fevent(a) {};
          bool operator() ( int i, int j ){
            TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
            TRecoCHODHit* hj = (TRecoCHODHit*) fevent->GetHit(j);
            return hi->GetChannelID()<hj->GetChannelID();
          }
        }; 
        struct PlaneCondition{
          TRecoCHODEvent* fevent;
          PlaneCondition(TRecoCHODEvent* a) : fevent(a) {};
          bool operator() ( int i ){
            TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
            return hi->GetChannelID()<=63;
          }
        }; 
        struct QuadrantCondition{
          TRecoCHODEvent* fevent;
          int fQ;
          int fP;
          QuadrantCondition(TRecoCHODEvent* a, int q, int p) : fevent(a),fQ(q),fP(p) {};
          bool operator() ( int i ){
            TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
            return ((hi->GetChannelID()>=16*fQ+64*fP)&&(hi->GetChannelID()<16*(fQ+1)+64*fP));
          }
        }; 
};

template <typename T>
class make_vector {
public:
  typedef make_vector<T> my_type;
  my_type& operator<< (const T& val) {
    data_.push_back(val);
    return *this;
  }
  operator std::vector<T>() const {
    return data_;
  }
private:
  std::vector<T> data_;
};

#endif
