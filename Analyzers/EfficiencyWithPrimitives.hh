#ifndef EFFICIENCYWITHPRIMITIVES_HH
#define EFFICIENCYWITHPRIMITIVES_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
#include "GeometricAcceptance.hh"
#include "TwoLinesCDA.hh"
#include "LAVMatching.hh"
#include "SAVMatching.hh"


class TH1I;
class TH2F;
class TGraph;
class TTree;
class AnalysisTools;



class EfficiencyWithPrimitives : public NA62Analysis::Analyzer
{
public:
  EfficiencyWithPrimitives(NA62Analysis::Core::BaseAnalysis *ba);
  ~EfficiencyWithPrimitives();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void PostProcess();
  void DrawPlot();
  void EndOfJobUser();
  Int_t FromCellToSuperCell(int a, int b);
  long long GetSlotTime(long long TimeStamp, Int_t FineTime, Int_t ReferenceFineTime, Int_t iTrigSlot, Int_t BitFineTime);
protected:
  

 
  Bool_t SkipEvent;
 


  TString fTrigDetName[7];

  Int_t BitFineTime;

  std::map<pair<int,int>,int>  supercells; 
  typedef struct{
    Double_t Energy;
    Double_t Time;
    Double_t Offset;
    Double_t Multiplicity;
    Int_t Number;
    Int_t Sample;
  } SCStruct;
  SCStruct *SC;
};
#endif
