#ifndef LAVAnalysis_h
#define LAVAnalysis_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoLAVEvent.hh"
#include "LAVMatching.hh"
#include "PhotonVetoCandidate.hh"

class Parameters;
class AnalysisTools;
class PhotonVetoCandidate;

class LAVAnal
{
  public :
    LAVAnal(Int_t,NA62Analysis::Core::BaseAnalysis *);
  virtual ~LAVAnal(){}
    UserMethods *GetUserMethods() { return fUserMethods;};
    void StartBurst(Int_t,Int_t);
    void Clear();
    PhotonVetoCandidate *GetPhotonCandidate(Int_t j) {return fPhotonCandidate[j];};
  bool IsPhotonsInLAV(int nphotons, double ReferenceTime, LAVMatching* pLAVMatching, TRecoLAVEvent* fLAVEvent);
  public:
    Int_t MakeCandidate(Double_t,TRecoLAVEvent*);
  Int_t TrackCompatibility(Double_t,TRecoLAVEvent*);

  private:
    Parameters *fPar;
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    PhotonVetoCandidate *fPhotonCandidate[80];
    Int_t fFlag;

  private:
    Bool_t *fLAVBadChannel;
    Double_t *fLAVChannelT0;
    TString fNameLAVHisto[12];
    Int_t fLAVPriorityMask[16];
    Double_t GetPhi(TRecoLAVHit*);

  private:
};

#endif
