#ifndef PhotonVetoCandidate_h
#define PhotonVetoCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class PhotonVetoCandidate
{
public :
  PhotonVetoCandidate(){}
  virtual ~PhotonVetoCandidate(){}
  void Clear(){   fID = -1;   fTime = -999999.; }

public:
  void SetTime(Double_t val)    {fTime=val;};
  void SetID(Int_t val)         {fID=val;};
  void SetLAVID(Int_t val)         {fLAVID=val;};

  Double_t GetTime()       {return fTime;};
  Double_t GetID()         {return fID;};
  Double_t GetLAVID()         {return fLAVID;};

private:
  Double_t fTime;
  Int_t fID;
  Int_t fLAVID;
};

#endif
