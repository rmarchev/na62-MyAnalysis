#ifndef RICHCandidate_h
#define RICHCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class RICHCandidate
{
public :
  RICHCandidate();
  virtual ~RICHCandidate();
  void Clear();

public:
  void SetIsRICHCandidate(Bool_t val) {fIsRICHCandidate=val;};

  //Single ring variables
  void SetIsRICHSingleRingCandidate(Bool_t val){fIsRICHSingleRingCandidate=val;};
  void SetIsRICHMultiRingCandidate(Bool_t val){fIsRICHMultiRingCandidate=val;};
  void SetIsRICHSingleRingCandidateSlava(Bool_t val){fIsRICHSingleRingCandidateSlava=val;};
  void SetSingleRingRadius(Double_t val)       {fSingleRingRadius=val;};
  void SetSingleRingRadiusErr(Double_t val)    {fSingleRingRadiusErr=val;};
  void SetSingleRingDX(Double_t val)           {fSingleRingDX=val;};
  void SetSingleRingDR(Double_t val)           {fSingleRingDR=val;};
  void SetSingleRingDY(Double_t val)           {fSingleRingDY=val;};
  void SetSingleRingXcenter(Double_t val)      {fSingleRingXcenter=val;};
  void SetSingleRingXcenterErr(Double_t val)   {fSingleRingXcenterErr=val;};
  void SetSingleRingYcenter(Double_t val)      {fSingleRingYcenter=val;};
  void SetSingleRingYcenterErr(Double_t val)   {fSingleRingYcenterErr=val;};
  void SetSingleRingTime(Double_t val)         {fSingleRingTime=val;};
  void SetSingleRingChi2(Double_t val)         {fSingleRingChi2=val;};
  void SetSingleRingNiter(Double_t val)         {fSingleRingNiter=val;};
  void SetSingleRingNHits(Int_t val)           {fSingleRingNHits=val;};
  void SetSingleRingID(Int_t val)              {fSingleRingCandID=val;};


  void SetDiscriminant(Double_t val) {fDiscriminant=val;};
  void SetDX(Double_t val)           {fDX=val;};
  void SetDY(Double_t val)           {fDY=val;};
  void SetXcenter(Double_t val)      {fXcenter=val;};
  void SetYcenter(Double_t val)      {fYcenter=val;};
  void SetRadius(Double_t val)       {fRadius=val;};
  void SetSingleRingDeltaTime(Double_t val)    {fDeltaTime=val;};
  void SetChi2(Double_t val)         {fChi2=val;};
  void SetNHits(Int_t val)           {fNHits=val;};
  void SetNRings(Int_t val)           {fNRings=val;};
  void SetMirrorID(Int_t val)        {fMirrorID=val;};
  void SetID(Int_t val)              {fCandID=val;};
  void SetMultiID(Int_t val)              {fMultiID=val;};
  void SetSingleID(Int_t val)              {fSingleID=val;};
  void SetMostLikelyHypothesis(Int_t val) {fMostLikelyHypothesis=val;};
  void SetIsPion(Double_t val) {fIsRICHPion=val;};
  void SetPosNexp(Double_t val) { fPosNexpectedHits=val;   }
  void SetPiNexp (Double_t val) {fPiNexpectedHits=val;   }
  void SetMuNexp (Double_t val) { fMuNexpectedHits=val;   }

  Bool_t   GetIsRICHCandidate() {return fIsRICHCandidate;};
  Bool_t   GetIsRICHSingleRingCandidate() {return fIsRICHSingleRingCandidate;};
  Bool_t   GetIsRICHMultiRingCandidate() {return fIsRICHMultiRingCandidate;};
  Bool_t   GetIsRICHSingleRingCandidateSlava() {return fIsRICHSingleRingCandidateSlava;};
  Double_t GetIsPion()           {return fIsRICHPion;};
  Double_t GetSingleRingDX()           {return fSingleRingDX;};
  Double_t GetSingleRingDR()           {return fSingleRingDR;};
  Double_t GetSingleRingDY()           {return fSingleRingDY;};
  Double_t GetSingleRingID()           {return fSingleRingCandID;};
  Double_t GetSingleRingXcenter()      {return fSingleRingXcenter;};
  Double_t GetSingleRingXcenterErr()      {return fSingleRingXcenterErr;};
  Double_t GetSingleRingYcenter()      {return fSingleRingYcenter;};
  Double_t GetSingleRingYcenterErr()      {return fSingleRingYcenterErr;};
  Double_t GetSingleRingRadius()       {return fSingleRingRadius;};
  Double_t GetSingleRingRadiusErr()    {return fSingleRingRadiusErr;};
  Double_t GetSingleRingTime()         {return fSingleRingTime;};
  Double_t GetSingleRingChi2()         {return fSingleRingChi2;};
  Double_t GetSingleRingDeltaTime()    {return fDeltaTime;};
  Double_t GetSingleRingNiter()         {return fSingleRingNiter;};
  Int_t    GetSingleRingNHits()        {return fSingleRingNHits;};

  Double_t GetDiscriminant() {return fDiscriminant;};
  Double_t GetDX()           {return fDX;};
  Double_t GetDY()           {return fDY;};
  Double_t GetID()           {return fCandID;};
  Double_t GetSingleID()           {return fSingleID;};
  Double_t GetMultiID()           {return fMultiID;};
  Double_t GetXcenter()      {return fXcenter;};
  Double_t GetYcenter()      {return fYcenter;};
  Double_t GetRadius()       {return fRadius;};
  Double_t GetChi2()         {return fChi2;};
  Int_t GetNHits()           {return fNHits;};
  Int_t GetNRings()           {return fNRings;};
  Int_t GetMirrorID()        {return fMirrorID;};
  Int_t GetMostLikelyHypothesis() {return fMostLikelyHypothesis;};

  Double_t GetPosNexp() {return fPosNexpectedHits;   }
  Double_t GetPiNexp() {return fPiNexpectedHits;   }
  Double_t GetMuNexp() {return fMuNexpectedHits;   }
  Double_t GetRICHMultiMuonLikelihood() {return fRICHMuLikelihood;   }
  Double_t GetRICHMultiPionLikelihood() {return fRICHPiLikelihood;   }
  Double_t GetRICHMultiElectronLikelihood() {return fRICHElectronLikelihood;   }

  void SetRICHMultiMuonLikelihood(Double_t val) {fRICHMuLikelihood = val;   }
  void SetRICHMultiPionLikelihood(Double_t val) {fRICHPiLikelihood = val;   }
  void SetRICHMultiElectronLikelihood(Double_t val) {fRICHElectronLikelihood = val;   }

private:

  Double_t fRICHMuLikelihood;
  Double_t fRICHPiLikelihood;
  Double_t fRICHElectronLikelihood;
  Double_t fIsRICHPion;

  Bool_t fIsRICHSingleRingCandidate;
  Bool_t fIsRICHMultiRingCandidate;
  Bool_t fIsRICHSingleRingCandidateSlava;
  Bool_t fIsRICHCandidate;
  Double_t fDiscriminant;
  Double_t fDX;
  Double_t fDY;
  Double_t fXcenter;
  Double_t fYcenter;
  Double_t fRadius;
  Double_t fCandID;
  Double_t fSingleID;
  Double_t fMultiID;
  Double_t fDeltaTime;
  Double_t fChi2;
  Double_t fPiNexpectedHits;
  Double_t fPosNexpectedHits;
  Double_t fMuNexpectedHits;
  Int_t fNHits;
  Int_t fNRings;
  Int_t fMirrorID;
  Int_t fMostLikelyHypothesis;

  //Single ring
  Double_t fSingleRingDR;
  Double_t fSingleRingDX;
  Double_t fSingleRingDY;
  Double_t fSingleRingXcenter;
  Double_t fSingleRingXcenterErr;
  Double_t fSingleRingYcenter;
  Double_t fSingleRingYcenterErr;
  Double_t fSingleRingRadius;
  Double_t fSingleRingRadiusErr;
  Double_t fSingleRingCandID;
  Double_t fSingleRingTime;
  Double_t fSingleRingChi2;
    Double_t fSingleRingNiter;
  Int_t    fSingleRingNHits;

};

#endif
