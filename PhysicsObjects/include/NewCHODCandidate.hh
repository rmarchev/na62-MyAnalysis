#ifndef NewCHODCandidate_h
#define NewCHODCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class NewCHODCandidate
{
public :
  NewCHODCandidate();
  virtual ~NewCHODCandidate();
  void Clear();

public:
    void SetRecoHitID(int val) {fID=val;};
  void SetDiscriminant(Double_t val) {fDiscriminant=val;};
  void SetDeltaTime(Double_t val) {fDeltaTime=val;};
  void SetX(Double_t val) {fX=val;};
  void SetY(Double_t val) {fY=val;};
  void SetIsNewCHODCandidate(Bool_t val) {fIsNewCHODCandidate=val;};
  Double_t GetDiscriminant() {return fDiscriminant;};
  Double_t GetDeltaTime() {return fDeltaTime;};
int GetRecoHitID() {return fID;};
  Double_t GetX() {return fX;};
  Double_t GetY() {return fY;};
  Bool_t   GetIsNewCHODCandidate() {return fIsNewCHODCandidate;};


  private:
    Double_t fID;
  Double_t fDiscriminant;
  Double_t fDeltaTime;
  Double_t fX;
  Double_t fY;
  Bool_t fIsNewCHODCandidate;

};

#endif
