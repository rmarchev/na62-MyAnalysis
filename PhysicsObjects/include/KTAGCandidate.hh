#ifndef KTAGCandidate_h
#define KTAGCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class KTAGCandidate
{
public :
  KTAGCandidate(){}
  virtual ~KTAGCandidate(){}
  void Clear();

public:
  void SetIsKTAGCandidate(Bool_t val) {fIsKTAGCandidate=val;};
  void SetTime(Double_t val) {fTime=val;};
  void SetID(Int_t val) {fID=val;};

  Bool_t   GetIsKTAGCandidate() {return fIsKTAGCandidate;};
  Double_t GetTime() {return fTime;};
  Int_t GetID() {return fID;};

private:
  Bool_t fIsKTAGCandidate;
  Double_t fTime;
  Int_t fID;
};

#endif
