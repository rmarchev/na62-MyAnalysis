#ifndef IRCAnalysis_h
#define IRCAnalysis_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoIRCEvent.hh"
#include "PhotonVetoCandidate.hh"

class Parameters;
class AnalysisTools;
class PhotonVetoCandidate;

class IRCAnal
{
  public :
    IRCAnal(NA62Analysis::Core::BaseAnalysis *);
  virtual ~IRCAnal(){}
    UserMethods *GetUserMethods() { return fUserMethods;};
    void StartBurst(Int_t,Int_t);
    void Clear();
    PhotonVetoCandidate *GetPhotonCandidate(Int_t j) {return fPhotonCandidate[j];};
    double TimeSlewCorrected(TRecoIRCHit *hit) ;
  public:
    Int_t MakeCandidate(Double_t,TRecoIRCEvent*);

  private:
    Parameters *fPar;
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    PhotonVetoCandidate *fPhotonCandidate[20];

  private:
    Int_t fIRCPriorityMask[16];
    Int_t fYear;
    Int_t fRunID;
    bool fIsMC;
    Double_t fTimeCut;

  private:
};

#endif
