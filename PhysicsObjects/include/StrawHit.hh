#ifndef StrawHit_h
#define StrawHit_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

class StrawHit 
{
  public :
    StrawHit();
    virtual ~StrawHit();
    void Clear();

  public:
   Int_t GetIndex(Int_t j) { return fIndex[j]; };
   void SetIndex(Int_t j, Int_t val) { fIndex[j] = val; };
   int GetPaired(int j) { return fPaired[j]; };
   void SetPaired(int j, int val) { fPaired[j]=val; };

  private:
   Int_t fIndex[500];
   int fPaired[500];
};

#endif
