#ifndef CHANTIAnal_h
#define CHANTIAnal_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoCHANTIEvent.hh"
#include "CHANTICandidate.hh"

class AnalysisTools;

class CHANTIAnal
{
  public :
    CHANTIAnal(NA62Analysis::Core::BaseAnalysis *);
  virtual ~CHANTIAnal(){}
    UserMethods *GetUserMethods() {return fUserMethods;};
    void Clear();
  void SetEvent(TRecoCHANTIEvent* val){ fCHANTIEvent = val;};
    void StartBurst(Int_t,Int_t);
    Int_t TrackMatching(Int_t,Double_t,Double_t,Double_t);
    CHANTICandidate *GetCHANTICandidate() {return fCHANTICandidate;};

  public:

  private:
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    TRecoCHANTIEvent *fCHANTIEvent;
    CHANTICandidate *fCHANTICandidate;

  private:
    Int_t fYear;
    Int_t fRun;
};

#endif
