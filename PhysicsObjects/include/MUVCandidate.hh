#ifndef MUVCandidate_h
#define MUVCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class MUVCandidate 
{
  public :
    MUVCandidate();
    virtual ~MUVCandidate();
    void Clear();

  public:
    void SetIsMUVCandidate(Bool_t val) {fIsMUVCandidate=val;};
    void SetID(Int_t val)              {fID=val;};
    void SetDiscriminant(Double_t val) {fDiscriminant=val;};
    void SetDeltaTime(Double_t val)    {fDeltaTime=val;   };
    void SetX(Double_t val)            {fX=val;           };
    void SetY(Double_t val)            {fY=val;           };
    void SetEnergy(Double_t val)       {fEnergy=val;      };
    void SetNCells(Int_t val)          {fNCells=val;      };
    void SetNMerged(Int_t val)         {fNMerged=val;     };
    void SetEMerged(Double_t val)      {fEMerged=val;     };

    Bool_t GetIsMUVCandidate() {return fIsMUVCandidate;};
    Int_t GetID()              {return fID;};
    Double_t GetDiscriminant() {return fDiscriminant;};
    Double_t GetDeltaTime()    {return fDeltaTime;   };
    Double_t GetX()            {return fX;           };
    Double_t GetY()            {return fY;           };
    Double_t GetEnergy()       {return fEnergy;      };
    Int_t    GetNCells()       {return fNCells;      };
    Int_t    GetNMerged()      {return fNMerged;     };
    Double_t GetEMerged()      {return fEMerged;     };

  private:
    Bool_t fIsMUVCandidate;
    Int_t    fID;
    Double_t fDiscriminant;
    Double_t fDeltaTime;
    Double_t fX;
    Double_t fY;
    Double_t fEnergy;
    Int_t    fNCells;
    Int_t fNMerged;
    Double_t fEMerged;
};

#endif
