#ifndef GigaTrackerCandidate_h
#define GigaTrackerCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class GigaTrackerCandidate
{
  public :
    GigaTrackerCandidate();
    virtual ~GigaTrackerCandidate();
    void Clear();

  public:
    void SetIsGigaTrackerCandidate(Double_t val) {fIsGigaTrackerCandidate=val;};
    void SetMomentum(Double_t val) {fMomentum=val;};
    void SetSlopeXZ(Double_t val) {fSlopeXZ=val;};
    void SetSlopeYZ(Double_t val) {fSlopeYZ=val;};
    void SetTime(Double_t val) {fTime=val;};
    void SetTime1(Double_t val) {fTime1=val;};
    void SetTime2(Double_t val) {fTime2=val;};
    void SetTime3(Double_t val) {fTime3=val;};
    void SetPosition(TVector3 val) {fPosition=val;};
    void SetPosition(Double_t valx, Double_t valy, Double_t valz) {fPosition=TVector3(valx,valy,valz);};
    void SetPosGTK1(TVector3 val) {fPosGTK1=val;};
    void SetPosGTK1(Double_t valx, Double_t valy, Double_t valz) {fPosGTK1=TVector3(valx,valy,valz);};
    void SetPosGTK2(TVector3 val) {fPosGTK2=val;};
    void SetPosGTK2(Double_t valx, Double_t valy, Double_t valz) {fPosGTK1=TVector3(valx,valy,valz);};
    void SetVertex(TVector3 val) {fVertex=val;};
    void SetCDA(Double_t val) {fCDA=val;};
    void SetDiscriminant(Double_t val) {fDiscriminant=val;};
    void SetChi2X(Double_t val) {fChi2X=val;};
    void SetType(Int_t val) {fType=val;};

    Bool_t   GetIsGigaTrackerCandidate() {return fIsGigaTrackerCandidate;};
    Double_t GetMomentum() {return fMomentum;};
    Double_t GetSlopeXZ() {return fSlopeXZ;};
    Double_t GetSlopeYZ() {return fSlopeYZ;};
    Double_t GetTime() {return fTime;};
    Double_t GetTime1() {return fTime1;};
    Double_t GetTime2() {return fTime2;};
    Double_t GetTime3() {return fTime3;};
    TVector3 GetPosition() {return fPosition;};
    TVector3 GetPosGTK1() {return fPosGTK1;};
    TVector3 GetPosGTK2() {return fPosGTK2;};
    TVector3 GetVertex() {return fVertex;};
    Double_t GetCDA() {return fCDA;};
    Double_t GetDiscriminant() {return fDiscriminant;};
    Double_t GetChi2X() {return fChi2X;};
    Int_t GetType() {return fType;};

  private:
    Bool_t fIsGigaTrackerCandidate;
    Double_t fMomentum;
    Double_t fSlopeXZ;
    Double_t fSlopeYZ;
    Double_t fTime;
    Double_t fTime1;
    Double_t fTime2;
    Double_t fTime3;
    TVector3 fPosition;
    TVector3 fPosGTK1;
    TVector3 fPosGTK2;
    TVector3 fVertex;
    Double_t fCDA;
    Double_t fDiscriminant;
    Double_t fChi2X;
    Int_t fType;
};

#endif
