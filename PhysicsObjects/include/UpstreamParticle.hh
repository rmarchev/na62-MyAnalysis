#ifndef UpstreamParticle_h
#define UpstreamParticle_h

#include "MyParticle.hh"

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
#include "GigaTrackerCandidate.hh"
//class Particle;
class GigaTrackerCandidate;

class UpstreamParticle : public MyParticle
{
public :
  UpstreamParticle(){}
  virtual ~UpstreamParticle(){}
  void Clear();

public :

  // Downstream particle
  Int_t GetDownstreamTrackID() {return fDownstreamTrackID;};
  void SetDownstreamTrackID(Int_t val) {fDownstreamTrackID = val;};

  // KTAG
  Bool_t GetIsKTAGCandidate() {return fIsKTAGCandidate;};
  Bool_t GetIsBeamPionCandidate() {return fIsBeamPionCandidate;};
  Int_t GetKTAGID() {return fKTAGID;};
  Double_t GetKTAGTime() {return fKTAGTime;};

  void SetIsBeamPionCandidate(Bool_t val) { fIsBeamPionCandidate=val;};
  void SetIsKTAGCandidate(Bool_t val) {fIsKTAGCandidate=val;};
  void SetKTAGID(Int_t val) {fKTAGID=val;};
  void SetKTAGTime(Double_t val) {fKTAGTime=val;};

  //CHOD
  void SetCHODMult(Int_t val){ fCHODMult = val;}
  Int_t GetCHODMult(){ return fCHODMult;}
  // CHANTI
  Bool_t GetIsCHANTICandidate() {return fIsCHANTICandidate;};
  Int_t GetCHANTIID() {return fCHANTIID;};
  Double_t GetCHANTITime() {return fCHANTITime;};
  void SetIsCHANTICandidate(Bool_t val) {fIsCHANTICandidate=val;};
  void SetCHANTIID(Int_t val) {fCHANTIID=val;};
  void SetCHANTITime(Double_t val) {fCHANTITime=val;};

  //Quality of matching
  Double_t GetIsGoodKTAG   () {return fGoodKTAG;};
  Double_t GetIsGoodGTK   () {return fGoodGTK;};
  Double_t GetIsGoodCHANTI   () {return fGoodCHANTI;};

  void SetIsGoodKTAG   (bool val) { fGoodKTAG   = val;};
  void SetIsGoodGTK(bool val)     { fGoodGTK= val;};
  void SetIsGoodCHANTI (bool val) { fGoodCHANTI = val;};


  // Matching with the downstream track
  TVector3 GetVertex() {return fVertex;};
  Double_t GetCDA() {return fCDA;};
  TLorentzVector GetMomentumAtDecay() {return fMomentumAtDecay;};
  Double_t GetChi2X() {return fChi2X;};
  TVector3 GetPosGTK1() {return fPosGTK1;};
  TVector3 GetPosGTK2() {return fPosGTK2;};
  void SetVertex(TVector3 val) {fVertex=val;};
  void SetCDA(Double_t val) {fCDA=val;};
  void SetMomentumAtDecay(TLorentzVector val) {fMomentumAtDecay=val;};
  void SetChi2X(Double_t val) {fChi2X=val;};
  void SetPosGTK1(TVector3 val) {fPosGTK1=val;};
  void SetPosGTK2(TVector3 val) {fPosGTK2=val;};
    void SetRICHDiscriminant(double val) {fRICHDiscr=val;};
    void SetCHODDiscriminant(double val) {fCHODDiscr=val;};

  // GTK activity
  Int_t GetNGTKCandidates() {return fNGTKCandidates;};
  void SetNGTKCandidates(int val) {fNGTKCandidates=val;};
  GigaTrackerCandidate GetGTKCandidate(int val) {return fGTKCandidate[val];};
  void SetGTKCandidate(int j, GigaTrackerCandidate val) {fGTKCandidate[j]=val;};
  Int_t GetNGTK1Hits() {return fNGTK1Hits;};
  void SetNGTK1Hits(int val) {fNGTK1Hits=val;};
  Int_t GetGTK1Hit(int j) {return fGTK1Hit[j];};
  void SetGTK1Hit(int j, int val) {fGTK1Hit[j]=val;};
  Int_t GetNGTK2Hits() {return fNGTK2Hits;};
  void SetNGTK2Hits(int val) {fNGTK2Hits=val;};
  Int_t GetGTK2Hit(int j) {return fGTK2Hit[j];};
  void SetGTK2Hit(int j, int val) {fGTK2Hit[j]=val;};
  Int_t GetNGTK3Hits() {return fNGTK3Hits;};
  void SetNGTK3Hits(int val) {fNGTK3Hits=val;};
  Int_t GetGTK3Hit(int j) {return fGTK3Hit[j];};
  void SetGTK3Hit(int j, int val) {fGTK3Hit[j]=val;};
    Double_t GetRICHDiscriminant() {return fRICHDiscr;};
    Double_t GetCHODDiscriminant() {return fCHODDiscr;};

protected :
  Int_t fCHODMult;          //CHOD multiplicity

  Int_t fDownstreamTrackID; // ID of the matched downstream track
  Bool_t fIsBeamPionCandidate; // Flag idicating the presence of a beamlike pion out of time with KTAG
  Bool_t fIsKTAGCandidate;  // Flag idicating the presence of a KTAG candidat matching the GTK-track
  Int_t fKTAGID;            // ID of the KTAG candidate matching the GTK-track
  Double_t fKTAGTime;       // Time of the KTAG candidate matching the GTK-track

  Bool_t fIsCHANTICandidate;  // Flag idicating the presence of a CHANTI candidate matching the GTK-track
  Int_t fCHANTIID;            // ID of the CHANTI candidate closest in time to the GTK-track
  Double_t fCHANTITime;       // Time of the CHANTI candidate closest in time to the GTK-track

  TVector3 fVertex;                // Vertex formed with the downstream track
  Double_t fCDA;                   // CDA of the vertex formed with the downstream track
  TLorentzVector fMomentumAtDecay; // 4-Momentum at decay vertex
  Double_t fChi2X;                 // Chi2 in the XZ of the GigaTracker candidate
  TVector3 fPosGTK1;              // 3-Position of the beam particle at GTK1
  TVector3 fPosGTK2;              // 3-Position of the beam particle at GTK2

  Double_t fRICHDiscr;
    Double_t fCHODDiscr;
  Int_t fNGTKCandidates; // Number of GTK candidates.
  GigaTrackerCandidate fGTKCandidate[10];

  bool fGoodKTAG;
  bool fGoodGTK;
  bool fGoodCHANTI;


  int fNGTK1Hits;
  int fGTK1Hit[50];
  int fNGTK2Hits;
  int fGTK2Hit[50];
  int fNGTK3Hits;
  int fGTK3Hit[50];


};

#endif
