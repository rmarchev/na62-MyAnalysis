#ifndef StrawCandidate_h
#define StrawCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class StrawCandidate
{
  public :
    StrawCandidate();
    virtual ~StrawCandidate();
    void Clear();

  public:




  void SetTrackID(Int_t val)      {fTrackID=val;};
  void SetGoodTrack(Bool_t val)   {fGoodTrack=val;};
  void SetCharge(Int_t val)   {fCharge=val;};
  void SetMultiVertex(Bool_t val) {fMultiVertex=val;};
  void SetMultiCDA(double val) {fMultiCDA=val;};
  void SetAcceptance(Int_t val)   {fAcceptance=val;};
  Bool_t Acceptance();
  void SetAcceptanceStraw(Int_t val)   {fAcceptanceStraw=val;};
  void SetAcceptanceCHOD(Int_t val)   {fAcceptanceCHOD=val;};
  void SetAcceptanceNewCHOD(Int_t val)   {fAcceptanceNewCHOD=val;};
  void SetAcceptanceLKr(Int_t val)   {fAcceptanceLKr=val;};
  void SetAcceptanceRICH(Int_t val)   {fAcceptanceRICH= val;};
  void SetAcceptanceMUV1(Int_t val)   {fAcceptanceMUV1= val;};
  void SetAcceptanceMUV2(Int_t val)   {fAcceptanceMUV2= val;};
  void SetAcceptanceMUV3(Int_t val)   {fAcceptanceMUV3= val;};

  Int_t GetTrackID()      {return fTrackID;};
  Bool_t GetGoodTrack()   {return fGoodTrack;};
  Int_t GetCharge()   {return fCharge;};
  Bool_t GetMultiVertex() {return fMultiVertex;};
  double GetMultiCDA() {return fMultiCDA;};
  Int_t GetAcceptance()   {return fAcceptance;};
  Int_t GetAcceptanceStraw()  {return fAcceptanceStraw;};
  Int_t GetAcceptanceCHOD()   {return fAcceptanceCHOD;};
  Int_t GetAcceptanceNewCHOD()   {return fAcceptanceNewCHOD;};
  Int_t GetAcceptanceLKr()    {return fAcceptanceLKr;};
  Int_t GetAcceptanceRICH()   {return fAcceptanceRICH;};
  Int_t GetAcceptanceMUV1()   {return fAcceptanceMUV1;};
  Int_t GetAcceptanceMUV2()   {return fAcceptanceMUV2;};
  Int_t GetAcceptanceMUV3()   {return fAcceptanceMUV3;};

private:
  Int_t fTrackID;
  Bool_t fGoodTrack;
  Int_t fCharge;
  Bool_t fMultiVertex;
  double fMultiCDA;
  Int_t fAcceptance;
  Int_t fAcceptanceStraw;
  Int_t fAcceptanceCHOD;
  Int_t fAcceptanceNewCHOD;
  Int_t fAcceptanceRICH;
  Int_t fAcceptanceLKr;
  Int_t fAcceptanceMUV1;
  Int_t fAcceptanceMUV2;
  Int_t fAcceptanceMUV3;

};

#endif
