#ifndef MyParticle_h
#define MyParticle_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class MyParticle
{
public :
  MyParticle(){}
  virtual ~MyParticle(){}
  //~Particle();
  // void Clear();
  void Clear();

public:

  // virtual Int_t GetTrackID() {return fTrackID;};
  // virtual TLorentzVector GetMomentum() {return fMomentum;};
  // virtual TVector3 GetPosition() {return fPosition;};
  // virtual Double_t GetTime() {return fTime;};
  // virtual Double_t GetQuality() {return fQuality;};
  // virtual Int_t GetType() {return fType;};
  // virtual void SetTrackID(Int_t val) {fTrackID=val;};
  // virtual void SetMomentum(TLorentzVector val) {fMomentum=val;};
  // virtual void SetPosition(TVector3 val) {fPosition=val;};
  // virtual void SetTime(Double_t val) {fTime=val;};
  // virtual void SetQuality(Double_t val) {fQuality=val;};
  // virtual void SetType(Int_t val) {fType=val;};

  Int_t GetTrackID() {return fTrackID;};
  TLorentzVector GetMomentum() {return fMomentum;};
  TVector3 GetPosition() {return fPosition;};
  Double_t GetTime() {return fTime;};
  Double_t GetQuality() {return fQuality;};
  Int_t GetType() {return fType;};
  void SetTrackID(Int_t val) {fTrackID=val;};
  void SetMomentum(TLorentzVector val) {fMomentum=val;};
  void SetPosition(TVector3 val) {fPosition=val;};
  void SetTime(Double_t val) {fTime=val;};
  void SetQuality(Double_t val) {fQuality=val;};
  void SetType(Int_t val) {fType=val;};

protected :
  Int_t fTrackID;             // ID of the track of the TReco***Candidate
  TLorentzVector fMomentum;   // 4-Momentum of the particle: pion hypothesis for the downstream track, kaon hypothesis for the upstream track
  TVector3 fPosition;         // 3-Position of the particle
  Double_t fTime;             // Time of the tracker
  Double_t fQuality;          // Quality of the track
  Int_t fType;                // Type of track

};

#endif
