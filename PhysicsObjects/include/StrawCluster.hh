#ifndef StrawCluster_h
#define StrawCluster_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

class StrawCluster 
{
  public :
    StrawCluster(int,int);
    virtual ~StrawCluster();
    void Clear();

  public:
    int GetChamber() {return fChamber;};
    int GetView() {return fView;};
    int GetNClusters() {return fNClusters;};
    int GetNHits(int j) {return fNHits[j];};
    int GetIndex(int i, int j) {return fIndex[i][j];};
    double GetX(int j) {return fX[j];};
    double GetZ(int j) {return fZ[j];};
    double GetT(int j) {return fT[j];};
    bool GetEdge(int j) {return fEdge[j];};
    double GetQuality(int j) {return fQuality[j];};
    void SetNClusters(int val) {fNClusters=val;};
    void SetNHits(int j, int val) {fNHits[j]=val;};
    void SetIndex(int i, int j, int val) {fIndex[i][j]=val;};
    void SetX(int j, double val) {fX[j]=val;};
    void SetZ(int j, double val) {fZ[j]=val;};
    void SetT(int j, double val) {fT[j]=val;};
    void SetEdge(int j, bool val) {fEdge[j]=val;};
    void SetQuality(int j, double val) {fQuality[j]=val;};

  private:
    int fChamber;
    int fView;
    int fNClusters;
    int fNHits[50];
    int fIndex[3][50];
    double fX[50];
    double fZ[50];
    double fT[50];
    bool fEdge[50];
    double fQuality[50];

};

#endif
