#ifndef LKRANAL_HH_h
#define LKRANAL_HH_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "Parameters.hh"
#include "UserMethods.hh"
#include "TRecoLKrEvent.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "LKrCandidate.hh"
#include "L0TPData.hh"

class AnalysisTools;
class LKrAuxClusterReco;
class LKrAnal
{
public :
  LKrAnal(Int_t,NA62Analysis::Core::BaseAnalysis *);
  virtual ~LKrAnal(){}
  void Clear(Int_t);
  void StartBurst(Int_t,Int_t);
  void SetEvent(TRecoLKrEvent* evt){fLKrEvent = evt;}

  //SetTrackInfo
  void SetTrackPosAtLKr(Double_t x,Double_t y,Double_t z){fTrackAtLKr=TVector3(x,y,z);}
  void SetTrackPosAtLKr(TVector3 val){fTrackAtLKr=val;}
  void SetTrackClPosAtLKr(Double_t x,Double_t y){fTrackClAtLKr=TVector2(x,y);}
  void SetTrackClPosAtLKr(TVector2 val){fTrackClAtLKr=val;}
  void SetTrackTime(Double_t val){fTrackTime=val;}
  void SetTrackCHODTime(Double_t val){fTrackCHODTime=val;}

  //GetTrackInfo
  TVector3 GetTrackPosAtLKr(){return fTrackAtLKr;}
  TVector2 GetTrackClPosAtLKr(){return fTrackClAtLKr;}
  Double_t GetTrackTime(){return fTrackTime;}
  Double_t GetTrackCHODTime(){return fTrackCHODTime;}

  Int_t ClustersLeft(Int_t flag,double mm2) ;
  Int_t PiPlusLKrRadius(double ptrack);
  UserMethods *GetUserMethods() { return fUserMethods;};
  LKrCandidate* GetLKrClusterCandidate(){ return fLKrClusterCandidate;}
  LKrCandidate* GetLKrCellCandidate(){ return fLKrCellCandidate;}

  Int_t TrackClusterMatching(Double_t chodTime, Double_t trackTime, TVector3 posatlkr);
  Int_t TrackCellMatching(Double_t chodTime, Double_t trackTime,  TVector3 posatlkr);

  Int_t ExtraActivity(Double_t reftime, TVector3 at_lkr);
  bool LKrHitInTime(double E, double dt);
  bool LKrClusterInTime( double dt,double E, bool isAux);

  // Photon search for pinunu analysis
  Int_t ExtraClusters(Double_t reftime, TVector2 posmatchclust, TVector3 posatlkr, Int_t eventnumber);
  Int_t ExtraAuxClusters(Double_t reftime,TVector2 posmatchclust, TVector3 posatlkr);
  Bool_t SelectTiming(Double_t dtime, Double_t energy);
  Bool_t SelectAuxTiming(Double_t dtime, Double_t energy);
  LKrCandidate *GetPhotonCandidate(Int_t j) {return fPhotonCandidate[j];};
  LKrCandidate *GetNewClusterCandidate(Int_t j) {return fNewClusterCandidate[j];};


  // 2-photon analysis
  Int_t PhotonAnalysis();
  Int_t GetNEMDoublets() {return fNEMDoublets;};
  Int_t *GetTwoEMClustersID() {return fTwoEMClustersID;};

  void ClusterAnalysis();
  Bool_t TwoPhotonCandidate();
  Bool_t SelectMIPCluster(Int_t jclus, TRecoLKrCandidate *thisCluster);
  Bool_t SelectEMCluster(Int_t jclus, TRecoLKrCandidate *thisCluster) ;
  Bool_t SelectOtherCluster(Int_t jclus, TRecoLKrCandidate *thisCluster, Bool_t isMIPCluster, Bool_t isEMCluster) ;
private:
  TRecoLKrEvent* fLKrEvent;

  TVector3 fTrackAtLKr;
  TVector2 fTrackClAtLKr;
  Double_t fTrackTime;
  Double_t fTrackCHODTime;

  LKrCandidate *fLKrClusterCandidate;
  LKrCandidate *fLKrCellCandidate;
  LKrCandidate *fPhotonCandidate[10];
  LKrCandidate *fNewClusterCandidate[5];
  LKrAuxClusterReco* fLKrAuxClusterReco;

  AnalysisTools *fTools;
  UserMethods *fUserMethods;
  Parameters *par;

  Double_t **fLKrCellT0;
  Int_t fFlag;
  L0TPData *fL0Data;
  Int_t fYear;
  Int_t fRunID;
  Int_t fEventNumber;
  //bool fIsFilter;

private:
  Int_t fNEMClusters;
  Int_t fNMIPClusters;
  Int_t fNOtherClusters;
  Int_t fEMClusterID[10];
  Int_t fMIPClusterID[10];
  Int_t fOtherClusterID[10];
  Int_t fNEMDoublets;
  Int_t *fTwoEMClustersID;

private:
  inline void AddEMClusters() {fNEMClusters++;};
  inline void AddMIPClusters() {fNMIPClusters++;};
  inline void AddOtherClusters() {fNOtherClusters++;};
  inline void SetEMClusterID(Int_t val) {fEMClusterID[fNEMClusters]=val;};
  inline void SetMIPClusterID(Int_t val) {fMIPClusterID[fNMIPClusters]=val;};
  inline void SetOtherClusterID(Int_t val) {fOtherClusterID[fNOtherClusters]=val;};

private:


};


#endif
