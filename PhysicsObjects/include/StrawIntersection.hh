#ifndef StrawIntersection_h
#define StrawIntersection_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

class StrawIntersection 
{
  public :
    StrawIntersection();
    virtual ~StrawIntersection();
    void Clear();

  public:
    double GetX() { return fX; }; 
    double GetY() { return fY; }; 
    double GetU() { return fU; }; 
    double GetV() { return fV; }; 
    double GetXcoor() { return fXcoor; }; 
    double GetYcoor() { return fYcoor; }; 
    int GetType() { return fClusterId.size(); };
    double GetTrailingTime() { return fTrailingTime; };
    double GetQuality() { return fQuality; };
    int GetClusterId(Int_t jCluster) { return fClusterId[jCluster]; };
    int GetViewId(Int_t jCluster) { return fViewId[jCluster]; };

    void SetTrailingTime(double val) {fTrailingTime=val;} ;
    void SetQuality(double val) {fQuality=val;};
    void SetSubType(int val) {fSubType=val;};
    void SetCoordinate(double *val);
    void SetClusterId(int val){fClusterId.push_back(val);};
    void SetViewId(int val){fViewId.push_back(val);};

  private:
    double fTrailingTime;
    double fQuality;
    int fSubType;
    double fX;
    double fY;
    double fU;
    double fV;
    double fXcoor;
    double fYcoor;
    std::vector<int> fClusterId; //< Vector of the Id of the clusters forming the intersection. 
    std::vector<int> fViewId;
};

#endif
