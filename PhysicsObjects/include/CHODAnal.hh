#ifndef CHODANAL_HH
#define CHODANAL_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include "Parameters.hh"
#include "UserMethods.hh"
#include "TRecoCHODEvent.hh"
#include "TRecoLKrEvent.hh"
#include "LKrAuxClusterReco.hh"
#include "TRecoNewCHODEvent.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "CHODCandidate.hh"
#include "DownstreamParticle.hh"

class AnalysisTools;
class LKrAuxClusterReco;

class CHODAnal
{
public:
    CHODAnal(int,NA62Analysis::Core::BaseAnalysis *);
    ~CHODAnal();

    void Clear();
    void StartBurst(Int_t runid);
    //void StartBurst();
    UserMethods *GetUserMethods() { return fUserMethods;};

    void SetEvent(TRecoCHODEvent* evt){fCHODEvent = evt;}
    void SetLKrEvent(TRecoLKrEvent* evt){fLKrEvent = evt;}
    void SetNewCHODEvent(TRecoNewCHODEvent* evt){fNewCHODEvent = evt;}
    bool LKrHitInTime(double E, double dt) ;
    Int_t TrackMatching(TRecoSpectrometerCandidate* thisTrack);
    Int_t MakeAllTrajectories(double reftime, TVector3 posatlkr, TVector3 posatmirror, TVector3 posatstraw4, TVector3 posatnewchodout) ;
    Int_t MakeMultiplicity(double reftime);
    Int_t MakeMultiplicityHits(double reftime);
    Int_t MakeMultiplicityNewCHOD(double reftime);
    Int_t MultiplicityCellMatching(Double_t reftime,  TVector3 posatlkr, Int_t flag) ;
    Bool_t FindMultiplicity(DownstreamParticle &track) ;


    CHODCandidate* GetCHODCandidate(){ return fCHODCandidate;}
    Int_t GetLKrMult() {return fLKrMult;};
    Int_t GetLKrHadrMult() {return fLKrHadrMult;};
    Int_t GetCHODSlabsInTime() {return fCHODSlabsInTime;};
    Int_t GetCHODMult() {return fCHODMult;};
    Int_t GetCHODNewCHODMult() {return fCHODNewCHODMult;};
    Int_t GetNewCHODMult() {return fNewCHODMult;};

    int fFlag;
    bool CHODQ1();
    double CHODTimeCorrection(int iS, int iP, double tot, double xslab, double yslab, double enslew);
    vector<double> CHODTrackMatching(double xPos, double yPos, double ttime, int flag,bool mcflag);
    vector<double> CHODMultMatching(double xPos, double yPos, double ttime,int itV, int itH,int flag, bool mcflag) ;
    void AlignChannels(Double_t reftime) ;
private:
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    CHODCandidate *fCHODCandidate;
    Parameters * param;
    EventHeader * fHeader;
private:

    Bool_t fIsFilter;
    Bool_t fIsMC;

    double fCHODPosV[64];
    double fCHODPosH[64];
    double fCHODAllSlewSlope[128][16];
    double fCHODAllSlewConst[128][16];
    double fCHODLightVelocitiesCorr[128][16];
    double fCHODAllT0[128][16];
    double fCHODAllFineT0[128][16];
    double fCHODLengthV[128];
    double fCHODLengthH[128];
    double fCHODT0[128];
    double fCHODVelocity[128];
    TString fHNamedHitTime[3];
    TString fHNameModvsT[3];
    TString fHNameDTimevsChi2[3];
    TString fHNameDistvsChi2[3];

    vector<int> fvCHODQ[2][4];

    Double_t fCHODLightVelocities[128];
    Double_t fPMCoordinate[128] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                   1.9, 8.4, 14.9, 21.4, 31.3, 41.2, 51.1, 61.0,
                                   60.0, 69.9, 79.8, 89.7, 99.6, 106.1, 112.6, 119.1,
                                   121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0,
                                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                   1.9, 8.4, 14.9, 21.4, 31.3, 41.2, 51.1, 61.0,
                                   60.0, 69.9, 79.8, 89.7, 99.6, 106.1, 112.6, 119.1,
                                   121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0,
                                   60.0, 69.9, 79.8, 89.7, 99.6, 106.1, 112.6, 119.1,
                                   121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0,
                                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                   1.9, 8.4, 14.9, 21.4, 31.3, 41.2, 51.1, 61.0,
                                   60.0, 69.9, 79.8, 89.7, 99.6, 106.1, 112.6, 119.1,
                                   121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0, 121.0,
                                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                   1.9, 8.4, 14.9, 21.4, 31.3, 41.2, 51.1, 61.0};


    Double_t fSlabLimitsX[33] = {-1210.,-1110.,-1010.,-910.,-810.,-710.,-650.,-580.5,-520.,-450.5,-390.,
                                 -320.5,-260.,-190.5,-130.,-60.5,0.,60.5,130.,190.5,260.,320.5,
                                 390.,450.5,520.,580.5,650.,710.,810.,910.,1010.,1110.,1210.};
    Double_t fSlabLimitsY[33] = {-1210.,-1110.,-1010.,-910.,-810.,-710.,-650.,-580.5,-520.,-450.5,-390.,
                                 -320.5,-260.,-190.5,-130.,-60.5,0.,60.5,130.,190.5,260.,320.5,
                                 390.,450.5,520.,580.5,650.,710.,810.,910.,1010.,1110.,1210.};
    Double_t fSlabCenter[128]; // x coordinate for 0<=i<64, y coordinate for 64<=i<128



    // Double_t *fCHODPosV;
    // Double_t *fCHODPosH;
    // Double_t **fCHODAllT0;
    // Double_t **fCHODAllFineT0;
    // Double_t **fCHODAllSlewSlope;
    // Double_t **fCHODAllSlewConst;

    Int_t fRun;
    Int_t fLKrHadrMult = 0;
    Int_t fLKrMult = 0;
    Int_t fCHODMult = 0;
    Int_t fCHODSlabsInTime = 0;
    Int_t fCHODNewCHODMult = 0;
    Int_t fNewCHODMult = 0;

    TRecoCHODEvent* fCHODEvent;
    TRecoLKrEvent* fLKrEvent;
    LKrAuxClusterReco* fLKrAuxClusterReco;
    TRecoNewCHODEvent* fNewCHODEvent;
    Int_t MakeCandidate(Double_t,Double_t,Double_t,Double_t*);
    Int_t Quadrant(Int_t);
    struct ChannelOrder{
        TRecoCHODEvent* fevent;
        ChannelOrder(TRecoCHODEvent* a) : fevent(a) {};
        bool operator() ( int i, int j ){
            TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
            TRecoCHODHit* hj = (TRecoCHODHit*) fevent->GetHit(j);
            return hi->GetChannelID()<hj->GetChannelID();
        }
    };
    struct PlaneCondition{
        TRecoCHODEvent* fevent;
        PlaneCondition(TRecoCHODEvent* a) : fevent(a) {};
        bool operator() ( int i ){
            TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
            return hi->GetChannelID()<=63;
        }
    };
    struct QuadrantCondition{
        TRecoCHODEvent* fevent;
        int fQ;
        int fP;
        QuadrantCondition(TRecoCHODEvent* a, int q, int p) : fevent(a),fQ(q),fP(p) {};
        bool operator() ( int i ){
            TRecoCHODHit* hi = (TRecoCHODHit*) fevent->GetHit(i);
            return ((hi->GetChannelID()>=16*fQ+64*fP)&&(hi->GetChannelID()<16*(fQ+1)+64*fP));
        }
    };



};


#endif
