#ifndef CHODCandidate_h
#define CHODCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class CHODCandidate
{
public :
  CHODCandidate();
  virtual ~CHODCandidate();
  void Clear();

public:
    void SetDiscriminant(Double_t val) {fDiscriminant=val;};
    void SetDeltaTime(Double_t val) {fDeltaTime=val;};
    void SetX(Double_t val) {fX=val;};
    void SetY(Double_t val) {fY=val;};
    void SetCounterV(Int_t val) {fCounterV=val;};
    void SetRecoHitHID(Int_t val) {fHitHID=val;};
    void SetRecoHitVID(Int_t val) {fHitVID=val;};
    void SetCounterH(Int_t val) {fCounterH=val;};
    void SetTimeV(Double_t val) {fTimeV=val;};
    void SetTimeH(Double_t val) {fTimeH=val;};
    void SetIsCHODCandidate(Bool_t val) {fIsCHODCandidate=val;};

    Double_t GetDiscriminant() {return fDiscriminant;};
    Double_t GetDeltaTime() {return fDeltaTime;};
    Double_t GetX() {return fX;};
    Double_t GetY() {return fY;};
    Int_t GetCounterV() {return fCounterV;};
    Int_t GetRecoHitVID() {return fHitVID;};
    Int_t GetRecoHitHID() {return fHitHID;};
    Int_t GetCounterH() {return fCounterH;};
    Double_t GetTimeV() {return fTimeV;};
    Double_t GetTimeH() {return fTimeH;};
    Bool_t   GetIsCHODCandidate() {return fIsCHODCandidate;};

public:
  //Multiplicity variables
  void SetCHODMult     (Int_t val){fCHODMult=val;};
  void SetCHODNewCHODMult     (Int_t val){fCHODNewCHODMult=val;};
  void SetNewCHODMult  (Int_t val){fNewCHODMult=val;};
  void SetLKrMult      (Int_t val){fLKrMult=val;};
  void SetLKrCHODdR    (TVector3 val){fLKrCHODdR=val;};
  void SetLKrCHODdt    (Double_t val){fLKrCHODdt=val;};
  void SetLKrNewCHODdR (TVector3 val){fLKrNewCHODdR=val;};
  void SetLKrNewCHODdt (Double_t val){fLKrNewCHODdt=val;};
  void SetCHODNewCHODdR(TVector3 val){fCHODNewCHODdR=val;};
  void SetCHODNewCHODdt(Double_t val){fCHODNewCHODdt=val;};
  void SetLKrCHODdR    (Double_t x, Double_t y, Double_t z){fLKrCHODdR    =TVector3(x,y,z);};
  void SetLKrNewCHODdR (Double_t x, Double_t y, Double_t z){fLKrNewCHODdR =TVector3(x,y,z);};
  void SetCHODNewCHODdR(Double_t x, Double_t y, Double_t z){fCHODNewCHODdR=TVector3(x,y,z);};

  Int_t GetLKrMult() {return fLKrMult;};
  Int_t GetCHODMult() {return fCHODMult;};
  Int_t GetCHODNewCHODMult() {return fCHODNewCHODMult;};
  Int_t GetNewCHODMult() {return fNewCHODMult;};
  TVector3 GetLKrCHODdR(){return fLKrCHODdR;}
  TVector3 GetLKrNewCHODdR(){return fLKrNewCHODdR;}
  TVector3 GetCHODNewCHODdR(){return fCHODNewCHODdR;}
  Double_t GetLKrNewCHODdt (){return fLKrNewCHODdt;};
  Double_t GetCHODNewCHODdt(){return fCHODNewCHODdt;};
  Double_t GetLKrCHODdt    (){return fLKrCHODdt;};


private:
  Int_t fCHODMult;
  Int_t fCHODNewCHODMult;
  Int_t fNewCHODMult;
  Int_t fLKrMult;

  TVector3 fLKrCHODdR;
  TVector3 fLKrNewCHODdR;
  TVector3 fCHODNewCHODdR;
  Double_t fCHODNewCHODdt;
  Double_t fLKrNewCHODdt;
  Double_t fLKrCHODdt;

  private:
  Double_t fDiscriminant;
  Double_t fDeltaTime;
  Double_t fX;
  Double_t fY;
  Bool_t fIsCHODCandidate;
  Int_t fCounterV;
  Int_t fCounterH;
    Int_t fHitHID;
    Int_t fHitVID;
    Double_t fTimeV;
  Double_t fTimeH;

};

#endif
