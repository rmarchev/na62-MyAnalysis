#ifndef MUV3ANAL_HH
#define MUV3ANAL_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoMUV3Event.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "MUV3Candidate.hh"

class AnalysisTools;

class MUV3Anal
{
public :
  MUV3Anal(NA62Analysis::Core::BaseAnalysis *);
  virtual ~MUV3Anal();
  UserMethods *GetUserMethods() { return fUserMethods;};
  void Clear();
  Int_t TrackMatching(Int_t, TRecoSpectrometerCandidate*);
  Int_t TrackTimeMatching(TRecoSpectrometerCandidate *thisTrack);

  void SetEvent(TRecoMUV3Event* evt){fMUV3Event = evt;}

public:
  MUV3Candidate *GetMUV3Candidate() {return fMUV3Candidate;};
  MUV3Candidate *GetMUV3TightCandidate() {return fMUV3TightCandidate;};
  MUV3Candidate *GetMUV3MediumCandidate() {return fMUV3MediumCandidate;};
  MUV3Candidate *GetMUV3BroadCandidate() {return fMUV3BroadCandidate;};

private:
  AnalysisTools *fTools;
  UserMethods *fUserMethods;
  TRecoMUV3Event *fMUV3Event;

  MUV3Candidate *fMUV3Candidate;
  MUV3Candidate *fMUV3TightCandidate;
  MUV3Candidate *fMUV3MediumCandidate;
  MUV3Candidate *fMUV3BroadCandidate;

private:
  TString fMUV3Anal_dtmin;
  TString fMUV3Anal_distvst[3];
  TString fMUV3Anal_xy[3];
  TString fMUV3Anal_distvsdeltat[3];
  TString fMUV3Anal_mindistvst[3];
  TString fMUV3Anal_minxy[3];
};

#endif
