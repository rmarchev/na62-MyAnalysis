#ifndef SACAnal_h
#define SACAnal_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "Parameters.hh"
#include "UserMethods.hh"
#include "TRecoSACEvent.hh"
#include "PhotonVetoCandidate.hh"

class Parameters;
class AnalysisTools;
class PhotonVetoCandidate;

class SACAnal
{
public :
  SACAnal(NA62Analysis::Core::BaseAnalysis *);
  virtual ~SACAnal(){}
  UserMethods *GetUserMethods() { return fUserMethods;};
  void StartBurst(Int_t,Int_t);
  void Clear();
  PhotonVetoCandidate *GetPhotonCandidate(Int_t j) {return fPhotonCandidate[j];};
    double TimeSlewCorrected(TRecoSACHit *hit) ;
public:
  Int_t MakeCandidate(Double_t,TRecoSACEvent*);

private:
  Parameters *fPar;
  AnalysisTools *fTools;
  UserMethods *fUserMethods;
  PhotonVetoCandidate *fPhotonCandidate[20];

private:
  Int_t fSACPriorityMask[16];
  Double_t *fSACChannelT0;
  Int_t fYear;
    Int_t fRunID;
    bool fIsMC;

  Double_t fTimeCut;

private:
};

#endif
