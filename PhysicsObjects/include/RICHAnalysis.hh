#ifndef RICHAnalysis_h
#define RICHAnalysis_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include "TVector3.h"

#include "UserMethods.hh"
#include "TRecoRICHEvent.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "RICHCandidate.hh"
#include "Parameters.hh"

//class RICHCandidate
//class Parameters;
class AnalysisTools;
class RICHImprovedRing;

class RICHAnalysis
{
  public :
    RICHAnalysis(int,NA62Analysis::Core::BaseAnalysis *);
    virtual ~RICHAnalysis();
    UserMethods *GetUserMethods() { return fUserMethods;};
    void Clear();
    void StartBurst(Int_t,Int_t,time_t);
    Int_t TrackMultiRingMatching(Double_t,TRecoRICHEvent*,TRecoSpectrometerCandidate*);
    Int_t TrackSingleRingMatching(Double_t,TRecoRICHEvent*,TRecoSpectrometerCandidate*);
    RICHCandidate *GetRICHMultiCandidate() {return fRICHMultiCandidate;};
    RICHCandidate *GetRICHSingleCandidate() {return fRICHSingleCandidate;};

  public:
    Double_t GetFocalLength() {return fFocalLength;};
    Double_t GetElectronRingRadius() {return fElectronRingRadius;};
    Int_t GetElectronRingNhits() {return fElectronRingNhits;};
    Double_t GetRefIndex() {return fRefIndex;};

  private:
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    RICHCandidate *fRICHMultiCandidate;
    RICHCandidate *fRICHSingleCandidate;
    RICHImprovedRing *fImprovedRing;
    Parameters *fPar;

  private:
    Double_t **fDeltaMisa;
    Double_t ***fMirrorPos;
    Double_t fMirrorPosition[20][2];  ///< Mirror centre potisions (sequential numbering)
    Int_t    fMirrorNumber[20]; ///< Mapping of standard mirror numbers to contiguous numbers
  double fMirrorAlign[52];
    Int_t fYear;
    bool fIsMC;
    bool fIsFilter;
    Double_t fFocalLength;
    Double_t fElectronRingRadius;
    Int_t fElectronRingNhits;
    Double_t fRefIndex;
  double fTOffset;

  private:
    TRecoRICHEvent *fRICHEvent;
    void MirrorVector(TVector3*,TVector3*);
    Int_t MirrorSurface(Double_t,Double_t,Double_t,Bool_t);
    Int_t MirrorSurface(Double_t,Double_t);
};

#endif
