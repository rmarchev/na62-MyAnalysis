#ifndef MUVANAL_HH
#define MUVANAL_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoMUV1Event.hh"
#include "TRecoMUV2Event.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "MUVCandidate.hh"

class AnalysisTools;

class MUVAnal
{
public :
  MUVAnal(NA62Analysis::Core::BaseAnalysis *);
  virtual ~MUVAnal();
  UserMethods *GetUserMethods() { return fUserMethods;};
  void SetMUV1Event(TRecoMUV1Event* evt){fMUV1Event = evt;}
  void SetMUV2Event(TRecoMUV2Event* evt){fMUV2Event = evt;}
  void Clear();
  Int_t TrackMUV1Matching(TRecoSpectrometerCandidate*);
  Int_t TrackMUV2Matching(TRecoSpectrometerCandidate*);
  Double_t ComputeMUV1TotalInTimeEnergy(Double_t,Double_t,TRecoMUV1Event*);
  Double_t ComputeMUV2TotalInTimeEnergy(Double_t,Double_t,TRecoMUV2Event*);

public:
  MUVCandidate *GetMUV1Candidate() {return fMUV1Candidate;};
  MUVCandidate *GetMUV2Candidate() {return fMUV2Candidate;};

private:
  AnalysisTools *fTools;
  UserMethods *fUserMethods;
  MUVCandidate *fMUV1Candidate;
  MUVCandidate *fMUV2Candidate;

private:
  TRecoMUV1Event *fMUV1Event;
  TRecoMUV2Event *fMUV2Event;
};

#endif
