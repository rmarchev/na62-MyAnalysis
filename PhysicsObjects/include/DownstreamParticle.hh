#ifndef DownstreamParticle_h
#define DownstreamParticle_h

#include "MyParticle.hh"

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>
class MyParticle;

class DownstreamParticle : public MyParticle
{
public :
  DownstreamParticle();
  virtual ~DownstreamParticle();
  void Clear();
  ////    void Store(DownstreamParticle*);

public:
  // General
  TLorentzVector GetMuonMomentum() {return fMuonMomentum;};
  TLorentzVector GetMomentum() {return fPionMomentum;};
  TLorentzVector GetElectronMomentum() {return fElectronMomentum;};
  void SetMomentum(TLorentzVector val) {fPionMomentum=val;};
  void SetMuonMomentum(TLorentzVector val) {fMuonMomentum=val;};
  void SetElectronMomentum(TLorentzVector val) {fElectronMomentum=val;};

    void SetTrackID(Int_t val) {fTrackID=val;};
    void SetCharge(Int_t val) {fTrackCharge=val;};
  Int_t GetCharge() {return fTrackCharge;};
  Int_t GetTrackID() {return fTrackID;};
  // RICH variables (2 different reconstructions)

    Double_t GetRICHSingleDiscriminant()           {  return fRICHSingleDiscriminant;};
    Double_t GetRICHMultiDiscriminant ()           {  return fRICHMultiDiscriminant;};

  Bool_t         GetRICHMultiIsCandidate() {return fRICHMultiIsCandidate;};
  Bool_t         GetRICHMultiIsCandidateMomRange() {return fRICHMultiIsCandidateMomRange;};
  Bool_t   GetIsRICHSingleRingCandidateSlava() {return fIsRICHSingleRingCandidateSlava;};
  Int_t GetRICHMultiMostLikelyHypothesis() {return fMostLikelyHypothesis;   }
  Int_t GetRICHMultiMuonLikelihood() {return fRICHMuLikelihood;   }
  Int_t GetRICHMultiPionLikelihood() {return fRICHPiLikelihood;   }
  Int_t GetRICHMultiElectronLikelihood() {return fRICHElectronLikelihood;   }
  TLorentzVector GetRICHMultiMomentum() {return fRICHMultiMomentum;};
  Double_t       GetRICHMultiRadius()     {return fRICHMultiRadius;};
  Double_t       GetRICHMultiMass()     {return fRICHMultiMass;};
  Double_t       GetRICHMultiTime()     {return fRICHMultiTime;};
  Double_t       GetRICHMultiChi2()     {return fRICHMultiChi2;};
  Int_t         GetRICHSingleIsCandidate() {return fRICHSingleIsCandidate;};
  TLorentzVector GetRICHSingleMomentum() {return fRICHSingleMomentum;};
  Double_t       GetRICHSingleRadius()     {return fRICHSingleRadius;};
  Double_t       GetRICHSingleMass()     {return fRICHSingleMass;};
  Double_t       GetRICHSingleNHits()     {return fRICHAssoNHit;};
  Double_t       GetRICHSingleTime()     {return fRICHSingleTime;};
  Double_t       GetRICHSingleChi2()     {return fRICHSingleChi2;};
  Double_t GetPosNexp() {return fPosNexpectedHits;   }
  Double_t GetPiNexp() {return fPiNexpectedHits;   }
  Double_t GetMuNexp() {return fMuNexpectedHits;   }

  Double_t GetIsRICHPion(){return fIsRICHPion;};
  void SetIsRICHSingleRingCandidateSlava(Bool_t val){fIsRICHSingleRingCandidateSlava=val;};
  void SetIsRICHPion(Double_t val){fIsRICHPion=val;};
  void SetRICHMultiIsCandidate(Bool_t val) {fRICHMultiIsCandidate=val;};
  void SetRICHMultiIsCandidateMomRange(Int_t val) {fRICHMultiIsCandidateMomRange = val;};
  void SetRICHMultiMostLikelyHypothesis(Int_t val)   { fMostLikelyHypothesis = val; }
  void SetRICHMultiMuonLikelihood(Double_t val) {fRICHMuLikelihood = val;   }
  void SetRICHMultiPionLikelihood(Double_t val) {fRICHPiLikelihood = val;   }
  void SetRICHMultiElectronLikelihood(Double_t val) {fRICHElectronLikelihood = val;   }
  void SetPosNexp(Double_t val) { fPosNexpectedHits=val;   }
  void SetPiNexp (Double_t val) {fPiNexpectedHits=val;   }
  void SetMuNexp (Double_t val) { fMuNexpectedHits=val;   }

  void SetRICHMultiMomentum(TLorentzVector val) {fRICHMultiMomentum=val;};
  void SetRICHMultiMass(Double_t val)           {fRICHMultiMass=val;};
  void SetRICHMultiRadius(Double_t val)           {fRICHMultiRadius=val;};
  void SetRICHMultiTime(Double_t val)           {fRICHMultiTime=val;};
  void SetRICHMultiChi2(Double_t val)           {fRICHMultiChi2=val;};
  void SetRICHSingleIsCandidate(Int_t val) {fRICHSingleIsCandidate=val;};
  void SetRICHSingleMomentum(TLorentzVector val) {fRICHSingleMomentum=val;};
  void SetRICHSingleRadius(Double_t val)           {fRICHSingleRadius=val;};
  void SetRICHSingleMass(Double_t val)           {fRICHSingleMass=val;};
  void SetRICHSingleTime(Double_t val)           {fRICHSingleTime=val;};
  void SetRICHSingleChi2(Double_t val)           {fRICHSingleChi2=val;};
  void SetRICHSingleNHits(Double_t val)           {fRICHAssoNHit=val;};

    void SetRICHSingleDiscriminant(Double_t val)           {  fRICHSingleDiscriminant=val;};
    void SetRICHMultiDiscriminant(Double_t val)           {  fRICHMultiDiscriminant=val;};

  // LKr variables
  Int_t GetLKrID() {return fLKrID;};
    bool GetLKrMerged() {return fLKrMerged;};
  Double_t GetLKrTime() {return fLKrTime;};
  Double_t GetLKrEovP() {return fLKrEovP;};
  Double_t GetLKrSeedEnergy() {return fLKrSeedEnergy;};
  Int_t GetLKrNCells() {return fLKrNCells;};
  TVector2 GetLKrPosition() {return fLKrPosition;};
    void SetLKrMerged(bool val) {fLKrMerged=val;};
  void SetLKrID(Int_t val) {fLKrID=val;};
  void SetLKrTime(Double_t val) {fLKrTime=val;};
  void SetLKrEovP(Double_t val) {fLKrEovP=val;};
  void SetLKrSeedEnergy(Double_t val) {fLKrSeedEnergy=val;};
  void SetLKrNCells(Int_t val) {fLKrNCells=val;};
  void SetLKrPosition(Double_t xval, Double_t yval) {fLKrPosition.Set(xval,yval);};

  // MUV1 variables
  Int_t GetMUV1ID() {return fMUV1ID;};
  Int_t GetMUV1OuterNhits() {return fMUV1OuterNhits;};
  Double_t GetMUV1Energy() {return fMUV1Energy;};
  Double_t GetMUV1OuterEnergy() {return fMUV1OuterEnergy;};
  Double_t GetMUV1Time() {return fMUV1Time;};
  Double_t GetMUV1ShowerWidth() {return fMUV1ShowerWidth;};
  Double_t GetMUV1SeedRatio() {return fMUV1SeedRatio;};
  Double_t GetMUV1EMerged() {return fMUV1EMerged;};
  void SetMUV1ID(Int_t val) {fMUV1ID=val;};
  void SetMUV1OuterNhits(Int_t val) {fMUV1OuterNhits=val;};
  void SetMUV1Energy(Double_t val) {fMUV1Energy=val;};
  void SetMUV1OuterEnergy(Double_t val) {fMUV1OuterEnergy=val;};
  void SetMUV1Time(Double_t val) {fMUV1Time=val;};
  void SetMUV1ShowerWidth(Double_t val) {fMUV1ShowerWidth=val;};
  void SetMUV1SeedRatio(Double_t val) {fMUV1SeedRatio=val;};
  void SetMUV1EMerged(Double_t val) {fMUV1EMerged=val;};

  // MUV2 variables
  Int_t GetMUV2ID() {return fMUV2ID;};
  Int_t GetMUV2OuterNhits() {return fMUV2OuterNhits;};
  Double_t GetMUV2Energy() {return fMUV2Energy;};
  Double_t GetMUV2OuterEnergy() {return fMUV2OuterEnergy;};
  Double_t GetMUV2Time() {return fMUV2Time;};
  Double_t GetMUV2ShowerWidth() {return fMUV2ShowerWidth;};
  Double_t GetMUV2SeedRatio() {return fMUV2SeedRatio;};
  void SetMUV2ID(Int_t val) {fMUV2ID=val;};
  void SetMUV2OuterNhits(Int_t val) {fMUV2OuterNhits=val;};
  void SetMUV2Energy(Double_t val) {fMUV2Energy=val;};
  void SetMUV2OuterEnergy(Double_t val) {fMUV2OuterEnergy=val;};
  void SetMUV2Time(Double_t val) {fMUV2Time=val;};
  void SetMUV2ShowerWidth(Double_t val) {fMUV2ShowerWidth=val;};
  void SetMUV2SeedRatio(Double_t val) {fMUV2SeedRatio=val;};

  // Total calorimetric energy
  Double_t GetCalorimetricEnergy() {return fCalorimetricEnergy;};
  void SetCalorimetricEnergy(Double_t val) {fCalorimetricEnergy=val;};

  // MUV3 variables
  Int_t GetMUV3ID() {return fMUV3ID;};
  Int_t GetMUV3PosID() {return fMUV3PosID;};
  Double_t GetMUV3Time() {return fMUV3Time;};
  TVector2 GetMUV3Distance() {return fMUV3Distance;};
  void SetMUV3ID(Int_t val) {fMUV3ID=val;};
  void SetMUV3PosID(Int_t val) {fMUV3PosID=val;};
  void SetMUV3Time(Double_t val) {fMUV3Time=val;};
  void SetMUV3Distance(TVector2 val) {fMUV3Distance=val;};

  // Beam track associated to the downstream track
  Int_t GetUpstreamTrackID() {return fUpstreamTrackID;};
  void SetUpstreamTrackID(Int_t val) {fUpstreamTrackID=val;};

  // Position of the track at various planes
  TVector3 GetPositionAtGTK(Int_t j) {return fPosAtGTK[j];};
  TVector3 GetPositionAtStraw(Int_t j) {return fPosAtStraw[j];};
  TVector3 GetPositionAtRICH(Int_t j) {return fPosAtRICH[j];};
  TVector3 GetPositionAtLAV(Int_t j) {return fPosAtLAV[j];};
  TVector3 GetPositionAtNewCHOD(Int_t j) {return fPosAtNewCHOD[j];};
  TVector3 GetPositionAtCHOD(Int_t j) {return fPosAtCHOD[j];};
  TVector3 GetPositionAtIRC(Int_t j) {return fPosAtIRC[j];};
  TVector3 GetPositionAtLKr() {return fPosAtLKr;};
  TVector3 GetPositionAtMUV(Int_t j) {return fPosAtMUV[j];};
  void SetPositionAtGTK(Int_t j, TVector3 pos)   {fPosAtGTK[j] = pos;};
  void SetPositionAtStraw(Int_t j, TVector3 pos) {fPosAtStraw[j] = pos;};
  void SetPositionAtRICH(Int_t j, TVector3 pos)  {fPosAtRICH[j] = pos;};
  void SetPositionAtLAV(Int_t j, TVector3 pos)   {fPosAtLAV[j] = pos;};
  void SetPositionAtCHOD(Int_t j, TVector3 pos)  {fPosAtCHOD[j] = pos;};
  void SetPositionAtNewCHOD(Int_t j, TVector3 pos)  {fPosAtNewCHOD[j] = pos;};
  void SetPositionAtIRC(Int_t j, TVector3 pos)   {fPosAtIRC[j] = pos;};
  void SetPositionAtLKr(TVector3 pos)            {fPosAtLKr = pos;};
  void SetPositionAtMUV(Int_t j, TVector3 pos)   {fPosAtMUV[j] = pos;};

  // Photon candidates associated to the track
  Bool_t GetIsPhotonLKrCandidate() {return fIsPhotonLKrCandidate;};
  Bool_t GetIsPhotonLKrActivity() {return fIsPhotonLKrActivity;};
  Bool_t GetIsPhotonLAVCandidate() {return fIsPhotonLAVCandidate;};
  Bool_t GetIsLAVTrack() {return fIsLAVTrack;};
  Bool_t GetIsPhotonIRCCandidate() {return fIsPhotonIRCCandidate;};
  Bool_t GetIsPhotonSACCandidate() {return fIsPhotonSACCandidate;};
  Bool_t GetIsPhotonSAVCandidate() {return fIsPhotonSAVCandidate;};
  Bool_t GetIsPhotonNewLKrCandidate() {return fIsPhotonNewLKrCandidate;};
  void SetIsPhotonLKrCandidate(Bool_t val) {fIsPhotonLKrCandidate=val;};
  void SetIsPhotonLKrActivity(Bool_t val) {fIsPhotonLKrActivity=val;};
  void SetIsLAVTrack(Bool_t val) {fIsLAVTrack=val;};
  void SetIsPhotonLAVCandidate(Bool_t val) {fIsPhotonLAVCandidate=val;};
  void SetIsPhotonIRCCandidate(Bool_t val) {fIsPhotonIRCCandidate=val;};
  void SetIsPhotonSACCandidate(Bool_t val) {fIsPhotonSACCandidate=val;};
  void SetIsPhotonSAVCandidate(Bool_t val) {fIsPhotonSAVCandidate=val;};
  void SetIsPhotonNewLKrCandidate(Bool_t val) {fIsPhotonNewLKrCandidate=val;};

  // LKr Photon Candidate
  Int_t    GetNPhotonLKrCandidates()              {return fNPhotonLKrCandidates;};
  Double_t GetPhotonLKrCandidateTime(Int_t j)     {return fPhotonLKrCandidateTime[j];};
  TVector2 GetPhotonLKrCandidatePosition(Int_t j) {return fPhotonLKrCandidatePosition[j];};
  Double_t GetPhotonLKrCandidateEnergy(Int_t j)   {return fPhotonLKrCandidateEnergy[j];};
  Int_t    GetPhotonLKrCandidateNCells(Int_t j)   {return fPhotonLKrCandidateNCells[j];};
  Int_t    GetNPhotonNewLKrCandidates()           {return fNPhotonNewLKrCandidates;};
  void     SetNPhotonLKrCandidates(Int_t val)                  {fNPhotonLKrCandidates=val;};
  void     SetPhotonLKrCandidateTime(Int_t j,Double_t val)     {fPhotonLKrCandidateTime[j]=val;};
  void     SetPhotonLKrCandidatePosition(Int_t j,Double_t valx, Double_t valy) {fPhotonLKrCandidatePosition[j]=TVector2(valx,valy);};
  void     SetPhotonLKrCandidateEnergy(Int_t j,Double_t val)   {fPhotonLKrCandidateEnergy[j]=val;};
  void     SetPhotonLKrCandidateNCells(Int_t j,Int_t val)      {fPhotonLKrCandidateNCells[j]=val;};
  void     SetNPhotonNewLKrCandidates(Int_t val)               {fNPhotonNewLKrCandidates=val;};

  // LAV Photon Candidate
  Int_t    GetNPhotonLAVCandidates()          {return fNPhotonLAVCandidates;};
  Double_t GetPhotonLAVCandidateTime(Int_t j) {return fPhotonLAVCandidateTime[j];};
  Int_t    GetPhotonLAVCandidateID(Int_t j)   {return fPhotonLAVCandidateID[j];};
  Int_t    GetPhotonLAVCandidateLAVID(Int_t j)   {return fPhotonLAVCandidateLAVID[j];};
  void     SetNPhotonLAVCandidates(Int_t val)              {fNPhotonLAVCandidates=val;};
  void     SetPhotonLAVCandidateTime(Int_t j,Double_t val) {fPhotonLAVCandidateTime[j]=val;};
  void     SetPhotonLAVCandidateID(Int_t j,Int_t val)      {fPhotonLAVCandidateID[j]=val;};
  void     SetPhotonLAVCandidateLAVID(Int_t j,Int_t val)      {fPhotonLAVCandidateLAVID[j]=val;};

  // IRC Photon Candidate
  Int_t    GetNPhotonIRCCandidates()          {return fNPhotonIRCCandidates;};
  Double_t GetPhotonIRCCandidateTime(Int_t j) {return fPhotonIRCCandidateTime[j];};
  Int_t    GetPhotonIRCCandidateID(Int_t j)   {return fPhotonIRCCandidateID[j];};
  void     SetNPhotonIRCCandidates(Int_t val)              {fNPhotonIRCCandidates=val;};
  void     SetPhotonIRCCandidateTime(Int_t j,Double_t val) {fPhotonIRCCandidateTime[j]=val;};
  void     SetPhotonIRCCandidateID(Int_t j,Int_t val)      {fPhotonIRCCandidateID[j]=val;};

  // SAC Photon Candidate
  Int_t    GetNPhotonSACCandidates()          {return fNPhotonSACCandidates;};
  Double_t GetPhotonSACCandidateTime(Int_t j) {return fPhotonSACCandidateTime[j];};
  Int_t    GetPhotonSACCandidateID(Int_t j)   {return fPhotonSACCandidateID[j];};
  void     SetNPhotonSACCandidates(Int_t val)              {fNPhotonSACCandidates=val;};
  void     SetPhotonSACCandidateTime(Int_t j,Double_t val) {fPhotonSACCandidateTime[j]=val;};
  void     SetPhotonSACCandidateID(Int_t j,Int_t val)      {fPhotonSACCandidateID[j]=val;};

  // SAV Photon Candidate
  Int_t    GetNPhotonSAVCandidates()          {return fNPhotonSAVCandidates;};
  void     SetNPhotonSAVCandidates(Int_t val)              {fNPhotonSAVCandidates=val;};



  //Riccardo analyzer test functions
  void    SetCaloTimeRA(Double_t val)          {fCaloTimeRA = val;};
  void    SetMuonProb(Double_t val)          {fMuProb = val;};
  void    SetElectronProb(Double_t val)          {fElProb = val;};
  void    SetPionProb(Double_t val)          {fPiProb = val;};
  void    SetMIPDiscr(Double_t val)          {fMIPdiscr = val;};
  void    SetIsMIP(Bool_t val)          {fIsMIP = val;};
  void    SetIsMuonDiscriminant(Bool_t val)          {fIsMuonDiscriminant = val;};
  void    SetIsMulti(Bool_t val)          {fIsMulti = val;};
  void    SetIsEM(Bool_t val)          {fIsEM = val;};
  void    SetIsRAMuon(Bool_t val)          {fIsRAMuon = val;};


  Double_t    GetIsRAMuon()          {return fIsRAMuon;};
  Double_t    GetIsEM()          {return fIsEM;};
  Double_t    GetCaloTimeRA()          {return fCaloTimeRA;};
  Double_t    GetMuonProb()          {return fMuProb;};
  Double_t    GetElectronProb()          {return fElProb;};
  Double_t    GetPionProb()          {return fPiProb;};
  Double_t    GetMIPDiscr()          {return fMIPdiscr;};
  Bool_t      GetIsMuonDiscriminant()          {return fIsMuonDiscriminant;};
  Bool_t      GetIsMIP()          {return fIsMIP;};
  Bool_t      GetIsMulti()          {return fIsMulti;};


  Double_t GetIsGoodCHOD   () {return fGoodCHOD;};
  Double_t GetIsGoodNewCHOD() {return fGoodNewCHOD;};
  Double_t GetIsGoodRICHSR () {return fGoodRICHSR;};
  Double_t GetIsGoodRICHMR () {return fGoodRICHMR;};
  Double_t GetIsGoodLKr    () {return fGoodLKr;};
  Double_t GetIsGoodMUV1   () {return fGoodMUV1;};
  Double_t GetIsGoodMUV2   () {return fGoodMUV2;};
  Double_t GetIsGoodMUV3   () {return fGoodMUV3;};

  void SetIsGoodCHOD   (bool val) { fGoodCHOD   = val;};
  void SetIsGoodNewCHOD(bool val) { fGoodNewCHOD= val;};
  void SetIsGoodRICHSR (bool val) { fGoodRICHSR = val;};
  void SetIsGoodRICHMR (bool val) { fGoodRICHMR = val;};
  void SetIsGoodLKr    (bool val) { fGoodLKr    = val;};
  void SetIsGoodMUV1   (bool val) { fGoodMUV1   = val;};
  void SetIsGoodMUV2   (bool val) { fGoodMUV2   = val;};
  void SetIsGoodMUV3   (bool val) { fGoodMUV3   = val;};

  //Downstream Time
  void SetDownstreamTime(Double_t val) {fDownstreamTime = val;};
  Double_t       GetDownstreamTime() {return fDownstreamTime;};

  // CHOD and NewCHOD variables
  Int_t GetCounterV() {return fCounterV;};
  Int_t GetCounterH() {return fCounterH;};
    Int_t GetCHODRecoHitVID() {return fCHODRecoHitVID;};
    Int_t GetCHODRecoHitHID() {return fCHODRecoHitHID;};
    Int_t GetNewCHODRecoHitID() {return fNewCHODRecoHitID;};

  Double_t GetTimeV() {return fTimeV;};
  Double_t GetTimeH() {return fTimeH;};

  Double_t       GetCHODTime() {return fCHODTime;};
  Double_t       GetNewCHODTime() {return fNewCHODTime;};
  TVector3       GetCHODPosition() {return fCHODPosition;};
  TVector3       GetNewCHODPosition() {return fNewCHODPosition;};
  Int_t GetCHODSlabsInTime() {return fCHODSlabsInTime;};
  Int_t GetCHODMult() {return fCHODMult;};
  Int_t GetNewCHODMult() {return fNewCHODMult;};
  Int_t GetCHODNewCHODMult() {return fCHODNewCHODMult;};

  void SetCounterV(Int_t val) {fCounterV=val;};
  void SetCounterH(Int_t val) {fCounterH=val;};
    void SetNewCHODRecoHitID(Int_t val) { fNewCHODRecoHitID=val;};
    void SetCHODRecoHitVID(Int_t val) { fCHODRecoHitVID=val;};
    void SetCHODRecoHitHID(Int_t val) { fCHODRecoHitHID=val;};

  void SetTimeV(Double_t val) {fTimeV=val;};
  void SetTimeH(Double_t val) {fTimeH=val;};

  void SetCHODTime(Double_t val) {fCHODTime=val;};
  void SetNewCHODTime(Double_t val) {fNewCHODTime=val;};
  void SetCHODMult     (Int_t val){fCHODMult=val;};
  void SetCHODSlabsInTime(Int_t val) {fCHODSlabsInTime=val;};
  void SetNewCHODMult  (Int_t val){fNewCHODMult=val;};
  void SetCHODNewCHODMult     (Int_t val){fCHODNewCHODMult=val;};
  void SetCHODPosition(Double_t x, Double_t y, Double_t z) {fCHODPosition=TVector3(x,y,z);};
  void SetNewCHODPosition(Double_t x, Double_t y, Double_t z) {fNewCHODPosition=TVector3(x,y,z);};

public:
  //Multiplicity variables
  void SetLKrMult      (Int_t val){fLKrMult=val;};
  void SetLKrHadrMult      (Int_t val){fLKrHadrMult=val;};
  void SetLKrCHODdR    (TVector3 val){fLKrCHODdR=val;};
  void SetLKrCHODdt    (Double_t val){fLKrCHODdt=val;};
  void SetLKrNewCHODdR (TVector3 val){fLKrNewCHODdR=val;};
  void SetLKrNewCHODdt (Double_t val){fLKrNewCHODdt=val;};
  void SetCHODNewCHODdR(TVector3 val){fCHODNewCHODdR=val;};
  void SetCHODNewCHODdt(Double_t val){fCHODNewCHODdt=val;};
  void SetLKrCHODdR    (Double_t x, Double_t y, Double_t z){fLKrCHODdR    =TVector3(x,y,z);};
  void SetLKrNewCHODdR (Double_t x, Double_t y, Double_t z){fLKrNewCHODdR =TVector3(x,y,z);};
  void SetCHODNewCHODdR(Double_t x, Double_t y, Double_t z){fCHODNewCHODdR=TVector3(x,y,z);};

  Int_t GetLKrMult() {return fLKrMult;};
  Int_t GetLKrHadrMult() {return fLKrHadrMult;};
  TVector3 GetLKrCHODdR(){return fLKrCHODdR;}
  TVector3 GetLKrNewCHODdR(){return fLKrNewCHODdR;}
  TVector3 GetCHODNewCHODdR(){return fCHODNewCHODdR;}
  Double_t GetLKrNewCHODdt (){return fLKrNewCHODdt;};
  Double_t GetCHODNewCHODdt(){return fCHODNewCHODdt;};
  Double_t GetLKrCHODdt    (){return fLKrCHODdt;};


private:
  Int_t fLKrMult;              //Multiplicity in LKr in time and space with CHOD and NewCHOD
  Int_t fLKrHadrMult;              //Multiplicity in LKr in time and space with CHOD and NewCHOD taking the rich-chod slope to search for hadronic shower signatures
  TVector3 fLKrCHODdR;
  TVector3 fLKrNewCHODdR;
  TVector3 fCHODNewCHODdR;
  Double_t fCHODNewCHODdt;
  Double_t fLKrNewCHODdt;
  Double_t fLKrCHODdt;

protected :
  Double_t fPiNexpectedHits;
  Double_t fPosNexpectedHits;
  Double_t fMuNexpectedHits;

  TLorentzVector fPionMomentum;            // 4-Momentum in muon hypothesis
  TLorentzVector fMuonMomentum;            // 4-Momentum in muon hypothesis
  TLorentzVector fElectronMomentum;        // 4-Momentum in positron hypothesis

  //Riccardo analyzer test variables
  Double_t fCaloTimeRA;                    // Calorimetric time by Riccardo
  Double_t fMuProb;                        // muon probability
  Double_t fElProb;                        // electron probability
  Double_t fPiProb;                        // pion probability
  Double_t fMIPdiscr;                      // mip discriminant < 1 for muons
  Bool_t fIsMuonDiscriminant;              // muon dis flag
  Bool_t fIsMIP;                           // mip flag
  Bool_t fIsMulti;                         // event with many clusters
  Bool_t fIsRAMuon;                         // event with positive ID from RA analyzer
  Bool_t fIsEM;                         // event with EM like cluster


  //Detector good matching flags

  bool fGoodCHOD;
  bool fGoodNewCHOD;
  bool fGoodRICHSR;
  bool fGoodRICHMR;
  bool fGoodLKr ;
  bool fGoodMUV1;
  bool fGoodMUV2;
  bool fGoodMUV3;

  Int_t fCounterV;
  Int_t fCounterH;
  Double_t fTimeV;
  Double_t fTimeH;
  Double_t fDownstreamTime;
  Double_t fCHODTime;                      // Time of the CHOD candidate matching the track wrt to the trigger time
  Double_t fNewCHODTime;                      // Time of the CHOD candidate matching the track wrt to the trigger time
  TVector3 fCHODPosition;                  // Position of the CHOD candidate matching the track
  TVector3 fNewCHODPosition;                  // Position of the CHOD candidate matching the track
  Int_t fCHODNewCHODMult;                      //Hit Multiplicity in CHOD-newchod
  Int_t fCHODMult;                         //Multiplicity in CHOD-lkr
  Int_t fCHODSlabsInTime;                         //Multiplicity in CHOD-lkr
  Int_t fNewCHODMult;                      //Multiplicity in NewCHOD-lkr

    Int_t fCHODRecoHitVID;
    Int_t fCHODRecoHitHID;
    Int_t fNewCHODRecoHitID;
  Int_t fMostLikelyHypothesis;
  Double_t fRICHMuLikelihood;
  Double_t fRICHPiLikelihood;
  Double_t fRICHElectronLikelihood;
  Int_t fIsRICHSingleRingCandidateSlava;
  Double_t fIsRICHPion;

  Bool_t         fRICHMultiIsCandidate;    // Flag indicating the presence of a multiring-RICH candidate
  Bool_t         fRICHMultiIsCandidateMomRange;    // Flag indicating the presence of a multiring-RICH candidate in the 15-35 region
  TLorentzVector fRICHMultiMomentum;       // Momentum of the track as measured by the multiring-RICH candidate under the pion hypothesis
  Double_t       fRICHMultiRadius;           // Radius of the Particle provided by the multiring-RICH candidate
  Double_t       fRICHSingleRadius;           // Radius of the Particle provided by the singlering-RICH candidate
  Double_t       fRICHMultiMass;           // Mass of the Particle provided by the multiring-RICH candidate
  Double_t       fRICHMultiTime;           // Time of the multiring-RICH candidate
  Double_t       fRICHMultiChi2;           // Chi2 of the multiring-RICH candidate
  Int_t         fRICHSingleIsCandidate;   // Flag indicating the presence of a singlering-RICH candidate
  TLorentzVector fRICHSingleMomentum;      // Momentum of the track as measured by the singlering-RICH candidate under the pion hypothesis
  Double_t       fRICHSingleMass;          // Mass of the Particle provided by the singlering-RICH candidate
  Double_t       fRICHSingleTime;          // Time of the multiring-RICH candidate
  Double_t       fRICHSingleChi2;          // Chi2 of the singlering-RICH candidate
  Int_t fRICHAssoNHit;
    Double_t   fRICHSingleDiscriminant;
    Double_t   fRICHMultiDiscriminant;
  Int_t fLKrID;                            // ID of the LKr cluster matching the track
  Double_t fLKrTime;                       // Time of the LKr cluster matching the track
  Double_t fLKrEovP;                       // E/P of the LKr cluster matching the track
  Double_t fLKrSeedEnergy;                 // Energy of the seed (max energy cell in case of improved reco)
  Int_t fLKrNCells;                        // Number of cells
  TVector2 fLKrPosition;                   // XY of the LKr cluster matching the track
    bool fLKrMerged;

  Int_t fMUV1ID;                           // ID of the MUV1 cluster matching the track
  Double_t fMUV1Energy;                    // MUV1 energy associated to the track
  Double_t fMUV1OuterEnergy;                    // MUV1 energy associated to the track
  Double_t fMUV1OuterNhits;                // MUV1 nhits in time
  Double_t fMUV1Time;                      // Time of the MUV1 cluster matching the track
  Double_t fMUV1ShowerWidth;               // Shower width of the MUV1 cluster matching the track
  Double_t fMUV1SeedRatio;                 // Seed Ratio width of the MUV1 cluster matching the track
  Double_t fMUV1EMerged;

  Int_t fMUV2ID;                           // ID of the MUV2 cluster matching the track
  Double_t fMUV2Energy;                    // MUV2 energy associated to the track
  Double_t fMUV2OuterEnergy;                    // MUV2 energy associated to the track
  Double_t fMUV2OuterNhits;                // MUV2 nhits in time
  Double_t fMUV2Time;                      // Time of the MUV2 cluster matching the track
  Double_t fMUV2ShowerWidth;               // Shower width of the MUV2 cluster matching the track
  Double_t fMUV2SeedRatio;                 // Seed Ratio width of the MUV2 cluster matching the track

  Double_t fCalorimetricEnergy;            // Calorimetric (LKr+MUV1+MUV2) associated to the track

  Int_t fMUV3ID;                           // ID of the MUV3 candidate matching the track
  Int_t fMUV3PosID;                        // Positive ID of the MUV3 candidate matching the track in time and space
  Double_t fMUV3Time;                      // Time of the MUV3 candidate matching the track
  TVector2 fMUV3Distance;                  // Distance of the MUV3 candidate from the track extrapolation

  Int_t fUpstreamTrackID;                  // ID of the upstream track associated to this track
    Int_t fTrackID;                  // ID of the upstream track associated to this track
  Int_t fTrackCharge;                  // charge of the downstream track

  TVector3 fPosAtGTK[3];                   // Position at GTK1,2,3
  TVector3 fPosAtStraw[4];                 // Position at Straw1,2,3,4
  TVector3 fPosAtRICH[3];                  // Position at RICH, in and out windows
  TVector3 fPosAtLAV[12];                  // Position at LAVs'
  TVector3 fPosAtCHOD[3];                  // Position at CHOD V,H, average
  TVector3 fPosAtNewCHOD[3];               // Position at CHOD V,H, average
  TVector3 fPosAtIRC[2];                   // Position at IRC and SAC
  TVector3 fPosAtLKr;                      // Position at LKr
  TVector3 fPosAtMUV[3];                   // Position at MUV1,2,3

  // Photon candidates assoiacted to the downstream track
  Bool_t fIsPhotonLKrCandidate;            // Flag to indicate the existence of a photon candidate in lkr
  Bool_t fIsPhotonLKrActivity;             // Flag to indicate the existence of a photon hits in time in the lkr
  Bool_t fIsLAVTrack;            // Flag to indicate the existence of a photon candidate in lkr
  Bool_t fIsPhotonLAVCandidate;            // Flag to indicate the existence of a photon candidate in lav
  Bool_t fIsPhotonIRCCandidate;            // Flag to indicate the existence of a photon candidate in irc
  Bool_t fIsPhotonSACCandidate;            // Flag to indicate the existence of a photon candidate in sac
  Bool_t fIsPhotonSAVCandidate;            // Flag to indicate the existence of a photon candidate in sac
  Bool_t fIsPhotonNewLKrCandidate;            // Flag to indicate the existence of a photon candidate in lkr

  Int_t fNPhotonLKrCandidates;               // Number of photon candidates in LKr
  Double_t fPhotonLKrCandidateTime[10];      // Time of a photon candidate in LKr
  TVector2 fPhotonLKrCandidatePosition[10];  // Position of a photon candidate in LKr
  Double_t fPhotonLKrCandidateEnergy[10];    // Energy of a photon candidate in LKr
  Int_t fPhotonLKrCandidateNCells[10];       // Number of cells forming a photon candidate in LKr
  Int_t fNPhotonNewLKrCandidates;            // Number of new photon candidates in LKr

  Int_t fNPhotonLAVCandidates;               // Number of photon candidates in LAV
  Double_t fPhotonLAVCandidateTime[80];      // Time of a photon candidate in LAV
  Int_t fPhotonLAVCandidateID[80];           // ID of a photon candidate in LAV
  Int_t fPhotonLAVCandidateLAVID[80];        // LAV ID of a photon candidate

  Int_t fNPhotonIRCCandidates;               // Number of photon candidates in IRC
  Double_t fPhotonIRCCandidateTime[20];      // Time of a photon candidate in IRC
  Int_t fPhotonIRCCandidateID[20];           // ID of a photon candidate in IRC

  Int_t fNPhotonSACCandidates;               // Number of photon candidates in SAC
  Double_t fPhotonSACCandidateTime[20];      // Time of a photon candidate in SAC
  Int_t fPhotonSACCandidateID[20];           // ID of a photon candidate in SAC

  Int_t fNPhotonSAVCandidates;               // Number of photon candidates in SAC or IRC CREAM
};

#endif
