#ifndef GigaTrackerAnalysis_h
#define GigaTrackerAnalysis_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include <TF1.h>

#include "UserMethods.hh"
#include "TRecoGigaTrackerEvent.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "GigaTrackerCandidate.hh"
#include "UpstreamParticle.hh"

class Parameters;
class AnalysisTools;
class TRandom3;

class GigaTrackerAnalysis
{
  public :
    GigaTrackerAnalysis(NA62Analysis::Core::BaseAnalysis *);
    virtual ~GigaTrackerAnalysis();
    UserMethods *GetUserMethods() { return fUserMethods;};
    void StartBurst(Int_t,Int_t,Int_t,double*);
    void Clear();
    int GetNGigaTrackerCandidates() {return fNGigaTrackerCandidates;};
    GigaTrackerCandidate *GetGigaTrackerCandidate(int k) {return fGigaTrackerCandidate[k];};
    int GetNGigaTrackerGTK1Hits() {return fNGigaTrackerGTK1Hits;};
    int *GetGigaTrackerGTK1Hit() {return fGigaTrackerGTK1Hit;};
    int GetNGigaTrackerGTK2Hits() {return fNGigaTrackerGTK2Hits;};
    int *GetGigaTrackerGTK2Hit() {return fGigaTrackerGTK2Hit;};
    int GetNGigaTrackerGTK3Hits() {return fNGigaTrackerGTK3Hits;};
    int *GetGigaTrackerGTK3Hit() {return fGigaTrackerGTK3Hit;};
    int GetNHits() {return fNHits;};

  public:

  private:
    Parameters *fPar;
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    TRandom3 *fRand;
    int fNGigaTrackerCandidates;
    GigaTrackerCandidate *fGigaTrackerCandidate[10];
    int fNGigaTrackerGTK1Hits;
    int *fGigaTrackerGTK1Hit;
    int fNGigaTrackerGTK2Hits;
    int *fGigaTrackerGTK2Hit;
    int fNGigaTrackerGTK3Hits;
    int *fGigaTrackerGTK3Hit;
    Int_t fYear;
    Int_t fBurst;
    Int_t fRun;
    double fTriggerOffset;
    double fTOffset[25];
    bool fIsFilter;
    bool fIsMC;
    double ftOffset[30];
    int fNHits;
    double fTimeRef;

  private:
    Double_t fCDADT_triplet[400][100];
    Double_t fNORMK_triplet;
    Double_t fCDADT_doublet[400][100];
    Double_t fNORMK_doublet;
    Double_t GTKDiscriminant(Int_t,Double_t,Double_t);
    Double_t PDFTrack(Int_t,Int_t,Int_t);
    TF1 *fTW[3][10][45];

  private:
    TRecoGigaTrackerEvent *fGigaTrackerEvent;
    Double_t fGTKOffset;
    Double_t fZGTK[3];
    Bool_t fIsTriplet;
    Bool_t fIsDoublet;
    double fKaonX[3];
    double fKaonY[3];

  private:

  // Standard reconstruction
  public:
    Int_t TrackCandidateMatching(Int_t,Double_t,TRecoGigaTrackerEvent*,TRecoSpectrometerCandidate*);
    void ComputeCDADTNormalization(Double_t a[][100], Double_t b[][100]);
    TLorentzVector GetMomentum(Double_t,Double_t,Double_t,Double_t);

  // ALternative reconstruction
  public:
    // int ReconstructCandidateNoGTK(TRecoSpectrometerCandidate*);
  int ReconstructCandidateNoGTK(TRecoGigaTrackerEvent*,TRecoSpectrometerCandidate*);
    int ReconstructCandidates(TRecoGigaTrackerEvent*,double,double,bool);
    double GetTimeOff() {return fTOffset[0];};
    double GetTimeShift() {return fTimeShift;};
    void SetTimeShift(double val) {fTimeShift=val;};
    double GetTimeCorr() {return fTCorr;};
    void SetTimeCorr(double val) {fTCorr=val;};
    void SetKaonX(double val, int j) {fKaonX[j]=val;};
    void SetKaonY(double val, int j) {fKaonY[j]=val;};
    void SetReferenceDetector(int val) {fReferenceDetector=val;};
    void SetRecoTimingCut(double val) {fRecoTimingCut=val;};
    double GetRecoTimingCut() {return fRecoTimingCut;};
    void SetMatchingTimingCut(double val1, double val2) {fMatchingTimingCut[0]=val1;fMatchingTimingCut[1]=val2;};
    double GetMatchingTimingCut(int j) {return fMatchingTimingCut[j];};
    void SetEarlyDecayFlag(bool i) {fEarlyDecayFlag=i;};
    int TrackCandidateMatching(TRecoGigaTrackerEvent*,UpstreamParticle*,double,int,int); // 3 pion testing
    int TrackCandidateMatching(TRecoGigaTrackerEvent*,TRecoSpectrometerCandidate*,Double_t,int,int);
    Double_t Chi2True(TRecoGigaTrackerCandidate*,UpstreamParticle*);
    Double_t Chi2TrueTest(TRecoGigaTrackerCandidate*,TRecoGigaTrackerCandidate*);
    Double_t Chi2Test(TRecoGigaTrackerCandidate*,UpstreamParticle*);
    Double_t Chi2Event(TRecoGigaTrackerCandidate*);
    Double_t Discriminant(Double_t, Double_t);
    void Discriminant(Double_t, Double_t, double*);
    TVector3 ComputeVertex(TRecoGigaTrackerCandidate*,TRecoSpectrometerCandidate*,Double_t*);
  //TVector3 ComputeVertex(TVector3*,TVector3*,TRecoSpectrometerCandidate*,TVector3*);

  protected:
    void ApplyTimeCorrections();
    void CorrectHitTime(TRecoGigaTrackerHit*);
    void DiscriminantNormalization();

    struct StationOrder{
      TRecoGigaTrackerEvent* fevent;
      StationOrder(TRecoGigaTrackerEvent* p) : fevent(p) {};
      bool operator() ( int i, int j ){
        TRecoGigaTrackerHit* h1 = (TRecoGigaTrackerHit*) fevent->GetHit(i);
        TRecoGigaTrackerHit* h2 = (TRecoGigaTrackerHit*) fevent->GetHit(j);
        return h1->GetStationNo()<h2->GetStationNo();
      }
    };
    Bool_t BuildTrack(int,int,TRecoGigaTrackerEvent*,int);
    void LinearLeastSquareFit(Double_t *x, Double_t *y, Int_t Nsample, Double_t *sigma, Double_t &a, Double_t &b, Double_t &rho, Double_t &chi2);

    struct DeltaTimeCondition{
      GigaTrackerAnalysis* fanalysis;
      TRecoGigaTrackerEvent* fevent;
      Double_t ftime;
      DeltaTimeCondition(GigaTrackerAnalysis* a, TRecoGigaTrackerEvent* p, Double_t t) : fanalysis(a), fevent(p), ftime(t) {};
      bool operator() ( int i ){
        TRecoGigaTrackerCandidate* c1 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(i);
        Double_t dt = c1->GetTime()-ftime;
        double dtmin = fanalysis->GetMatchingTimingCut(0);
        double dtmax = fanalysis->GetMatchingTimingCut(1);
        return (dt>dtmin&&dt<dtmax); // Time condition added
      }
    };

    struct Chi2TrueCondition{
      GigaTrackerAnalysis* fanalysis;
      TRecoGigaTrackerEvent* fevent;
      UpstreamParticle* fkaon;
      Chi2TrueCondition(GigaTrackerAnalysis* a, TRecoGigaTrackerEvent* p, UpstreamParticle* k) : fanalysis(a), fevent(p), fkaon(k) {};
      bool operator() ( int i, int j ){
        TRecoGigaTrackerCandidate* c1 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(i);
        TRecoGigaTrackerCandidate* c2 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(j);
        Double_t chi2_1 = fanalysis->Chi2True(c1,fkaon);
        Double_t chi2_2 = fanalysis->Chi2True(c2,fkaon);
        if (chi2_1==chi2_2) chi2_1=c1->GetChi2(),chi2_2=c2->GetChi2();
        return chi2_1<chi2_2;
      }
    };

    struct Chi2Condition{
      GigaTrackerAnalysis* fanalysis;
      TRecoGigaTrackerEvent* fevent;
      Double_t ftime;
      Chi2Condition(GigaTrackerAnalysis* a, TRecoGigaTrackerEvent* p, Double_t t) : fanalysis(a), fevent(p), ftime(t) {};
      bool operator() ( int i ){
        TRecoGigaTrackerCandidate* c1 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(i);
        Double_t chi2 = c1->GetChi2();
        Double_t chi2_ev = fanalysis->Chi2Event(c1);
        Double_t dt = c1->GetTime()-ftime;
        double dtmin = fanalysis->GetMatchingTimingCut(0);
        double dtmax = fanalysis->GetMatchingTimingCut(1);
//        return (chi2<80&&chi2_ev<20&&dt>dtmin&&dt<dtmax); // Time condition added
        return (chi2<50&&chi2_ev<20&&dt>dtmin&&dt<dtmax); // Time condition added
      }
    };

    struct DiscriminantCondition{
      GigaTrackerAnalysis* fanalysis;
      TRecoGigaTrackerEvent* fevent;
      TRecoSpectrometerCandidate* ftrack;
      Double_t ftime;
      int fiD;
      DiscriminantCondition(GigaTrackerAnalysis* a, TRecoGigaTrackerEvent* p, TRecoSpectrometerCandidate* s, Double_t t, int k) : fanalysis(a), fevent(p), ftrack(s), ftime(t), fiD(k) {};
      bool operator() ( int i, int j ){
        TRecoGigaTrackerCandidate* c1 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(i);
        Double_t cda = 9999999.;
        TVector3 vtx = fanalysis->ComputeVertex(c1,ftrack,&cda);
        Double_t dt = c1->GetTime()-ftime;
        double discr1[2];
        fanalysis->Discriminant(cda,dt,discr1);
        TRecoGigaTrackerCandidate* c2 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(j);
        cda = 9999999.;
        vtx = fanalysis->ComputeVertex(c2,ftrack,&cda);
        dt = c2->GetTime()-ftime;
        double discr2[2];
        fanalysis->Discriminant(cda,dt,discr2);
        return (discr1[fiD]<discr2[fiD]);
      }
    };

    struct DifferenceCondition{
      GigaTrackerAnalysis* fanalysis;
      TRecoGigaTrackerEvent* fevent;
      TRecoSpectrometerCandidate* ftrack;
      Double_t ftime;
      int fiL;
      DifferenceCondition(GigaTrackerAnalysis* a, TRecoGigaTrackerEvent* p, TRecoSpectrometerCandidate* s, Double_t t, int k) : fanalysis(a), fevent(p), ftrack(s), ftime(t), fiL(k) {};
      bool operator() ( int i ){
        TRecoGigaTrackerCandidate* c1 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(i);
        Double_t cda1 = 9999999.;
        TVector3 vtx1 = fanalysis->ComputeVertex(c1,ftrack,&cda1);
        Double_t dt1 = c1->GetTime()-ftime;
        Double_t discr1[2];
        fanalysis->Discriminant(cda1,dt1,discr1);
        TRecoGigaTrackerCandidate* c2 = (TRecoGigaTrackerCandidate*)fevent->GetCandidate(fiL);
        Double_t cda2 = 9999999.;
        TVector3 vtx2 = fanalysis->ComputeVertex(c2,ftrack,&cda2);
        Double_t dt2 = c2->GetTime()-ftime;
        Double_t discr2[2];
        fanalysis->Discriminant(cda2,dt2,discr2);
//        return (fabs(discr2[0]-discr1[0])<0.1);
        return (fabs(discr2[0]-discr1[0])<0.3);
      }
    };

    double fTimeShift;
    double fTCorr;
    double fRecoTimingCut;
    double fMatchingTimingCut[2];
    int fReferenceDetector;
    bool fEarlyDecayFlag;
    vector<double> fPDFKaonCDA[2];
    vector<double> fPDFPileCDA[2];
    vector<double> fPDFKaonDT[2];
    vector<double> fPDFPileDT[2];
    Double_t fIntPDFKaonCDA[2];
    Double_t fIntPDFKaonDT[2];
    Double_t fIntPDFPileCDA[2];
    Double_t fIntPDFPileDT[2];
    Double_t PDFKaonCDA(Double_t);
    Double_t PDFPileCDA(Double_t);
    Double_t PDFKaonDT(Double_t);
    Double_t PDFPileDT(Double_t);
    Double_t EvaluateCondition(Double_t var, Double_t cut, Double_t integral) { return (var>cut)?integral:-1; }
    Double_t DiffDiscr(TRecoGigaTrackerEvent*,TRecoSpectrometerCandidate*,Double_t,int,int,int);

    // Hit Matching
    public:
    void TrackHitMatching(TRecoGigaTrackerEvent*,TRecoSpectrometerCandidate*,Double_t,int);
    TVector2 GetGTK3Pos(TRecoSpectrometerCandidate*);

    protected:
    struct StationCondition{
      TRecoGigaTrackerEvent* fevent;
      int fSt;
      StationCondition(TRecoGigaTrackerEvent* p, int s) : fevent(p), fSt(s) {};
      bool operator() ( int i ){
        TRecoGigaTrackerHit* h1 = (TRecoGigaTrackerHit*) fevent->GetHit(i);
        return (h1->GetStationNo()==fSt);
      }
    };

    struct TimeCondition{
      GigaTrackerAnalysis* fanalysis;
      TRecoGigaTrackerEvent* fevent;
      double ftime;
      TimeCondition(GigaTrackerAnalysis* a, TRecoGigaTrackerEvent* p, double t) : fanalysis(a), fevent(p), ftime(t) {};
      bool operator() ( int i ){
        TRecoGigaTrackerHit* h1 = (TRecoGigaTrackerHit*) fevent->GetHit(i);
        return (fabs(h1->GetTime()-ftime)<fanalysis->GetRecoTimingCut());
      }
    };

    struct PositionCondition{
      GigaTrackerAnalysis* fanalysis;
      TRecoGigaTrackerEvent* fevent;
      TRecoSpectrometerCandidate *ftrack;
      PositionCondition(GigaTrackerAnalysis* a, TRecoGigaTrackerEvent* p, TRecoSpectrometerCandidate *t) : fanalysis(a), fevent(p), ftrack(t) {};
      bool operator() ( int i, int j ){
        TRecoGigaTrackerHit* h1 = (TRecoGigaTrackerHit*) fevent->GetHit(i);
        TRecoGigaTrackerHit* h2 = (TRecoGigaTrackerHit*) fevent->GetHit(j);
        TVector2 pos = fanalysis->GetGTK3Pos(ftrack);
        Double_t d1 = (h1->GetPosition().XYvector()-pos).Mod();
        Double_t d2 = (h2->GetPosition().XYvector()-pos).Mod();
        return (d1<d2);
      }
    };
};

#endif
