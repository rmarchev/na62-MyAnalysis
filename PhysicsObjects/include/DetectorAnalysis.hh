#ifndef DETECTORANALYSIS_HH
#define DETECTORANALYSIS_HH 1

#include "TDetectorVEvent.hh"
#include "UserMethods.hh"
#include "TRecoSpectrometerEvent.hh"
#include "TRecoSpectrometerCandidate.hh"
#include <vector>
#include "Analyzer.hh"

//namespace rado{

//class VDetectorRapper {
//public:
//  VDetectorRapper(TDetectorVEvent* evt);
//
//  //VDetectorRapper(): fTime(-99999) {}
//  double GetTime()const{return fTime;}
//  double GetX()const{return fX;}
//  double GetY()const{return fY;}
//  double GetZ()const{return fZ;}
//  void SetX(double val){ fX = val;}
//  void SetY(double val){ fY = val;}
//  void SetZ(double val){ fZ = val;}
//  TDetectorVEvent* GetEvent(){return fVEvt;}
//protected:
//  int fNCand;
//  double fTime;
//  double fX;
//  double fY;
//  double fZ;
//  TDetectorVEvent* fVEvt;
//};

// class StrawRapper: public VDetectorRapper {
class StrawRapper {
public:
  StrawRapper(){}
  // StrawRapper(TDetectorVEvent* evt, NA62Analysis::Core::BaseAnalysis *ba);
  // StrawRapper(TRecoSpectrometerEvent* evt,NA62Analysis::Core::BaseAnalysis *ba);
  StrawRapper(NA62Analysis::Core::BaseAnalysis *ba);

  UserMethods *GetUserMethods() { return fUserMethods;};
  ~StrawRapper();
  void SetParameters();
  // void Analysis();
  void Analysis(TRecoSpectrometerEvent* track);
  void ApplyAlphaBeta(double alpha,double beta,TRecoSpectrometerCandidate* track);

  TRecoSpectrometerEvent* GetEvent(){return fEvt;}

  void SetZ1(double val){ fZ1 = val;}
  void SetZ2(double val){ fZ2 = val;}
  void SetZ3(double val){ fZ3 = val;}
  void SetZ4(double val){ fZ4 = val;}

  double GetZ1(){ return fZ1;}
  double GetZ2(){ return fZ2;}
  double GetZ3(){ return fZ3;}
  double GetZ4(){ return fZ4;}

  //double GetMomentum(){ return fMom;}
  //double GetMomentumBeforeFit(){ return fMom;}
  //double GetNCandidates(){ return fNCand;}

private:

  //Position in all of the 4 straws
  double fX1;
  double fX2;
  double fX3;
  double fX4;
  double fY1;
  double fY2;
  double fY3;
  double fY4;
  double fZ1;
  double fZ2;
  double fZ3;
  double fZ4;

  int fNCand;
  double fNChambers;
  double fMomBeforeFit;
  double fMom;
  UserMethods* fUserMethods;
  TRecoSpectrometerEvent* fEvt;
};
//}


#endif
