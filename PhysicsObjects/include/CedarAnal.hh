#ifndef KTAGAnalysis_h
#define KTAGAnalysis_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoCedarEvent.hh"
#include "KTAGCandidate.hh"

class AnalysisTools;
class KTAGCandidate;

class CedarAnal
{
public :
  CedarAnal(Int_t flag, NA62Analysis::Core::BaseAnalysis *);
  virtual ~CedarAnal(){}
  UserMethods *GetUserMethods() { return fUserMethods;};
  void Clear();
  void StartBurst(int burstid, double *off) ;
  void SetEvent(TRecoCedarEvent* val){fCedarEvent = val;}
  void SetTimeCorr(double val){fTCorr = val;}
  double GetTimeCorr(){return fTCorr;}
  Int_t TrackMatching(Int_t,Double_t,Double_t,double ,bool );
  Int_t TrackMatchingSimple(Double_t timeref,int iC, bool mcflag, int flag) ;
  KTAGCandidate *GetKTAGCandidate() {return fCedarCandidate;};

public:

private:
  AnalysisTools *fTool;
  UserMethods *fUserMethods;
  KTAGCandidate *fCedarCandidate;
  double fTOffset[25];
  double fTimeOffset;
  double fTCorr;
  int fFlag;
  bool fIsMC;
private:
  TRecoCedarEvent *fCedarEvent;

private:
};

#endif
