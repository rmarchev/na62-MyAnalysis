#ifndef StrawRapper_h
#define StrawRapper_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoSpectrometerEvent.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "StrawCandidate.hh"
#include "StrawHit.hh"
#include "StrawCluster.hh"
#include "StrawIntersection.hh"

#include "VertexLSF.hh"

#include "CHODAnal.hh"
#include "LKrAnal.hh"

class AnalysisTools;

class StrawRapper
{
  public :
    StrawRapper(Int_t, NA62Analysis::Core::BaseAnalysis *,CHODAnal*,LKrAnal*);
    virtual ~StrawRapper();
    UserMethods *GetUserMethods() { return fUserMethods;};
    void StartBurst();
    void Clear();
    Bool_t SelectTrack(Int_t,TRecoSpectrometerEvent*);
    StrawCandidate *GetStrawCandidate() {return fStrawCandidate;};
    StrawHit *GetStrawCandidateHit() {return fStrawCandidateHit;};
    StrawHit *GetStrawNoCandidateHit() {return fStrawNoCandidateHit;};
    int GetStrawClustersNoMatched(int jc, int jv) {return fStrawCluster[jc][jv]->GetNClusters();};

  private:
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    CHODAnal *fCHODAnalysis;
    LKrAnal *fLKrAnalysis;
    TRecoSpectrometerEvent *fSpectrometerEvent;
    StrawCandidate *fStrawCandidate;
    TRecoLKrEvent *fLKr;

  private:
    Int_t fFlag;
    Double_t fZStation[4];
    Double_t fXCenter[4];
    VertexLSF fVertexLSF;

  private:
    void CreateStrawGeometry();
    bool FakeTrack(int,int);
    Bool_t MultiTrack(int);
    void Test2TrackVertex(int,int,TVector3*);
    Int_t Acceptance(TRecoSpectrometerCandidate*);
  Bool_t InRICHAcceptance(TRecoSpectrometerCandidate* track);
  Int_t GetTotalAcceptance();
  public:
  void AnalyzeHits(TRecoSpectrometerEvent *,int,int);
  void AnalyzeHits(TRecoSpectrometerEvent *,int,int,int);
  void LoadEvent(TRecoLKrEvent*);
  Double_t ReconstructSegments(TVector3 *,double, int);

  // Segment reconstruction
  private:
  void SolveLR();
////  void OneViewSegment(Int_t,Int_t,Int_t);
  void OneViewSegment(int);
  void ComputeSegment(Int_t,Int_t,Int_t);
  void MultipleViewSegment(Int_t chamber,Int_t,Int_t,Double_t,Double_t,Double_t,Double_t);
  void ComputeSegment(Double_t ,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t);
  void CountTrackHits();
  void FillTrackHitsArray();
  void FillTrackHitsArray(TRecoSpectrometerCandidate*);
  void FillNoTrackHitsArray();
  Int_t GetTrackHitIndex(Int_t);
  Double_t SigmaRadius(Double_t);
  Double_t GetYCoordinate(Double_t,Int_t,Double_t,Int_t);
  Double_t GetXCoordinate(Double_t,Int_t,Double_t,Int_t);
  double GetLocalCoordinate(TRecoSpectrometerCandidate*,int,double);
  double ChooseTheCombination(double*,double*,double*,double*,double*,double*);
  double Chi2LinearFit(double*,double*,double*,double*,double*);
  bool Pairing(double*,int*);
  void BuildChamberHit(int);
  int IntType1(int);
  int IntType2(int,int);
  int IntType3(int,int);
  void ComputeCoordinate(const int &, const double &, double*);
  void ComputeCoordinate(const int &, const double &, const double &, double*);
  int IntersectionQuality(int,double*,double,double*);
  void UpdateCoordinate(int,double*,double);
  std::vector<StrawIntersection>::iterator GetHit(int cH, int j) { return fChamberHit[cH].begin()+j; };
  bool AcceptanceTwoView(int,double*);
  int StoreHit(int,double*,int*,double*);
  int StrawAcceptance(int,double*,int);

  private:
  int fEventNumber;
  Double_t fVertex[6];
  Double_t fSIB;
  Double_t fMinIB;
  double fMinChi2[2];
  double fMinDist[2];
  double fMinTime[2];
  double fMinXCoor[2][2];
  double fMinYCoor[2][2];
  double fMinSlopeX[2];
  double fMinSlopeY[2];
  double fPExp;
  double fMinYCh1;
  double fMinLKrDT;
  double fMinLKrDX;
  double fMinLKrDY;
  double fMinLKrD;
  double fSaveTime;
  Int_t fNHits;
  Int_t fNTrackHits;
  Int_t fNNoTrackHits;
  Int_t fmv1;
  Int_t fmv2;
  Double_t fDeltaZ1;
  Double_t fDeltaZ2;
  Double_t fDeltaR1;
  Double_t fDeltaR2;
  Int_t fCountMultipleView;
  TRecoSpectrometerCandidate* fThisTrack;
  TRecoSpectrometerCandidate* fThisTrack2;
  StrawHit *fStrawCandidateHit;
  StrawHit *fStrawNoCandidateHit;
  StrawCluster *fStrawCluster[4][4];
  double fStrawHitLocalX[4][4][4][122];
  double fStrawHitGlobalZ[4][4][4][122];
  double fHoleChamberMax[4][4];
  double fHoleChamberMin[4][4];
  double fViewPlaneTransverseSize;
  double fChamberZPosition[4];
  int fIDTrack;
  std::vector<StrawIntersection> fChamberHit[4]; //< Vector of reconstructed chamber-hits.

  protected:
   struct HitCommon{
     TRecoSpectrometerEvent *fev;
     int fidtr;
     HitCommon(TRecoSpectrometerEvent *e,int id) : fev(e), fidtr(id) {};
     bool operator() (int i){
       if (i==fidtr) return false;
       int nhcomm(0);
       TRecoSpectrometerCandidate *cj = (TRecoSpectrometerCandidate *)fev->GetCandidate(fidtr);
       int *hcj = cj->GetHitsIndexes();
       TRecoSpectrometerCandidate *ci = (TRecoSpectrometerCandidate *)fev->GetCandidate(i);
       int *hci = (int *)ci->GetHitsIndexes();
       for (int jh(0); jh<cj->GetNHits(); jh++) {
         for (int ih(0); ih<ci->GetNHits(); ih++) {
           if (hci[ih]==hcj[jh]) nhcomm++;
         }
       }
       return nhcomm>1?true:false;
     }
   };

   struct TrackQualityCondition{
     StrawRapper *fthis;
     TRecoSpectrometerEvent* fevent;
     int fjT;
     TrackQualityCondition(StrawRapper* t, TRecoSpectrometerEvent* a, int k) : fthis(t),fevent(a),fjT(k) {};
     bool operator() ( int i ){
       if (i==fjT) return true;
       if (fthis->FakeTrack(i,0)) return true;
       return false;
     }
   };

   struct PlaneChamberCondition{
     int fc;
     int fv;
     int fp;
     TRecoSpectrometerEvent* fevent;
     StrawHit* fhit;
     PlaneChamberCondition(int c, int v, int p, TRecoSpectrometerEvent* e, StrawHit* h) : fc(c),fv(v),fp(p),fevent(e),fhit(h) {};
     bool operator() ( int i ){
       int idHit = fhit->GetIndex(i);
       TRecoSpectrometerHit *hit = (TRecoSpectrometerHit *)fevent->GetHit(idHit);
       int jPlane = 2*hit->GetHalfViewID()+hit->GetPlaneID();
       if (hit->GetChamberID()==fc && hit->GetViewID()==fv && jPlane==fp) return false;
       return true;
     }
   };

};

#endif
