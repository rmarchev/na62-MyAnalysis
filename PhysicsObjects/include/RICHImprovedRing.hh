#ifndef RICHImprovedRing_H
#define RICHImprovedRing_H 1

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TMinuit.h"
using namespace std;

class AnalysisTools;
class TRecoRICHEvent;
class TRecoRICHCandidate;
class TRecoRICHHit;

class RICHImprovedRing
{
  public:
    RICHImprovedRing();
    ~RICHImprovedRing();
////    Bool_t Chi2Fit(TRecoRICHEvent*,TRecoRICHCandidate*,TVector3,TVector3,Double_t**,Double_t*);
////    Bool_t Chi2Fit(TRecoRICHEvent*,TRecoRICHCandidate*,TVector3,TVector3,Double_t*,Double_t*);
    Bool_t Chi2Fit(TRecoRICHEvent*,TRecoRICHCandidate*,TVector3,int,Double_t*,Double_t*);
    static void RingChi2FCN(Int_t &, Double_t *, Double_t &, Double_t *, Int_t);
    Double_t RingChi2(Double_t *);
    Double_t GetDeltaMisa(Int_t i, Int_t j) {return fDeltaMisa[i][j];};
    void SetYear(Int_t val) {fYear=val;};
    void SetIsMC(bool val) {fIsMC=val;};

  private:
    AnalysisTools *tools;
    int fIdBad;
    TVector2 GetChPosAngCorr(Int_t);

  private:
    Int_t fNPars;
    TMinuit *fFitter;
    Double_t **fDeltaMisa;
    double *fmirroralign;
    Double_t p[25][6][2];
    Double_t v[25][25][25][2];
    Double_t ac[25][28];
    Double_t bcost[25][28];
    Int_t fakemirror[25][25];
    Int_t fYear;
    bool fIsMC;
    int fjMax;
};

#endif
