#ifndef SAVAnalysis_h
#define SAVAnalysis_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>

#include "UserMethods.hh"
#include "TRecoSAVEvent.hh"
#include "PhotonVetoCandidate.hh"

class Parameters;
class AnalysisTools;

class SAVAnal
{
  public :
    SAVAnal(NA62Analysis::Core::BaseAnalysis *);
  virtual ~SAVAnal(){}
    UserMethods *GetUserMethods() { return fUserMethods;};
    void StartBurst(Int_t,Int_t);
    void Clear();
    PhotonVetoCandidate *GetPhotonCandidate(Int_t j) {return fPhotonCandidate[j];};

  public:
    Int_t MakeCandidate(Double_t,TRecoSAVEvent*);

  private:
    Parameters *fPar;
    AnalysisTools *fTools;
    UserMethods *fUserMethods;
    PhotonVetoCandidate *fPhotonCandidate[20];

  private:
    Double_t *fSAVChannelT0;
    Double_t fTimeCut;
};

#endif
