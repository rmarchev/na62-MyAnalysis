#ifndef MULTITRACKANAL_HH
#define MULTITRACKANAL_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include "Parameters.hh"
#include "UserMethods.hh"
#include "TRecoCHODEvent.hh"
#include "TRecoLKrEvent.hh"
#include "LKrAuxClusterReco.hh"
#include "TRecoNewCHODEvent.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "CHODCandidate.hh"
#include "Parameters.hh"
#include "AnalysisTools.hh"
#include "EventHeader.hh"
#include "Constants.hh"
#include "LKrAuxClusterReco.hh"
#include "NA62Exceptions.hh"
#include "TSystem.h"
#include "GeometricAcceptance.hh"
#include "DownstreamParticle.hh"
#include "UpstreamParticle.hh"


class AnalysisTools;
class LKrAuxClusterReco;

class MultiTrackAnal{
public:
  MultiTrackAnal(int flag,TString dir,NA62Analysis::Core::BaseAnalysis *ba);
  ~MultiTrackAnal(){};

  Int_t ComputeMultiplicity(DownstreamParticle* track,UpstreamParticle* beamtrack);
  void SetDownstreamParticle(DownstreamParticle* val){fTrack=val;};
  void SetUpstreamParticle(UpstreamParticle* val){fBeamTrack=val;};
  void SetDecayType(Int_t flag);
  void Print();

  TRecoMUV0Event* GetMUV0Event(){return fMUV0Event;};
  TRecoHACEvent* GetHACEvent(){return fHACEvent;};
  void SetMUV0Event(TRecoMUV0Event* val){fMUV0Event=val;};
  void SetHACEvent(TRecoHACEvent* val){fHACEvent=val;};
  UserMethods* GetUserMethods(){return fUserMethods;}
private:
  int fFlag;
  TString fDecay;
  DownstreamParticle* fTrack;
  UpstreamParticle* fBeamTrack;
  UserMethods* fUserMethods;

  TRecoMUV0Event* fMUV0Event;
  TRecoHACEvent*  fHACEvent;

  Bool_t fIsHAC;
  Bool_t fIsMUV0;

};

#endif
