#ifndef CHANTICandidate_h
#define CHANTICandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class CHANTICandidate 
{
  public :
    CHANTICandidate();
    virtual ~CHANTICandidate();
    void Clear();

  public:
    void SetIsCHANTICandidate(Bool_t val) {fIsCHANTICandidate=val;};
    void SetTime(Double_t val) {fTime=val;};
    void SetID(Int_t val) {fID=val;};
  
    Bool_t   GetIsCHANTICandidate() {return fIsCHANTICandidate;};
    Double_t GetTime() {return fTime;};
    Int_t GetID() {return fID;};

  private:
    Bool_t fIsCHANTICandidate;
    Double_t fTime;
    Int_t fID;
};

#endif
