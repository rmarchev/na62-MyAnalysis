#ifndef AnalysisTools_H
#define AnalysisTools_H 1

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include "TVector3.h"
#include "TLorentzVector.h"
using namespace std;

class BTubeField;
class BlueTubeTracker;
class TRecoSpectrometerCandidate;
class TRecoLKrCandidate;
//class MyParticle;

class AnalysisTools
{
  public:
  AnalysisTools();
  static AnalysisTools* GetInstance();

  private:
  static AnalysisTools* fInstance;

  public:
  TVector3 SingleTrackVertex(TVector3,TVector3,TVector3,TVector3,Double_t&);
  std::pair<TLorentzVector,TLorentzVector> GetTwoPiPlusMomentum(TRecoSpectrometerCandidate* t1,TRecoSpectrometerCandidate* t2);

  TVector3 MultiTrackVertexSimple(Int_t nTracks, TLorentzVector *ptracks, TVector3 *postracks, Double_t *cda) ;
  TVector3 GetPositionAtZ(TRecoSpectrometerCandidate*, Double_t);
  TVector3 GetKaonPositionAtZ(TVector3 mom,TVector3 pos, Double_t zpos);
  TVector3 GetZ(TVector3 theta,TVector3 pos, Double_t zpos) ;
  TVector3 Propagate(Int_t charge, TVector3 *momentum, TVector3 *position, Double_t *thetaxafter, Double_t fZEnd);
  TLorentzVector Get4Momentum(Double_t *partrack);
  TLorentzVector Get4Momentum(Double_t pinmev,Double_t dxdz, Double_t dydz,Double_t massingev);
  void ClusterCorrections(Int_t,TRecoLKrCandidate *);
  void BTubeCorrection(TVector3*,Double_t*,Double_t*,Double_t);
  //TVector3 BlueFieldCorrection(MyParticle*,Double_t);
  TVector3 BlueFieldCorrection(TVector3 *, TVector3 , Int_t , Double_t );
  TVector3 GetPositionUpstream(TRecoSpectrometerCandidate *tr, TVector3 *p3tr, double zpos);
  Int_t GetLAVStation(Double_t);
  Bool_t TrackIsInAcceptance(TRecoSpectrometerCandidate*);
  Bool_t LKrGeometricalAcceptance(double , double );
  Int_t SelectTriggerMask(int triggerType, int type, int mask) ;

  void MirrorVector(TVector3 *vec, TVector3 *axis) ;
  Int_t MirrorSurface(Double_t xin, Double_t yin, Double_t SafeFactor, Bool_t flag) ;
  void InitializeMirrorCoordinate();
public:
  BTubeField *fBTubeField;
  BlueTubeTracker *fTracker;
  Double_t fMirrorPos[25][6][2];

  private:
  Bool_t fIsClusterCorrected;
};

#endif
