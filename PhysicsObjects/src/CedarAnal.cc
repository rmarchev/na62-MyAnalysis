#include "CedarAnal.hh"
#include "KTAGCandidate.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"

CedarAnal::CedarAnal(Int_t flag,NA62Analysis::Core::BaseAnalysis *ba){
    //Parameters *par = Parameters::GetInstance();
    fFlag=flag;
    fTool = AnalysisTools::GetInstance();
    fUserMethods = new UserMethods(ba);

    if(fFlag==0){

        fUserMethods->BookHisto(new TH1I("CedarAnalysis_nsectors","",10,0,10));
        fUserMethods->BookHisto(new TH1F("CedarAnalysis_alltime","",1000,-50,50));
        // fUserMethods->BookHisto(new TH1F("CedarAnalysis_alltimegtk","",1000,-50,50));
        fUserMethods->BookHisto(new TH1F("CedarAnalysis_alltimegtk","",10000,-500,500));
        fUserMethods->BookHisto(new TH1F("CedarAnalysis_time","",1000,-50,50));
        // fUserMethods->BookHisto(new TH1F("CedarAnalysis_timegtk","",1000,-50,50));
        fUserMethods->BookHisto(new TH1F("CedarAnalysis_timegtk","",10000,-500,500));
        fUserMethods->BookHisto(new TH2F("CedarAnalysis_timechid","",1024,0,1024,200,-5,5));
        fUserMethods->BookHisto(new TH2F("CedarAnalysis_timestamp","",25,0,250000000,200,-5,5));
        fUserMethods->BookHisto(new TH2F("CedarAnalysis_timeint","",50,0,100,100,-1.5,1.5));
    }

    if(fFlag==1){
        for(int i(0);i<2;i++){

            fUserMethods->BookHisto(new TH1F(Form("CedarAnalysis_Track%d_dt",i),"",800,-50,50));
            fUserMethods->BookHisto(new TH1F(Form("KSTrack%d_dt",i),"",800,-50,50));
        }
    }

    fCedarCandidate = new KTAGCandidate();
}

void CedarAnal::Clear() {
    fCedarCandidate->Clear();
    fTCorr =0;
}


void CedarAnal::StartBurst(int burstid, double *off) {
    for (int k=0; k<25; k++) fTOffset[k] = off[k];
    //for (int k=0; k<25; k++) fTOffset[k] = 0;
}

Int_t CedarAnal::TrackMatching(Int_t noGTK, Double_t timeref, Double_t timegtkref,double tstamp,bool mcflag) {
    fIsMC=mcflag;
    // Double_t timedw = !noGTK ? timegtkref : timeref;
    Double_t timedw = timeref;
    if(fCedarEvent->GetNCandidates()< 1) return 0;
    int nt = (int)(tstamp/10000000.);
    if (nt>24) nt = 24;

    double nHits = 0;
    for (int jHit(0);jHit<fCedarEvent->GetNHits();jHit++) {
        TRecoCedarHit *hit = (TRecoCedarHit*)fCedarEvent->GetHit(jHit);
        double dt = (hit->GetTime()-timedw);
        if (fabs(dt-20.)<10.) nHits++;
    }

    Double_t mintime = 9999999.;
    Double_t mingtktime = 9999999.;
    Int_t minid = -1;
    //cout << "NCand = " << fCedarEvent->GetNCandidates() << endl;
    for (Int_t jktag=0; jktag<fCedarEvent->GetNCandidates(); jktag++) {
        TRecoCedarCandidate *ktag = (TRecoCedarCandidate *)fCedarEvent->GetCandidate(jktag);
        fUserMethods->FillHisto("CedarAnalysis_nsectors",ktag->GetNSectors());
        if (ktag->GetNSectors()<5) continue;
        Double_t dt    ;
        Double_t dtgtk ;

        if(!fIsMC){

            dt = ktag->GetTime()+fTOffset[nt]+fTCorr-timedw;
            dtgtk = ktag->GetTime()+fTOffset[nt]+fTCorr-timegtkref;
        }
        else {
            dt = ktag->GetTime()-timedw;
            dtgtk = ktag->GetTime()-timegtkref;

        }
        if(fFlag==0){

            fUserMethods->FillHisto("CedarAnalysis_alltime",dt);
            fUserMethods->FillHisto("CedarAnalysis_alltimegtk",ktag->GetTime());
        }

        if (fabs(dt)<fabs(mintime)) {
            mintime = dt;
            mingtktime = dtgtk;
            minid = jktag;
        }

    }
    if (minid==-1) return 0;
    //cout << "KTime = " << mintime+timedw << "fTCorr = " << fTCorr << "toffset =" << fTOffset[nt] << "reftime = " << timedw << "dt = " << mintime << endl;
    if(fFlag==0){

        fUserMethods->FillHisto("CedarAnalysis_time",mintime);
        fUserMethods->FillHisto("CedarAnalysis_timegtk",mintime+timedw);
        //fUserMethods->FillHisto("CedarAnalysis_timechid",chid,mintime);
        fUserMethods->FillHisto("CedarAnalysis_timestamp",tstamp,mintime);
        fUserMethods->FillHisto("CedarAnalysis_timeint",nHits,mintime);
    }

    //if(!fIsMC){
    //
    //  //Fill cedar time in case wout of time events are needed
    //  if (mintime<-0.4||mintime>0.4) return 0;  // New cut instead of -2,4 ns (11/10/2016)
    //  //if (mingtktime<-1||mingtktime>1) return 0;  // New cut instead of -2,4 ns (11/10/2016)
    //
    //
    //} else {
    //if (mintime<-2||mintime>2) return 0;  // New cut instead of -2,4 ns (11/10/2016)
    //
    //}
    fCedarCandidate->SetTime(mintime+timedw); // time vs the coarse trigger time
    fCedarCandidate->SetID(minid);
    fCedarCandidate->SetIsKTAGCandidate(1);

    return 1;
}

Int_t CedarAnal::TrackMatchingSimple(Double_t timeref,int iC, bool mcflag,int flag) {
    fIsMC=mcflag;
    Double_t timedw = timeref;
    if(fCedarEvent->GetNCandidates()< 1) return -1;
    std::map<Double_t,int> closestcedar;
    int icedar=-1;
    double dtcedar;

    for (int i=0; i<fCedarEvent->GetNCandidates(); i++) {
        TRecoCedarCandidate* cedar = (TRecoCedarCandidate*) fCedarEvent->GetCandidate(i);
        dtcedar = fabs(timeref - cedar->GetTime());
        if(flag==2){

        }

        if(cedar->GetNSectors() < 4) continue;
        closestcedar[dtcedar] = i;
    }

    if(closestcedar.size() == 0) return -1;
    if(closestcedar.size() > 0) icedar = closestcedar.begin()->second;
    else icedar = -1;
    if(icedar < 0) return -1;


    TRecoCedarCandidate* mincedar = (TRecoCedarCandidate*) fCedarEvent->GetCandidate(closestcedar.begin()->second);
    double mintime = mincedar->GetTime();
    if(flag==1) fUserMethods->FillHisto(Form("CedarAnalysis_Track%d_dt",iC),timeref- mintime);
    if(flag==2) fUserMethods->FillHisto(Form("KSTrack%d_dt",iC),timeref- mintime);

    if (fabs(timeref-mintime)> 2 ) return -1;  // New cut instead of -2,4 ns (11/10/2016)


    fCedarCandidate->SetTime(mintime); // time vs the coarse trigger time
    fCedarCandidate->SetID(closestcedar.begin()->second);
    fCedarCandidate->SetIsKTAGCandidate(1);

    return 1;
}
