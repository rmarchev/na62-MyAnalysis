#include "Parameters.hh"
#include "LKrAnal.hh"
//#include "LKrCandidate.cc"
#include "AnalysisTools.hh"
#include "LKrAuxClusterReco.hh"

LKrAnal::LKrAnal(Int_t flag, NA62Analysis::Core::BaseAnalysis *ba) {

  fFlag=flag;
  fTools = AnalysisTools::GetInstance();
  par = new Parameters();
  fUserMethods = new UserMethods(ba);
  fLKrClusterCandidate = new LKrCandidate();
  fLKrCellCandidate = new LKrCandidate();
  fLKrAuxClusterReco = new LKrAuxClusterReco();

  //Parameters *par = Parameters::GetInstance();
  //par->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/run2015/database/lkrt0.dat");
  //fLKrCellT0 = (Double_t **)par->GetLKrCellT0();
  //par->LoadParameters(t0finename.Data());
  //par->StoreLKrParameters();

  if(fFlag==0 || fFlag==10){

    fUserMethods->BookHisto(new TH2F("LKrAnalysis_distvst_cluster","",200,-100,100,300,0,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_distvstchod_cluster","",400,-100,100,300,0,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_xy_cluster","",300,-300,300,300,-300,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_mindistvst_cluster","",200,-100,100,300,0,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_mindistvstchod_cluster","",400,-100,100,300,0,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_minxy_cluster","",300,-300,300,300,-300,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_energyvsdist_cluster","",300,0,300,400,0,100));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_mindistvst_cell","",200,-100,100,300,0,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_mindistvstchod_cell","",400,-100,100,300,0,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_minxy_cell","",300,-300,300,300,-300,300));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_energyvsdist_cell","",300,0,300,400,0,100));
  }

  if (fFlag==1) { // Extra clusters finding
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dthit_vs_ehit_aftercut","",2000,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis1_dthit_vs_ehit_aftercut","",2000,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dthit_vs_dist_aftercut","",1200,0,1200,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_ehit_vs_dist_aftercut","",1200,0,1200,2000,0,100000));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_hits_dx_vs_dy","",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_hits_x_vs_y","",240,-1200,1200,240,-1200,1200));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_ExtraEnergyRad","",1200,0,1200));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_energyr1","",2000,0,100000));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_energyr2","",2000,0,100000));

    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_all","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_ecluster_vs_dt_all","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_ecluster_vs_dt_forfit","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_ecluster_vs_dt","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dx_vs_dy","",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_x_vs_y","",240,-1200,1200,240,-1200,1200));

    fUserMethods->BookHisto(new TH1F("LKrAnalysis_not_matched_highe_10","",500,-50,50));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_not_matched_highe","",500,-50,50));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_not_matched_lowe","",500,-50,50));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_photon","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_not_matched_highe","",500,-50,50));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_not_matched_lowe","",500,-50,50));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_photon","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_Aux_dist_vs_dt_all","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_Aux_ecluster_vs_dt_all","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_Aux_dist_vs_dt","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_Aux_ecluster_vs_dt","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_Aux_dx_vs_dy","",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_Aux_x_vs_y","",240,-1200,1200,240,-1200,1200));

    fUserMethods->BookHisto(new TH1F("LKrAnalysis_Aux_not_matched_highe_10","",500,-50,50));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_Aux_not_matched_highe","",500,-50,50));
    fUserMethods->BookHisto(new TH1F("LKrAnalysis_Aux_not_matched_lowe","",500,-50,50));
    for (Int_t kk=0; kk<10; kk++) fPhotonCandidate[kk] = new LKrCandidate();
    for (Int_t kk=0; kk<5; kk++) fNewClusterCandidate[kk] = new LKrCandidate();
  }

  if (flag==2) { // Two photon analysis
    fTwoEMClustersID = new Int_t[45];
    //fUserMethods->BookHisto(new TH2F("hbo","",40,0,40,400,-200,200));
    fUserMethods->BookHisto(new TH1F("Clusters_n","",10,0,10));
    fUserMethods->BookHisto(new TH1F("Clusters_all_time","",400,0,500));
    fUserMethods->BookHisto(new TH2F("NCellsvsEClusters_raw","",300,0,100,150,0,150));
    fUserMethods->BookHisto(new TH2F("NCellEnergyratiovsSeedEnergyRatio_raw","",110,0,1.1,200,0,20));
    fUserMethods->BookHisto(new TH1F("MipClusterEnergy","",200,0,100));
    fUserMethods->BookHisto(new TH1F("MipClusterEnergy_zoom","",100,0,2));
    fUserMethods->BookHisto(new TH2F("MipClusterXY","",128,-1263.2,1263.2,128,-1263.2,1263.2));
    fUserMethods->BookHisto(new TH2F("NCellEnergyratiovsSeedEnergyRatio_nomip","",110,0,1.1,400,-20,20));
    fUserMethods->BookHisto(new TH1F("EMClusterEnergy","",200,0,100));
    fUserMethods->BookHisto(new TH2F("EMClusterXY","",128,-1263.2,1263.2,128,-1263.2,1263.2));
    fUserMethods->BookHisto(new TH1F("OtherClusterEnergy","",200,0,100));
    fUserMethods->BookHisto(new TH2F("OtherClusterXY","",128,-1263.2,1263.2,128,-1263.2,1263.2));
    fUserMethods->BookHisto(new TH1F("Clusters_nMips","",10,0,10));
    fUserMethods->BookHisto(new TH1F("Clusters_nEM","",10,0,10));
    fUserMethods->BookHisto(new TH1F("Clusters_nOther","",10,0,10));
    fUserMethods->BookHisto(new TH1F("TwoEMClusterTime","",200,-10,10));
  }

  if (fFlag==3) { // Extra clusters finding after full selection

    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi_dt_vs_dist" ,"",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi_dt_vs_eclus","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi_dx_vs_dy"   ,"",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi_x_vs_y"     ,"",240,-1200,1200,240,-1200,1200));

    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi1_dt_vs_dist" ,"",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi1_dt_vs_eclus","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi1_dx_vs_dy"   ,"",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi1_x_vs_y"     ,"",240,-1200,1200,240,-1200,1200));


    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi2_dt_vs_dist" ,"",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi2_dt_vs_eclus","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi2_dx_vs_dy"   ,"",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_pi2_x_vs_y"     ,"",240,-1200,1200,240,-1200,1200));


    fUserMethods->BookHisto(new TH2F("LKrAux_pi_dt_vs_dist" ,"",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi_dt_vs_eclus","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi_dx_vs_dy"   ,"",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi_x_vs_y"     ,"",240,-1200,1200,240,-1200,1200));

    fUserMethods->BookHisto(new TH2F("LKrAux_pi1_dt_vs_dist" ,"",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi1_dt_vs_eclus","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi1_dx_vs_dy"   ,"",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi1_x_vs_y"     ,"",240,-1200,1200,240,-1200,1200));


    fUserMethods->BookHisto(new TH2F("LKrAux_pi2_dt_vs_dist" ,"",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi2_dt_vs_eclus","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi2_dx_vs_dy"   ,"",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAux_pi2_x_vs_y"     ,"",240,-1200,1200,240,-1200,1200));



    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_all","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_ecluster_vs_dt_all","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_ecluster_vs_dt","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_not_matched_ecluster_vs_dt_forfit","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dx_vs_dy","",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_x_vs_y","",240,-1200,1200,240,-1200,1200));


    fUserMethods->BookHisto(new TH2F("LKrAux_dist_vs_dt_all","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_ecluster_vs_dt_all","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_ecluster_vs_dt","",200,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_dist_vs_dt","",200,0,2000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAux_dx_vs_dy","",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAux_x_vs_y","",240,-1200,1200,240,-1200,1200));


    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dthit_vs_ehit","",2000,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dthit_vs_dist","",1200,0,1200,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dthit_vs_ehit_aftercut","",2000,0,100000,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_dthit_vs_dist_aftercut","",1200,0,1200,1200,-150,150));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_ehit_vs_dist_aftercut","",1200,0,1200,2000,0,100000));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_hits_dx_vs_dy","",2400,-1200,1200,2400,-1200,1200));
    fUserMethods->BookHisto(new TH2F("LKrAnalysis_hits_x_vs_y","",240,-1200,1200,240,-1200,1200));

  }

}


void LKrAnal::StartBurst(Int_t year, Int_t runid) {

  //Parameters *par = Parameters::GetInstance();
  TString t0finename;

  if(!fUserMethods->GetWithMC()) t0finename.Form("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/run%d/lkrt0.dat",runid);
  par->LoadParameters(t0finename.Data());
  fLKrCellT0 = (Double_t **)par->GetLKrCellT0();
  cout << t0finename << " loaded" << endl;
  fYear = year;
  fRunID = runid;
  //  fIsFilter = fUserMethods->GetTree("Reco")->FindBranch("FilterWord") ? true : false;
  par->StoreLKrParameters();
}

void LKrAnal::Clear(Int_t flag) {

  if (flag==0) {
    fLKrClusterCandidate->Clear();
    fLKrCellCandidate->Clear();
  }

  if (flag==1) {
    for (Int_t kk=0; kk<10; kk++) fPhotonCandidate[kk]->Clear();
    for (Int_t kk=0; kk<5; kk++) fNewClusterCandidate[kk]->Clear();
  }

  // if (flag==3) {
  //   for (Int_t kk=0; kk<10; kk++) fPhotonCandidate[kk]->Clear();
  //   for (Int_t kk=0; kk<5; kk++) fNewClusterCandidate[kk]->Clear();
  // }

  if (flag==2) {
    fNEMClusters = 0;
    fNMIPClusters = 0;
    fNOtherClusters = 0;
    fill(fEMClusterID,fEMClusterID+10,-1);
    fill(fMIPClusterID,fMIPClusterID+10,-1);
    fill(fOtherClusterID,fOtherClusterID+10,-1);
    fNEMDoublets = 0;
    for (Int_t j=0; j<45; j++) fTwoEMClustersID[j]=-1;
    fLKrClusterCandidate->Clear();
    fLKrCellCandidate->Clear();
  }
  fLKrAuxClusterReco->Clear();
}

///////////////////////////////////////////////////////
// Cluster analysis for pip0 selection using photons //
///////////////////////////////////////////////////////
Int_t LKrAnal::PhotonAnalysis(/*Bool_t mcflag, Int_t runid, TRecoLKrEvent* event, L0TPData* l0tp*/) {
  //fL0Data = l0tp;
  fUserMethods->FillHisto("Clusters_n",fLKrEvent->GetNCandidates());
  ClusterAnalysis();
  fUserMethods->FillHisto("Clusters_nMips",fNMIPClusters);
  fUserMethods->FillHisto("Clusters_nEM",fNEMClusters);
  fUserMethods->FillHisto("Clusters_nOther",fNOtherClusters);
  Int_t fIsTwoPhotonEvent = 0;
  if (fNEMClusters>=2) fIsTwoPhotonEvent = TwoPhotonCandidate();
  return fIsTwoPhotonEvent;
}
void LKrAnal::ClusterAnalysis() {
  for (Int_t jclus=0; jclus<fLKrEvent->GetNCandidates(); jclus++) {
    TRecoLKrCandidate *thisCluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(jclus);
    // if (!mcflag && !fIsFilter) fTools->ClusterCorrections(runid,thisCluster);
    fUserMethods->FillHisto("Clusters_all_time",thisCluster->GetClusterTime());
    fUserMethods->FillHisto("NCellsvsEClusters_raw",thisCluster->GetClusterEnergy()/1000.,thisCluster->GetNCells());
    fUserMethods->FillHisto("NCellEnergyratiovsSeedEnergyRatio_raw",thisCluster->GetClusterSeedEnergy()/thisCluster->GetClusterEnergy(),thisCluster->GetNCells()/(thisCluster->GetClusterEnergy()/1000.));
    Bool_t isMIPCluster = SelectMIPCluster(jclus,thisCluster);
    Bool_t isEMCluster = SelectEMCluster(jclus,thisCluster);
    Bool_t isOtherCluster = SelectOtherCluster(jclus,thisCluster,isMIPCluster,isEMCluster);
  }
  return;
}
Bool_t LKrAnal::TwoPhotonCandidate() {
  for (Int_t iemclus=0; iemclus<fNEMClusters-1; iemclus++) {
    TRecoLKrCandidate *iCluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(fEMClusterID[iemclus]);
    //if (CloseCluster(fEMClusterID[iemclus],iCluster->GetClusterX(),iCluster->GetClusterY(),fLKrEvent)) continue;
    for (Int_t jemclus=iemclus+1; jemclus<fNEMClusters; jemclus++) {
      TRecoLKrCandidate *jCluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(fEMClusterID[jemclus]);
      //if (CloseCluster(fEMClusterID[jemclus],jCluster->GetClusterX(),jCluster->GetClusterY(),fLKrEvent)) continue;
      Double_t dtime = iCluster->GetClusterTime()-jCluster->GetClusterTime();
      fUserMethods->FillHisto("TwoEMClusterTime",dtime);
      if (fabs(dtime)>2.5) continue;
      fTwoEMClustersID[2*fNEMDoublets+0] = fEMClusterID[iemclus];
      fTwoEMClustersID[2*fNEMDoublets+1] = fEMClusterID[jemclus];
      fNEMDoublets++;
      if (fNEMDoublets==45) return 1;
    }
  }
  return (fNEMDoublets)?1:0;
}
Bool_t LKrAnal::SelectMIPCluster(Int_t jclus, TRecoLKrCandidate *thisCluster) {
  if (thisCluster->GetNCells()>=6) return 0;
  if (thisCluster->GetClusterDDeadCell()<20.) return 0;
  Double_t seedRatio = thisCluster->GetClusterSeedEnergy()/thisCluster->GetClusterEnergy();
  Double_t cellRatio = thisCluster->GetNCells()/(thisCluster->GetClusterEnergy()/1000.);
  if (cellRatio<0.2) return 0;
  fUserMethods->FillHisto("MipClusterEnergy",thisCluster->GetClusterEnergy()/1000.);
  fUserMethods->FillHisto("MipClusterEnergy_zoom",thisCluster->GetClusterEnergy()/1000.);
  fUserMethods->FillHisto("MipClusterXY",thisCluster->GetClusterX(),thisCluster->GetClusterY());
  if (fNMIPClusters<10) SetMIPClusterID(jclus);
  AddMIPClusters();
  return 1;
}
Bool_t LKrAnal::SelectEMCluster(Int_t jclus, TRecoLKrCandidate *thisCluster) {
  if (thisCluster->GetClusterEnergy()<3000) return 0;
  if (thisCluster->GetClusterDDeadCell()<20) return 0;
  //if (!fTools->LKrAcceptance(thisCluster->GetClusterX(),thisCluster->GetClusterY(),150.,1130.)) return 0;
  Double_t seedRatio = thisCluster->GetClusterSeedEnergy()/thisCluster->GetClusterEnergy();
  Double_t cellRatio = (thisCluster->GetNCells()-2.21)/(0.93*thisCluster->GetClusterEnergy()/1000.);
  fUserMethods->FillHisto("NCellEnergyratiovsSeedEnergyRatio_nomip",seedRatio,cellRatio);
  Bool_t isNotEM = true;
  if (seedRatio>0.23&&seedRatio<0.46&&cellRatio<2&&cellRatio>=1) isNotEM = false;
  if (isNotEM) return 0;
  fUserMethods->FillHisto("EMClusterEnergy",thisCluster->GetClusterEnergy()/1000.);


  if (fNEMClusters<10) SetEMClusterID(jclus);
  AddEMClusters();
  return 1;
}
Bool_t LKrAnal::SelectOtherCluster(Int_t jclus, TRecoLKrCandidate *thisCluster, Bool_t isMIPCluster, Bool_t isEMCluster) {
  if (isMIPCluster) return 0;
  if (isEMCluster) return 0;
  fUserMethods->FillHisto("OtherClusterEnergy",thisCluster->GetClusterEnergy()/1000.);
  fUserMethods->FillHisto("OtherClusterXY",thisCluster->GetClusterX(),thisCluster->GetClusterY());
  if (fNOtherClusters<10) SetOtherClusterID(jclus);
  AddOtherClusters();
  return 1;
}

bool LKrAnal::LKrHitInTime(double E, double dt) {
  if(E < 45.0) return false; // Do not consider individual hits if E_cell < 45 MeV
  if(dt > 2*(0.2 - 63.0/sqrt(E)) && dt < 2*(1.6 + 50.0/sqrt(E))) return true;
  return false;
}

///////////////////////////////////////
// Photon search for pinunu analysis //
///////////////////////////////////////
Int_t LKrAnal::ExtraActivity(Double_t reftime, TVector3 at_lkr) {

  double energy_accu_l1 = 0.0;
  double energy_accu_l2 = 0.0;
  double energy_accu_l3 = 0.0;

  int n_lkr_cand = fLKrEvent->GetNCandidates();
  TClonesArray& Hits = (*(fLKrEvent->GetHits()));
  // Loop on the hits
  for(int j = 0; j < fLKrEvent->GetNHits(); j++) {

    TRecoLKrHit * lkr_hit = (TRecoLKrHit*)Hits.At(j);
    if(lkr_hit->GetEnergy() < 0) continue; // FIXME

    // Timing cut
    bool pass_time_cut = false;
    bool intime_lkr_hit = LKrHitInTime(lkr_hit->GetEnergy(),lkr_hit->GetTime() - reftime);

    if(intime_lkr_hit) pass_time_cut = true;

    if(pass_time_cut == true) {

      fUserMethods->FillHisto("LKrAnalysis_dthit_vs_ehit_aftercut",lkr_hit->GetEnergy(),lkr_hit->GetTime() - reftime);
      double d_trk = sqrt((lkr_hit->GetPosition().X() - at_lkr.X())*(lkr_hit->GetPosition().X() - at_lkr.X()) + (lkr_hit->GetPosition().Y() - at_lkr.Y())*(lkr_hit->GetPosition().Y() - at_lkr.Y()));
      double dx_trk=(lkr_hit->GetPosition().X() - at_lkr.X());
      double dy_trk=(lkr_hit->GetPosition().Y() - at_lkr.Y());
      fUserMethods->FillHisto("LKrAnalysis_dthit_vs_dist_aftercut",d_trk,lkr_hit->GetTime() - reftime);
      fUserMethods->FillHisto("LKrAnalysis_ehit_vs_dist_aftercut",lkr_hit->GetEnergy(),d_trk);
      fUserMethods->FillHisto("LKrAnalysis_hits_dx_vs_dy",dx_trk,dy_trk);
      fUserMethods->FillHisto("LKrAnalysis_hits_x_vs_y",lkr_hit->GetPosition().X(),lkr_hit->GetPosition().Y());

      if(d_trk > 200)
        fUserMethods->FillHisto("LKrAnalysis1_dthit_vs_ehit_aftercut",lkr_hit->GetEnergy(),lkr_hit->GetTime() - reftime);
      if(d_trk > 150.0 && d_trk < 500.0) {
        energy_accu_l1 += lkr_hit->GetEnergy();
      }
      if(d_trk >= 500.0) {
        fUserMethods->FillHisto("LKrAnalysis_ExtraEnergyRad",d_trk);
        energy_accu_l2 += lkr_hit->GetEnergy();
      }
    }
  }


  fUserMethods->FillHisto("LKrAnalysis_energyr1",energy_accu_l1);
  fUserMethods->FillHisto("LKrAnalysis_energyr2",energy_accu_l2);

  if(energy_accu_l1 > 1000.0 || energy_accu_l2 > 100.0) {

    return 1;
  }

  return 0;
}

Int_t LKrAnal::PiPlusLKrRadius(double ptrack ) {


  Double_t timeshift=-1.22;
  TVector2 postrack(fTrackAtLKr.X(),fTrackAtLKr.Y());
  EventHeader* header=fUserMethods->GetEventHeader();

  for (Int_t jclus=0; jclus<fLKrEvent->GetNCandidates(); jclus++) {
    TRecoLKrCandidate *thisCluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(jclus);
    TVector2 posclust(thisCluster->GetClusterX(),thisCluster->GetClusterY());
    TVector2 dx = (posclust-postrack);
    Double_t dist = (posclust-postrack).Mod();
    Double_t dt = thisCluster->GetClusterTime()-fTrackTime-timeshift;
    Double_t E = thisCluster->GetClusterEnergy();
    if ((posclust-fTrackClAtLKr).Mod()>400.) continue;
    if(E < 1000) continue;
    fUserMethods->FillHisto("LKrAnalysis_pi_dt_vs_dist",dist,dt);
    fUserMethods->FillHisto("LKrAnalysis_pi_dt_vs_eclus",thisCluster->GetClusterEnergy(),dt);
    if(fabs(dt) < 10){

    fUserMethods->FillHisto("LKrAnalysis_pi_dx_vs_dy",dx.X(),dx.Y());
    fUserMethods->FillHisto("LKrAnalysis_pi_x_vs_y",thisCluster->GetClusterX(),thisCluster->GetClusterY());
    }

    if(ptrack > 15 && ptrack <= 25){
      fUserMethods->FillHisto("LKrAnalysis_pi1_dt_vs_dist",dist,dt);
      fUserMethods->FillHisto("LKrAnalysis_pi1_dt_vs_eclus",thisCluster->GetClusterEnergy(),dt);

      if(fabs(dt) < 10){
      fUserMethods->FillHisto("LKrAnalysis_pi1_dx_vs_dy",dx.X(),dx.Y());
      fUserMethods->FillHisto("LKrAnalysis_pi1_x_vs_y",thisCluster->GetClusterX(),thisCluster->GetClusterY());
      }

    }

    if(ptrack > 25 && ptrack <= 35){
      fUserMethods->FillHisto("LKrAnalysis_pi2_dt_vs_dist",dist,dt);
      fUserMethods->FillHisto("LKrAnalysis_pi2_dt_vs_eclus",thisCluster->GetClusterEnergy(),dt);
      if(fabs(dt) < 10){
      fUserMethods->FillHisto("LKrAnalysis_pi2_dx_vs_dy",dx.X(),dx.Y());
      fUserMethods->FillHisto("LKrAnalysis_pi2_x_vs_y",thisCluster->GetClusterX(),thisCluster->GetClusterY());
      }

    }

  }




  fLKrAuxClusterReco->SetfCutCellDistance(5);
  fLKrAuxClusterReco->FindClusters(fTrackTime, fLKrEvent);

  for (Int_t i=0; i<fLKrAuxClusterReco->GetNClusters(); i++) {

    Double_t Energy = fLKrAuxClusterReco->GetCandidate(i)->GetClusterEnergy();
    Double_t x   = fLKrAuxClusterReco->GetCandidate(i)->GetClusterX();
    Double_t y   = fLKrAuxClusterReco->GetCandidate(i)->GetClusterY();
    Int_t ncells = fLKrAuxClusterReco->GetCandidate(i)->GetNCells();
    Double_t dt  = fLKrAuxClusterReco->GetCandidate(i)->GetClusterTime() - fTrackTime - 4.6; //( offset needs to be applied)

    TVector2 posclust(x,y);
    TVector2 dx = (posclust-postrack);
    Double_t dist = (posclust-postrack).Mod();


    if ((posclust-fTrackClAtLKr).Mod()>400.) continue;
    if (Energy<1000.) continue;

    fUserMethods->FillHisto("LKrAux_pi_dt_vs_dist",dist,dt);
    fUserMethods->FillHisto("LKrAux_pi_dt_vs_eclus",Energy,dt);
    if(fabs(dt) < 10){
      fUserMethods->FillHisto("LKrAux_pi_dx_vs_dy",dx.X(),dx.Y());
      fUserMethods->FillHisto("LKrAux_pi_x_vs_y",x,y);
    }

    if(ptrack > 15 && ptrack <= 25){

      fUserMethods->FillHisto("LKrAux_pi1_dt_vs_dist",dist,dt);
      fUserMethods->FillHisto("LKrAux_pi1_dt_vs_eclus",Energy,dt);
      if(fabs(dt) < 10){
        fUserMethods->FillHisto("LKrAux_pi1_dx_vs_dy",dx.X(),dx.Y());
        fUserMethods->FillHisto("LKrAux_pi1_x_vs_y",x,y);
      }
    }

    if(ptrack > 25 && ptrack <= 35){
      fUserMethods->FillHisto("LKrAux_pi2_dt_vs_dist",dist,dt);
      fUserMethods->FillHisto("LKrAux_pi2_dt_vs_eclus",Energy,dt);
      if(fabs(dt) < 10){
        fUserMethods->FillHisto("LKrAux_pi2_dx_vs_dy",dx.X(),dx.Y());
        fUserMethods->FillHisto("LKrAux_pi2_x_vs_y",x,y);
      }
    }

  }

  return 1;
}
Int_t LKrAnal::ClustersLeft(Int_t flag,double mm2) {

  Double_t timeshift=-1.22;
  TVector2 postrack(fTrackAtLKr.X(),fTrackAtLKr.Y());
  EventHeader* header=fUserMethods->GetEventHeader();
  if(!fUserMethods->GetWithMC()){

  std::cout << "---------Event information for PiPi0 ---------" << std::endl;
  std::cout << "Run ID***" << "Burst ID***" << " EventID***" << " MMiss2***"  << endl;
  std::cout << header->GetRunID()<< "  " << header->GetBurstID() << "  " << header->GetEventNumber() << "  "<< mm2<< endl;
  std::cout << "------------------------------------ ---------" << std::endl;

  }

  for (Int_t jclus=0; jclus<fLKrEvent->GetNCandidates(); jclus++) {
    TRecoLKrCandidate *thisCluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(jclus);
    TVector2 posclust(thisCluster->GetClusterX(),thisCluster->GetClusterY());
    TVector2 dx = (posclust-postrack);
    Double_t dist = (posclust-postrack).Mod();
    Double_t dtime = thisCluster->GetClusterTime()-fTrackTime+timeshift;
    fUserMethods->FillHisto("LKrAnalysis_not_matched_all",dist,dtime);
    fUserMethods->FillHisto("LKrAnalysis_not_matched_ecluster_vs_dt_all",thisCluster->GetClusterEnergy(),dtime);
    if ((posclust-fTrackClAtLKr).Mod()<120.) continue;
    fUserMethods->FillHisto("LKrAnalysis_not_matched",dist,dtime);
    fUserMethods->FillHisto("LKrAnalysis_not_matched_ecluster_vs_dt",thisCluster->GetClusterEnergy(),dtime);
    fUserMethods->FillHisto("LKrAnalysis_dx_vs_dy",dx.X(),dx.Y());
    fUserMethods->FillHisto("LKrAnalysis_x_vs_y",thisCluster->GetClusterX(),thisCluster->GetClusterY());


  }

  fLKrAuxClusterReco->SetfCutCellDistance(5);
  fLKrAuxClusterReco->FindClusters(fTrackTime, fLKrEvent);

  for (Int_t i=0; i<fLKrAuxClusterReco->GetNClusters(); i++) {



    Double_t Energy = fLKrAuxClusterReco->GetCandidate(i)->GetClusterEnergy();
    Double_t x   = fLKrAuxClusterReco->GetCandidate(i)->GetClusterX();
    Double_t y   = fLKrAuxClusterReco->GetCandidate(i)->GetClusterY();
    Int_t ncells = fLKrAuxClusterReco->GetCandidate(i)->GetNCells();
    Double_t dt  = fLKrAuxClusterReco->GetCandidate(i)->GetClusterTime() - fTrackTime - 4.6; //( offset needs to be applied)

    TVector2 posclust(x,y);
    TVector2 dx = (posclust-postrack);
    Double_t dist = (posclust-postrack).Mod();


    fUserMethods->FillHisto("LKrAux_dist_vs_dt_all",dist,dt);
    fUserMethods->FillHisto("LKrAux_ecluster_vs_dt_all",Energy, dt);
    if ((posclust-fTrackClAtLKr).Mod()<120.) continue;

    fUserMethods->FillHisto("LKrAux_ecluster_vs_dt",Energy, dt);
    fUserMethods->FillHisto("LKrAux_dist_vs_dt",dist, dt);
    fUserMethods->FillHisto("LKrAux_dx_vs_dy",dx.X(),dx.Y());
    fUserMethods->FillHisto("LKrAux_x_vs_y",x,y);

  }

  int n_lkr_cand = fLKrEvent->GetNCandidates();
  TClonesArray& Hits = (*(fLKrEvent->GetHits()));
  // Loop on the hits
  for(int j = 0; j < fLKrEvent->GetNHits(); j++) {

    TRecoLKrHit * lkr_hit = (TRecoLKrHit*)Hits.At(j);
    if(lkr_hit->GetEnergy() < 0) continue; // FIXME

    // Timing cut
      double d_trk = sqrt((lkr_hit->GetPosition().X() - fTrackAtLKr.X())*(lkr_hit->GetPosition().X() - fTrackAtLKr.X()) + (lkr_hit->GetPosition().Y() - fTrackAtLKr.Y())*(lkr_hit->GetPosition().Y() - fTrackAtLKr.Y()));
      double dx_trk=(lkr_hit->GetPosition().X() - fTrackAtLKr.X());
      double dy_trk=(lkr_hit->GetPosition().Y() - fTrackAtLKr.Y());
      fUserMethods->FillHisto("LKrAnalysis_dthit_vs_ehit",lkr_hit->GetEnergy(),lkr_hit->GetTime() - fTrackTime);
      fUserMethods->FillHisto("LKrAnalysis_dthit_vs_dist",d_trk,lkr_hit->GetTime() - fTrackTime);
      if(d_trk < 150) continue;
      fUserMethods->FillHisto("LKrAnalysis_dthit_vs_ehit_aftercut",lkr_hit->GetEnergy(),lkr_hit->GetTime() - fTrackTime);
      fUserMethods->FillHisto("LKrAnalysis_dthit_vs_dist_aftercut",d_trk,lkr_hit->GetTime() - fTrackTime);
      fUserMethods->FillHisto("LKrAnalysis_ehit_vs_dist_aftercut",lkr_hit->GetEnergy(),d_trk);
      fUserMethods->FillHisto("LKrAnalysis_hits_dx_vs_dy",dx_trk,dy_trk);
      fUserMethods->FillHisto("LKrAnalysis_hits_x_vs_y",lkr_hit->GetPosition().X(),lkr_hit->GetPosition().Y());

  }
  return 1;

}

Int_t LKrAnal::ExtraClusters(Double_t reftime, TVector2 posmatchclust, TVector3 posatlkr, Int_t eventnumber) {
  Int_t nPhoton = 0;
  Double_t timeshift = 0;
  if (fYear==2016) { // Measured offset in 2016
    timeshift = -1.22;
    if (fRunID>6145) timeshift = -1.22-0.4;
  }
  for (Int_t jclus=0; jclus<fLKrEvent->GetNCandidates(); jclus++) {
    TRecoLKrCandidate *thisCluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(jclus);
    Int_t seedid = thisCluster->GetIdSeed();
    Int_t ixcell = seedid/1000;
    Int_t iycell = seedid-1000*ixcell;
    //Double_t t0cell = (ixcell>=0 && iycell>=0) ? fLKrCellT0[ixcell][iycell] : 0;
    Double_t t0cell = 0;
    TVector2 posclust(thisCluster->GetClusterX(),thisCluster->GetClusterY());
    double x = thisCluster->GetClusterX();
    double y = thisCluster->GetClusterY();
    TVector2 postrack(posatlkr.X(),posatlkr.Y());
    TVector2 dx = posclust-postrack ;
    Double_t dist = (posclust-postrack).Mod();
    // Double_t dtime = thisCluster->GetClusterTime()-reftime-t0cell+timeshift;
    //cout << "tcl = " << thisCluster->GetClusterTime() << " tref = " << reftime << "timeshift = " << timeshift << endl;
    Double_t dtime = thisCluster->GetClusterTime()-reftime+timeshift;
    fUserMethods->FillHisto("LKrAnalysis_not_matched_all",dist,dtime);
    fUserMethods->FillHisto("LKrAnalysis_not_matched_ecluster_vs_dt_all",thisCluster->GetClusterEnergy(),dtime);
    ////    if (eventnumber==225151) cout << "       " << jclus << " " << reftime << " " << dist << " " << dtime << " " << (posclust-*posmatchclust).Mod() << endl;
    if ((posclust-posmatchclust).Mod()<100.) continue;
    fUserMethods->FillHisto("LKrAnalysis_not_matched",dist,dtime);
    Bool_t isPhoton = false;
    // if(dist > 150)
    if(dist > 100)
      fUserMethods->FillHisto("LKrAnalysis_not_matched_ecluster_vs_dt_forfit",thisCluster->GetClusterEnergy(),dtime);

    if(!fUserMethods->GetWithMC()){
      // isPhoton = dist>150. ? SelectTiming(dtime,thisCluster->GetClusterEnergy()/1000.) : false;
      //std::cout << "Before dt = " << dtime << std::endl;
      // isPhoton = dist>150. ? LKrClusterInTime(dtime,thisCluster->GetClusterEnergy()/1000.,0) : false;
      isPhoton = dist>100. ? LKrClusterInTime(dtime,thisCluster->GetClusterEnergy()/1000.,0) : false;
      // if(x>0 && x<600 && y > -300 && y < 300 && dist > 150 && thisCluster->GetClusterEnergy() > 2000.)
      if(x>0 && x<600 && y > -300 && y < 300 && dist > 100 && thisCluster->GetClusterEnergy() > 2000.)
        if(fabs(dtime+36.) < 3) isPhoton=true;

    } else{
      // isPhoton = dist>150 ? 1 : false;
      isPhoton = dist>100 ? 1 : false;
    }
    //Fix bug of cream refiring at -36 ns

    if (!isPhoton) continue;
    fUserMethods->FillHisto("LKrAnalysis_not_matched_photon",dist,dtime);
    fUserMethods->FillHisto("LKrAnalysis_not_matched_ecluster_vs_dt",thisCluster->GetClusterEnergy(),dtime);
    fUserMethods->FillHisto("LKrAnalysis_dx_vs_dy",dx.X(),dx.Y());
    fUserMethods->FillHisto("LKrAnalysis_x_vs_y",x,y);

    if (nPhoton<10) {
      fPhotonCandidate[nPhoton]->SetTime(dtime+reftime);
      fPhotonCandidate[nPhoton]->SetX(posclust.X());
      fPhotonCandidate[nPhoton]->SetY(posclust.Y());
      fPhotonCandidate[nPhoton]->SetEnergy(thisCluster->GetClusterEnergy()/1000.);
      fPhotonCandidate[nPhoton]->SetNCells(thisCluster->GetNCells());
    }
    nPhoton++;
  }
  if (!nPhoton) return 0;

  return nPhoton;
}

Int_t LKrAnal::ExtraAuxClusters(Double_t reftime, TVector2 posmatchclust, TVector3 posatlkr) {

  //Auxiliary reconstruction
  int nPhoton=0;

  fLKrAuxClusterReco->SetfCutCellDistance(5);
  //fLKrAuxClusterReco->SetfHitTimeCorrection(0);
  fLKrAuxClusterReco->FindClusters(reftime, fLKrEvent);
  //fLKrAuxClusterReco->PrintClusters();
  //fLKrAuxClusterReco->PrintSummary();
  for (Int_t i=0; i<fLKrAuxClusterReco->GetNClusters(); i++) {
    //if (i==clid) continue;

    Double_t Energy = fLKrAuxClusterReco->GetCandidate(i)->GetClusterEnergy();
    // if(Energy < 1000) continue;
    Double_t x   = fLKrAuxClusterReco->GetCandidate(i)->GetClusterX();
    Double_t y   = fLKrAuxClusterReco->GetCandidate(i)->GetClusterY();
    Int_t ncells = fLKrAuxClusterReco->GetCandidate(i)->GetNCells();
    Double_t dt  = fLKrAuxClusterReco->GetCandidate(i)->GetClusterTime()-reftime;

    TVector2 posclust(x,y);
    TVector2 postrack(posatlkr.X(),posatlkr.Y());
    TVector2 dx = (posclust-postrack);
    Double_t dist = (posclust-postrack).Mod();

    fUserMethods->FillHisto("LKrAnalysis_Aux_dist_vs_dt_all",dist,dt);
    fUserMethods->FillHisto("LKrAnalysis_Aux_ecluster_vs_dt_all",Energy, dt);

    // if ((posclust-posmatchclust).Mod()<120.) continue;
    if ((posclust-posmatchclust).Mod()<100.) continue;

    // if(dist < 150) continue;
    if(dist < 100) continue;
    //if(dist > 150 && dist < 500 && Energy < 1000) continue;
    //if(dist > 500 && Energy < 150) continue;
    if(Energy < 1000) continue;
    //if(fabs(dt) > 8) continue;
    //if(Energy>=10000 && fabs(dt) > 25) continue;
    //if(Energy>2000 && Energy < 10000){
    //if(dt < -10 || dt > 15)continue;
    //}
    //if(Energy<=2000 && fabs(dt)>8) continue;
    bool isPhoton=false;
    if(!fUserMethods->GetWithMC()){
        // isPhoton = dist>150. ? SelectAuxTiming(dt,Energy/1000.) : false;
      // isPhoton = dist>150. ? LKrClusterInTime(dt,Energy/1000.,1) : false;
      isPhoton = dist>100. ? LKrClusterInTime(dt,Energy/1000.,1) : false;
    }
    if(x>0 && x<600 && y > -300 && y < 300 && Energy > 2000 && dist > 100 )
      if(fabs(dt+36.) < 3) isPhoton=true;

    if(!isPhoton) continue;
    fUserMethods->FillHisto("LKrAnalysis_Aux_ecluster_vs_dt",Energy, dt);
    fUserMethods->FillHisto("LKrAnalysis_Aux_dist_vs_dt",dist, dt);
    fUserMethods->FillHisto("LKrAnalysis_Aux_dx_vs_dy",dx.X(),dx.Y());
    fUserMethods->FillHisto("LKrAnalysis_Aux_x_vs_y",x,y);

    if(nPhoton < 5){
      fNewClusterCandidate[nPhoton]->SetTime(dt+reftime);
      fNewClusterCandidate[nPhoton]->SetX(x);
      fNewClusterCandidate[nPhoton]->SetY(y);
      fNewClusterCandidate[nPhoton]->SetEnergy(Energy/1000.);
      fNewClusterCandidate[nPhoton]->SetNCells(ncells);
    }
    nPhoton++;
  }

  if(!nPhoton) return 0;
  return nPhoton;

}


bool LKrAnal::LKrClusterInTime(double dt, double E, bool isAux) {

  if(isAux && E < 1.0) return false; // Do not consider clusters if E_cluster < 1 GeV

  Double_t dtime36 = dt+36;
  Double_t dtime25 = dt+25;
  Double_t dtime25m= dt-25;

  if (E>=10) {
    if(!isAux)
      fUserMethods->FillHisto("LKrAnalysis_not_matched_highe_10",dt);
    else
      fUserMethods->FillHisto("LKrAnalysis_Aux_not_matched_highe_10",dt);

  }
  if (E>2 && E < 10) {

    if(!isAux)
      fUserMethods->FillHisto("LKrAnalysis_not_matched_highe",dt);
    else
      fUserMethods->FillHisto("LKrAnalysis_Aux_not_matched_highe",dt);

  }
  if(E <= 2) {
    if(!isAux)
      fUserMethods->FillHisto("LKrAnalysis_not_matched_lowe",dt);
    else
      fUserMethods->FillHisto("LKrAnalysis_Aux_not_matched_lowe",dt);

  }

  double sigma = 0.56 + 1.53/E -0.233/sqrt(E);

  if(E <= 1.0 && fabs(dt) < 5) return true;
  if(E >  1.0 && E <= 2 &&fabs(dt) < 5*sigma ) return true; //Fitted function cut at 10 sigma
  if(E > 10 &&fabs(dtime25) < 3*sigma) return true;
  if(E > 10 &&fabs(dtime25m)< 3*sigma) return true;
  if(E > 2 && E <= 15 &&fabs(dt) < 15*sigma ) return true; //Fitted function cut at 10 sigma
  if(E > 15 &&fabs(dt) < 70*sigma ) return true; //Fitted function cut at 10 sigma

  return false;

}

Bool_t LKrAnal::SelectAuxTiming(Double_t dtime, Double_t energy) {
    Double_t dtime36 = dtime+36;
    if (energy>=10) {
        fUserMethods->FillHisto("LKrAnalysis_Aux_not_matched_highe_10",dtime);
        if (fabs(dtime)<25) return true; // Broad cut because of the photon rate: adjust ? (see analysis meeting 22/07/2016)
        else return false;
    }
    if (energy>2 && energy < 10) {
        fUserMethods->FillHisto("LKrAnalysis_Aux_not_matched_highe",dtime);
        if (dtime>-15 && dtime<15) return true; // Broad cut because of the photon rate: adjust ? (see analysis meeting 22/07/2016)
        else return false;
    }
    if(energy <= 2) {
        fUserMethods->FillHisto("LKrAnalysis_Aux_not_matched_lowe",dtime);
        if (dtime>-8 && dtime<8) return true; // Tight cut because of the MIP rate: adjust ? (see analysis meeting 22/07/2016) -> pileup effect within 25 ns ?
        else return false;
    }
    return false;
}
Bool_t LKrAnal::SelectTiming(Double_t dtime, Double_t energy) {
  Double_t dtime12 = dtime-12.5;
  //Double_t dtime25 = dtime+25;
  if (energy>=10) {
    fUserMethods->FillHisto("LKrAnalysis_not_matched_highe_10",dtime);
    if (fabs(dtime)<25) return true; // Broad cut because of the photon rate: adjust ? (see analysis meeting 22/07/2016)
    else return false;
  }
  if (energy>2 && energy < 10) {
    fUserMethods->FillHisto("LKrAnalysis_not_matched_highe",dtime);
    if (dtime>-15 && dtime<15) return true; // Broad cut because of the photon rate: adjust ? (see analysis meeting 22/07/2016)
    else return false;
  }
  if(energy <= 2) {
    fUserMethods->FillHisto("LKrAnalysis_not_matched_lowe",dtime);
    if (dtime>-8 && dtime<8) return true; // Tight cut because of the MIP rate: adjust ? (see analysis meeting 22/07/2016) -> pileup effect within 25 ns ?
    else return false;
  }
  return false;
}

//////////////////////////////////////////////////
// Track - cluster matching for pinunu analysis //
//////////////////////////////////////////////////
Int_t LKrAnal::TrackClusterMatching( Double_t chodTime, Double_t trackTime, TVector3 posatlkr) {
  Double_t xpart = posatlkr.X();
  Double_t ypart = posatlkr.Y();
  Double_t timeshift = -.6;
  //if (fYear==2016) {
  //  if (fFlag==0) timeshift = -1.2-1.4;
  //  if (fFlag==2) timeshift = -0.45;
  //}


  std::map<Double_t,int> closestlkr;
  std::vector<Double_t> dtchod;
  std::vector<Double_t> dttrack;
  std::vector<Double_t> dtrkcl;
  std::vector<TVector2> dr;
  int ilkr=-1;
  TRecoLKrCandidate* lkr;
  if(fLKrEvent->GetNCandidates() == 0) return ilkr;

  for (int i=0; i<fLKrEvent->GetNCandidates(); i++) {
    lkr = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(i);

    Int_t seedid = lkr->GetIdSeed();
    Int_t ixcell = seedid/1000;
    Int_t iycell = seedid-1000*ixcell;
    Double_t t0cell = 0;
    TVector2 posclust(lkr->GetClusterX(),lkr->GetClusterY());
    TVector2 postrack(xpart,ypart);
    Double_t dist = (posclust-postrack).Mod();
    Double_t dtime = lkr->GetClusterTime()-trackTime-t0cell+timeshift;
    Double_t dtimechod = lkr->GetClusterTime()-chodTime-t0cell+timeshift;
    Double_t dx = (posclust-postrack).X();
    Double_t dy = (posclust-postrack).Y();


    //maybe the number 3 should be changed later
    dttrack.push_back(dtime);
    dtchod.push_back(dtimechod);
    dtrkcl.push_back(dist) ;
    dr.push_back(posclust-postrack) ;
    closestlkr[dist] = i;

    if(fFlag == 0){

      fUserMethods->FillHisto("LKrAnalysis_distvst_cluster",dtime,dist);
      fUserMethods->FillHisto("LKrAnalysis_distvstchod_cluster",dtimechod,dist);
      fUserMethods->FillHisto("LKrAnalysis_xy_cluster",dx,dy);
    }

    //std::cout << "t0 cell = " << t0cell  << " clustertime == " << lkr->GetClusterTime() << "seedid =" << seedid<< std::endl;
    //std::cout << "dtlkr= " << dtlkr  << "choddt =" << dtimechod << "trdt ==" << dtime << " i = " << i<< std::endl;
  }



  ilkr = closestlkr.begin()->second;

  if(ilkr==-1) return ilkr;

  TRecoLKrCandidate *cluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(ilkr);

  double mindist = dtrkcl[ilkr];
  double minid = closestlkr.begin()->second;
  double mintime = dttrack[ilkr];
  double mintimechod = dtchod[ilkr];
  double mindx = dr[ilkr].X();
  double mindy = dr[ilkr].Y();

  //std::cout << "Final ------------- dtlkr= " << closestlkr.begin()->first << " ilkr = " << ilkr<< std::endl;
  if(fFlag == 0 || fFlag == 10 ){

    fUserMethods->FillHisto("LKrAnalysis_mindistvst_cluster",mintime,mindist);
    fUserMethods->FillHisto("LKrAnalysis_mindistvstchod_cluster",mintimechod,mindist);
    fUserMethods->FillHisto("LKrAnalysis_minxy_cluster",mindx,mindy);
    fUserMethods->FillHisto("LKrAnalysis_energyvsdist_cluster",mindist,cluster->GetClusterEnergy()/1000.);
  }

  fLKrClusterCandidate->SetDiscriminant(mindist);
  fLKrClusterCandidate->SetClusterID(ilkr);
  fLKrClusterCandidate->SetDeltaTime(mintime);
  fLKrClusterCandidate->SetEnergy(cluster->GetClusterEnergy()/1000.);
  fLKrClusterCandidate->SetSeedEnergy(cluster->GetClusterSeedEnergy()/1000.);
  fLKrClusterCandidate->SetX(cluster->GetClusterX());
  fLKrClusterCandidate->SetY(cluster->GetClusterY());
  fLKrClusterCandidate->SetNCells(cluster->GetNCells());
  Bool_t isdeadcell = cluster->GetClusterDDeadCell()<20. ? 1 : 0;
  fLKrClusterCandidate->SetIsDeadCell(isdeadcell);

  return ilkr;
}
Int_t LKrAnal::TrackCellMatching(Double_t chodTime, Double_t trackTime,  TVector3 posatlkr) {
  Double_t xpart = posatlkr.X();
  Double_t ypart = posatlkr.Y();
  Int_t minid = -1;
  Double_t timeshift = 0;
  //  if (fYear==2016) {
  timeshift = -1.51;
  //    if (fFlag==0) timeshift = -1.51;
  //    if (fFlag==2) timeshift = 0.85;
  //  }
  // Look for cells associated to the track, if no cluster candidate matches the track
  Double_t eclus = 0.;
  Double_t tclus = 0.;
  Double_t xclus = 0.;
  Double_t yclus = 0.;
  Int_t ncells = 0;
  Double_t emax = -999999.;
  TClonesArray& Hits = (*(fLKrEvent->GetHits()));
  for (Int_t jHit=0; jHit<fLKrEvent->GetNHits(); jHit++) {
    TRecoLKrHit *hit = (TRecoLKrHit*)Hits[jHit];
    Double_t xcell = hit->GetPosition().X();
    Double_t ycell = hit->GetPosition().Y();
    Double_t ecell = hit->GetEnergy()/1000.;
    if (ecell<=-999999.) continue;
    Double_t t0cell = 0;
    Double_t tcell = hit->GetTime() -1 - t0cell+timeshift;
    Double_t dist = sqrt((xcell-xpart)*(xcell-xpart)+(ycell-ypart)*(ycell-ypart));
    Double_t dtime = tcell-trackTime;
     if (dist<100. && fabs(dtime)<20) { // Select a region around the extrapolated track position at LKr
      eclus += ecell;
      if (emax<ecell) {
        emax = ecell;
        tclus = tcell+1.3;
      }
      xclus += xcell*ecell;
      yclus += ycell*ecell;
      ncells++;
      minid = 1;
    }
  }
  if (!ncells || emax<0.04) { // no cells found
    tclus = -99999.;
    eclus = 0;
    xclus = -99999.;
    yclus = -99999.;
    minid = -1;
    return minid;
  }

  xclus /= eclus;
  yclus /= eclus;
  Double_t ue = eclus;
  if (ncells>9) {
    if (ue<22) eclus = ue/(0.7666+0.0573489*log(ue));
    if (ue>=22 && ue<65) eclus = ue/(0.828962+0.0369797*log(ue));
    if (ue>=65) eclus = ue/(0.828962+0.0369797*log(65));
  }
  eclus *= 1.03;
  eclus = eclus<15 ? (eclus+0.015)/(15+0.015)*15*0.9999 : eclus;
  Double_t mindist = sqrt((xclus-xpart)*(xclus-xpart)+(yclus-ypart)*(yclus-ypart));
  fLKrCellCandidate->SetDiscriminant(mindist);
  fLKrCellCandidate->SetDeltaTime(tclus-trackTime);
  fLKrCellCandidate->SetEnergy(eclus);
  fLKrCellCandidate->SetSeedEnergy(emax);
  fLKrCellCandidate->SetX(xclus);
  fLKrCellCandidate->SetY(yclus);
  fLKrCellCandidate->SetNCells(ncells);
  if (fFlag==0 || fFlag==10) {
    fUserMethods->FillHisto("LKrAnalysis_mindistvst_cell",tclus-trackTime,mindist);
    fUserMethods->FillHisto("LKrAnalysis_mindistvstchod_cell",tclus-chodTime,mindist);
    fUserMethods->FillHisto("LKrAnalysis_minxy_cell",xclus-xpart,yclus-ypart);
    fUserMethods->FillHisto("LKrAnalysis_energyvsdist_cell",mindist,eclus);
  }
  return minid;
}
