#include "Parameters.hh"
#include "AnalysisTools.hh"
#include "EventHeader.hh"
#include "Constants.hh"
#include "LKrAuxClusterReco.hh"
#include "NA62Exceptions.hh"
#include "TSystem.h"
#include "MultiTrackAnal.hh"
#include "GeometricAcceptance.hh"


MultiTrackAnal::MultiTrackAnal(int flag,TString dir,NA62Analysis::Core::BaseAnalysis *ba){

  fFlag=flag;


  switch(flag){
  case 0: fDecay="Pnn"; break;
  case 1: fDecay="Pimin"; break;
  case 2: fDecay="1LAVIRCPrange"; break;
  case 3: fDecay="1LAVSACPrange"; break;
  case 4: fDecay="2LAVs";break;
  case 5: fDecay="Kmu2_ctrl";break;
  case 6: fDecay="K2pi_norm";break;
  case 7: fDecay="K2pi_ctrl"; break;
  case 8: fDecay="Pnn_Ctrl"; break;
  }
  fUserMethods = new UserMethods(ba);
  //Multi-track studies
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_rstraw1",fDecay.Data()),"",120,0,1200,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_rstraw1_vs_zvtx",fDecay.Data()),"",250,50000,200000,120,0,1200));


  fUserMethods->BookHisto(new TH1F(Form("%s_muv0_dtchod",fDecay.Data()),"",100,-100,100));
  fUserMethods->BookHisto(new TH1F(Form("%s_muv0_dtchodmin",fDecay.Data()),"",100,-100,100));
  fUserMethods->BookHisto(new TH2F(Form("%s_muv0_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH1F(Form("%s_hac_dtchod",fDecay.Data()),"",800,-100,100));
  fUserMethods->BookHisto(new TH1F(Form("%s_hac_dtchodmin",fDecay.Data()),"",800,-100,100));
  fUserMethods->BookHisto(new TH2F(Form("%s_hac_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_lkrmerged",fDecay.Data()),"",2,0,2,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_chodslabsintime",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_newchodmult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_chodmult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_lkrmult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_chodmult_vs_lkrmult",fDecay.Data()),"",100,0,100,100,0,100));
  fUserMethods->BookHisto(new TH2F(Form("%s_chodslabsmult_vs_lkrmult",fDecay.Data()),"",100,0,100,100,0,100));
  fUserMethods->BookHisto(new TH2F(Form("%s_newchodmult_vs_lkrmult",fDecay.Data()),"",100,0,100,100,0,100));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_muv1mult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_muv2mult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mmiss_vs_chodnewchodmult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));

  fUserMethods->BookHisto(new TH2F(Form("%s_hmult_mmiss_vs_lkrmult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_hmult_mmiss_vs_newchodmult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_hmult_mmiss_vs_muv1mult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_hmult_mmiss_vs_muv2mult",fDecay.Data()),"",100,0,100,375,-0.1,0.14375));

  fUserMethods->BookHisto(new TH2F(Form("%s_nomult_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mult_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_nomultmuv0_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_nomulthac_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_multmuv0hac_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));

  fUserMethods->BookHisto(new TH2F(Form("%s_muv0_x_vs_y_straw1",fDecay.Data()),"",240,-1200,1200,240,-1200,1200));
  fUserMethods->BookHisto(new TH2F(Form("%s_nomuv0_x_vs_y_straw1",fDecay.Data()),"",240,-1200,1200,240,-1200,1200));
  fUserMethods->BookHisto(new TH2F(Form("%s_hac_x_vs_y_straw1",fDecay.Data()),"",240,-1200,1200,240,-1200,1200));
  fUserMethods->BookHisto(new TH2F(Form("%s_nohac_x_vs_y_straw1",fDecay.Data()),"",240,-1200,1200,240,-1200,1200));

  fUserMethods->BookHisto(new TH2F(Form("%s_mtrej_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mtrej_mmiss_vs_rstraw1",fDecay.Data()),"",120,0,1200,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mtrej_rstraw1_vs_zvtx",fDecay.Data()),"",250,50000,200000,120,0,1200));

  fUserMethods->BookHisto(new TH2F(Form("%s_mtrej_rcut_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mtrej_rcut_mmiss_vs_rstraw1",fDecay.Data()),"",120,0,1200,375,-0.1,0.14375));
  fUserMethods->BookHisto(new TH2F(Form("%s_mtrej_rcut_rstraw1_vs_zvtx",fDecay.Data()),"",250,50000,200000,120,0,1200));
  fUserMethods->BookHisto(new TH2F(Form("%s_mtrej_rcutrej_mmiss_vs_p",fDecay.Data()),"",200,0,100,375,-0.1,0.14375));

}


Int_t MultiTrackAnal::ComputeMultiplicity(DownstreamParticle* track,UpstreamParticle* beamtrack){

  SetDownstreamParticle(track);
  SetUpstreamParticle(beamtrack);

  double xSTRAW_station[4];
  xSTRAW_station[0] = 101.2;
  xSTRAW_station[1] = 114.4;
  xSTRAW_station[2] = 92.4;
  xSTRAW_station[3] = 52.8; // [ mm ]

  double XAtStraw1 = fTrack->GetPositionAtStraw(0).X() - xSTRAW_station[0];
  double YAtStraw1 = fTrack->GetPositionAtStraw(0).Y();
  double XAtLKr    = fTrack->GetPositionAtLKr().X();
  double YAtLKr    = fTrack->GetPositionAtLKr().Y();


  bool multiplicity = false;
  double muv1eouter = fTrack->GetMUV1OuterEnergy()/1000.;
  double muv2eouter = fTrack->GetMUV2OuterEnergy()/1000.;
  Int_t muv1mult = fTrack->GetMUV1OuterNhits();
  Int_t muv2mult = fTrack->GetMUV2OuterNhits();
  Int_t chodmult = fTrack->GetCHODMult();
  Int_t chodslabsintime = fTrack->GetCHODSlabsInTime();
  Int_t chodnewchodmult = fTrack->GetCHODNewCHODMult();
  Int_t newchodmult = fTrack->GetNewCHODMult();
  Int_t lkrmult = fTrack->GetLKrMult();
  Int_t lkrhadrmult = fTrack->GetLKrHadrMult();
  bool lkrmerged    = fTrack->GetLKrMerged();
  double rstraw1 = TMath::Sqrt(XAtStraw1*XAtStraw1 + YAtStraw1*YAtStraw1) ;
  TVector3 vtx = fBeamTrack->GetVertex();
  double mm2;
  if(fDecay.EqualTo("Kmu2_ctrl"))
    mm2=(beamtrack->GetMomentum()-fTrack->GetMuonMomentum()).Mag2();
  else
    mm2=(beamtrack->GetMomentum()-fTrack->GetMomentum()).Mag2();

  double p=fTrack->GetMomentum().P();
  bool    ctrlregion = mm2>0.068 ? 1 :0;

  fUserMethods->FillHisto(Form("%s_mmiss_vs_p",fDecay.Data()),p ,mm2) ;
  fUserMethods->FillHisto(Form("%s_mmiss_vs_rstraw1",fDecay.Data()),rstraw1,mm2);
  fUserMethods->FillHisto(Form("%s_rstraw1_vs_zvtx",fDecay.Data()),vtx.Z(),rstraw1);
  //Check the timing in muv0
  std::map<double,int> muv0map;
  TClonesArray& muv0hits = *(fMUV0Event->GetHits());
  for(int iC=0;iC < fMUV0Event->GetNHits();iC++){

    TRecoMUV0Hit* muv0 = (TRecoMUV0Hit*)muv0hits[iC];
    double dtime = fTrack->GetRICHSingleTime() - muv0->GetTime();
    fUserMethods->FillHisto(Form("%s_muv0_dtchod",fDecay.Data()),dtime);
    muv0map.insert(std::make_pair(fabs(dtime),iC));
  }

  fIsMUV0 = false;

  if(muv0map.size()!=0){
    int imin = muv0map.begin()->second;
    TRecoMUV0Hit* muv0min = (TRecoMUV0Hit*)muv0hits[imin];
    double mintime = fTrack->GetRICHSingleTime() - muv0min->GetTime();
    fUserMethods->FillHisto(Form("%s_muv0_dtchodmin",fDecay.Data()),mintime);
    fUserMethods->FillHisto(Form("%s_muv0_mmiss_vs_p",fDecay.Data()),p,mm2);

      if(mintime > -10 && mintime <  8) fIsMUV0 = true;

    }

  //Check the timing in hasc
  std::map<double,int> hacmap;
  TClonesArray& hachits = *(fHACEvent->GetHits());
  for(int iC=0;iC < fHACEvent->GetNHits();iC++){

    TRecoHACHit* hac = (TRecoHACHit*)hachits[iC];
    double dtime = fTrack->GetRICHSingleTime() - hac->GetTime();

    fUserMethods->FillHisto(Form("%s_hac_dtchod",fDecay.Data()),dtime);
    hacmap.insert(std::make_pair(fabs(dtime),iC));
  }

  fIsHAC=false;

  if(hacmap.size()!=0){
    int imin = hacmap.begin()->second;
    TRecoHACHit* hacmin = (TRecoHACHit*)hachits[imin];
    double mintime = fTrack->GetRICHSingleTime() - hacmin->GetTime();

    fUserMethods->FillHisto(Form("%s_hac_dtchodmin",fDecay.Data()),mintime);
    fUserMethods->FillHisto(Form("%s_hac_mmiss_vs_p",fDecay.Data()),p,mm2);

    if(mintime > -3 && mintime <  3) fIsHAC = true;

  }
  fUserMethods->FillHisto(Form("%s_mmiss_vs_lkrmerged",fDecay.Data()),(double)lkrmerged,mm2);
  fUserMethods->FillHisto(Form("%s_mmiss_vs_chodslabsintime",fDecay.Data()),chodslabsintime,mm2);
  fUserMethods->FillHisto(Form("%s_mmiss_vs_newchodmult",fDecay.Data()),newchodmult,mm2);
  fUserMethods->FillHisto(Form("%s_mmiss_vs_muv1mult",fDecay.Data()),muv1mult,mm2);
  fUserMethods->FillHisto(Form("%s_mmiss_vs_muv2mult",fDecay.Data()),muv2mult,mm2);
  fUserMethods->FillHisto(Form("%s_mmiss_vs_chodmult",fDecay.Data()),chodmult,mm2);
  fUserMethods->FillHisto(Form("%s_mmiss_vs_lkrmult",fDecay.Data()),lkrmult,mm2);
  fUserMethods->FillHisto(Form("%s_chodslabsmult_vs_lkrmult",fDecay.Data()),lkrmult,chodslabsintime);
  fUserMethods->FillHisto(Form("%s_chodmult_vs_lkrmult",fDecay.Data()),lkrmult,chodmult);
  fUserMethods->FillHisto(Form("%s_newchodmult_vs_lkrmult",fDecay.Data()),lkrmult,newchodmult);
  fUserMethods->FillHisto(Form("%s_mmiss_vs_chodnewchodmult",fDecay.Data()),chodnewchodmult,mm2);

  // fUserMethods->FillHisto(Form("%s_muv1eouter_vs_muv1mult",fDecay.Data()),muv1eouter,muv1mult);
  // fUserMethods->FillHisto(Form("%s_muv2eouter_vs_muv2mult",fDecay.Data()),muv2eouter,muv2mult);


  if(newchodmult < 1 && chodmult < 1 && chodnewchodmult < 1 && chodslabsintime <= 3){
    fUserMethods->FillHisto(Form("%s_hmult_mmiss_vs_lkrmult",fDecay.Data()),lkrmult,mm2);
    fUserMethods->FillHisto(Form("%s_hmult_mmiss_vs_newchodmult",fDecay.Data()),newchodmult,mm2);
    fUserMethods->FillHisto(Form("%s_hmult_mmiss_vs_muv1mult",fDecay.Data()),muv1mult,mm2);
    fUserMethods->FillHisto(Form("%s_hmult_mmiss_vs_muv2mult",fDecay.Data()),muv2mult,mm2);
  }


  //if(lkrmult >1) multiplicity = true;
  //if(lkrmult < 2 &&chodmult > 1 && muv1mult > 3) multiplicity = true;
  //if(lkrmult < 2 &&chodmult > 1 && newchodmult > 2) multiplicity = true;
  //if(lkrmult < 2 &&chodmult < 2 && newchodmult > 3) multiplicity = true;

  if(chodmult > 0) multiplicity = true;
  if(newchodmult > 0) multiplicity = true;
  if(chodnewchodmult > 0) multiplicity = true;
  if(chodslabsintime > 3) multiplicity = true;
  if(lkrmerged) multiplicity = true;
  if(!multiplicity) fUserMethods->FillHisto(Form("%s_nomult_mmiss_vs_p",fDecay.Data()),p,mm2);
  if(multiplicity)  fUserMethods->FillHisto(Form("%s_mult_mmiss_vs_p",fDecay.Data()),p,mm2);
  if(!fIsHAC&&!multiplicity)  fUserMethods->FillHisto(Form("%s_nomulthac_mmiss_vs_p",fDecay.Data()),p,mm2);
  if(!fIsMUV0&&!multiplicity) fUserMethods->FillHisto(Form("%s_nomultmuv0_mmiss_vs_p",fDecay.Data()),p,mm2);
  if(!fIsMUV0&&!fIsHAC&&!multiplicity ) fUserMethods->FillHisto(Form("%s_multmuv0hac_mmiss_vs_p",fDecay.Data()),p,mm2);

  if(fIsMUV0) fUserMethods->FillHisto(Form("%s_muv0_x_vs_y_straw1",fDecay.Data()),XAtStraw1,YAtStraw1);
  if(fIsHAC) fUserMethods->FillHisto(Form("%s_hac_x_vs_y_straw1",fDecay.Data()),XAtStraw1,YAtStraw1);
  if(!fIsHAC) fUserMethods->FillHisto(Form("%s_nohac_x_vs_y_straw1",fDecay.Data()),XAtStraw1,YAtStraw1);
  if(!fIsMUV0) fUserMethods->FillHisto(Form("%s_nomuv0_x_vs_y_straw1",fDecay.Data()),XAtStraw1,YAtStraw1);

  if(multiplicity||fIsMUV0||fIsHAC) return 1;
  //if(multiplicity||fIsMUV0) return 1;

  fUserMethods->FillHisto(Form("%s_mtrej_mmiss_vs_p",fDecay.Data()),p ,mm2) ;
  fUserMethods->FillHisto(Form("%s_mtrej_mmiss_vs_rstraw1",fDecay.Data()),rstraw1,mm2);
  fUserMethods->FillHisto(Form("%s_mtrej_rstraw1_vs_zvtx",fDecay.Data()),vtx.Z(),rstraw1);


  double rstraw1incm = rstraw1*0.1; //[in cm]
  double a = 0.00436;
  double b = 83;

  double cutvar = rstraw1incm + a*vtx.Z()*0.1; //[in cm]

  //r straw1 cut
  bool rcut = false;

  if(cutvar < b) rcut = true;

  if(!rcut){
    fUserMethods->FillHisto(Form("%s_mtrej_rcut_mmiss_vs_p",fDecay.Data()),p,mm2);
    fUserMethods->FillHisto(Form("%s_mtrej_rcut_mmiss_vs_rstraw1",fDecay.Data()),rstraw1,mm2);
    fUserMethods->FillHisto(Form("%s_mtrej_rcut_rstraw1_vs_zvtx",fDecay.Data()),vtx.Z(),rstraw1);
  } else
    fUserMethods->FillHisto(Form("%s_mtrej_rcutrej_mmiss_vs_p",fDecay.Data()),p,mm2);

  if(rcut && !fDecay.EqualTo("Kmu2_ctrl")) return 1;

  return 0;
}

void MultiTrackAnal::Print(){
  std::cout << "------- Multiplicities -----" << std::endl;
  std::cout << "CHOD = " << fTrack->GetCHODMult() << std::endl;

  return;
}
