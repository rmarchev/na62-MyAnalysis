#include "StrawCandidate.hh"

StrawCandidate::StrawCandidate()
{}

StrawCandidate::~StrawCandidate()
{}

void StrawCandidate::Clear() {
  fTrackID = -1;
  fGoodTrack = 0;
  fCharge = 0;
  fMultiVertex = 0;
  fMultiCDA = 99999.;
  fAcceptance = 0;
 fAcceptanceStraw= 0;
  fAcceptanceCHOD = 0;
  fAcceptanceNewCHOD = 0;
  fAcceptanceRICH = 0;
  fAcceptanceLKr  = 0;
  fAcceptanceMUV1 = 0;
  fAcceptanceMUV2 = 0;
  fAcceptanceMUV3 = 0;
}
