#include "MUVAnal.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"

MUVAnal::MUVAnal(NA62Analysis::Core::BaseAnalysis *ba) {
  Parameters *par = Parameters::GetInstance();
  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);

  fUserMethods->BookHisto(new TH2F("MUVAnalysis_distvst_muv1","",200,-100,100,300,0,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_xy_muv1","",300,-300,300,300,-300,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_mindistvst_muv1","",200,-100,100,300,0,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_minxy_muv1","",300,-300,300,300,-300,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_energyvsdist_muv1","",300,0,300,400,0,100));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_energyvsenergy_muv1","",400,0,100,400,0,100));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_distvst_muv2","",200,-100,100,300,0,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_xy_muv2","",300,-300,300,300,-300,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_mindistvst_muv2","",200,-100,100,300,0,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_minxy_muv2","",300,-300,300,300,-300,300));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_energyvsdist_muv2","",300,0,300,400,0,100));
  fUserMethods->BookHisto(new TH2F("MUVAnalysis_energyvsenergy_muv2","",400,0,100,400,0,100));

  fMUV1Candidate = new MUVCandidate();
  fMUV2Candidate = new MUVCandidate();
}

MUVAnal::~MUVAnal()
{ }

void MUVAnal::Clear() {
  fMUV1Candidate->Clear();
  fMUV2Candidate->Clear();
}

// Associate a cluster in MUV1 to the track using the position
// The association makes sense if a MIP is associated to the track in LKr
// Use different criteria in case of not MIP in LKr
Int_t MUVAnal::TrackMUV1Matching( TRecoSpectrometerCandidate *thisTrack) {
  TVector3 posatmuv = fTools->GetPositionAtZ(thisTrack,243418.);
  TVector2 postrack(posatmuv.X(),posatmuv.Y());

  // Look for a cluster matching the track (distance criteria only)
  Int_t minid = -1;
  Double_t mindist = 99999999.;
  Double_t mintime = -99999.;
  Double_t mindx = -99999.;
  Double_t mindy = -99999.;
  Double_t minenergy = 0;
  for (Int_t jclus=0; jclus<fMUV1Event->GetNCandidates(); jclus++) {
    TRecoMUV1Candidate *thisCluster = (TRecoMUV1Candidate *)fMUV1Event->GetCandidate(jclus);
    TVector2 posclust(thisCluster->GetPosition().X(),thisCluster->GetPosition().Y());
    Double_t dist = (posclust-postrack).Mod();
    Double_t dtime = thisCluster->GetTime()-thisTrack->GetTime();
    Double_t dx = (posclust-postrack).X();
    Double_t dy = (posclust-postrack).Y();
    fUserMethods->FillHisto("MUVAnalysis_distvst_muv1",dtime,dist);
    fUserMethods->FillHisto("MUVAnalysis_xy_muv1",dx,dy);
    if (dist<mindist) {
      mindist = dist;
      minid = jclus;
      mintime = dtime;
      mindx = dx;
      mindy = dy;
      minenergy = thisCluster->GetEnergy();
    }
  }
  if (minid==-1) return minid;

  fUserMethods->FillHisto("MUVAnalysis_mindistvst_muv1",mintime,mindist);
  fUserMethods->FillHisto("MUVAnalysis_minxy_muv1",mindx,mindy);
  fMUV1Candidate->SetDiscriminant(mindist); // distance of the closest cluster
  fMUV1Candidate->SetDeltaTime(mintime); // time of the closest cluster wrt the track time
  fMUV1Candidate->SetEnergy(minenergy/1000.); // energy of the cluster colsest to the track
  fMUV1Candidate->SetX(mindx);
  fMUV1Candidate->SetY(mindy);


  // Merge close clusters starting from the one matching the track
  // critria are: clusters closer than 150 mm each other and within +- 20 ns from the matching one
  Bool_t clusteradded[300];
  fill(clusteradded,clusteradded+300,0);
  clusteradded[minid] = 1;
  Int_t clusterminid = minid;
  Int_t nmerged = 0;
  Double_t etot = minenergy;
  Double_t emerged = 0;
  Bool_t areClose = 1;
  while(areClose) {
    Double_t mindistclust = 99999999.;
    Double_t minene = 0;
    TRecoMUV1Candidate *prevCluster = (TRecoMUV1Candidate *)fMUV1Event->GetCandidate(minid);
    minid = -1;
    for (Int_t jclus=0; jclus<fMUV1Event->GetNCandidates(); jclus++) { // look for the closest cluster
      if (jclus>=300) continue; // protection against too crowdy events
      if (clusteradded[jclus]==1) continue; // skip already merged clusters
      TRecoMUV1Candidate *thisCluster = (TRecoMUV1Candidate *)fMUV1Event->GetCandidate(jclus);
      Double_t distclust = (thisCluster->GetPosition()-prevCluster->GetPosition()).Mod();
      if (distclust>=500.) continue; // clusters within 150 mm from the previous one
      if (fabs(thisCluster->GetTime()-(mintime+thisTrack->GetTime()))>=20) continue; // cluster in time with the matching one
      if (distclust<mindistclust) {
        mindistclust = distclust;
        minid = jclus;
        minene = thisCluster->GetEnergy();
      }
    }
    if (minid<0) break; // no cluster satisfying the above criteria found
    clusteradded[minid] = 1; // flag the cluster as used
    etot += minene;
    emerged += minene;
    nmerged++;
  }
  fUserMethods->FillHisto("MUVAnalysis_energyvsdist_muv1",mindist,etot/1000.);
  fUserMethods->FillHisto("MUVAnalysis_energyvsenergy_muv1",minenergy/1000.,emerged/1000.);
  fMUV1Candidate->SetNMerged(nmerged);
  fMUV1Candidate->SetEMerged(emerged/1000.);

  return clusterminid;
}

Double_t MUVAnal::ComputeMUV1TotalInTimeEnergy(Double_t timeref, Double_t timecut, TRecoMUV1Event* muvevent) {
  Double_t totEnergy = 0;
  for (Int_t jclus=0; jclus<muvevent->GetNCandidates(); jclus++) {
    TRecoMUV1Candidate *thisCluster = (TRecoMUV1Candidate *)muvevent->GetCandidate(jclus);
    Double_t dtime = thisCluster->GetTime()-timeref;
    if (fabs(dtime)<timecut) totEnergy += thisCluster->GetEnergy();
  }
  return totEnergy;
}

Int_t MUVAnal::TrackMUV2Matching( TRecoSpectrometerCandidate *thisTrack) {
  TVector3 posatmuv = fTools->GetPositionAtZ(thisTrack,244435.);
  TVector2 postrack(posatmuv.X(),posatmuv.Y());

  // Look for a cluster matching the track (distance criteria only)
  Int_t minid = -1;
  Double_t mindist = 99999999.;
  Double_t mintime = -99999.;
  Double_t mindx = -99999.;
  Double_t mindy = -99999.;
  Double_t minenergy = 0;
  for (Int_t jclus=0; jclus<fMUV2Event->GetNCandidates(); jclus++) {
    TRecoMUV2Candidate *thisCluster = (TRecoMUV2Candidate *)fMUV2Event->GetCandidate(jclus);
    TVector2 posclust(thisCluster->GetPosition().X(),thisCluster->GetPosition().Y());
    Double_t dist = (posclust-postrack).Mod();
    Double_t dtime = thisCluster->GetTime()-thisTrack->GetTime();
    Double_t dx = (posclust-postrack).X();
    Double_t dy = (posclust-postrack).Y();
    fUserMethods->FillHisto("MUVAnalysis_distvst_muv2",dtime,dist);
    fUserMethods->FillHisto("MUVAnalysis_xy_muv2",dx,dy);
    if (dist<mindist) {
      mindist = dist;
      minid = jclus;
      mintime = dtime;
      mindx = dx;
      mindy = dy;
      minenergy = thisCluster->GetEnergy();
    }
  }
  if (minid==-1) return minid;
  fUserMethods->FillHisto("MUVAnalysis_mindistvst_muv2",mintime,mindist);
  fUserMethods->FillHisto("MUVAnalysis_minxy_muv2",mindx,mindy);
  fMUV2Candidate->SetDiscriminant(mindist); // distance of the closest cluster
  fMUV2Candidate->SetDeltaTime(mintime); // time of the closest cluster wrt the track time
  fMUV2Candidate->SetEnergy(minenergy/1000.); // energy of the cluster colsest to the track
  fMUV2Candidate->SetX(mindx);
  fMUV2Candidate->SetY(mindy);

  // Merge close clusters starting from the one matching the track
  // critria are: clusters closer than 150 mm each other and within +- 20 ns from the matching one
  Bool_t clusteradded[300];
  fill(clusteradded,clusteradded+300,0);
  clusteradded[minid] = 1;
  Int_t clusterminid = minid;
  Int_t nmerged = 0;
  Double_t etot = minenergy;
  Double_t emerged = 0;
  Bool_t areClose = 1;
  while(areClose) {
    Double_t mindistclust = 99999999.;
    Double_t minene = 0;
    TRecoMUV2Candidate *prevCluster = (TRecoMUV2Candidate *)fMUV2Event->GetCandidate(minid);
    minid = -1;
    for (Int_t jclus=0; jclus<fMUV2Event->GetNCandidates(); jclus++) { // look for the closest cluster
      if (jclus>=300) continue; // protection against too crowdy events
      if (clusteradded[jclus]==1) continue; // skip already merged clusters
      TRecoMUV2Candidate *thisCluster = (TRecoMUV2Candidate *)fMUV2Event->GetCandidate(jclus);
      Double_t distclust = (thisCluster->GetPosition()-prevCluster->GetPosition()).Mod();
      if (distclust>=500.) continue; // clusters within 150 mm from the previous one
      if (fabs(thisCluster->GetTime()-(mintime+thisTrack->GetTime()))>=20) continue; // cluster in time with the matching one
      if (distclust<mindistclust) {
        mindistclust = distclust;
        minid = jclus;
        minene = thisCluster->GetEnergy();
      }
    }
    if (minid<0) break; // no cluster satisfying the above criteria found
    clusteradded[minid] = 1; // flag the cluster as used
    etot += minene;
    emerged += minene;
    nmerged++;
  }
  fUserMethods->FillHisto("MUVAnalysis_energyvsdist_muv2",mindist,etot/1000.);
  fUserMethods->FillHisto("MUVAnalysis_energyvsenergy_muv2",minenergy/1000.,emerged/1000.);
  fMUV2Candidate->SetNMerged(nmerged);
  fMUV2Candidate->SetEMerged(emerged/1000.);

  return clusterminid;
}

Double_t MUVAnal::ComputeMUV2TotalInTimeEnergy(Double_t timeref, Double_t timecut, TRecoMUV2Event* muvevent) {
  Double_t totEnergy = 0;
  for (Int_t jclus=0; jclus<muvevent->GetNCandidates(); jclus++) {
    TRecoMUV2Candidate *thisCluster = (TRecoMUV2Candidate *)muvevent->GetCandidate(jclus);
    Double_t dtime = thisCluster->GetTime()-timeref;
    if (fabs(dtime)<timecut) totEnergy += thisCluster->GetEnergy();
  }
  return totEnergy;

}
