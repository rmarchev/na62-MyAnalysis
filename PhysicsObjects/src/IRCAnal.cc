#include "IRCAnal.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"
//#include "PhotonVetoCandidate.cc"

IRCAnal::IRCAnal(NA62Analysis::Core::BaseAnalysis *ba) {
  fPar = Parameters::GetInstance();

  // IRC priority definition
  fIRCPriorityMask[0] = 0; // --SNH--
  fIRCPriorityMask[1] = 4; // LL __ __ __
  fIRCPriorityMask[2] = 5; // __ LH __ __
  fIRCPriorityMask[3] =10; // LL LH __ __
  fIRCPriorityMask[4] = 2; // __ __ TH __
  fIRCPriorityMask[5] = 7; // LL __ TH __
  fIRCPriorityMask[6] =11; // __ LH TH __
  fIRCPriorityMask[7] =13; // LL LH TH __
  fIRCPriorityMask[8] = 1; // __ __ __ TL
  fIRCPriorityMask[9] =12; // LL __ __ TL
  fIRCPriorityMask[10]= 6; // __ LH __ TL
  fIRCPriorityMask[11]=14; // LL LH __ TL
  fIRCPriorityMask[12]= 3; // __ __ TH TL
  fIRCPriorityMask[13]= 8; // LL __ TH TL
  fIRCPriorityMask[14]= 9; // __ LH TH TL
  fIRCPriorityMask[15]=15; // LL LH TH TL

  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);

  // Histo booking
  fUserMethods->BookHisto(new TH2F("IRCAnalysis_hit_chtime","",10,0,10,400,-50,50));
  fUserMethods->BookHisto(new TH2F("IRCAnalysis_hit_tottime","",400,0,200,400,-50,50));
  fUserMethods->BookHisto(new TH2F("IRCAnalysis_hit_besttime","",400,0,200,400,-50,50));
  fUserMethods->BookHisto(new TH2F("IRCAnalysis_hit_mintime","",400,0,200,400,-50,50));

  // Photon candidates
  for (Int_t kk=0; kk<20; kk++) fPhotonCandidate[kk] = new PhotonVetoCandidate();
}


void IRCAnal::StartBurst(Int_t year,Int_t runid) {
  fYear = year;
  fRunID = runid;
  fIsMC = fUserMethods->GetWithMC();
  if (year==2015) fTimeCut = 10;
  if (year==2016) fTimeCut = 7;
}

void IRCAnal::Clear() {
  for (Int_t kk=0; kk<20; kk++) fPhotonCandidate[kk]->Clear();
}

Int_t IRCAnal::MakeCandidate(Double_t reftime, TRecoIRCEvent* event) {

  TClonesArray& Hits = (*(event->GetHits()));

  // At least a good hit within 5 ns from the reference time
  Double_t totest[4] = {-1.3,-1.3,-1.3,-1.3};
  if (!fIsMC && fRunID>7000) {
    totest[0] += -0.65+1;
    totest[1] += 0.6-0.25;
    totest[2] += 0+0.26;
    totest[3] += 0.36;
  }
  if (fIsMC) {
    totest[0] = -2.5;
    totest[1] = -2.5;
    totest[2] = -2.5;
    totest[3] = -2.5;
  }

  Int_t bestIRCHitType = 0;
  for (Int_t jHit=0; jHit<event->GetNHits(); jHit++) {
    TRecoIRCHit *hit = (TRecoIRCHit*)Hits[jHit];
    Int_t chid = hit->GetChannelID();
    Int_t edge = hit->GetEdgeMask();
    double time = TimeSlewCorrected(hit);
    Double_t dtime = time-reftime-totest[chid];
    fUserMethods->FillHisto("IRCAnalysis_hit_chtime",chid,dtime);
    if (fIRCPriorityMask[edge]>bestIRCHitType) bestIRCHitType = fIRCPriorityMask[edge]; //removed conditions on 10 ns, not needed and dangerous
  }
  if (bestIRCHitType<7 && bestIRCHitType!=4) return 0;

  Int_t nirc = 0;
  Double_t etot = 0;
  Double_t etot2 = 0;
  Double_t maxtot = 0;
  Double_t maxtime = 999999.;
  Double_t mintime = 999999.;
  Double_t mintime2 = 999999.;
  Double_t mintime3 = 999999.;

  for (Int_t jHit=0; jHit<event->GetNHits(); jHit++) {
    TRecoIRCHit *hit = (TRecoIRCHit*)Hits[jHit];
    Int_t chid = hit->GetChannelID();
    Int_t edge = hit->GetEdgeMask();
    //if (fIRCPriorityMask[edge]<7 && bestIRCHitType!=4) continue; // BUG
    if (fIRCPriorityMask[edge]<7 && fIRCPriorityMask[edge]!=4) continue; // BUG removed
    if (fYear==2016) {
      if (fIRCPriorityMask[edge]<9) continue;
    }
    double time = TimeSlewCorrected(hit);

    Double_t dtime = time-reftime+0.52-totest[chid];
    Double_t tot = (hit->GetTrailingEdgeLow() && hit->GetLeadingEdgeLow()) ? hit->GetTrailingEdgeLow()-hit->GetLeadingEdgeLow() : 0;

    fUserMethods->FillHisto("IRCAnalysis_hit_tottime",tot,dtime);

    if (tot>=2 && fabs(dtime)<fabs(mintime)) {
       mintime = dtime;
       etot = tot; // save tot of this hit
    }
    if (tot<2 && fabs(dtime)<fabs(mintime2)) {
      mintime2 = dtime;
      etot2 = tot; // save tot of this hit
    }
    if (tot<2 && fabs(dtime-7)<fabs(mintime3-7)) { // double peak at tot = 0
      mintime3 = dtime;
      etot2 = tot; // save tot of this hit
    }

    // hit with max tot
    if (tot>=maxtot) {
      maxtot = tot;
      maxtime = dtime;
    }


    // if (fabs(dtime)<fabs(mintime)) { // hit closest in time to ref
    //    mintime = dtime;
    // }
    // etot += tot;
    // if (tot>=maxtot) {
    //   maxtot = tot;
    //   maxtime = dtime;
    // }

    if (nirc<20) {
        fPhotonCandidate[nirc]->SetTime(dtime+reftime);
        fPhotonCandidate[nirc]->SetID(jHit);
    }
    nirc++;
  }

  if (nirc) {
      fUserMethods->FillHisto("IRCAnalysis_hit_besttime",etot,maxtime);
//    cout << mintime << " " << mintime2 << endl;
//    fUserMethods->FillHisto("IRCAnalysis_hit_mintime",etot,mintime);
      if (mintime<999999.) fUserMethods->FillHisto("IRCAnalysis_hit_mintime",etot,mintime);
      if (mintime2<999999.) fUserMethods->FillHisto("IRCAnalysis_hit_mintime",etot2,mintime2);
      if (mintime3<999999.) fUserMethods->FillHisto("IRCAnalysis_hit_mintime",etot2,mintime3);
  }


//  if (nirc && fabs(mintime)<fTimeCut) return 1;
  if (nirc) { // Improved treatment of hits with missing slewing
//    if (etot>2 && fabs(mintime)<fTimeCut) return 1;
//    if (etot<=2 && mintime>-5 && mintime<=10.) return 1;
//    if (etot<=2 && mintime2>-5 && mintime2<=10.) return 1;
      if (etot>=2 && etot<999999. && fabs(mintime)<fTimeCut) return 1;
      if (etot2<2 && fabs(mintime2)<7) return 1;
      if (etot2<2 && fabs(mintime3-7)<7) return 1; // double peak check at tot = 0
  }


//  if (nirc && fabs(mintime)<fTimeCut) return 1;
  return 0;
}

double IRCAnal::TimeSlewCorrected(TRecoIRCHit *hit) {
  double tot = (hit->GetTrailingEdgeLow() && hit->GetLeadingEdgeLow()) ? hit->GetTrailingEdgeLow()-hit->GetLeadingEdgeLow() : 0;
  double tslew = 0;
  if (tot>2&&tot<40) tslew = 6.38-0.303*tot+0.003578*tot*tot;
  if (tot>=40 && tot<60) tslew = 6.38-0.303*40+0.003578*40*40;
  if (tot>2&&tot<15) tslew += 1.2;
  return hit->GetTime()-tslew;
}
