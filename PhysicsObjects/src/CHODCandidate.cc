#include "CHODCandidate.hh"

CHODCandidate::CHODCandidate()
{}

CHODCandidate::~CHODCandidate()
{}

void CHODCandidate::Clear() {
  fLKrMult=0;              //Multiplicity in LKr in time and space with CHOD and NewCHOD
  fCHODNewCHODMult=0;              //Multiplicity in LKr in time and space with CHOD and NewCHOD
  fCHODMult=0;              //Multiplicity in LKr in time and space with CHOD and NewCHOD
  fNewCHODMult=0;              //Multiplicity in LKr in time and space with CHOD and NewCHOD
  fLKrCHODdR     = TVector3(9999,9999,9999);
  fLKrNewCHODdR  = TVector3(9999,9999,9999);
  fCHODNewCHODdR = TVector3(9999,9999,9999);
  fCHODNewCHODdt = 9999;
  fLKrNewCHODdt = 9999;
  fLKrCHODdt=9999;

  fDiscriminant = 99999999.;
  fDeltaTime = 99999999.;
  fX = -99999.;
  fY = -99999.;
  fCounterV = -1;
  fCounterH = -1;
  fTimeV = -99999.;
  fTimeH = -99999.;
  fIsCHODCandidate = 0;
  fHitHID=0;
  fHitVID=0;
}
