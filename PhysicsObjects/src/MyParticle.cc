#include "MyParticle.hh"

//Particle::Particle()
//{ }
//
//Particle::~Particle()
//{ }

void MyParticle::Clear()
{
  fTrackID = -1;
  fMomentum.SetXYZM(0.,0.,0.,0.);
  fPosition.SetXYZ(0.,0.,0.);
  fTime = -99999.;
  fQuality = -1.;
  fType = 0;
}
