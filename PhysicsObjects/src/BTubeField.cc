#include "BTubeField.hh"
#include "TMath.h"

BTubeField::BTubeField() {
  fEC = TMath::C()* 1.e-9 * 1.e-4 * 1.e-2;
  SetBTube();
}

BTubeField::~BTubeField() {
}

void BTubeField::GetCorrection(TVector3 *vertex, Double_t *dxdz, Double_t *dydz, Double_t pmom) {
  Double_t zend = vertex->Z();
  Double_t zini = 180000.;
  if (zend>=zini) return;

  // Starting point
  Double_t thex = (*dxdz);
  Double_t they = (*dydz);
  Double_t xposin = vertex->X()+(*dxdz)*(zini-zend);
  Double_t yposin = vertex->Y()+(*dydz)*(zini-zend);

  // Start integration
  TVector3 bsurface[2];
  Int_t nZStep = 40;
  for (Int_t jz=0; jz<nZStep; jz++) {
    Double_t zstepin = fZTube[jz]; 
    Double_t zstart = zini>=zstepin ? zstepin : zini; // zini for jz=0 only
    if (jz==nZStep-1) { // Z vertex outside the region with measured field -> free propagation
      xposin += thex*(zend-zstart);
      yposin += they*(zend-zstart);
      continue; 
    }
    bsurface[0] = ComputeBFieldAtXY(xposin+thex*(zstepin-zstart),yposin+they*(zstepin-zstart),jz);
    Double_t zstepout = fZTube[jz+1]; 
    Double_t zstop = zend<=zstepout ? zstepout : zend;
    Double_t dz = zstart-zstop;
    Double_t xposout = xposin-thex*dz;
    Double_t yposout = yposin-they*dz;
    TVector3 partpos(xposout,yposout,0.);
    bsurface[1] = ComputeBFieldAtXY(xposout+thex*(zstepout-zstop),yposout+they*(zstepout-zstop),jz+1);
    TVector3 baverage = ComputeBFieldAtZ(zstart,zstop,bsurface,jz);

    // Momentum in 
    TVector3 partdir(thex,they,0);
    TVector3 pin;
    pin.SetZ(pmom/sqrt(1.+thex*thex+they*they)); 
    pin.SetX(pin.Z()*thex);
    pin.SetY(pin.Z()*they);

    // Reference system rotation (y along B)
    TVector3 yversor(0,1,0);
    TVector3 bperp = (baverage.Unit()).Cross(yversor);
    Double_t alphab = baverage.Angle(yversor);
    TVector3 brotated = baverage;
    brotated.Rotate(alphab,bperp); 
    TVector3 protated = pin;
    protated.Rotate(alphab,bperp);  
    TVector3 beta = (1/pmom)*protated;
    TVector3 ptkick = -1000*fEC*(beta.Cross(brotated))*fabs(dz);
    TVector3 partposrotated = partpos;
    partposrotated.Rotate(alphab,bperp); 
    partposrotated -= 0.5*dz*(1/pmom)*ptkick;
    TVector3 partdirrotated = partdir;
    partdirrotated.Rotate(alphab,bperp);
    partdirrotated += (1/pmom)*ptkick;

    // Rotate back in the NA62 reference system
    ptkick.Rotate(-alphab,bperp);
    partposrotated.Rotate(-alphab,bperp);
    partdirrotated.Rotate(-alphab,bperp);

    // Modify direction and position 
    thex = partdirrotated.X();
    they = partdirrotated.Y();
    xposin = partposrotated.X();  
    yposin = partposrotated.Y();  
    if (zend>zstepout) break;
  }

  // Recompute slope and positions
  *dxdz = thex;
  *dydz = they;
  vertex->SetX(xposin);
  vertex->SetY(yposin);
} 

TVector3 BTubeField::ComputeBFieldAtXY(Double_t xpos, Double_t ypos, Int_t jz) {
  Double_t xypass = 99999;
  Double_t xyoffs = 99999;
  Int_t idmax = 0;
  xypass = 100.;
  xyoffs = -800;
  idmax = 16;
  Int_t jx = (xpos-xyoffs)/fabs(xypass); 
  Int_t jy = (ypos-xyoffs)/fabs(xypass); 
  Double_t xposgrid = xyoffs+jx*xypass;
  Double_t yposgrid = xyoffs+jy*xypass;
  Double_t dx = fabs(xpos-xposgrid)/fabs(xypass);
  Double_t dy = fabs(ypos-yposgrid)/fabs(xypass);
  Int_t kx = jx<idmax ? jx : idmax;
  if (kx<0) kx=0;
  Int_t ky = jy<idmax ? jy : idmax;
  if (ky<0) ky=0;
  Int_t kxnext = kx<idmax ? kx+1 : kx;
  Int_t kynext = ky<idmax ? ky+1 : ky;
  TVector3 b1 = GetBTube(kx,ky,jz);
  TVector3 b2 = GetBTube(kxnext,ky,jz);
  TVector3 b3 = GetBTube(kx,kynext,jz);
  TVector3 b4 = GetBTube(kxnext,kynext,jz);
  TVector3 bvalue = (1-dx-dy+dx*dy)*b1+(dx-dx*dy)*b2+(dy-dx*dy)*b3+dx*dy*b4;
  return bvalue;
}

TVector3 BTubeField::ComputeBFieldAtZ(Double_t zstart, Double_t zstop, TVector3 *bsurface, Int_t jz) {
  Double_t weight[2] = {0,0};
  Double_t zpass = fZTube[jz]-fZTube[jz+1]; 
  weight[0] = fabs(zstart-fZTube[jz])/fabs(zpass);
  weight[1] = fabs(zstop-fZTube[jz+1])/fabs(zpass);
  TVector3 bstart = bsurface[0]*(1-weight[0])+bsurface[1]*weight[0];
  TVector3 bstop  = bsurface[0]*weight[1]+bsurface[1]*(1-weight[1]);
  TVector3 baverage = 0.5*(bstart+bstop);
  return baverage;
}

void BTubeField::SetBTube() {
  for (Int_t jz=0; jz<40; jz++) {
    for (Int_t jx=0; jx<11; jx++) {
      for (Int_t jy=0; jy<11; jy++) {
        fBxTube[jx][jy][jz] = 0.;
        fByTube[jx][jy][jz] = 0.;
        fBzTube[jx][jy][jz] = 0.;
      }
    }
    fZTube[jz] = -99999.;
  }

  ifstream btubefile("/afs/cern.ch/user/r/ruggiero/workspace/public/database/bresidual.txt");
  if (!btubefile.is_open()) {
    cout << "Error: B tube file not found" << endl;
    exit(1);
  }
   
  Int_t kk=0;
  Int_t kz = 0;
  while(!btubefile.eof()) {
    Int_t kxy = kk%29;
    Double_t xpos;
    Double_t ypos;
    Double_t zpos;
    Double_t bx;
    Double_t by;
    Double_t bz;
    btubefile >> xpos >> ypos >> zpos >> bx >> by >> bz;
    fbx[kz][kxy] = bx;
    fby[kz][kxy] = by;
    fbz[kz][kxy] = bz;
    if (kxy==28) fZTube[kz] = zpos;
    if (kxy==28 && kz==39) break;
    if (kxy==28) kz++;
    kk++;     
  }
  btubefile.close();

  for (Int_t jz=0; jz<40; jz++) {
    for (Int_t jx=0; jx<17; jx++) {
      for (Int_t jy=0; jy<17; jy++) {
        if (jx<  3 && jy<  3) continue;
        if (jx> 13 && jy<  3) continue;
        if (jx> 13 && jy> 13) continue;
        if (jx<  3 && jy> 13) continue; 

        if (jy>= 0 && jy<= 3) { if (jx>= 3 && jx<= 8) { Int_t idarray[9] = {jx,jy,jz, 3, 0,23,24,13,14}; FillTubeField(idarray,500,300); }  // External bottom
                                if (jx>= 8 && jx<=13) { Int_t idarray[9] = {jx,jy,jz, 8, 0,24,25,14,15}; FillTubeField(idarray,500,300); }}
        if (jy>=13 && jy<=16) { if (jx>= 3 && jx<= 8) { Int_t idarray[9] = {jx,jy,jz, 3,13,11,12,19,20}; FillTubeField(idarray,500,300); }  // External up
                                if (jx>= 8 && jx<=13) { Int_t idarray[9] = {jx,jy,jz, 8,13,10, 9,18,17}; FillTubeField(idarray,500,300); }}
        if (jx>= 0 && jx<= 3) { if (jy>= 3 && jy<= 8) { Int_t idarray[9] = {jx,jy,jz, 0, 3,22,13,21,12}; FillTubeField(idarray,300,500); }  // External left
                                if (jy>= 8 && jy<=13) { Int_t idarray[9] = {jx,jy,jz, 0, 8,21,12,20,11}; FillTubeField(idarray,300,500); }}
        if (jx>=13 && jx<=16) { if (jy>= 3 && jy<= 8) { Int_t idarray[9] = {jx,jy,jz,13, 3,15,26,16,27}; FillTubeField(idarray,300,500); }  // External right
                                if (jy>= 8 && jy<=13) { Int_t idarray[9] = {jx,jy,jz,13, 8,16,27, 9,28}; FillTubeField(idarray,300,500); }}
      }
    }
    for (Int_t jx=0; jx<17; jx++) {
      for (Int_t jy=0; jy<17; jy++) {
        if (jx>= 6 && jx<  8) { if (jy>= 6 && jy<= 8) { Int_t idarray[9] = {jx,jy,jz, 6, 6, 4, 5, 3, 8}; FillTubeField(idarray,200,200); }  // Inner column left 
                                if (jy>= 8 && jy<=10) { Int_t idarray[9] = {jx,jy,jz, 6, 8, 3, 8, 2, 1}; FillTubeField(idarray,200,200); }
                                if (jy>= 3 && jy<= 6) { Int_t idarray[9] = {jx,jy,jz, 6, 3,-1,14, 4, 5}; FillTubeField(idarray,200,300); }
                                if (jy>=10 && jy<=13) { Int_t idarray[9] = {jx,jy,jz, 6,10, 2, 1,-1,10}; FillTubeField(idarray,200,300); }} 
        if (jx>= 8 && jx< 10) { if (jy>= 6 && jy<= 8) { Int_t idarray[9] = {jx,jy,jz, 8, 6, 5, 6, 8, 7}; FillTubeField(idarray,200,200); }  // Inner column right
                                if (jy>= 8 && jy<=10) { Int_t idarray[9] = {jx,jy,jz, 8, 8, 8, 7, 1, 0}; FillTubeField(idarray,200,200); }
                                if (jy>= 3 && jy<= 6) { Int_t idarray[9] = {jx,jy,jz, 8, 3,14,-1, 5, 6}; FillTubeField(idarray,200,300); }
                                if (jy>=10 && jy<=13) { Int_t idarray[9] = {jx,jy,jz, 8,10, 1, 0,10,-1}; FillTubeField(idarray,200,300); }} 
        if (jx>= 3 && jx<  6) { if (jy>= 6 && jy<= 8) { Int_t idarray[9] = {jx,jy,jz, 3, 6,-1, 4,12, 3}; FillTubeField(idarray,300,200); }  // Middle column left
                                if (jy>= 8 && jy<=10) { Int_t idarray[9] = {jx,jy,jz, 3, 8,12, 3,-1, 2}; FillTubeField(idarray,300,200); }
                                if (jy>= 3 && jy<= 6) { Int_t idarray[9] = {jx,jy,jz, 3, 3,13,-1,-1, 4}; FillTubeField(idarray,300,300); }
                                if (jy>=10 && jy<=13) { Int_t idarray[9] = {jx,jy,jz, 3,10,-1, 2,11,-1}; FillTubeField(idarray,300,300); }}
        if (jx>=10 && jx< 13) { if (jy>= 6 && jy<= 8) { Int_t idarray[9] = {jx,jy,jz,10, 6, 6,-1, 7,16}; FillTubeField(idarray,300,200); }  // Middle column right
                                if (jy>= 8 && jy<=10) { Int_t idarray[9] = {jx,jy,jz,10, 8, 7,16, 0,-1}; FillTubeField(idarray,300,200); }
                                if (jy>= 3 && jy<= 6) { Int_t idarray[9] = {jx,jy,jz,10, 3,-1,15, 6,-1}; FillTubeField(idarray,300,300); }
                                if (jy>=10 && jy<=13) { Int_t idarray[9] = {jx,jy,jz,10,10, 0,-1,-1, 9}; FillTubeField(idarray,300,300); }}
      }
    }
  }
}

void BTubeField::FillTubeField(Int_t *idarray, Double_t passx, Double_t passy) {
  Int_t jx = idarray[0];
  Int_t jy = idarray[1];
  Int_t jz = idarray[2];
  Int_t cx = idarray[3];
  Int_t cy = idarray[4];
  Int_t nid[4];
  nid[0] = idarray[5];
  nid[1] = idarray[6];
  nid[2] = idarray[7];
  nid[3] = idarray[8];
  Double_t b1x = nid[0]>=0 ? fbx[jz][nid[0]] : 0.; 
  Double_t b2x = nid[1]>=0 ? fbx[jz][nid[1]] : 0.; 
  Double_t b3x = nid[2]>=0 ? fbx[jz][nid[2]] : 0.; 
  Double_t b4x = nid[3]>=0 ? fbx[jz][nid[3]] : 0.; 
  Double_t b1y = nid[0]>=0 ? fby[jz][nid[0]] : 0.; 
  Double_t b2y = nid[1]>=0 ? fby[jz][nid[1]] : 0.; 
  Double_t b3y = nid[2]>=0 ? fby[jz][nid[2]] : 0.; 
  Double_t b4y = nid[3]>=0 ? fby[jz][nid[3]] : 0.; 
  Double_t b1z = nid[0]>=0 ? fbz[jz][nid[0]] : 0.; 
  Double_t b2z = nid[1]>=0 ? fbz[jz][nid[1]] : 0.; 
  Double_t b3z = nid[2]>=0 ? fbz[jz][nid[2]] : 0.; 
  Double_t b4z = nid[3]>=0 ? fbz[jz][nid[3]] : 0.; 
  if (nid[0]==-1) {
    b1x = fBxTube[cx][cy][jz]*1000;
    b1y = fByTube[cx][cy][jz]*1000;
    b1z = fBzTube[cx][cy][jz]*1000;
  }
  if (nid[1]==-1) {
    if (cx==8 && cy==3) {
      b2x = fBxTube[cx+2][cy][jz]*1000;
      b2y = fByTube[cx+2][cy][jz]*1000;
      b2z = fBzTube[cx+2][cy][jz]*1000;
    } else {
      b2x = fBxTube[cx+3][cy][jz]*1000;
      b2y = fByTube[cx+3][cy][jz]*1000;
      b2z = fBzTube[cx+3][cy][jz]*1000;
    } 
  }
  if (nid[2]==-1) {
    if (cx==3 && cy==8) {
      b3x = fBxTube[cx][cy+2][jz]*1000;
      b3y = fByTube[cx][cy+2][jz]*1000;
      b3z = fBzTube[cx][cy+2][jz]*1000;
    } else {
      b3x = fBxTube[cx][cy+3][jz]*1000;
      b3y = fByTube[cx][cy+3][jz]*1000;
      b3z = fBzTube[cx][cy+3][jz]*1000;
    } 
  }
  if (nid[3]==-1) {
    if (cx==8 && cy==10) {
      b4x = fBxTube[cx+2][cy+3][jz]*1000;
      b4y = fByTube[cx+2][cy+3][jz]*1000;
      b4z = fBzTube[cx+2][cy+3][jz]*1000;
    } else if (cx==10 && cy==8) {
      b4x = fBxTube[cx+3][cy+2][jz]*1000;
      b4y = fByTube[cx+3][cy+2][jz]*1000;
      b4z = fBzTube[cx+3][cy+2][jz]*1000;
    } else {
      b4x = fBxTube[cx+3][cy+3][jz]*1000;
      b4y = fByTube[cx+3][cy+3][jz]*1000;
      b4z = fBzTube[cx+3][cy+3][jz]*1000;
    } 
  } 

  Double_t xgrid = -800+100*jx;
  Double_t ygrid = -800+100*jy;
  Double_t xcorner = -800+100*cx;
  Double_t ycorner = -800+100*cy;
  Double_t dx = fabs(xgrid-xcorner)/passx;
  Double_t dy = fabs(ygrid-ycorner)/passy;
  fBxTube[jx][jy][jz] = ((1-dx-dy+dx*dy)*b1x+(dx-dx*dy)*b2x+(dy-dx*dy)*b3x+dx*dy*b4x)/1000;
  fByTube[jx][jy][jz] = ((1-dx-dy+dx*dy)*b1y+(dx-dx*dy)*b2y+(dy-dx*dy)*b3y+dx*dy*b4y)/1000;
  fBzTube[jx][jy][jz] = ((1-dx-dy+dx*dy)*b1z+(dx-dx*dy)*b2z+(dy-dx*dy)*b3z+dx*dy*b4z)/1000;
}
