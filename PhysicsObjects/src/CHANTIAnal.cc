#include "CHANTIAnal.hh"
#include "CHANTICandidate.cc"
#include "AnalysisTools.hh"
#include "Parameters.hh"

CHANTIAnal::CHANTIAnal(NA62Analysis::Core::BaseAnalysis *ba) {
  Parameters *par = Parameters::GetInstance();
  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);
  fUserMethods->BookHisto(new TH2F("CHANTIAnalysis_dtime_ref_singlehit","",500,-50,50,20,0,20));
  fUserMethods->BookHisto(new TH2F("CHANTIAnalysis_dtime_ref","",500,-50,50,20,0,20));
  fUserMethods->BookHisto(new TH2F("CHANTIAnalysis_dtime_gtk","",500,-50,50,20,0,20));
  fUserMethods->BookHisto(new TH2F("CHANTIAnalysis_dtime_ktg","",500,-50,50,20,0,20));
  fCHANTICandidate = new CHANTICandidate();
}


void CHANTIAnal::StartBurst(Int_t year, Int_t run) {
  fYear = year;
  fRun = run;
}

void CHANTIAnal::Clear() {
  fCHANTICandidate->Clear();
}

Int_t CHANTIAnal::TrackMatching(Int_t noGTK, Double_t timeref, Double_t timegtk, Double_t timektg) {
  Double_t mintime = 999999;
  Double_t minid = -1;
  Double_t timecorr = 0;

  std::map<double, std::pair<double,int>> dtrich;
  std::map<double, std::pair<double,int>> dtktag;
  std::map<double, std::pair<double,int>> dtgtk;


  if (!noGTK) {
    //if (fRun<6200) timecorr = -1.2;
    //if (fRun>=6200) timecorr = -0.2;
  }

  Double_t timeCut = 0;
  if (fYear==2015) timeCut = 8.;
  if (fYear==2016) timeCut = 3.;

  for (Int_t jcand=0; jcand<fCHANTIEvent->GetNCandidates(); jcand++) {
    TRecoCHANTICandidate *cand = (TRecoCHANTICandidate *)fCHANTIEvent->GetCandidate(jcand);
    Double_t dtime_ref = cand->GetTime()+timecorr-timeref; // timing shift added
    Double_t dtime_gtk = cand->GetTime()+timecorr-timegtk;
    Double_t dtime_ktg = cand->GetTime()+timecorr-timektg;
    fUserMethods->FillHisto("CHANTIAnalysis_dtime_ref_singlehit",dtime_ref,cand->GetNHits());
    if (!cand->GetXYMult()) continue; // At least a XY coincidence
    fUserMethods->FillHisto("CHANTIAnalysis_dtime_ref",dtime_ref,cand->GetNHits());
    fUserMethods->FillHisto("CHANTIAnalysis_dtime_ktg",dtime_ktg,cand->GetNHits());
    fUserMethods->FillHisto("CHANTIAnalysis_dtime_gtk",dtime_gtk,cand->GetNHits());

    dtrich.insert(std::make_pair(fabs(dtime_ref),std::pair<double,int>(cand->GetTime(),jcand)));
    dtktag.insert(std::make_pair(fabs(dtime_ktg),std::pair<double,int>(cand->GetTime(),jcand)));
    dtgtk.insert(std::make_pair(fabs(dtime_gtk),std::pair<double,int>(cand->GetTime(),jcand)));

  }


  if(dtrich.size()==0 && dtktag.size()==0 && dtgtk.size()==0) return 0;
  if(dtrich.size()>0){

    double iminrich  = dtrich.begin()->second.second;
    double mindtrich = dtrich.begin()->first;
    if(mindtrich < 3){


      fCHANTICandidate->SetID(iminrich);
      fCHANTICandidate->SetTime(dtrich.begin()->second.first);
      fCHANTICandidate->SetIsCHANTICandidate(1);

    }

  }

  if(dtktag.size()>0){

    double iminktag  = dtktag.begin()->second.second;
    double mindtktag = dtktag.begin()->first;
    if(mindtktag < 3){


      fCHANTICandidate->SetID(iminktag);
      fCHANTICandidate->SetTime(dtktag.begin()->second.first);
      fCHANTICandidate->SetIsCHANTICandidate(1);

    }

  }

  if(dtgtk.size()>0){

    double imingtk  = dtgtk.begin()->second.second;
    double mindtgtk = dtgtk.begin()->first;
    if(mindtgtk < 3){


      fCHANTICandidate->SetID(imingtk);
      fCHANTICandidate->SetTime(dtgtk.begin()->second.first);
      fCHANTICandidate->SetIsCHANTICandidate(1);

    }

  }

  if(!fCHANTICandidate->GetIsCHANTICandidate()) return 0;
  return 1;
}
