#include "DetectorAnalysis.hh"
#include "Constants.hh"
#include <iostream>
//#include "utl.hh"

//namespace rado{

//VDetectorRapper::VDetectorRapper(TDetectorVEvent* evt){
//  fVEvt = evt;
//  fTime =-99999;
//
//}
// StrawRapper::StrawRapper(TDetectorVEvent* evt):VDetectorRapper(evt) , fEvt((TRecoSpectrometerEvent*)fVEvt) {
// StrawRapper::StrawRapper(TDetectorVEvent* evt, NA62Analysis::Core::BaseAnalysis *ba): VDetectorRapper(evt) ,fEvt((TRecoSpectrometerEvent*) fVEvt)  {

// StrawRapper::StrawRapper( TRecoSpectrometerEvent* evt, NA62Analysis::Core::BaseAnalysis *ba): fEvt(evt)  {
StrawRapper::StrawRapper(NA62Analysis::Core::BaseAnalysis *ba)  {


  fUserMethods = new UserMethods(ba);
  fUserMethods->BookHisto(new TH1F("Straw_NTracks","",100,0,100));
  //SetParameters();
  //Analysis();
}

//SetEvent();

void StrawRapper::SetParameters(){

  fZ1 = Constants::zSTRAW_station[0];
  fZ2 = Constants::zSTRAW_station[1];
  fZ3 = Constants::zSTRAW_station[2];
  fZ4 = Constants::zSTRAW_station[3];
  fNCand = fEvt->GetNCandidates();

}

// void StrawRapper::Analysis(){

void StrawRapper::Analysis(TRecoSpectrometerEvent* evt){

  fEvt = evt;

  fUserMethods->FillHisto("Straw_NTracks", fNCand);

  for(int iC(0); iC<fNCand; iC++){

    TRecoSpectrometerCandidate*  track = (TRecoSpectrometerCandidate*)fEvt->GetCandidate(iC);

    //Alpha and beta corrections
    ApplyAlphaBeta(/* Alpha */0.51e-8,/*Beta*/-1.33e-3,track);

  }
}

//------------Alpha and beta corrections-------------//
void StrawRapper::ApplyAlphaBeta(double alpha,double beta, TRecoSpectrometerCandidate* track){

  double charge = track->GetCharge();
  track->SetMomentum((1 + charge*alpha*track->GetMomentum())*(1 + beta)*track->GetMomentum());
}

StrawRapper::~StrawRapper(){
    delete fUserMethods;
}
//}
