#include "StrawCluster.hh"

StrawCluster::StrawCluster(int jc, int jv)
{
  fChamber = jc;
  fView = jv;
}

StrawCluster::~StrawCluster()
{}

void StrawCluster::Clear() {
  fChamber = -1;
  fView = -1;
  fNClusters = 0;
  for (int k=0; k<50; k++) {
    fNHits[k] = 0;
    fIndex[0][k] = -1;
    fIndex[1][k] = -1;
    fIndex[2][k] = -1;
    fX[k] = -99999.;
    fZ[k] = -99999.;
    fT[k] = -99999.;
    fEdge[k] = 0;
    fQuality[k] = 99999.;
  }
}
