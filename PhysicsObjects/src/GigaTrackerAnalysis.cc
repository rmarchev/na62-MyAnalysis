#include "GigaTrackerAnalysis.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"
#include <TProfile.h>
#include <numeric>

using namespace NA62Constants;

GigaTrackerAnalysis::GigaTrackerAnalysis(NA62Analysis::Core::BaseAnalysis *ba) {
  fPar = Parameters::GetInstance();

  // Geometry parameters
  fGTKOffset = 0.;
  fZGTK[0] = 79600.;
  fZGTK[1] = 92800.;
  fZGTK[2] = 102400.;

  // Some pointers
  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);

  // Histogram booking
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt10","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt11","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt12","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt13","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt14","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt15","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt16","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt17","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt18","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt19","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt20","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt21","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt22","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt23","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt24","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt25","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt26","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt27","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt28","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt29","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt30","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt31","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt32","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt33","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt34","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt35","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt36","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt37","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt38","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt39","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_chi2ev","",100,0,100));
  fUserMethods->BookHisto(new TH1F("GTKReco_chi2","",200,0,200));

  // Candidates
  for (int j(0); j<10; j++) fGigaTrackerCandidate[j] = new GigaTrackerCandidate();
  fGigaTrackerGTK1Hit = new int[50];
  fGigaTrackerGTK2Hit = new int[50];
  fGigaTrackerGTK3Hit = new int[50];

  // Projection discriminant
  fReferenceDetector = 0;
  DiscriminantNormalization();
  fReferenceDetector = 1;
  DiscriminantNormalization();
  fReferenceDetector = 0;

  // TW corrections for 2016 (from E. Gamberini 10/2016)
//  ApplyTimeCorrections();

  // Random generator init (pileup study only)
  fRand = new TRandom3();
}

GigaTrackerAnalysis::~GigaTrackerAnalysis()
{ }

void GigaTrackerAnalysis::StartBurst(Int_t year, Int_t runid, Int_t burstid, double *off) {
  fYear = year;
  fBurst = burstid;
  fRun = runid;
  fTriggerOffset = 0.;
  fIsMC = fUserMethods->GetWithMC();
  for (int k=0; k<25; k++) {
    fTOffset[k] = off[k];
    if (fIsMC) fTOffset[k] = -0.023;
  }
//  for (int k=0; k<25; k++) fTOffset[k] = 0;
}

void GigaTrackerAnalysis::Clear() {
  fTimeShift = 0.;
  fNGigaTrackerCandidates = 0;
  fNGigaTrackerGTK1Hits = 0;
  fNGigaTrackerGTK2Hits = 0;
  fNGigaTrackerGTK3Hits = 0;
  for (int j(0); j<10; j++) fGigaTrackerCandidate[j]->Clear();
  for (int k(0); k<50; k++) {
    fGigaTrackerGTK1Hit[k] = -1;
    fGigaTrackerGTK2Hit[k] = -1;
    fGigaTrackerGTK3Hit[k] = -1;
  }
  fIsTriplet = 0;
  fIsDoublet = 0;
  fTCorr = 0;
  fEarlyDecayFlag = 0;
  fTimeRef = 0;
  fNHits = 0;
  for (int kk=0; kk<3; kk++) {
    fKaonX[kk] = 0.;
    fKaonY[kk] = 0.;
  }
}

////////////////////////////////
// ALTERNATIVE RECONSTRUCTION //
////////////////////////////////
void GigaTrackerAnalysis::ApplyTimeCorrections() {

  // Define function for tw correction
  for(int iS(0); iS<3; iS++){
    for(int iC(0); iC<10; iC++){
      for(int iR(0); iR<45; iR++){ fTW[iS][iC][iR] = new TF1("fToT_Corr_2nd","pol2",0,25);
      }
    }
  }

  // Define function parameters
//  ifstream file("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/GTK_correction_tw_1141.dat");
  ifstream file("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/GTK_correction_tw_1181.dat");
  double par0, par1, par2;
  int chip;
  int station;
  int row;
  while(file>>station>>chip>>row>>par0>>par1>>par2) {
    fTW[station][chip][row]->SetParameters(par0, par1, par2);
  }
  file.close();
}

int GigaTrackerAnalysis::ReconstructCandidateNoGTK(TRecoGigaTrackerEvent* event ,TRecoSpectrometerCandidate *thisTrack) {

  // Remove existing candidates
  int nCand = event->GetNCandidates(); // mandatory because RemoveCandidate update NCandidates !
  // for (int iC(0);iC<nCand; iC++) event->RemoveCandidate(0);
  //for (int iC(0);iC<nCand; iC++) event->RemoveCandidate(0);


  TLorentzVector k4mom;
  Double_t pk = 74.9*1000;
  Double_t norm = 1 / (1 + 0.00122*0.00122 + 0.000026*0.000026);
  Double_t px = pk*0.00122*norm;
  Double_t py = pk*0.000026*norm;
  Double_t pz = pk*norm;
  k4mom.SetXYZM(px,py,pz,0.493667);

  TLorentzVector p4mom ;
  Double_t ptrack = 74.9*1000;
  Double_t normtrack = 1 / (1 + thisTrack->GetSlopeXBeforeMagnet()*thisTrack->GetSlopeXBeforeMagnet() + thisTrack->GetSlopeYBeforeMagnet()*thisTrack->GetSlopeYBeforeMagnet());
  Double_t ptrackx = pk*thisTrack->GetSlopeXBeforeMagnet()*normtrack;
  Double_t ptracky = pk*thisTrack->GetSlopeYBeforeMagnet()*normtrack;
  Double_t ptrackz = pk*normtrack;
  p4mom.SetXYZM(ptrackx,ptracky,ptrackz,0.13957018);


  TVector3 kmom = k4mom.Vect();
  TVector3 kpos(0,0,101800);
  TVector3 pmom = p4mom.Vect();
  TVector3 ppos = thisTrack->GetPositionBeforeMagnet();
  Double_t cda = -99999.;
  // TVector3 vtx = fTools->SingleTrackVertex(&kmom,&pmom,&kpos,&ppos,&cda);
  TVector3 vtx = fTools->SingleTrackVertex(kmom,pmom,kpos,ppos,cda);
  //vtx = fTools->BlueFieldCorrection(&pmom,ppos,thisTrack->GetCharge(),vtx.Z());
  //vtx = fTools->BlueFieldCorrection(&kmom,kpos,1,vtx.Z());
  vtx = fTools->SingleTrackVertex(kmom,pmom,kpos,ppos,cda);
  //vtx = fTools->SingleTrackVertex(&kmom,&pmom,&kpos,&ppos,&cda);
  Double_t discrmax = cda>25. ? 0 : 1.;
  fNGigaTrackerCandidates = 1;
  TRecoGigaTrackerCandidate* cand = (TRecoGigaTrackerCandidate*)event->AddCandidate();
  fGigaTrackerCandidate[0]->SetMomentum(74.9);
  fGigaTrackerCandidate[0]->SetSlopeXZ(0.00122);
  fGigaTrackerCandidate[0]->SetSlopeYZ(0.000026);
  fGigaTrackerCandidate[0]->SetTime(0);
  fGigaTrackerCandidate[0]->SetPosition(0.,0.,101801.);
  fGigaTrackerCandidate[0]->SetPosGTK1(0.,0.,79600.);
  fGigaTrackerCandidate[0]->SetPosGTK2(0.,0.,92800.);
  fGigaTrackerCandidate[0]->SetDiscriminant(discrmax);
  fGigaTrackerCandidate[0]->SetType(2);
  fGigaTrackerCandidate[0]->SetIsGigaTrackerCandidate(1);

  //cout << fGigaTrackerCandidate[0]->GetMomentum() << endl;
  return 0;
}


int GigaTrackerAnalysis::ReconstructCandidates(TRecoGigaTrackerEvent *event, double reftime, double tstamp, bool remove) {
  fNHits = 0;
  fTimeRef = reftime;

  // T stamp bin
  int nt = (int)(tstamp/10000000.);
  if (nt>24) nt = 24;

  // Remove existing candidates
  if (remove) {
    int nCand = event->GetNCandidates(); // mandatory because RemoveCandidate update NCandidates !
    for (int iC(0);iC<nCand; iC++) event->RemoveCandidate(0);
  }

  // Check bit overflow
  ULong64_t errorMask = event->GetErrorMask();
  if (((errorMask>>0)&0x1)==1) return 0;

  // Select hits per station
  std::vector<int> idHit;
  int nhits = event->GetNHits();
  int jhit[3] = {0}; // counter of selected hits per stations
  TClonesArray& Hits = (*(event->GetHits()));
  for (int jHit(0);jHit<nhits;jHit++) {
    TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*)Hits[jHit];
    int jS = hit->GetStationNo();
    int jC = hit->GetChipID();
    if (jhit[jS]>=50) return 0; // reject too crowdy events
    double dt = hit->GetTime()+fTOffset[nt]-reftime-fTimeShift;
    if (fabs(fTimeShift)<1) {
      fUserMethods->FillHisto(Form("GTKReco_testdt%d%d",jS+1,jC),dt);
      if (jS==0 && fabs(dt-15.)<10.) fNHits++;
    }
    if (fabs(dt)>fRecoTimingCut) continue;
    idHit.push_back(jHit);
    jhit[jS]++;
  }

  StationOrder so(event);
  std::sort(idHit.begin(),idHit.end(),so);
  int pS0 = jhit[0];
  int pS1 = jhit[0]+jhit[1];

  // Build candidates
  int nRecoCand = 0;
  vector<int>::iterator jh0=idHit.begin();
  while(jh0<next(begin(idHit),pS0)) {
    vector<int>::iterator jh1=idHit.begin();
    advance(jh1,pS0);
    while(jh1<next(begin(idHit),pS1)) {
      vector<int>::iterator jh2=idHit.begin();
      advance(jh2,pS1);
      while(jh2!=idHit.end()) {
        int nid = 1000000*(*jh2)+1000*(*jh1)+(*jh0);
        if (BuildTrack(nid,nt,event,-1)) nRecoCand++;

        // Add treatment for early decays to measure mis-tagging probability
        if (fEarlyDecayFlag) {
          if (jh0==next(begin(idHit),pS0-1) || jh1==next(begin(idHit),pS1-1)) {
            nid = 1000000*(*jh2)+1000*(*jh1)+(*jh0);
            if (jh0==next(begin(idHit),pS0-1)) {
              if (BuildTrack(nid,nt,event,0)) nRecoCand++;
            }
            if (jh1==next(begin(idHit),pS1-1)) {
              if (BuildTrack(nid,nt,event,1)) nRecoCand++;
            }
            if (jh0==next(begin(idHit),pS0-1) && jh1==next(begin(idHit),pS1-1)) {
              if (BuildTrack(nid,nt,event,2)) nRecoCand++;
            }
          }
        }
        // End treatment for early decays

        jh2++;
      }
      jh1++;
    }
    jh0++;
  }

  return nRecoCand;
}

void GigaTrackerAnalysis::CorrectHitTime(TRecoGigaTrackerHit *hit) {
  double hitTimeOld = hit->GetRawTime();
  double tot = hit->GetToT();
  int station = hit->GetStationNo();
  int chip = hit->GetChipID();
  int pixel = hit->GetChipPixelID();
  int row = pixel/40;
  double hitTime = hitTimeOld - fTW[station][chip][row]->Eval(tot);
  hit->SetTime(hitTime-ftOffset[10*station+chip]);
}

Bool_t GigaTrackerAnalysis::BuildTrack(int nid, int nt, TRecoGigaTrackerEvent *event, int addHit) {

  // Basic Geometrical Parameters for the Reconstruction
  double	magnetFieldStrength0 = -1.6678;//Tm
  double	magnetFieldStrength4 = -0.7505;//Tm
//  double	magnetFieldStrength4 = -0.7435;//Tm
  double	magnetZLength1       = 2.5 * 1.e3;
  double	magnetZLength2       = 0.4 * 1.e3;
  double	detectorZPosition    = 0.5 * (79.580 + 102.420) * 1.e3;
  double	magnet1PositionZ     = 82.980 * 1.e3 - 0.5 * magnetZLength1 - detectorZPosition;
  double	magnet2PositionZ     = 86.580 * 1.e3 - 0.5 * magnetZLength1 - detectorZPosition;
  double	magnet5PositionZ     = 102.000 * 1.e3 - 0.5 * magnetZLength2 - detectorZPosition;
  double	stationZLength	     = 200e-6*1e3;
  double        Z[3] = {79600.,92800.,102400.};
  double	station1PositionZ    = Z[0] - 0.5 * stationZLength - detectorZPosition;
  double	station2PositionZ    = Z[1] - 0.5 * stationZLength - detectorZPosition;
  double	station3PositionZ    = Z[2] - 0.5 * stationZLength - detectorZPosition;
  double	station2PositionY    = -60.e-3 * 1.e3;

  // Derived Parameters
  Double_t fClight                = TMath::C();
  Double_t fBLbend	       	  = -1.0*magnetFieldStrength0*magnetZLength1;
  Double_t fDeltaBend		  = magnet2PositionZ - magnet1PositionZ;
  Double_t fBeta		  = 1e-3*fClight * fBLbend * fDeltaBend ;
  Double_t fBLtrim		  = magnetFieldStrength4*magnetZLength2;
  Double_t fDeltaTrim		  = station3PositionZ - magnet5PositionZ;
  Double_t fDelta12		  = station2PositionZ - station1PositionZ;
  Double_t fDelta13		  = station3PositionZ - station1PositionZ;
  Double_t fDelta23		  = station3PositionZ - station2PositionZ;
  Double_t fAlpha		  = fDelta12 / fDelta13;
  Double_t fDeltaZ		  = -station2PositionY;

  // New variable declaration
  Double_t X[3];
  Double_t Xshift[3];
  Double_t Y[3];
  Double_t T[3];

  // Stations alignment
  double newparx[3] = {0.341404,-2.95852,-1.774972};
  double newpary[3] = {1.841573,0.823864,0.853356};
  double newparx_mc[3] = {0.664404,-2.747517,-2.236972};
  double newpary_mc[3] = {1.841573,0.273864,0.153356};



  // Hit ID per station
  Int_t jhit[3] = {nid%1000,(nid%1000000)/1000,nid/1000000};

  // Standard GTK Reconstruction
  if (addHit==-1) {
    TClonesArray& Hits = (*(event->GetHits()));
    TRecoGigaTrackerHit *hit[3];
    for(Int_t iStation=0; iStation < 3 ; iStation++){
      hit[iStation] = (TRecoGigaTrackerHit*)Hits[jhit[iStation]];
      //X[iStation] = !fIsMC ? hit[iStation]->GetRawPosition().X()-newparx[iStation] : hit[iStation]->GetPosition().X();
      X[iStation] = !fIsMC ? hit[iStation]->GetRawPosition().X()-newparx[iStation] : hit[iStation]->GetRawPosition().X()-newparx_mc[iStation];
      Xshift[iStation] = X[iStation];
      //Y[iStation] = !fIsMC ? hit[iStation]->GetRawPosition().Y()-newpary[iStation] : hit[iStation]->GetPosition().Y();
      Y[iStation] = !fIsMC ? hit[iStation]->GetRawPosition().Y()-newpary[iStation] : hit[iStation]->GetRawPosition().Y()-newpary_mc[iStation];
      T[iStation] = hit[iStation]->GetTime()+fTOffset[nt]+fTCorr; //ns
    }
  }

  // Early decay treatment to measure mis-tagging probability
  if (addHit==0) { // add hit in station 1
    X[0] = fKaonX[0]+fRand->Uniform(-0.3,0.3);
    Y[0] = fKaonY[0]+fRand->Uniform(-0.3,0.3);
    T[0] = fRand->Gaus(0.,0.2)+fTimeShift+fTimeRef;
    Xshift[0] = X[0];
    TRecoGigaTrackerHit *hit1 = (TRecoGigaTrackerHit *)event->GetHit(jhit[1]);
    X[1] = !fIsMC ? hit1->GetRawPosition().X()-newparx[1] : hit1->GetPosition().X();
    Y[1] = !fIsMC ? hit1->GetRawPosition().Y()-newpary[1] : hit1->GetPosition().Y();
    T[1] = hit1->GetTime()+fTOffset[nt]+fTCorr; //ns
    Xshift[1] = X[1];
    TRecoGigaTrackerHit *hit2 = (TRecoGigaTrackerHit *)event->GetHit(jhit[2]);
    X[2] = !fIsMC ? hit2->GetRawPosition().X()-newparx[2] : hit2->GetPosition().X();
    Y[2] = !fIsMC ? hit2->GetRawPosition().Y()-newpary[2] : hit2->GetPosition().Y();
    T[2] = hit2->GetTime()+fTOffset[nt]+fTCorr; //ns
    Xshift[2] = X[2];
  }
  if (addHit==1) { // add hit in station 2
    TRecoGigaTrackerHit *hit0 = (TRecoGigaTrackerHit *)event->GetHit(jhit[0]);
    X[0] = !fIsMC ? hit0->GetRawPosition().X()-newparx[0] : hit0->GetPosition().X();
    Y[0] = !fIsMC ? hit0->GetRawPosition().Y()-newpary[0] : hit0->GetPosition().Y();
    T[0] = hit0->GetTime()+fTOffset[nt]+fTCorr; //ns
    Xshift[0] = X[0];
    X[1] = fKaonX[1]+fRand->Uniform(-0.3,0.3);
    Y[1] = fKaonY[1]+fRand->Uniform(-0.3,0.3);
    T[1] = fRand->Gaus(0.,0.2)+fTimeShift+fTimeRef;
    Xshift[1] = X[1];
    TRecoGigaTrackerHit *hit2 = (TRecoGigaTrackerHit *)event->GetHit(jhit[2]);
    X[2] = !fIsMC ? hit2->GetRawPosition().X()-newparx[2] : hit2->GetPosition().X();
    Y[2] = !fIsMC ? hit2->GetRawPosition().Y()-newpary[2] : hit2->GetPosition().Y();
    T[2] = hit2->GetTime()+fTOffset[nt]+fTCorr; //ns
    Xshift[2] = X[2];
  }
  if (addHit==2) { // add hit in station 1 & 2
    X[0] = fKaonX[0]+fRand->Uniform(-0.3,0.3);
    Y[0] = fKaonY[0]+fRand->Uniform(-0.3,0.3);
    T[0] = fRand->Gaus(0.,0.2)+fTimeShift+fTimeRef;
    Xshift[0] = X[0];
    X[1] = fKaonX[1]+fRand->Uniform(-0.3,0.3);
    Y[1] = fKaonY[1]+fRand->Uniform(-0.3,0.3);
    T[1] = fRand->Gaus(0.,0.2)+fTimeShift+fTimeRef;
    Xshift[1] = X[1];
    TRecoGigaTrackerHit *hit2 = (TRecoGigaTrackerHit *)event->GetHit(jhit[2]);
    X[2] = !fIsMC ? hit2->GetRawPosition().X()-newparx[2] : hit2->GetPosition().X();
    Y[2] = !fIsMC ? hit2->GetRawPosition().Y()-newpary[2] : hit2->GetPosition().Y();
    T[2] = hit2->GetTime()+fTOffset[nt]+fTCorr; //ns
    Xshift[2] = X[2];
  }

  // Compute kinematics and time
  Double_t p = fBeta / (Y[0] * (1.0 - fAlpha) - Y[1] + fDeltaZ + (fAlpha * Y[2]));
  Double_t fShiftTrim              = -((1e-3*fClight * fBLtrim) / p) * fDeltaTrim;
  Xshift[2] -= fShiftTrim ;   // Correct the TRIM5 effect
  Double_t dydz = (Y[2] - Y[0]) / fDelta13;
  Double_t dxdz = (X[2] - X[0]) / fDelta13 - (((1e-3*fClight * fBLtrim) / p) * (1. - (fDeltaTrim / fDelta13)));
  Double_t Time = (T[0] + T[1] + T[2]) / 3.0; //ns

  // Track fitting to straight lines for horizontal view (X), with specific offset for GTK3 (trim effect corrected)
  Double_t a,b,rho,chi2X;
  Double_t sigma[3] = {0.0866,0.0866,0.220};
  Double_t z[3]     = {0,fDelta12,fDelta13};
  LinearLeastSquareFit(z,Xshift,3,sigma,a,b,rho,chi2X);

  // Constraints on vertical view (Y)
  Double_t sigmaY12 = 1.42 ;
  Double_t sigmaY23 = 1.20 ;
  Double_t chi2Y = TMath::Power((Y[1] - Y[0])/sigmaY12 , 2.) + TMath::Power((Y[2] - Y[1])/sigmaY23 , 2.) ;

  // Constraints on relative cluster times
  Double_t sigmaT = 0.250;
  Double_t chi2Time = TMath::Power((T[1] - T[0]  )/sigmaT , 2.) + TMath::Power((T[2] - T[1] )/sigmaT , 2.) ;

  // Global Chi2
  Double_t chi2 = (chi2X + chi2Y + chi2Time) ;
  //cout << chi2X << "  " <<chi2Y << "  " <<  endl;
  // Conditions
  double pkaon = 1e-6*p;
  if (pkaon<72000||pkaon>78000) return 0;
  if (dxdz>0.0016) return 0;
  if (dxdz<0.0009) return 0;
  if (dydz>0.0004) return 0;
  if (dydz<-0.0003) return 0;
  if (chi2X>20 && !fIsMC) return 0;
  if (chi2X>30 && fIsMC) return 0;
  if (chi2Y>20) return 0;
  if (chi2Time>30) return 0;

  // Corrections
  if (!fIsMC) {
    if (fRun<7000) {
      dxdz -= -1.43e-06+8.89e-07*X[2]-4.1e-09*X[2]*X[2]-1.8e-10*X[2]*X[2]*X[2];
      dydz -= -1.31e-06-6.37e-07*Y[2]+5.1e-09*Y[2]*Y[2]-4.3e-10*Y[2]*Y[2]*Y[2];
    }
    if (fRun>=7000) {
      dxdz -= -5e-06+8.89e-07*X[2]-4.1e-09*X[2]*X[2]-1.8e-10*X[2]*X[2]*X[2];
      dydz -= 4.19147e-06-7.8111e-07*Y[2];
      pkaon *= 1-0.0013;
    }
  }
////  if (fIsMC) pkaon *= 1+0.007;

  // Fill pararmeters
  TRecoGigaTrackerCandidate* cand = (TRecoGigaTrackerCandidate*)event->AddCandidate();
  Double_t pz = pkaon/TMath::Sqrt(1.+dxdz*dxdz+dydz*dydz);
  cand->SetMomentum(TVector3(pz*dxdz,pz*dydz,pz));
  cand->SetTime(Time-fTimeShift);
  cand->SetTime1(T[0]-fTimeShift);
  cand->SetTime2(T[1]-fTimeShift);
  cand->SetTime3(T[2]-fTimeShift);
  for (int jS(0); jS<3; jS++) cand->SetPosition(jS,TVector3(X[jS],Y[jS],Z[jS]));
  cand->SetChi2X(chi2X);
  cand->SetChi2Y(chi2Y);
  cand->SetChi2Time(chi2Time);
  cand->SetChi2(chi2);
  cand->SetType(123);


  // Test
  if (fabs(fTimeShift)<1.) {
    Double_t chi2ev = Chi2Event(cand);
    fUserMethods->FillHisto("GTKReco_chi2ev",chi2ev);
    fUserMethods->FillHisto("GTKReco_chi2",chi2);
  }

  return 1;
}
void GigaTrackerAnalysis::LinearLeastSquareFit(Double_t *x, Double_t *y, Int_t Nsample, Double_t *sigma, Double_t &a, Double_t &b, Double_t &rho, Double_t &chi2){

    Double_t xmean = TMath::Mean(Nsample,x);
    Double_t ymean = TMath::Mean(Nsample,y);
    Double_t varx = 0.;
    Double_t vary = 0.;
    Double_t covxy = 0.;

    for(Int_t i=0; i < Nsample ; i++){
      varx += (x[i] - xmean) * (x[i] - xmean) ;
      vary += (y[i] - ymean) * (y[i] - ymean) ;
      covxy += (x[i] - xmean) * (y[i] - ymean) ;
    }
    varx = varx / (Double_t)Nsample;
    vary = vary / (Double_t)Nsample;
    covxy = covxy / (Double_t)Nsample;
    b = covxy / varx;
    a = ymean - b * xmean;
    rho = covxy / (TMath::Sqrt(varx) * TMath::Sqrt(vary));

    chi2 = 0;
    for(Int_t i=0; i < Nsample ; i++){
      chi2 += ((y[i] - a - b*x[i])/sigma[i]) * ((y[i] - a - b*x[i])/sigma[i]) ;
    }

    return;
}

//////////////
// Matching //
//////////////
int GigaTrackerAnalysis::TrackCandidateMatching(TRecoGigaTrackerEvent *event, UpstreamParticle *kaon, double timeref, int sC, int nC) { // Three pion only: true matching
  if (!(nC-sC)) return -1;
  vector<int> id(nC-sC);
  iota(begin(id),end(id),sC);

  DeltaTimeCondition to(this,event,timeref);
  vector<int>::iterator igood = partition(id.begin(),id.end(),to);
  int nelements = distance(id.begin(),igood);
  if (!nelements) return -1;

  Chi2TrueCondition co(this,event,kaon);
////  return *min_element(id.begin(),id.end(),co);
  return *min_element(id.begin(),igood,co);
}

int GigaTrackerAnalysis::TrackCandidateMatching(TRecoGigaTrackerEvent *event, TRecoSpectrometerCandidate *strack, Double_t timeref, int sC, int nC) { // One track matching
  if (!(nC-sC)) return -1;

  // Select the best track
  vector<int> id(nC-sC);
  iota(begin(id),end(id),sC);
  Chi2Condition co(this,event,timeref);
  vector<int>::iterator igood = partition(id.begin(),id.end(),co); // Condition on chi2 of the track to suppress fake combinations
  int nelements = distance(id.begin(),igood);
  if (!nelements) return -1;
  DiscriminantCondition dc(this,event,strack,timeref,0);
  sort(id.begin(),igood,dc); // Sort event according to increasing discriminant
  vector<int>::iterator ilast = id.begin();
  advance(ilast,nelements-1);
  int last = *ilast; // Track with largest discriminant best candidate
  if (nelements>2) { // Solve some ambiguities (to be tuned)
    DifferenceCondition nc(this,event,strack,timeref,last);
    vector<int>::iterator istop = partition(id.begin(),ilast+1,nc);
    int ncomb = distance(id.begin(),istop);
    if (ncomb>1) {
      DiscriminantCondition dc2(this,event,strack,timeref,1);
      sort(id.begin(),istop,dc2);
      ilast = id.begin();
      advance(ilast,ncomb-1);
      int ulast2 = *next(ilast,-1);
      double diff2 = DiffDiscr(event,strack,timeref,last,ulast2,1);
      double diff1 = DiffDiscr(event,strack,timeref,last,ulast2,0);
      if ((diff2<0.1 && diff2>=0) || diff1<0.01) return -1;
    }
  }

  // Store the tracks in UpstreamParticle
  if (nelements>10) return -1;

  // Loop on tracks saved in id -> igood and save them
  fNGigaTrackerCandidates = nelements;
  for (int k(0); k<nelements; k++) {
    TRecoGigaTrackerCandidate *cand = (TRecoGigaTrackerCandidate *)event->GetCandidate(id[k]);
    Double_t cda = 9999999.;
    TVector3 vtx = ComputeVertex(cand,strack,&cda);
    Double_t dt = cand->GetTime()-timeref;
    Double_t discriminant = Discriminant(cda,dt);
    fGigaTrackerCandidate[k]->SetMomentum(1e-3*cand->GetMomentum().Mag());
    fGigaTrackerCandidate[k]->SetSlopeXZ(cand->GetMomentum().X()/cand->GetMomentum().Z());
    fGigaTrackerCandidate[k]->SetSlopeYZ(cand->GetMomentum().Y()/cand->GetMomentum().Z());
    fGigaTrackerCandidate[k]->SetTime(cand->GetTime());
    fGigaTrackerCandidate[k]->SetTime1(cand->GetTime1());
    fGigaTrackerCandidate[k]->SetTime2(cand->GetTime2());
    fGigaTrackerCandidate[k]->SetTime3(cand->GetTime3());
    fGigaTrackerCandidate[k]->SetPosition(cand->GetPosition(2));
    fGigaTrackerCandidate[k]->SetPosGTK1(cand->GetPosition(0));
    fGigaTrackerCandidate[k]->SetPosGTK2(cand->GetPosition(1));
    fGigaTrackerCandidate[k]->SetDiscriminant(discriminant);
    fGigaTrackerCandidate[k]->SetVertex(vtx);
    fGigaTrackerCandidate[k]->SetCDA(cda);
    fGigaTrackerCandidate[k]->SetChi2X(cand->GetChi2X());
    fGigaTrackerCandidate[k]->SetType(3);
    fGigaTrackerCandidate[k]->SetIsGigaTrackerCandidate(1);
  }

  // Match and save hits in GTK3
  for (int ist(0); ist<3; ist++) TrackHitMatching(event,strack,timeref,ist);

  // Return the index of the best track
  return last;
}

Double_t GigaTrackerAnalysis::DiffDiscr(TRecoGigaTrackerEvent *event, TRecoSpectrometerCandidate *strack, Double_t timeref, int i, int j, int iD) { // Compute a possible condition to solve the ambiguity
  TRecoGigaTrackerCandidate* c1 = (TRecoGigaTrackerCandidate*)event->GetCandidate(i);
  Double_t cda1 = 9999999.;
  TVector3 vtx1 = ComputeVertex(c1,strack,&cda1);
  Double_t dt1 = c1->GetTime()-timeref;
  Double_t discr1[2];
  Discriminant(cda1,dt1,discr1);
  TRecoGigaTrackerCandidate* c2 = (TRecoGigaTrackerCandidate*)event->GetCandidate(j);
  Double_t cda2 = 9999999.;
  TVector3 vtx2 = ComputeVertex(c2,strack,&cda2);
  Double_t dt2 = c2->GetTime()-timeref;
  Double_t discr2[2];
  Discriminant(cda2,dt2,discr2);
  return fabs(discr1[iD]-discr2[iD]);
}

Double_t GigaTrackerAnalysis::Chi2True(TRecoGigaTrackerCandidate *cand, UpstreamParticle *kaon) { // 3 pion testing purposes using true kaon
  Double_t sdd[5] = {0.3,3e-5,3e-5,2.,2.}; // GeV/c, rad, rad, mm, mm
  Double_t dd[5];
  dd[0] = 0.001*cand->GetMomentum().Mag()-kaon->GetMomentum().P();
  dd[1] = (cand->GetMomentum().X()/cand->GetMomentum().Z())-(kaon->GetMomentum().X()/kaon->GetMomentum().Z());
  dd[2] = (cand->GetMomentum().Y()/cand->GetMomentum().Z())-(kaon->GetMomentum().Y()/kaon->GetMomentum().Z());
  dd[3] = cand->GetPosition(2).X()-kaon->GetPosition().X();
  dd[4] = cand->GetPosition(2).Y()-kaon->GetPosition().Y();
  Double_t chi2true = 0.;
  for (int j(0); j<5; j++) chi2true += dd[j]*dd[j]/(sdd[j]*sdd[j]);
  return chi2true;
}

Double_t GigaTrackerAnalysis::Chi2TrueTest(TRecoGigaTrackerCandidate *cand, TRecoGigaTrackerCandidate *kaon) { // 3 pion testing purposes using true kaon candidate
  Double_t sdd[5] = {0.3,3e-5,3e-5,2.,2.}; // GeV/c, rad, rad, mm, mm
  Double_t dd[5];
  dd[0] = 0.001*cand->GetMomentum().Mag()-0.001*kaon->GetMomentum().Mag();
  dd[1] = (cand->GetMomentum().X()/cand->GetMomentum().Z())-(kaon->GetMomentum().X()/kaon->GetMomentum().Z());
  dd[2] = (cand->GetMomentum().Y()/cand->GetMomentum().Z())-(kaon->GetMomentum().Y()/kaon->GetMomentum().Z());
  dd[3] = cand->GetPosition(2).X()-kaon->GetPosition(2).X();
  dd[4] = cand->GetPosition(2).Y()-kaon->GetPosition(2).Y();
  Double_t chi2true = 0.;
  for (int j(0); j<5; j++) chi2true += dd[j]*dd[j]/(sdd[j]*sdd[j]);
  return chi2true;
}

Double_t GigaTrackerAnalysis::Chi2Test(TRecoGigaTrackerCandidate *cand, UpstreamParticle *kaon) { // 3 pion testing purposes using true kaon candidate
  Double_t sdd[5] = {0.3,3e-5,3e-5,2.,2.}; // GeV/c, rad, rad, mm, mm
  Double_t dd[5];
  dd[0] = 0.001*cand->GetMomentum().Mag()-kaon->GetMomentum().P();
  dd[1] = (cand->GetMomentum().X()/cand->GetMomentum().Z())-(kaon->GetMomentum().X()/kaon->GetMomentum().Z());
  dd[2] = (cand->GetMomentum().Y()/cand->GetMomentum().Z())-(kaon->GetMomentum().Y()/kaon->GetMomentum().Z());
  dd[3] = cand->GetPosition(2).X()-kaon->GetPosition().X();
  dd[4] = cand->GetPosition(2).Y()-kaon->GetPosition().Y();
  Double_t chi2true = 0.;
  for (int j(0); j<5; j++) chi2true += dd[j]*dd[j]/(sdd[j]*sdd[j]);
  return chi2true;
}

Double_t GigaTrackerAnalysis::Chi2Event(TRecoGigaTrackerCandidate *cand) {
  Double_t pcand = 1e-3*cand->GetMomentum().Mag();
  Double_t txcand = cand->GetMomentum().X()/cand->GetMomentum().Z();
  Double_t tycand = cand->GetMomentum().Y()/cand->GetMomentum().Z();
//  return (pcand-74.9)*(pcand-74.9)/(0.9*0.9)+(txcand-0.00122)*(txcand-0.00122)/(1.2e-4*1.2e-4)+(tycand-0.)*(tycand-0.)/(1.0e-4*1.0e-4);
  return (pcand-74.9)*(pcand-74.9)/(0.9*0.9)+(txcand-0.00122)*(txcand-0.00122)/(1.2e-4*1.2e-4)+(tycand-0.000025)*(tycand-0.000025)/(1.0e-4*1.0e-4);
}

TVector3 GigaTrackerAnalysis::ComputeVertex(TRecoGigaTrackerCandidate *cand, TRecoSpectrometerCandidate *strack, Double_t *cda) { // Analytical approach including blue field correction

  // Kaon moemntum and position
  TVector3 p3cand = 1e-03*cand->GetMomentum();
  TVector3 poscand = cand->GetPosition(2);

  // Pion momentum and position
  Double_t partrack[4] = {strack->GetMomentum(),strack->GetSlopeXBeforeMagnet(),strack->GetSlopeYBeforeMagnet(),1e-3*MPI};
  TVector3 p3pion = fTools->Get4Momentum(partrack).Vect();
  TVector3 pospion = strack->GetPositionBeforeMagnet();

  // Vertex: first iteration without momentum correction for blue field
  TVector3 vtx = fTools->SingleTrackVertex(p3cand,p3pion,poscand,pospion,*cda); // First interation: no blue field

  vtx = fTools->BlueFieldCorrection(&p3pion,pospion,strack->GetCharge(),vtx.Z());
  vtx = fTools->BlueFieldCorrection(&p3cand,poscand,1,vtx.Z());

  // Vertex: first iteration without momentum correction for blue field
  //TVector3 vtx = fTools->SingleTrackVertex(&p3cand,&p3pion,&poscand,&pospion,cda); // First interation: no blue field

  // Vertex: second iteration with momentum corrected for blue field
  // vtx = fTools->SingleTrackVertex(&p3cand,&p3pion,&poscand,&pospion,cda);

  vtx = fTools->SingleTrackVertex(p3cand,p3pion,poscand,pospion,*cda);
  //cout << " After ----"<<  "x = " << vtx.X() << "y = " << vtx.Y() << "z = " << vtx.Z() << endl;

  // // Correct incoming and outcoming momentum at origin for blue field
  // vtx = fTools->BlueFieldCorrection(&p3pion,pospion,strack->GetCharge(),vtx.Z());
  // vtx = fTools->BlueFieldCorrection(&p3cand,poscand,1,vtx.Z());

  // // Vertex: second iteration with momentum corrected for blue field
  // vtx = fTools->SingleTrackVertex(&p3cand,&p3pion,&poscand,&pospion,cda);

  return vtx;
}

//TVector3 GigaTrackerAnalysis::ComputeVertex(TVector3 *vkaon, TVector3 *poskaon, TRecoSpectrometerCandidate *strack, TVector3 *vtra) { // Analytical approach including blue field correction
//
//  // Kaon moemntum and position
//  TVector3 p3cand = *vkaon;
//  p3cand = 1e-03*p3cand;
//  TVector3 poscand = *poskaon;
//
//  // Pion momentum and position
//  Double_t partrack[4] = {strack->GetMomentum(),strack->GetSlopeXBeforeMagnet(),strack->GetSlopeYBeforeMagnet(),1e-3*MPI};
//  TVector3 p3pion = fTools->Get4Momentum(partrack).Vect();
//  TVector3 pospion = strack->GetPositionBeforeMagnet();
//
//  // Vertex: first iteration without momentum correction for blue field
//  TVector3 vtx = fTools->SingleTrackVertex(&p3cand,&p3pion,&poscand,&pospion,vtra); // First interation: no blue field
//
//  // Correct incoming and outcoming momentum at origin for blue field
//  vtx = fTools->BlueFieldCorrection(&p3pion,pospion,strack->GetCharge(),vtx.Z());
//  vtx = fTools->BlueFieldCorrection(&p3cand,poscand,1,vtx.Z());
//
//  // Vertex: second iteration with momentum corrected for blue field
//  vtx = fTools->SingleTrackVertex(&p3cand,&p3pion,&poscand,&pospion,vtra);
//
//  return vtx;
//}

//////////////////
// Discriminant //
//////////////////
void GigaTrackerAnalysis::DiscriminantNormalization() {
  double pass_cda = 0.01; // mm
  double pass_dt = 0.001; // ns

  int jD = fReferenceDetector;

  fPDFKaonCDA[jD].clear();
  fIntPDFKaonCDA[jD] = 0;
  fPDFPileCDA[jD].clear();
  fIntPDFPileCDA[jD] = 0;
  for (int jbincda(0); jbincda<6000; jbincda++) { // < 60 mm
    double cda = pass_cda*jbincda+0.5*pass_cda;
    double pdfkc = PDFKaonCDA(cda);
    if (pdfkc<0) pdfkc=0;
    fIntPDFKaonCDA[jD] += pdfkc*pass_cda/0.25;
//    if (fIntPDFKaonCDA[jD]>=1) fIntPDFKaonCDA[jD] = 1;
    fPDFKaonCDA[jD].push_back(fIntPDFKaonCDA[jD]);
    double pdfpc = PDFPileCDA(cda);
    if (pdfpc<0) pdfpc=0;
    fIntPDFPileCDA[jD] += pdfpc*pass_cda/0.25;
//    if (fIntPDFPileCDA[jD]>=1) fIntPDFPileCDA[jD] = 1;
    fPDFPileCDA[jD].push_back(fIntPDFPileCDA[jD]);
  }
  fPDFKaonDT[jD].clear();
  fIntPDFKaonDT[jD] = 0;
  fPDFPileDT[jD].clear();
  fIntPDFPileDT[jD] = 0;
  for (int jbindt(0); jbindt<1000; jbindt++) { // +-1 ns
    double dt = pass_dt*jbindt+0.5*pass_dt;
    fIntPDFKaonDT[jD] += (PDFKaonDT(dt)+PDFKaonDT(-dt))*pass_dt/0.01;
    fPDFKaonDT[jD].push_back(fIntPDFKaonDT[jD]);
    fIntPDFPileDT[jD] += (PDFPileDT(dt)+PDFPileDT(-dt))*pass_dt/0.01;
    fPDFPileDT[jD].push_back(fIntPDFPileDT[jD]);
  }
}

Double_t GigaTrackerAnalysis::Discriminant(Double_t cda_val, Double_t dt_val) {

//  int jD = fReferenceDetector;
  int jD = (fReferenceDetector==0||fReferenceDetector==2||fReferenceDetector==3) ? 0 : 1;

  double discr = 0;
  if (cda_val>59.9 || fabs(dt_val)>0.95) return discr; // range of validity of the normalization

  double pass_cda = 0.01; // mm
  double pass_dt = 0.001;//0.05; // ns
  double pkcda = 0.;
  vector<double>::iterator ikcda = fPDFKaonCDA[jD].begin();
  while(ikcda!=fPDFKaonCDA[jD].end()) {
    double cda = (double)(distance(fPDFKaonCDA[jD].begin(),ikcda)-1)*pass_cda;
    if ((pkcda=EvaluateCondition(cda,cda_val,*ikcda))>=0) break;
    ikcda++;
  }
  pkcda /= fIntPDFKaonCDA[jD];
  double pkdt = 0.;
  vector<double>::iterator ikdt = fPDFKaonDT[jD].begin();
  while(ikdt!=fPDFKaonDT[jD].end()) {
    double dt = (double)(distance(fPDFKaonDT[jD].begin(),ikdt)-1)*pass_dt;
    if ((pkdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ikdt))>=0) break;
    ikdt++;
  }
  pkdt /= fIntPDFKaonDT[jD];
  double ppcda = 0.;
  vector<double>::iterator ipcda = fPDFPileCDA[jD].begin();
  while(ipcda!=fPDFPileCDA[jD].end()) {
    double cda = (double)(distance(fPDFPileCDA[jD].begin(),ipcda)-1)*pass_cda;
    if ((ppcda=EvaluateCondition(cda,cda_val,*ipcda))>=0) break;
    ipcda++;
  }
  ppcda /= fIntPDFPileCDA[jD];
  double ppdt = 0.;
  vector<double>::iterator ipdt = fPDFPileDT[jD].begin();
  while(ipdt!=fPDFPileDT[jD].end()) {
    double dt = (double)(distance(fPDFPileDT[jD].begin(),ipdt)-1)*pass_dt;
    if ((ppdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ipdt))>=0) break;
    ipdt++;
  }
  ppdt /= fIntPDFPileDT[jD];

  // Simple discriminant
  double discr_kaon = (1-pkcda)*(1-pkdt);
  double discr_pile = (1-ppcda)*(1-ppdt);
  discr = discr_kaon;

  return discr;
}

void GigaTrackerAnalysis::Discriminant(Double_t cda_val, Double_t dt_val, double *discr) { // Similar as above, but returning discriminant both in kaon and pileup hypothesis

//  int jD = fReferenceDetector;
  int jD = (fReferenceDetector==0||fReferenceDetector==2||fReferenceDetector==3) ? 0 : 1;

  discr[0] = 0;
  discr[1] = 0;
  if (cda_val>59.9 || fabs(dt_val)>0.95) return;

  double pass_cda = 0.01; // mm
  double pass_dt = 0.001;//0.05; // ns
  double pkcda = 0.;
  vector<double>::iterator ikcda = fPDFKaonCDA[jD].begin();
  while(ikcda!=fPDFKaonCDA[jD].end()) {
    double cda = (double)(distance(fPDFKaonCDA[jD].begin(),ikcda)-1)*pass_cda;
    if ((pkcda=EvaluateCondition(cda,cda_val,*ikcda))>=0) break;
    ikcda++;
  }
  pkcda /= fIntPDFKaonCDA[jD];
  double pkdt = 0.;
  vector<double>::iterator ikdt = fPDFKaonDT[jD].begin();
  while(ikdt!=fPDFKaonDT[jD].end()) {
    double dt = (double)(distance(fPDFKaonDT[jD].begin(),ikdt)-1)*pass_dt;
    if ((pkdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ikdt))>=0) break;
    ikdt++;
  }
  pkdt /= fIntPDFKaonDT[jD];
  double ppcda = 0.;
  vector<double>::iterator ipcda = fPDFPileCDA[jD].begin();
  while(ipcda!=fPDFPileCDA[jD].end()) {
    double cda = (double)(distance(fPDFPileCDA[jD].begin(),ipcda)-1)*pass_cda;
    if ((ppcda=EvaluateCondition(cda,cda_val,*ipcda))>=0) break;
    ipcda++;
  }
  ppcda /= fIntPDFPileCDA[jD];
  double ppdt = 0.;
  vector<double>::iterator ipdt = fPDFPileDT[jD].begin();
  while(ipdt!=fPDFPileDT[jD].end()) {
    double dt = (double)(distance(fPDFPileDT[jD].begin(),ipdt)-1)*pass_dt;
    if ((ppdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ipdt))>=0) break;
    ipdt++;
  }
  ppdt /= fIntPDFPileDT[jD];

  discr[0] = (1-pkcda)*(1-pkdt);
  discr[1] = (1-ppcda)*(1-ppdt);

}

// PDF Projections (from fit on 3 pions: data 6342 filtered on /eos/na62/user/r/ruggierg/2016/filter_6342 with fit in output/analysis_onetrack_gigatracker.C)
Double_t GigaTrackerAnalysis::PDFKaonCDA(Double_t cda) {
  Double_t cdapar[8];
  cdapar[0] = 7.02e-02;
  cdapar[1] = 1.47e+00;
  cdapar[2] = 2.29e-02;
  cdapar[3] = 2.60e+00;
  cdapar[4] = 1.45e-02;
  cdapar[5] = 3.19e-01;
  cdapar[6] = 3.3e-05;
  cdapar[7] =-1.6e-06;
  Double_t cdag1 = cdapar[0]*exp(-0.5*(cda/cdapar[1])*(cda/cdapar[1]));
  Double_t cdag2 = cdapar[2]*exp(-0.5*(cda/cdapar[3])*(cda/cdapar[3]));
  Double_t cdaf1 = cdapar[4]*exp(-cdapar[5]*cda)+cdapar[6]+cdapar[7]*cda;
  if (cda>25.) return 0.;
  return cdag1+cdag2+cdaf1;
}

Double_t GigaTrackerAnalysis::PDFKaonDT(Double_t dt) {
  Double_t dtpar[6];

  // KTAG - GTK association
  if (fReferenceDetector==0 || fReferenceDetector==2 || fReferenceDetector==3) {
    dtpar[0] = 2.4e-02; // "Universal" pdf
    dtpar[1] = 0.;
    dtpar[2] = 1.3e-01;
    dtpar[3] = 3.2e-03;
    dtpar[4] = 0.;
    dtpar[5] = 2.1e-01;
  }

  // CHOD - GTK association
  if (fReferenceDetector==1) { // r1336
    dtpar[0] = 1.5e-02;
    dtpar[1] = 0.;
    dtpar[2] = 1.9e-01;
    dtpar[3] = 2.6e-03;
    dtpar[4] = 0.;
    dtpar[5] = 4.0e-01;
  }

  Double_t dtg1 = dtpar[0]*exp(-0.5*((dt-dtpar[1])/dtpar[2])*((dt-dtpar[1])/dtpar[2]));
  Double_t dtg2 = dtpar[3]*exp(-0.5*((dt-dtpar[4])/dtpar[5])*((dt-dtpar[4])/dtpar[5]));
  return dtg1+dtg2;
}

Double_t GigaTrackerAnalysis::PDFPileCDA(Double_t cda) {
  Double_t cdapar[6]; // "Universal" pdf
  cdapar[0] = 0.;
  cdapar[1] = 1.5e-03;
  cdapar[2] = 1.97e+01;
  cdapar[3] = 9.8e+00;
  cdapar[4] = 1.629e-02;
  cdapar[5] = 1.04e+01;
  Double_t cdap0 = cdapar[0];
  Double_t cdag1 = cdapar[1]*exp(-0.5*((cda-cdapar[2])/cdapar[3])*((cda-cdapar[2])/cdapar[3]));
  Double_t cdag2 = cdapar[4]*exp(-0.5*((cda)/cdapar[5])*((cda)/cdapar[5]));
  return cdap0+cdag1+cdag2;
}

Double_t GigaTrackerAnalysis::PDFPileDT(Double_t dt) {
  Double_t dtpar[4];

  // KTAG - GTK association
  if (fReferenceDetector==0 || fReferenceDetector==2 || fReferenceDetector==3) {
    dtpar[0] =4.6e-03;
    dtpar[1] =0.;
    dtpar[2] =0.;
    dtpar[3] =0.;
  }

  // CHOD - GTK association
  if (fReferenceDetector==1) {
    dtpar[0] = 3.65e-03;
    dtpar[1] = 0.;
    dtpar[2] = 0.;
    dtpar[3] = 0.;
  }

  return dtpar[0]+dtpar[1]*dt+dtpar[2]*dt*dt+dtpar[3]*dt*dt*dt*dt;
}

TLorentzVector GigaTrackerAnalysis::GetMomentum(Double_t pmom, Double_t dxdz, Double_t dydz, Double_t mass) {
  Double_t partrack[4] = {pmom,dxdz,dydz,mass};
  return fTools->Get4Momentum(partrack);
}

///////////////////////
// Hit GTK3 Matching //
///////////////////////
void GigaTrackerAnalysis::TrackHitMatching(TRecoGigaTrackerEvent *event, TRecoSpectrometerCandidate *strack, Double_t timeref, int iST) {
  vector<int> id(event->GetNHits());
  iota(begin(id),end(id),0);

  // All hits of station iST (begin-iend)
  StationCondition so(event,iST);
  auto iend = partition(id.begin(),id.end(),so);

  // All hits of station iST in time (begin-iendinT)
  TimeCondition to(this,event,timeref);
  auto iendinT = partition(id.begin(),iend,to);
  int nhits = distance(id.begin(),iendinT);

  // Sort the hits of station iST in time per distance from the track
  if (iST==2) {
    PositionCondition po(this,event,strack);
    sort(id.begin(),iendinT,po);
  }

  // Save hits
  id.resize(nhits);
  if (iST==0) fNGigaTrackerGTK1Hits = nhits;
  if (iST==1) fNGigaTrackerGTK2Hits = nhits;
  if (iST==2) fNGigaTrackerGTK3Hits = nhits;
  int kk(0);
  for (auto iH : id) {
    if (iST==0) fGigaTrackerGTK1Hit[kk] = iH;
    if (iST==1) fGigaTrackerGTK2Hit[kk] = iH;
    if (iST==2) fGigaTrackerGTK3Hit[kk] = iH;
    kk++;
  }
}

TVector2 GigaTrackerAnalysis::GetGTK3Pos(TRecoSpectrometerCandidate *strack) {
  Double_t partrack[4] = {strack->GetMomentum(),strack->GetSlopeXBeforeMagnet(),strack->GetSlopeYBeforeMagnet(),1e-3*MPI};
  TVector3 p3pion = fTools->Get4Momentum(partrack).Vect();
  TVector3 posGTK3 = fTools->BlueFieldCorrection(&p3pion,strack->GetPositionBeforeMagnet(),strack->GetCharge(),fZGTK[2]);
  return posGTK3.XYvector();
}
