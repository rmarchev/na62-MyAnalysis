#include "LAVAnal.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"
#include "LAVMatching.hh"
//#include "PhotonVetoCandidate.cc"

LAVAnal::LAVAnal(Int_t flag, NA62Analysis::Core::BaseAnalysis *ba) {
  fFlag = flag;
  fPar = Parameters::GetInstance();

  // LAV priority definition
  fLAVPriorityMask[0] = 0; // --SNH--
  fLAVPriorityMask[1] = 4; // LL __ __ __
  fLAVPriorityMask[2] = 5; // __ LH __ __
  fLAVPriorityMask[3] =10; // LL LH __ __
  fLAVPriorityMask[4] = 2; // __ __ TH __
  fLAVPriorityMask[5] = 7; // LL __ TH __
  fLAVPriorityMask[6] =11; // __ LH TH __
  fLAVPriorityMask[7] =13; // LL LH TH __
  fLAVPriorityMask[8] = 1; // __ __ __ TL
  fLAVPriorityMask[9] =12; // LL __ __ TL
  fLAVPriorityMask[10]= 6; // __ LH __ TL
  fLAVPriorityMask[11]=14; // LL LH __ TL
  fLAVPriorityMask[12]= 3; // __ __ TH TL
  fLAVPriorityMask[13]= 8; // LL __ TH TL
  fLAVPriorityMask[14]= 9; // __ LH TH TL
  fLAVPriorityMask[15]=15; // LL LH TH TL

  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);

  // Histo booking
  if (flag==0) { // Track matching
    for (Int_t j=0; j<12; j++) {
      fNameLAVHisto[j].Form("LAVAnalysis_hit_chtime_%d",j);
      fUserMethods->BookHisto(new TH2F(fNameLAVHisto[j].Data(),"",6000,(j+1)*10000-1000,(j+1)*10000+5000,100,-20,20));
    }
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_chtime","",1160,9000,125000,200,-100,100));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_phi","",10,-1,9,360.,0,6.283188));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_lavalltime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_tminlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_emaxlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_tbestlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_ebestlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_bestlavtime","",200,-10,10,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_tbestlavtvse","",1000,0,100000,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_ebestlavtvse","",1000,0,100000,200,-10,10));

    fUserMethods->BookHisto(new TH1I(Form("LAVAnalysis_nhits"),"",80,0,80));
    fUserMethods->BookHisto(new TH2F(Form("LAVAnalysis_nhits_vs_lavid"),"",13,0,13,50,0,50));
    for(int i = 1;i<=12;i++){

      //fUserMethods->BookHisto(new TH2F(Form("LAVAnalysis_lav%d_nclose",i),"",13,0,13,720.,-6.283188,6.283188));
      //fUserMethods->BookHisto(new TH2F(Form("LAVAnalysis_lav%d_dphi",i),"",13,0,13,720.,-6.283188,6.283188));
      fUserMethods->BookHisto(new TH2F(Form("LAVAnalysis_lav%d_dphi_vs_dtref",i),"",720.,-6.283188,6.283188,1000,-100,100));
      fUserMethods->BookHisto(new TH2F(Form("LAVAnalysis_lav%d_dphi_vs_dt",i),"",720.,-6.283188,6.283188,1000,-100,100));
      //fUserMethods->BookHisto(new TH2F(Form("LAVAnalysis_lav%d_dt",i),"",13,0,13,200,-10,10));
    }
  }
  if (flag==1) { // 2 photon analysis
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammachtime","",1160,9000,125000,200,-100,100));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammalavalltime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammatminlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammaemaxlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammatbestlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammaebestlavtime","",13,0,13,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammabestlavtime","",200,-10,10,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammatbestlavtvse","",1000,0,100000,200,-10,10));
    fUserMethods->BookHisto(new TH2F("LAVAnalysis_hit_2gammaebestlavtvse","",1000,0,100000,200,-10,10));
  }

  // Photon candidates
  for (Int_t kk=0; kk<80; kk++) fPhotonCandidate[kk] = new PhotonVetoCandidate();
}


void LAVAnal::StartBurst(Int_t runid, Int_t burstid) {
  //void LAVAnal::StartBurst(Int_t runid) {
  TString badchannelname;
  badchannelname.Form("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/run%d/lav_badchannel.dat",runid);
  cout << badchannelname.Data() << endl;
  fPar->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/run2015/database/lav_t0.dat");
  fPar->LoadParameters(badchannelname.Data());
  fPar->StoreLAVParameters();

  // burst to burst dependent noisy channels
  fLAVBadChannel = (Bool_t *)fPar->GetLAVBadChannel();

  // channel by channel T0
  fLAVChannelT0 = (Double_t *)fPar->GetLAVChannelT0();
}

void LAVAnal::Clear() {
  for (Int_t kk=0; kk<80; kk++) fPhotonCandidate[kk]->Clear();
}

Int_t LAVAnal::MakeCandidate(Double_t reftime, TRecoLAVEvent* event) {

  TClonesArray& Hits = (*(event->GetHits()));

  // At least a good hit not noisy within 5 ns from the reference time
  Int_t bestLAVHitType = 0;
  for (Int_t jHit=0; jHit<event->GetNHits(); jHit++) {
    TRecoLAVHit *hit = (TRecoLAVHit*)Hits[jHit];
    Int_t chid = hit->GetChannelID();
    if (fLAVBadChannel[chid]) continue; // skip bad channels from database
    Int_t edge = hit->GetEdgeMask();
    Double_t dtime = hit->GetTime()-reftime;
    fUserMethods->FillHisto((fFlag==0?"LAVAnalysis_hit_chtime":"LAVAnalysis_hit_2gammachtime"),chid,dtime);
    ////    if (fFlag==0) fUserMethods->FillHisto(fNameLAVHisto[hit->GetLAVID()-1].Data(),chid,dtime);
    if (fLAVPriorityMask[edge]>bestLAVHitType && fabs(dtime)<5) bestLAVHitType = fLAVPriorityMask[edge];
  }
  if (bestLAVHitType<9) return 0; // no lav hits to consider if no good in time hit is present in not noisy channels

  // Select the best hit per block:
  // at least a hit with mask >=9 within +-5 ns is present because of the previous condition.
  // Among these "good" hits we look for the hit closest in time to the reference in each block.
  Int_t bestHitID[3000];
  Double_t bestHitTime[3000];
  for (Int_t jj=0; jj<3000; jj++) {
    bestHitID[jj] = -1;
    bestHitTime[jj] = 9999999.;
  }
  for (Int_t jHit=0; jHit<event->GetNHits(); jHit++) {
    TRecoLAVHit *hit = (TRecoLAVHit*)Hits[jHit];
    Int_t chid = hit->GetChannelID();
    if (fLAVBadChannel[chid]) continue; // skip bad channels
    Int_t edge = hit->GetEdgeMask();
    Double_t dtime = hit->GetTime()-reftime;
    if (fLAVPriorityMask[edge]>=9) {
      if (fabs(dtime)<fabs(bestHitTime[hit->GetPackedChannelID()])) {
        if (fFlag==0) {
          fUserMethods->FillHisto(fNameLAVHisto[hit->GetLAVID()-1].Data(),chid,dtime);
          if ((hit->GetLAVID()-1)<=8) {
            Double_t phi = GetPhi(hit);
            fUserMethods->FillHisto("LAVAnalysis_hit_phi",hit->GetLAVID()-1,phi);
          }
        }
        bestHitTime[hit->GetPackedChannelID()] = dtime;
        bestHitID[hit->GetPackedChannelID()] = jHit;
      }
    }
  }

  // Loop over all the blocks and select the good hit closest in time to the ref time per station
  Int_t nlav = 0;
  Double_t mintime[12];
  Int_t minlavid[12];
  Double_t maxenergy[12];
  Double_t emaxtime[12];
  Int_t emaxlavid[12];
  for (Int_t jj=0; jj<12; jj++) {
    mintime[jj] = 9999999.;
    minlavid[jj] = -1;
    maxenergy[jj] = 0;
    emaxtime[jj] = 9999999.;
    emaxlavid[jj] = -1;
  }
  Double_t totenergy = 0;
  for (Int_t jBlock=0; jBlock<3000; jBlock++) {
    if (bestHitID[jBlock]==-1) continue; // blocks without hits
    TRecoLAVHit *hit = (TRecoLAVHit*)Hits[bestHitID[jBlock]];
    if (fLAVBadChannel[hit->GetChannelID()]) continue; // skip bad channels
    if (fLAVPriorityMask[hit->GetEdgeMask()]<9) continue; // skip bad masks
    Int_t lavid = hit->GetLAVID();
    //    if(lavid == 1) continue;
    fUserMethods->FillHisto((fFlag==0?"LAVAnalysis_hit_lavalltime":"LAVAnalysis_hit_2gammalavalltime"),lavid,bestHitTime[jBlock]);
    totenergy += hit->GetEnergy();
    if (hit->GetEnergy()>maxenergy[lavid-1]) {
      maxenergy[lavid-1] = hit->GetEnergy();
      emaxtime[lavid-1] = bestHitTime[jBlock]; // best hit time of the block with the hit with maximum energy
      emaxlavid[lavid-1] = lavid;
    }
    if (fabs(bestHitTime[jBlock])<fabs(mintime[lavid-1])) {
      mintime[lavid-1] = bestHitTime[jBlock]; // best hit time of the block with the hit with best time
      minlavid[lavid-1] = lavid;
    }
    if (nlav<80) {
      fPhotonCandidate[nlav]->SetTime(bestHitTime[jBlock]+reftime);
      fPhotonCandidate[nlav]->SetID(bestHitID[jBlock]);
      fPhotonCandidate[nlav]->SetLAVID(lavid);
    }
    nlav++;
  }

  if (!nlav) return 0;

  // Find the LAV block closest in time to the ref time
  Double_t globalmintime = 999999.;
  Int_t globalminlavid = -1;
  for (Int_t k=0; k<12; k++) {
    if (minlavid[k]==-1) continue;
    fUserMethods->FillHisto((fFlag==0?"LAVAnalysis_hit_tminlavtime":"LAVAnalysis_hit_2gammatminlavtime"),minlavid[k],mintime[k]);
    if (fabs(mintime[k])<fabs(globalmintime)) {
      globalmintime = mintime[k];
      globalminlavid = minlavid[k];
    }
  }
  fUserMethods->FillHisto((fFlag==0?"LAVAnalysis_hit_tbestlavtime":"LAVAnalysis_hit_2gammatbestlavtime"),globalminlavid,globalmintime);

  // Find the LAV block with maximum energy
  Double_t globalmaxenergy = 0.;
  Double_t globalmaxtime = 999999.;
  Int_t globalmaxlavid = -1;
  for (Int_t k=0; k<12; k++) {
    if (emaxlavid[k]==-1) continue;
    fUserMethods->FillHisto((fFlag==0?"LAVAnalysis_hit_emaxlavtime":"LAVAnalysis_hit_2gammaemaxlavtime"),emaxlavid[k],emaxtime[k]);
    if (maxenergy[k]>globalmaxenergy) {
      globalmaxenergy = maxenergy[k];
      globalmaxtime = emaxtime[k];
      globalmaxlavid = emaxlavid[k];
    }
  }
  fUserMethods->FillHisto((fFlag==0?"LAVAnalysis_hit_ebestlavtime":"LAVAnalysis_hit_2gammaebestlavtime"),globalmaxlavid,globalmaxtime);

  // Final time requirement
  if (fFlag==0) {

    fUserMethods->FillHisto("LAVAnalysis_hit_bestlavtime",globalmintime,globalmaxtime);
    fUserMethods->FillHisto("LAVAnalysis_hit_ebestlavtvse",totenergy,globalmaxtime);
    fUserMethods->FillHisto("LAVAnalysis_hit_tbestlavtvse",totenergy,globalmintime);
  }
  if (fFlag==1) {
    fUserMethods->FillHisto("LAVAnalysis_hit_2gammabestlavtime",globalmintime,globalmaxtime);
    fUserMethods->FillHisto("LAVAnalysis_hit_2gammaebestlavtvse",totenergy,globalmaxtime);
    fUserMethods->FillHisto("LAVAnalysis_hit_2gammatbestlavtvse",totenergy,globalmintime);
  }

  if (fabs(globalmintime)<3) return nlav;

  return 0;
}

Int_t LAVAnal::TrackCompatibility(Double_t reftime,TRecoLAVEvent* event) {

  TClonesArray& Hits = (*(event->GetHits()));

  Int_t nclosehits[12];
  for(int i = 0;i<12;i++){
    nclosehits[i]=0;
  }


  Int_t nintime=0;
  // Select the best hit per block:
  // at least a hit with mask >=9 within +-5 ns is present because of the previous condition.
  // Among these "good" hits we look for the hit closest in time to the reference in each block.
  for (Int_t ipvcand=0; ipvcand<80; ipvcand++) {

    int iHit = fPhotonCandidate[ipvcand]->GetID();
    if(iHit < 0) continue;
    TRecoLAVHit *hit = (TRecoLAVHit*)Hits[iHit];
    nintime++;
    Double_t time  = hit->GetTime();
    Double_t dtref = time - reftime;
    Int_t lavid = hit->GetLAVID();
    Double_t phi = GetPhi(hit);

    for (Int_t ipvcand1=0; ipvcand1<80; ipvcand1++) {
      int jHit = fPhotonCandidate[ipvcand1]->GetID();
      if(jHit < 0) continue;
      if(jHit==iHit) continue;

      TRecoLAVHit *hit1 = (TRecoLAVHit*)Hits[jHit];

      Double_t dtime = hit1->GetTime()-time;
      Int_t lavid1 = hit1->GetLAVID();
      Double_t phi1 = GetPhi(hit1);

      Double_t dphi = phi-phi1;
      Int_t dlavid = lavid1-lavid;

      if(dlavid==0){

        //fUserMethods->FillHisto(Form("LAVAnalysis_lav%d_dphi",lavid),lavid,dphi);
        ///fUserMethods->FillHisto(Form("LAVAnalysis_lav%d_dt",lavid),lavid,dtime);
        fUserMethods->FillHisto(Form("LAVAnalysis_lav%d_dphi_vs_dt",lavid),dphi,dtime);

        if(fabs(dtime) < 3){

          fUserMethods->FillHisto(Form("LAVAnalysis_lav%d_dphi_vs_dtref",lavid),dphi,dtref);

          if(fabs(dphi)<0.1)
            if(fabs(dtref) < 3){
              nclosehits[lavid-1]++;
            }
        }
      }

    }

  }

  bool isTrack = false;
  for(int j=1;j<=12;j++){

    fUserMethods->FillHisto("LAVAnalysis_nhits_vs_lavid",j,nclosehits[j-1]);

    if(nclosehits[j-1] > 3) isTrack=true;

  }
  fUserMethods->FillHisto("LAVAnalysis_nhits",nintime);

  if(isTrack) return 1;

  return 0;
}

Double_t LAVAnal::GetPhi(TRecoLAVHit *hit) {
  Double_t deg = 0.0174533;
  Int_t b  = hit->GetBlockID();
  Int_t BB = hit->GetBananaID();
  Int_t L  = hit->GetLayerID();
  Int_t SS = hit->GetLAVID()-1;
  Int_t BID = -1;
  Int_t nB = -1;
  if (SS<5) {
    BID = 0;
    nB = 8;
  }
  if (SS>=5&&SS<8) {
    BID = 1;
    nB = 12;
  }
  if (SS>=8&&SS<11) {
    BID = 2;
    nB = 15;
  }
  if (SS==11) {
    BID = 3;
    nB = 16;
  }
  double phiBL = 0.;
  if (SS<5) phiBL = -2.25*deg;
  if (SS>=5&&SS<11) phiBL = -1.50*deg;
  if (SS==11) phiBL = -1.40625*deg;
  Double_t phiBS = 0.;
  if (BID == 0) phiBS = 45.*deg;
  if (BID == 1) phiBS = 30.*deg;
  Double_t Phi = 0 + 99.0*deg + L*phiBL + BB*360.*deg/((double)nB) - 0.5*phiBS + (b+0.5)*phiBS/4.;
  return Phi;
}
bool LAVAnal::IsPhotonsInLAV(int nphotons, double ReferenceTime, LAVMatching* pLAVMatching, TRecoLAVEvent* fLAVEvent) {
  bool LAVMatched = false;
  bool LAVMatchedLAV12 = false;
  bool LAVMatchedLAV1 = false;
  bool upstreamLAVs = false;
  bool LAVGamma = false;
  int nLAVHits[12];
  double fLAVMinDPhi = 1.04720;

  TClonesArray& hitArray = (*(fLAVEvent->GetHits()));
  for (Int_t i=0; i<12; i++) nLAVHits[i]=0;
  LAVMatched = pLAVMatching->LAVHasTimeMatching(fLAVEvent);
  if (LAVMatched){
    for (Int_t i=0; i< pLAVMatching->GetNumberOfMatchedBlocks(); i++) {
      TRecoLAVHit* hit = (TRecoLAVHit*) hitArray[pLAVMatching->GetIndexOfMatchedBlocks()[i]];
      if (hit->GetLAVID() == 12) LAVMatchedLAV12 = true;
      if (hit->GetLAVID() == 1) LAVMatchedLAV1 = true;
      nLAVHits[hit->GetLAVID()-1]++;
    }
  }
  for (Int_t i=0; i<11; i++) {
      if (nLAVHits[i] > 1) upstreamLAVs = true;
  }
  Int_t nLAVClus = 0;
  Int_t iLAVClus[100]={0};
  TClonesArray& clusArray = (* (fLAVEvent->GetCandidates()));
  for (Int_t i=0; i< fLAVEvent->GetNCandidates(); i++){
    TRecoLAVCandidate* lavCandi = (TRecoLAVCandidate*) clusArray[i];
    if (lavCandi->GetAlgorithm() != 1) continue;
    Double_t dt = lavCandi->GetTime()  - ReferenceTime;
    if (TMath::Abs(dt) > 3) continue;
    if (nLAVClus<100) iLAVClus[nLAVClus] = i;
    nLAVClus++;
  }
  //if(nLAVClus==2)
      //cout << "new event -----------------" << endl;

    if (nLAVClus == nphotons) {
    if (nphotons==2) {
      double dphi = ((TRecoLAVCandidate*) fLAVEvent->GetCandidate(iLAVClus[0]))->GetPosition().Phi() - ((TRecoLAVCandidate*) fLAVEvent->GetCandidate(iLAVClus[1]))->GetPosition().Phi();
      //cout << "dphi = " << dphi << " Min dphi"<< fLAVMinDPhi << endl;
      if (dphi<-TMath::Pi()) dphi += TMath::Pi()*2;
      else if (dphi>TMath::Pi()) dphi -= TMath::Pi()*2;
      if (TMath::Abs(dphi) > fLAVMinDPhi) LAVGamma = true;
    } else LAVGamma = true;
    if (LAVMatchedLAV12) LAVGamma = false;
    if (LAVMatchedLAV1) LAVGamma = false;
  }

    //if(nLAVClus==2){
    //
    //    for (Int_t i=0; i<12; i++) {
    //        if (nLAVHits[i] > 0) {
    //            cout << "LAV" << i+1 <<" hit with " << nLAVHits[i] << " hits" << endl;
    //        }
    //    }
    //
    //    cout << LAVMatchedLAV1 << LAVMatchedLAV12 << LAVGamma << "nphotons = " << nLAVClus  << endl;
    //}

  return LAVGamma;
}
