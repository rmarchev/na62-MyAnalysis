#include "SAVAnal.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"

SAVAnal::SAVAnal(NA62Analysis::Core::BaseAnalysis *ba) {
  // fPar = Parameters::GetInstance();
  fPar = new Parameters();

  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);

  // Histo booking
  fUserMethods->BookHisto(new TH2F("SACAnalysis_CREAM_hit_chtime","",10,0,10,400,-50,50));
  fUserMethods->BookHisto(new TH2F("IRCAnalysis_CREAM_hit_chtime","",10,0,10,400,-50,50));
  fUserMethods->BookHisto(new TH2F("SACAnalysis_CREAM_mintime","",100,0,50000,400,-50,50));
  fUserMethods->BookHisto(new TH2F("IRCAnalysis_CREAM_mintime","",100,0,50000,400,-50,50));

  // Photon candidates
  for (Int_t kk=0; kk<20; kk++) fPhotonCandidate[kk] = new PhotonVetoCandidate();
}


void SAVAnal::StartBurst(Int_t year, Int_t runid) {
  if (year==2015) fPar->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/sav_t0_2015.dat");
  if (year==2016) {
    TString t0name;
    t0name.Form("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/run%d/sav_t0_2016.dat",runid);
    fPar->LoadParameters(t0name.Data());
    cout << t0name.Data() << endl;
  }
  fPar->StoreSAVParameters();
  fSAVChannelT0 = fPar->GetSAVChannelT0();
  if (year==2015) fTimeCut = 15;
  if (year==2016) fTimeCut = 7;
}

void SAVAnal::Clear() {
  for (Int_t kk=0; kk<20; kk++) fPhotonCandidate[kk]->Clear();
}

Int_t SAVAnal::MakeCandidate(Double_t reftime, TRecoSAVEvent* event) {

  if (!event->GetNHits()) return 0;

  TClonesArray& Hits = (*(event->GetHits()));
  Double_t mintime_irc = 999999.;
  Double_t mintime_sac = 999999.;
  Double_t minenergy_irc = 0.;
  Double_t minenergy_sac = 0.;
  Bool_t ismin_sac = 0;
  Bool_t ismin_irc = 0;
  for (Int_t jHit=0; jHit<event->GetNHits(); jHit++) {
    TRecoSAVHit *hit = (TRecoSAVHit*)Hits[jHit];
    Int_t detectorID = hit->GetDetector();
    Int_t chid = hit->GetChannelDetector()-1;
    //Double_t dtime = hit->GetTime()-reftime-fSAVChannelT0[hit->GetChannelID()-1];
    Double_t dtime = hit->GetTime()-reftime;
    if (detectorID==0) {
      fUserMethods->FillHisto("SACAnalysis_CREAM_hit_chtime",chid,dtime);
      if (fabs(dtime)<fabs(mintime_sac)) {
        mintime_sac = dtime;
        minenergy_sac = hit->GetEnergy();
        ismin_sac = 1;
      }
    }
    if (detectorID==1) {
      fUserMethods->FillHisto("IRCAnalysis_CREAM_hit_chtime",chid,dtime);
      if (fabs(dtime)<fabs(mintime_irc)) {
        mintime_irc = dtime;
        minenergy_irc = hit->GetEnergy();
        ismin_irc = 1;
      }
    }
  }

  Bool_t is_hit_sac = 0;
  Bool_t is_hit_irc = 0;
  if (ismin_sac) {
    fUserMethods->FillHisto("SACAnalysis_CREAM_mintime",minenergy_sac,mintime_sac);
    if (minenergy_sac>1000 && fabs(mintime_sac)<fTimeCut) is_hit_sac = 1;
  }
  if (ismin_irc) {
    fUserMethods->FillHisto("IRCAnalysis_CREAM_mintime",minenergy_irc,mintime_irc);
    if (minenergy_irc>1000 && fabs(mintime_irc)<fTimeCut) is_hit_irc = 1;
  }
  if (is_hit_sac && !is_hit_irc) return 1;
  if (!is_hit_sac && is_hit_irc) return 2;
  if (is_hit_sac && is_hit_irc) return 3;

  return 0;
}
