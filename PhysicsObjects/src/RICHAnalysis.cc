#include "RICHAnalysis.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"
#include "RICHParameters.hh"
#include "RICHImprovedRing.hh"

RICHAnalysis::RICHAnalysis(int flag, NA62Analysis::Core::BaseAnalysis *ba) {
  fPar = Parameters::GetInstance();

  fTools = AnalysisTools::GetInstance();

  fUserMethods = new UserMethods(ba);
  if (flag==0) {
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_xy","",1000,-500,500,1000,-500,500));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_modvst","",1600,-100,100,400,0,400));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_modvstchod","",400,-20,20,400,0,200));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_dtimevschi2rich","",400,0,100,1600,-100,100));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_distvschi2rich","",400,0,100,200,0,400));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_minxy","",1000,-500,500,1000,-500,500));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_minmodvst","",1600,-100,100,400,0,400));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_minmodvstchod","",400,-20,20,400,0,200));
    fUserMethods->BookHisto(new TH2F("RICHAnalysis_singlering_radvsp","",200,0.,100.,300,0,300));

  }
  fRICHMultiCandidate = new RICHCandidate();
  fRICHSingleCandidate = new RICHCandidate();
  fImprovedRing = new RICHImprovedRing();
}

RICHAnalysis::~RICHAnalysis()
{ }

void RICHAnalysis::Clear() {
  fRICHMultiCandidate->Clear();
  fRICHSingleCandidate->Clear();
}

void RICHAnalysis::StartBurst(Int_t year, Int_t nrun, time_t tburst) {
  fYear = year;
  fIsMC = fUserMethods->GetWithMC();
  if (fYear==2015) {
    fPar->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/richmirroroff_2015.dat");
    fPar->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/richmirrorpos_2015.dat");
  }
  if (fYear==2016) {
    fPar->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/richmirroroff_2016.dat");
    fPar->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/richmirrorpos_2016.dat");
  }
  fPar->StoreRICHParameters();
  fDeltaMisa = (Double_t **)fPar->GetRICHDeltaMisa();
  fMirrorPos = (Double_t ***)fPar->GetRICHPos();


  // Alignment
  TString line;
  TString fileAlignName = "/afs/cern.ch/na62/offline/metadata/RICHMirrorAlignment.txt";
  ifstream fileAlign(fileAlignName.Data());
  cout << "---> Reading" << fileAlignName.Data() << endl;
  while (line.ReadLine(fileAlign)) {
    if (line.BeginsWith("#")) continue;
    TObjArray * l = line.Tokenize(" ");
    int firstRun = ((TObjString*)(l->At(0)))->GetString().Atoi();
    int lastRun =  ((TObjString*)(l->At(1)))->GetString().Atoi();
    delete l;
    if (nrun<firstRun || nrun>lastRun) { // Not this entry
      line.ReadLine(fileAlign);
      line.ReadLine(fileAlign);
      line.ReadLine(fileAlign);
    } else { // Here we really read!
      line.ReadLine(fileAlign);
      TObjArray * l = line.Tokenize(" ");
      fMirrorAlign[0] = ((TObjString*)(l->At(0)))->GetString().Atof();
      fMirrorAlign[1] = ((TObjString*)(l->At(1)))->GetString().Atof();
      fMirrorAlign[2] = ((TObjString*)(l->At(2)))->GetString().Atof();
      fMirrorAlign[3] = ((TObjString*)(l->At(3)))->GetString().Atof();
      delete l;
      line.ReadLine(fileAlign);
      TObjArray *l2 = line.Tokenize(" ");
      for (int k=0; k<24; k++) {
        fMirrorAlign[4+k] = ((TObjString*)(l2->At(k)))->GetString().Atof();
      }
      delete l2;
      line.ReadLine(fileAlign);
      TObjArray *l3 = line.Tokenize(" ");
      for (int k=0; k<24; k++) {
        fMirrorAlign[28+k] = ((TObjString*)(l3->At(k)))->GetString().Atof();
      }
      delete l3;
    }
  }
  fileAlign.close();


  // Initialize mirror positions using standard database
  for (Int_t i=0; i<20; i++) { // sequential numbering used in this class
    fMirrorNumber[i] = RICHParameters::GetInstance()->GetMirrorNumber(i);
    fMirrorPosition[i][0] = RICHParameters::GetInstance()->GetMirrorCentreX(fMirrorNumber[i]);
    fMirrorPosition[i][1] = RICHParameters::GetInstance()->GetMirrorCentreY(fMirrorNumber[i]);
  }
  fMirrorPosition[18][0]=fMirrorPosition[1][0]; // x of Mirror 17
  fMirrorPosition[18][1]=(fMirrorPosition[7][1]+fMirrorPosition[8][1])/2.;
  fMirrorPosition[19][0]=fMirrorPosition[16][0]; // x of Mirror 22
  fMirrorPosition[19][1]=(fMirrorPosition[9][1]+fMirrorPosition[10][1])/2.;

  // Other parameters
  fFocalLength= 17020.;
  fElectronRingRadius = 190.0;
  fElectronRingNhits = 14.0;
  if (!fIsMC) {
    fElectronRingRadius = RICHParameters::GetInstance()->GetElectronRingRadius(nrun,tburst);
    fElectronRingNhits = RICHParameters::GetInstance()->GetElectronRingNHits(nrun,tburst);
  }
  fRefIndex = 1.0/(cos(atan(fElectronRingRadius/fFocalLength)));

  // ******
  fImprovedRing->SetYear(fYear);
  fImprovedRing->SetIsMC(fIsMC);
  fIsFilter = fUserMethods->GetTree("Reco")->FindBranch("FilterWord") ? true : false;


  // Time offset
  //if (!fIsMC) fTOffset = nrun>=6474 ? 0 : -0.07;
  if(fIsMC) fTOffset = 0.34;
  if(fIsMC && fUserMethods->GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.2")) fTOffset = 0;


}



Int_t RICHAnalysis::TrackSingleRingMatching(Double_t chodTime, TRecoRICHEvent* event, TRecoSpectrometerCandidate *thisTrack) {
  fRICHEvent = event;

  // Project the track onto the focal plane
  TVector3 posTrackAtMirror = fTools->GetPositionAtZ(thisTrack,236873.);
  Int_t mirrorid = MirrorSurface(posTrackAtMirror.X(),posTrackAtMirror.Y()); // edge effect on mirrors (default: no) ?
  TVector3 deltaVers = (posTrackAtMirror-TVector3(0.,0.,202873.)).Unit();
  Double_t partrack[4] = {thisTrack->GetMomentum(),thisTrack->GetSlopeXAfterMagnet(),thisTrack->GetSlopeYAfterMagnet(),0.13957018};
  TVector3 versorTrack = fTools->Get4Momentum(partrack).Vect().Unit();
  MirrorVector(&versorTrack,&deltaVers);
  TVector3 pmtPos = posTrackAtMirror+(-fFocalLength/versorTrack.Z())*versorTrack;
  if (mirrorid==99) return -1; // No ring candidate if track is not hitting a mirror (no protection against edge effect)

  // Associate tracks with a good ring
  Double_t ttime = thisTrack->GetTime();
  Double_t mindist = 9999999.;
  Double_t mintime = 9999999.;
  Double_t mintimechod = 9999999.;
  Int_t minidring = -1;
  Double_t minchi2 = 9999999.;
  TVector2 mindeltapos(-9999999.,-9999999.);
  TVector2 minrcenter(-9999999.,-9999999.);
  Double_t minradius = 0.;
  Double_t minringchi2 = 99999999.;
  Double_t minringnhits = 99999999.;

  // Loop on single rings
  for (Int_t jring=0; jring<fRICHEvent->GetNTimeCandidates(); jring++) {
    TRecoRICHCandidate *ring = (TRecoRICHCandidate *)fRICHEvent->GetTimeCandidate(jring);
    Double_t newring[6];
    bool goodFit = fImprovedRing->Chi2Fit(fRICHEvent,ring,pmtPos,mirrorid,fMirrorAlign,newring);
    if (!goodFit) continue;
    TVector2 ringcenter(newring[0],newring[1]);
    TVector2 deltapos(ringcenter.X()-pmtPos.X(),ringcenter.Y()-pmtPos.Y());
    Double_t dist = deltapos.Mod();
    double dtime = newring[4]-ttime+fTOffset;
    double dtimechod = newring[4]-chodTime+fTOffset;

    fUserMethods->FillHisto("RICHAnalysis_singlering_xy",deltapos.X(),deltapos.Y());
    fUserMethods->FillHisto("RICHAnalysis_singlering_modvst",dtime,dist);
    fUserMethods->FillHisto("RICHAnalysis_singlering_modvstchod",dtimechod,dist);

    Double_t chi2rich = 0.;
    if (chodTime<9999.) chi2rich = dtimechod*dtimechod/(2*0.3*0.3)+deltapos.X()*deltapos.X()/(3.*3.)+deltapos.Y()*deltapos.Y()/(3.*3.);
    else chi2rich = dtime*dtime/(2*6*6)+deltapos.X()*deltapos.X()/(3.*3.)+deltapos.Y()*deltapos.Y()/(3.*3.);
    fUserMethods->FillHisto("RICHAnalysis_singlering_dtimevschi2rich",chi2rich,dtime);
    fUserMethods->FillHisto("RICHAnalysis_singlering_distvschi2rich",chi2rich,dist);
    if (chi2rich<minchi2) {
      minchi2 = chi2rich;
      mindist = dist;
      mintime = dtime;
      mintimechod = dtimechod;
      mindeltapos = deltapos;
      minrcenter = ringcenter;
      minradius = newring[2];
      minringchi2 = newring[3];
      minidring = jring;
      minringnhits = newring[5];
    }
  }
  if (minidring>-1) {
    fUserMethods->FillHisto("RICHAnalysis_singlering_minxy",mindeltapos.X(),mindeltapos.Y());
    fUserMethods->FillHisto("RICHAnalysis_singlering_minmodvst",mintime,mindist);
    fUserMethods->FillHisto("RICHAnalysis_singlering_minmodvstchod",mintimechod,mindist);
    if (mindist<20 && fabs(mintime)<30) fUserMethods->FillHisto("RICHAnalysis_singlering_radvsp",thisTrack->GetMomentum()/1000,minradius);

    fRICHSingleCandidate->SetDiscriminant(minchi2);
    fRICHSingleCandidate->SetSingleRingDX(mindeltapos.X());
    fRICHSingleCandidate->SetSingleRingDY(mindeltapos.Y());
    fRICHSingleCandidate->SetSingleRingXcenter(minrcenter.X());
    fRICHSingleCandidate->SetSingleRingYcenter(minrcenter.Y());
    fRICHSingleCandidate->SetSingleRingRadius(minradius);
    fRICHSingleCandidate->SetSingleRingDeltaTime(mintime);
    fRICHSingleCandidate->SetSingleRingChi2(minringchi2);
    fRICHSingleCandidate->SetSingleRingNHits(minringnhits);
    fRICHSingleCandidate->SetMirrorID(mirrorid);

  }
  return minidring;
}

///////////////////////
// Mirror reflection //
///////////////////////
void RICHAnalysis::MirrorVector(TVector3 *vec, TVector3 *axis) {
  TVector3 S = ((vec->Dot(*axis))/(axis->Mag2()))*(*axis);
  TVector3 d = S-*vec;
  TVector3 ret = S+d;
  *vec = ret.Unit();
}

////////////////////////
// Find the mirror ID //
////////////////////////
Int_t RICHAnalysis::MirrorSurface(Double_t xin, Double_t yin) { // Roberta's routine based on closest mirror
  Double_t dmin = 1.0e12;
  Int_t mirrorID = 99;
  for (Int_t im=0; im<20; im++) {
    Double_t d =
      pow(fMirrorPosition[im][0]-xin, 2) +
      pow(fMirrorPosition[im][1]-yin, 2);
    if (d<dmin) {
      dmin = d;
      mirrorID = fMirrorNumber[im];
    }
  }// end of loop on mirrors
  return mirrorID;
}

Int_t RICHAnalysis::MirrorSurface(Double_t xin, Double_t yin, Double_t SafeFactor, Bool_t flag) {  // Mauro's routine based on geometrical cuts
  Int_t imirror=0;
  for (imirror=0; imirror<25; imirror++) {
    if (imirror==0 || imirror==2 || imirror==7 || imirror==18 || imirror==19 ) continue; // not existing mirrors
    if (flag==1 && imirror==23) continue; // skip semi-exagonal Jura
    if (flag==1 && imirror==24) continue; // skip semi-exagonal Saleve

    Double_t ac1=(fMirrorPos[imirror][0][1]-fMirrorPos[imirror][1][1])/(fMirrorPos[imirror][0][0]-fMirrorPos[imirror][1][0]);
    Double_t bcost1=(fMirrorPos[imirror][1][1]*fMirrorPos[imirror][0][0]-fMirrorPos[imirror][1][0]*fMirrorPos[imirror][0][1])/(fMirrorPos[imirror][0][0]-fMirrorPos[imirror][1][0]);
    Double_t ac3=(fMirrorPos[imirror][2][1]-fMirrorPos[imirror][3][1])/(fMirrorPos[imirror][2][0]-fMirrorPos[imirror][3][0]);
    Double_t bcost3=(fMirrorPos[imirror][3][1]*fMirrorPos[imirror][2][0]-fMirrorPos[imirror][3][0]*fMirrorPos[imirror][2][1])/(fMirrorPos[imirror][2][0]-fMirrorPos[imirror][3][0]);
    Double_t ac4=(fMirrorPos[imirror][3][1]-fMirrorPos[imirror][4][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][4][0]);
    Double_t bcost4=(fMirrorPos[imirror][4][1]*fMirrorPos[imirror][3][0]-fMirrorPos[imirror][4][0]*fMirrorPos[imirror][3][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][4][0]);
    Double_t ac6=(fMirrorPos[imirror][5][1]-fMirrorPos[imirror][0][1])/(fMirrorPos[imirror][5][0]-fMirrorPos[imirror][0][0]);
    Double_t bcost6=(fMirrorPos[imirror][0][1]*fMirrorPos[imirror][5][0]-fMirrorPos[imirror][0][0]*fMirrorPos[imirror][5][1])/(fMirrorPos[imirror][5][0]-fMirrorPos[imirror][0][0]);
    Double_t ConvSafeFactor1=SafeFactor*sqrt(1.+pow(ac1,2));
    Double_t ConvSafeFactor3=SafeFactor*sqrt(1.+pow(ac3,2));
    Double_t ConvSafeFactor4=SafeFactor*sqrt(1.+pow(ac4,2));
    Double_t ConvSafeFactor6=SafeFactor*sqrt(1.+pow(ac6,2));

////    if (imirror!=2) {
////      if ((yin<=ac1*xin+bcost1-ConvSafeFactor1) &&
////          (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) &&
////          (yin>=ac3*xin+bcost3+ConvSafeFactor3) &&
////          (yin>=ac4*xin+bcost4+ConvSafeFactor4) &&
////          (xin>=(fMirrorPos[imirror][4][0]+fMirrorPos[imirror][5][0])/2.+SafeFactor) &&
////          (yin<=ac6*xin+bcost6-ConvSafeFactor6) && (imirror<23)) return imirror;
////    } else {
////      if ((yin<=ac1*xin+bcost1-ConvSafeFactor1) &&
////          (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) &&
////          (yin>=ac3*xin+bcost3+ConvSafeFactor3) &&
////          (yin>=ac4*xin+bcost4+ConvSafeFactor4) &&
////          (xin>=0.+SafeFactor) &&
////          (yin<=ac6*xin+bcost6-ConvSafeFactor6) && (imirror<23)) return imirror;
////    }
    if (imirror!=23 && imirror!=24) {
      if ((yin<=ac1*xin+bcost1-ConvSafeFactor1) &&
          (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) &&
          (yin>=ac3*xin+bcost3+ConvSafeFactor3) &&
          (yin>=ac4*xin+bcost4+ConvSafeFactor4) &&
          (xin>=(fMirrorPos[imirror][4][0]+fMirrorPos[imirror][5][0])/2.+SafeFactor) &&
          (yin<=ac6*xin+bcost6-ConvSafeFactor6) && (imirror<23)) return imirror;
    } else if (imirror==23) {
      if ((yin<=ac1*xin+bcost1-ConvSafeFactor1) &&
          (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) &&
          (yin>=ac3*xin+bcost3+ConvSafeFactor3) &&
          (yin>=ac4*xin+bcost4+ConvSafeFactor4) &&
          (xin>=0.+SafeFactor) &&
          (yin<=ac6*xin+bcost6-ConvSafeFactor6) && (imirror==23)) return imirror;
    } else if (imirror==24) {
      if ((yin<=-ac1*xin+bcost1+ConvSafeFactor1) &&
          (xin>=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.+SafeFactor) &&
          (yin>=-ac3*xin+bcost3+ConvSafeFactor3) &&
          (yin>=-ac4*xin+bcost4+ConvSafeFactor4) &&
          (xin<=0.-SafeFactor) &&
          (yin<=-ac6*xin+bcost6-ConvSafeFactor6) && (imirror==24)) return imirror;
    }

////    if (flag==1) {
////      if (imirror==23 || imirror==24){
////        Double_t acs1=(fMirrorPos[imirror][1][1]-fMirrorPos[imirror][0][1])/(fMirrorPos[imirror][1][0]-fMirrorPos[imirror][0][0]);
////        Double_t bcosts1=(fMirrorPos[imirror][0][1]*fMirrorPos[imirror][1][0]-fMirrorPos[imirror][0][0]*fMirrorPos[imirror][1][1])/(fMirrorPos[imirror][1][0]-fMirrorPos[imirror][0][0]);
////        Double_t acs2=(fMirrorPos[imirror][3][1]-fMirrorPos[imirror][2][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][2][0]);
////        Double_t bcosts2=(fMirrorPos[imirror][2][1]*fMirrorPos[imirror][3][0]-fMirrorPos[imirror][2][0]*fMirrorPos[imirror][3][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][2][0]);
////        SafeFactor=-4.;
////        Double_t ConvSafeFactors1=SafeFactor*sqrt(1.+pow(acs1,2));
////        Double_t ConvSafeFactors2=SafeFactor*sqrt(1.+pow(acs2,2));
////        if ((yin<=acs1*xin+bcosts1+ConvSafeFactors1) && (xin>=(fMirrorPos[imirror][0][0]+fMirrorPos[imirror][3][0])/2.-SafeFactor) && (yin>=acs2*xin+bcosts2-ConvSafeFactors2) && (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.+SafeFactor) && (imirror==23)) return imirror;
////        if ((yin<=acs1*xin+bcosts1+ConvSafeFactors1) && (xin<=(fMirrorPos[imirror][0][0]+fMirrorPos[imirror][3][0])/2.+SafeFactor) && (yin>=acs2*xin+bcosts2-ConvSafeFactors2) && (xin>=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) && (imirror==24)) return imirror;
////      }
////    }

  }
  imirror=99;
  return imirror;
}
