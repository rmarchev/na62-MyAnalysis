#include "RICHImprovedRing.hh"
#include "AnalysisTools.hh"
#include "TRecoRICHEvent.hh"
#include "TRecoRICHCandidate.hh"
#include "TRecoRICHHit.hh"
#include "TMath.h"

using namespace NA62Constants;

std::vector<TVector2> hitPositionForChiFit;
std::vector<int> hitIDForChiFit;
double hMax;

RICHImprovedRing::RICHImprovedRing() {
  fNPars = 3; // Parameters for fit
  fFitter = new TMinuit(fNPars);
  fFitter->SetFCN(RingChi2FCN);
  tools = AnalysisTools::GetInstance();
  fjMax = -1;
}

RICHImprovedRing::~RICHImprovedRing() {
}


/********************************************//**
 * Minuit minimization function
 ************************************************/
void RICHImprovedRing::RingChi2FCN(Int_t &, Double_t *, Double_t &f, Double_t *par, Int_t iflag)
{
  if(1 || iflag == 4) {
    Int_t nHits = hitPositionForChiFit.size();
    f = 0.;
    for (Int_t iHit = 0; iHit < nHits; iHit++) {
      TVector2 PMPosition = hitPositionForChiFit[iHit];
      Double_t u = PMPosition.X() - par[0];
      Double_t v = PMPosition.Y() - par[1];
      Double_t d = TMath::Sqrt(u*u+v*v);
      Double_t dr = d - par[2];
      f += dr*dr/(4.7*4.7);
    }
  } // iflag=4 for Minuit
}

/********************************************//**
 * Ring Chi2
 ************************************************/
Double_t RICHImprovedRing::RingChi2(Double_t *par)
{
  Int_t nHits = hitPositionForChiFit.size();
  Double_t f = 0.;
  double maxdr = 0;
  for (Int_t iHit = 0; iHit < nHits; iHit++) {
    TVector2 PMPosition = hitPositionForChiFit[iHit];
    Double_t u = PMPosition.X() - par[0];
    Double_t v = PMPosition.Y() - par[1];
    Double_t d = TMath::Sqrt(u*u+v*v);
    Double_t dr = par[2] - d;
    if (fabs(dr)>fabs(maxdr)) {
      maxdr = dr;
      fjMax = hitIDForChiFit[iHit];
    }
    f += dr*dr/(4.7*4.7);
  }
  hMax = fjMax;
////  return f/(nHits-3); // added
  return f; // added
}

/********************************************//**
 * Chi2 Fit
 ************************************************/
Bool_t RICHImprovedRing::Chi2Fit(TRecoRICHEvent* event, TRecoRICHCandidate *thisRing, TVector3 pmtpos, int mirrorid, Double_t *deltamirror, Double_t *newring) {
  Float_t Xcog = 0;
  Float_t Ycog = 0;
  Float_t Rmean = 0;
  fmirroralign = deltamirror;

  // Extract the hits belonging to the ring candidate and compute the CoG
//  hitPositionForChiFit.clear();
  Int_t nHits = thisRing->GetNHits();
  TClonesArray& Hits = (*(event->GetHits()));
  Int_t *hitIndex = (Int_t *)thisRing->GetHitsIndexes();
  fjMax = -1;
  hMax = -1;

  Double_t pars[10],epars[10];
  TVector2 CurrentRingCenter;
  Double_t CurrentRingRadius, CurrentRingChi2;
  Double_t TimeAverage = 0;
  int goodNHits = 0;
  for (int kiter=0; kiter<2; kiter++) {
    goodNHits = 0;
    thisRing->SetEvent(event);
    hitPositionForChiFit.clear();
    hitIDForChiFit.clear();
    TimeAverage = 0;
    for (Int_t jHit=0; jHit<nHits; jHit++) {
      TRecoRICHHit *richhit = (TRecoRICHHit*)Hits[hitIndex[jHit]];
      double xpos = richhit->GetPosition().X()-GetChPosAngCorr(richhit->GetChannelSeqID()).X();//-deltamirror[4+mirrorid-1];
      double ypos = richhit->GetPosition().Y()-GetChPosAngCorr(richhit->GetChannelSeqID()).Y();//-deltamirror[28+mirrorid-1];
      Double_t hdist2 = (xpos-pmtpos.X())*(xpos-pmtpos.X())+(ypos-pmtpos.Y())*(ypos-pmtpos.Y());
      if (jHit==fjMax) continue;
      if (hdist2>200.*200.) continue;
      if (hdist2<80.*80.) continue;
      if (richhit->GetOrSuperCellID()==1) continue;
      if (fabs(thisRing->GetTime()-richhit->GetTime())>2.) continue; // ADDED
      hitPositionForChiFit.push_back(TVector2(xpos,ypos));
      hitIDForChiFit.push_back(jHit);
      Xcog += hitPositionForChiFit[goodNHits].X();
      Ycog += hitPositionForChiFit[goodNHits].Y();
      TimeAverage  += richhit->GetTime();
      goodNHits++;
    }
    Xcog /= (double)goodNHits;
    Ycog /= (double)goodNHits;
    TimeAverage /= (double)goodNHits;
    for (Int_t iHit=0; iHit<goodNHits; iHit++) {
      Rmean += sqrt(pow(Xcog-hitPositionForChiFit[iHit].X(),2)+pow(Ycog-hitPositionForChiFit[iHit].Y(),2));
    }
    Rmean /= (double)goodNHits;
    if (goodNHits<4) return 0;

    // Perform the fit
    Double_t amin,edm,errdef;
    Int_t nvpar,nparx,icstat,ierflag;
    Double_t arglist[1];
    arglist[0] = -1;
    fFitter->mnexcm("SET PRI", arglist, 1, ierflag); // Set MINUIT print level
    fFitter->mnexcm("SET NOW", arglist, 0, ierflag); // Set MINUIT warnings
    fFitter->mnparm(0, "x0", Xcog, 0.01, 0., 0., ierflag);
    fFitter->mnparm(1, "y0", Ycog, 0.01, 0., 0., ierflag);
    fFitter->mnparm(2, "R", Rmean, 0.01, 0., 0., ierflag);
    arglist[0] = 0;
    fFitter->mnexcm("MIGRAD", arglist, 1, ierflag); // Calls the minimization
    fFitter->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
    for(Int_t iPar = 0; iPar < fNPars; iPar++) fFitter->GetParameter(iPar,pars[iPar],epars[iPar]);

    // Define variables
    CurrentRingCenter.Set(pars[0],pars[1]);
    CurrentRingRadius = pars[2];
    CurrentRingChi2 = RingChi2(pars);
//    if (CurrentRingChi2<5) break;
    if (TMath::Prob(CurrentRingChi2,goodNHits-3)>0.005) break; // Modified
  }

  newring[0] = pars[0];
  newring[1] = pars[1];
  newring[2] = CurrentRingRadius;
////  newring[3] = CurrentRingChi2;
  newring[3] = TMath::Prob(CurrentRingChi2,goodNHits-3); // Modified
  newring[4] = TimeAverage;
  newring[5] = goodNHits;

  return 1;
}

/********************************************//**
 * Mirror focus position corrections
 ************************************************/
TVector2 RICHImprovedRing::GetChPosAngCorr(Int_t iCh)
{
  TVector2 jurarotation;
  TVector2 saleverotation;
  if (fYear==2015) {
    jurarotation = TVector2(127,0);
    saleverotation = TVector2(177,0);
  }
  if (fYear==2016) {
////    jurarotation = TVector2(146.8,19.8);
////    saleverotation = TVector2(196.7,9.5);
    jurarotation = TVector2(127+fmirroralign[0],fmirroralign[1]);
    saleverotation = TVector2(177+fmirroralign[2],fmirroralign[3]);
  }
////  if (fIsMC) {
////    jurarotation = TVector2(127,0);
////    saleverotation = TVector2(177,0);
////  }
  TVector2 Beta;
  if (iCh < 1952/2 || (1952 <= iCh && iCh < 1952+(243/2))) Beta = jurarotation;
  else if ((1952/2 <= iCh && iCh < 1952) || (1952+(243/2) <= iCh && iCh <1952+243 )) Beta = saleverotation;
  TVector2 Correction = Beta;// the correction due to the mirror rotation is already multiplied by the fFocalLength and it is given in mm;
  return Correction;
}
