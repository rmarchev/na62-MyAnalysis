#include "RICHCandidate.hh"

RICHCandidate::RICHCandidate()
{}

RICHCandidate::~RICHCandidate()
{}

void RICHCandidate::Clear() {
  fIsRICHSingleRingCandidateSlava = 0;
  fIsRICHMultiRingCandidate = 0;
  fIsRICHSingleRingCandidate = 0;
  fIsRICHCandidate = 0;
  fIsRICHPion = 0;
  fMostLikelyHypothesis = -2.;
  fPiNexpectedHits= -1 ;
  fPosNexpectedHits = -1;
  fMuNexpectedHits = -1;
  fDiscriminant = 99999999.;
  fDX           = -9999999.;
  fDY           = -9999999.;
  fXcenter      = -99999.;
  fYcenter      = -99999.;
  fRadius       = 0.;
  fDeltaTime    = 99999999.;
  fChi2         = 99999999.;
  fNHits        = 0;
  fNRings       = 0;
  fSingleRingNiter        = 0;
  fMirrorID     = 99;
  fRICHMuLikelihood = -1;
  fRICHPiLikelihood= -1;
  fRICHElectronLikelihood = -1;
  fCandID       = -1;
  //Single ring
  fSingleRingDR           = -9999999.;
  fSingleRingDX           = -9999999.;
  fSingleRingDY           = -9999999.;
  fSingleRingXcenter      = -99999.;
  fSingleRingXcenterErr   = -99999.;
  fSingleRingYcenter      = -99999.;
  fSingleRingYcenterErr   = -99999.;
  fSingleRingRadius       = 0.;
  fSingleRingTime    = 99999999.;
  fSingleRingChi2         = 99999999.;
  fSingleRingNHits        = 0;
  fSingleRingCandID       = -1;
}
