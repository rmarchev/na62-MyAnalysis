#include "DownstreamParticle.hh"

DownstreamParticle::DownstreamParticle()
{ }

DownstreamParticle::~DownstreamParticle()
{ }
void DownstreamParticle::Clear()
{
  //Particle::Clear();
  fCaloTimeRA = -99999999999;                    // Calorimetric time by Riccardo
  fMuProb = 999999;                        // muon probability
  fElProb = 999999;                        // muon probability
  fPiProb = 999999;                        // pion probability
  fMIPdiscr = 9999999;                      // mip discriminant < 1 for muons
  fIsMIP =0;                           // mip flag
  fTrackCharge =0;
  fIsMulti = 0;
  fLKrMult=0;              //Multiplicity in LKr in time and space with CHOD and NewCHOD
  fLKrHadrMult=0;
  fLKrCHODdR     = TVector3(9999,9999,9999);
  fLKrNewCHODdR  = TVector3(9999,9999,9999);
  fCHODNewCHODdR = TVector3(9999,9999,9999);
  fCHODNewCHODdt = 9999;
  fLKrNewCHODdt = 9999;
  fLKrCHODdt=9999;
  fLKrMerged=false;
  fPionMomentum.SetXYZM(0.,0.,0.,0.);
  fMuonMomentum.SetXYZM(0.,0.,0.,0.);
  fElectronMomentum.SetXYZM(0.,0.,0.,0.);
  fDownstreamTime = -99999.;
  fCHODTime = -99999.;
  fNewCHODTime = -99999.;
  fCHODNewCHODMult = 0.;
  fCHODMult = 0.;
  fCHODSlabsInTime = 0.;
  fIsRAMuon = 0;
  fIsEM = 0;

  fNewCHODMult = 0.;
  fCHODPosition.SetXYZ(0.,0.,0.);
  fNewCHODPosition.SetXYZ(0.,0.,0.);
  fIsRICHSingleRingCandidateSlava = 0;
  fIsRICHPion = 0;
  fRICHSingleDiscriminant = 0;
  fRICHMultiIsCandidate = 0;
  fMostLikelyHypothesis = -2.;
  fPiNexpectedHits= -1 ;
  fPosNexpectedHits = -1;
  fMuNexpectedHits = -1;

  fNewCHODRecoHitID = -1;
  fCHODRecoHitVID = -1;
  fCHODRecoHitHID = -1;
  fCounterV = -1;
  fCounterH = -1;
  fTimeV = -99999.;
  fTimeH = -99999.;

  fGoodCHOD = 0;
  fGoodNewCHOD = 0;
  fGoodRICHSR = 0;
  fGoodRICHMR = 0;
  fGoodLKr  = 0;
  fGoodMUV1 = 0;
  fGoodMUV2 = 0;
  fGoodMUV3 = 0;

  fRICHMultiRadius = -99999.;
  fRICHMultiMass = -99999.;
  fRICHMultiMomentum.SetXYZM(0.,0.,0.,0.);
  fRICHMultiTime = -99999.;
  fRICHMultiChi2 = -99999.;
  fRICHSingleIsCandidate = 0;
  fRICHSingleRadius = -99999.;
  fRICHSingleMass = -99999.;
  fRICHSingleMomentum.SetXYZM(0.,0.,0.,0.);
  fRICHSingleTime = -99999.;
  fRICHSingleChi2 = -99999.;
  fLKrID = -1;
  fLKrTime = -99999.;
  fLKrEovP = -10.;
  fLKrSeedEnergy = 0.;
  fLKrNCells = 0.;
  fMUV1ID = -1;
  fMUV1OuterNhits = 0;
  fMUV1Energy = 0.;
  fMUV1Time = -99999.;
  fMUV1ShowerWidth = -100.;
  fMUV1SeedRatio = 0.;
  fMUV1EMerged = 0.;
  fMUV2ID = -1;
  fMUV2OuterNhits = 0;
  fMUV2Energy = 0.;
  fMUV2Time = -99999.;
  fMUV2ShowerWidth = -100.;
  fMUV2SeedRatio = -0.;
  fCalorimetricEnergy = 0.;
  fMUV3ID = -1;
  fMUV3PosID = -1;
  fMUV3Time = -99999.;
  fMUV3Distance.Set(-99999.,-99999.);
  fUpstreamTrackID = -1;
  fTrackID = -1;
  fRICHMuLikelihood = -1;
  fRICHPiLikelihood = -1;
  fRICHElectronLikelihood = -1;
  for (Int_t j=0; j<3; j++) fPosAtGTK[j] = TVector3(0,0,0);
  for (Int_t j=0; j<4; j++) fPosAtStraw[j] = TVector3(0,0,0);
  for (Int_t j=0; j<3; j++) fPosAtRICH[j] = TVector3(0,0,0);
  for (Int_t j=0; j<12; j++) fPosAtLAV[j] = TVector3(0,0,0);
  for (Int_t j=0; j<3; j++) fPosAtCHOD[j] = TVector3(0,0,0);
  for (Int_t j=0; j<3; j++) fPosAtNewCHOD[j] = TVector3(0,0,0);
  for (Int_t j=0; j<2; j++) fPosAtIRC[j] = TVector3(0,0,0);
  fPosAtLKr = TVector3(0,0,0);
  for (Int_t j=0; j<3; j++) fPosAtMUV[j] = TVector3(0,0,0);
  fIsPhotonLKrCandidate = 0;
  fIsPhotonLKrActivity = 0;
  fIsPhotonLAVCandidate = 0;
  fIsLAVTrack = 0;
  fIsPhotonIRCCandidate = 0;
  fIsPhotonSACCandidate = 0;
  fIsPhotonSAVCandidate = 0;
  fIsPhotonNewLKrCandidate = 0;
  fNPhotonLKrCandidates = 0;
  fNPhotonNewLKrCandidates = 0;
  for (Int_t j=0; j<10; j++) {
    fPhotonLKrCandidateTime[j] = 999999.;
    fPhotonLKrCandidatePosition[j] = TVector2(-99999.,-99999.);
    fPhotonLKrCandidateEnergy[j] = 0.;
    fPhotonLKrCandidateNCells[j] = 0;
  }
  fNPhotonLAVCandidates = 0;
  for (Int_t j=0; j<80; j++) {
    fPhotonLAVCandidateTime[j] = 999999.;
    fPhotonLAVCandidateID[j] = 0.;
    fPhotonLAVCandidateLAVID[j] = 0.;
  }
  fNPhotonIRCCandidates = 0;
  for (Int_t j=0; j<20; j++) {
    fPhotonIRCCandidateTime[j] = 999999.;
    fPhotonIRCCandidateID[j] = 0.;
  }
  fNPhotonSACCandidates = 0;
  for (Int_t j=0; j<20; j++) {
    fPhotonSACCandidateTime[j] = 999999.;
    fPhotonSACCandidateID[j] = 0.;
  }
  fNPhotonSAVCandidates = 0;

}
