#include "Parameters.hh"
#include "CHODAnal.hh"
#include "AnalysisTools.hh"
#include "EventHeader.hh"
#include "Constants.hh"
#include "LKrAuxClusterReco.hh"
#include "NA62Exceptions.hh"
#include "TSystem.h"
#include <numeric>
#include "GeometricAcceptance.hh"
//#include "SpectrometerMUV3AssociationOutput.hh"
//#include "LAVMatching.hh"

CHODAnal::CHODAnal(int flag,NA62Analysis::Core::BaseAnalysis *ba) {

    fFlag = flag;
    fTools = AnalysisTools::GetInstance();
    fUserMethods = new UserMethods(ba);
    param = new Parameters();


    if(fFlag==1){

        fUserMethods->BookHisto(new TH2F("LKr_dt_vs_ehit","",2000,0,100000,1200,-150,150));
        fUserMethods->BookHisto(new TH2F("LKr_dt_vs_dist","",1200,0,1200,1200,-150,150));
        fUserMethods->BookHisto(new TH2F("LKr_dt_vs_ehit_after","",2000,0,100000,1200,-150,150));
        fUserMethods->BookHisto(new TH2F("LKr_dt_vs_dist_after","",1200,0,1200,1200,-150,150));
        fUserMethods->BookHisto(new TH2F("LKr_dx_vs_dy","",2400,-1200,1200,2400,-1200,1200));
        fUserMethods->BookHisto(new TH2F("LKr_x_vs_y","",240,-1200,1200,240,-1200,1200));


        fUserMethods->BookHisto(new TH1F("NewCHOD_dt","",800,-200,200));
        fUserMethods->BookHisto(new TH1F("NewCHOD_intime_dt","",800,-200,200));

        fUserMethods->BookHisto(new TH2F("NewCHODLKr_dist_vs_dt","",800,-200,200,300,0,900));
        fUserMethods->BookHisto(new TH2F("NewCHODLKr_dt_vs_ehit","",2000,0,100000,1200,-150,150));
        fUserMethods->BookHisto(new TH2F("NewCHODLKr_dx_vs_dy","",240,-1200,1200,240,-1200,1200));
        fUserMethods->BookHisto(new TH2F("NewCHODLKr_track_dx_vs_dy","",40,-1200,1200,80,-1200,1200));

        fUserMethods->BookHisto(new TH2F("NewCHODLKr_in_track_dx_vs_dy","",40,-1200,1200,80,-1200,1200));
        fUserMethods->BookHisto(new TH2F("NewCHODLKr_in_dist_vs_dt","",800,-200,200,300,0,900));
        fUserMethods->BookHisto(new TH2F("NewCHODLKr_in_dx_vs_dy","",240,-1200,1200,240,-1200,1200));
        fUserMethods->BookHisto(new TH2F("NewCHODLKr_in_dt_vs_ehit","",2000,0,100000,1200,-150,150));

        fUserMethods->BookHisto(new TH1F("CHODLKr_chi2","",200,0,50));
        fUserMethods->BookHisto(new TH2F("CHODLKr_dist_vs_dt","",800,-200,200,300,0,900));
        fUserMethods->BookHisto(new TH2F("CHODLKr_dt_vs_ehit","",2000,0,100000,1200,-150,150));
        fUserMethods->BookHisto(new TH2F("CHODLKr_dx_vs_dy","",240,-1200,1200,240,-1200,1200));
        fUserMethods->BookHisto(new TH2F("CHODLKr_track_dx_vs_dy","",40,-1200,1200,80,-1200,1200));

        fUserMethods->BookHisto(new TH2F("CHODLKr_in_track_dx_vs_dy","",40,-1200,1200,80,-1200,1200));
        fUserMethods->BookHisto(new TH2F("CHODLKr_in_dist_vs_dt","",800,-200,200,300,0,900));
        fUserMethods->BookHisto(new TH2F("CHODLKr_in_dx_vs_dy","",240,-1200,1200,240,-1200,1200));
        fUserMethods->BookHisto(new TH2F("CHODLKr_in_dt_vs_ehit","",2000,0,100000,1200,-150,150));

        fUserMethods->BookHisto(new TH1F("CHODNewCHOD_chi2","",200,0,50));
        fUserMethods->BookHisto(new TH2F("CHODNewCHOD_dist_vs_dt","",800,-200,200,300,0,900));
        fUserMethods->BookHisto(new TH2F("CHODNewCHOD_dx_vs_dy","",240,-1200,1200,240,-1200,1200));
        fUserMethods->BookHisto(new TH2F("CHODNewCHOD_track_dx_vs_dy","",40,-1200,1200,80,-1200,1200));

        fUserMethods->BookHisto(new TH2F("CHODNewCHOD_in_track_dx_vs_dy","",40,-1200,1200,80,-1200,1200));
        fUserMethods->BookHisto(new TH2F("CHODNewCHOD_in_dist_vs_dt","",800,-200,200,300,0,900));
        fUserMethods->BookHisto(new TH2F("CHODNewCHOD_in_dx_vs_dy","",240,-1200,1200,240,-1200,1200));

        fUserMethods->BookHisto(new TH1F("CHODSlabs_dt","",800,-200,200));
        fUserMethods->BookHisto(new TH1F("CHOD_intime_dt","",800,-200,200));
        fUserMethods->BookHisto(new TH1I("CHOD_intime_Nmult","",50,0,50));
        fUserMethods->BookHisto(new TH1I("NewCHOD_intime_Nmult","",50,0,50));
        fUserMethods->BookHisto(new TH1I("LKr_intime_Nmult","",50,0,50));
        fUserMethods->BookHisto(new TH1I("LKrNewCHOD_Nmult","",50,0,50));
        fUserMethods->BookHisto(new TH1I("LKrCHOD_Nmult","",50,0,50));
        fUserMethods->BookHisto(new TH1I("CHODNewCHOD_Nmult","",50,0,50));
        fUserMethods->BookHisto(new TH1I("LKrMerged","",2,0,2));

    }

    fCHODCandidate = new CHODCandidate();


    fHeader = fUserMethods->GetEventHeader();


    for (Int_t i=0; i<128; i++) {
        if (i<16) fSlabCenter[i] = (fSlabLimitsX[16-i-1] + fSlabLimitsX[16-i])/2.;
        else if (i<48) fSlabCenter[i] = (fSlabLimitsX[i-16] + fSlabLimitsX[i-16+1])/2.;
        else if (i<64) fSlabCenter[i] = (fSlabLimitsX[80-i] + fSlabLimitsX[80-i-1])/2.;
        else if (i<96) fSlabCenter[i] = (fSlabLimitsY[96-i] + fSlabLimitsY[96-i-1])/2.;
        else fSlabCenter[i] = (fSlabLimitsY[i-96] + fSlabLimitsY[i-96+1])/2.;
    }

}
CHODAnal::~CHODAnal() {


}

void CHODAnal::StartBurst(Int_t runid) {

    fIsMC = fUserMethods->GetWithMC();
    // Read CHOD parameters
    TString CHODConfigFile;

    if(runid < 7000)
        CHODConfigFile = Form("%s/config/CHOD.2016.conf",getenv("NA62RECOSOURCE"));
    if(runid > 7000)
        CHODConfigFile = Form("%s/config/CHOD.2017.conf",getenv("NA62RECOSOURCE"));

    TString CHODLightVelocitiesFile;
    if (runid>5435 && runid<=6473) CHODLightVelocitiesFile = Form("%s/config/%s",getenv("NA62RECOSOURCE"),"CHOD-LightVelocities.run5435_0000-run6473_9999.dat");
    if (runid>6474 && runid<7000) CHODLightVelocitiesFile = Form("%s/config/%s",getenv("NA62RECOSOURCE"),"CHOD-LightVelocities.run6474_0000-run6912_9999.dat");

    // Read Geometry
    ifstream CHODConfig(CHODConfigFile.Data());
    cout << " --->  Reading " << CHODConfigFile.Data() << endl;
    TString line;
    while (line.ReadLine(CHODConfig)) {
        if (line.BeginsWith("#")) continue;
        TObjArray * l = line.Tokenize(" ");
        if (line.BeginsWith("SC_PositionV")) {
            double pos[16];
            for (int jl(0); jl<16; jl++) pos[jl] = ((TObjString*)(l->At(jl+1)))->GetString().Atof();
            for (int j=0; j<16; j++)  fCHODPosV[j] = -pos[j];
            for (int j=16; j<32; j++) fCHODPosV[j] = -pos[31-j];
            for (int j=32; j<48; j++) fCHODPosV[j] = pos[j-32];
            for (int j=48; j<64; j++) fCHODPosV[j] = pos[63-j];
            for (int j=0; j<16; j++)  fCHODPosH[j] = pos[15-j];
            for (int j=16; j<32; j++) fCHODPosH[j] = -pos[j-16];
            for (int j=32; j<48; j++) fCHODPosH[j] = -pos[47-j];
            for (int j=48; j<64; j++) fCHODPosH[j] = pos[j-48];
        }
    }
    cout << "---> File " << CHODConfigFile.Data() << " read " << endl;
    CHODConfig.close();

    //Slab length
    double slabLength[16] = {1210.,1210.,1210.,1210.,1210.,1210.,1210.,1210.,1191.,1126.,1061.,996.,897.,798.,699.,600.};
    for (Int_t j=0; j<16; j++)  fCHODLengthV[j] = slabLength[j];
    for (Int_t j=16; j<32; j++) fCHODLengthV[j] = slabLength[31-j];
    for (Int_t j=32; j<48; j++) fCHODLengthV[j] = slabLength[j-32];
    for (Int_t j=48; j<64; j++) fCHODLengthV[j] = slabLength[63-j];
    for (Int_t j=0; j<16; j++)  fCHODLengthH[j] = slabLength[15-j];
    for (Int_t j=16; j<32; j++) fCHODLengthH[j] = slabLength[j-16];
    for (Int_t j=32; j<48; j++) fCHODLengthH[j] = slabLength[47-j];
    for (Int_t j=48; j<64; j++) fCHODLengthH[j] = slabLength[j-48];



    // Standard T0
    int nRL = 0;
    // TString CHODT0File = Form("/eos/experiment/na62/user/n/na62prod/daemonspace/NA62FWr1421-Run%d/NA62Reconstruction/config/CHOD-T0.dat",runid);
    TString CHODT0File;

    //2016 data
    if(runid < 7000 && !fIsMC)
        CHODT0File = Form("/eos/experiment/na62/user/n/na62prod/daemonspace/NA62FWr1666-Run%d/NA62Reconstruction/config/CHOD-T0.dat",runid);
    //CHODT0File = Form("/afs/cern.ch/work/n/na62prod/daemonspace/NA62FWr1666-Run%d/NA62Reconstruction/config/CHOD-T0.dat",runid);
    ifstream chodt0(CHODT0File.Data());
    if(!chodt0.good()) {
        std::cout << "  " << std::endl;
        std::cout << " No T0`s on eos, loading the T0 from afs!!" << std::endl;
        std::cout << "  " << std::endl;
        //CHODT0File = Form("/afs/cern.ch/user/n/na62prod/public/CDB/Prod-01/%d/CHOD-T0.dat",runid);
        CHODT0File = Form("/afs/cern.ch/work/n/na62prod/daemonspace/NA62FWr1666-Run%d/NA62Reconstruction/config/CHOD-T0.dat",runid);;

    }
    //2017 data
    if(runid > 7000 && !fIsMC)
        CHODT0File = Form("/afs/cern.ch/work/n/na62prod/daemonspace/NA62FWr1767-Run%d/NA62Reconstruction/config/CHOD-T0.dat",runid);

    if(fIsMC)
        CHODT0File = Form("/eos/experiment/na62/user/n/na62prod/daemonspace/NA62FWr1666-Run6610/NA62Reconstruction/config/CHOD-T0.dat");
    //CHODT0File = Form("/afs/cern.ch/user/n/na62prod/daemonspace/NA62FWr1666-Run%d/NA62Reconstruction/config/CHOD-T0.dat",runid);

    //TString CHODT0File = Form("%s/config/CHOD-T0AtPM.run6474_0000-run6912_9999.dat",getenv("NA62RECOSOURCE"));

    ifstream CHODT0(CHODT0File.Data());
    cout << " --->  Reading " << CHODT0File.Data() << endl;
    while (line.ReadLine(CHODT0)) {
        if (line.BeginsWith("#")) continue;
        TObjArray * l = line.Tokenize(" ");
        double t0 = ((TObjString*)(l->At(2)))->GetString().Atof();
        fCHODAllT0[(int)(nRL/16)][(int)(nRL%16)] = fabs(t0)>=99 ? 0.0 : t0;
        l->Delete();
        nRL++;
    }
    cout << "---> File " << CHODT0File.Data() << " read " << nRL << " lines" << endl;
    CHODT0.close();

    // Slewings
    nRL = 0;

    TString runref = runid>6473 ? "run6474_0000-run6912_9999" : "run5435_0000-run6473_9999";

    //CHOD-SlewCorr.run6474_0000-run6912_9999.dat
    //CHOD-SlewCorr.run5435_0000-run6473_9999.dat
    //TString CHODSlewingFile = Form("%s/config/CHOD-SlewCorr.run1520_0076-run1520_2083.dat",getenv("NA62RECOSOURCE"));
    TString CHODSlewingFile = Form("%s/config/CHOD-SlewCorr.%s.dat",getenv("NA62RECOSOURCE"),runref.Data());
    //TString CHODSlewingFile = Form("%s/config/CHOD-SlewCorr.run6474_0000-run6912_9999.dat",getenv("NA62RECOSOURCE"));
    ifstream CHODSlewing(CHODSlewingFile.Data());
    cout << " --->  Reading " << CHODSlewingFile.Data() << endl;
    while (line.ReadLine(CHODSlewing)) {
        TObjArray * l = line.Tokenize(" ");
        fCHODAllSlewSlope[(int)(nRL/16)][(int)(nRL%16)] = !fIsMC ? ((TObjString*)(l->At(0)))->GetString().Atof(): 0;
        fCHODAllSlewConst[(int)(nRL/16)][(int)(nRL%16)] = !fIsMC ? ((TObjString*)(l->At(1)))->GetString().Atof(): 0;
        l->Delete();
        nRL++;
    }
    cout << "---> File " << CHODSlewingFile.Data() << " read " << nRL << " lines" << endl;
    CHODSlewing.close();

    // Fine T0
    //ifstream CHODFineT0("/afs/cern.ch/work/r/ruggierg/public/na62git/database/run6670/CHODfinet0.dat");
    //nRL = 0;
    for (int jP(0); jP<2048; jP++) {
        fCHODAllFineT0[(int)(nRL/16)][(int)(nRL%16)] = 0.;
    }
    //CHODFineT0.close();

    // Read file for MC CHOD T0
    TString CHODMCT0File = "/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/mc/chod_channel_parameters.dat";
    //TString CHODMCT0File = "/eos/experiment/na62/user/n/na62prod/daemonspace/NA62FWr1421-Run6610/NA62Reconstruction/config/CHOD-T0.dat";
    ifstream CHODMCT0(CHODMCT0File.Data());
    cout << " ---> Reading " << CHODMCT0File.Data() << endl;
    int nCH = 0;
    while (line.ReadLine(CHODMCT0)) {
        TObjArray * l = line.Tokenize(" ");
        fCHODT0[nCH] = ((TObjString*)(l->At(0)))->GetString().Atof();
        fCHODVelocity[nCH] = ((TObjString*)(l->At(1)))->GetString().Atof();
        l->Delete();
        nCH++;
    }
    cout << "---> File " << CHODMCT0File.Data() << " read " << nCH << " lines" << endl;
    CHODMCT0.close();


    // Light Velocities
    ifstream CHODLightVelocities(CHODLightVelocitiesFile.Data());
    if(CHODLightVelocities.is_open()){
        cout << " ---> Reading " << CHODLightVelocitiesFile.Data() << endl;
        for(Int_t iSlab=0; iSlab<128; iSlab++){
            CHODLightVelocities >> fCHODLightVelocities[iSlab];
            //Int_t IP=0;
            if (iSlab >= 0 && iSlab < 16) {
                for (Int_t iIntersectingSlab=64; iIntersectingSlab<80; iIntersectingSlab++) {
                    Double_t lightvelCorr = - fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10.*(121.-fPMCoordinate[iSlab]));
                    //IP = iSlab*16 + (iIntersectingSlab-64);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab-64] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
            else if (iSlab >= 16 && iSlab < 32) {
                for (Int_t iIntersectingSlab=80; iIntersectingSlab<96; iIntersectingSlab++) {
                    Double_t lightvelCorr = + fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10.*(-fPMCoordinate[iSlab]));
                    //IP = iSlab*16 + (iIntersectingSlab-80);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab-80] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
            else if (iSlab >= 32 && iSlab < 48) {
                for (Int_t iIntersectingSlab=96; iIntersectingSlab<112; iIntersectingSlab++) {
                    Double_t lightvelCorr = + fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10.*(-121.+fPMCoordinate[iSlab]));
                    //IP = iSlab*16 + (iIntersectingSlab-96);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab-96] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
            else if (iSlab >= 48 && iSlab < 64) {
                for (Int_t iIntersectingSlab=112; iIntersectingSlab<128; iIntersectingSlab++) {
                    Double_t lightvelCorr = - fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10.*fPMCoordinate[iSlab]);
                    //IP = iSlab*16 + (iIntersectingSlab-112);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab-112] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
            else if (iSlab >= 64 && iSlab < 80) {
                for (Int_t iIntersectingSlab=0; iIntersectingSlab<16; iIntersectingSlab++) {
                    Double_t lightvelCorr = + fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10.*(-fPMCoordinate[iSlab]));
                    //IP = iSlab*16 + (iIntersectingSlab);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
            else if (iSlab >= 80 && iSlab < 96) {
                for (Int_t iIntersectingSlab=16; iIntersectingSlab<32; iIntersectingSlab++) {
                    Double_t lightvelCorr = + fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10*(-121.+fPMCoordinate[iSlab]));
                    //IP = iSlab*16 + (iIntersectingSlab-16);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab-16] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
            else if (iSlab >= 96 && iSlab < 112) {
                for (Int_t iIntersectingSlab=32; iIntersectingSlab<48; iIntersectingSlab++) {
                    Double_t lightvelCorr = - fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10*fPMCoordinate[iSlab]);
                    //IP = iSlab*16 + (iIntersectingSlab-32);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab-32] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
            else if (iSlab >= 112 && iSlab < 128) {
                for (Int_t iIntersectingSlab=48; iIntersectingSlab<64; iIntersectingSlab++) {
                    Double_t lightvelCorr = - fCHODLightVelocities[iSlab]*(fSlabCenter[iIntersectingSlab]-10*(121.-fPMCoordinate[iSlab]));
                    //IP = iSlab*16 + (iIntersectingSlab-48);
                    fCHODLightVelocitiesCorr[iSlab][iIntersectingSlab-48] = fabs(lightvelCorr)>=99 ? 0.0 : lightvelCorr;
                }
            }
        }
        CHODLightVelocities.close();
    }


}

void CHODAnal::Clear() {
    fCHODCandidate->Clear();
    // Clear
    for (int jP(0); jP<2; jP++) {
        for (int jQ(0); jQ<4; jQ++) fvCHODQ[jP][jQ].clear();
    }
}

bool CHODAnal::LKrHitInTime(double E, double dt) {
    double Egev = E/1000.;
    if(Egev < 0.5) return false; // Do not consider individual hits if E_cell < 45 MeV
    //if(dt > 2*(0.2 - 63.0/sqrt(E)) && dt < 2*(1.6 + 50.0/sqrt(E))) return true;

    Double_t dtime36 = dt+36;
    Double_t dtime25 = dt+25;
    Double_t dtime25m= dt-25;

    double sigma = 0.56 + 1.53/Egev -0.233/sqrt(Egev);

    if(Egev < 0.3 && fabs(dt) < 4 ) return true; //Fitted function cut at 10 ns
    if(Egev >= 0.3 && Egev < 2 && dt < 10 && dt > -7 ) return true; //Fitted function cut at 10 ns
    if(Egev >= 2 && dt < 10 && dt > -10 ) return true; //Fitted function cut at 10 ns

    return false;

}
Bool_t CHODAnal::FindMultiplicity(DownstreamParticle &track) {

    fLKrMult=0;
    fLKrHadrMult=0;
    fCHODMult=0;
    fCHODNewCHODMult=0;
    fNewCHODMult=0;

    double reftime     = track.GetCHODTime();

    TVector3 atlkr     = track.GetPositionAtLKr();
    TVector3 atchod    = track.GetPositionAtCHOD(2);
    TVector3 atnewchod = track.GetPositionAtNewCHOD(2);

    std::vector<int> nch_hintime;
    std::vector<int> ch_hintime;
    std::vector<int> lkr_hintime;

    std::vector<std::pair<int,int>> chodlkr;
    std::vector<std::pair<int,int>> newchodlkr;
    std::vector<std::pair<int,int>> chodnewchod;

    TClonesArray newchodhits = (*fNewCHODEvent->GetHits());
    TClonesArray lkrhits = (*fLKrEvent->GetHits());

    for(auto iH(0);iH<fCHODEvent->GetNHits();iH++){
        TRecoCHODHit *Hit = (TRecoCHODHit*)fCHODEvent->GetHit(iH);
        double dt(-999999);


        if(fUserMethods->GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.1") || fUserMethods->GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.2"))
            dt = reftime-(Hit->GetTime() - 7); //correct for the different T0 procedure
        else
            dt = reftime-Hit->GetTime();
        //cout << reftime << "   " << Hit->GetTime() << endl;
        fUserMethods->FillHisto("CHODSlabs_dt",dt);
        //cout << "CHOD iH = " << iH << " track hit HID = " << track.GetCHODRecoHitHID() << " track hit VID = " << track.GetCHODRecoHitVID() << " dt = " << dt << endl;
        if(fabs(dt) > 7 || iH== track.GetCHODRecoHitHID() || iH== track.GetCHODRecoHitVID()) continue;
        //cout << "CHOD iH in time= " << iH << " track hit HID = " << track.GetCHODRecoHitHID() << " track hit VID = " << track.GetCHODRecoHitVID() << endl;
        fUserMethods->FillHisto("CHOD_intime_dt",dt);
        fCHODSlabsInTime++;
        ch_hintime.push_back(iH);
    }

    //fill a map with all lkr hits matching in time with the reference
    //and sort them by distance to the track extrapolated position
    //do not consider hits within 10 cm from the track
    for(int iH=0;iH < fLKrEvent->GetNHits();iH++){

        TRecoLKrHit* LKr = (TRecoLKrHit*)lkrhits.At(iH);

        double LKrtime = LKr->GetTime();
        double LKrdt   = reftime - LKr->GetTime();
        double ELKr    = LKr->GetEnergy();

        TVector3 LKrpos= LKr->GetPosition();
        double dx=(atlkr - LKrpos).X();
        double dy=(atlkr - LKrpos).Y();
        double trackdist = sqrt(dx*dx+dy*dy);
        bool intime=LKrHitInTime(ELKr,LKrdt);
        fUserMethods->FillHisto("LKr_dt_vs_ehit", ELKr,LKrdt);
        fUserMethods->FillHisto("LKr_dt_vs_dist", trackdist,LKrdt);

        if(!intime) continue;
        if(trackdist < 100) continue;
        //cout << "LKr iH = " << iH << " trackdist = " << trackdist << " time = " << LKrdt << endl;
        fUserMethods->FillHisto("LKr_dt_vs_ehit_after", ELKr,LKrdt);
        fUserMethods->FillHisto("LKr_dt_vs_dist_after", trackdist,LKrdt);
        fUserMethods->FillHisto("LKr_dx_vs_dy", dx,dy);
        fUserMethods->FillHisto("LKr_x_vs_y", LKrpos.X(),LKrpos.Y());
        lkr_hintime.push_back(iH);

    }
    //fill a map with all newchod candidates matching within 5 ns of  reference time
    //and sort them by distance to the track extrapolated position
    //track matching cluster stays at position 0 at the map
    for(int iH=0;iH < fNewCHODEvent->GetNHits();iH++){

        TRecoNewCHODHit* newchod = (TRecoNewCHODHit*)newchodhits.At(iH);

        double newchodtime = newchod->GetTime();
        double newchoddt   = reftime - newchod->GetTime();

        TVector3 newchodpos= newchod->GetPosition();
        double dx=(atnewchod - newchodpos).X();
        double dy=(atnewchod - newchodpos).Y();

        double trackdist = sqrt(dx*dx+dy*dy);
        fUserMethods->FillHisto("NewCHOD_dt", newchoddt);
        //cout << "NewCHOD iH = " << iH << " track hit ID = " << track.GetNewCHODRecoHitID() << endl;
        if(fabs(newchoddt) > 5 || iH == track.GetNewCHODRecoHitID()) continue;
        //cout << "NewCHOD iH in time= " << iH << " track hit ID = " << track.GetNewCHODRecoHitID() << endl;;
        fUserMethods->FillHisto("NewCHOD_intime_dt", newchoddt);
        nch_hintime.push_back(iH);
    }

    //Make LKr-NEWCHOD and LKr-CHOD multiplicities
    //the multiplicity is defined as the number of lkr hits
    //that have a matching pair of slabs(hit pad) in
    //chod(newchod) not belonging to the track
    for(auto const& iH: lkr_hintime) {
        TRecoLKrHit* LKr = (TRecoLKrHit*)lkrhits.At(iH);
        double time = LKr->GetTime();
        Double_t ELKr = LKr->GetEnergy();
        TVector3 LKrpos= LKr->GetPosition();
        TVector2 LKrxy(LKrpos.X(),LKrpos.Y());

        for(auto const& iP: nch_hintime){

            TRecoNewCHODHit* newchod = (TRecoNewCHODHit*)newchodhits.At(iP);

            double newchodtime = newchod->GetTime();
            double newchoddt   = reftime - newchod->GetTime();
            double newchodlkrdt= time - newchod->GetTime();
            TVector3 newchodpos= newchod->GetPosition();
            TVector2 newchodxy(newchodpos.X(),newchodpos.Y());
            double dx1 = (LKrxy - newchodxy).X();
            double dy1 = (LKrxy - newchodxy).Y();
            double dxtr1 = (atlkr - newchodpos).X();
            double dytr1 = (atlkr - newchodpos).Y();
            double newchodlkrdist = (LKrxy - newchodxy).Mod();

            fUserMethods->FillHisto("NewCHODLKr_dt_vs_ehit", ELKr,newchodlkrdt);
            fUserMethods->FillHisto("NewCHODLKr_dist_vs_dt", newchodlkrdt,newchodlkrdist);
            fUserMethods->FillHisto("NewCHODLKr_track_dx_vs_dy", dxtr1, dytr1);
            fUserMethods->FillHisto("NewCHODLKr_dx_vs_dy", dx1, dy1);

            if(fabs(dx1) > 250 || fabs(dy1) > 140) continue;

            fUserMethods->FillHisto("NewCHODLKr_in_track_dx_vs_dy", dxtr1, dytr1);
            fUserMethods->FillHisto("NewCHODLKr_in_dist_vs_dt", newchodlkrdt,newchodlkrdist);
            fUserMethods->FillHisto("NewCHODLKr_in_dt_vs_ehit", ELKr,newchodlkrdt);
            fUserMethods->FillHisto("NewCHODLKr_in_dx_vs_dy", dx1, dy1);

            newchodlkr.push_back(std::make_pair(iP,iH));
        }


        vector<double> chodpar;
        Clear();
        if(!CHODQ1()) continue;
        chodpar = CHODTrackMatching(LKrxy.X(),LKrxy.Y(),time,1,fIsMC);
        TVector3 chodminpos(chodpar[7],chodpar[8],Constants::zCHOD);
        //Found CHOD matching NewCHOD candidate, now impose some conditions on the quality
        fUserMethods->FillHisto("CHODLKr_chi2", chodpar[0]);

        double dx2 = LKrxy.X() - chodpar[7];
        double dy2 = LKrxy.Y() - chodpar[8];

        TVector2 choddx(atchod.X()-chodpar[7],atchod.Y()-chodpar[8]);
        double dtrack=choddx.Mod();

        fUserMethods->FillHisto("CHODLKr_dt_vs_ehit", ELKr,chodpar[2]);
        fUserMethods->FillHisto("CHODLKr_dist_vs_dt", chodpar[2],sqrt(chodpar[1]));
        fUserMethods->FillHisto("CHODLKr_track_dx_vs_dy", choddx.X(),choddx.Y());
        fUserMethods->FillHisto("CHODLKr_dx_vs_dy", dx2, dy2);

        if(sqrt(chodpar[1]) > 130 ) continue;
        if(fabs(chodpar[2]) > 15 ) continue;
        if(chodpar[10] == track.GetCHODRecoHitHID() && chodpar[9] == track.GetCHODRecoHitVID()) continue;

        fUserMethods->FillHisto("CHODLKr_in_track_dx_vs_dy", choddx.X(), choddx.Y());
        fUserMethods->FillHisto("CHODLKr_in_dist_vs_dt", chodpar[2],sqrt(chodpar[1]));
        fUserMethods->FillHisto("CHODLKr_in_dt_vs_ehit", ELKr,chodpar[2]);
        fUserMethods->FillHisto("CHODLKr_in_dx_vs_dy", dx2, dy2);

        chodlkr.push_back(std::make_pair(chodpar[9],iH));
    }

    //Make CHOD-NEWCHOD multiplicity is defined as the number of
    //newchod hits that have a matching pair of slabs in chod not
    //belonging to the track
    for(auto const& iP: nch_hintime){

        TRecoNewCHODHit* newchod = (TRecoNewCHODHit*)newchodhits.At(iP);

        double time = newchod->GetTime();
        double newchoddt= time - newchod->GetTime();
        TVector3 newchodpos= newchod->GetPosition();
        TVector2 newchodxy(newchodpos.X(),newchodpos.Y());

        vector<double> chodpar;
        Clear();
        if(!CHODQ1()) continue;
        chodpar = CHODTrackMatching(newchodxy.X(),newchodxy.Y(),time,1,fIsMC);
        TVector3 chodminpos(chodpar[7],chodpar[8],Constants::zCHOD);
        //Found CHOD matching NewCHOD candidate, now impose some conditions on the quality
        fUserMethods->FillHisto("CHODNewCHOD_chi2", chodpar[0]);

        double dx2 = newchodxy.X() - chodpar[7];
        double dy2 = newchodxy.Y() - chodpar[8];

        TVector2 choddx(atchod.X()-chodpar[7],atchod.Y()-chodpar[8]);
        double dtrack=choddx.Mod();

        fUserMethods->FillHisto("CHODNewCHOD_dist_vs_dt", chodpar[2],sqrt(chodpar[1]));
        fUserMethods->FillHisto("CHODNewCHOD_track_dx_vs_dy", choddx.X(),choddx.Y());
        fUserMethods->FillHisto("CHODNewCHOD_dx_vs_dy", dx2, dy2);

        if(fabs(dx2) > 250 || fabs(dy2) > 140) continue;
        if(fabs(chodpar[2]) > 15 ) continue;
        if(chodpar[10] == track.GetCHODRecoHitHID() && chodpar[9] == track.GetCHODRecoHitVID()) continue;

        fUserMethods->FillHisto("CHODNewCHOD_in_track_dx_vs_dy", choddx.X(), choddx.Y());
        fUserMethods->FillHisto("CHODNewCHOD_in_dist_vs_dt", chodpar[2],sqrt(chodpar[1]));
        fUserMethods->FillHisto("CHODNewCHOD_in_dx_vs_dy", dx2, dy2);


        chodnewchod.push_back(std::make_pair(chodpar[9],iP));
    }

    int nstdcl(0);
    bool isclmatch=false;
    for(auto i(0);i<fLKrEvent->GetNCandidates();i++){
        //cout << i << " lkr tr id = " << track.GetLKrID() << endl;
        if(i==track.GetLKrID()){
            isclmatch=true;
            continue;
        }
        TRecoLKrCandidate* lkrcand = (TRecoLKrCandidate*)fLKrEvent->GetCandidate(i);
        double time = lkrcand->GetClusterTime();
        double xcl  = lkrcand->GetClusterX();
        double ycl  = lkrcand->GetClusterY();
        double dx   = atlkr.X() - xcl;
        double dy   = atlkr.Y() - ycl;
        double dist = sqrt(dx*dx+dy*dy);

        if(fabs(reftime-time) > 6) continue;
        if(dist < 40 || dist > 100) continue;
        nstdcl++;
    }

    bool isMerged=false;
    if( isclmatch &&nstdcl>0 && ch_hintime.size()>0)
        isMerged=true;

    //cout << "n extra std cl = " << nstdcl  << " merged = " << isMerged << endl;
    fUserMethods->FillHisto("CHOD_intime_Nmult",ch_hintime.size());
    fUserMethods->FillHisto("NewCHOD_intime_Nmult",nch_hintime.size());
    fUserMethods->FillHisto("LKr_intime_Nmult",lkr_hintime.size());
    fUserMethods->FillHisto("LKrCHOD_Nmult",chodlkr.size());
    fUserMethods->FillHisto("LKrNewCHOD_Nmult",newchodlkr.size());
    fUserMethods->FillHisto("CHODNewCHOD_Nmult",chodnewchod.size());
    fUserMethods->FillHisto("LKrMerged",isMerged);

    track.SetLKrMerged(isMerged);
    track.SetLKrMult(lkr_hintime.size());
    track.SetCHODSlabsInTime(ch_hintime.size());
    track.SetNewCHODMult(newchodlkr.size());
    track.SetCHODMult(chodlkr.size());
    track.SetCHODNewCHODMult(chodnewchod.size());
    return true;
}

Int_t CHODAnal::Quadrant(Int_t channelid) {
    if (channelid<=63) {
        if (channelid>=0  && channelid <=15) return 0;
        if (channelid>=16 && channelid <=31) return 1;
        if (channelid>=32 && channelid <=47) return 2;
        if (channelid>=48 && channelid <=63) return 3;
    } else {
        if (channelid>=64  && channelid<=79)  return 0;
        if (channelid>=80  && channelid<=95)  return 1;
        if (channelid>=96  && channelid<=111) return 2;
        if (channelid>=112 && channelid<=127) return 3;
    }
    return -1;
}

Int_t CHODAnal::MultiplicityCellMatching(Double_t reftime,  TVector3 posatlkr, Int_t flag) {
    Double_t xpart = posatlkr.X();
    Double_t ypart = posatlkr.Y();
    Int_t minid = -1;
    Double_t timeshift = 0;

    timeshift = -1.51;

    // Look for cells associated to the track, if no cluster candidate matches the track
    Double_t eclus = 0.;
    Double_t tclus = 0.;
    Double_t xclus = 0.;
    Double_t yclus = 0.;
    Int_t ncells = 0;
    Double_t emax = -999999.;
    TClonesArray& Hits = (*(fLKrEvent->GetHits()));
    for (Int_t jHit=0; jHit<fLKrEvent->GetNHits(); jHit++) {

        TRecoLKrHit *hit = (TRecoLKrHit*)Hits[jHit];
        Double_t xcell = hit->GetPosition().X();
        Double_t ycell = hit->GetPosition().Y();
        Double_t ecell = hit->GetEnergy()/1000.;
        if (ecell<=-999999.) continue;
        // --> "Dead cells" are not passed to the reconstruction.
        // Double_t t0cell = (ixcell>=0 && iycell>=0) ? fLKrCellT0[ixcell][iycell] : 0;
        Double_t tcell = hit->GetTime() -1 +timeshift;
        Double_t dist = sqrt((xcell-xpart)*(xcell-xpart)+(ycell-ypart)*(ycell-ypart));
        Double_t dtime = tcell-reftime;
        if (dist<100. && fabs(dtime)<10) { // Select a region around the extrapolated track position at LKr
            eclus += ecell;
            if (emax<ecell) {
                emax = ecell;
                tclus = tcell+1.3;
            }
            xclus += xcell*ecell;
            yclus += ycell*ecell;
            ncells++;
            minid = 1;
        }
    }
    if (!ncells || emax<0.04) { // no cells found
        tclus = -99999.;
        eclus = 0;
        xclus = -99999.;
        yclus = -99999.;
        minid = -1;
        return minid;
    }

    xclus /= eclus;
    yclus /= eclus;
    Double_t ue = eclus;
    if (ncells>9) {
        if (ue<22) eclus = ue/(0.7666+0.0573489*log(ue));
        if (ue>=22 && ue<65) eclus = ue/(0.828962+0.0369797*log(ue));
        if (ue>=65) eclus = ue/(0.828962+0.0369797*log(65));
    }
    eclus *= 1.03;
    eclus = eclus<15 ? (eclus+0.015)/(15+0.015)*15*0.9999 : eclus;

    //if(ncells < 2) return -1;
    Double_t mindist = sqrt((xclus-xpart)*(xclus-xpart)+(yclus-ypart)*(yclus-ypart));

    double cellratio = ncells/eclus;
    double seedratio = emax/eclus;
    if (flag==0) {

        fUserMethods->FillHisto("LKrMultCell_track_mindist_vs_dtchod",tclus-reftime,mindist);
        fUserMethods->FillHisto("LKrMultCell_track_cellratio_vs_eratio",seedratio,cellratio);
        fUserMethods->FillHisto("LKrMultCell_track_minxy",xclus-xpart,yclus-ypart);
        fUserMethods->FillHisto("LKrMultCell_track_energy_vs_dist",mindist,eclus);
    }

    if (flag==1) {

        fUserMethods->FillHisto("LKrMultCell_nontrack_mindist_vs_dtchod",tclus-reftime,mindist);
        fUserMethods->FillHisto("LKrMultCell_nontrack_cellratio_vs_eratio",seedratio,cellratio);
        fUserMethods->FillHisto("LKrMultCell_nontrack_minxy",xclus-xpart,yclus-ypart);
        fUserMethods->FillHisto("LKrMultCell_nontrack_energy_vs_dist",mindist,eclus);
    }

    if (flag==2) {
        fUserMethods->FillHisto("LKrMultCell_nontrack_hadr_mindist_vs_dtchod",tclus-reftime,mindist);
        fUserMethods->FillHisto("LKrMultCell_nontrack_hadr_cellratio_vs_eratio",seedratio,cellratio);
        fUserMethods->FillHisto("LKrMultCell_nontrack_hadr_minxy",xclus-xpart,yclus-ypart);
        fUserMethods->FillHisto("LKrMultCell_nontrack_hadr_energy_vs_dist",mindist,eclus);

    }
    return minid;
}


bool CHODAnal::CHODQ1() {
    bool isQ1 = false;

    // Sort the hits per increasing channel number
    vector<int> idHit(fCHODEvent->GetNHits());
    iota(begin(idHit),end(idHit),0);
    ChannelOrder co(fCHODEvent);
    sort(idHit.begin(),idHit.end(),co);

    // Divide hits per plane if any
    PlaneCondition pc(fCHODEvent);
    auto from = idHit.begin();
    int nV = distance(from,partition(from,idHit.end(),pc));
    if (!nV) return isQ1;
    if (!(fCHODEvent->GetNHits()-nV)) return isQ1;
    vector<int> vP[2];
    vP[0].assign(from,from+nV);
    vP[1].assign(from+nV,idHit.end());

    // Separate hits per quadrant
    vector<int>::iterator fromP[2];
    fromP[0] = vP[0].begin();
    fromP[1] = vP[1].begin();
    int jQ = 0;
    vector<int> vQ[2];
    while ((fromP[0]!=vP[0].end() || fromP[1]!=vP[1].end()) && jQ<4) {
        for (int jP(0); jP<2; jP++) {
            QuadrantCondition qc(fCHODEvent,jQ,jP);
            int nQ = distance(fromP[jP],partition(fromP[jP],vP[jP].end(),qc));
            if (!nQ) continue;
            //cout << jP << "  " << jQ << "   " << fromP[jP] << "  " << fromP[jP]+nQ << endl;
            fvCHODQ[jP][jQ].assign(fromP[jP],fromP[jP]+nQ);
            fromP[jP] += nQ;
        }
        jQ++;
    }

    // Select events with hits in corresponding H-V quadrants
    for (int jq(0); jq<4; jq++) {
        if (fvCHODQ[0][jq].size()&&fvCHODQ[1][jq].size()) isQ1 = true;
    }

    return isQ1;
}


double CHODAnal::CHODTimeCorrection(int iS, int iP, double tot, double xslab, double yslab, double enslew) {
    double t0(0);

    //if(fUserMethods->GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.0") || !fIsMC)
    if(fUserMethods->GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.1") || fUserMethods->GetStreamInfo()->GetMCInfo().GetRevision().EqualTo("v0.11.2"))
        t0 = fCHODLightVelocitiesCorr[iS][iP]<99. ? fCHODLightVelocitiesCorr[iS][iP]-fCHODAllFineT0[iS][iP] : 0.;
    else
        t0 = fCHODAllT0[iS][iP]<99. ? fCHODAllT0[iS][iP]-(fCHODAllFineT0[iS][iP]) : 0.;
    //t0 = fCHODAllT0[iS][iP]<99. ? fCHODAllT0[iS][iP] : 0.;
    double efftot = tot<15. ? tot : 15.;
    double sC = tot>0. ? fCHODAllSlewSlope[iS][iP]*efftot+fCHODAllSlewConst[iS][iP] : 0.;

    //if (fUserMethods->GetWithMC()) {
    //t0 = fCHODAllT0[iS][iP]<99. ? fCHODAllT0[iS][iP]-(fCHODAllFineT0[iS][iP]-0.1) : 0.;
    //t0 = 0;
    //if (iS<64) return (fCHODT0[iS]-fCHODVelocity[iS]*(fabs(yslab)+(fCHODLengthV[0]-fCHODLengthV[iS]))+9.);
    //if (iS>63) return (fCHODT0[iS]-fCHODVelocity[iS]*(fabs(xslab)+(fCHODLengthV[0]-fCHODLengthH[iS-64]))+9.);
    //}

    if(t0==0) sC=0;
    return t0+enslew*sC;

}

vector<double> CHODAnal::CHODTrackMatching(double xPos, double yPos, double ttime, int flag,bool mcflag) {
    vector<double> par(9);
    double minpar[11];

    //CHODQ1();
    //cout << "r mc "  <<  fUserMethods->GetStreamInfo()->GetRecoInfo().GetRevision().Data()<< endl;
    // Loop over the quadrant pairs
    double minchi2chod = 99999999.;

    for (int jQ(0); jQ<4; jQ++) {
        if (!fvCHODQ[0][jQ].size() || !fvCHODQ[1][jQ].size()) continue;

        double minchi2 = 99999999.;
        for (auto &iV : fvCHODQ[0][jQ]) {
            TRecoCHODHit *HitV = (TRecoCHODHit*)fCHODEvent->GetHit(iV);
            int counterV = HitV->GetChannelID();
            for (auto &iH : fvCHODQ[1][jQ]) {
                TRecoCHODHit *HitH = (TRecoCHODHit*)fCHODEvent->GetHit(iH);
                int counterH = HitH->GetChannelID();
                double tVcorr = HitV->GetTime()-CHODTimeCorrection(counterV,(counterH-64)%16,HitV->GetTimeWidth(),fCHODPosV[counterV],fCHODPosH[counterH-64],1);
                double tHcorr = HitH->GetTime()-CHODTimeCorrection(counterH,counterV%16,HitH->GetTimeWidth(),fCHODPosV[counterV],fCHODPosH[counterH-64],1);

                double dtime = 0.5*(tVcorr+tHcorr)-ttime;
                double dist2 = (fCHODPosV[counterV]-xPos)*(fCHODPosV[counterV]-xPos)+(fCHODPosH[counterH-64]-yPos)*(fCHODPosH[counterH-64]-yPos);
                double chi2chod;
                if(flag==0)
                    chi2chod = (tVcorr-tHcorr)*(tVcorr-tHcorr)/(9*3*3)+dtime*dtime/(9*7*7)+dist2/(4*13*13); // Check the weights !
                if(flag==1)
                    chi2chod = (tVcorr-tHcorr)*(tVcorr-tHcorr)/(9*6*6)+dtime*dtime/(3*7*7)+dist2/(6*16*16); // Check the weights !

                if (chi2chod<minchi2) {
                    minchi2 = chi2chod;
                    minpar[0] = minchi2;
                    minpar[1] = dist2;
                    minpar[2] = dtime;
                    minpar[3] = (double)counterV;
                    minpar[4] = (double)counterH;
                    minpar[5] = tVcorr;
                    minpar[6] = tHcorr;
                    minpar[7] = fCHODPosV[counterV];//x position
                    minpar[8] = fCHODPosH[counterH-(int)64];//y position
                    minpar[9] = iV;//vertical hit index
                    minpar[10] = iH;//horizontal hit index
                }
            }
        }
        if (minchi2<minchi2chod) {
            minchi2chod = minchi2;
            par.clear();
            for (int k(0); k<11; k++) par.push_back(minpar[k]);
        }
    }

    if(flag==0){

        fCHODCandidate->SetDiscriminant(par[0]);
        fCHODCandidate->SetDeltaTime(par[2]);
        fCHODCandidate->SetX(par[7]);
        fCHODCandidate->SetY(par[8]);
        fCHODCandidate->SetRecoHitVID(par[9]);//vertical reco hit index
        fCHODCandidate->SetRecoHitHID(par[10]);//horizontal reco hit index
        fCHODCandidate->SetCounterV(par[3]);
        fCHODCandidate->SetCounterH(par[4]);
        fCHODCandidate->SetTimeV(par[5]);
        fCHODCandidate->SetTimeH(par[6]);

    }

    return par;
}


vector<double> CHODAnal::CHODMultMatching(double xPos, double yPos, double ttime,int itV, int itH,int flag,bool mcflag) {
    vector<double> par(9);
    double minpar[11];

    for(auto i(0);i < 11;i++){
        minpar[i]=99999999999;
    }
    // Loop over the quadrant pairs
    double minchi2chod = 99999999.;
    //cout << " NCHODhits = " << fCHODEvent->GetNHits() << endl;
    for (int jQ(0); jQ<4; jQ++) {
        if (!fvCHODQ[0][jQ].size() || !fvCHODQ[1][jQ].size()) continue;
        //cout << "Vsize = "<< fvCHODQ[0][jQ].size() << " Hsize = " << fvCHODQ[1][jQ].size() << endl;
        double minchi2 = 99999999.;
        for (auto &iV : fvCHODQ[0][jQ]) {
            //cout << "iV = " << iV << endl;
            if(iV==itV) continue; //no matching with the track hits
            //cout << "iV pass = " << iV  << "iTrack = " << itV << endl;
            TRecoCHODHit *HitV = (TRecoCHODHit*)fCHODEvent->GetHit(iV);
            int counterV = HitV->GetChannelID();
            for (auto &iH : fvCHODQ[1][jQ]) {
                //cout << "iH = " << iH << endl;
                if(iH==itH) continue;//no matching with the track hits
                //cout << "iH pass = " << iH  << "iTrack = " << itH << endl;
                TRecoCHODHit *HitH = (TRecoCHODHit*)fCHODEvent->GetHit(iH);
                int counterH = HitH->GetChannelID();
                //cout << "NOT FAILL " << itH << endl;
                double tVcorr = HitV->GetTime()-CHODTimeCorrection(counterV,(counterH-64)%16,HitV->GetTimeWidth(),fCHODPosV[counterV],fCHODPosH[counterH-64],1);
                double tHcorr = HitH->GetTime()-CHODTimeCorrection(counterH,counterV%16,HitH->GetTimeWidth(),fCHODPosV[counterV],fCHODPosH[counterH-64],1);

                double dtime = 0.5*(tVcorr+tHcorr)-ttime;
                double dist2 = (fCHODPosV[counterV]-xPos)*(fCHODPosV[counterV]-xPos)+(fCHODPosH[counterH-64]-yPos)*(fCHODPosH[counterH-64]-yPos);
                double chi2chod(99999);
                if(flag==0)
                    chi2chod = (tVcorr-tHcorr)*(tVcorr-tHcorr)/(9*3*3)+dtime*dtime/(9*7*7)+dist2/(4*13*13); // Check the weights !
                if(flag==1)
                    chi2chod = (tVcorr-tHcorr)*(tVcorr-tHcorr)/(9*6*6)+dtime*dtime/(3*7*7)+dist2/(6*16*16); // Check the weights !

                if (chi2chod<minchi2) {
                    minchi2 = chi2chod;
                    minpar[0] = minchi2;
                    minpar[1] = dist2;
                    minpar[2] = dtime;
                    minpar[3] = (double)counterV;
                    minpar[4] = (double)counterH;
                    minpar[5] = tVcorr;
                    minpar[6] = tHcorr;
                    minpar[7] = fCHODPosV[counterV];//x position
                    minpar[8] = fCHODPosH[counterH-(int)64];//y position
                    minpar[9] = iV;//vertical hit index
                    minpar[10]= iH;//horizontal hit index
                }
            }
        }
        if (minchi2<minchi2chod) {
            minchi2chod = minchi2;
            par.clear();
            for (int k(0); k<11; k++) par.push_back(minpar[k]);

        }
    }

    if (minchi2chod==99999999) {
        par.clear();
        for (int k(0); k<11; k++) par.push_back(minpar[k]);
    }

    return par;
}

void CHODAnal::AlignChannels(Double_t reftime) {
    Int_t counterV = fCHODCandidate->GetCounterV(); // 0-63
    Int_t counterH = fCHODCandidate->GetCounterH(); // 64-127
    Int_t chid = 16*counterV+((counterH-64)%16);
    fUserMethods->FillHisto("CHODAnalysis_dtime",chid,reftime-fCHODCandidate->GetTimeV());
    chid = 16*counterH+(counterV%16);
    fUserMethods->FillHisto("CHODAnalysis_dtime",chid,reftime-fCHODCandidate->GetTimeH());
}
