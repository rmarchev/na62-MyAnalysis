#include "NewCHODCandidate.hh"

NewCHODCandidate::NewCHODCandidate()
{}

NewCHODCandidate::~NewCHODCandidate()
{}

void NewCHODCandidate::Clear() {
    fID = -1;
  fDiscriminant = 99999999.;
  fDeltaTime = 99999999.;
  fX = -99999.;
  fY = -99999.;
  fIsNewCHODCandidate = 0;
}
