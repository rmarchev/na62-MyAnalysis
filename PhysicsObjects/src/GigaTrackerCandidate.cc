#include "GigaTrackerCandidate.hh"

GigaTrackerCandidate::GigaTrackerCandidate()
{}

GigaTrackerCandidate::~GigaTrackerCandidate()
{}

void GigaTrackerCandidate::Clear() {
  fIsGigaTrackerCandidate = 0;
  fMomentum = 0;
  fSlopeXZ = 9999999.;
  fSlopeYZ = 9999999.;
  fTime = 9999999.;
  fTime1 = 9999999.;
  fTime2 = 9999999.;
  fTime3 = 9999999.;
  fPosition.SetXYZ(-99999.,-99999.,-99999.);
  fPosGTK1.SetXYZ(-99999.,-99999.,-99999.);
  fPosGTK2.SetXYZ(-99999.,-99999.,-99999.);
  fDiscriminant = -1;
  fChi2X = 999999.;
  fType = 0;
}
