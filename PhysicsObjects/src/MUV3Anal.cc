#include "MUV3Anal.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"

MUV3Anal::MUV3Anal(NA62Analysis::Core::BaseAnalysis*ba) {
  Parameters *par = Parameters::GetInstance();
  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);

  fMUV3Anal_dtmin.Form("MUV3Analysis_dtmin");
  fUserMethods->BookHisto(new TH1F(fMUV3Anal_dtmin.Data(),"",200,-100,100));
  for (Int_t j=0; j<3; j++) {
    fMUV3Anal_distvst[j].Form("MUV3Analysis_distvst_%d",j);
    fUserMethods->BookHisto(new TH2F(fMUV3Anal_distvst[j].Data(),"",200,-100,100,300,0,600));
    fMUV3Anal_xy[j].Form("MUV3Analysis_xy_%d",j);
    fUserMethods->BookHisto(new TH2F(fMUV3Anal_xy[j].Data(),"",300,-600,600,300,-600,600));
    fMUV3Anal_distvsdeltat[j].Form("MUV3Analysis_distvsdeltat_%d",j);
    fUserMethods->BookHisto(new TH2F(fMUV3Anal_distvsdeltat[j].Data(),"",200,-50,50,300,0,600));
    fMUV3Anal_mindistvst[j].Form("MUV3Analysis_mindistvst_%d",j);
    fUserMethods->BookHisto(new TH2F(fMUV3Anal_mindistvst[j].Data(),"",200,-100,100,300,0,600));
    fMUV3Anal_minxy[j].Form("MUV3Analysis_minxy_%d",j);
    fUserMethods->BookHisto(new TH2F(fMUV3Anal_minxy[j].Data(),"",300,-600,600,300,-600,600));
  }


  fMUV3TightCandidate = new MUV3Candidate();
  fMUV3MediumCandidate = new MUV3Candidate();
  fMUV3BroadCandidate = new MUV3Candidate();
}

MUV3Anal::~MUV3Anal()
{ }

void MUV3Anal::Clear() {
  fMUV3TightCandidate->Clear();
  fMUV3MediumCandidate->Clear();
  fMUV3BroadCandidate->Clear();
}

Int_t MUV3Anal::TrackTimeMatching(TRecoSpectrometerCandidate *thisTrack) {

  std::map<double,std::pair<double,int>> timemap;

  for (Int_t jcand=0; jcand<fMUV3Event->GetNCandidates(); jcand++) {
    TRecoMUV3Candidate *thisCand = (TRecoMUV3Candidate *)fMUV3Event->GetCandidate(jcand);

    Double_t dtime = thisCand->GetTime()-thisTrack->GetTime();
    timemap.insert(std::make_pair(fabs(dtime),std::pair<double,int>(thisCand->GetTime(),jcand)));


  }

  if(timemap.size()==0) return -1;
  double dtmin =timemap.begin()->second.first - thisTrack->GetTime();
  int imin = timemap.begin()->second.second;
  fUserMethods->FillHisto(fMUV3Anal_dtmin.Data(),dtmin);
  fMUV3TightCandidate->SetIsMUV3Candidate(1);
  fMUV3TightCandidate->SetDeltaTime(dtmin);

  return imin;

}


Int_t MUV3Anal::TrackMatching(Int_t flag,TRecoSpectrometerCandidate *thisTrack) {
  TVector3 posatmuv = fTools->GetPositionAtZ(thisTrack,246800.);
  TVector2 postrack(posatmuv.X(),posatmuv.Y());

  // Look for a cluster matching the track (distance criteria only)
  Int_t minid = -1;
  Double_t mindist = 99999999.;
  Double_t mintime = -99999.;
  Double_t mindx = -99999.;
  Double_t mindy = -99999.;
  Int_t mintype = -1;
  for (Int_t jcand=0; jcand<fMUV3Event->GetNCandidates(); jcand++) {
    TRecoMUV3Candidate *thisCand = (TRecoMUV3Candidate *)fMUV3Event->GetCandidate(jcand);
    TVector2 poscand(thisCand->GetX(),thisCand->GetY());
    Double_t dist = (poscand-postrack).Mod();
    Double_t dtime = thisCand->GetTime()-thisTrack->GetTime();
    Double_t dx = (poscand-postrack).X();
    Double_t dy = (poscand-postrack).Y();
    if (flag==0 && thisCand->GetType()!=kTightCandidate) continue;
    if (flag==1 && (thisCand->GetType()!=kTightCandidate && thisCand->GetType()!=kLooseMaskedCandidate)) continue;
    fUserMethods->FillHisto(fMUV3Anal_distvst[flag].Data(),dtime,dist);
    fUserMethods->FillHisto(fMUV3Anal_xy[flag].Data(),dx,dy);
    fUserMethods->FillHisto(fMUV3Anal_distvsdeltat[flag].Data(),thisCand->GetDeltaTime(),dist);
    if (dist<mindist) {
      mindist = dist;
      minid = jcand;
      mintime = dtime;
      mindx = dx;
      mindy = dy;
      mintype = thisCand->GetType();
    }
  }
  if (minid==-1) return minid;
  fUserMethods->FillHisto(fMUV3Anal_mindistvst[flag].Data(),mintime,mindist);
  fUserMethods->FillHisto(fMUV3Anal_minxy[flag].Data(),mindx,mindy);

  switch (flag) {
  case 0:
    fMUV3TightCandidate->SetIsMUV3Candidate(1);
    fMUV3TightCandidate->SetDiscriminant(mindist);
    fMUV3TightCandidate->SetDeltaTime(mintime);
    fMUV3TightCandidate->SetType(mintype);
    break;
  case 1:
    fMUV3MediumCandidate->SetIsMUV3Candidate(1);
    fMUV3MediumCandidate->SetDiscriminant(mindist);
    fMUV3MediumCandidate->SetDeltaTime(mintime);
    fMUV3MediumCandidate->SetType(mintype);
    break;
  case 2:
    fMUV3BroadCandidate->SetIsMUV3Candidate(1);
    fMUV3BroadCandidate->SetDiscriminant(mindist);
    fMUV3BroadCandidate->SetDeltaTime(mintime);
    fMUV3BroadCandidate->SetType(mintype);
    break;
  }

  return minid;
}
