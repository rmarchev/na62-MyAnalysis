#include "StrawIntersection.hh"

StrawIntersection::StrawIntersection()
{}

StrawIntersection::~StrawIntersection()
{}

void StrawIntersection::Clear() {
  fTrailingTime = -99999.;
  fQuality = 9999.;
  fSubType = -1; 
  fX=fY=fU=fV=fXcoor=fYcoor=-9999.;
  fClusterId.clear();
  fViewId.clear();
}

void StrawIntersection::SetCoordinate(double *xycoor)
{
  double sq2 = sqrt(2.);
  fXcoor = xycoor[0]; 
  fYcoor = xycoor[1]; 
  fX = xycoor[0]; 
  fY = xycoor[1]; 
  fU = xycoor[2]; 
  fV = xycoor[3]; 

  switch (fSubType) {
    case 20:
    fV = -9999;
    fX = -9999;
    fY = -9999;
    fXcoor = -9999;
    fYcoor = -9999;
    break;

    case 21:
    fU = -9999;
    fX = -9999;
    fY = -9999;
    fXcoor = -9999;
    fYcoor = -9999;
    break;

    case 22:
    fV = -9999;
    fU = -9999;
    fY = -9999;
    fYcoor = -9999;
    break;

    case 23:
    fU = -9999;
    fV = -9999;
    fX = -9999;
    fXcoor = -9999;
    break;

    case 14:
    fV = -9999; 
    break;

    case 13:
    fU = -9999; 
    break;

    case 11:
    fY = -9999; 
    fYcoor = (-fU+fV)/sq2;
    break;
  
    case 7:
    fX = -9999; 
    fXcoor = (fU+fV)/sq2;
    break;

    case 5:
    fX = -9999;
    fY = -9999;
    fXcoor = (fU+fV)/sq2;
    fYcoor = (-fU+fV)/sq2;
    break;

    case 4:
    fX = -9999;
    fU = -9999;
    fXcoor = -fY+fV*sq2;
    break;

    case 3:
    fX = -9999;
    fV = -9999;
    fXcoor = fY+fU*sq2;
    break;

    case 2:
    fY = -9999;
    fU = -9999;
    fYcoor = -fX+fV*sq2;
    break;

    case 1:
    fY = -9999;
    fV = -9999;
    fYcoor = fX-fU*sq2;
    break;

    case 0:
    fU = -9999;
    fV = -9999;
    break;

    default:
    break;
  }
}
