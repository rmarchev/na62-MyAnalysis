#include "SACAnal.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"
//#include "PhotonVetoCandidate.cc"

SACAnal::SACAnal(NA62Analysis::Core::BaseAnalysis *ba) {
  //fPar = Parameters::GetInstance();
  fPar = new Parameters();

  // SAC priority definition
  fSACPriorityMask[0] = 0; // --SNH--
  fSACPriorityMask[1] = 4; // LL __ __ __
  fSACPriorityMask[2] = 5; // __ LH __ __
  fSACPriorityMask[3] =10; // LL LH __ __
  fSACPriorityMask[4] = 2; // __ __ TH __
  fSACPriorityMask[5] = 7; // LL __ TH __
  fSACPriorityMask[6] =11; // __ LH TH __
  fSACPriorityMask[7] =13; // LL LH TH __
  fSACPriorityMask[8] = 1; // __ __ __ TL
  fSACPriorityMask[9] =12; // LL __ __ TL
  fSACPriorityMask[10]= 6; // __ LH __ TL
  fSACPriorityMask[11]=14; // LL LH __ TL
  fSACPriorityMask[12]= 3; // __ __ TH TL
  fSACPriorityMask[13]= 8; // LL __ TH TL
  fSACPriorityMask[14]= 9; // __ LH TH TL
  fSACPriorityMask[15]=15; // LL LH TH TL

  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);

  // Histo booking
  fUserMethods->BookHisto(new TH2F("SACAnalysis_hit_chtime","",10,0,10,400,-50,50));
  fUserMethods->BookHisto(new TH2F("SACAnalysis_hit_tottime","",400,0,200,400,-50,50));
  fUserMethods->BookHisto(new TH2F("SACAnalysis_hit_besttime","",400,0,400,400,-50,50));
  fUserMethods->BookHisto(new TH2F("SACAnalysis_hit_mintime","",400,0,400,400,-50,50));

  // Photon candidates
  for (Int_t kk=0; kk<20; kk++) fPhotonCandidate[kk] = new PhotonVetoCandidate();
}

void SACAnal::StartBurst(Int_t year, Int_t runid) {
  if (year==2015) fPar->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/sac_t0_2015.dat");
  if (year==2016) {
    TString t0name;
    t0name.Form("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/run%d/sac_t0_2016.dat",runid);
    fPar->LoadParameters(t0name.Data());
    cout << t0name.Data() << endl;
  }
  fPar->StoreSACParameters();
  fSACChannelT0 = fPar->GetSACChannelT0();
  fYear = year;
  fIsMC = fUserMethods->GetWithMC();
  fRunID = runid;
  if (year==2015) fTimeCut = 15;
  if (year==2016) fTimeCut = 7;
}

void SACAnal::Clear() {
  for (Int_t kk=0; kk<20; kk++) fPhotonCandidate[kk]->Clear();
}

Int_t SACAnal::MakeCandidate(Double_t reftime, TRecoSACEvent* event) {

  TClonesArray& Hits = (*(event->GetHits()));

  // At least a good hit not noisy within 10 ns from the reference time
  Int_t bestSACHitType = 0;
  for (Int_t jHit=0; jHit<event->GetNHits(); jHit++) {
    TRecoSACHit *hit = (TRecoSACHit*)Hits[jHit];
    Int_t chid = hit->GetChannelID();
    Int_t edge = hit->GetEdgeMask();
    double time = TimeSlewCorrected(hit);

      //Double_t dtime = hit->GetTime()-reftime;
      Double_t dtime = !fIsMC ? time-reftime-fSACChannelT0[chid] : time -reftime;
    fUserMethods->FillHisto("SACAnalysis_hit_chtime",chid,dtime);
    if (fSACPriorityMask[edge]>bestSACHitType && fabs(dtime)<15) bestSACHitType = fSACPriorityMask[edge];
  }
  if (!event->GetNHits()) return 0;
  if (fYear==2016) {
      //if (bestSACHitType<9) return 0;
  }

  Int_t nsac = 0;
  Double_t etot = 0;
  Double_t etot2 = 999999.;
  Double_t maxtot = 0;
  Double_t maxtime = 999999.;
  Double_t mintime = 999999.;
  for (Int_t jHit=0; jHit<event->GetNHits(); jHit++) {
    TRecoSACHit *hit = (TRecoSACHit*)Hits[jHit];
    Int_t chid = hit->GetChannelID();
    Int_t edge = hit->GetEdgeMask();
    if (fYear==2016) {
        //    if (fSACPriorityMask[edge]<9) continue;
    }
    // Double_t dtime = hit->GetTime()-reftime-fSACChannelT0[chid];
    Double_t dtime = hit->GetTime()-reftime;
    Double_t tot = (hit->GetTrailingEdgeLow() && hit->GetLeadingEdgeLow()) ? hit->GetTrailingEdgeLow()-hit->GetLeadingEdgeLow() : 0;
    fUserMethods->FillHisto("SACAnalysis_hit_tottime",tot,dtime);

    if (tot>=2) {
        if (fabs(dtime)<fabs(mintime)) { // hit closest in time to ref
            mintime = dtime;
        }
    } else {
        if (fabs(dtime+3)<fabs(mintime+3)) { // hit closest in time to ref
            mintime = dtime;
        }
    }

    etot += tot;
    if (tot>=maxtot) {
      maxtot = tot;
      maxtime = dtime;
    }
    if (nsac<20) {
      fPhotonCandidate[nsac]->SetTime(dtime+reftime);
      fPhotonCandidate[nsac]->SetID(jHit);
    }
    nsac++;
  }
  if (nsac) {
    if (fYear==2015) {
      if (etot==0) mintime -= 8;
    }
    fUserMethods->FillHisto("SACAnalysis_hit_besttime",etot,maxtime);
    fUserMethods->FillHisto("SACAnalysis_hit_mintime",etot,mintime);
  }

  if (nsac) {
    if (etot<2) {
      if (mintime>-7 && mintime<10) return 1;
    }
    if (etot>=2 && etot<25.) {
      if (fabs(mintime)<7) return 1;
      if (fabs(mintime-(-11.3524+0.2105*etot))<3.) return 1;
    }
    if (etot>=25.) {
      if (fabs(mintime)<7.) return 1;
    }
  }


  //if (nsac && fabs(mintime)<fTimeCut) return 1;

  return 0;
}
double SACAnal::TimeSlewCorrected(TRecoSACHit *hit) {
  double tot = (hit->GetTrailingEdgeLow() && hit->GetLeadingEdgeLow()) ? hit->GetTrailingEdgeLow()-hit->GetLeadingEdgeLow() : 0;
  double tslew = 0;
  if (tot>5&&tot<70) tslew = 9.366-0.292*tot+0.002078*tot*tot;
  if (tot>=70) tslew = 9.366-0.292*70+0.002078*70*70;
  return hit->GetTime()-tslew;
}
