#include "UpstreamParticle.hh"



void UpstreamParticle::Clear()
{
  //Particle::Clear();
    fCHODDiscr = -999.;
    fRICHDiscr = -999.;
    fGoodKTAG = 0;
    fGoodGTK = 0;
    fGoodCHANTI = 0;

  fDownstreamTrackID = -1;
  fIsBeamPionCandidate = 0;
  fIsKTAGCandidate = 0;
  fCHODMult = 0;
  fKTAGID = -1;
  fKTAGTime = -99999.;
  fIsCHANTICandidate = 0;
  fCHANTIID = -1;
  fCHANTITime = -99999.;
  fVertex.SetXYZ(-99999.,-99999.,0.);
  fCDA = 9999999.;
  fMomentumAtDecay.SetXYZM(-99999.,-99999.,0.,0.);
  fNGTKCandidates = 0;
  //for (int jG(0); jG<10; jG++) fGTKCandidate[jG].Clear();
  fChi2X = 999999999.;
  fPosGTK1.SetXYZ(0.,0.,0.);
  fPosGTK2.SetXYZ(0.,0.,0.);
  fNGTK1Hits = 0;
  fNGTK2Hits = 0;
  fNGTK3Hits = 0;
  for (int jH(0); jH<50; jH++) {
    fGTK1Hit[jH] = -1;
    fGTK2Hit[jH] = -1;
    fGTK3Hit[jH] = -1;
  }
}
